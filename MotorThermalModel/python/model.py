# this is a simple model of a motor designed to extend over the standard linear model by including temperature 
# variable resistance. It is the hope of the author that this will allow more accurate matching of the datasheet 
# test data and less conservative models overall.
import numpy as np
import matplotlib.pyplot as plt
t=np.linspace(0, 5, 5000)
plt.plot([0,5],[1,1],'r')
plt.plot([0,1,1,5],[5.06,5.06,0,0],'b')
plt.show()