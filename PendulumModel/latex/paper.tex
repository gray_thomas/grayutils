%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2345678901234567890123456789012345678901234567890123456789012345678901234567890
%        1         2         3         4         5         6         7         8

\documentclass[letterpaper, 10 pt, conference]{ieeeconf}  % Comment this line out if you need a4paper
%\documentclass[a4paper, 10pt, conference]{ieeeconf}      % Use this line for a4 paper

\IEEEoverridecommandlockouts                              % This command is only needed if 
                                                          % you want to use the \thanks command

\overrideIEEEmargins                                      % Needed to meet printer requirements.

% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document

% The following packages can be found on http:\\www.ctan.org
\usepackage{graphicx} % for pdf, bitmapped graphics files
%\usepackage{epsfig} % for postscript graphics files
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{times} % assumes new font selection scheme installed
\usepackage{amsmath} % assumes amsmath package installed
\usepackage{amssymb}  % assumes amsmath package installed

\title{\LARGE \bf
Linear Systems Analysis Final Project
}


\author{Gray C. Thomas$^{1}$
\thanks{$^{1}$Department of Mechanical Engineering,
        University of Texas at Austin, Austin, TX 78712, USA
}}


\begin{document}



\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
This project discusses the control of a Bell-Boeing V-22 Osprey Tilt-rotor system using two separate simplified linear models for altitude and forward velocity dynamics. The altitude model is parametric in one variable, and the sensitivity of the model to this parameter is analysed. The closed loop systems are judged on their step response overshoot, settling time, and on their impulse responses. The altitude system's process disturbance step response is also analysed.
\end{abstract}
\setcounter{equation}{0}
\section*{Altitude System}
\subsection{Impulse Response}
Starting with the simple model of the altitude transfer function \eqref{Gs}, and a PID controller \eqref{Gcs} we can put together a realizable transfer function for the forward chain dynamics \eqref{forwardChain}. However, while the forward chain is strictly proper, the PID controller alone is not strictly speaking realizable as it has more zeros than poles. The controller would be implemented in a real system by approximating the derivative signal and using the approximation as a separate input signal. 
\begin{equation} \label{Gs}
G_{(s)}=\frac{1}{(20s+1)(\beta s+1)(0.5 s +1)} 
\end{equation} 
\begin{equation} \label{Gcs}
G_{c(s)}=\frac{K(s^2+1.5 s+0.5)}{s}
\end{equation}
\begin{equation} \label{forwardChain}
G_{forward\;chain(s)}=\frac{K(s^2+1.5 s+0.5)}{s(20s+1)(\beta s+1)(0.5 s +1)} 
\end{equation}

Expanding the forward chain transfer function into polynomial fraction form \eqref{polyExpansion} sets up a simple transition to a state space \eqref{state} representation using a method valid for all strictly proper transfer functions in this form \eqref{A}\eqref{B}\eqref{C}\eqref{D}. This format can be validated by differentiating the equation for the fourth state variable and substituting the derivatives of the other state variables when they appear to arrive at a differential equation relating derivatives of the output to derivatives of the input--equivalent to a transfer function. But this derivation is space consuming and trivial to reproduce. 

\begin{equation} \label{polyExpansion}
\frac{(K)s^2+(1.5K) s^1+(0.5K)s^0}{(10\beta)s^4+(20.5\beta+10)s^3+(20.5+\beta)s^2+(1)s^1} 
\end{equation}
\begin{equation} \label{state}
\begin{matrix}
\dot{x}	= A x + B u\\
y=C x + D u 
\end{matrix} 
\end{equation}
\begin{equation} \label{A}
A=\begin{bmatrix}
0&0&0&0\\
1&0&0&-1/(10\beta)\\
0&1&0&-(20.5+\beta)/(10\beta)\\
0&0&1&-(20.5\beta+10)/(10\beta)
\end{bmatrix}
\end{equation}
\begin{equation} \label{B}
B=\begin{bmatrix}
0.5K/(10\beta)\\
1.5K/(10\beta)\\
K/(10\beta)\\
0
\end{bmatrix}
\end{equation}
\begin{equation} \label{C}
C=\begin{bmatrix}0&0&0&1\end{bmatrix}
\end{equation}
\begin{equation} \label{D}
D=\begin{bmatrix}0\end{bmatrix}
\end{equation}

Additionally, the plant transfer function can be realized independent of the controller using a similar strategy \eqref{A2}\eqref{B2}\eqref{C2}\eqref{D2}.
\begin{equation} \label{A2}
A=\begin{bmatrix}
0&0&-1/(10\beta)\\
1&0&-(20.5+\beta)/(10\beta)\\
0&1&-(20.5\beta+10)/(10\beta)
\end{bmatrix}
\end{equation}
\begin{equation} \label{B2}
B=\begin{bmatrix}
1/(10\beta)\\
0\\
0
\end{bmatrix}
\end{equation}
\begin{equation} \label{C2}
C=\begin{bmatrix}0&0&1\end{bmatrix}
\end{equation}
\begin{equation} \label{D2}
D=\begin{bmatrix}0\end{bmatrix}
\end{equation}

The equation for the closed loop transfer function can be found through application of Black's formula \eqref{feedback}.
\begin{equation} \label{feedback}
C_{l(s)}=\frac{K(s^2+1.5 s+0.5)}{s(20s+1)(\beta s+1)(0.5 s +1)+K(s^2+1.5 s+0.5)} 
\end{equation}

 The impulse response of these transfer functions could be determined by partial fraction expansion of the polynomial fractions and then term by term inverse Laplace transformation of the resulting simple fractions, but this routine functionality handled well by the Python Control Library and the SciPy signals library, so I have relied on these open source tools to make these plots. The open loop impulse response clearly displays negative real root behaviour as it sweeps up and decays exponentially with no oscillations, as can be seen in Fig.~\ref{PartAAltitudeResponse}. The addition of the controller in open loop causes an integration which offsets the final value of the impulse response in Fig.~\ref{PartAForwardChainAltitudeResponse}. The closed loop system of course varies with gain. Fig.~\ref{PartAFig1_impulseResponse} shows the wild oscillations of 280 gain.


\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartAAltitudeResponse.pdf}
      \caption{The altitude dynamic response displays no oscillatory behaviour since it has only real roots.}
      \label{PartAAltitudeResponse}
\end{figure}

\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartAForwardChainAltitudeResponse.pdf}
      \caption{The pole at the origin gives the forward chain a DC offset in its impulse response. Impulse response given with unity gain.}
      \label{PartAForwardChainAltitudeResponse}
\end{figure}

\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartAFig1_impulseResponse.pdf}
      \caption{It could be said that the impulse response of this closed loop system is tragic at 280 gain magnitude.}
      \label{PartAFig1_impulseResponse}
\end{figure}


\subsection{Effect of the Beta Parameter}
As a first step towards understanding the stability of the closed loop system we can run a brute force test using a polynomial root finder over a logarithmic range of gains. The results, shown in Fig.~\ref{partB_PosStabilityAt10Beta} show a region with positive pole real parts between gains of 0.485 and 136.553 gain units. This relationship varies with $\beta$, and the region shifts to $k\in[0.632,95.237]$ for $\beta=8$ and $k\in[0.403,178.474]$ for $\beta=12$, with the initial estimates from sampling refined to $\epsilon^{-7}$ precision using bisection. Thus if we know $\beta$ to be between 8 and 12 then the range of stable gains is $K\in(0,0.403)\cup(178.474,\infty)$, according to this simplified model which does not include the inevitable time delay which would likely introduce a finite gain limit.

\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/partB_PosStabilityAt10Beta.pdf}
      \caption{A look at the maximum real part pole of the closed loop transfer function with logarithmically varying gains shows a clear zone of instability where this pole crosses the $j\omega$ axis and then returns. This is shown for $\beta=10$, the nominal value. Only the positive spectrum of gains are relevant since the pole at the origin flees on the positive real axis for negative gains. More exact crossover points were found using bisection search. Crossover occurs at 0, 0.485, and 136.553 gain units.  }
      \label{partB_PosStabilityAt10Beta}
\end{figure}

The root locus plots Fig.~\ref{PartBPositiveLocusOf8BetaOpen} and Fig.~\ref{PartBPositiveLocusOf12BetaOpen} shed some light on the existence of this unstable region and the stability of extremely large gains. Since the two complex poles leave the real axis in the vicinity of the origin near one other pole they locally follow the three pole escape behaviour, but once they reach a sufficient distance from the origin they are in the vicinity of two other poles and two other zeros and thus approach the two independent pole behaviour and the associated infinite slope. The locus is bent back towards the stable half plane in the interim by proximity to the zero at negative one. Changing $\beta$ does not significantly impact the structure of the locus, but does move one of the poles, which impacts the extent to which the two complex locus venture out of the stable half plane.


\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartBPositiveLocusOf8BetaOpen.pdf}
      \caption{While largely the same, the third pole from the origin can be seen increasing in magnitude with lower values of $\beta$, causing a slight change in the locus of the complex poles as they arc out of and then return to the open left half-plane.}
      \label{PartBPositiveLocusOf8BetaOpen}
\end{figure}

\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartBPositiveLocusOf12BetaOpen.pdf}
      \caption{The root locus of the altitude control system is largely the same with variation in $\beta$, but provides a simple explanation for the two distinct ranges of stable gains.}
      \label{PartBPositiveLocusOf12BetaOpen}
\end{figure}

For stable LTI systems the controllability Grammian matrix can be solved algebraically using the Lyapunov-like equation \eqref{Lyapunov-like}
\begin{equation} \label{Lyapunov-like}
A L_c + L_c A^T = -B B^T
\end{equation}
Which can be solved by inversion of a rather large matrix, or using sparse techniques. I used the SLICOT Fortran-based linear controls library, which is wrapped in python and the built into the Python Controls Library to calculate the Grammian in this fashion when the symbolic matrix inversion proved unwieldy. I examined the controllability relationship in two parts, and as a function of two variables, since the relationship to gain was much more pronounced than the relationship to $\beta$. Fig.~\ref{PartBcontrollabilityLowGain} shows the controllability Grammian's smallest absolute value of any eigenvalue in the lower of the two stable gain regimes. In this regime the dependence on $\beta$ is minimal, but negative. The Grammian matrix is very close to numerically singular for most of this regime. Fig.~\ref{PartBcontrollabilityHighGain} shows the other regime, where the relationship to gain is less pronounced and the Grammian matrix is clearly non-singular. In this regime there is a small, but noticeable, relationship between $\beta$ and the controllability. This relationship is positive, whereas the relationship is negative in the low gain regime. However, a symbolic representation of why this relationship holds is rendered difficult by the size and complexity of the symbolic Gaussian elimination of the 10 by 11 augmented matrix which calculates the individual elements of the Grammian.

\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartBcontrollabilityLowGain.pdf}
      \caption{The closed loop system admits two ranges of stable gains, and in the very low range the dependence of the least eigenvalue of the controllability grammian on $\beta$ is much less significant than the dependence on gain. However, the relationship between controllability and $\beta$ is negative, while for high gains it is positive. }
      \label{PartBcontrollabilityLowGain}
\end{figure}

\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartBcontrollabilityHighGain.pdf}
      \caption{The relationship between gain and controllability levels off in the higher register of gains, and the dependence on $\beta$ becomes more clear.}
      \label{PartBcontrollabilityHighGain}
\end{figure}



\subsection{Step Response of the Closed Loop System}
Fig.~\ref{PartCStepResponse} shows the step response of the closed loop system with $K=280$, a low value in the upper register of stable gains. The two resonant poles are dominant at this gain, and the result strongly resembles a second order system with a low damping ratio. The settling time was numerically evaluated using a sampling at twenty times the Nyquist frequency so as to have a high probability of containing a point in the last excursion of the function over the $2\%$ threshold. These samples were checked backwards, and bisection search was used to isolate the settling time once the first sample was found to have left the threshold. A more advanced algorithm might first search for local minima and maxima by finding the zeros of the impulse response using bisection, and then use this set of extrema times in place of samples to guarantee finding the first time the function leaves the $2\%$ threshold. After much oscillation the closed loop settles at 43.90 time units.

In a similar fashion the overshoot percentage was found using a sampling to isolate the absolute maximum and the sample before and after it, that is to isolate the bounds for a bisection search of the impulse response, the derivative of the step response, yielding the time of maximum step response. The step response's maximum value is then evaluated and division by the steady state step transfer function yields the overshoot ratio of 92.14\% an almost certainly unacceptable value for an altitude controller. 

\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartCStepResponse.pdf}
      \caption{The step response illustrates the highly oscillatory step response of the system. Sampling at twice the nyquist frequency followed by bisection showed the settling time to be $43.90$, and a similar method found the percent overshoot to be $92.14\%$, an impressively ill-advised feature. }
      \label{PartCStepResponse}
\end{figure}

\subsection{Rejection of Step Control Signal Disturbance}
The transfer function with respect to a disturbance has the same stability properties and loop transfer function as the closed loop system, but treats the controller as a feedback transfer function rather than part of the forward chain. For this reason the addition of a pre-filter in part $e$ will not change this property. The transfer function can be obtained by completing Black's formula with $G_{c(s)}$ in the feedback path rather than in the forward function resulting in the same poles \eqref{disturbance poles}, but different zeros \eqref{disturbance}. Fig.~\ref{partD_StepResponse} shows the step response of this system, which settles to zero even under step input due to the zero at the origin introduced by $G_{c(s)}$ in the feedback path. If the system were excited by a ramp input, it would settle to $\frac{2}{K}$.

\begin{equation} \label{disturbance poles}
p_{(s)}=s(20s+1)(\beta s+1)(0.5 s +1)+K(s^2+1.5 s+0.5) 
\end{equation}
\begin{equation} \label{disturbance}
CL_{disturbance(s)}=\frac{s}{p_{(s)}} 
\end{equation}
\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/partD_StepResponse.pdf}
      \caption{The step disturbance response of the closed loop system oscillates at the same frequency as the closed loop impulse response, since the two share the same poles, but the transfer function of the disturbance system has a zero at the origin, which causes its steady state step response error to approach zero. It deviates from the impulse response of the closed loop system only in the lack of the zeros introduced by the controller.}
      \label{partD_StepResponse}
\end{figure}
\subsection{Inclusion of a Naive Pre-Filter}
By including a new transfer function \eqref{pre}, which is the reciprocal of the controller's unity gain transfer function, we will give the new closed loop system the same transfer function after two pole zero cancellations as the previously discussed step response to disturbances: \eqref{preCancel}. As can be seen in Fig.~\ref{PartE-AFig1_impulseResponse} this impulse response is a scaled version of the previously mentioned disturbance step response. 

\begin{equation} \label{pre}
G_{p(s)}=\frac{0.5}{s^2+1.5s+0.5} 
\end{equation}

\begin{equation} \label{preCancel}
CL_{p(s)}=\frac{0.5K}{p_{(s)}} 
\end{equation}

\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartE-AFig1_impulseResponse.pdf}
      \caption{The addition of a pre-filter clearly improves the impulse response, lowering the magnitude of the spikes by just over a factor of three. This impulse response signal is simply a scaling of the step disturbance response signal since the pre-filter cancels the zeros of the controller transfer function.}
      \label{PartE-AFig1_impulseResponse}
\end{figure}
\subsubsection{Loss of Controllability}
Of course, this system will not be fully controllable since the two zeros introduced into the closed loop system by the controller directly overlap the poles of the pre-filter. The same computational approach to finding the relationship between $\beta$ and the controllability Grammian as used in subsection B is shown in Fig.~\ref{PartE-BcontrollabilityHighGain} illustrating the lack of controllability to numerical precision--as expected from a pole zero cancellation.

\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartE-BcontrollabilityHighGain.pdf}
      \caption{More a numerical conditioning error than a relationship, the high gain controllability of the system with the pre-filter is absent within numerical precision on matrix operations of this complexity. This is explained by the cancellation between the poles of the pre-filter and the identical zeros introduced by the PID controller. The low gain plot is functionally equivalent.}
      \label{PartE-BcontrollabilityHighGain}
\end{figure}

\subsubsection{Step Response Performance Improvements}
Fig.~\ref{PartE-CStepResponse} shows that the prefilter has not effected the DC gain of the transfer function by preserving the $\frac{K}{2}$ value of the numerator at $s=0$. It also highlights the importance of zeros on the step response, since the lack of the zeros introduced by the roots of the controller are the only difference between this response and that in Fig.~\ref{PartCStepResponse}. The overshoot is only $7.08\%$ as opposed to the earlier $92.14\%$, and the settling time is $25.84$--down from $43.90$ in the initial design.
%$43.90$, and a similar method found the percent overshoot to be $92.14\%$
\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartE-CStepResponse.pdf}
      \caption{The effect of the pre-filter on the step response is impressive, bringing the settling time to $25.84$, with percent overshoot of only $7.08\%$. }
      \label{PartE-CStepResponse}
\end{figure}
\subsubsection{Lack of Effect on Control Signal Disturbances}
Since the pre-filter effects neither the forward path between the disturbance and the output nor the feedback path from the disturbance summing node to the controller's output the result of subsection D does not change with the addition of the pre-filter.


\setcounter{equation}{0}
\section*{Forward Velocity}
The forward velocity transfer function for the aircraft can be modelled as \eqref{Gs2}, and is has a simple PI controller of the form \eqref{Gcs2}, giving a closed loop transfer function \eqref{CLs2}. 
\begin{equation} \label{Gs2}
G_{(s)}=\frac{10}{s^2+4.5s+9} 
\end{equation}

\begin{equation} \label{Gcs2}
G_{c(s)}=\frac{K+Ks}{s} 
\end{equation}

\begin{equation} \label{CLs2}
CL_{(s)}=\frac{10Ks+10K}{s^3+4.5s^2+(9+10K)s+10K} 
\end{equation}

\subsection{Damping Ratio Choice Problem}
In order to find a gain which produces any desired damping ratio within the plausible range I will perform polynomial long division to divide a quadratic with the desired damping ratio out of the poles, and set as zero the expressions for the remainders.
\begin{equation} \label{CL2Poles}
CL_{(s)}=s^3+4.5s^2+(9+10K)s+10K
\end{equation}

\begin{equation} \label{QuadraticWithPoles}
p_{(s)}=s^2+2\zeta\omega_0 s+\omega_0^2
\end{equation}
\begin{equation} \label{rem1}
R(\frac{CL_{(s)}}{s p_{(s)}})=(4.5-2\zeta\omega_0) s^2+(9+10K-\omega_0^2)s+10K
\end{equation}
\begin{equation} \label{rem2}
q=4.5-2\zeta\omega_0
\end{equation}
\begin{equation} \label{rem3}
R(\frac{CL_{(s)}}{(s+q)p_{(s)}})=(9+10K-\omega_0^2-q 2\zeta\omega_0)s+10K-q \omega_0^2
\end{equation}
And since we want the remainder \eqref{rem3} to equal zero for all $s$ we set each coefficient of the polynomial to zero in \eqref{rem4}
\begin{equation} \label{rem4}
0=(9+10K-\omega_0^2-q 2\zeta\omega_0)\;\;\;0=10K-q \omega_0^2
\end{equation}
From here we have the following system of three unknowns

\begin{equation} \label{rem5}
\begin{split}
\\
0&=-\omega_0^2-q 2\zeta\omega_0 +9+10K\\
q \omega_0^2&=10K
\end{split}
\end{equation}
Which simplifies to a third order polynomial
\begin{equation} \label{rem6}
\begin{split}
0&=-\omega_0^2-q 2\zeta\omega_0 +9+q \omega_0^2\\
0&=-\omega_0^2-(4.5-2\zeta\omega_0) 2\zeta\omega_0 +9+(4.5-2\zeta\omega_0) \omega_0^2\\
0&=-2\zeta\omega_0^3+(3.5-4\zeta^2)\omega_0^2-9\zeta\omega_0 +9
\end{split}
\end{equation}
Where the real solution to this cubic is the solution of interest. If this solution is not unique then the formula predicts multiple valid gains, but by checking the cubic discriminant quickly it appears that this will not occur for $\zeta$ on the unit range. Once that is determined from a numeric polynomial root finder we can calculate the gain, and in the process we can also find the other pole. 
\begin{equation} \label{rem7}
\begin{split}
q&=4.5-2\zeta\omega_0\\
K&=0.1 q \omega_0^2
\end{split}
\end{equation}
Execution of this process identified the gain 0.437 as the solution to a desired damping ratio $\zeta=0.6$, as can be seen in the root locus, Fig.~\ref{PartF_PositiveLocusWithZeta600}, to result in a system dominated by a real pole near the origin.

\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartF_PositiveLocusWithZeta600.pdf}
      \caption{The root locus of the forward velocity system, highlighting those poles which produce the desired damping ratio of 0.6, as approximated by a numerical computation based on symbolic polynomial long division, algebraic re-arrangement into a single variable third order polynomial, and numerical root finding.}
      \label{PartF_PositiveLocusWithZeta600}
\end{figure}
\subsection{Performance of the Closed Loop Forward Velocity System}
With this conservative gain the closed loop step response, shown in Fig.~\ref{PartGStepResponse438}, has no overshoot and settles slowly (9.686 time units) in a largely first order manner. There is no steady-state error.
\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartGStepResponse438.pdf}
      \caption{The step response of the damping ratio 0.6 closed loop system, using a gain of approximately 0.438 shows no overshoot. The system settling time is 9.686 using the 2\% criteria. }
      \label{PartGStepResponse438}
\end{figure}
\subsection{A More Aggressive Damping Ratio Solution}
Setting $\zeta=\sqrt{2}-1$ produces the more performance oriented pole locations shown in Fig.~\ref{PartGStepResponse1472} using an identical procedure as before. As gain increases, the real pole moves away from the origin and dominates the response to a lesser extent, while the complex pole become less damped and higher frequency. For extreme gains the complex poles will again be dominated by the real pole as they move to higher and higher frequencies. The damping envelope of these high frequency poles will have virtually the same time constant, but they will be attenuated by the low frequency pole.
\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartF_PositiveLocusWithZeta415.pdf}
      \caption{Using a slightly adjusted desired damping ratio, the new closed loop system is dominated to a lesser extent by the real pole.}
      \label{PartF_PositiveLocusWithZeta415}
\end{figure}
\subsubsection{Improved Closed Loop Performance}
As can be seen from Fig.~\ref{PartGStepResponse1472}, these new pole locations have a much faster step response of 4.016 time units. There is still no overshoot to numerical precision.
\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartGStepResponse1472.pdf}
      \caption{Using the larger gain required by the damping ratio of $\sqrt(2)-1$ still produces no overshoot in the step response. The 2\% criteria settling time is much faster, at 4.016 }
      \label{PartGStepResponse1472}
\end{figure}


\subsection{Consideration of a Full PID Approach}
By adding a full PID compensator to the forward path the system will gain a pole relative to the PI compensator, and the root locus behaviour will change to our benefit. Fig.~\ref{PartI_PositiveLocus} shows how the complex poles will approach these two compensator zeros rather than becoming arbitrarily high frequency. The step response of the initial controller suffers from overshoot at higher gains, as can be seen in Figs.~\ref{PartGStepResponse7404}\&\ref{PartF_PositiveLocusWithZeta200}, whereas for arbitrarily high gains the PID compensated closed loop system can have have far faster settling times without overshoot, as seen in Fig.~\ref{PartGStepResponse6000}. In terms of plant model uncertainties, both controllers will react gracefully to modified pole locations and plant gain, and will both be able to handle step disturbances in the control output due to their integration terms. For the same reason they will both have no steady state error. As can be seen from their respective step responses, the PID controller is capable of faster settling times overall, and its complex poles approach low frequency, high damping placements as the gain increases, which limits the total possible percent overshoot. So I would suggest the PID controller for any desired settling time faster than 4 time units.


\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartI_PositiveLocus.pdf}
      \caption{The PID compensator keeps the oscillation frequency of the complex poles in check as the gain is increased, and allows the real pole to become arbitrarily fast, ignoring the inevitable time delays. }
      \label{PartI_PositiveLocus}
\end{figure}



\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartGStepResponse7404.pdf}
      \caption{The step response suffers in the original controller at high gain values. At the gain pictured the settling time is 2.27, but the overshoot is 43.52\%}
      \label{PartGStepResponse7404}
\end{figure}

\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartF_PositiveLocusWithZeta200.pdf}
      \caption{The original controller gets very high frequency, lightly damped complex poles as the gain increases. This is a gain of 7.4}
      \label{PartF_PositiveLocusWithZeta200}
\end{figure}

\begin{figure}[thpb]
      \centering
      \includegraphics[width=3.25in]{../data/PartGStepResponse6000.pdf}
      \caption{Using a relatively low gain with the PID controller. The 2\% criteria settling time continues to decrease as gain is increased, but the computational task of accurately plotting is much easier at low gains. The settling time of 1.43 is highlighted here. }
      \label{PartGStepResponse6000}
\end{figure}












%\addtolength{\textheight}{-12cm}  


%\begin{thebibliography}{99}

%\bibitem{c1} G. O. Young, “Synthetic structure of industrial plastics (Book style with paper title and editor),” 	in Plastics, 2nd ed. vol. 3, J. Peters, Ed.  New York: McGraw-Hill, 1964, pp. 15–64.

%\end{thebibliography}




\end{document}
