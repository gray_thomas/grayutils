'''
Created on May 16, 2014

@author: hcrl
'''
import numpy as np
import math


def randomOrthogonalMatrix(vectorSize, numberOfVectors):
    matrix = np.ones((vectorSize, numberOfVectors))
    for i in range(0, numberOfVectors):
        vec1 = np.random.rand(vectorSize)
        for j in range(0, i):
            vec0 = matrix[:, j]
            vec1 -= vec0 * vec0.dot(vec1)
        vec1 *= 1.0 / math.sqrt((vec1.dot(vec1)))
        matrix[:, i] = vec1
    return matrix

def gramSchmidt(matrix):
    newMatrix = np.empty(matrix.shape)
    newMatrixCols = 0
    for i in range(0, matrix.shape[1]):
        vec1 = matrix[:, i]
        for j in range(0, newMatrixCols):
            vec0 = newMatrix[:, j]
            vec1 -= vec0 * vec0.dot(vec1)
        if (vec1.dot(vec1) > 1e-7):
            vec1 *= 1.0 / math.sqrt((vec1.dot(vec1)))
            newMatrix[:, newMatrixCols] = vec1
            newMatrixCols += 1
        else:
            vec1 *= 0
    return newMatrix[:, 0:newMatrixCols]

def intersection(matrixA, matrixB):
    
    
    return matrixA
    

def largestAbsElement(matrix):
    return np.max([abs(np.max(matrix)), abs(np.min(matrix))])
    
def randomNonSingularDiagonalMatrix(maxSV, minSV, rank):
    matrix = np.empty((rank, rank))
    matrix *= 0
    for i in range(0, rank):
        matrix[i, i] = math.exp(np.random.random() * math.log(maxSV / minSV)) * minSV * np.sign(np.random.random() - 0.5)
    return matrix

class DecomposedMapping(object):
    def __init__(self, rangeSize, rank, domainSize):
        self.domainSize = domainSize
        self.rank = rank
        self.rangeSize = rangeSize
        self.domainBasis = np.ones((domainSize, rank))
        self.domainBasis[0:rank, :] = np.identity(rank)
        self.domainBasis[rank:domainSize, :] *= 0
        self.rangeBasis = np.ones((rangeSize, rank))
        self.rangeBasis[0:rank, :] = np.identity(rank)
        self.rangeBasis[rank:rangeSize] *= 0
        self.singularValues = np.identity(rank)
    def randomize(self):
        self.domainBasis = randomOrthogonalMatrix(self.domainSize, self.rank)
        self.rangeBasis = randomOrthogonalMatrix(self.rangeSize, self.rank)
        self.singularValues = randomNonSingularDiagonalMatrix(100, 0.01, self.rank)
    def getRangeBasis(self):
        return self.rangeBasis
    def getDomainBasis(self):
        return self.domainBasis
    def getSingularValues(self):
        return self.singularValues
    def getRangeProjector(self):
        return self.rangeBasis.dot(self.rangeBasis.T)
    def getDomainProjector(self):
        return self.domainBasis.dot(self.domainBasis.T)
    def printme(self):
        print "Decomposed Mapping: domainBasis, singularValues, rangeBasis ="
        print self.domainBasis
        print self.singularValues
        print self.rangeBasis

class SimpleLinearRobot(object):
    def __init__(self, dof):
        self.actuationMatrix = np.identity(dof)
        self.massMatrix = np.identity(dof)
        self.jointSpaceSize = dof
        self.gravityGeneralizedTorque = np.ones((dof, 1))
        self.coriolisGeneralizedTorque = np.ones((dof, 1))
        self.generalizedJoints = np.ones((dof, 1))
        self.generalizedJointVelocities = np.ones((dof, 1))
        
        
    def setMassMatrix(self, massMatrix):
        if (massMatrix.shape[0] != self.jointSpaceSize):
            print "mass matrix is wrong size"
        self.massMatrix = np.array(massMatrix)
    def setActuationMatrix(self, actuationMatrix):
        self.actuationMatrix = np.array(actuationMatrix)
    def setGravityGeneralizedTorque(self, value):
        self.gravityGeneralizedTorque = np.array(value)
    def setCoriolisGeneralizedTorque(self, value):
        self.coriolisGeneralizedTorque = np.array(value)
    def setGeneralizedJoints(self, value):
        self.generalizedJoints = np.array(value)
    def setGeneralizedJointVelocity(self, value):
        self.generalizedJointVelocities = np.array(value)
    def getMassMatrix(self):
        return self.massMatrix
    def getActuationMatrix(self):
        return self.actuationMatrix()
    def getJointSpaceSize(self):
        return self.jointSpaceSize

def rightPseudoInverse(matrix):
    return matrix.T.dot((matrix.dot(matrix.T)).inverse())
def weightedRightPseudoInverse(matrix, weight):
    pass

def wbc(massMatrix, coriolisVector, qDotVector, taskList):
    pass

def tsid(jacobian, massMatrix, coriolisVector, qDotVector):
    pass

def uf(jacobian, massMatrix, coriolisVector, qDotVector, Vmatrix):
    pass

def checkSolution(torqueVector, massMatrix, coriolisVector, qDDotVector):
    assert abs(massMatrix.dot(qDDotVector) + coriolisVector - torqueVector) < 1e-10
    
def getRandomPositiveDefiniteMatrix(size, condition):
    randomMatrixZeroOne = np.random.rand(size, size);
    randomSymmetricMatrix = randomMatrixZeroOne.T.dot(randomMatrixZeroOne)
    vals = np.linalg.eigvals(randomSymmetricMatrix)
    sortedVals = np.sort(vals)
    largeEig = sortedVals[-1]
    smallEig = sortedVals[0]
    # (a+c)/(b+c)=d -> a+c = db+dc-> a-db=-c+dc -> c= (a-db)/(d-1)
    phaseShift = (largeEig - smallEig * condition) / (condition - 1)
    ret = randomSymmetricMatrix + np.identity(size, "double") * phaseShift
    return ret

def generateFakeRobot(dof, massMatrixConditionNumber, maxCoriolis, maxQDot):
    robot = SimpleLinearRobot(dof)
    robot.setMassMatrix(getRandomPositiveDefiniteMatrix(dof, massMatrixConditionNumber))
    robot.setGeneralizedJointVelocity((np.random.rand(dof) * 2 - np.ones(dof)) * maxQDot)
    robot.setGeneralizedJoints((np.random.rand(dof) * 2 - np.ones(dof)) * np.pi)
    robot.setCoriolisGeneralizedTorque((np.random.rand(dof) * 2 - np.ones(dof)) * maxCoriolis)
    robot.setGravityGeneralizedTorque((np.random.rand(dof) * 2 - np.ones(dof)) * maxCoriolis)
    return robot

def generateLowDimensionalMap(subspaceDimension, spacialDimension, scale):
    # J qDot = xDot
    return (np.random.randn(subspaceDimension, spacialDimension) - np.ones(subspaceDimension, spacialDimension)) * scale
def generateFakeTask(robot, taskDim, maxqDDot):
    jacobian = generateLowDimensionalMap(taskDim, dof, 1)
    jacobianDot = generateLowDimensionalMap(taskDim, dof, 1)
    qDDotRequired = (np.random.rand(dof) * 2 - np.ones(dof)) * maxqDDot

def main():
    print "Hello World"
    mapping = DecomposedMapping(3, 2, 3)
    mapping.randomize()
    mapping.printme()
    robot = generateFakeRobot(4, 20, 10, 5)
    aRank = 4
    bRank = 4
    cRank = 6
    # The following finds a basis for the range overlap of two jacobians
    Ra = randomOrthogonalMatrix(cRank, aRank)
    Rb = randomOrthogonalMatrix(cRank, bRank)
    print "Ra:"
    print Ra
    assert largestAbsElement(np.identity(aRank) - (Ra.T).dot(Ra)) < 1e-14
    print "Rb:"
    print Rb
    assert largestAbsElement(np.identity(bRank) - (Rb.T).dot(Rb)) < 1e-14
    print Ra.dot(Ra.T)
    print Rb.dot(Rb.T)
    projRa = Ra.dot(Ra.T)
    projRb = Rb.dot(Rb.T)
    assert largestAbsElement(projRa - projRa.dot(projRa)) < 1e-14
    assert largestAbsElement(projRb - projRb.dot(projRb)) < 1e-14
    print "preG=np.concatenate((np.identity(cRank)-projRa,np.identity(cRank)-projRb),axis=1):"
#     preG = np.concatenate((np.identity(cRank)-projRa, projRb), axis=1)
#     print preG
#     print "G=gramSchmidt(preG):"
#     G = gramSchmidt(preG)
#     gRank=G.shape[1]
#     print gramSchmidt(preG)
    
    (w,vl)=np.linalg.eig(projRa.dot(projRb))
    print "np.linalg.eig(projRa.dot(projRb))"
    print "w:"
    print w  
    print "vl:"
    print vl
    G=np.empty((cRank,cRank))
    gRank=0
    for i in range(0,w.shape[0]):
        if abs(w[i]-1)<1e-7:
            print "vl[:,",i,"]",  vl[:,i]
            G[:,gRank]=vl[:,i]
            gRank+=1
    G=G[:,0:gRank]
    G=gramSchmidt(G)
    print "G:"
    print G
    print "G-(projRa.dot(projRb)).dot(G)"
    print G-(projRa.dot(projRb)).dot(G)
    
    print "(G.T).dot(G)"
    print (G.T).dot(G)
    
    print largestAbsElement(np.identity(gRank) - (G.T).dot(G))
    assert largestAbsElement(np.identity(gRank) - (G.T).dot(G)) < 1e-14
    assert largestAbsElement(np.identity(aRank) - (Ra.T).dot(Ra)) < 1e-14
    print "Ua=(Ra.T).dot(G)"
    Ua = (Ra.T).dot(G)
    print Ua
    print "Ub=(Rb.T).dot(G)"
    Ub = (Rb.T).dot(G)
    print Ub
    print "Ra.dot(Ua)"
    print Ra.dot(Ua)
    print "gramSchmidt(np.concatenate((projRa,I-projRa),axis=1))"
    print gramSchmidt(np.concatenate((np.identity(6) - projRb, np.identity(6) - projRa), axis=1))
    assert largestAbsElement(G - (Ra.dot(Ra.T)).dot(G)) < 1e-14
    assert largestAbsElement((Ra.T).dot(G - Ra.dot(Ua))) < 1e-14
    
    print "G - Ra.dot(Ua)"
    print G - Ra.dot(Ua)
    assert largestAbsElement(G - Ra.dot(Ua)) < 1e-14
    print "np.identity(gRank) - (Ua.T).dot(Ua)"
    print np.identity(gRank) - (Ua.T).dot(Ua)
    assert largestAbsElement(np.identity(gRank) - (Ua.T).dot(Ua)) < 1e-14
    
    assert largestAbsElement(Rb - (Rb.dot(Rb.T)).dot(Rb)) < 1e-14
    assert largestAbsElement(G - (Rb.dot(Rb.T)).dot(G)) < 1e-14
    randU = randomOrthogonalMatrix(6, 10).dot(randomNonSingularDiagonalMatrix(100, 0.01, 10).dot(randomOrthogonalMatrix(10, 2)))
    print "gramSchmidt(np.ones((3, 3))"
    print gramSchmidt(np.ones((3, 3)))
#     generateFakeTask(robot, 2, 4)
#     (massMatrix, coriolisVector, qDotVecotr ) = robot
#     print massMatrix, coriolisVector,qDotVecotr

if __name__ == '__main__':
    main()
