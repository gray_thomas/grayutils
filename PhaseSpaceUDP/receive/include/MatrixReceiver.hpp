#ifndef MATRIX_RECEIVER_HPP
#define MATRIX_RECEIVER_HPP
#include "Eigen/Dense"
#include <vector>
#include "PhaseSpaceUDPReceiver.hpp"
#include "MatrixListener.hpp"
using namespace Eigen;

namespace gu
{
namespace humeEstimation
{

template <unsigned int N>
class MatrixReceiver : public PhaseSpaceUDPReceiver
{
	Matrix<double, 3, N> lastLEDs;
	Matrix<bool, 1, N> lastValid;
	std::vector<MatrixListener<N>*> listeners;

public:
	MatrixReceiver();
	virtual ~MatrixReceiver(){}
	void add_listener(MatrixListener<N> *listener);
	virtual void handleUpdate();
};

}
}
#endif
