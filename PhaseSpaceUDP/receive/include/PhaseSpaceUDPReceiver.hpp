/*
 * PhaseSpaceUDPReceiver.hpp
 *
 *    Created on: Apr 7, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef PHASESPACEUDPRECEIVER_HPP_
#define PHASESPACEUDPRECEIVER_HPP_
#include "comm_udp.h"
#include "Daemon.hpp"
#include <vector>

#include <fstream>
#include <iostream>

#include <string>
#include <string.h>

#include <sys/socket.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <time.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
class PhaseSpaceUDPReceiver: public gu::threads::Daemon
{
protected:
	struct sockaddr_in mySocketAddress, remoteSocketAddress;
	int count;
	int receivedDataSize;
	int socketInteger;
	int socketAddressStructureLength;
	int loopIndex;
protected:
	message mostRecentMessage;
	virtual void handleUpdate();
	void loop();
public:
	virtual ~PhaseSpaceUDPReceiver()
	{
	}
	message& getMessage()
	{
		return mostRecentMessage;
	}
};

#endif /* PHASESPACEUDPRECEIVER_HPP_ */
