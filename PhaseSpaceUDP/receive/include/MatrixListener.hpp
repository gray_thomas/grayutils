#ifndef MATRIX_LISTENER_HPP
#define MATRIX_LISTENER_HPP
#include "Eigen/Dense"
using namespace Eigen;

namespace gu
{
namespace humeEstimation
{

template <unsigned int N>
class MatrixListener
{
public:
	virtual ~MatrixListener(){}
	virtual void handleUpdate(const Matrix<double, 3, N> &leds, const Matrix<bool, 1, N> &detected)=0;
};
}
}
#endif
