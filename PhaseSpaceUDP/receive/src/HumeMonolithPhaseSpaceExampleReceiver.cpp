/*
 * exampleReceiver.cpp
 *
 *    Created on: Apr 6, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "comm_udp.h"
#include "Daemon.hpp"
#include "PhaseSpaceUDPReceiver.hpp"
#include <vector>

#include <fstream>
#include <iostream>

#include <string>
#include <string.h>

#include <sys/socket.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <time.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
using namespace std;


class WritingPhaseSpaceUDPReceiver: public PhaseSpaceUDPReceiver
{
protected:
	FILE* myfile;
public:
	WritingPhaseSpaceUDPReceiver(const char* filename)
	{
		myfile =fopen(filename, "w");
	}
	~WritingPhaseSpaceUDPReceiver()
	{
		fclose(myfile);
	}
void handleUpdate()
	{
		Int32BitField* bitfield = (Int32BitField*) &mostRecentMessage.validBits;
		message* msg = &mostRecentMessage;
		count++;
		fprintf(myfile, "{%d,%d,%d,%d} (%f %f %f)(%f %f %f)(%f %f %f)(%f %f %f)\n",
					bitfield->b3, bitfield->b4, bitfield->b5, bitfield->b6,
					msg->x[3], msg->y[3], msg->z[3],
					msg->x[4], msg->y[4], msg->z[4], 
					msg->x[5], msg->y[5], msg->z[5], 
					msg->x[6], msg->y[6], msg->z[6]
					);
		if (count % 100 == 0)
			fprintf(stderr,
					"{%d,%d,%d,%d} (%f %f %f)(%f %f %f)(%f %f %f)(%f %f %f)\n",
					bitfield->b3, bitfield->b4, bitfield->b5, bitfield->b6,
					msg->x[3], msg->y[3], msg->z[3],
					msg->x[4], msg->y[4], msg->z[4], 
					msg->x[5], msg->y[5], msg->z[5], 
					msg->x[6], msg->y[6], msg->z[6]
					);

	}
};

class HowdyDaemon: public gu::threads::Daemon
{
	string name;
public:
	HowdyDaemon(string name) :
				Daemon(),
				name(name)
	{

	}
	virtual void loop(void)
	{

		printf("Thread %s says Howdy!\n", name.c_str());

		sleep(1); // just for keeping time, we already check for signals.

	}
};

int main(void)
{
	WritingPhaseSpaceUDPReceiver receiver("test.data");
	receiver.start();
	// HowdyDaemon fred("Fred");
	// fred.start();
	// HowdyDaemon mark("Mark");
	// mark.start();
	// Wait for SIGINT to arrive.
	pause();

	// Done.
	puts("Terminated.");
	return EXIT_SUCCESS;
}
