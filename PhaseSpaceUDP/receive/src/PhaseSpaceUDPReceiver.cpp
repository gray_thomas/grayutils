/*
 * PhaseSpaceUDPReceiver.cpp
 *
 *    Created on: Apr 7, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */


#include "PhaseSpaceUDPReceiver.hpp"

void PhaseSpaceUDPReceiver::handleUpdate()
	{
		Int32BitField* bitfield = (Int32BitField*) &mostRecentMessage.validBits;
		message* msg = &mostRecentMessage;
		count++;
		if (count % 100 == 0)
			fprintf(stderr,
					"Received Data: {%d,%d,%d, %d,%d,%d,%d} \n\t(%f %f %f)(%f %f %f)(%f %f %f)\n\t(%f %f %f)(%f %f %f)(%f %f %f)(%f %f %f)\n",
					bitfield->b0, bitfield->b1, bitfield->b2, bitfield->b3, bitfield->b4, bitfield->b5, bitfield->b6,
					msg->x[0], msg->y[0], msg->z[0], msg->x[1], msg->y[1], msg->z[1], msg->x[2], msg->y[2],
					msg->z[2], msg->x[3], msg->y[3], msg->z[3], msg->x[4], msg->y[4], msg->z[4], msg->x[5],
					msg->y[5], msg->z[5], msg->x[6], msg->y[6], msg->z[6]);

	}

void PhaseSpaceUDPReceiver::loop()
	{
		if (isFirstLoop())
		{
			socketAddressStructureLength = sizeof(remoteSocketAddress);
			if ((socketInteger = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
			{
				printf("failed to create socket\n");
				return;
			}
			fprintf(stderr,"socketInteger is %d\n",socketInteger);

			memset((char *) &mySocketAddress, 0, sizeof(mySocketAddress));
			mySocketAddress.sin_family = AF_INET;

			mySocketAddress.sin_port = htons(POS_PORT);
			mySocketAddress.sin_addr.s_addr = htonl(INADDR_ANY);
			fprintf(stdout, "mySocketAddress.sin_port %d\n", mySocketAddress.sin_port);
			int bindresult=bind(socketInteger, (const sockaddr*) &mySocketAddress, (socklen_t) sizeof(mySocketAddress));
			fprintf(stderr,"bindresult is %d\n",bindresult);
			loopIndex = 0;
		}
		int flags = 0;
		receivedDataSize = recvfrom(socketInteger, (void*) &mostRecentMessage, sizeof(message), flags,
				(sockaddr*) &remoteSocketAddress, (socklen_t*) &socketAddressStructureLength);

		// if (mostRecentMessage.index != (loopIndex + 1))
		// 	fprintf(stderr, "Missing %d packets (%08x->%08x)\n", mostRecentMessage.index - loopIndex,
		// 			mostRecentMessage.index, loopIndex);

		loopIndex = mostRecentMessage.index;
		handleUpdate();

	}
