#include "MatrixReceiver.hpp"
#include "PhaseSpaceUDPReceiver.hpp"
#include "comm_udp.h"

namespace gu
{
namespace humeEstimation
{
template<unsigned int N>
MatrixReceiver<N>::MatrixReceiver() :
			lastLEDs(),
			lastValid(),
			listeners()
{
}

template<unsigned int N>
void MatrixReceiver<N>::add_listener(MatrixListener<N> *listener)
{
	listeners.push_back(listener);
}

// forgot I would have to specialize this. Fix this if you ever increase the number of LEDs.
template<>
void MatrixReceiver<7>::handleUpdate()
{
	Int32BitField* bitfield = (Int32BitField*) &mostRecentMessage.validBits;
	message* msg = &mostRecentMessage;
	lastValid << bitfield->b0, bitfield->b1, bitfield->b2, bitfield->b3, bitfield->b4, bitfield->b5, bitfield->b6;
	lastLEDs << msg->x[0], msg->x[1], msg->x[2], msg->x[3], msg->x[4], msg->x[5], msg->x[6], msg->y[0], msg->y[1], msg->y[2], msg->y[3], msg->y[4], msg->y[5], msg->y[6], msg->z[0], msg->z[1], msg->z[2], msg->z[3], msg->z[4], msg->z[5], msg->z[6];
	for (auto it = listeners.begin(); it != listeners.end(); ++it)
	{
		(*it)->handleUpdate(lastLEDs, lastValid);
	}
}
// Instantiate the templated class for size seven lists of LED positions (the current setup)
template class MatrixReceiver<7> ;
}
}
