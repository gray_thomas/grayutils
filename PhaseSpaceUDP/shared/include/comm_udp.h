#ifndef COMMON_UDP_H
#define COMMON_UDP_H
#ifdef __cplusplus
extern "C"
{
#endif

#define CMD_CALIBRATE				0x01000000
#define CMD_SET_TORQUE				0x02000000
#define CMD_SET_MODE				0x03000000
#define CMD_SET_MODE_RAW_TORQUE		0x03000001
#define CMD_SET_MODE_COMP_TORQUE	0x03000002

#define CMD_DATA_LEN 29

#define NOTIFY_ADDR             "192.168.1.6"
#define NOTIFY_PORT             51123
#define CMD_PORT                51124
#define STT_PORT                51125
#define POS_PORT                51128

#ifndef __cplusplus
typedef struct
{
	pthread_mutex_t *mutex;
	void *win;
}udp_arg;
#endif

//typedef struct
//{
//	int			index;
//	int			count;
//	long long	timeStamp;
//	double		data[29];
//} message;
#include <stdint.h>


typedef struct
{
	uint32_t b31 :1;
	uint32_t b30 :1;
	uint32_t b29 :1;
	uint32_t b28 :1;
	uint32_t b27 :1;
	uint32_t b26 :1;
	uint32_t b25 :1;
	uint32_t b24 :1;
	uint32_t b23 :1;
	uint32_t b22 :1;
	uint32_t b21 :1;
	uint32_t b20 :1;
	uint32_t b19 :1;
	uint32_t b18 :1;
	uint32_t b17 :1;
	uint32_t b16 :1;
	uint32_t b15 :1;
	uint32_t b14 :1;
	uint32_t b13 :1;
	uint32_t b12 :1;
	uint32_t b11 :1;
	uint32_t b10 :1;
	uint32_t b9 :1;
	uint32_t b8 :1;
	uint32_t b7 :1;
	uint32_t b6 :1;
	uint32_t b5 :1;
	uint32_t b4 :1;
	uint32_t b3 :1;
	uint32_t b2 :1;
	uint32_t b1 :1;
	uint32_t b0 :1;
} Int32BitField;

typedef struct
{
	int32_t index;
	uint32_t timeStamp;
	double x[32];
	double y[32];
	double z[32];
	uint32_t validBits;
	uint32_t extraInt;
} message;

typedef struct
{
	int command;
	int data_len;
	long buf[CMD_DATA_LEN];
	char exp[CMD_DATA_LEN];

	// internal use
	char received_check;
} command;



#ifdef __cplusplus
}
#endif
#endif
