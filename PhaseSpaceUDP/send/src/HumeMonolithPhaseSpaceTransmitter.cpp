// simple rigid body tracking progr

#include <stdio.h>
#include <math.h>

#include "owl.h"
#include "owl_math.h"
#include "comm_udp.h"
#include <vector>
#include <fstream>
#include <iostream>

#define MARKER_COUNT 9
#define SERVER_NAME "localhost"
#define INIT_FLAGS 0

std::ofstream savefile_condition;
//TODO: This has never been tested since the file comm_udp.c was merged with it.

struct sockaddr_in si_other;
int slen;

int s = 0;
int indexOfMessage = 0;
void send_udp(message *pMsg, const char * sendAddress)
{
	if (s == 0)
	{
		if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
			return;
		si_other.sin_addr.s_addr = inet_addr(sendAddress);

	}

	struct sockaddr_in si_to;
	memset((char *) &si_to, 0, sizeof(si_to));
	si_to.sin_family = AF_INET;
	si_to.sin_port = htons(POS_PORT);
	si_to.sin_addr.s_addr = inet_addr(sendAddress); // this is the meka ip address

	slen = sizeof(si_to);

	pMsg->index = indexOfMessage; // index increments
//	int ret = sendto(s, pMsg, sizeof(message) + sizeof(double)*(pMsg->count-1), 0, &si_to, sizeof(si_to));
			// why is this not just size of message? Why send more? Why not include the data in the structure definition?
	int ret = sendto(s, pMsg, sizeof(message), 0, (const struct sockaddr*) &si_to, sizeof(si_to));
	if (indexOfMessage % 100 == 0)
		fprintf(stderr, "Size of message is %d, ADDR: %08x(%d:%d %d)\n", (int) sizeof(message),si_to.sin_addr.s_addr, slen, (int) sizeof(si_to), ret);
	++indexOfMessage;
}

void owl_print_error(const char *s, int n);

void copy_p(const float *a, float *b)
{
	for (int i = 0; i < 7; i++)
		b[i] = a[i];
}
void print_p(const float *p)
{
	for (int i = 0; i < 7; i++)
		printf("%f ", p[i]);
}

int main(int argc, char *argv[])
{
	const char * ipstring = "";
	if (argc != 2)
	{
		ipstring = "192.168.1.6";
		fprintf(stderr, "No IP address specified, using default %s\n", ipstring);
	}
	else
	{
		int a, b, c, d;
		if (sscanf(argv[1], "%d.%d.%d.%d", &a, &b, &c, &d) != 4)
		{
			ipstring = "192.168.1.6";
			fprintf(stderr, "Invalid IP: %s, using default %s\n", argv[1], ipstring);
		}
		else
		{
			ipstring = argv[1];
			fprintf(stderr, "Good IP: using %d.%d.%d.%d = %s\n", a, b, c, d, ipstring);
		}
	}
	static int time(0);

	static int count(0);
	int tracker(0);

	OWLMarker markers[32];

	if (owlInit(SERVER_NAME, INIT_FLAGS) < 0)
		return 0;

	owlTrackeri(tracker, OWL_CREATE, OWL_POINT_TRACKER);

	// // set markers
	for (int i = 0; i < 32; i++)
		owlMarkeri(MARKER(tracker, i), OWL_SET_LED, i);

	// activate tracker
	owlTracker(tracker, OWL_ENABLE);

	// flush requests and check for errors
	if (!owlGetStatus())
	{
		owl_print_error("error in point tracker setup", owlGetError());
		return 0;
	}

	// set default frequency
	owlSetFloat(OWL_FREQUENCY, OWL_MAX_FREQUENCY);

	// start streaming
	owlSetInteger(OWL_STREAMING, OWL_ENABLE);

	long long index = 0;
	message myMessage;
	message* msg = &myMessage;
	// main loop
	while (1)
	{
		int err;

		// get markers
		//  note: markers have to be read,
		//  even if they are not used
		int m = owlGetMarkers(markers, 32);

		// check for error
		if ((err = owlGetError()) != OWL_NO_ERROR)
		{
			owl_print_error("error", err);
			break;
		}

		// no data yet
		if (m == 0)
		{
			continue;
		}

		msg->index = (int) index;
		msg->timeStamp = index;

		int bits = 0x0000;
//		GT-This is a better way:
		for (int i(0); i < 32; i++)
		{
			msg->x[i] = markers[i].x;
			msg->y[i] = markers[i].y;
			msg->z[i] = markers[i].z;
			bits = bits << 1;		//lowest index => highest value bit
			bits |= (markers[i].cond >= 0);
		}
		msg->validBits = bits;
		Int32BitField* bitfield = (Int32BitField*) &bits;
		if (count % 100 == 0)
			fprintf(stderr,
					"Data[0:4]: validity:%x = {%d,%d,%d,%d,%d,%d,%d,%d,...} (%f %f %f)(%f %f %f)(%f %f %f)(%f %f %f)\n",
					bits, bitfield->b0, bitfield->b1, bitfield->b2,bitfield->b3, bitfield->b4, bitfield->b5,
					bitfield->b6, bitfield->b7, msg->x[0], msg->y[0], msg->z[0], msg->x[1], msg->y[1], msg->z[1],
					msg->x[2], msg->y[2], msg->z[2], msg->x[3], msg->y[3], msg->z[3]);

		++count;
		++time;
		send_udp(msg, ipstring);
	}
	owlDone();
}

void owl_print_error(const char *s, int n)
{
	if (n < 0)
		printf("%s: %d\n", s, n);
	else if (n == OWL_NO_ERROR)
		printf("%s: No Error\n", s);
	else if (n == OWL_INVALID_VALUE)
		printf("%s: Invalid Value\n", s);
	else if (n == OWL_INVALID_ENUM)
		printf("%s: Invalid Enum\n", s);
	else if (n == OWL_INVALID_OPERATION)
		printf("%s: Invalid Operation\n", s);
	else
		printf("%s: 0x%x\n", s, n);
}
