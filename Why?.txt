Because I want to understand, to truly understand, the control of robots.
Because this is my lightsaber, and I cannot be a jedi until I build my own.
Because these tools will allow me to reach the real frontier of research.
Because building tools and abstractions is my secret comic-book superpower.
Because I am at home in my arcane sanctuary.
Because I said I would.
Because I can only write about stuff that I've actually done.
Because I'm excited to see it work.
Because I'm in my element when I'm debugging code.
Because I want to become a master of code.
Because these problems I'm solving are so deeply compelling.
Because using magic is part of being a wizard.
Because I want to design what cannot be controlled except by advanced software.
Because I dream of automatically tuning dynamic models.
Because I dream of robots which nimbly balance on poles, railings, and ledges.
Because I dream of great feats of robot athleticism.
Because I want to dance with a robot.
This is why I am writing software right now. 
This is why I sneak in software development where I can.