#include <iostream>
#include "smtclient.hpp"


class Ankle_Actuator
{
public:
    std::string base;
    SMT::Resource desired_force;
    SMT::Resource measured_force;
    SMT::Resource position;
    SMT::Resource embedded_velocity;
    SMT::Resource load_cell_force;
    Ankle_Actuator(SMT::SMTClient client, std::string leg, std::string joint):
        base("/" + leg + "/" + joint),
        desired_force(client.getResource(base + "/JointForce_Des_N")),
        measured_force(client.getResource(base + "/JointForce_Meas_N")),
        position(client.getResource(base + "/Linear_Pos_m")),
        embedded_velocity(client.getResource(base + "/Linear_Vel_mps")),
        load_cell_force(client.getResource(base + "/LoadCell_N"))
    {
        //  std::string base = "/"+leg+"/"+joint;
        // SMT::SMTClient client;
    }
    void set_desired_force(float force)
    {
        desired_force.set<float>(force);
    }
    float get_measured_force()
    {
        return measured_force.get<float>();
    }
    float get_position()
    {
        return measured_force.get<float>();
    }
};

int main(void)
{
    std::cout << "Hello World" << std::endl;
    std::string resourceName = "/right_leg/j2/JointAPS_Angle_Rad";
    SMT::SMTClient client;
    Ankle_Actuator rlj5(client, std::string("right_leg"), std::string("j5"));
    Ankle_Actuator rlj6(client, std::string("right_leg"), std::string("j6"));
    Ankle_Actuator ankles[] = {rlj5, rlj6};
    SMT::Resource resource = client.getResource(resourceName);

    // ASSERT_EQ(resource.getType(),"float32");
    for (int i = 0; i < 2; i++)
    {
        Ankle_Actuator &ankle = ankles[i];
        ankle.set_desired_force(0.0);
    }

    float value = -32.1234;

    resource.set<float>(value);
    // ASSERT_EQ(resource.get<float>(), value);
}