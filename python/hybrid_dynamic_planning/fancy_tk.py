#---------Imports
from numpy import arange, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import Tkinter as tk
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import hybrid_impedance_walker as hiw
#---------End of imports

class Application(tk.Frame):

    def say_hi(self):
        print "hi there, everyone!"

    def createWidgets(self):    
        self.add_button_panel(self).pack({"side":"left"})
        # self.add_animation_canvas(self).pack({"side":"left"})
        FigureCanvasTkAgg(self.sim_fig, master=self).get_tk_widget().pack()
        # self.fig.show()

    def re_simulate(self):
        self.sim.simulate()

    def add_Generic_slider(self,parent,name,lo,hi,res,default,command):
        frame=tk.Frame(parent)
        text=tk.Label(frame, text=name).pack({"side":"left"})
        self.L1_slider = tk.Scale(frame, from_=lo, to=hi, 
            orient=tk.HORIZONTAL, resolution=res, command=command)
        self.L1_slider.pack()
        self.L1_slider.set(default)
        return frame

    def add_button_panel(self,parent):
        button_pannel=tk.Frame(master=self)
        self.add_quit_button(button_pannel).pack({"side": "bottom"})
        self.add_resimulate_button(button_pannel).pack()
        self.add_Generic_slider(button_pannel,"G",8.0,10.0,0.01, self.sim.G, lambda x: self.sim.set_val("G",float(x))).pack()
        self.add_Generic_slider(button_pannel,"Tf",0.1,10.0,0.1, self.sim.Tf, lambda x: self.sim.set_val("Tf",float(x))).pack()
        self.add_Generic_slider(button_pannel,"Kp_y",0.0,10000.0,1.0, self.sim.Kp[1,1], lambda x: self.sim.set_mat("Kp",1,1,float(x))).pack()
        self.add_Generic_slider(button_pannel,"Kd_y",0.0,1000.0,1.0, self.sim.Kd[1,1], lambda x: self.sim.set_mat("Kd",1,1,float(x))).pack()
        self.add_Generic_slider(button_pannel,"Kp_o",0.0,1000.0,1.0, self.sim.Kp[0,0], lambda x: self.sim.set_mat("Kp",0,0,float(x))).pack()
        self.add_Generic_slider(button_pannel,"Kd_o",0.0,1000.0,1.0, self.sim.Kd[0,0], lambda x: self.sim.set_mat("Kd",0,0,float(x))).pack()
        self.add_Generic_slider(button_pannel,"x_0",-1.0,1.0,0.01, self.sim.X0[0], lambda x: self.sim.set_vec("X0",0,float(x))).pack()
        # self.w2 = tk.Scale(self, from_=0.00, to=2.00, orient=tk.HORIZONTAL, resolution=0.01)
        # self.w2.grid(column=5, row = 1)
        # self.w2.set(1.0)

        return button_pannel

    def add_quit_button(self,parent):
        quit_button = tk.Button(parent)
        quit_button["text"] = "QUIT"
        quit_button["fg"]   = "red"
        quit_button["command"] =  self.quit
        return quit_button

    def add_resimulate_button(self,parent):
        resim = tk.Button(parent)
        resim["text"] = "Re-Simulate",
        resim["command"] = self.re_simulate
        return resim


    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.pack()
        self.sim = hiw.SingleImpedancePointFoot()
        self.sim_fig=self.sim.generate_figure()
        self.sim.init()
        self.sim.simulate()
        self.createWidgets()
        self.sim.begin_animation()

# root = tk.Tk()

def main():
    root = tk.Tk()
    app = Application(master=root)
    app.mainloop()
    root.destroy()
    

if __name__ == "__main__":
    main()

