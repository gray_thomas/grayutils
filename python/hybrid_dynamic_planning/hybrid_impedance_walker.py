# Double pendulum formula translated from the C code at
# http://www.physics.usyd.edu.au/~wheat/dpend_html/solve_dpend.c
# @editor: gray_thomas

from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation

# Monoantistasimeopod
class SingleImpedancePointFoot:
    def __init__(self):
        self.i=0
        self.G = 9.8 # acceleration due to gravity, in m/s^2
        self.M = 20.0 # Kg
        self.J = 1.0 # Kg m^2
        self.ys = 1.0 # height setpoint in meters
        self.os = 0.0 # pitch setpoint in radians
        self.Kd = np.array([
                #   e_o  ,    no x  ,    e_y  
                [   10.0  ,               0.0  ], # theta  (kg m m /s/s) per (rad/s) = kg m m/s/rad
                #   x is free
                [   0.0  ,               100.0  ], # y (kg m/s/s) per (m/s) = kg/s
            ])
        self.Kp = np.array([
                #   e_o  ,  no x,   e_y  
                [   500.0  ,          0.0  ], # theta (kg m m /s/s) per (rad) = kg m m/s/s/rad
                # x is free 
                [   0.0  ,          10000.0  ], # y (kg m/s/s) per (m) = kg/s/s
            ])
        self.T0 = 0.0 # seconds
        self.Tf = 0.5 # seconds
        self.dt = 0.005 # seconds
        self.footstep = 0.0 # position in meters
        self.X0 = np.array([
                -0.2, # x_com
                0.98, # y_com
                0.1, # pitch
                0.8, # xd_com
                0.0, # yd_com
                0.0, # pitch_dot
            ])


        # self.init()

    def derivs(self, X, t):
        x = X[0]
        y = X[1]
        o = X[2]
        xd = X[3]
        yd = X[4]
        od = X[5]
        e_o = o-self.os # theta error
        ed_o = od # theta error rate of change
        e_y = y-self.ys # height error
        ed_y = yd # height error rate of change
        e = np.array([
                [e_o],
                [e_y]
            ])
        ed = np.array([
                [ed_o],
                [ed_y]
            ])
        control = -self.Kp.dot(e) - self.Kd.dot(ed)
        r_o = control[0,0] # reaction moment on pitch at center of mass
        r_y = control[1,0] # reaction force on height
        p_x = x - self.footstep # vector from foot to center of mass, x component
        p_y = y - 0.0 # vector from foot to center of mass, y component
        # -r_x * p_y + r_y * p_x = r_o  # moment balance
        r_x = (1.0/p_y) * (r_y * p_x - r_o) # algebraic manipulation of moment balance
        xdd = (1.0/self.M) * r_x # Newton's equations of motion
        ydd = (1.0/self.M) * r_y - self.G # Newton's equations of motion, with gravity acceleration
        odd = (1.0/self.J) * r_o # Netwon's EOM, rotational case
        dXdt = np.array([
                xd , # x_com
                yd , # y_com
                od , # pitch
                xdd , # xd_com
                ydd , # yd_com
                odd , # pitch_dot
            ])

        return dXdt

    def set_val(self,name, val):
        self[name]=val

    def __getitem__(self,name):
        return self.__dict__[name]

    def __setitem__(self,name,val):
        self.__dict__[name]=val

    def set_mat(self,name,i,j,val):
        self[name][i,j]=val

    def set_vec(self,name,i,val):
        self[name][i]=val

    def simulate(self): 
        self.t = np.arange(self.T0, self.Tf, self.dt)
        self.X = self.X0[:]
        self.data = integrate.odeint(self.derivs, self.X, self.t)
        self.data_row_labels=["x","y","theta","xd","yd","thetad"]
        self.i=len(self.t)

    def generate_figure(self):
        self.fig = plt.figure()
        self.phase_ax = self.fig.add_subplot(221, autoscale_on=True)
        self.ax = self.fig.add_subplot(222, autoscale_on=False, xlim=(-2, 2), ylim=(-0.2, 1.875),aspect='equal')
        self.height_ax = self.fig.add_subplot(223, autoscale_on=True)
        self.pitch_ax = self.fig.add_subplot(224, autoscale_on=True)
        self.ax.grid()
        return self.fig

    def init(self):
        self.line, = self.ax.plot([], [], 'o-', lw=2)
        self.phase_space, = self.phase_ax.plot([],[], lw=2)
        self.phase_space_point, = self.phase_ax.plot([],[],'r*', lw=8, ms=8)
        self.height_des_line, = self.height_ax.plot([],[], 'k',lw=2)
        self.height_line, = self.height_ax.plot([],[], 'b' , lw=2)
        self.height_dot, = self.height_ax.plot([],[], 'r*', lw=8, ms=8)
        self.pitch_des_line, = self.pitch_ax.plot([],[], 'k',lw=2)
        self.pitch_line, = self.pitch_ax.plot([],[], 'b' , lw=2)
        self.pitch_dot, = self.pitch_ax.plot([],[], 'r*', lw=8, ms=8)
        self.time_template = 'time = %.3fs'
        self.time_text = self.ax.text(0.05, 0.9, '', transform=self.ax.transAxes)
        self.line.set_data([], [])
        self.time_text.set_text('')
        self.phase_ax.set_xlabel(r"$x_{com}$")
        self.phase_ax.set_ylabel(r"$\dot x_{com}$")
        self.phase_ax.set_title("Phase Space")
        self.height_ax.set_xlabel(r"$x_{com}$")
        self.height_ax.set_ylabel(r"$y_{com}$")
        self.height_ax.set_title("Height Graph")
        self.pitch_ax.set_xlabel(r"$x_{com}$")
        self.pitch_ax.set_ylabel(r"$\theta_{body}$")
        self.pitch_ax.set_title("Pitch Graph")
        self.ax.set_title("Robot Animation")
        return self.line, self.time_text

    def init_animation(self):
        self.line.set_data([], [])
        self.phase_ax.relim()
        self.phase_ax.autoscale_view()
        self.height_ax.relim()
        self.height_ax.autoscale_view()
        self.pitch_ax.relim()
        self.pitch_ax.autoscale_view()
        # self.phase_space.set_data(self.data[:,0],self.data[:,3])
        self.time_text.set_text('')
        return self.line, self.time_text

    def animate(self,i):
        if self.i>=len(self.t):
            self.i=0
        i=self.i

        if (i==0 or i==1):
            self.phase_ax.relim()
            self.phase_ax.autoscale_view()
            self.height_ax.relim()
            self.height_ax.autoscale_view()
            self.pitch_ax.relim()
            self.pitch_ax.autoscale_view()
            self.fig.canvas.draw()
            self.phase_space.set_data(self.data[:,0],self.data[:,3])
            self.height_line.set_data(self.data[:,0],self.data[:,1])            
            self.height_des_line.set_data([self.data[0,0],self.data[-1,0]],[self.ys, self.ys])
            self.pitch_line.set_data(self.data[:,0],self.data[:,2])
            self.pitch_des_line.set_data([self.data[0,0],self.data[-1,0]],[self.os, self.os])

        self.phase_space_point.set_data(self.data[i,0],self.data[i,3])
        self.height_dot.set_data(self.data[i,0],self.data[i,1]) 
        self.pitch_dot.set_data(self.data[i,0],self.data[i,2])
        # self.height_line.set_data(self.data)

        

        self.thisx = [self.footstep, self.data[i,0], self.data[i,0]+0.2*cos(self.data[i,2])]
        self.thisy = [0.0,           self.data[i,1], self.data[i,1]+0.2*sin(self.data[i,2])]

        self.line.set_data(self.thisx, self.thisy)

        self.time_text.set_text(self.time_template%(i*self.dt))
        self.i+=1
        return self.line, self.time_text, self.phase_space_point, self.height_dot,         self.pitch_dot, self.height_line, self.height_des_line, self.phase_space, self.pitch_line, self.pitch_des_line

    def begin_animation(self):
        self.ani = animation.FuncAnimation(self.fig, self.animate, np.arange(1, len(self.t)),
        interval=50, blit=True, init_func=self.init_animation)
        

#ani.save('double_pendulum.mp4', fps=15, clear_temp=True)
def main():
    sim = SingleImpedancePointFoot()
    sim.generate_figure()
    sim.init()
    sim.simulate()
    sim.begin_animation()
    plt.show()

if __name__ == "__main__":
    main()
