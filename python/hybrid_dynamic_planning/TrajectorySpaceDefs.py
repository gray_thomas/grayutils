import numpy as np
from math import sqrt, pow, atan2, pi
g=9.8

def angle_wrap(angle):
    if angle>pi:
        angle=angle_wrap(angle-2.0*pi)
    if angle<-pi:
        angle=angle_wrap(angle+2.0*pi)
    return angle

def to_deg(angle):
    return angle*180.0/pi

def to_trajectory_space(X):
    x, y, xd, yd = unpack_state_space(X)
    tfv=-yd/g
    xv = x + xd*yd/g
    yv = y + yd*yd/g*0.5
    hv = abs(xd)*xd
    return pack_trajectory_space(xv,yv,hv,tfv)

def pack_state_space(x,y,xd,yd):
    X=np.array([x,y,xd,yd])
    return X

def pack_trajectory_space(xv, yv, xd, tfv):
    T=np.array([xv,yv,xd,tfv])
    return T

# x, y, xd, yd = unpack_state_space(X)
def unpack_state_space(X):
    x = X[0]
    y = X[1]
    xd = X[2]
    yd = X[3]
    return x, y, xd, yd

#  xv, yv, xd, tfv = unpack_trajectory_space(T)
def unpack_trajectory_space(T):
    xv = T[0]
    yv = T[1]
    xd = T[2]
    tfv = T[3]
    return xv, yv, xd, tfv