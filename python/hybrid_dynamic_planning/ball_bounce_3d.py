"""
A simple example of an animated plot... In 3D!
"""
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation

def Gen_RandLine(time, dims=2, dt=0.01,initial_speed=0.1,restitution_constant=0.8) :
    """
    Create a line using a ball bouncing algorithm.

    time is the simulation duration.
    dims is the number of dimensions the line has.
    """
    length = int(time/dt)
    ballPos = np.empty((dims, length))
    ballVel = np.empty((dims, length))
    ballPos[:,0]=np.random.rand(dims)
    ballVel[:,0]=(np.random.rand(dims) - 0.5*np.ones(3))*initial_speed
    gravity = np.zeros(dims)
    gravity[-1]=-9.8
    damping=0.001
    
    for index in range(1, length) :
        # scaling the random numbers by 0.1 so
        # movement is small compared to position.
        # subtraction by 0.5 is to change the range to [-0.5, 0.5]
        # to allow a line to move backwards.
        ballAccel = gravity-damping*ballVel[:, index-1]
        if abs(ballPos[-1, index-1])<0.01:
            if abs(ballVel[-1,index-1])<0.2:
                ballPos[-1, index-1]=0
                ballVel[-1,index-1]=0
                ballAccel[-1]=ballPos[-1, index-1]
        ballVel[:, index] = ballVel[:, index-1]+ dt*ballAccel
        ballPos[:, index] = ballPos[:, index-1]+ dt*ballVel[:, index-1]+0.5*dt*dt*ballAccel
        for i in range(0,3):
            if ballPos[i, index]<0 and ballVel[i,index]<0:
                ballVel[i, index]*=-restitution_constant
                # ballPos[i, index]=0  
            if ballPos[i, index]>1 and ballVel[i,index]>0:
                ballVel[i, index]*=-restitution_constant
                # ballPos[i, index]=1

    return ballPos, ballVel

def update_lines(num, ax, dataLines, lines, points) :
    for line, point, data in zip(lines, points, dataLines) :
        # NOTE: there is no .set_data() for 3 dim data...
        line.set_data(data[0:2, :num])
        line.set_3d_properties(data[2,:num])
        point.set_data(data[0:2, num])
        point.set_3d_properties(data[2,num])
        ax.view_init(30-0.1*num, num*1)
    return lines

# Attaching 3D axis to the figure
fig = plt.figure()
ax = p3.Axes3D(fig)
dt=0.01
time=10
balls=50
initial_speed=2.1
restitution_constant=0.75

# Fifty lines of random 3-D lines
data, vels = zip(*[Gen_RandLine(time, 3, dt=dt, initial_speed=initial_speed,restitution_constant=restitution_constant) for index in range(balls)])

# Creating fifty line objects.
# NOTE: Can't pass empty arrays into 3d version of plot()
lines = [ax.plot(dat[0, 0:1], dat[1, 0:1], dat[2, 0:1])[0] for dat in data]
points = [ax.plot(dat[0, 0:1], dat[1, 0:1] , dat[2, 0:1 ],'b.',ms=10)[0] for dat in data]

# Setting the axes properties
ax.set_xlim3d([0.0, 1.0])
ax.set_xlabel('X')

ax.set_ylim3d([0.0, 1.0])
ax.set_ylabel('Y')

ax.set_zlim3d([0.0, 1.0])
ax.set_zlabel('Z')

ax.set_title('3D Ball Bouncing')

# Creating the Animation object
line_ani = animation.FuncAnimation(fig, update_lines, int(time/dt), fargs=(ax, data, lines, points),
                              interval=50, blit=False)
# line_ani.save('the_balls_bouncing_movie.mp4', bitrate=3000, writer='avconv',fps=30)
plt.show()