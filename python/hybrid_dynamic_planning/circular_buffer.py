#circular_buffer.py
data=[]
is_full=False
index_of_start=0
finish=0
size=4
print "I'm a buffer module and I'm being imported!"
for i in range (0,size):
	data.append(-1)

def append(datum):
	# list the globals this function is allowed to change
	global finish
	global index_of_start
	global is_full
	data[finish]=datum 
	new_finish=(finish+1)%size
	if (not is_full) and finish+1==size:
		is_full=True
	else:
		if is_full:
			index_of_start+=1

	# when we are full, data[finish] is data[start]
	# so this step overwrites our previous starting datum:
	finish=new_finish

# def index(meta_index):
# 	return data[meta_index-index_of_start] # works until the first loop

def index(meta_index):
	if meta_index<index_of_start:
		index_to_return=meta_index%size
		meta_index_to_return=index_of_start # corresponds to start
		indexes_beyond_startfinish = (index_to_return-finish+size) % size
		meta_index_to_return+=indexes_beyond_startfinish
		print "this data index "+str(meta_index)+" has been deleted, so I'll give you index number "+str(
			meta_index_to_return)+" instead, ok?"
	return data[meta_index%size]

def reverse_index(reverse_meta_index):
	if reverse_meta_index>=size:
		print "this reverse index is too large."
	elif not is_full and finish-reverse_meta_index<0:
		print "this revere index doesn't exist!"
	else:
		return data[(finish+size-reverse_meta_index)%size]

def print_me():
	print_string= "hello world, "
	print_string+="I am a buffer! \n"
	print_string+="My size is " + str(size) + " "
	print_string+="and my elements are: "
	for i in range (0,size):
		if i==size-1:
			print_string+="& "
		print_string+=str(data[i])+" "
	print_string+="\n"
	if is_full:
		print_string+="my start and my finish are both "+str(finish)+ ", since i'm full, and "
		print_string+="start is meta_index "+str(index_of_start)
	else:
		print_string+="my finish is "+str(finish)
	
	print print_string

def simple_print_me():
	print "buf: [%d,%d,%s]--[%+4.2f, %+4.2f, %+4.2f, %+4.2f] " % (
		finish, index_of_start, str(is_full), data[0], data[1], data[2], data[3])