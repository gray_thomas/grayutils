import numpy as np
import matplotlib.pyplot as plt
from math import log, sqrt

t0=np.linspace(-1,1,1000)
a=1
b=-1
d=1
eps=0.05
for c in np.linspace(1.5,3.5,10):
	m_pos_lim=d
	m_neg_lim=-c
	tmax=log(eps*abs(a)*sqrt(1+d*d))
	tmin=-log(eps*abs(b)*sqrt(1+c*c))
	t=np.linspace(tmin,tmax,1000)
	x=(a*np.exp(t)+b*np.exp(-t))*eps
	y=(a*d*np.exp(t)-b*c*np.exp(-t))*eps
	plt.plot(x,y)
	plt.axis('equal')
plt.show()