# Double pendulum formula translated from the C code at
# http://www.physics.usyd.edu.au/~wheat/dpend_html/solve_dpend.c
# @editor: gray_thomas

from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection
from mpl_toolkits.mplot3d import Axes3D
from math import sqrt, pow, atan2, pi
from SingleContactNumericSimulator import SimCalculator
from TrajectorySpaceDefs import *


class CenterOfMassGraphic:
    def __init__(self):
        pass
    def init(self,axis):
        size=0.0625
        edge_thickness=1.35
        self.origin=[-10,-10]
        self.wedge1=mpatches.Wedge(self.origin, size, 90, 180, color='k')
        self.wedge2=mpatches.Wedge(self.origin, size, 270, 360, color='k')
        self.circle=mpatches.Circle(self.origin, size, facecolor='w', linewidth=edge_thickness, edgecolor='k')
        axis.add_patch(self.circle)
        axis.add_patch(self.wedge1)
        axis.add_patch(self.wedge2)
        return [self.circle, self.wedge1, self.wedge2]

    def update(self,data):
        center=(data[0],data[1])
        self.circle.center= center
        self.wedge1.center=center
        self.wedge1._recompute_path()
        self.wedge2.center=center
        self.wedge2._recompute_path()
        return [self.circle, self.wedge1, self.wedge2]

class AnimationSubPlot:
    def __init__(self,fig, sim, r,c,n):
        self.ax = fig.add_subplot(r,c,n, autoscale_on=False, xlim=(-2, 2), ylim=(-0.2, 1.875),aspect='equal')
        self.ax.grid()
        self.graphics=[self, CenterOfMassGraphic(), sim.platform]
        self.sim=sim
        
    def get_graphic_objects(self):
        self.ax.set_title("Single Support Bend")
        graphic_returns=[]
        for graphic_object in self.graphics:
            graphic_returns.extend(graphic_object.init(self.ax))
        return self.graphics, graphic_returns

    def add_graphic(self, graphic):
        self.graphics.append(graphic)

    def init(self,axis):
        self.leg, = axis.plot([], [], 'o-', lw=2)
        self.time_template = 'time = %.3fs'
        self.time_text = axis.text(0.05, 0.9, '', transform=axis.transAxes)
        self.leg.set_data([], [])
        self.time_text.set_text('')
        return [self.leg, self.time_text]

    def update(self,data):
        self.time_text.set_text(self.time_template%(self.sim.i*self.sim.dt))
        F_x, F_y, inContact = self.sim.calculate_single_support_force(data)
        if inContact:
            self.thisx = [self.sim.platform.foot[0], data[0]]
            self.thisy = [self.sim.platform.foot[1], data[1]]
        else:
            self.thisx = [data[0], data[0]]
            self.thisy = [data[1], data[1]]

        self.leg.set_data(self.thisx, self.thisy)
        return [self.leg, self.time_text]

    def reset(self):
        self.ax.get_figure().canvas.draw()

class HeightSubPlot:
    def __init__(self,fig,sim,r,c,n):
        self.ax = fig.add_subplot(r,c,n, autoscale_on=True)
        self.sim=sim
        
    def get_graphic_objects(self):
        self.ax.set_xlabel(r"$x_{com}$")
        self.ax.set_ylabel(r"$y_{com}$")
        self.ax.set_title("Height Graph")
        self.graphics=[self]
        graphic_returns=self.init(self.ax)
        return self.graphics, graphic_returns

    def init(self,axis):
        self.line, = self.ax.plot([],[], 'b' , lw=2)
        self.dot, = self.ax.plot([],[], 'r*', lw=8, ms=8)
        return [self.line, self.dot]

    def update(self,data):
        self.dot.set_data(data[0],data[1]) 
        return [self.dot]

    def reset(self):
        self.ax.relim()
        self.ax.autoscale_view()
        self.ax.get_figure().canvas.draw() # TODO: check/debug
        self.line.set_data(self.sim.data[:,0],self.sim.data[:,1])       

class TrajectorySpaceSubPlot:
    def __init__(self,fig,sim,r,c,n):
        self.ax = fig.add_subplot(r,c,n, projection='3d')
        self.sim=sim
        self.past_data=[]
        self.sim.add_sim_notify(self.update_data)
        self.past_lines=[]

    def update_data(self):
        self.past_data.append(np.array(self.sim.data))
        xvs=[]
        yvs=[]
        hvs=[]
        for i in range(0,len(self.sim.t)):
            xv,yv,hv,t=unpack_trajectory_space(to_trajectory_space(self.sim.data[i,:]))
            xvs.append(xv)
            yvs.append(yv)
            hvs.append(hv)
        self.past_lines.extend(self.ax.plot(xvs,yvs,hvs))

    def get_graphic_objects(self):
        # print "hey"
        self.ax.set_xlabel(r"$x_{v}$")
        self.ax.set_ylabel(r"$y_{v}$")
        self.ax.set_zlabel(r"$h_v$")
        self.ax.set_title("Trajectory Space")
        self.ax.set_zlim3d(-2,2)
        self.ax.set_xlim3d(-3,1)
        self.ax.set_ylim3d(0.0,5.0)
        self.graphics=[self]
        graphic_returns=self.init(self.ax)
        return self.graphics, graphic_returns

    def init(self, axis):
        self.point=axis.plot([-9999],[-95699],[-9989],'b^')
        return self.point

    def update(self,X):
        xv,yv,xd,t=unpack_trajectory_space(to_trajectory_space(X))
        self.point[0].set_data([xv],[yv])
        self.point[0].set_3d_properties([xd])
        lines=[self.point[0]]
        lines.extend(self.past_lines)
        return lines
    def reset(self):
        pass


class TrajectorySpace2DSubPlot:
    def __init__(self,fig,sim,r,c,n):
        self.ax = fig.add_subplot(r,c,n)
        # self.sim=sim
        self.past_data=[]
        self.sim.add_sim_notify(self.update_data)
        self.past_arrows=[]

    def update_data(self):
        self.past_data.append(np.array(self.sim.data))
        xv,yv,xd,t=unpack_trajectory_space(to_trajectory_space(self.sim.data[len(self.sim.t)-1,:]))
        soa =np.array([[xv,yv,xd,0]]) 
        X,Y,U,V = zip(*soa)
        self.past_arrows.append(self.ax.quiver(X,Y,U,V))

    def get_graphic_objects(self):
        # print "hey"
        self.ax.set_xlabel(r"$x_{v}$")
        self.ax.set_ylabel(r"$y_{v}$")
        self.ax.set_zlabel(r"$\dot x$")
        self.ax.set_title("Trajectory Space")
        self.ax.set_zlim3d(-2,2)
        self.ax.set_xlim3d(-3,1)
        self.ax.set_ylim3d(0.0,5.0)
        self.graphics=[self]
        graphic_returns=self.init(self.ax)
        return self.graphics, graphic_returns

    def init(self, axis):
        self.point=axis.plot([-9999],[-95699],'b^')
        return self.point

    def update(self,X):
        xv,yv,hv,t=unpack_trajectory_space(to_trajectory_space(X))
        self.point[0].set_data([xv],[yv])
        self.point[0].set_3d_properties([hv])
        lines=[self.point[0]]
        lines.extend(self.past_lines)
        return lines
    def reset(self):
        pass

# Monoantistasimeopod
class SingleSupportSimulationAnimator:
    def __init__(self):
        self.i=0
        self.sim=SimCalculator()

    def generate_figure(self):
        self.fig = plt.figure(figsize=(18,16))
        self.height_plot=HeightSubPlot(self.fig,self.sim,1,2,1)
        self.animation_plot=AnimationSubPlot(self.fig,self.sim,2,2,2)
        self.trajectory_plot=TrajectorySpaceSubPlot(self.fig, self.sim, 2,2,4)
        self.plots=[self.height_plot, self.animation_plot, self.trajectory_plot]
        return self.fig

    def init(self):
        self.graphic_returns=[]
        self.graphic_objects=[]
        for plot in self.plots:
            graphic_objects, graphic_returns = plot.get_graphic_objects()
            self.graphic_returns.extend(graphic_returns)
            self.graphic_objects.extend(graphic_objects)
        return self.graphic_returns

    def init_animation(self):
        for plot in self.plots:
            plot.reset()
        return self.graphic_returns

    def animate(self,i):
        i=self.sim.increment_time()
        if (i==0 or i==1):
            for plot in self.plots:
                plot.reset()
        # print i
        graphic_returns=[]
        for graphic in self.graphic_objects:
            graphic_returns.extend(graphic.update(self.sim.data[i,:]))

        return graphic_returns

    def begin_animation(self):
        self.ani = animation.FuncAnimation(self.fig, self.animate, np.arange(1, len(self.sim.t)),
        interval=1, blit=True, init_func=self.init_animation)

    def start(self):
        self.generate_figure()
        self.init()
        self.sim.simulate()
        print unpack_trajectory_space(to_trajectory_space(self.sim.data[0,:]))
        self.begin_animation()
        

#ani.save('double_pendulum.mp4', fps=15, clear_temp=True)
def main():
    animator = SingleSupportSimulationAnimator()
    animator.start()
    plt.show()

if __name__ == "__main__":
    main()
