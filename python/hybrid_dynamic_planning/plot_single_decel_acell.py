import matplotlib.pyplot as plt 
import numpy as np 
from mpl_toolkits.mplot3d import Axes3D
from math import pi, cos, sin
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.colors as mplcolors
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np

def plot_constant_initial_velocity():
	print "hello world"
	inv_h = 10.0
	omax=pi*0.9
	omin=-omax
	rmin=0
	rmax=0.2

	os=np.linspace(omin,omax,100)
	rs = np.linspace(rmin,rmax,20)
	fig = plt.figure(facecolor="white")
	ax = fig.add_subplot(1,1,1, projection="3d")
	for r in rs:
		xvs = r*np.cos(os)+inv_h*r*r*np.sin(os)*np.cos(os)
		yvs = r*np.sin(os)+inv_h*r*r*np.sin(os)*np.sin(os)
		qvs = inv_h*r*r*np.cos(os)*np.abs(np.cos(os))
		ax.plot(xvs,yvs,qvs,lw=2)
	for o in os:
		xvs = rs*np.cos(o)+inv_h*rs*rs*np.sin(o)*np.cos(o)
		yvs = rs*np.sin(o)+inv_h*rs*rs*np.sin(o)*np.sin(o)
		qvs = inv_h*rs*rs*np.cos(o)*np.abs(np.cos(o))
		ax.plot(xvs,yvs,qvs,lw=2)
	ax.set_xlabel(r"$x_v$",size=20)
	ax.set_ylabel(r"$y_v$",size=20)
	ax.set_zlabel(r"$h_d$",size=20)
	# ax.set_title(r"${\rmTrajectories\ with\ a\ common\ origin}$",size=25)


	fig = plt.figure(facecolor="white")
	ax = fig.add_subplot(111, projection='3d')

	o = np.linspace(omin, omax, 100)
	r = np.linspace(rmin, rmax, 40)

	x =  np.outer(r*r, inv_h*np.sin(o)*np.cos(o))+np.outer(r, np.cos(o))
	y =  np.outer(r*r, inv_h*np.sin(o)*np.sin(o))+np.outer(r, np.sin(o))
	z =  np.outer(r*r, inv_h*np.cos(o)*np.abs(np.cos(o)))
	theta = np.outer(np.ones(np.size(r)),0.5*np.ones(np.size(o))+o*(0.5/np.pi))
	surf=ax.plot_surface(x, y, z, norm=theta, rstride=1, cstride=1, facecolors=cm.coolwarm(theta),
        linewidth=0, antialiased=False, alpha=0.7)
	ax.set_xlabel(r"$x_v$",size=20)
	ax.set_ylabel(r"$y_v$",size=20)
	ax.set_zlabel(r"$h_d$",size=20)
	# ax.set_title(r"${\rmTrajectories\ with\ a\ common\ origin}$",size=25)

	ax.zaxis.set_major_locator(LinearLocator(10))
	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
	m = cm.ScalarMappable(cmap=cm.coolwarm)
	m.set_array(o)
	cbar = plt.colorbar(m, shrink=0.5, aspect=5, norm=mplcolors.Normalize(vmin=-pi, vmax=pi))
	cbar.set_alpha(0.7)
	cbar.set_label(r"${\rm Angle\ of\ initial\ velocity}$",size=20)

	# cbar = mpl.colorbar.ColorbarBase(ax, cmap=cm,
 #                       norm=mplcolors.Normalize(vmin=-0.5, vmax=1.5))
	cbar.set_clim(-pi, pi)
	# fig.colorbar(surf, shrink=0.5, aspect=5)

	plt.show()



if __name__=="__main__":
	plot_constant_initial_velocity()