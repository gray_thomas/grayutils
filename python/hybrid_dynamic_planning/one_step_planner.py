# Gray Thomas
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import numpy as np
import random as rand
import math

class State:
    def __init__(self,x,xd,zmp,zmpH):
        self.x=x
        self.xd=xd
        self.zmp=zmp
        self.zmp_height=zmp_height

class Model:
    def __init__(self):
        pass
    def get_xdd(self,state):
        pass

class Simulator:
    def __init__(self, model):
        print "init"
    def get(self):
        pass

class One_Step_Planner:
    def __init__(self,x,xd,zmp,dt):
        self.g = 9.8 # meters per second per second
        self.h = 0.9 # meters
        self.ha= 0.1 # meters
        self.hw= 1.0/0.4 # 1/meters
        self.x = x
        self.xd = xd
        self.zmp = zmp
        self.dt = dt
        self.max_step=0.4
        self.min_zmp = -1
        self.max_zmp = 1
        self.goal_vel = -0.4
        self.relative_goal_time=0.3
    def euler_integrate(self, steps):
        list_of_phase_points=[]
        list_of_phase_points.append((self.x, self.xd))
        for i in range(1,steps):
            (x,xd)=list_of_phase_points[i-1]
            xn=x+self.dt*xd
            xdd=self.get_xdd(x,xd)
            xdn=xd+self.dt*xdd
            list_of_phase_points.append((xn,xdn))
        return list_of_phase_points
    def noisy_euler_integrate(self, x, xd, zmp, time):
        self.x=x
        self.xd=xd 
        self.zmp=zmp 
        steps=int(time/self.dt)
        list_of_phase_points=[]
        list_of_phase_points.append((self.x, self.xd))
        for i in range(1,steps):
            (x,xd)=list_of_phase_points[i-1]
            xn=x+self.dt*xd
            xdd=self.get_xdd(x,xd)
            xdn=xd+self.dt*xdd
            list_of_phase_points.append((xn,xdn))
        return list_of_phase_points
    def scipy_integrate(self, steps):
        sol=odeint(self.get_diff_eq(),np.array([self.x,self.xd]),np.linspace(0,steps*self.dt,steps))
        return sol
    def get_diff_eq(self):
        func=lambda y, t: np.array([y[1],self.xdd(y[0],y[1])])
        return func
    def get_xdd_flat(self):
        return self.g/self.h*(x-self.zmp)
    def get_xdd(self,x,xd):
        h=self.get_height(x)
        h_rate=self.get_height_prime(x)
        h_accel=self.get_height_double_prime(x)
        delta=x-self.zmp
        numerator=h_accel*xd*xd+self.g
        denominator=h-h_rate*delta
        xdd=numerator*delta/denominator
        return xdd# xdd = (h"(x)*xd*xd + g) (x-zmp) / (h(x)-h'(x)*(x-zmp))
    def get_height(self,x):
        return self.h+math.sin(x*self.hw)*self.ha
    def get_height_prime(self,x):
        return math.cos(x*self.hw)*self.ha*self.hw
    def get_height_double_prime(self,x):
        return -math.sin(x*self.hw)*self.ha*self.hw*self.hw
    
    def plan(self,x, xd, stance_zmp, time_to_land, goal_time, goal_velocity):
        self.x=x 
        self.xd=xd
        self.zmp=stance_zmp
        first_step=self.euler_integrate(int(time_to_land/0.01))
        (xf,xdf)=first_step[-1]
        self.x=xf
        self.xd=xdf
        self.goal_vel = goal_velocity
        self.relative_goal_time=goal_time-time_to_land # account for the landing time
        self.max_zmp=self.max_step+self.x
        self.min_zmp=self.x-self.max_step
        second_step=self.bisect_zmp_space_2()#choice of bisection function hacked in here
        return first_step, second_step, self.zmp
    
    def bisect_zmp_space(self):
        self.zmp=self.max_zmp*0.5+self.min_zmp*0.5
        test_path=self.euler_integrate(int(self.relative_goal_time/self.dt))
        (xf,xdf)=test_path[-1]
        if abs(xdf-self.goal_vel)<1e-3:
            return test_path
        else:
            if xdf>self.goal_vel:
                self.min_zmp=self.zmp
            else:
                self.max_zmp=self.zmp
            return self.bisect_zmp_space()
    def bisect_zmp_space_2(self):
        self.zmp=self.max_zmp*0.5+self.min_zmp*0.5
        test_path=self.euler_integrate(int(self.relative_goal_time/self.dt))
        (xf,xdf)=test_path[-1]
        vel_error=xdf-self.goal_vel
        pos_error=xf-0
        error=vel_error*0.9+pos_error*0.2
        if abs(error)<1e-3:
            return test_path
        else:
            if error>0:
                self.min_zmp=self.zmp
            else:
                self.max_zmp=self.zmp
            return self.bisect_zmp_space_2()
def plot_phase_portrait(ips, color,width):
    (xs,xds)=zip(*ips)
    plt.plot(xs,xds,color, linewidth=width)


def demo_plot():
    planner=One_Step_Planner(x=0,xd=0.1,zmp=0.0,dt=0.01)
    planner.xd=0.1
    integrated_phase_space=planner.euler_integrate(200)
    ips2= planner.scipy_integrate(200)
    (xs, xds)=zip(*integrated_phase_space)
    plt.plot(xs,xds)
    plt.plot(ips2[:,0],ips2[:,1])
    plt.plot(planner.zmp,0,'x')
    plt.axis([-0.5,0.5,-1,1])
    
def options_plot(x=0,xd=0.2,num=10,time=0.3):
    zmps=np.linspace(-1+x,1+x,num)
    time_text = plt.gca().text(0.05, 0.9, 'time = %0.2f'% time, transform=plt.gca().transAxes)
    end_points=[]
    for zmp in zmps:
        planner=One_Step_Planner(x=x,xd=xd,zmp=zmp,dt=0.001)
        trajectory=planner.euler_integrate(int(time/0.001))
        end_points.append(trajectory[-1])
        plot_phase_portrait(trajectory,'',3)
    print end_points
    plot_phase_portrait(end_points,'',5)
#     plt.plot([end_points[0][0],end_points[-1][0]],[end_points[0][1],end_points[-1][1]])
    plt.xlabel("$x$",size=25)
    plt.ylabel("$\dotx$",size=25)

#           <-.     
#        (  o  ) <- positive odd       
#         `-> 
# z   A       
# A   |
# |   F->   ->x
# a note on physics: 
# xdd m = x_grf
# zdd m = z_grf - g
# odd I = x_grf * z - z_grf * x

# we set z = h(x)
# zd = h'(x) * xd
# zdd = h'(x) * xdd  +  h''(x) * xd^2 = z_grf/m - g/m
# odd = 0 = x_grf * z - z_grf * (x-zmp)
# x_grf*z=z_grf*(x-zmp)
# (h'(x)*xdd + h"(x) xd xd+g) * m = z_grf
# (h'(x)*xdd + h"(x) xd xd+g) * m * (x-zmp) = x_grf * z
# (h'(x)*xdd + h"(x) xd xd+g) * m * (x-zmp) = xdd * m * h(x)
# (h'(x)*xdd + h"(x) xd xd+g) * (x-zmp) = xdd * h(x)
# xdd (h(x)-h'(x)*(x-zmp)) = (h"(x) xd xd +g) (x-zmp)
# xdd = (h"(x)*xd*xd + g) (x-zmp) / (h(x)-h'(x)*(x-zmp))
# sign agrees with direction while (h"(x)*xd*xd + g > 0) and ( h(x)-h'(x))*(x-zmp) >0)
# d(xdd)/d(zmp) = ((h"(x)*xd*xd + g)/ ((h(x)-h'(x)*x)+h'(x)*zmp))) x
# -((h"(x)*xd*xd + g) / (h(x)-h'(x)*(x-zmp))) zmp
# kinematically reachable while sqrt(h(x)*h(x)+(x-zmp)*(x-zmp))<max_leg
    
def robustness_plot(steps_to_take):
    planner=One_Step_Planner(x=0,xd=0.1,zmp=0.0,dt=0.01)
    simulator=One_Step_Planner(x=0,xd=0.1,zmp=0.0,dt=0.01)
    x=0
    xd=0.01
    zmp=0
    time_1=0.2
    period=0.5
    zmp_noise=0.03
    xd_noise=0.1
    xd_bias=rand.triangular(-0.01,0.01)
    planner_goal_vel=0.2
    
    for i in range(0,steps_to_take):
        step1_planned, step2_planned, zmp_planned=planner.plan(x,xd,zmp,time_1,period+time_1,planner_goal_vel)
        
        step1_sim=simulator.noisy_euler_integrate(x,xd,zmp,time_1)
        x_at_transition,xd_before_transition=step1_sim[-1]
        xd_after_transition=xd_before_transition+rand.triangular(-xd_noise,xd_noise)+xd_bias
        actual_zmp=zmp_planned+rand.triangular(-zmp_noise,zmp_noise)        
        step2_sim=simulator.noisy_euler_integrate(x_at_transition,xd_after_transition,actual_zmp,period-time_1)
        
        x,xd=step2_sim[-1]
        zmp=actual_zmp
        planner_goal_vel*=-1
        
        plot_phase_portrait(step1_planned,'b',3)
        plot_phase_portrait(step2_planned,'b',3)
        plt.plot(zmp_planned,0,'bo',linewidth=3)
        plt.plot(actual_zmp,0,'rx',linewidth=3)
        plt.plot([zmp_planned,actual_zmp],[0,0],'r',linewidth=1)
        plt.plot([x_at_transition,x_at_transition],[xd_before_transition,xd_after_transition],'r',linewidth=1)
        plot_phase_portrait(step1_sim,'m',2)
        plot_phase_portrait(step2_sim,'r',2)
    
    plt.xlabel(r"$x$",size=25)
    plt.ylabel(r"$\dot x$",size=25)
    
# def ball_plot(x0,xd0):
#     dt=0.01
#     x=x0
#     while(x>0)
    
def main():
    print "hello world"
    plt.close('all')
    robustness_plot(20)
    plt.figure()
    options_plot(num=25)
    plt.figure()
    options_plot(num=25,time=0.4)
    plt.figure()
    options_plot(num=100,time=0.5)
    plt.figure()
    options_plot(num=100,time=0.55)
    plt.draw()
    plt.show()
    
if __name__ == "__main__":
    main()
    
    