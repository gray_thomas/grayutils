import numpy as np
import random 
from math import sqrt
class State(object):
    def __init__(self, x, y):
        self.x,self.y = x, y

def random_state():
    return State(np.random.random(),np.random.random())

def distance(a,b):
    """ distance metric for states """
    return sqrt(pow(a.x-b.x,2)+pow(a.y-b.y,2))



class RRT(object):
    def __init__(self, root_state, iterations):
        self.root_state=root_state
        self.states=[root_state]
        self.edges=[]
        for i in range(0,iterations):
            xrand=random_state()
            xnear=self.nearest_neighbor(xrand)
            xnew=self.try_to_reach(xnear,xrand)
            self.edges.append(Edge(xnear,xnew))
            self.states.append(xnew)
    def try_to_reach(self, start, goal):
        dx=goal.x-start.x
        dy=goal.y-start.y
        max_dist=0.1
        dist=sqrt(dx*dx+dy*dy)
        if dist>max_dist:
            dx*=0.1/dist
            dy*=0.1/dist
        new_state=State(start.x+dx,start.y+dy)
        return new_state
    def nearest_neighbor(self,state):
        """ brute force search """
        min_distance=-1
        nearest_neighbor=self.root_state
        for test in self.states:
            dis=distance(state,test)
            if min_distance==-1 or dis<min_distance:
                min_distance=dis
                nearest_neighbor=test
        return nearest_neighbor

    def plot(ax):
        for edge in self.edges:
            plot([edge.a.x,edge.b.x],[edge.a.y,edge.b.y])


class Edge(object):
    def __init__(self,a,b):
        self.a,self.b=a,b

def extend(rrt, state):
    pass



def build_rrt(init):
    pass

if __name__=="__main__":
        import numpy as np
        import matplotlib
        matplotlib.use("Agg")
        import matplotlib.pyplot as plt
        import matplotlib.animation as manimation

        FFMpegWriter = manimation.writers['ffmpeg']
        metadata = dict(title='Movie Test', artist='Matplotlib',
                comment='Movie support!')
        writer = FFMpegWriter(fps=10, metadata=metadata)

        fig = plt.figure(figsize=(4,4))
        ax=fig.add_subplot(111)
        plt.xlim(-0, 1)
        plt.ylim(-0, 1)

        num=1000
        rrt=RRT(State(0.25,0.25),num)
        frames=100
        q=int(num/frames)

        cmap= plt.get_cmap('rainbow')
        print dir(cmap)
        print cmap(0.2)
        with writer.saving(fig, "rrt_demo.mp4", 100):
            i=0
            for edge in rrt.edges:
                ax.plot([edge.a.x,edge.b.x],[edge.a.y,edge.b.y],color=cmap(0.5*sqrt((1.0*i)/num)))
                if i%q==0:
                    writer.grab_frame() 
                i+=1


    

