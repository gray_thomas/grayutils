from numpy import sin, cos, pi, array
import numpy as np
import scipy.integrate as integrate
from mpl_toolkits.mplot3d import Axes3D
from math import sqrt, pow, atan2, pi, floor
from PlatformModel import Platform, FuzzyPushOffFunction, NFuzzyPushOffFunction
from TrajectorySpaceDefs import *
import random

class SimCalculator:
    def __init__(self):
        self.i=0
        self.G = g # acceleration due to gravity, in m/s^2
        self.M = 20.0 # Kg
        self.ys = 1.0 # height setpoint in meters
        self.os = 0.0 # pitch setpoint in radians
        self.T0 = 0.0 # seconds
        self.Tf = 1.0 # seconds
        self.dt = 0.01 # seconds
        self.footstep = 0.0 # position in meters
        self.X0 = np.array([
                -0.5, # x_com
                0.8, # y_com
                1.5, # xd_com
                1.0, # yd_com
            ])
        self.max_leg_length=1.5
        self.min_leg_length=0.25
        self.platform=Platform(-0.5,-0.1,0.5,0.1,0.4,0.5,
            [self.min_leg_length,self.max_leg_length])
        # self.lookup=FuzzyPushOffFunction(1.0,1.0,1.0,600.0)
        self.lookup=NFuzzyPushOffFunction([1.0,1.0,1.0],900.0)
        self.sim_notifies=[]
        # self.init()

    def calculate_single_support_force(self, X):
        x, y, xd, yd = unpack_state_space(X)
        x_f = self.platform.foot[0]
        y_f = self.platform.foot[1]
        x_r = x-x_f
        y_r = y-y_f
        angle = atan2(y_r,x_r)
        leg_length = sqrt(pow(x_r,2)+pow(y_r,2))
        platform_angle=self.platform.theta
        relative_angle = angle_wrap(angle-platform_angle-0.5*pi)
        limit_angle=atan2(self.platform.mu,1.0)
        friction_cone_percentage=relative_angle/limit_angle

        F_leg=self.lookup.lookup(friction_cone_percentage)
        F_x = x_r / leg_length * F_leg
        F_y = y_r / leg_length * F_leg

        inContact = not (
            leg_length>self.max_leg_length or 
            leg_length<self.min_leg_length or 
            friction_cone_percentage>1 or 
            friction_cone_percentage<-1)

        if (not inContact):
            F_x=0
            F_y=0
        return F_x, F_y, inContact

    def derivs(self, X, t):
        x, y, xd, yd = unpack_state_space(X)

        F_x, F_y, inContact = self.calculate_single_support_force(X)
        
        xdd = (1.0/self.M) * F_x # Newton's equations of motion
        ydd = (1.0/self.M) * F_y - self.G # Newton's equations of motion, with gravity acceleration
        dXdt = pack_state_space(xd,yd,xdd,ydd)

        return dXdt

    def add_sim_notify(self,notify):
        self.sim_notifies.append(notify)

    def update_parameters(self):
        N=20
        n_z=int(floor(random.random()*10))
        values=[pow(random.random(),4) for n in range(0,N)]
        for i in range(0,n_z):
            values[i]=0.0
        values.reverse()
        self.lookup.values=values
        # self.lookup.c=random.random()
        # self.lookup.b=random.random()
        # self.lookup.a=random.random()

    def simulate(self): 
        self.t = np.arange(self.T0, self.Tf, self.dt)
        self.X = self.X0[:]
        self.data = integrate.odeint(self.derivs, self.X, self.t)
        for notify in self.sim_notifies:
            notify()
        self.data_row_labels=["x","y","xd","yd"]
        self.i=len(self.t)

    def increment_time(self):
        self.i+=1
        if self.i>=len(self.t):
            self.update_parameters()
            self.simulate()
            self.i=0
        return self.i