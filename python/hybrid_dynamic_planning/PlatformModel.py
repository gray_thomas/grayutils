from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection
from mpl_toolkits.mplot3d import Axes3D
from math import sqrt, pow, atan2, pi, floor, ceil
from TrajectorySpaceDefs import *

class FuzzyPushOffFunction:
    def __init__(self, a,b,c,sup):
        self.a=a
        self.b=b
        self.c=c
        self.sup=sup
    def lookup(self, s):
        basis_c=s if s>0 else 0
        basis_b=1-abs(s)
        basis_a=-s if s<0 else 0
        return self.sup*(self.a*basis_a+self.b*basis_b+self.c*basis_c)

class NFuzzyPushOffFunction:
    def __init__(self, values, sup):
        self.values=values
        self.sup=sup
    def lookup(self, s):
        # print self.values
        if s>1:
            return self.sup*self.values[len(self.values)-1]
        if s<-1:
            return self.sup*self.values[0]
        sn=(s+1)*0.5*(len(self.values)-1)
        sn_l=floor(sn)
        sn_h=ceil(sn)
        # print sn_l,sn_h, len(self.values)
        if (sn_l == sn_h):
            return self.sup*self.values[int(sn_l)]
        return self.sup*(self.values[int(sn_l)]*(sn_h-sn)+self.values[int(sn_h)]*(sn-sn_l))

if __name__=="__main__":
    f=NFuzzyPushOffFunction([0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0],10.0)
    print f.lookup(-2.0)
    print f.lookup(-1.0)
    print f.lookup(0.0)
    print f.lookup(0.801)
    print f.lookup(0.90)
    print f.lookup(0.999)
    print f.lookup(1.0)
    print f.lookup(20.0)

class Platform:
    def __init__(self,x0,y0,x1,y1,mu,foot_loc,leg_lims):
        self.x0=x0
        self.y0=y0
        self.x1=x1
        self.y1=y1
        self.l=sqrt(pow(x1-x0,2)+pow(y1-y0,2))
        self.theta=atan2((y1-y0),(x1-x0))
        self.mu=mu
        self.foot_loc=foot_loc
        self.foot=self.update_foot_point()
        self.leg_lims=leg_lims

    def update_foot_point(self):
        x_f=self.x0*(1-self.foot_loc)+self.x1*self.foot_loc
        y_f=self.y0*(1-self.foot_loc)+self.y1*self.foot_loc
        return [x_f,y_f]

    def init(s,axis):
        axis.plot([s.x0,s.x1],[s.y0,s.y1],'k',linewidth=4)
        limit_angle=atan2(s.mu,1.0)
        s.regime=mpatches.Wedge(s.foot, s.leg_lims[1],
            to_deg(s.theta+0.5*pi-limit_angle),
            to_deg(s.theta+0.5*pi+limit_angle),
            width=s.leg_lims[1]-s.leg_lims[0],
            alpha=0.2)
        axis.add_patch(s.regime)
        return []
    def update(self,data):
        return []