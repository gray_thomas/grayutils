import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
import matplotlib.patches

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.axes(xlim=(-2, 2), ylim=(-2, 2))
plt.grid(True)
# patch_one = matplotlib.patches.Circle((0, 0), 1)
patch_one = matplotlib.patches.Wedge((0,0), 1, -10, 10)

# initialization function: plot the background of each frame
def init():
    patch_one.radius = 1
    ax.add_patch(patch_one)
    return patch_one,

# animation function.  This is called sequentially
def animate(i):
    patch_one.r -=i*0.0001
    patch_one._recompute_path()
    return patch_one,

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=200, interval=20, blit=True)

plt.show()