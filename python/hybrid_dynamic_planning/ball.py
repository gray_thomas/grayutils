print "Hello World"
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
gg=9.81
TT = 0.003
ddz=-gg

dz0=10
z0=3

z=z0
dz=dz0
time=0
list_z=[z0]
list_dz=[dz0]

while z>0:
	z += dz * TT
	dz += ddz * TT
	list_z.append(z)
	list_dz.append(dz)
	time+=TT
print time
fig = plt.figure()
ax = fig.add_subplot(111, autoscale_on=False, xlim=(-7, 7), ylim=(-2, 10))
ax.grid()
plt.xlabel("$x$",size=20)
plt.ylabel("$z$",size=20)
time_template = 'time = %.3fs'
time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)
line, = ax.plot([], [], 'o', lw=8, ms=20)
ax.plot([0],[z0],'x',lw=8)
ax.plot([-2,2],[0,0],lw=2)
def init():
    line.set_data([], [])
    time_text.set_text('')
    return line, time_text

def animate(i):
    thisx = [0]
    thisy = [list_z[i]]

    line.set_data(thisx, thisy)
    time_text.set_text(time_template%(i*TT))
    return line, time_text

ani = animation.FuncAnimation(fig, animate, np.arange(1, len(list_z)),
    interval=5, blit=True, init_func=init, repeat_delay=500)
plt.show()