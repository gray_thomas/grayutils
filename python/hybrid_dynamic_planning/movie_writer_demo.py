# This example uses a MovieWriter directly to grab individual frames and
# write them to a file. This avoids any event loop integration, but has
# the advantage of working with even the Agg backend. This is not recommended
# for use in an interactive setting.
# -*- noplot -*-

import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation

FFMpegWriter = manimation.writers['ffmpeg']
metadata = dict(title='Movie Test', artist='Matplotlib',
        comment='Movie support!')
writer = FFMpegWriter(fps=60, metadata=metadata)

fig = plt.figure()
l, = plt.plot([], [], 'k-o')

plt.xlim(-5, 5)
plt.ylim(-5, 5)

x0,y0,x1,y1 = 0, 0,0 ,0

with writer.saving(fig, "writer_test.mp4", 100):
    for i in range(600):
        x1 += 0.05 * np.random.randn()
        y1 += 0.05 * np.random.randn()
        x0 += 0.1*x1
        y0 += 0.1*y1
        l.set_data(x0, y0)
        writer.grab_frame()