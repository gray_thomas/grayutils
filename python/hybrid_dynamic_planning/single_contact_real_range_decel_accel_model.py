import matplotlib.pyplot as plt 
import numpy as np 
from mpl_toolkits.mplot3d import Axes3D
from math import pi, cos, sin, atan, sqrt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.colors as mplcolors
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np

# xvs,yvs,hds,tvs = elementwise_transform(xs,ys,xds,yds)
g=9.8
def elementwise_transform(xs,ys,xds,yds):
	xvs=xs+(1.0/g)*xds*yds
	yvs=ys+(0.5/g)*yds*yds
	hds=(1.0/g)*xds*np.abs(xds)
	tvs=-yds/g
	return xvs,yvs,hds,tvs

def plot_constant_initial_velocity():
	print "hello world"

	omax=pi*0.5+atan(0.3)
	omin=pi*0.5-atan(0.3)
	rmin=0.25
	rmax=1.0

	os=np.linspace(omin,omax,20)
	rs = np.linspace(rmin,rmax,10)
	hs= np.linspace(rmin,rmax,3)
	fig = plt.figure(facecolor="white")
	ax = fig.add_subplot(1,1,1, projection="3d")
	for h in hs:
		for r in rs:
			xs=r*np.cos(os)
			ys=r*np.sin(os)
			xds=-sqrt(g/h)*xs
			yds=-sqrt(g/h)*(ys-h*np.ones(np.size(ys)))
			xvs,yvs,hds,tvs = elementwise_transform(xs,ys,xds,yds)
			ax.plot(xvs,yvs,hds,lw=3)

			xvs = r*np.cos(os)+r*r*np.sin(os)*np.cos(os)/h-r*np.ones(np.size(os))
			yvs = r*r*np.sin(os)*np.sin(os)/h+h/2*np.ones(np.size(os))
			qvs = -(r*r/h)*np.cos(os)*np.abs(np.cos(os))
			ax.plot(xvs,yvs,qvs,lw=1)
		for o in os:
			xvs = rs*np.cos(o)+rs*rs*np.sin(o)*np.cos(o)/h-rs
			yvs = rs*rs*np.sin(o)*np.sin(o)/h+h/2*np.ones(np.size(rs))
			qvs = -rs*rs*np.cos(o)*np.abs(np.cos(o))/h
			ax.plot(xvs,yvs,qvs,lw=2)
	ax.plot([0],[0],[0],'k+',ms=5)
	ax.plot([0],[0],[0],'ko',ms=3)
	ax.plot(rmin*np.cos(os),rmin*np.sin(os),np.zeros(np.size(os)),'k')
	ax.plot(rmax*np.cos(os),rmax*np.sin(os),np.zeros(np.size(os)),'k')
	ax.set_xlabel(r"$x_v$",size=20)
	ax.set_ylabel(r"$y_v$",size=20)
	ax.set_zlabel(r"$h_d$",size=20)
	# ax.set_title(r"${\rmTrajectories\ with\ a\ common\ origin}$",size=25)


	fig = plt.figure(facecolor="white")
	ax = fig.add_subplot(111, projection='3d')

	o = np.linspace(omin, omax, 100)
	r = np.linspace(rmin, rmax, 40)

	x =  np.outer(r*r, np.sin(o)*np.cos(o))+np.outer(r, np.cos(o))
	y =  np.outer(r*r, np.sin(o)*np.sin(o))+np.outer(r, np.sin(o))
	z =  np.outer(r*r, np.cos(o)*np.abs(np.cos(o)))
	theta = np.outer(np.ones(np.size(r)),0.5*np.ones(np.size(o))+o*(0.5/np.pi))
	surf=ax.plot_surface(x, y, z, norm=theta, rstride=1, cstride=1, facecolors=cm.coolwarm(theta),
        linewidth=0, antialiased=False, alpha=0.7)
	ax.set_xlabel(r"$x_v$",size=20)
	ax.set_ylabel(r"$y_v$",size=20)
	ax.set_zlabel(r"$h_d$",size=20)
	# ax.set_title(r"${\rmTrajectories\ with\ a\ common\ origin}$",size=25)

	ax.zaxis.set_major_locator(LinearLocator(10))
	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
	m = cm.ScalarMappable(cmap=cm.coolwarm)
	m.set_array(o)
	cbar = plt.colorbar(m, shrink=0.5, aspect=5, norm=mplcolors.Normalize(vmin=-pi, vmax=pi))
	cbar.set_alpha(0.7)
	cbar.set_label(r"${\rm Angle\ of\ initial\ velocity}$",size=20)

	# cbar = mpl.colorbar.ColorbarBase(ax, cmap=cm,
 #                       norm=mplcolors.Normalize(vmin=-0.5, vmax=1.5))
	cbar.set_clim(-pi, pi)
	# fig.colorbar(surf, shrink=0.5, aspect=5)

	plt.show()



if __name__=="__main__":
	plot_constant_initial_velocity()