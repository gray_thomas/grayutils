'''
Created on Nov 11, 2013

@author: Gray Thomas
'''
import scipy.integrate as integrate
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d

trainingDataFileName = "../../../data/pendubot_run1.dat"
output_filename = "../../../data/example_out.dat"

def loadOutputFileAsTest():
    outputTestData = np.loadtxt(output_filename, usecols=range(4))
    testT = np.unwrap(outputTestData[:, 0])
    testO = np.unwrap(outputTestData[:, 1])
    testW = np.unwrap(outputTestData[:, 2])
    testA = np.unwrap(outputTestData[:, 3])
    return testT, testO, testW, testA

def loadTrainingFile():
    skiprowsnum = 500
    data = np.loadtxt(trainingDataFileName, usecols=range(5), skiprows=skiprowsnum)
    rows = data.shape[0]
    time_data = np.linspace(0, rows * 0.01, rows)
    theta1_data = np.unwrap(data[:, 1])
    theta2_data = np.unwrap(data[:, 2])
    theta1 = interp1d(time_data, theta1_data, kind='cubic')  # why does this change the integration result?
    theta2 = interp1d(time_data, theta2_data, kind='cubic')

    return theta1, rows

def fetchCommonMathFunctions():
    pi = np.pi
    sqrt = np.sqrt
    cos = np.cos
    sin = np.sin
    sign = np.sign
    exp = np.math.exp
    def sig(x):
        return 1 / (1 + exp(-x))
    return pi, sqrt, cos, sin, sign, exp, sig

def getGoodGuessParameters():
    beta = 0.001 
    epsilon = 1.33 
    xi = 0.013 
    tau = 0.101
    return beta, epsilon, xi, tau

def generateODEFunc():
    pi, sqrt, cos, sin, sign, exp, sig = fetchCommonMathFunctions()
    beta, epsilon, xi, tau = getGoodGuessParameters()
    def dX_dt(x, time):
        theta, omega = x
        alpha = (-sin(theta) - beta * tau * omega -
                 sign(omega) * xi * (cos(theta) + tau * tau * omega * omega)) / ((1 + epsilon) * tau * tau)
        return [omega, alpha]
    return dX_dt

def main():

    theta1, rows = loadTrainingFile()
    testT, testO, testW, testA = loadOutputFileAsTest()
    
    initialTime = 0
    finalTime = rows * 0.01
    timeSteps = 20000

    time = np.linspace(initialTime, finalTime, timeSteps)
    xinit = [-0.35, 3.3]
    x = integrate.odeint(generateODEFunc(), xinit, time)
    theta, omega = x.T
    
    dX_dt = generateODEFunc()
    alpha = range (0, len(theta))
    for i in range (0, len(theta)):
        n, alpha[i] = dX_dt([theta[i], omega[i]], 0)
    output_data = np.array([time, theta, omega, alpha])
    # this is the simple version of our training data where we read theta and omega and are judged on our prediction of alpha.
    print "output_data size =%d by %d" % (output_data.shape[0], output_data.shape[1])
    np.savetxt(output_filename, output_data.T)

    fig, ax = plt.subplots()
    ax.plot(time, theta, time, theta1(time))  # , testT, testO)#, testT, testW, testT, testA)
    
    plt.grid(True)
    plt.show()
    return
if __name__ == '__main__':
    main()
