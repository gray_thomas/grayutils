
'''
Created on Dec 12, 2013

@author: Gray Thomas
'''
import scipy.integrate as integrate
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import fileinput
from scipy.interpolate import interp1d


baseFolder = "/home/hcrl/wk/grayutils/BackPropogationHomeworkSolver/"
dataFolder = baseFolder + "PendulumNetDemo/src/main/data/"
testDataFolder = baseFolder + "src/main/data/"
windowsMainFolder= "C:/users/gray/wk/grayutils/"
trainingDataFileName = ("/home/hcrl/wk/grayutils/BackPropogationHomeworkSolver/PendulumNetDemo/" + 
            "src/main/data/trainFullNetwork_NoMem_patterns.trn")




def loadTrainingFile(trainingDataFileName):
    # Open a file
    fo = open(trainingDataFileName, "r+")
    print "Name of the file: ", fo.name

    values = [int(s) for s in (fo.readline()).split(" ")]
    inputSize = values[0]
    outputSize = values[1]
    print "Size is: %d by %d" % (values[0], values[1])
    fo.close()
    
    np.loadtxt(trainingDataFileName)
    data = np.loadtxt(trainingDataFileName, skiprows=1, usecols=range(inputSize + outputSize))
    inputs = data[:, range(inputSize)]
    outputs = data[:, range(inputSize, inputSize + outputSize)]
    

    return inputs, outputs, inputSize, outputSize

def fetchCommonMathFunctions():
    pi = np.pi
    sqrt = np.sqrt
    cos = np.cos
    sin = np.sin
    sign = np.sign
    exp = np.math.exp
    def sig(x):
        return 1 / (1 + exp(-x))
    return pi, sqrt, cos, sin, sign, exp, sig

def getGoodGuessParameters():
    beta = 0.001 
    epsilon = 1.33 
    xi = 0.013 
    tau = 0.101
    return beta, epsilon, xi, tau

def generateODEFunc():
    pi, sqrt, cos, sin, sign, exp, sig = fetchCommonMathFunctions()
    beta, epsilon, xi, tau = getGoodGuessParameters()
    def dX_dt(x, time):
        theta, omega = x
        alpha = (-sin(theta) - beta * tau * omega - 
                 sign(omega) * xi * (cos(theta) + tau * tau * omega * omega)) / ((1 + epsilon) * tau * tau)
        return [omega, alpha]
    return dX_dt

def plotTrainingData():
    
    (inputs, outputs) = loadTrainingFile(trainingDataFileName)
    plt.figure()
    dt = 0.01
    times = [dt * i for i in range(0, inputs.shape[0])]
    plt.subplot(3, 1, 1)
    plt.plot(times, outputs[:, 2], "b")
    plt.plot(times, inputs[:, 0], "k")
    plt.subplot(3, 1, 2)
    plt.plot(times, inputs[:, 1], "g")
    plt.subplot(3, 1, 3)
    plt.plot(times, inputs[:, 2], "k")
    plt.show()
def plotInterimSolution(epoch, name, inSize, memSize, outSize, dt):
    plt.rcParams['legend.loc'] = 'best'
    font0 = matplotlib.font_manager.FontProperties()
    font0.set_family("serif")
    matplotlib.rcParams.update({'font.size': 12, 'font.family':'serif', "text.usetex " : True})
    (inputs, outputs, inputSize, outputsSize) = loadTrainingFile( name + str(epoch) + ".ioDat")
    memory = outputs[:, np.array(range(0, memSize))]

    print outSize, outputsSize
    networkOutputs = outputs[:, range(memSize, memSize + outSize, 1)]
    desiredOutputs = outputs[:, range(memSize + outSize, memSize + 2 * outSize)]
    unJudgedSize = outputsSize - 2 - (memSize + 2 * outSize)
    print "unjudged output number", unJudgedSize
    extraOutputs = outputs[:, range(memSize + 2 * outSize, outputsSize - 2)]
    pointError = outputs[:, [-2]]
    cumulativeError = outputs[:, [-1]]
    times = [dt * i for i in range(0, inputs.shape[0])]
    numSubPlots = outSize + unJudgedSize + 1
    print numSubPlots
    fig, axs = plt.subplots(numSubPlots, 1, sharex=True, sharey=False)
    axs[0].set_title(r"One Dimensional Linear System, 100% Error in A matrix estimate")
    for outputIndex in range(0, outSize):
        plt.setp(axs[outputIndex].plot(times, networkOutputs[:, outputIndex], "bo-"), linewidth=3.0)
        plt.setp(axs[outputIndex].plot(times, desiredOutputs[:, outputIndex], "kx-"), linewidth=3.0)
        axs[outputIndex].yaxis.set_major_formatter(plt.FormatStrFormatter('%0.1e'))
        axs[outputIndex].legend(["Judged Output " + str(outputIndex + 1), "Desired Value"])
    for unJudgedIndex in range(0, unJudgedSize):
        plt.setp(axs[outSize + unJudgedIndex].plot(times, extraOutputs[:, unJudgedIndex], "b"), linewidth=3.0)
        axs[outSize + unJudgedIndex].yaxis.set_major_formatter(plt.FormatStrFormatter('%0.1e'))
        axs[outSize + unJudgedIndex].legend(["Unjudged Output " + str(unJudgedIndex + 1)])
    plt.setp(axs[outSize + unJudgedSize].plot(times, pointError[:, :], "k"), linewidth=3.0)
#     axs[outSize + unJudgedSize].ylabel("Point Error")
#     fig.xlabel("Time")
    axs[outSize + unJudgedSize].xaxis.set_major_formatter(plt.FormatStrFormatter('%0.2e'))
    axs[outSize + unJudgedSize].yaxis.set_major_formatter(plt.FormatStrFormatter('%0.1e'))
    axs[outSize + unJudgedSize].legend(["Point Error"])
    
    plt.show()
def main():
    plotInterimSolution(0, windowsMainFolder+"networkUtils/src/main/data/linearA", inSize=1, memSize=1, outSize=1, dt=1.0)
#     plotInterimSolution(0, dataFolder +"testError_TwoMem_dataRuns", inSize=1, memSize=2, outSize=1, dt=0.001)
#     for i in range(0,999,100):
#         plotInterimSolution(i, dataFolder +"trainFullNetwork_TwoMem_dataRuns", inSize=1, memSize=2, outSize=1, dt=0.001)
#         plotInterimSolution(i, dataFolder +"trainFullNetwork_OneMem_dataRuns", inSize=2, memSize=1, outSize=2, dt=0.001)
#     for i in range(0,20,2):
#         plotInterimSolution(i, testDataFolder +"testBackPropSimpleLinearCaseConvergesForGuess", inSize=1, memSize=1, outSize=1, dt=0.001)
    return
if __name__ == '__main__':
    main()
