'''
Created on Jan 11, 2014

@author: Gray Thomas
'''
import scipy.integrate as integrate
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import fileinput
from scipy.interpolate import interp1d


def plotFloatingMemoryGraph(fileName,title="",x_label="Time",y_label={}):
    plt.rcParams['legend.loc'] = 'best'
    font0 = matplotlib.font_manager.FontProperties()
    font0.set_family("serif")
    matplotlib.rcParams.update({'font.size': 12, 'font.family':'serif', "text.usetex " : True})
    data =  np.loadtxt(fileName)
    assert data.shape[1]%2==0
    numPlots=data.shape[1]/2
    numData=data.shape[0]

    t = range(0,numData)
    fig, axs = plt.subplots(numPlots, 1, sharex=True, sharey=False)
    axs[0].set_title(title)
    for plot in range(0, numPlots):
        plt.setp(axs[plot].plot(t, data[:,plot*2], "ko-"), linewidth=3.0)
        plt.setp(axs[plot].plot(t, data[:,plot*2+1], "bx-"), linewidth=3.0)
        label="output "+str(plot)
        if y_label.has_key(plot):
            label=y_label[plot]

        axs[plot].set_ylabel(label)
    axs[-1].set_xlabel(x_label)
    xa = axs[-1].get_xaxis()
    xa.set_major_locator(plt.MaxNLocator(integer=True))

    plt.show()
    return 
def plotFloatingMemoryGraphWErrorCompare(fileName,title="",x_label="Time",y_label={}):
    plt.rcParams['legend.loc'] = 'best'
    font0 = matplotlib.font_manager.FontProperties()
    font0.set_family("serif")
    matplotlib.rcParams.update({'font.size': 12, 'font.family':'serif', "text.usetex " : True})
    data =  np.loadtxt(fileName)
    assert data.shape[1]%2==0
    numPlots=data.shape[1]/2
    numData=data.shape[0]

    t = range(0,numData)
    fig, axs = plt.subplots(2*numPlots, 1, sharex=True, sharey=False)
    axs[0].set_title(title)
    xfmt = plt.ScalarFormatter()
    xfmt.set_scientific(True)
    xfmt.set_powerlimits((-2,2))
    for plot in range(0, numPlots):
        plt.setp(axs[2*plot].plot(t, data[:,plot*2], "ko-"), linewidth=3.0)
        plt.setp(axs[2*plot].plot(t, data[:,plot*2+1], "bx-"), linewidth=3.0)
        plt.setp(axs[2*plot+1].plot(t, data[:,plot*2+1]-data[:,plot*2], "bo"), linewidth=3.0)
        label="output "+str(plot)
        if y_label.has_key(plot):
            label=y_label[plot]

        axs[2*plot].set_ylabel(label)
        axs[2*plot].get_yaxis().set_major_formatter(xfmt)
        axs[2*plot+1].set_ylabel("$\delta$"+label)
        axs[2*plot+1].get_yaxis().set_major_formatter(xfmt)
    axs[-1].set_xlabel(x_label)
    xa = axs[-1].get_xaxis()
    xa.set_major_locator(plt.MaxNLocator(integer=True))

    plt.show()
    return axs[2*plot+1].get_yaxis()
    
gnuMainFolder = "/home/hcrl/wk/grayutils/"
windowsMainFolder= "C:/users/gray/wk/grayutils/"
mainFolder=gnuMainFolder
dataFolder =  mainFolder+"NetworkUtils/src/main/data/"
def main():
    for i in range (0,2):
        plotFloatingMemoryGraphWErrorCompare(dataFolder+"linearB"+str(i)+".dat",title="Floating Memory Epoch "+str(i),x_label="Discrete Time Index",y_label={0:"$x_0$",1:"$y_0$"})
    for i in range (0,16,2):
        plotFloatingMemoryGraphWErrorCompare(dataFolder+"linearA"+str(i)+".dat",title="Floating Memory Epoch "+str(i),x_label="Discrete Time Index",y_label={0:"$x_0$",1:"$y_0$"})
    return
if __name__ == '__main__':
    main()
