
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
from dyNetYamlReader import DyNetPlotterYamlData


class TrainingFileManager(object):
    def __init__(self, folderLocation):
        self.folderLocation = folderLocation + "/"
        self.plotsToPlot=[]
        self.memoizedYaml={}
    def loadRawMatrix(self, name):
        data = np.loadtxt(self.folderLocation + name)
        return data
    def saveRawMatrix(self, name, data):
        data = np.savetxt(self.folderLocation + name, data)
        return data
    def saveIODat(self, name, data, numInputs, numOutputs):
        data = np.savetxt(self.folderLocation + name, data, header=str(numInputs)+" "+str(numOutputs))
        return data
    def loadIODat(self, name):
        data = np.loadtxt(self.folderLocation + name, skiprows=1)
        return data
    def saveFig(self, fig, plotName):
        self.plotsToPlot.append((fig,plotName))
    def saveFigsAfterPltShow(self):
        for fig, plotName in self.plotsToPlot:
            with PdfPages(self.folderLocation+plotName) as pp:
                pp.savefig(fig,bbox_inches='tight')
    def loadDyNetYaml(self, name):
        if not self.memoizedYaml.has_key(name):
            self.memoizedYaml[name]=DyNetPlotterYamlData(self.folderLocation+name)
        return self.memoizedYaml[name]
