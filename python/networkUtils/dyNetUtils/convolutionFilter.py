import numpy as np
import matplotlib.pyplot as plt
import math
class ConvolutionFilter(object):
    def __init__(self, sigmaInSeconds, dt):
        self.sigma = sigmaInSeconds / dt  # seconds to tics
        self.tmax = 12 * sigmaInSeconds
        self.nConv = int(self.tmax / dt)
        self.ts = np.linspace(-self.tmax, self.tmax, self.nConv * 2 + 1)
        self.gauss = lambda t : math.exp(-0.5 * t ** 2 / sigmaInSeconds ** 2)
        unitConst = np.array([1 for t in self.ts])
        unitRamp = np.array([-t for t in self.ts])
        unitParabola = np.array([t * t * 0.5 for t in self.ts])
        self.convolutionKernel = np.array([self.gauss(t) for t in self.ts])
        self.convolutionKernel = self.convolutionKernel / np.sum(self.convolutionKernel)
        self.diffKernel = np.array([-t * self.gauss(t) for t in self.ts])
        self.diffKernel = self.diffKernel / np.dot(self.diffKernel, unitRamp)
        self.ddifKernel = np.array([t ** 2 * self.gauss(t) for t in self.ts])
#         self.ddifKernel = self.ddifKernel * (1.0 / np.dot(self.ddifKernel, np.array([0.5 * t * t for t in self.ts])))
#         self.ddifKernel = self.ddifKernel - self.convolutionKernel
        # make a linear combination of the diff and the convolution filter which satisifies both unit convolution with unit parabola
        # and zero convolution with constants
        # ddif = a conv + c ddif;
        # [[conv * parab, ddif * parab];[conv * const, ddif * const]] [a;b] = [1;0]
        mat = np.array([[np.dot(self.convolutionKernel, unitParabola), np.dot(self.ddifKernel, unitParabola)],
                      [np.dot(self.convolutionKernel, unitConst), np.dot(self.ddifKernel, unitConst)]])
        des = np.array([[1.0], [0.0]])
        sol = np.linalg.solve(mat, des)
        self.ddifKernel = sol[0] * self.convolutionKernel + sol[1] * self.ddifKernel
    def filt(self, dat):
        return np.convolve(dat, self.convolutionKernel, mode='same')
    def diff(self, dat):
        return np.convolve(dat, self.diffKernel, mode='same')
    def ddif(self, dat):
        return np.convolve(dat, self.ddifKernel, mode='same')
    def test(self):
        unitConst = np.array([1 for t in self.ts])
        unitRamp = np.array([-t for t in self.ts])
        unitParabola = np.array([t * t * 0.5 for t in self.ts])
        print np.dot(self.convolutionKernel, unitConst)
        print np.dot(self.diffKernel, unitRamp)
        print np.dot(self.ddifKernel, unitParabola)
        print np.dot(self.convolutionKernel, unitRamp)
        print np.dot(self.diffKernel, unitParabola)
        print np.dot(self.diffKernel, unitConst)
        print np.dot(self.ddifKernel, unitConst)
        print np.dot(self.ddifKernel, unitRamp)
        print self.gauss(-1)
    def plot(self):
        plt.plot(self.convolutionKernel)
        plt.plot(self.diffKernel)
        plt.plot(self.ddifKernel)
if __name__ == '__main__':
    filt = ConvolutionFilter(1.0, 0.01)
    filt.test()
    filt.plot()
    plt.show()
