from yaml import load, dump
import matplotlib.pyplot as plt
import numpy as np
import io

class DyNetPlotterUpdate(object):
    def __init__(self, yamlObj):
#         print yamlObj
        self.trust_parameter = yamlObj['trust parameter']
        self.error_change = yamlObj['change in error']
        self.parameter_delta = np.array(yamlObj['parameter delta'])        
    
class DyNetPlotterEpoch(object):
    def __init__(self, yamlObj):
        ['weights used', 'measurement prediction', 'error', 'state prediction', 'update', 'states used']
        self.parameters_used = np.array(yamlObj['parameters used'])
        self.states_used = np.array(yamlObj['states used'])
        self.measurements_predicted = np.array(yamlObj['measurement prediction'])
        self.states_predicted = np.array(yamlObj['state prediction'])
        self.error = yamlObj['error']
        self.update = DyNetPlotterUpdate(yamlObj['update'])
        yamlObj['states used']
class DyNetNames(object):
    def __init__(self, yamlObject):
        self.inputs = yamlObject['inputs']
        self.measurements = yamlObject['measurements']
        self.parameters = yamlObject['parameters']
        self.states = yamlObject['states']
class DyNetPlotterYamlData(object):
    def __init__(self, fileName):
        stream = io.FileIO(fileName)
        yml = load(stream)
        stream.close()
        baseObject = yml['DyNetTrainer DataDump']
        self.names = DyNetNames(baseObject)
        self.inputs = np.array(baseObject['trainingData']['inputs'])
        self.measurements = np.array(baseObject['trainingData']['measurements'])
        self.epochData = []
        print "loading dyNet yaml",fileName, " with length", len(baseObject['epochData'])
        for i in range(0, len(baseObject['epochData'])):
            self.epochData.append(DyNetPlotterEpoch(baseObject['epochData'][i]))



def func():
    plt.hold(True)
    plt.plot(dat.epochData[5].measurements_predicted, '#360022', linewidth=2)
    plt.plot(dat.epochData[4].measurements_predicted, '#451d00', linewidth=2)
    plt.plot(dat.epochData[3].measurements_predicted, '#430005', linewidth=2)
    plt.plot(dat.epochData[2].measurements_predicted, '#b40071', linewidth=2)
    plt.plot(dat.epochData[1].measurements_predicted, '#e76200', linewidth=2)
    plt.plot(dat.epochData[0].measurements_predicted, '#e10010', linewidth=2)