#circular_buffer_class.py

class CircularBuffer:

	def __init__(self):
		self.Init()

	def Init(self):
		self.BUF = []
		self.HEAD = 0
		self.TAIL = 0
		self.IsEMPTY = True
		self.SIZE=4
		for ii in range(self.SIZE):
			self.BUF.append(-1)

	def ReInit(self):
		self.HEAD = 0
		self.TAIL = 0
		self.IsEMPTY = True
		for ii in range(self.SIZE):
			self.BUF[ii]=-1

	def Add(self, datum):
		self.BUF[self.HEAD] = datum
		if self.HEAD == self.SIZE - 1 :
			self.HEAD = 0
			self.TAIL = 0 # unnecessary
		else:
			if self.HEAD == self.TAIL and not self.IsEMPTY:
				self.HEAD = self.HEAD + 1
				self.TAIL = self.HEAD
			else:
				self.HEAD = self.HEAD + 1

		self.IsEMPTY = False

	def GetIndex(self,tail_index):
		return self.BUF[(tail_index+self.TAIL)%self.SIZE]

	def GetNewest(self):
		return self.BUF[self.HEAD-1]

	def GetOldest(self):
		return self.BUF[self.TAIL]

	def PrintMe(self):
		print "buf: [%d,%d,%s]--[%+4.2f, %+4.2f, %+4.2f, %+4.2f] " % (
			self.HEAD, self.TAIL, str(self.IsEMPTY), self.BUF[0], self.BUF[1], self.BUF[2], self.BUF[3])
