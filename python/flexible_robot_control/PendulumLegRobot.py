'''
Created on Aug 15, 2014

@author: hcrl
'''
import numpy as np
from gu.math import transform

class Vec2d(object):
    def __init__(self, x,y):
        self.x=x
        self.y=y
class Transform2D(object):
    def __init__(self, x,y,o):
        self.x=x
        self.y=y
        self.o=o
    def transform(self,vec):
        ret=Vec2d(math.cos(o)*vec.x-math.sin(o)*vec.y,math.cos(o)*vec.y+math.sin(o)*vec.x)
        return ret

class Inertia2D(object):
    def __init__(self, m, mx,my,jzz):
        self.m=m
        self.mx=mx
        self.my=my
        self.jzz=jzz
class PendulumLegRobotParams(object):
    '''
    Simulation Object representing a robot with one leg, Three rigid bodies, Series Elastic Actuators, and a point foot.
    The parameters include:
    -- mass of body
    -- rotational inertia of body
    -- 
    '''

class PendulumLegRobotState(object):
    '''
    Simulation Object representing the state of a robot with one leg, Three rigid bodies, Series Elastic Actuators, and a point foot.
    The state includes:
    -- position and velocity of a floating base, 
    -- position and velocity for two rotary joints, 
    -- spring deflection and spring deflection rate for two SEAs
    '''
    
    def __init__(self, base):
        self.position={"base":Transform2D(transform.x,transform.y,transform.o),"hip":0,"knee":0}
        self.velocity={"base":Transform2D(0,0,0),"hip":0,"knee":0}
        self.torque={"Hip":0,"Knee":0}
        self.torqueRate={"Hip":0,"Knee":0}
        
def main():
    params=Transform2D(0,0,0)
    robot=PendulumLegRobotState(params)
    print robot        

if __name__ == '__main__':
    main()