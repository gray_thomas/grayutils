from math import atan, sin, cos
import numpy as np


class Contact(object):

    """
    Contact objects represent all information specific to a contact.

    They are used to setup the convex optimization. Distinct from
    contact records. All details of the optimization should be encapsulated
    within the contact, so that we can switch out models easily.
    This archetecture must also allow the inevitable optimization of
    the active set.
    """

    def __init__(self, platform, position, appendage, max_leg=1.0, min_phi=10.0, max_force=300.0, name="anonymous contact"):
        self.platform = platform
        self.position = position
        self.appendage = appendage
        self.max_leg = max_leg
        self.x_f = platform.x0 * (1 - position) + platform.x1 * position
        self.y_f = platform.y0 * (1 - position) + platform.y1 * position
        self.F_x = 0
        self.F_y = 0
        self.M = 0
        self.error = False
        self.min_phi = min_phi
        self.max_force = max_force
        self.num_basis = 2
        self.num_inequalities = 3
        self.name = name

    def set_force(self, F_x, F_y):
        self.F_x = F_x
        self.F_y = F_y
        self.M = F_y * self.x_f - F_x * self.y_f

    def basis(self):
        """return the basis vectors parameterizing point contact."""
        friction_angle = atan(self.platform.mu)
        foot_x, foot_y, platform_theta = self.x_f, self.y_f, self.platform.theta
        fx1 = -sin(platform_theta + friction_angle)
        fy1 = cos(platform_theta + friction_angle)
        fx2 = -sin(platform_theta - friction_angle)
        fy2 = cos(platform_theta - friction_angle)
        return [fx1, fy1, fy1 * foot_x - fx1 * foot_y], [fx2, fy2, fy2 * foot_x - fx2 * foot_y]

    def inequality_constraints(self):
        """return (A, b) where Ax <= b, x is opt variables."""
        A = np.array([[-1.0, 0.0], [0.0, -1.0], [1.0, 1.0]])
        b = np.array([[-self.min_phi], [-self.min_phi], [self.max_force]])
        return A, b
