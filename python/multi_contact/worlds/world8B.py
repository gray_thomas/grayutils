from math import tan, pi
from PlatformWithChecks import PlatformWithChecks
from multi_contact.math.ArcLength2Spline import ArcLength2Spline
from Contact import Contact
from MultiContactStance import MultiContactStance
import numpy as np
from collections import namedtuple
from math import tan, atan

name = "8B"
SimpleRobot = namedtuple("SimpleRobot", ["mass", "gravity_accel", "max_speed"])
robot = SimpleRobot(mass=20.0, gravity_accel=-9.81, max_speed=1.0)


xlims = [-0.10, 2.50]
ylims = [0.45, 2.25]
xlims_chibi = [-0.3, 2.4]
ylims_chibi = [0.35, 2.15]

_friction_mu = tan(30.0 * pi / 180.0)
fmu = lambda deg: tan(deg * pi / 180.0)

platforms = []
contacts = []
contact_data = []

step1 = PlatformWithChecks(0.00, 0.60, 0.25, 0.50, fmu(30.0), name="0-step1")
step1c1 = Contact(step1, 0.9,  "l_foot",
               min_phi=0.0, max_force=400.0, max_leg=1.2, name="lf:a")
platforms.append(step1)
contacts.append(step1c1)

step2 = PlatformWithChecks(0.75, 0.50, 1.00, 0.50, fmu(30.0), name="1-step2"),
platforms.append(step2)

ciel1 = PlatformWithChecks(0.75, 2.00, 0.25, 2.00, fmu(30.0), name="2-ciel 1"),
platforms.append(ciel1)

step3 = PlatformWithChecks(1.00, 0.90, 1.50, 0.80, fmu(30.0), name="3-step3"),
platforms.append(step3)

ciel2 = PlatformWithChecks(1.75, 2.10, 1.00, 1.90, fmu(30.0), name="4-ciel 2"),
platforms.append(ciel2)

step4 = PlatformWithChecks(1.75, 0.30, 2.25, 0.70, fmu(30.0), name="5-step4"),
platforms.append(step4)


_C_b = Contact(platforms[1], 0.1,  "r_foot",
               min_phi=0.0, max_force=400.0, max_leg=1.2, name="rf:b")
_C_c = Contact(platforms[2], 0.5,  "l_hand",
               min_phi=0.0, max_force=100.0, max_leg=1.2, name="lh:c")
_C_d = Contact(platforms[1], 0.95, "l_foot",
               min_phi=0.0, max_force=400.0, max_leg=1.2, name="lf:d")
_C_e = Contact(platforms[3], 0.05, "r_foot",
               min_phi=0.0, max_force=400.0, max_leg=1.2, name="step high")
_C_f = Contact(platforms[4], 0.02, "l_hand",
               min_phi=0.0, max_force=400.0, max_leg=1.2, name="lf:d")
_C_f = Contact(platforms[4], 0.02, "l_hand",
               min_phi=0.0, max_force=100.0, max_leg=1.2, name="lf:d")
_C_g = Contact(platforms[5], 0.8, "l_foot",
               min_phi=0.0, max_force=400.0, max_leg=1.2, name="lf:d")

__hy = [1.6, 1.62,  1.3, 1.2]
__hx = [0.25, 0.65,  1.6, 1.9]
__tan = [tan(0.25), tan(-0.20), tan(-0.6), tan(0.2)]
print atan(0.24)
__height_points = [[__hx[0], __hy[0]]]
for i in range(1, len(__hy)):
    t1, t3 = __tan[i - 1], __tan[i]
    x1, x3 = __hx[i - 1], __hx[i]
    y1, y3 = __hy[i - 1], __hy[i]
    # <x2,y2> = <x1,y1> + (x2-x1)<1,t1>
    # <x2,y2> = <x3,y3> + (x2-x3)<1,t3>
    # y2 = y1 + (x2-x1)t1 = y3 + (x2-x3)t3
    # y1-y3+x2t1-x1t1=x2t3-x3t3
    x2 = (y1 - y3 + x3 * t3 - x1 * t1) / (t3 - t1)
    y2 = y1 + (x2-x1)*t1
    __height_points.append([x2, y2])
    __height_points.append([x3, y3])
_height_points = np.array(__height_points)
print _height_points

x0 = (_height_points[0, 0], _height_points[0, 1], 0.0, 0.0)

com_path = ArcLength2Spline(_height_points)


contact_data = [
    MultiContactStance.from_com_path_and_contact_limits(
        com_path, [_C_a], "a"),
    MultiContactStance.from_com_path_and_contact_limits(
        com_path, [_C_b], "b"),
    MultiContactStance.from_com_path_and_contact_limits(
        com_path, [_C_c], "c"),
    MultiContactStance.from_com_path_and_contact_limits(
        com_path, [_C_d], "d")
]



# com_path.get_arc_length_functions()
# [Z~0:0.9999999439888865,D~0.9999999439888865:1.278454205459266,Z~1.278454205459266:2]
