from math import tan, pi
from multi_contact.PlatformWithChecks import PlatformWithChecks
from multi_contact.Bezier2Splines import Bezier2Splines
from multi_contact.Contact import Contact
import numpy as np
from collections import namedtuple

SimpleRobot = namedtuple("SimpleRobot", ["mass", "gravity_accel", "max_speed"])
robot = SimpleRobot(mass=20.0, gravity_accel=-9.81, max_speed=2.0)

stair_num = 5

stairs = []
for i in range(0, stair_num):
    stairs.append(PlatformWithChecks(0.25 + 0.5 * i, 0.25 + 0.25 * i, 0.75 +
                                     0.5 * i, 0.25 + 0.25 * i, tan(30.0 * pi / 180.0), name="s" + repr(i)))
    # PlatformWithChecks(0.00, 0.50, 0.25, 0.50, _friction_mu, name="p0"),
    # PlatformWithChecks(0.75, 0.50, 1.00, 0.50, _friction_mu, name="p1"),
    # PlatformWithChecks(0.75, 2.00, 0.25, 2.00, _friction_mu, name="p2"),

lips = []
for i in range(0, stair_num):
    lips.append(PlatformWithChecks(0.25 + 0.5 * i, 0.25 * i,
                                   0.25 + 0.5 * i, 0.25 + 0.25 * i, 0.01, name="l" + repr(i)))

rails = []
for i in range(0, stair_num):
    rails.append(PlatformWithChecks(0.25 + 0.5 * i, 1.00 + 0.25 * i, 0.75 +
                                    0.5 * i, 1.25 + 0.25 * i, tan(75.0 * pi / 180.0), name="s" + repr(i)))

platforms = stairs + lips + rails

stair_contacts = []
for i in range(0, stair_num):
    stair_contacts.append(Contact(
        stairs[i], 0.5,  "l_foot" if i % 2 == 0 else "r_foot", min_phi=0.0, max_force=400.0, max_leg=1.3, name="s" + repr(i)))
hand_contacts = []
for i in range(0, stair_num):
    hand_contacts.append(Contact(
        rails[i], 0.5, "r_hand" if i % 2 == 0 else "l_hand", min_phi=0.0, max_force=100.0, max_leg=0.35, name="r" + repr(i)))

contact_data = []
for i in range(0, stair_num):
    contact_data.append([hand_contacts[i], stair_contacts[i]])


_height_point_list = [[0.5, 1.25]]
for i in range(0, stair_num - 1):
    _height_point_list.append([0.625 + 0.5 * i, 1.25 + 0.25 * i])
    _height_point_list.append([0.75 + 0.5 * i, 1.375 + 0.25 * i])
    _height_point_list.append([0.875 + 0.5 * i, 1.50 + 0.25 * i])
    _height_point_list.append([1.00 + 0.5 * i, 1.50 + 0.25 * i])
_height_points = np.array(_height_point_list)

xlims = [0.0, 3.0]
ylims = [0.0, 3.0]

x0 = (_height_points[0, 0], _height_points[0, 1], 0.0, 0.0)
com_path = Bezier2Splines(_height_points)

#[I~0.0:0.6325126414205542,
# Z~0.6325126414205542:0.9999999265282425,
# D~0.9999999265282425:1.2056519434983628,
# I~1.2056519434983628:2.6325126150368856,
# Z~2.6325126150368856:2.9999999816137937,
# D~2.9999999816137937:3.205651962694372,
# I~3.205651962694372:4.6325126155648135,
# Z~4.6325126155648135:4.9999999522292535,
# D~4.9999999522292535:5.205651968073555,
# I~5.205651968073555:6.632512622646167,
# Z~6.632512622646167:6.999999951062769,
# D~6.999999951062769:7.205651943951146,
# I~7.205651943951146:8]
