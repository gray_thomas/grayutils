""" World with banister, not ceiling."""

from math import tan, pi
from PlatformWithChecks import PlatformWithChecks
from multi_contact.math.ArcLength2Spline import ArcLength2Spline
from Contact import Contact
from MultiContactStance import MultiContactStance
import numpy as np
from collections import namedtuple

name = "10A"
SimpleRobot = namedtuple("SimpleRobot", ["mass", "gravity_accel", "max_speed"])
robot = SimpleRobot(mass=20.0, gravity_accel=-9.81, max_speed=2.0)

_friction_mu = tan(30.0 * pi / 180.0)
_leg_lims = [0.2, 1.5]
platforms = [
    PlatformWithChecks(0.00, 0.50, 0.25, 0.48, _friction_mu, name="p0"),
    PlatformWithChecks(0.75, 0.73, 1.00, 0.75, _friction_mu, name="p1"),
    PlatformWithChecks(1.50, 1.00, 1.75, 0.98, _friction_mu, name="p1"),
    PlatformWithChecks(2.25, 2.00, 0.00, 2.00, _friction_mu, name="p2"),
]

_C_a = Contact(platforms[0], 0.5,  "l_foot",
               min_phi=0.0, max_force=400.0, max_leg=1.2, name="lf:a")
_C_b = Contact(platforms[1], 0.5,  "r_foot",
               min_phi=0.0, max_force=400.0, max_leg=1.2, name="rf:b")
_C_c1 = Contact(platforms[3], 0.67,  "l_hand",
                min_phi=0.0, max_force=80.0, max_leg=0.9, name="lh:c")
_C_c2 = Contact(platforms[3], 0.34,  "l_hand",
                min_phi=0.0, max_force=80.0, max_leg=0.9, name="lh:c")
_C_c3 = Contact(platforms[3], 0.01,  "l_hand",
                min_phi=0.0, max_force=80.0, max_leg=0.9, name="lh:c")
_C_d = Contact(platforms[2], 0.5, "l_foot",
               min_phi=0.0, max_force=400.0, max_leg=1.2, name="lf:d")


_height_points = np.array([
    [0.25, 1.60],
    [1.00, 1.70],
    [1.75, 2.10]
])

x0 = (_height_points[0, 0], _height_points[0, 1], 1.0, 0.0)
xlims = [-0.10, 2.10]
ylims = [0.45, 2.75]
xlims_chibi = [-0.3, 2.4]
ylims_chibi = [0.35, 2.65]

com_path = ArcLength2Spline(_height_points)

contact_data = [
    MultiContactStance.from_com_path_and_contact_limits(
        com_path, [_C_a, _C_c1], ""),
    MultiContactStance.from_xi_limits(
        com_path, [[0.1, None]], [_C_b, _C_c1], ""),
    MultiContactStance.from_xi_limits(
        com_path, [[0.1, None]], [_C_b, _C_c2], ""),
    MultiContactStance.from_xi_limits(
        com_path, [[1.0, None]], [_C_c2, _C_d], "stop pose"),
    MultiContactStance.from_xi_limits(
        com_path, [[1.0, None]], [_C_c3, _C_d], "stop pose")
]

# com_path.get_arc_length_functions()
# [Z~0:0.9999999439888865,D~0.9999999439888865:1.278454205459266,Z~1.278454205459266:2]
