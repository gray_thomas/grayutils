from math import sqrt, atan2, pi
import matplotlib.patches as mpatches

def to_deg(ang):
    return 180.0*ang/pi

class PlatformWithChecks:
    def __init__(self,x0,y0,x1,y1,mu,name="anonymous platform", linewidth=4):
        self.x0=x0
        self.y0=y0
        self.x1=x1
        self.y1=y1
        self.l=sqrt(pow(x1-x0,2)+pow(y1-y0,2))
        self.theta=atan2((y1-y0),(x1-x0))
        self.mu=mu
        self.name=name
        self.linewidth=linewidth

    def init(s,axis):
        axis.plot([s.x0,s.x1],[s.y0,s.y1],'k',linewidth=s.linewidth)
        limit_angle=atan2(s.mu,1.0)

        return []
    def update(self,data):
        return []