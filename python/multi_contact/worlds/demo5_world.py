from math import tan, pi
from multi_contact.PlatformWithChecks import PlatformWithChecks
from multi_contact.Bezier2Splines import Bezier2Splines
from multi_contact.Contact import Contact
import numpy as np
from collections import namedtuple

SimpleRobot = namedtuple("SimpleRobot", ["mass", "gravity_accel", "max_speed"])
robot = SimpleRobot(mass=20.0, gravity_accel=-9.81, max_speed=2.0)

_friction_mu = tan(20.0 * pi / 180.0)
_leg_lims = [0.2, 1.5]
platforms = [
    PlatformWithChecks(0.00, 0.50, 0.25, 0.50, _friction_mu, name="p0"),
    PlatformWithChecks(0.75, 0.50, 1.00, 0.50, _friction_mu, name="p1"),
    PlatformWithChecks(0.75, 2.00, 0.25, 2.00, _friction_mu, name="p2"),
]
x0=(0.25, 1.6, 0.0, 0.0)
_C_a = Contact(platforms[0], 0.9, "l_foot", max_leg=1.2, name="lf:a")
_C_b = Contact(platforms[1], 0.1, "r_foot", max_leg=1.2, name="rf:b")
_C_c = Contact(platforms[2], 0.5, "l_hand", max_leg=1.2, name="lh:c")
contact_data = [
    [_C_a, _C_c],
    [_C_b, _C_c]]

_height_points = np.array([
    [0.25, 1.60],
    [0.50, 1.55],
    [0.75, 1.60]
])
com_path = Bezier2Splines(_height_points)
