"""
The world of demo 1.

This is setup so that the world can be reused by other demos.
"""

from PlatformWithChecks import PlatformWithChecks
from graphics.MultiContactRobotGraphic import MultiContactRobotGraphic
from PlanFollowingController import PlanFollowingController
from MultiContactGraphics import MultiContactSimulationAnimator
from MultiContactNumericSimulator import MultiContactNumericSimulator
from Contact import Contact
from Bezier2Splines import Bezier2Splines
import numpy as np
from collections import namedtuple


def contacts(platforms, robot_graphic):
    """create a series of platform contacts and times to switch."""
    pl_a = Contact(platforms[0], 0.1, "l_foot")
    pl_b = Contact(platforms[2], 0.9, "r_hand")
    pl_c = Contact(platforms[0], 0.7, "r_foot")
    pl_d = Contact(platforms[1], 0.3, "l_foot")
    pl_e = Contact(platforms[2], 0.5, "l_hand")
    contact_data = [
        [pl_a, pl_b],
        [pl_a, pl_b, pl_c],
        [pl_b, pl_c],
        [pl_c],
        [pl_c, pl_d],
        [pl_d],
        [pl_d, pl_e]]
    return contact_data

def height_surface():
    height_points = np.array([
        [0.25, 1.0],
        [1.0, 1.25],
        [1.5, 1.0],
        [2.0, 0.75],
        [2.25, 1.25],
        [2.5, 1.75],
        [3.0, 1.5],
    ])
    height_surface = Bezier2Splines(height_points)
    return height_surface

robot_params={
    "mass": 20.0,
    "gravity": -9.81,
    "min_phi": 10.0,
    "max_force": 300.0
}
Robot = namedtuple(
        "Robot", ("mass", "gravity_accel", "max_leg_length"))
robot = Robot(20, -9.81, 1.2)

def platforms():
    friction_mu = 0.4
    platforms = [
        PlatformWithChecks(0.25, 0.25, 1.00, 0.50, friction_mu),
        PlatformWithChecks(1.50, 0.50, 2.25, 0.50, friction_mu),
        PlatformWithChecks(1.25, 1.75, 0.50, 1.50, friction_mu),
        PlatformWithChecks(2.50, 1.00, 3.25, 1.00, friction_mu),
        PlatformWithChecks(2.00, 2.00, 2.00, 1.50, friction_mu),
    ]
    return platforms

x_lims = [0.0, 4.0]
y_lims = [0.0, 2.0]


