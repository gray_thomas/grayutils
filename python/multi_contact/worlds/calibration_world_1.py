from math import tan, pi
from PlatformWithChecks import PlatformWithChecks
from multi_contact.math.ArcLength2Spline import ArcLength2Spline
from Contact import Contact
from MultiContactStance import MultiContactStance
import numpy as np
from collections import namedtuple
from gu.gutypes import mutabledata
from multi_contact.sim.MultiContactNumericSimulator import SimulationResult
from multi_contact.sim.ContactRecord import ContactRecord

index = 3
# 0 is falling
# 1 is standing
# 2 is moving forward
# 3 is curving up
# 4 curves more


SimpleRobot = namedtuple("SimpleRobot", ["mass", "gravity_accel", "max_speed"])
robot = SimpleRobot(mass=20.0, gravity_accel=-9.81, max_speed=1.0)

_friction_mu = tan(30.0 * pi / 180.0)
platforms = [
    PlatformWithChecks(-0.05, -0.70, 0.05, -0.70,
                       _friction_mu, name="p0", linewidth=8)
]
x0 = (0.0, 0.0, 0.0, 0.0)


if index == 2:
    _height_points = np.array([
        [-0.25, 0.0],
        [0.0, 0.0],
        [0.25, 0.0]
    ])
    com_path = ArcLength2Spline(_height_points)
else:
    _height_points = np.array([
        [-0.25, 0.05],
        [0.0, -0.05],
        [0.25, 0.05]
    ])

    com_path = ArcLength2Spline(_height_points)
xlims = [-0.5, 0.5]
ylims = [-0.75, 0.25]

xi = com_path.max_xi * 0.5
if index == 4:
    xi_dot = 1.25
else:
    xi_dot = 0.75
print "hi"
ydd = com_path.ddx_dxi2(xi)[1][0] * xi_dot * xi_dot



contact_data = [
]
ContactFake = mutabledata("ContactFake", [
    "x_f", "y_f", "F_x", "F_y", 'M', 'error', 'appendage', 'foot'])


sim = mutabledata("FakeSim", [
                  "list_of_contact_record_lists", "data", "list_of_robot_accel", "phase_space_data", 'times'])()
sim.list_of_contact_record_lists = [
    [],
    [],
    [ContactRecord(ContactFake(
        x_f=0, y_f=-0.7, F_x=0, F_y=-robot.mass * robot.gravity_accel, M=0, error=0, appendage="l_foot"))],
    [ContactRecord(ContactFake(
        x_f=0, y_f=-0.7, F_x=0, F_y=-robot.mass * robot.gravity_accel, M=0, error=0, appendage="l_foot"))],
    [ContactRecord(ContactFake(
        x_f=0, y_f=-0.7, F_x=0, F_y=-robot.mass * (robot.gravity_accel - ydd), M=0, error=0, appendage="l_foot"))],
    [ContactRecord(ContactFake(
        x_f=0, y_f=-0.7, F_x=0, F_y=-robot.mass * (robot.gravity_accel - ydd), M=0, error=0, appendage="l_foot"))]
]

print xi_dot, ydd, -ydd/robot.gravity_accel, -robot.mass * (robot.gravity_accel - ydd)
sim.data = np.array(
    [
        [0.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, xi_dot, 0.0],
        [0.0, 0.0, xi_dot, 0.0],
        [0.0, 0.0, xi_dot, 0.0]
    ])
sim.list_of_robot_accel = (
    [
        [0.0, -9.8],
        [0.0, -9.8],
        [0.0, 0.0],
        [0.0, 0.0],
        [0.0, ydd],
        [0.0, ydd]
    ])
sim.phase_space_data = [
    [0.0, 0.0, -9.8],
    [0.0, 0.0, -9.8],
    [0.0, 0.0, -9.8],
    [0.0, 0.0, -9.8],
    [0.0, 0.0, -9.8],
    [0.0, 0.0, -9.8]]
sim.times = [0.0] * 6
sim_res = lambda: SimulationResult(sim)


def main():
    import multi_contact.worlds.calibration_world_1 as world
    from multi_contact.graphics.MultiContactRobotGraphic import MultiContactRobotGraphic
    # with open("../demos/sim_res.pkl", 'r') as fil:
    # sim_res = pkl.load(fil)  # So fast!!
    from multi_contact.sim.MultiContactNumericSimulator import SimulationResult
    from multi_contact.graphics.MultiContactGraphics import MultiContactSimulationAnimator
    import matplotlib.pyplot as plt
    # sim_res = SimulationResult.fall()
    sim_res = world.sim_res()

    animator = MultiContactSimulationAnimator(
        sim_res, xlims=world.xlims, ylims=world.ylims, show_phase_space=False)
    if index >= 1:
        animator.show_platforms(world.platforms)
    if index >= 2:
        animator.show_com_path(world.com_path, num_tangents=0, line_scale=2.0)
    # animator.show_com_path(world.com_path, num_tangents=10)
    animator.animation_plot.add_graphic(
        MultiContactRobotGraphic(world.com_path, line_scale=2.0))
    # animator.phase_space_plot.setup_raw_phase_space()
    # animator.phase_space_plot.setup_phase_space_projection()
    # animator.phase_space_plot.setup_sister_space_projection()
    # animator.phase_space_plot.plot_integration_data(plan)

    # animator.start()
    animator.init()
    animator.init_animation()

    sim_res.i = index
    animator.animate(index)
    animator.fig.savefig("calibration_world_%d.pdf" % index)
    plt.show()

if __name__ == '__main__':
    main()
