import lyapunov as ly
import ly_utils as lu
from multi_contact.math.RegionSet import RegionSet
from multi_contact.math.Bezier2Splines import convert_s
from math import sqrt
import numpy as np 


@lu.named_state_system("SState", ['s'], xname="s_state", tname='path_s')
class KinematicLimitExplorer(object):

    def __init__(self, com_path, stance):

        self.com_path = com_path
        self.state = (0, (0.0,))
        self.stance = stance
        self.events = [self.feasible]
        self.valid_regions = RegionSet()
        lu.initialize_by_guards(self)

    @property
    def xi(self):
        return float(self.com_path.s2xi(self.s))

    def is_feasible(self):
        """If any leg is over extended, then return false."""
        x, y = self.com_path.x(self.xi)
        for contact in self.stance:
            xf, yf = contact.x_f, contact.y_f
            if sqrt(pow(x - xf, 2) + pow(y - yf, 2)) > contact.max_leg:
                return False
        return True

    @lu.Guard
    def feasible(self):
        return lu.ThrowEventIf[self.is_feasible()]

    @feasible.jumper
    def feasible(self):
        self.valid_regions.start_region(self.xi)
        self.events = [self.infeasible, self.end_of_path]
        raise lu.Jump()

    @lu.Guard
    def infeasible(self):
        return lu.ThrowEventIf[not self.is_feasible()]

    @infeasible.jumper
    def infeasible(self):
        self.valid_regions.end_region(self.xi)
        self.events = [self.feasible]
        raise lu.Jump()

    @lu.Guard
    def end_of_path(self):
        return lu.ThrowEventIf[self.xi>=self.com_path.max_xi]

    @end_of_path.jumper
    def end_of_path(self):
        self.valid_regions.end_region(self.com_path.max_xi)
        self.events = []
        raise lu.StopIntegration()

    def compute(self, n=100, printouts=False):
        switch_times = []
        system = self
        # integrate from start to end of com_path, variable step size
        # print dir(ly)
        stepper = ly.euler(system, np.linspace(0,self.com_path.max_s,n))

        for jump, s, state, guard in lu.hybrid_integrate(stepper):
            # print jump, s
            if guard and printouts:
                print guard.name, "at", s, ",", self.xi
        # self._integrate()
        return self.valid_regions

    def __call__(self):
        """Unity."""
        return 1,


class MultiContactStance(list):

    """Better than just a list of contacts."""

    def __init__(self, contact_list, xi_regions, name):
        super(MultiContactStance, self).__init__(contact_list)
        # print "MultiContactStance '%s' valid over" % name, xi_regions
        self.xi_regions = xi_regions
        self.name = name

    @classmethod
    def from_com_path_and_contact_limits(cls, com_path, contact_list, name, n=20):
        xi_regions = KinematicLimitExplorer(com_path, contact_list).compute(n=n)
        return cls(contact_list, xi_regions, name)
    @classmethod
    def from_x_limits(cls, com_path, x_lims, contact_list, name, n=20):
        low_sat = lambda x: x if x != None else com_path.x(0.0)[0]
        high_sat = lambda x: x if x != None else com_path.x(com_path.max_xi)[0]
        x_lims = [[low_sat(low), high_sat(high)] for low, high in x_lims]

        xi_lims = [[float(com_path.find_xi(x)) for x in lims] for lims in x_lims]
        xi_regions = KinematicLimitExplorer(com_path, contact_list).compute(n=n)
        res = RegionSet.conjunction([xi_regions,RegionSet(xi_lims)])
        return cls(contact_list, res, name)
    @classmethod
    def from_xi_limits(cls, com_path, xi_lims, contact_list, name, n=20):
        low_sat = lambda x: x if x != None else 0.0
        high_sat = lambda x: x if x != None else com_path.max_xi
        xi_lims = [[low_sat(low), high_sat(high)] for low, high in xi_lims]

        xi_regions = KinematicLimitExplorer(com_path, contact_list).compute(n=n)
        res = RegionSet.conjunction([xi_regions,RegionSet(xi_lims)])
        return cls(contact_list, res, name)


if __name__ == '__main__':
    import multi_contact.worlds.world8B #
    import multi_contact.worlds.demo8_world as world
    from multi_contact.worlds.demo8_world import _C_a, _C_b, _C_c, _C_d
    print KinematicLimitExplorer(world.com_path, [_C_a]).compute()
    print KinematicLimitExplorer(world.com_path, [_C_b]).compute()
    print KinematicLimitExplorer(world.com_path, [_C_c]).compute()
    print KinematicLimitExplorer(world.com_path, [_C_d]).compute()
    print world.com_path.x(0.0)
    print world.com_path.x(world.com_path.max_xi)
    MultiContactStance.from_com_path_and_contact_limits(
        world.com_path, [_C_b, _C_c, _C_d], "stop pose")
    contact_data = [
        MultiContactStance.from_com_path_and_contact_limits(
            world.com_path, [_C_a, _C_c], "test1"),
        MultiContactStance.from_com_path_and_contact_limits(
            world.com_path, [_C_b, _C_c], "test2"),
        MultiContactStance.from_x_limits(
            world.com_path, [[0.7, None]], [_C_b, _C_d], "test3")
    ]
    # import multi_contact.worlds.world8B as
    # MultiContactStance.from_com_path_and_contact_limits(
    #     world.com_path, [_C_b, _C_c, _C_d], "stop pose")