step = 0.15  # meters.
time = 0.5  # seconds.

bod = 10.61
abd = 2.1656
tgh = 2.66
clf = .20
leg_mass = clf  # Kg.
robot_mass = bod + 2*(abd+tgh+clf)  # Kg.
print "robot mass", robot_mass, "Kg"
print "leg mass", leg_mass, "Kg"
print "momentum", leg_mass * step / time, "Kg m/s"
print "change in CoM velocity", leg_mass / robot_mass * step / time, "m/s"
