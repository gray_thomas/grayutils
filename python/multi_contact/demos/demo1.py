"""
Multicontact simulator.

present a 2D robot changing contacts as it moves left to
right through a virtual multicontact world.
"""

from PlatformWithChecks import PlatformWithChecks
from graphics.MultiContactRobotGraphic import MultiContactRobotGraphic
from PlanFollowingController import PlanFollowingController
from MultiContactGraphics import MultiContactSimulationAnimator
from MultiContactNumericSimulator import MultiContactNumericSimulator
from Contact import Contact
from Bezier2Splines import Bezier2Splines
import numpy as np
import worlds.demo1_world as world

def main():
    """run the demo."""
    platforms = world.platforms()
    height_surface = world.height_surface()
    magic = [0.2, 0.4, 0.5, 0.55, 0.65, 999]
    robot_graphic = MultiContactRobotGraphic()
    contacts = world.contacts(platforms, robot_graphic)
    planner = PlanFollowingController(
        contacts, height_surface, params = world.robot, switch_times=magic, plan_switch_time=1.2)
    sim = MultiContactNumericSimulator(
        robot_graphic, platforms, planner, time=0.9)
    animator = MultiContactSimulationAnimator(
        sim, xlims=world.x_lims, ylims=world.y_lims)
    animator.show_platforms(platforms)
    animator.show_height_surface(height_surface)
    animator.animation_plot.add_graphic(robot_graphic)
    # animator.movie("move_stop.mp4","Simple Multi-Contact Robot Simulation")
    animator.start()


if __name__ == "__main__":
    main()

