"""
Multicontact simulator.

Demo with three contact states. Wide cones. No kinematics.
"""

# result which simulates chatter (makes a video)
# result which shows failure
# animation of the planning process.


from multi_contact.graphics.MultiContactRobotGraphic import MultiContactRobotGraphic
from multi_contact.sim.PlanController import PlanController, NewPlanController
from multi_contact.graphics.MultiContactGraphics import MultiContactSimulationAnimator, PlanAnimator
from multi_contact.sim.MultiContactNumericSimulator import MultiContactNumericSimulator
from multi_contact.planning.MaxSpeedPlanner import MaxSpeedPlanner, CommandEnum
import numpy as np
import matplotlib.pyplot as plt
import cPickle as pkl
from gu.colormaps import *
from enum import Enum
from multi_contact.solvers.SolverListSystem import SolverListSystem
import time as tm

DemoResult=Enum("DemoResult","animation video frames phase_space all_ps time_plot ps_video ps_compare max_speed")
figures_folder = "../../../tex/MultiContact/raw_figs/"
video_folder = "/home/gray/Videos/ThomasSentis2016IROS Video/Video Clips/"

import multi_contact.worlds.world9B as world
print "loaded World %s" % world.name


def main(result=DemoResult.video, re_plan = True, re_sim = True, no_anim=False, time_step=0.002):
    start_time = tm.time()

    if result==DemoResult.max_speed:
        xis = np.linspace(0.0, world.com_path.max_xi, 200)
        solvers = SolverListSystem(world)
        xi_dot_max = world.robot.max_speed
        i=0.00
        total_number = len(world.contact_data)
        for index, solver in enumerate(solvers.solvers):
            dat=[]
            for xi in xis:
                solvers.state = 0.0, (xi, 0.0, 0.0)
                if solver.is_viable(xi):
                    dat.append([solver.min_xi_dot(xi),solver.max_xi_dot(xi)])
                else:
                    dat.append([float('nan'),float('nan')])
            p = index * 1.0 / (total_number - 1)
            color = tuple(
                [o * p + (1 - p) * b for b, o in zip(w2(ut_brown), w2(ut_burnt_orange))])
            line_color = tuple(
                [o * p + (1 - p) * b for b, o in zip(w0(ut_brown), w0(ut_burnt_orange))])
            plt.plot(xis,np.array(dat)+i, color = line_color, lw=4)
            i+=-0.005

        plt.figure()
        for index, solver in enumerate(solvers.solvers):
            dat=[]
            for xi in xis:
                solvers.state = 0.0, (xi, 0.0, 0.0)
                if solver.is_viable(xi):
                    dat.append([solver.min_xi_ddot(xi, 0.0),solver.max_xi_ddot(xi, 0.0)])
                else:
                    dat.append([float('nan'),float('nan')])
            p = index * 1.0 / (total_number - 1)
            color = tuple(
                [o * p + (1 - p) * b for b, o in zip(w2(ut_brown), w2(ut_burnt_orange))])
            line_color = tuple(
                [o * p + (1 - p) * b for b, o in zip(w0(ut_brown), w0(ut_burnt_orange))])
            plt.plot(xis,np.array(dat), color = line_color, lw=4)
            # i+=-0.005
        plt.show()
        return

    """run the demo."""
    preplan_time=tm.time()
    try:
        if re_plan:
            raise IOError
        with open("plan_%s.pkl"%world.name, 'r') as fil:
            plan = pkl.load(fil)
            plan.build_functions()

    except IOError:
        print "Planning!"
        planner = MaxSpeedPlanner(world)
        plan = planner.integrate()
        assert plan != None
        with open("plan_%s.pkl"%world.name, 'w') as fil:
            plan.delete_functions()
            pkl.dump(plan, fil)
            plan.build_functions()

    with open("all_segs_%s.pkl"%world.name, 'w') as fil:
        for seg in planner.result.all_segments_ever:
            seg.delete_functions()
        pkl.dump(planner.result.all_segments_ever, fil)
        for seg in planner.result.all_segments_ever:
            seg.build_functions()
    postplan_time = tm.time()
    print "planning time", postplan_time - preplan_time, "s. Duration of plan", plan.time(plan.xi_max)
    # print [(world.com_path.x(p.xi_min)[0,0], p.command, p.stance_index) for p in plan]
    # for p in plan:
    #     xs = [world.com_path.x(xi)[0,0] for xi in p.xis]
    #     plt.plot(xs, p.xis)
    #     plt.plot(xs, p.xi_dots)
    # # plt.show()
    # exit()
    if result==DemoResult.phase_space:
        phase_fig = plt.figure(figsize=(5,3.5))
        phase_ax = phase_fig.add_subplot(111)
        phase_ax.set_title(r"$\rmPhase\,Space$",fontsize=16)
        phase_ax.set_xlabel(r"$\xi$",fontsize=14)
        phase_ax.set_ylabel(r"$\dot\xi$",fontsize=14)
        # print planner.result.all_segments_ever
        # exit()
        for seg in planner.result.plan:
            # print seg
            # print seg.stance_index
            # print seg.dstate
            xis = np.linspace(seg.xi_min, seg.xi_max, 1000)
            xi_dots = [seg(xi) for xi in xis]
            if seg.command == CommandEnum.max:
                plt.plot(xis[0], xi_dots[0], color=ut_burnt_orange, marker='o', ms=4, mew=1, markeredgecolor='w')
                plt.plot(xis, xi_dots,lw=4,color=ut_burnt_orange, linestyle='-.', dash_capstyle='round', solid_capstyle='round')
                plt.plot(xis[-1], xi_dots[-1], color=ut_burnt_orange, marker='o', ms=10, mew=1, markeredgecolor='w')
            if seg.command == CommandEnum.zero:
                plt.plot(xis[0], xi_dots[0], color=ut_green, marker='o', ms=4, mew=1, markeredgecolor='w')
                plt.plot(xis, xi_dots,lw=2,color=ut_green, linestyle='-', dash_capstyle='butt', solid_capstyle='butt')
                plt.plot(xis, xi_dots,lw=5,color=ut_green, linestyle='--', dash_capstyle='butt', solid_capstyle='butt')
                plt.plot(xis[-1], xi_dots[-1], color=ut_green, marker='o', ms=10,mew=1,  markeredgecolor='w')
            if seg.command == CommandEnum.chatter:
                plt.plot(xis[0], xi_dots[0], color=ut_brown, marker='o', ms=4, mew=1, markeredgecolor='w')
                plt.plot(xis, xi_dots,lw=2,color=ut_brown, linestyle='-', dash_capstyle='butt', solid_capstyle='butt')
                plt.plot(xis, xi_dots,lw=5,color=ut_brown, linestyle='--', dash_capstyle='butt', solid_capstyle='butt')
                plt.plot(xis[-1], xi_dots[-1], color=ut_brown, marker='o', ms=10,mew=1,  markeredgecolor='w')
            if seg.command == CommandEnum.min:
                plt.plot(xis[0], xi_dots[0], color=ut_blue, marker='o', ms=4, mew=1, markeredgecolor='w')
                plt.plot(xis, xi_dots,lw=4,color=ut_blue, solid_capstyle='round')
                plt.plot(xis[-1], xi_dots[-1], color=ut_blue, marker='o', ms=10, mew=1, markeredgecolor='w')
        phase_ax.axis([-0.1,plan.xi_max+0.1,-0.1,world.robot.max_speed+.1])
        phase_fig.tight_layout()
            # plt.plot(seg.call.x, seg.call.y, 'o')
        phase_fig.savefig(figures_folder+"Demo%sMaxPhaseSpace.pdf"%world.name)
        phase_fig.savefig(figures_folder+"Demo%sMaxPhaseSpace.png"%world.name)
        plt.show()
        exit()

    if result==DemoResult.all_ps:
        phase_fig = plt.figure(figsize=(5,3.5))
        phase_ax = phase_fig.add_subplot(111)
        phase_ax.set_title(r"$\rmPhase\,Space$",fontsize=16)
        phase_ax.set_xlabel(r"$\xi$",fontsize=14)
        phase_ax.set_ylabel(r"$\dot\xi$",fontsize=14)
        print planner.result.all_segments_ever
        print "should be %d entries" % len(planner.result.all_segments_ever)
        # exit()
        max_xi = 0.0
        for seg in planner.result.all_segments_ever:
            # print seg
            # print seg.stance_index
            # print seg.dstate
            xis = np.linspace(seg.xi_min, seg.xi_max, 1000)
            max_xi=max(max_xi, seg.xi_max)
            xi_dots = [seg(xi) for xi in xis]
            if seg.command == CommandEnum.max:
                plt.plot(xis[0], xi_dots[0], color=ut_burnt_orange, marker='o', ms=8, mew=0.5, markeredgecolor='w')
                plt.plot(xis, xi_dots,lw=4,color=ut_burnt_orange, linestyle='-.', dash_capstyle='round', solid_capstyle='round')
                plt.plot(xis[-1], xi_dots[-1], color=ut_burnt_orange, marker='o', ms=16, mew=0.5, markeredgecolor='w')
            if seg.command == CommandEnum.search:
                plt.plot(xis[0], xi_dots[0], color=ut_yellow, marker='o', ms=8, mew=0.5, markeredgecolor='w')
                plt.plot(xis, xi_dots,lw=4,color=ut_yellow, linestyle='--', dash_capstyle='round', solid_capstyle='round')
                plt.plot(xis[-1], xi_dots[-1], color=ut_yellow, marker='o', ms=24, mew=0.5, markeredgecolor='w')
            if seg.command == CommandEnum.zero:
                plt.plot(xis[0], xi_dots[0], color=ut_green, marker='o', ms=8, mew=0.5, markeredgecolor='w')
                plt.plot(xis, xi_dots,lw=2,color=ut_green, linestyle='-', dash_capstyle='butt', solid_capstyle='butt')
                plt.plot(xis, xi_dots,lw=5,color=ut_green, linestyle='--', dash_capstyle='butt', solid_capstyle='butt')
                plt.plot(xis[-1], xi_dots[-1], color=ut_green, marker='o', ms=16,mew=0.5,  markeredgecolor='w')
            if seg.command == CommandEnum.chatter:
                plt.plot(xis[0], xi_dots[0], color=ut_brown, marker='o', ms=8, mew=0.5, markeredgecolor='w')
                plt.plot(xis, xi_dots,lw=2,color=ut_brown, linestyle='-', dash_capstyle='butt', solid_capstyle='butt')
                plt.plot(xis, xi_dots,lw=5,color=ut_brown, linestyle='--', dash_capstyle='butt', solid_capstyle='butt')
                plt.plot(xis[-1], xi_dots[-1], color=ut_brown, marker='o', ms=16,mew=0.5,  markeredgecolor='w')
            if seg.command == CommandEnum.min:
                print "min", len(xi_dots)
                plt.plot(xis[0], xi_dots[0], color=ut_blue, marker='o', ms=16, mew=0.5, markeredgecolor='w')
                plt.plot(xis, xi_dots,lw=4,color=ut_blue, solid_capstyle='round')
                plt.plot(xis[-1], xi_dots[-1], color=ut_blue, marker='o', ms=8, mew=0.5, markeredgecolor='w')
        phase_ax.axis([-0.1,max_xi+0.1,-0.1,world.robot.max_speed+.1])
        phase_fig.tight_layout()
            # plt.plot(seg.call.x, seg.call.y, 'o')
        phase_fig.savefig(figures_folder+"Demo%sMaxPhaseSpace.pdf"%world.name)
        phase_fig.savefig(figures_folder+"Demo%sMaxPhaseSpace.png"%world.name)
        plt.show()
        exit()

    elif result==DemoResult.time_plot:
        # plan.delete_functions()
        # plan.build_functions()
        # seg = plan[len(plan)-1]
        # print seg.times
        # print seg.xis
        # print plan[len(plan)-1]
        # exit()
        # for seg in plan:
        #     print seg
        # print plan[3].times[0:2]
        # print plan[4].times[0:2]
        phase_fig = plt.figure(figsize=(5,3.5))
        phase_ax = phase_fig.add_subplot(111)
        phase_ax.set_title(r"$\rmTime\,Trajectory$",fontsize=16)
        phase_ax.set_xlabel(r"$t$",fontsize=14)
        phase_ax.set_ylabel(r"$\xi$",fontsize=14)
        for seg in plan:
            xis = np.linspace(seg.xi_min, seg.xi_max, 1000)
            xi_dots = [seg(xi) for xi in xis]
            times = [seg.time(xi) for xi in xis]
            if seg.command == CommandEnum.max:
                plt.plot(times[0], xis[0], color=ut_burnt_orange, marker='o', ms=4, mew=1, markeredgecolor='w')
                plt.plot(times, xis,lw=4,color=ut_burnt_orange, linestyle='-.', dash_capstyle='round', solid_capstyle='round')
                plt.plot(times[-1], xis[-1], color=ut_burnt_orange, marker='o', ms=10, mew=1, markeredgecolor='w')
            if seg.command == CommandEnum.zero:
                plt.plot(times[0], xis[0], color=ut_green, marker='o', ms=4, mew=1, markeredgecolor='w')
                plt.plot(times, xis,lw=2,color=ut_green, linestyle='-', dash_capstyle='butt', solid_capstyle='butt')
                plt.plot(times, xis,lw=5,color=ut_green, linestyle='--', dash_capstyle='butt', solid_capstyle='butt')
                plt.plot(times[-1], xis[-1], color=ut_green, marker='o', ms=10,mew=1,  markeredgecolor='w')
            if seg.command == CommandEnum.min:
                plt.plot(times[0], xis[0], color=ut_blue, marker='o', ms=4, mew=1, markeredgecolor='w')
                plt.plot(times, xis,lw=4,color=ut_blue, solid_capstyle='round')
                plt.plot(times[-1], xis[-1], color=ut_blue, marker='o', ms=10, mew=1, markeredgecolor='w')
        # phase_ax.axis([-0.1,plan.xi_max+0.1,-0.1,world.robot.max_speed+.1])
        phase_fig.tight_layout()
            # plt.plot(seg.call.x, seg.call.y, 'o')
        phase_fig.savefig(figures_folder+"Demo%sTimePlot.pdf"%world.name)
        phase_fig.savefig(figures_folder+"Demo%sTimePlot.png"%world.name)
        plt.show()
        exit()
    try:
        if re_sim:
            raise IOError
        with open("sim_res_%s.pkl"%world.name, 'r') as fil:
            sim_res = pkl.load(fil)  # So fast!!
    except IOError:
        # seg = UnfinishedTrajectorySegment(CommandEnum.max, 0, None)
        # seg.update(0.0, 0.0, 0.0, 0.0)
        # seg.update(0.1, 0.1, 0.0, 0.1)
        # seg.update(0.2, 0.2, 0.0, 0.2)
        # seg = seg.finish()
        # tree = TrajectoryTree([seg])
        # dummy_controller_test_plan=tree
        # controller = NewPlanController(world, dummy_controller_test_plan)
        controller = NewPlanController(world, plan)
        total_time_to_simulate= plan.time(plan.xi_max)+0.2
        print "simulating to %.2f s"%total_time_to_simulate
        x,y,xd,yd= world.x0
        x0=x, y, xd+0.001, yd
        sim = MultiContactNumericSimulator(controller,
                                           time=total_time_to_simulate, x0=x0, dt=time_step)
        sim_res = sim.simulate()
        print "done simulating"
        with open("sim_res_%s.pkl"%world.name, 'w') as fil:
            pkl.dump(sim_res, fil)
    postsim_time=tm.time()

    print "planning time", postplan_time - preplan_time, "s, sim time", postsim_time-postplan_time, "s"
    if result==DemoResult.animation and not no_anim: # animation
        animator = MultiContactSimulationAnimator(
                sim_res, xlims=world.xlims, ylims=world.ylims, show_phase_space=False)
        animator.show_platforms(world.platforms)
        animator.show_com_path(world.com_path, num_tangents=10)
        animator.animation_plot.add_graphic(
            MultiContactRobotGraphic(world))
        animator.start()
        plt.show()
        # animator.phase_space_plot.setup_raw_phase_space()
        # animator.phase_space_plot.setup_phase_space_projection()
        # animator.phase_space_plot.setup_sister_space_projection()
        # animator.phase_space_plot.plot_integration_data(plan)

    if result==DemoResult.ps_compare:
        phase_fig = plt.figure(figsize=(1.6*5,1.6*3.5))
        phase_ax = phase_fig.add_subplot(111)
        phase_ax.set_title(r"$\rmPhase\,Space$",fontsize=16)
        phase_ax.set_xlabel(r"$\xi$",fontsize=14)
        phase_ax.set_ylabel(r"$\dot\xi$",fontsize=14)
        for seg in plan:
            xis = np.linspace(seg.xi_min, seg.xi_max, 1000)
            xi_dots = [seg(xi) for xi in xis]
            if seg.command == CommandEnum.max:
                plt.plot(xis[0], xi_dots[0], color=ut_burnt_orange, marker='o', ms=4, mew=1, markeredgecolor='w')
                plt.plot(xis, xi_dots,lw=4,color=ut_burnt_orange, linestyle='-.', dash_capstyle='round', solid_capstyle='round')
                plt.plot(xis[-1], xi_dots[-1], color=ut_burnt_orange, marker='o', ms=10, mew=1, markeredgecolor='w')
            if seg.command == CommandEnum.zero:
                plt.plot(xis[0], xi_dots[0], color=ut_green, marker='o', ms=4, mew=1, markeredgecolor='w')
                plt.plot(xis, xi_dots,lw=2,color=ut_green, linestyle='-', dash_capstyle='butt', solid_capstyle='butt')
                plt.plot(xis, xi_dots,lw=5,color=ut_green, linestyle='--', dash_capstyle='butt', solid_capstyle='butt')
                plt.plot(xis[-1], xi_dots[-1], color=ut_green, marker='o', ms=10,mew=1,  markeredgecolor='w')
            if seg.command == CommandEnum.min:
                plt.plot(xis[0], xi_dots[0], color=ut_blue, marker='o', ms=4, mew=1, markeredgecolor='w')
                plt.plot(xis, xi_dots,lw=4,color=ut_blue, solid_capstyle='round')
                plt.plot(xis[-1], xi_dots[-1], color=ut_blue, marker='o', ms=10, mew=1, markeredgecolor='w')
        phase_ax.axis([-0.1,plan.xi_max+0.1,-0.1,world.robot.max_speed+.1])
        plt.plot(sim_res.phase_space_data[:,0], sim_res.phase_space_data[:,1],'k',lw=1)
        print sim_res.phase_space_data
        phase_fig.tight_layout()
            # plt.plot(seg.call.x, seg.call.y, 'o')
        # phase_fig.savefig(figures_folder+"Demo%spscompare.pdf"%world.name)
        # phase_fig.savefig(figures_folder+"Demo%sMaxPhaseSpace.png"%world.name)
        plt.show()
        exit()
    elif result==DemoResult.frames:
        total_time = plan.time(plan.xi_max)
        for ind, time in enumerate(np.linspace(0.02*total_time,0.98*total_time,8)):
            frame = int(200*time)
            animator = MultiContactSimulationAnimator(
                sim_res, xlims=world.xlims, ylims=world.ylims, show_phase_space=False)
            animator.show_platforms(world.platforms)
            animator.show_com_path(world.com_path, num_tangents=10)
            animator.animation_plot.add_graphic(
                MultiContactRobotGraphic(world))
            animator.init()
            animator.init_animation()
            sim_res.i = frame
            animator.animate(600)
            extent = animator.animation_plot.ax.get_window_extent().transformed(animator.fig.dpi_scale_trans.inverted())
            animator.fig.savefig(
                figures_folder+"Demo%sFrame%d.pdf"%
                (world.name, ind), bbox_inches=extent)
            animator.fig.savefig(
                figures_folder+"Demo%sFrame%d.png"%
                (world.name, ind), bbox_inches=extent)
        plt.show()
    elif result==DemoResult.video:
        animator = MultiContactSimulationAnimator(
                sim_res, xlims=world.xlims, ylims=world.ylims)
        animator.show_platforms(world.platforms)
        animator.show_com_path(world.com_path, num_tangents=10)
        animator.animation_plot.add_graphic(
            MultiContactRobotGraphic(world))
        animator.movie(video_folder+"demo%s_new.mp4"%world.name, "Simple Multi-Contact Robot Simulation")
    elif result==DemoResult.ps_video:
        animator = PlanAnimator(sim_res, planner.result.all_segments_ever, plan)
        animator.movie(video_folder+"demo%s_planning.mp4"%world.name, "Simple Multi-Contact Robot Planning")
    else:
        print "unexpected result type!"

if __name__ == '__main__':
    import argparse
    print "Welcome to 2D Multi-Contact World"

    parser = argparse.ArgumentParser(description='Run the slip model.')
    parser.add_argument(
        '-p', '--profile', help='run_profiler', action='store_true')

    args = parser.parse_args()
    if args.profile:
        print "Profiling!!!"
        import cProfile
        print cProfile.run("main(do_not_resuse_data=True)")
        # main(1000)
    else:
        main()
