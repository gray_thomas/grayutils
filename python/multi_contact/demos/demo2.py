"""
Multicontact simulator.

Demo with a single contact state, just tests the finding of motive transitions.
"""

from PlatformWithChecks import PlatformWithChecks
from graphics.MultiContactRobotGraphic import MultiContactRobotGraphic
from PlanFollowingController import PlanFollowingController
from MultiContactGraphics import MultiContactSimulationAnimator
from MultiContactNumericSimulator import MultiContactNumericSimulator
from MultiContactPlanner import MultiContactPlanner
from Contact import Contact
from Bezier2Splines import Bezier2Splines
import numpy as np


def setup_contact_list(platforms, robot_graphic):
    """create a series of platform contacts and times to switch."""
    pl_a = Contact(platforms[0], 0.1, "l_foot")
    pl_b = Contact(platforms[2], 0.9, "r_hand")
    pl_c = Contact(platforms[0], 0.9, "r_foot")
    pl_d = Contact(platforms[1], 0.3, "l_foot")
    pl_e = Contact(platforms[2], 0.5, "l_hand")
    contact_data = [
        [pl_a, pl_b, pl_c]]
    magic = [999]
    return contact_data, magic


def main():
    """run the demo."""
    friction_mu = 0.4
    leg_lims = [0.2, 0.7]
    platforms = [
        PlatformWithChecks(0.25, 0.25, 1.00, 0.50, friction_mu, 0.5, leg_lims),
        PlatformWithChecks(1.50, 0.50, 2.25, 0.50, friction_mu, 0.5, leg_lims),
        PlatformWithChecks(1.25, 1.75, 0.50, 1.50, friction_mu, 0.5, leg_lims),
        PlatformWithChecks(2.50, 1.00, 3.25, 1.00, friction_mu, 0.5, leg_lims),
        PlatformWithChecks(2.00, 2.00, 2.00, 1.50, friction_mu, 0.5, leg_lims),
    ]
    height_points = np.array([
        [0.499999, 1.0],
        [0.75, 1.0],
        [1.0, 1.125]
    ])
    height_surface = Bezier2Splines(height_points)
    robot_graphic = MultiContactRobotGraphic()
    contact_data, switch_times = setup_contact_list(platforms, robot_graphic)
    # real_planner = MultiContactPlanner(contact_data, height_surface)
    # result: E = 12.2830107718 s = 0.192667260869
    planner = PlanFollowingController(contact_data, height_surface,
                                       switch_times=switch_times, plan_switch_time=1.2, plan_switch_s=0.305923650968)
    sim = MultiContactNumericSimulator(robot_graphic, platforms, planner,
                                       time=0.581, x0=(0.5, 1.0, 0.0, 0.0))
    animator = MultiContactSimulationAnimator(
        sim, xlims=[0.0, 4.0], ylims=[0.0, 2.0])
    animator.show_platforms(platforms)
    animator.show_height_surface(height_surface, num_tangents=10)
    animator.animation_plot.add_graphic(robot_graphic)
    # animator.movie("perfect_stop.mp4","Simple Multi-Contact Robot Simulation")
    animator.start()


if __name__ == "__main__":
    main()
