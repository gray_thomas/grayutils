"""
Multicontact simulator.

Demo with two contact states. Wide cones. No kinematics.
"""


from graphics.MultiContactRobotGraphic import MultiContactRobotGraphic
from PlanFollowingController import PlanFollowingController
from MultiContactGraphics import MultiContactSimulationAnimator
from MultiContactNumericSimulator import MultiContactNumericSimulator
from MultiContactPlanner import MultiContactPlanner
from Contact import Contact
from Bezier2Splines import Bezier2Splines
import numpy as np
from math import tan, pi
import matplotlib.pyplot as plt
from intersection.SpeedContourGenerator import SpeedContourGenerator
from intersection.SpeedContourGenerator2 import SpeedContourGenerator2
from intersection.OptimalMulticontactSystem import InfeasibilityException
from collections import namedtuple
import ly_utils as lu
import lyapunov as ly
import worlds.demo6_world as world
from scipy.interpolate import interp1d


def graph_contours(contact_data, height_surface):
    Robot = namedtuple(
        "Robot", ("mass", "gravity_accel", "max_leg_length"))
    robot = Robot(20, -9.81, 1.2)
    generators = []
    data = []
    labels = []
    color = []
    j = 0.0
    cmap = plt.get_cmap("CMRmap")
    for i, contact_combination in enumerate(contact_data):
        j += 0.2
        for speed in [0.0, 2.0, 4.0]:
            j += 0.04
            for is_max_accel in [True, False]:
                j += 0.01
                gen = SpeedContourGenerator(
                    contact_combination, height_surface, robot, speed=speed, is_max_accel=is_max_accel)
                label = "c" + str(i) + ", s=" + str(speed) + \
                    "(m/s), " + ("accel" if is_max_accel else "decel")
                labels.append(label)
                color.append(cmap(j))
                generators.append(gen)
                data.append([])

    s_vals = np.linspace(0, 1, 100)
    system = lu.ParallelHybridSystems(generators)
    logger = ly.Recorder(system)
    stepper = ly.dormand_prince(system, s_vals)
    stepper.time_tolerance = 1e-3
    stepper.rel_tolerance = 1e-2
    for energy, events in stepper:
        logger.log(events)
        print "stepping"
        for i, gen in enumerate(generators):
            try:
                data[i].append((gen.arc_s, gen.get_accel()))
            except InfeasibilityException:
                pass
        if events:
            try:
                system.handle_events(events)
            except StopIteration:
                print "StopIteration raised"
                break
            except lu.StopContinuousIteration:
                print "Discrete dynamics event"
                stepper.step_across(system.state[1])
    plt.figure()
    for dat, color in zip(data, color):
        plt.plot([x[0] for x in dat], [x[1]
                                       for x in dat], color=color, linewidth=3)

    plt.legend(labels)

    plt.xlabel("arc length along CoM path")
    plt.ylabel("acceleration along CoM path")
    plt.title("Contours Of Constant Speed")
    plt.show()
    print "graph them!"


def graph_contours3D(contact_data, height_surface):
    Robot = namedtuple(
        "Robot", ("mass", "gravity_accel", "max_leg_length"))
    robot = Robot(20, -9.81, 1.2)

    cmap = plt.get_cmap("CMRmap")

    speed_contour_generator = SpeedContourGenerator2(
        contact_data[0], height_surface, robot, speed=0.0, is_max_accel=True)

    plt.figure()
    x = np.linspace(0, 1, 1000)
    plt.plot(x, height_surface.path_2_arc(x))
    plt.show()


def main():
    """run the demo."""
    platforms = world.setup_platforms()
    height_surface = world.setup_height_surface()
    robot_graphic = MultiContactRobotGraphic()
    contact_data = world.setup_contact_list(platforms, robot_graphic)
    # real_planner = MultiContactPlanner(contact_data, height_surface)
    plan_now = False
    is_graph_contours = False
    if plan_now:
        real_planner = MultiContactPlanner(contact_data, height_surface)
        plot_tuple, fused_array, switch_times = real_planner.plan_path()

        controller = PlanFollowingController(contact_data, height_surface,
                                           switch_times=switch_times, plan_switch_time=switch_times[0])
        sim = MultiContactNumericSimulator(robot_graphic, platforms, controller,
                                           time=switch_times[1], x0=(0.25, 1.6, 0.0, 0.0))
        sim.simulate()
        sim_energy = []
        sim_x = []
        for i in range(0, len(sim.times)):
            x, y, xd, yd = sim.data[i, :]
            t = sim.times[i]
            sim_energy.append(0.5 * 20.0 * (xd * xd + yd * yd))
            sim_x.append(x)
        planned_x = []
        planned_energy = []
        for i in range(0, fused_array.shape[1]):
            time, path_s, energy = fused_array[:, i]
            x, y = height_surface.point(0, path_s)
            planned_x.append(x)
            planned_energy.append(energy)
        plt.plot(planned_x, planned_energy)
        plt.hold(True)
        plt.plot(sim_x, sim_energy)
        plt.xlabel("x")
        plt.ylabel("energy")
        plt.show()
    elif is_graph_contours:
        graph_contours3D(contact_data, height_surface)

    else:
        # switch_times=[0.371398422629, 999]  # demo5 world
        switch_times = [0.404425333453, 999]  # demo6 world
        # planner claims 0.371398422629
        print "making plot"

        controller = PlanFollowingController(contact_data, height_surface,
                                           switch_times=switch_times, plan_switch_s=0.5)
        sim = MultiContactNumericSimulator(robot_graphic, platforms, controller,
                                           # time = 2.0 * 0.371398422629, x0 =
                                           # (0.25, 1.6, 0.0, 0.0))
                                           time=2.0 * 0.404425333453, x0=(0.25, 1.6, 0.0, 0.0))  # demo6
        # time=0.38, x0=(1.25, 1.6, 0.0, 0.0))
        animator = MultiContactSimulationAnimator(
            sim, xlims=[-0.20, 1.20], ylims=[0.3, 2.5], show_phase_space=True)
        animator.show_platforms(platforms)
        animator.show_height_surface(height_surface, num_tangents=10)
        animator.animation_plot.add_graphic(robot_graphic)
        # animator.phase_space_plot.setup_phase_space_projection()
        # animator.phase_space_plot.setup_sister_space_projection()
        # animator.show_frames([37]) # 20 = 0.1 seconds
        # animator.show_frames([37*2]) # 20 = 0.1 seconds

        # animator.show_frames([37*3]) # 20 = 0.1 seconds
        # animator.show_frames([37 * 2 + 1])  # 20 = 0.1 seconds
        # animator.show_frames([1]) # 20 = 0.1 seconds
        # animator.show_frames([-1]) # 20 = 0.1 seconds
        # animator.movie("demo6.mp4","Simple Multi-Contact Robot Simulation")
        animator.start()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Run the slip model.')
    parser.add_argument(
        '-p', '--profile', help='run_profiler', action='store_true')

    args = parser.parse_args()
    if args.profile:
        print "Profiling!!!"
        import cProfile
        print cProfile.run("main()")
        # main(1000)
    else:
        main()
