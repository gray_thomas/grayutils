"""
Multicontact simulator.

Demo with two contact states. Wide cones. No kinematics.
"""


from graphics.MultiContactRobotGraphic import MultiContactRobotGraphic
from PlanFollowingController import PlanFollowingController
from MultiContactGraphics import MultiContactSimulationAnimator
from MultiContactNumericSimulator import MultiContactNumericSimulator
from MultiContactPlanner import MultiContactPlanner
from Contact import Contact
from Bezier2Splines import Bezier2Splines
import numpy as np
from math import tan, pi
import matplotlib.pyplot as plt
from intersection.SpeedContourGenerator import SpeedContourGenerator
from intersection.SpeedContourGenerator2 import SpeedContourGenerator2
from intersection.OptimalMulticontactSystem import InfeasibilityException
from collections import namedtuple
import ly_utils as lu
import lyapunov as ly
import worlds.demo8_world as world
from scipy.interpolate import interp1d
import graphics.multicontact_phase_space as mcps
import cPickle as pkl
from MultiContactPhaseSpaceSurfacer import MultiContactPhaseSpaceSurfacer, Surfacer2


def main():
    """run the demo."""

    if True:  # re-plan!
        print "Planning!"
        real_planner = MultiContactPlanner(world)
        plot_tuple, fused_array, switch_times = real_planner.plan_path()
        with open("plan.pkl", 'w') as fil:
            pkl.dump(switch_times, fil)
    else:
        with open("plan.pkl", 'r') as fil:
            switch_times = pkl.load(fil)

    switch = switch_times[0]
    print "switch time is ", switch
    switch_times = [switch, 999]
    controller = PlanFollowingController(world,
                                         switch_times=switch_times, plan_switch_s=0.5)

    if False:  # resimulate!
        print "simulating"
        sim = MultiContactNumericSimulator(controller,
                                           time=2.0 * switch, x0=world.x0)
        sim_res = sim.simulate()
        with open("sim_res.pkl", 'w') as fil:
            pkl.dump(sim_res, fil)
    else:
        with open("sim_res.pkl", 'r') as fil:
            sim_res = pkl.load(fil)  # So fast!!

    animator = MultiContactSimulationAnimator(
        sim_res, xlims=world.xlims, ylims=world.ylims, show_phase_space=True)
    animator.show_platforms(world.platforms)
    animator.show_com_path(world.com_path, num_tangents=10)
    animator.animation_plot.add_graphic(
        MultiContactRobotGraphic(world.com_path))
    animator.phase_space_plot.setup_raw_phase_space()
    animator.phase_space_plot.setup_phase_space_projection()
    animator.phase_space_plot.setup_sister_space_projection()


    if False:  # recompute!
        print "computing accel lookup surface"
        surfacer = Surfacer2(
            world, s_vals=np.linspace(world.com_path.max_arc*0.05, world.com_path.max_arc*0.95, 30), sd_vals=np.linspace(0, 1.5, 30))
        surf_dat2 = np.array(surfacer.data)
        with open("surf_res2.pkl", 'w') as fil:
            pkl.dump(surf_dat2, fil)
    else:
        with open("surf_res2.pkl", 'r') as fil:
            surf_dat2 = pkl.load(fil)
    # animator.phase_space_plot.setup_3d_surface(surf_dat2)
    animator.start()
    # animator.init()
    # animator.init_animation()
    # sim_res.i = 50
    # animator.animate(600)
    # plt.show()
    # animator.movie("demo8.mp4","Simple Multi-Contact Robot Simulation")


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Run the slip model.')
    parser.add_argument(
        '-p', '--profile', help='run_profiler', action='store_true')

    args = parser.parse_args()
    if args.profile:
        print "Profiling!!!"
        import cProfile
        print cProfile.run("main()")
        # main(1000)
    else:
        main()
