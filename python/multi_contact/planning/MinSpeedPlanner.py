"""
MinSpeedPlanner: a module for multi contact phase space planning

"""


import lyapunov as ly
import ly_utils as lu
import numpy as np
from multi_contact.solvers.SolverListSystem import SolverListSystem, GuardedSolverList
from PlannerResult import PlannerResult, UnfinishedTrajectorySegment
from multi_contact.solvers.CommandEnum import CommandEnum
from enum import Enum


PlannerState = Enum(
    "PlannerState", "decelerate_forward quasi_static_forward follow_speed_forward accelerate_back search_ahead")


@lu.named_state_system("PathTimeState", ['xi', 'xi_dot', 't'], xname='xi_t_state', tname='sigma')
class MinSpeedPlanner(lu.HybridAutomata):

    """
    A hybrid automata with five nodes.

    The nodes are implemented as SharedSuperObjectSubclasses of the
    MinSpeedPlanner, which means that the nodes have access to the planner's
    methods and properties, but not to any member variables assigned after the
    class definition, e.g. self._world. They can definite their own
    self._world. But they share the self.world property, and thus cannot
    redefine it.

    """

    def __init__(self, world):
        self._world = world
        self._result = PlannerResult()
        self._solvers = GuardedSolverList(world)
        super(MinSpeedPlanner, self).__init__(
            PlannerState.decelerate_forward,
            ForwardDecelerator(self))
        self.add_system(
            PlannerState.follow_speed_forward,
            ForwardFollower(self))
        self.add_system(
            PlannerState.quasi_static_forward,
            QuasiStaticFollower(self))
        self.add_system(
            PlannerState.accelerate_back,
            BackwardAccelerator(self))
        self.add_system(
            PlannerState.search_ahead,
            ForwardSearcher(self))

    @property
    def result(self):
        return self._result

    @property
    def world(self):
        return self._world

    @property
    def solvers(self):
        return self._solvers

    @property
    def state(self):
        return self.solvers.state

    @state.setter
    def state(self, val):
        self.solvers.state = val

    def integrate(self):
        system = self
        stepper = ly.dormand_prince(system, np.linspace(0, 10, 1000))
        stepper.time_tolerance = 1e-6
        stepper.relative_tolerance = 1e-7
        stepper.absolute_tolerance = 1e-5

        for jump, sigma, state, guard in lu.hybrid_integrate(stepper):
            print self.automata_state, jump, sigma, state, guard.name if guard else "None"
            # since these state-systems also build a result.
            self.active.update()

        return self.result.forward_trajectory


@lu.SharedSuperObjectSubclass(MinSpeedPlanner)
class ForwardDecelerator(object):

    """
    decelerate_forward  (ForwardDecelerator):
      - (solver_change) solver state change -> accelerate_forward (new state)
      - (min_speed) reach min speed -> {follow_speed_forward, search_ahead }
      - (zero_speed) reach 0 speed -> {quasi_static_forward, search_ahead }
      - (max_speed) reach top feasible speed-> untraversable path
      - (end_of_path) -> untraversable path
    """

    def __init__(self):
        self._events = [self.solver_change, self.min_speed,
                        self.zero_speed, self.max_speed, self.end_of_path]
        self.trajectory = self.result
        self.max_xi_dot = self.world.robot.max_speed
        self.max_xi = self.world.com_path.xi_max
        self.unfinished_segment = None

    @property
    def events(self):
        return self._events

    @property
    def stance_index(self):
        return self.solvers.min_xi_ddot_prior_sstate.best_stances[0]

    def __call__(self):
        """calculate the state derivative."""
        return (self.xi_dot, self.solvers.easy_min_xi_ddot(), 1.0)

    def init(self):
        """Reset solver memory, jump, start new segment."""
        self.solvers.min_xi_ddot_prior_sstate = None
        self.solvers.min_xi_ddot_guard.jump()
        self.unfinished_segment = UnfinishedTrajectorySegment(
            CommandEnum.min, self.stance_index)
        # self.result.forward_start(CommandEnum.min, self.stance_index)

    def update(self):
        """Record data for segment intersection."""
        # self.result.forward_update(self.xi, self.xi_dot)
        self.unfinished_segment.update(self.xi, self.xi_dot)

    @lu.Guard
    def solver_change(self):
        return self.solvers.min_xi_ddot_guard()

    @solver_change.jumper
    def solver_change(self):
        self.solvers.min_xi_ddot_guard.jump()
        # self.result.forward_end()
        self.result.forward_segments.append(
            self.unfinished_segment.finish())
        # self.result.forward_start(CommandEnum.min, self.stance_index)
        self.unfinished_segment = UnfinishedTrajectorySegment(
            CommandEnum.min, self.stance_index)
        self._events = self.events[0:]  # TODO: check?
        raise lu.Jump()

    @lu.Guard
    def zero_speed(self):
        return lu.EventThrowIf[self.xi_dot < 0.0]

    @zero_speed.jumper
    def zero_speed(self):
        # TODO: branching logic: either FF or SA
        self.result.forward_segments.append(
            self.unfinished_segment.finish())
        self.state = self.sigma, (self.xi, 0.0, 0.0)
        if solvers.is_followable(0.0):
            self.switch_state(PlannerState.quasi_static_forward)
        else:
            self.switch_state(PlannerState.search_ahead)
        raise lu.StateJump()

    @lu.Guard
    def min_speed(self):
        return lu.EventThrowIf[self.xi_dot < self.solvers.min_xi_dot()]

    @min_speed.jumper
    def min_speed(self):
        # TODO: branching logic: either FF or SA
        self.result.forward_segments.append(
            self.unfinished_segment.finish())
        self.state = self.sigma, (self.xi, self.solvers.min_xi_dot(), 0.0)
        if solvers.is_followable(0.0):
            self.switch_state(PlannerState.follow_speed_forward)
        else:
            self.switch_state(PlannerState.search_ahead)
        raise lu.StateJump()

    @lu.Guard
    def max_speed(self):
        return lu.EventThrowIf[self.xi_dot > self.solvers.max_xi_dot()]

    @max_speed.jumper
    def max_speed(self):
        self.result.forward_segments.append(self.unfinished_segment.finish())
        # self.result.forward_end()
        self.result.traversable = False
        raise lu.StopIntegration()  # Untraversable

    @lu.Guard
    def end_of_path(self):
        return lu.EventThrowIf[self.xi > self.max_xi]

    @end_of_path.jumper
    def end_of_path(self):
        self.result.forward_segments.append(self.unfinished_segment.finish())
        # self.result.forward_end()
        self.result.traversable = False
        raise lu.StopIntegration()  # Untraversable

    # end_of_path


@lu.SharedSuperObjectSubclass(MinSpeedPlanner)
class ForwardFollower(object):

    """
    follow_speed_forward (ForwardFollower):
      - (min_xi_ddot_sstate_change) solver state change (loop)
      - (max_xi_ddot_sstate_change) solver state change (loop)
      - (empty_follow_set) switch to chatter (loop)
      - (forced acceleration) -> decelerate_forward
      - (sudden_min_speed_drop) -> decelerate_forward
      - (forced_deceleration) -> search_ahead
      - (sudden_min_speed_rise) -> search_ahead
      - (zero_speed) -> quasi_static_forward
      - (end_of_path) (unfollowable)
    """

    def __init__(self):
        self._events = [self.min_xi_ddot_sstate_change, self.max_xi_ddot_sstate_change,
                        self.empty_follow_set, self.forced_acceleration, self.forced_deceleration, self.end_of_path]
        self.chattering = False
        self.trajectory = self.result
        self.max_xi = self.world.com_path.max_xi
        self.max_xi_dot = self.world.robot.max_speed
        self.unfinished_segment = None

    def init(self):
        # This cannot be implemented without first differentiating the min speed.
        raise NotImplemented()
        if self.solvers.is_followable(0.0):
            s = self.solvers.get_followable_set(0.0)
            self.unfinished_segment = UnfinishedTrajectorySegment(
                CommandEnum.zero, s[0])
            self.chattering = False
        else:
            self.unfinished_segment = UnfinishedTrajectorySegment(
                CommandEnum.chatter, ())
            self.chattering = True
        self._events = [self.end_of_path]

    @property
    def events(self):
        return self._events

    def __call__(self):
        raise NotImplemented()
        # need to differentiate min speed
        return (self.solvers.min_xi_dot(), 0.0, 1.0)

    def update(self):
        self.unfinished_segment.update(self.xi, self.xi_dot)

    @lu.Guard
    def end_of_path(self):
        return lu.EventThrowIf[self.xi >= self.max_xi]

    @end_of_path.jumper
    def end_of_path(self):
        self.result.forward_segments.append(
            self.unfinished_segment.finish())
        self.result.build_forward()
        self.state = self.sigma, (self.max_xi, 0.0, 0.0)
        self.switch_state(PlannerState.decelerate_back)
        raise lu.StateJump()


@lu.SharedSuperObjectSubclass(MaxSpeedPlanner)
class BackwardDecelerator(object):

    """
    decelerate_back (BackwardDecelerator):
      - (intersection) reach intersection while not final -> update history -> follow_speed_forward(start_of_followable)
      - (intersection) reach intersection and final -> finish history -> return success!
      - (max_speed) reach max speed (while decelerating) -> update history (no good path) -> follow_speed_forward(start_of_followable)
      - (solver_change) solver state change -> decelerate_back (new state)
    """

    def __init__(self):
        self._events = [self.intersection]
        self.trajectory = self.result
        # self.events.append(self.max_speed)
        # self.events.append(solver_change)

    def __call__(self):
        return (-self.xi_dot, -self.solvers.easy_min_xi_ddot(), -1.0)

    @property
    def events(self):
        return self._events

    def init(self):
        self.solvers.forward = False
        sstate = self.solvers.min_xi_ddot_sstate()
        index = sstate.best_stances[0]
        self.unfinished_segment = UnfinishedTrajectorySegment(
            CommandEnum.min, index)
        self.solvers.min_xi_ddot_prior_sstate = sstate

    def update(self):
        self.unfinished_segment.update(self.xi, self.xi_dot)

    @lu.Guard
    def intersection(self):
        xi_dot = self.trajectory.forward_trajectory
        if xi_dot.xi_max > self.xi:
            other_speed = xi_dot(self.xi)
            return lu.EventThrowIf[xi_dot(self.xi) <= self.xi_dot]
        return lu.EventThrowIf[False]

    @intersection.jumper
    def intersection(self):
        seg = self.unfinished_segment.reverse_finish()
        self.trajectory.temp_reverse_segments.append(seg)
        self.trajectory.splice_in_reverse_trajectory_at(self.xi)
        raise lu.StopIntegration()


@lu.SharedSuperObjectSubclass(MaxSpeedPlanner)
class ForwardSearcher(object):

    """
    search_ahead (ForwardSearcher):
      - (forced_acceleration) reaches forced acceleration -> search_ahead (prepped)
      - (forced_deceleration) reaches forced decel while prepped -> search_ahead (not prepped)
      - (followable_region) reaches followable region while prepped -> decelerate_back (start_of_followable = start_of_followable)
      - (end_of_path) reaches max_xi -> decelerate_back(start_of_followable = (max_xi, 0.0, final=True))
    """

    def __init__(self):
        pass
        # self.events.append(self.forced_acceleration)
        # self.events.append(self.forced_deceleration)
        # self.events.append(self.followable_region)
        # self.events.append(self.end_of_path)

if __name__ == '__main__':
    import multi_contact.worlds.demo8_world as world
    # path_s_decomposition, stances = MultiContactExplorer(world).compute()
    planner = MaxSpeedPlanner(world)
    print planner.__state_doc__
    res = MaxSpeedPlanner(world).integrate()
    import matplotlib.pyplot as plt
    for seg in res:
        xis = np.linspace(seg.xi_min, seg.xi_max, 1000)
        xi_dots = [seg(xi) for xi in xis]
        plt.plot(xis, xi_dots)
        plt.plot(seg.call.x, seg.call.y, 'o')

    for xi in np.linspace(res.xi_min, res.xi_max, 200):
        print xi
        print res.leaf(xi)
        plt.plot(xi, res.leaf(xi).stance_index, 'o')

    print res.leaf(0.24).stance_index
    plt.show()
