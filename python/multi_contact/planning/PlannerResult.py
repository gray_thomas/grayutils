from enum import Enum
from scipy.interpolate import interp1d
from numpy import isnan
from multi_contact.solvers.CommandEnum import CommandEnum
from scipy import interp


class UnfinishedTrajectorySegment(object):

    def __init__(self, command, stance_index, dstate):
        self.xis = []
        self.xi_dots = []
        self.xi_ddots = []
        self.times = []
        self.stance_index = stance_index
        self.command = command
        self.dstate = dstate

    def update(self, xi, xi_dot, xi_ddot, time):
        self.xis.append(xi)
        self.xi_dots.append(xi_dot)
        self.xi_ddots.append(xi_ddot)
        self.times.append(time)

    def finish(self):
        # print self.xis, self.xi_dots
        return TrajectorySegment(self.xis, self.xi_dots, self.xi_ddots, self.times,
            self.command, self.stance_index, self.dstate)

    def reverse_finish(self):
        return TrajectorySegment(
            list(reversed(self.xis)),
            list(reversed(self.xi_dots)),
            list(reversed([-xiddot for xiddot in self.xi_ddots])),
            list(reversed(self.times)),
            self.command, self.stance_index, self.dstate)

class TrajectorySegment(object):

    def __init__(self, xis, xi_dots, xi_ddots, times, command, stance_index, dstate):
        self.xis = xis[:]
        self.xi_dots = xi_dots[:]
        self.xi_ddots = xi_ddots[:]
        self.times = times[:]
        l = zip(xis, xi_dots, xi_ddots, times)
        l2 = zip(l[0:-1], l[1:])
        for t1, t2 in l2:
            xi1, xi_dot1, xi_ddot1, time1 = t1
            xi2, xi_dot2, xi_ddot2, time2 = t2
            assert xi2 > xi1
            assert time2 > time1
        self.build_functions()
        self.xi_min = xis[0]
        self.xi_max = xis[-1]
        self.time_start = 0
        self.n = 1
        self.command = command
        self.stance_index = stance_index
        self.dstate = dstate

    def clip(self, xi):
        if xi>self.xi_max:
            return self.xi_max
        if xi<self.xi_min:
            return self.xi_min
        return xi

    @classmethod
    def copy(cls, obj):
        xis = obj.xis
        xi_dots = obj.xi_dots
        xi_ddots = obj.xi_ddots
        times = obj.times
        command = obj.command
        stance_index = obj.stance_index
        dstate = obj.dstate
        ts = cls(xis, xi_dots, xi_ddots, times, command, stance_index, dstate)
        ts.time_start = obj.time_start
        ts.xi_min = obj.xi_min
        ts.xi_max = obj.xi_max
        return ts

    def delete_functions(self):
        self.xi_dot_by_xi = None
        self.t_by_xi = None
        self.xi_by_t = None
        self.xi_ddot_by_xi = None

    def build_functions(self):
        if len(self.xis) == 1:
            self.xi_dot_by_xi = lambda x: self.xi_dots[0]
            self.t_by_xi = lambda xi: self.times[0]
            self.xi_by_t = lambda t: self.xis[0]
            self.xi_ddot_by_xi  = lambda xi: self.xi_ddots[0]
        else:
            self.xi_dot_by_xi = lambda xi: interp(xi, self.xis, self.xi_dots)
            self.t_by_xi = lambda xi: interp(xi, self.xis, self.times)
            self.xi_by_t = lambda t: interp(t, self.times, self.xis)
            self.xi_ddot_by_xi  = lambda xi: interp(xi, self.xis, self.xi_ddots)

    def time(self, xi):
        try:
            return self.t_by_xi(self.clip(xi)) + self.time_start
        except ValueError:
            print xi, self.xi_min, self.xi_max, self.clip(xi), self.t_by_xi.x[0], self.t_by_xi.x[-1]
            exit()

    def xi_trajectory(self, time):
        return self.xi_by_t(time-self.time_start)

    def leaf(self, xi):
        return self

    def leaf_time(self, time):
        return self

    def count(self, xi):
        return 0.0

    def __repr__(self):
        return "%s{%.3f m, %.3f m}(%.3f s--%.3f s)" % (repr(self.command), self.xi_min, self.xi_max, self.time(self.xi_min), self.time(self.xi_max))

    def __call__(self, xi):
        """ return xi_dot at point xi """
        if xi > self.xi_max:
            return self.xi_dot_by_xi(self.xi_max)
        if xi < self.xi_min:
            return self.xi_dot_by_xi(self.xi_min)
        return self.xi_dot_by_xi(xi)

    def __getitem__(self, count):
        if count < 0:
            raise IndexError()
        if count >= self.n:
            raise IndexError()
        return self


class TrajectoryTree(object):

    def __init__(self, tsegments):

        self.d = len(tsegments) / 2
        part1 = tsegments[:self.d]
        part2 = tsegments[self.d:]
        if len(part1) == 1:
            self.low = part1[0]
        else:
            self.low = TrajectoryTree(part1)
        if len(part2) == 1:
            self.high = part2[0]
        else:
            self.high = TrajectoryTree(part2)
        self.critical_xi = tsegments[self.d].xi_min
        time_start = 0
        prev_max = None
        for seg in tsegments:
            seg.time_start = time_start - seg.t_by_xi(seg.xi_min)
            time_start = seg.time(seg.xi_max)
            if prev_max != None:
                if prev_max != seg.xi_min:
                    print "Trajectory Tree: xi gap!", prev_max, seg.xi_min
                assert abs(prev_max - seg.xi_min) < 1e-3
                assert seg.time(seg.xi_max) != 0.0
                assert time_start != 0.0
            prev_max = seg.xi_max

    @classmethod
    def make(cls, tsegments):
        if len(tsegments) > 1:
            return cls(tsegments)
        else:
            return tsegments[0]

    def time(self, xi):
        return self.leaf(xi).time(xi)

    @property
    def n(self):
        return self.d + self.high.n

    def delete_functions(self):
        self.high.delete_functions()
        self.low.delete_functions()

    def build_functions(self):
        self.high.build_functions()
        self.low.build_functions()

    def __repr__(self):
        return " ".join(["[l:", str(self.low), ", h:", str(self.high), "]"])

    def count(self, xi):
        if xi >= self.critical_xi:
            return self.high.count(xi) + self.d
        return self.low.count(xi)

    def __getitem__(self, count):
        if count >= self.d:
            return self.high[count - self.d]
        return self.low[count]

    def command(self, xi):
        return self.leaf(xi).command

    def stance_index(self, xi):
        return self.leaf(xi).stance_index

    def dstate(self, xi):
        return self.leaf(xi).dstate

    def leaf(self, xi):
        if xi >= self.critical_xi:
            return self.high.leaf(xi)
        return self.low.leaf(xi)

    def leaf_time(self, time):
        if time >= self.time(self.critical_xi):
            return self.high.leaf_time(time)
        return self.low.leaf_time(time)

    def __iter__(self):
        self.i = 0
        return self

    def __call__(self, xi):
        if xi >= self.critical_xi:
            return self.high(xi)
        return self.low(xi)

    def next(self):
        i = self.i
        if i >= self.n:
            raise StopIteration()
        self.i += 1
        return self[i]

    def __len__(self):
        return self.n

    @property
    def xi_max(self):
        return self.high.xi_max

    @property
    def xi_min(self):
        return self.low.xi_min

    def __call__(self, x):
        if x >= self.critical_xi:
            assert not isnan(self.high(x))
            return self.high(x)
        assert not isnan(self.low(x))
        return self.low(x)


def clip(tree, lo_xi, hi_xi):
    leaves = [leaf for leaf in tree if leaf.xi_max >
              lo_xi and leaf.xi_min < hi_xi]
    leaves[0].xi_min = lo_xi
    leaves[-1].xi_max = hi_xi
    return TrajectoryTree.make(leaves)


def CompositeFunctor(wfuncs):
    if len(wfuncs) == 0:
        raise NotImplementedError()
    if len(wfuncs) == 1:
        return wfuncs[0]
    wfuncs = sorted(wfuncs, key=lambda f: f.x_min)
    for f1, f2 in zip(wfuncs[0:-1], wfuncs[1:]):
        assert (f1.x_max == f2.x_min)
    return _CompositeFunctor(wfuncs)


class PlannerResultOrderOfOperationsException(Exception):

    def __init__(self, *args, **kwargs):
        super(PlannerResultOrderOfOperationsException, self).__init__(
            *args, **kwargs)


class PlannerResultRedundantPointException(Exception):

    def __init__(self, *args, **kwargs):
        super(PlannerResultRedundantPointException, self).__init__(
            *args, **kwargs)


class PlannerResult(object):

    def __init__(self):
        self._plan = None
        self._temp_reverse_trajectory = None
        self._forward_segments = []
        self._temp_reverse_segments = []
        self._seg = None
        self._forward = None
        self.all_segments_ever = []

    @property
    def plan(self):
        return self._plan

    @property
    def forward_trajectory(self):
        return self._plan

    def __call__(self):
        return self._plan

    def __repr__(self):
        return ("PlannerResult<plan=%s,all_segments_ever=%s>" %
                (
                    repr(self._plan),
                    repr(self.all_segments_ever)))

    def update(self, xi, xi_dot, xi_ddot, time):
        if self._seg == None:
            return #ignore
        if len(self._seg.xis) > 0:
            if (
                    (xi == self._seg.xis[-1])
                    and (xi_dot == self._seg.xi_dots[-1])
                    and (time == self._seg.times[-1])):
                print "skipping redundant point"
                return
        self._seg.update(xi, xi_dot, xi_ddot, time)

    def start_forward(self, command, stance, dstate):
        self._finish_seg()
        self._seg = UnfinishedTrajectorySegment(
            command, stance, dstate)
        self._forward = True

    def start_pseudo(self, command, dstate):
        self._finish_seg()
        self._seg = UnfinishedTrajectorySegment(
            command, None, dstate)
        self._forward = None

    def _finish_seg(self):
        if self._seg != None:
            if self._forward==True:
                self._forward_segments.append(
                    self._seg.finish())
                self.all_segments_ever.append(self._seg.finish())
            elif self._forward==False:
                self._temp_reverse_segments.append(
                    self._seg.reverse_finish())
                self.all_segments_ever.append(self._seg.reverse_finish())
            else:
                self.all_segments_ever.append(self._seg.finish())
        print self
        self._seg = None
        self._forward = None

    def failure(self):
        self._finish_seg()

    def start_reverse(self, command, stance, dstate):
        self._finish_seg()
        self._seg = UnfinishedTrajectorySegment(
            command, stance, dstate)
        self._forward = False

    def interpolate_forward(self):
        self._finish_seg()
        if len(self._forward_segments) == 0:
            return
        if self.plan == None:
            self._plan = TrajectoryTree.make(
                self._forward_segments)
        else:
            self._plan = TrajectoryTree.make(
                list(iter(self._plan)) + self._forward_segments)
        self._forward_segments = []

    def build_reverse(self):
        self._finish_seg()
        if len(self._temp_reverse_segments) == 0:
            return
        if self._temp_reverse_trajectory == None:
            self._temp_reverse_trajectory = TrajectoryTree.make(
                list(reversed(self._temp_reverse_segments)))
        else:
            self._temp_reverse_trajectory = TrajectoryTree.make(
                list(reversed(self._temp_reverse_segments)) + list(iter(_temp_reverse_trajectory)))
        self._temp_reverse_segments = []

    def splice_reverse_trajectory(self, xi):
        self._finish_seg()
        self.build_reverse()
        self.interpolate_forward()
        forward = self.plan
        back = self._temp_reverse_trajectory
        forward = clip(forward, forward.xi_min, xi)
        back = clip(back, xi, back.xi_max)
        self._plan = TrajectoryTree(
            [l for l in forward] + [l for l in back])
        self._temp_reverse_trajectory = None


if __name__ == '__main__':
    seg = UnfinishedTrajectorySegment(CommandEnum.max, 0, None)
    seg.update(0.0, 0.0, 0.0, 0.0)
    seg.update(0.1, 0.1, 0.0, 0.1)
    seg.update(0.2, 0.2, 0.0, 0.2)
    seg = seg.finish()
    print seg(0.15)
    print seg.command
    print seg.count(0.15)
    seg1 = UnfinishedTrajectorySegment(CommandEnum.zero, 1, None)
    seg1.update(0.2, 0.2, 0.0, 0.0)
    seg1.update(0.3, 0.2, 0.0, 0.1)
    seg1 = seg1.finish()
    seg2 = UnfinishedTrajectorySegment(CommandEnum.min, 2, None)
    seg2.update(0.5, 0.0, 0.0, 0.0)
    seg2.update(0.4, 0.1, 0.0, -0.1)
    seg2.update(0.3, 0.2, 0.0, -0.2)
    seg2 = seg2.reverse_finish()
    print seg2
    tree = TrajectoryTree([seg, seg1, seg2])
    print tree.command(0.199)
    tree.time(tree.xi_max)
    print tree
    print tree[0]
    print tree[1]
    print tree[2]
    print tree.n
    print len(tree)
    for leaf in tree:
        print leaf
    print clip(tree, 0.04, 0.21)

    a = TrajectorySegment(
        [0.1 * i for i in range(0, 11)], [0.1 * i for i in range(0, 11)], [0.0 for i in range(0, 11)], [0.1 * i for i in range(0, 11)], CommandEnum.max, 4, None)
    b = TrajectorySegment([0.1 * i for i in range(10, 21)],
                          [0.1 * i for i in range(10, 21)],
                          [0.0 for i in range(10, 21)], [0.1 * i for i in range(0, 11)], CommandEnum.max, 5, None)
    c = TrajectorySegment(
        [0.1 * i for i in range(20, 51)], [2.0 for i in range(20, 51)], [0.0 for i in range(20, 51)], [0.1 * i for i in range(0, 31)], CommandEnum.zero, 6, None)
    ft = TrajectoryTree.make([a, b, c])
    d = TrajectorySegment([0.1 * i for i in range(40, 51)],
                          [2.0 - 0.2 * i for i in range(0, 11)], [0.0 for i in range(0, 11)], [-59 + 0.1 * i for i in range(0, 11)], CommandEnum.zero, 7, None)
    bt = TrajectoryTree.make([d])

    ft = clip(ft, ft.xi_min, bt.xi_min)
    bt = clip(bt, bt.xi_min, bt.xi_max)
    ft = TrajectoryTree([l for l in ft] + [l for l in bt])

    print ft
    print "time", ft.time(ft.xi_max)

    print ft.leaf(-0.1).stance_index

    ft.delete_functions()
    ft.build_functions()

    import matplotlib.pyplot as plt
    import numpy as np
    # Plot phase space
    if False:
        for seg in ft:
            xis = np.linspace(seg.xi_min, seg.xi_max, 1000)
            xi_dots = [seg(xi) for xi in xis]
            plt.plot(xis, xi_dots)
            plt.plot(seg.xi_dot_by_xi.x, seg.xi_dot_by_xi.y, 'o')
    # plot versus time
    if True:

        for seg in ft:
            ts = np.linspace(ft.time(seg.xi_min), ft.time(seg.xi_max), 1000)
            xis=[]
            xidots=[]
            xiddots=[]
            for t in ts:
                xi = seg.xi_trajectory(t)
                xis.append(xi)
                xidots.append(seg.xi_dot_by_xi(xi))
                xiddots.append(seg.xi_ddot_by_xi(xi))
            plt.plot(ts, xis)
            plt.plot(ts, xidots)
            plt.plot(ts, xiddots)
            # plt.plot(seg.xi_dot_by_xi.x, seg.xi_dot_by_xi.y, 'o')
    plt.show()
