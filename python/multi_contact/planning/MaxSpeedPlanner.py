"""
MaxSpeedPlanner: a module for multi contact phase space planning

The MaxSpeedPlanner class defined in this module generates the fastest
possible trajectory traversing the com_path under multi-contact power.
"""


import ly_utils as ly
import ly_utils as lu
import numpy as np
from gu.colormaps import *
from multi_contact.solvers.SolverListSystem import SolverListSystem, GuardedSolverList
from PlannerResult import PlannerResult
from multi_contact.solvers.CommandEnum import CommandEnum
from enum import Enum


PlannerState = Enum(
    "PlannerState", "accelerate_forward follow_speed_forward decelerate_back search_ahead")


@lu.named_state_system("PathTimeState", ['xi', 'xi_dot', 't'], xname='xi_t_state', tname='sigma')
class MaxSpeedPlanner(lu.HybridAutomata):

    """
    A hybrid automata with four nodes.

    accelerate_forward : follow the maximum acceleration arc
    follow_speed_forward : tries to stay at a constant max speed
    search_ahead : looks for a possible end point for a decelerating
    decelerate_back : calculate the fastest speed which could reach that point.

    decelerate_back is a special state, which attempts to intersect with the
    result of the previous segments. It uses the result object which is shared
    by all the nodes.

    The nodes are implemented as SharedSuperObjectSubclasses of the
    MaxSpeedPlanner, which means that the nodes have access to the planner's
    methods and properties, but not to any member variables assigned after the
    class definition, e.g. self._world. They can definite their own
    self._world. But they share the self.world property, and thus cannot
    redefine it.

    """

    def __init__(self, world):
        self._world = world
        self._result = PlannerResult()
        self._solvers = GuardedSolverList(world)
        self._critical_point = None
        super(MaxSpeedPlanner, self).__init__(
            PlannerState.accelerate_forward,
            ForwardAccelerator(self))
        self.add_system(
            PlannerState.follow_speed_forward,
            ForwardFollower(self))
        self.add_system(
            PlannerState.decelerate_back,
            BackwardDecelerator(self))
        self.add_system(
            PlannerState.search_ahead,
            ForwardSearcher(self))

    @property
    def critical_point(self):
        return self._critical_point

    @critical_point.setter
    def critical_point(self, value):
        self._critical_point = value

    @property
    def result(self):
        return self._result

    @property
    def world(self):
        return self._world

    @property
    def solvers(self):
        return self._solvers

    @property
    def state(self):
        return self.solvers.state

    @state.setter
    def state(self, val):
        self.solvers.state = val

    def integrate(self):
        system = self
        stepper = ly.dormand_prince(system, np.linspace(0, 10, 1000))
        stepper.time_tolerance = 1e-4
        stepper.relative_tolerance = 1e-4
        stepper.absolute_tolerance = 1e-3

        for jump, sigma, state, guard in lu.hybrid_integrate(stepper):
            print self.automata_state, jump, sigma, state, guard.name if guard else "no-guards"
            # if jump>=8:
            #     break
            self.result.update(self.xi, self.xi_dot, self()[1], self.t)

        return self.result.plan


@lu.SharedSuperObjectSubclass(MaxSpeedPlanner)
class ForwardAccelerator(object):

    """
    accelerate_forward (start) (ForwardAccelerator):
      + (solver_change) solver state change -> accelerate_forward (new state)
      + (max_speed) reach followable max speed (while accelerating) -> follow_speed_forward
      - (search) reach forced acceleration max speed -> search_ahead(prepped)
      - (too_slow) reach zero/infeasibility (raise untraversable path exception)
      + (end_of_path) reaches max_xi -> decelerate_back(start_of_followable = (max_xi, 0.0, final=True))
    """

    def __init__(self):
        self.setup_guards()
        self._events = [self.end_of_path, self.solver_change, self.max_speed]
        # self.events.append(self.search)
        # self.events.append(self.too_slow)
        # self.events.append(self.end_of_path)
        # self.trajectory = self.result
        self.max_xi_dot = self.world.robot.max_speed
        self.max_xi = self.world.com_path.max_xi

    @property
    def events(self):
        return self._events

    def __call__(self):
        """calculate the state derivative."""
        return (self.xi_dot, self.solvers.easy_max_xi_ddot(), 1.0)

    def init(self):
        print "init FA at %.2f, %.2f" % (self.xi, self.xi_dot)
        new_sstate = self.solvers.max_xi_ddot_sstate()
        index = new_sstate.best_stances[0]
        self.result.start_forward(CommandEnum.max, index, new_sstate)
        self.solvers.max_xi_ddot_prior_sstate = new_sstate



    def setup_guards(self):
        def end_of_path_call():
            return lu.EventThrowIf[self.xi > self.max_xi]

        def end_of_path_jump():
            self.result.interpolate_forward()
            self.state = self.sigma, (float(self.max_xi), 0.0, 0.0)
            self.switch_state(PlannerState.decelerate_back)
            raise lu.StateJump()
        self.end_of_path = ly.Guard2(
            call=end_of_path_call, jump=end_of_path_jump, name="FA::end of path")

        def solver_change_call():
            if self.xi > self.max_xi:
                return lu.EventThrowIf[False]
            return self.solvers.max_xi_ddot_guard.call()

        def solver_change_jump():
            self.solvers.max_xi_ddot_guard.jump()
            sstate = self.solvers.max_xi_ddot_prior_sstate
            index = self.solvers.max_xi_ddot_prior_sstate.best_stances[0]
            self.result.start_forward(CommandEnum.max, index, sstate)
            self._events = self.events[0:]
            raise lu.Jump()

        self.solver_change = ly.Guard2(
            call=solver_change_call, jump=solver_change_jump, name="FA::solver change")

        def max_speed_call():
            return lu.EventThrowIf[self.xi_dot > self.max_xi_dot]

        def max_speed_jump():
            # TODO: branching logic: either FF or SA
            # self.trajectory.forward_segments.append(
            #     self.unfinished_segment.finish())
            self.state = self.sigma, (self.xi, self.max_xi_dot, 0.0)
            sstate = self.solvers.get_followable_sstate(0.0)
            print sstate
            if sstate.fastest_decel_stance==None:
                # the sysem cannot decelerate or maintain speed. We must search
                # self.solvers.shift_delta()
                self.result.interpolate_forward()
                self.switch_state(PlannerState.search_ahead)
            else:
                # the system can decelerate, and clearly must accelerate. We can follow
                self.switch_state(PlannerState.follow_speed_forward)
            raise lu.StateJump()
        self.max_speed = ly.Guard2(
            call=max_speed_call, jump=max_speed_jump, far_side=True, name="FA::max speed")


@lu.SharedSuperObjectSubclass(MaxSpeedPlanner)
class ForwardFollower(object):

    """
    follow_speed_forward (ForwardFollower):
      - (solver_change) solver state change -> follow_speed_forward (new state)
      - (forced_acceleration) reach forced acceleration -> search_ahead(prepped)
      - (forced_deceleration) reach forced deceleration -> accelerate_forward
      - (end_of_path) reaches max_xi -> decelerate_back(start_of_followable = (max_xi, 0.0, final=True))
    """

    def __init__(self):
        self.setup_guards()
        self._events = [self.end_of_path, self.forced_accel, self.forced_decel, self.solver_change]
        self.max_xi = self.world.com_path.max_xi
        self.max_xi_dot = self.world.robot.max_speed

    def init(self):
        print "initializing FF at ", self.xi, self.xi_dot
        new_sstate = self.solvers.get_followable_sstate(0.0)
        self.start_record(new_sstate)
        self.solvers.follow_prior_sstate = new_sstate

    @property
    def events(self):
        return self._events

    def __call__(self):
        return (self.max_xi_dot, 0.0, 1.0)

    def start_record(self,sstate):
        if sstate.widest_range_followable_stance==None:
            index = (sstate.slowest_accel_stance,sstate.slowest_decel_stance)
            self.result.start_forward(CommandEnum.chatter, index, sstate)
        else:
            index = sstate.widest_range_followable_stance
            self.result.start_forward(CommandEnum.zero, index, sstate)

    # def update(self):
    #     self.unfinished_segment.update(self.xi, self.xi_dot, self.t)
    def setup_guards(self):

        def solver_change_call():
            if self.xi > self.max_xi or self.solvers.min_xi_ddot() > 0.0 or self.solvers.max_xi_ddot() < 0.0:
                return lu.EventThrowIf[False]
            return self.solvers.followable_guard.call()

        def solver_change_jump():
            self.solvers.followable_guard.jump()
            self.start_record(self.solvers.follow_prior_sstate)
            self._events = self.events[0:]
            raise lu.Jump()

        self.solver_change = ly.Guard2(
            call=solver_change_call, jump=solver_change_jump, name="FF::solver change")

        def end_of_path_call():
            return lu.EventThrowIf[self.xi >= self.max_xi]

        def end_of_path_jump():
            # self.result.forward_segments.append(
            #     self.unfinished_segment.finish())
            self.result.interpolate_forward()
            self.state = self.sigma, (float(self.max_xi), 0.0, 0.0)
            self.switch_state(PlannerState.decelerate_back)
            raise lu.StateJump()
        self.end_of_path = ly.Guard2(
            call=end_of_path_call, jump=end_of_path_jump, name="FF::end of path")

        def forced_accel_call():
            if self.xi >= self.max_xi:
                return lu.EventThrowIf[False]
            return self.solvers.throw_if(self.solvers.min_xi_ddot() > 0.0)

        def forced_accel_jump():
            self.solvers.shift_delta()
            self.result.interpolate_forward()
            self.switch_state(PlannerState.search_ahead)
            raise lu.StateJump()
        self.forced_accel = ly.Guard2(
            call=forced_accel_call, jump=forced_accel_jump, far_side=True, name="FF::forced accel")

        def forced_decel_call():
            if self.xi >= self.max_xi:
                return lu.EventThrowIf[False]
            return lu.EventThrowIf[self.solvers.max_xi_ddot() < 0.0]

        def forced_decel_jump():
            self.switch_state(PlannerState.accelerate_forward)
            raise lu.StateJump()
        self.forced_decel = ly.Guard2(
            call=forced_decel_call, jump=forced_decel_jump, far_side=True, name="FF::forced decel")


@lu.SharedSuperObjectSubclass(MaxSpeedPlanner)
class BackwardDecelerator(object):

    """
    decelerate_back (BackwardDecelerator):
      - (intersection) reach intersection while not final -> update history -> follow_speed_forward(start_of_followable)
      - (intersection) reach intersection and final -> finish history -> return success!
      - (max_speed) reach max speed (while decelerating) -> update history (no good path) -> follow_speed_forward(start_of_followable)
      + (solver_change) solver state change -> decelerate_back (new state)
    """

    def __init__(self):
        self.setup_guards()
        self._events = [self.intersection, self.solver_change, self.min_speed]
        # self.trajectory = self.result
        # self.events.append(self.max_speed)
        # self.events.append(solver_change)

    def __call__(self):
        return (-self.xi_dot, -self.solvers.easy_min_xi_ddot(), -1.0)

    @property
    def events(self):
        return self._events

    def init(self):
        self.solvers.forward = False
        sstate = self.solvers.min_xi_ddot_sstate()
        index = sstate.best_stances[0]
        self.result.start_reverse(CommandEnum.min, index, sstate)
        self.solvers.min_xi_ddot_prior_sstate = sstate

    def setup_guards(self):

        def solver_change_call():
            return self.solvers.min_xi_ddot_guard()

        def solver_change_jump():
            self.solvers.min_xi_ddot_guard.jump()
            # self.trajectory.temp_reverse_segments.append(
            #     self.unfinished_segment.reverse_finish())
            index = self.solvers.min_xi_ddot_prior_sstate.best_stances[0]
            self.result.start_reverse(
                CommandEnum.min, index, self.solvers.min_xi_ddot_prior_sstate)
            # print self.result.all_segments_ever
            # self.unfinished_segment = UnfinishedTrajectorySegment(
            #     CommandEnum.min, index)
            self._events = self.events[0:]
            raise lu.Jump()
        self.solver_change = ly.Guard2(
            call=solver_change_call, jump=solver_change_jump, name="DB::solver change")

        def min_speed_call():
            return ly.EventThrowIf[self.xi_dot<0.0]
        def min_speed_jump():
            self.result.failure()
            raise ly.StopIntegration()
        self.min_speed = ly.Guard2(call=min_speed_call, jump=min_speed_jump, name="DB::min speed")

        def intersection_call():
            xi_dot = self.result.plan
            if xi_dot.xi_max > self.xi:
                other_speed = xi_dot(self.xi)
                return lu.EventThrowIf[xi_dot(self.xi) <= self.xi_dot]
            return lu.EventThrowIf[False]

        def intersection_jump():
            # seg = self.unfinished_segment.reverse_finish()
            # self.trajectory.temp_reverse_segments.append(seg)
            self.result.splice_reverse_trajectory(self.xi)
            if self.critical_point == None:
                print "we are done!"
                raise lu.StopIntegration()
            else:
                print "hopping back to critical point"
                xi, xi_dot = self.critical_point
                self.solvers.state = self.sigma, (xi, xi_dot, 0.0)
                print "critical point is still the same, right!!? (%.3f, %.3f)" % self.critical_point
                print "we are at the critical point, no? (%.3f, %.3f)" % (self.xi, self.xi_dot)
                self.critical_point = None
                print self.solvers.get_followable_sstate(0.0)
                if self.solvers.is_followable(0.0):
                    self.switch_state(PlannerState.follow_speed_forward)
                else:
                    print self.solvers.get_chatter_sets(0.0)
                    print "we are about to chatter"
                    self.switch_state(PlannerState.follow_speed_forward)
                raise lu.StateJump()
        self.intersection = ly.Guard2(
            call=intersection_call, jump=intersection_jump, name="DB::intersection")


@lu.SharedSuperObjectSubclass(MaxSpeedPlanner)
class ForwardSearcher(object):

    """
    search_ahead (ForwardSearcher):
      + (forced_acceleration) reaches forced acceleration -> search_ahead (prepped)
      - (forced_deceleration) reaches forced decel while prepped -> search_ahead (not prepped)
      - (followable_region) reaches followable region while prepped -> decelerate_back (start_of_followable = start_of_followable)
      - (end_of_path) reaches max_xi -> decelerate_back(start_of_followable = (max_xi, 0.0, final=True))
      + (solver_change) change in the searching sstate -> [internal]
    """

    def __init__(self):
        self.setup_guards()
        self.max_xi = self.world.com_path.max_xi
        self.max_xi_dot = self.world.robot.max_speed
        self._events = []

    @property
    def events(self):
        return self._events

    def __call__(self):
        return (1.0, 0.0, 1.0)

    def init(self):
        self._events = [self.detect_critical_point, self.end_of_path, self.solver_change]

        sstate = self.solvers.get_followable_sstate(0.0)
        self.result.start_pseudo(CommandEnum.search, sstate)
        self.solvers.follow_prior_sstate = sstate

        # self.result.start_forward(CommandEnum.search, -1,  True)
        for event in self.events:
            assert event() < 0
        print "initiated SA"

    def setup_guards(self):

        def prep_call():
            return self.solvers.throw_if(self.solvers.min_xi_ddot() > 1e-7)

        def prep_jump():
            # self.solvers.shift_delta()
            # self.result.start_forward(CommandEnum.search, -1, True)
            self._events = [detect_critical_point, self.end_of_path]
            raise lu.StateJump()
        self.prep = ly.Guard2(call=prep_call, jump=prep_jump, name="SA::prep")

        def solver_change_call():
            if self.xi > self.max_xi or self.solvers.min_xi_ddot() < -1e-7 or self.solvers.min_xi_ddot() > 1e-7:
                return lu.EventThrowIf[False]
            return self.solvers.followable_guard.call()

        def solver_change_jump():
            self.solvers.followable_guard.jump()
            self.result.start_pseudo(CommandEnum.search, self.solvers.follow_prior_sstate)
            self._events = self.events[0:]
            raise lu.Jump()

        self.solver_change = ly.Guard2(
            call=solver_change_call, jump=solver_change_jump, name="SA::solver change")

        def detect_critical_point_call():
            if self.solvers.min_xi_ddot() < -1e-7:
                print self.solvers.get_followable_sstate(0.0)
            return self.solvers.throw_if(self.solvers.min_xi_ddot() < -1e-7)

        def detect_critical_point_jump():
            # self.result.start_forward(CommandEnum.search, -1, True)
            # self.result.update(self.xi, self.max_xi, 0.0)
            assert (self.max_xi_dot == self.xi_dot)
            self.critical_point = (self.xi, self.xi_dot)
            print "saving critical point! (%0.3f, %0.3f)" % self.critical_point
            print self.solvers.get_followable_sstate(0.0)
            self.switch_state(PlannerState.decelerate_back)
            raise lu.StateJump()
        self.detect_critical_point = ly.Guard2(
            call=detect_critical_point_call, jump=detect_critical_point_jump, far_side=True, name="SA::detect critical point")

        def end_of_path_call():
            return self.solvers.throw_if(self.xi >= self.max_xi)

        def end_of_path_jump():
            self.state = self.sigma, (float(self.max_xi), 0.0, 0.0)
            self.switch_state(PlannerState.decelerate_back)
            raise lu.StateJump()
        self.end_of_path = ly.Guard2(
            call=end_of_path_call, jump=end_of_path_jump, name="SA::end of path")

import matplotlib.pyplot as plt


def original_plot(plan, world):
    for seg in plan:
        xis = np.linspace(seg.xi_min, seg.xi_max, 1000)
        print seg.command, ":"
        print seg.xi_min, seg.xis[0]
        print seg.xi_max, seg.xis[-1]
        print seg.xis
        print
        assert seg.xi_max <= seg.xis[-1]
        assert seg.xi_min >= seg.xis[0]

    phase_fig = plt.figure(figsize=(5, 3.5))
    phase_ax = phase_fig.add_subplot(111)
    phase_ax.set_title(r"$\rmPhase\,Space$", fontsize=16)
    phase_ax.set_xlabel(r"$\xi$", fontsize=14)
    phase_ax.set_ylabel(r"$\dot\xi$", fontsize=14)
    for seg in plan:
        xis = np.linspace(seg.xi_min, seg.xi_max, 1000)
        xi_dots = [seg(xi) for xi in xis]
        if seg.command == CommandEnum.max:
            plt.plot(xis[0], xi_dots[0], color=ut_burnt_orange,
                     marker='o', ms=4, mew=1, markeredgecolor='w')
            plt.plot(xis, xi_dots, lw=4, color=ut_burnt_orange,
                     linestyle='-.', dash_capstyle='round', solid_capstyle='round')
            plt.plot(xis[-1], xi_dots[-1], color=ut_burnt_orange,
                     marker='o', ms=10, mew=1, markeredgecolor='w')
        if seg.command == CommandEnum.zero:
            plt.plot(xis[0], xi_dots[0], color=ut_green,
                     marker='o', ms=4, mew=1, markeredgecolor='w')
            plt.plot(xis, xi_dots, lw=2, color=ut_green, linestyle='-',
                     dash_capstyle='butt', solid_capstyle='butt')
            plt.plot(xis, xi_dots, lw=5, color=ut_green, linestyle='--',
                     dash_capstyle='butt', solid_capstyle='butt')
            plt.plot(xis[-1], xi_dots[-1], color=ut_green,
                     marker='o', ms=10, mew=1,  markeredgecolor='w')
        if seg.command == CommandEnum.chatter:
            plt.plot(xis[0], xi_dots[0], color=ut_brown,
                     marker='o', ms=4, mew=1, markeredgecolor='w')
            plt.plot(xis, xi_dots, lw=2, color=ut_brown, linestyle='-',
                     dash_capstyle='butt', solid_capstyle='butt')
            plt.plot(xis, xi_dots, lw=5, color=ut_brown, linestyle='--',
                     dash_capstyle='butt', solid_capstyle='butt')
            plt.plot(xis[-1], xi_dots[-1], color=ut_brown,
                     marker='o', ms=10, mew=1,  markeredgecolor='w')
        if seg.command == CommandEnum.min:
            plt.plot(xis[0], xi_dots[0], color=ut_blue,
                     marker='o', ms=4, mew=1, markeredgecolor='w')
            plt.plot(xis, xi_dots, lw=4, color=ut_blue, solid_capstyle='round')
            plt.plot(xis[-1], xi_dots[-1], color=ut_blue,
                     marker='o', ms=10, mew=1, markeredgecolor='w')
    phase_ax.axis([-0.1, plan.xi_max + 0.1, -0.1, world.robot.max_speed + .1])
    phase_fig.tight_layout()


def plot_all(segs, world):
    phase_fig = plt.figure(figsize=(5, 3.5))
    phase_ax = phase_fig.add_subplot(111)
    phase_ax.set_title(r"$\rmPhase\,Space$", fontsize=16)
    phase_ax.set_xlabel(r"$\xi$", fontsize=14)
    phase_ax.set_ylabel(r"$\dot\xi$", fontsize=14)
    for seg in segs:
        xis = np.linspace(seg.xi_min, seg.xi_max, 1000)
        xi_dots = [seg(xi) for xi in xis]
        if seg.command == CommandEnum.max:
            plt.plot(xis[0], xi_dots[0], color=ut_burnt_orange,
                     marker='o', ms=4, mew=1, markeredgecolor='w')
            plt.plot(xis, xi_dots, lw=4, color=ut_burnt_orange,
                     linestyle='-.', dash_capstyle='round', solid_capstyle='round')
            plt.plot(xis[-1], xi_dots[-1], color=ut_burnt_orange,
                     marker='o', ms=10, mew=1, markeredgecolor='w')
        elif seg.command == CommandEnum.zero:
            plt.plot(xis[0], xi_dots[0], color=ut_green,
                     marker='o', ms=4, mew=1, markeredgecolor='w')
            plt.plot(xis, xi_dots, lw=2, color=ut_green, linestyle='-',
                     dash_capstyle='butt', solid_capstyle='butt')
            plt.plot(xis, xi_dots, lw=5, color=ut_green, linestyle='--',
                     dash_capstyle='butt', solid_capstyle='butt')
            plt.plot(xis[-1], xi_dots[-1], color=ut_green,
                     marker='o', ms=10, mew=1,  markeredgecolor='w')
        elif seg.command == CommandEnum.chatter:
            plt.plot(xis[0], xi_dots[0], color=ut_brown,
                     marker='o', ms=4, mew=1, markeredgecolor='w')
            plt.plot(xis, xi_dots, lw=2, color=ut_brown, linestyle='-',
                     dash_capstyle='butt', solid_capstyle='butt')
            plt.plot(xis, xi_dots, lw=5, color=ut_brown, linestyle='--',
                     dash_capstyle='butt', solid_capstyle='butt')
            plt.plot(xis[-1], xi_dots[-1], color=ut_brown,
                     marker='o', ms=10, mew=1,  markeredgecolor='w')
        elif seg.command == CommandEnum.min:
            plt.plot(xis[0], xi_dots[0], color=ut_blue,
                     marker='o', ms=4, mew=1, markeredgecolor='w')
            plt.plot(xis, xi_dots, lw=4, color=ut_blue, solid_capstyle='round')
            plt.plot(xis[-1], xi_dots[-1], color=ut_blue,
                     marker='o', ms=10, mew=1, markeredgecolor='w')
        else:
            print "unhandled command: %s" % repr(seg.command)
    phase_ax.axis([-0.1, plan.xi_max + 0.1, -0.1, world.robot.max_speed + .1])
    phase_fig.tight_layout()

import cPickle as pkl
if __name__ == '__main__':
    import multi_contact.worlds.world8 as world

    if True:
        planner = MaxSpeedPlanner(world)
        plan = planner.integrate()
        # segs = planner.result.all_segments_ever
        # print segs
        # for seg in segs:
        #     seg.delete_functions()
        with open("msp%s.pkl" % world.name, 'w') as fil:
            plan.delete_functions()
            pkl.dump(plan, fil)
            plan.build_functions()
    else:
        with open("msp%s.pkl" % world.name, 'r') as fil:
            plan = pkl.load(fil)
            plan.build_functions()

    # for seg in segs:
    #     seg.build_functions()
    # print segs
    from PlannerResult import TrajectorySegment
    for seg in plan:
        print seg
        cop = TrajectorySegment.copy(seg)
        print cop
        print cop.dstate
    print plan
    # original_plot(plan, world)
    # plot_all(segs, world)

    print plan.leaf(0.24).stance_index
    plt.show()
