import lyapunov as ly
import ly_utils as lu
import numpy as np
from gu.colormaps import *
from multi_contact.solvers.SolverListSystem import SolverListSystem, GuardedSolverList
from PlannerResult import PlannerResult
from multi_contact.solvers.CommandEnum import CommandEnum
from enum import Enum
@lu.named_state_system("PathTimeState", ['xi', 'xi_dot', 't'], xname='xi_t_state', tname='sigma')
class ChatterResolver(lu.HybridAutomata):

    def __init__(self, world):
        self._world = world
        self._result = PlannerResult()
        self._solvers = GuardedSolverList(world)
        self._critical_point = None
        # super(MaxSpeedPlanner, self).__init__(
        #     PlannerState.accelerate_forward,
        #     ForwardAccelerator(self))
        # self.add_system(
        #     PlannerState.follow_speed_forward,
        #     ForwardFollower(self))
        # self.add_system(
        #     PlannerState.decelerate_back,
        #     BackwardDecelerator(self))
        # self.add_system(
        #     PlannerState.search_ahead,
        #     ForwardSearcher(self))

    @property
    def critical_point(self):
        return self._critical_point

    @critical_point.setter
    def critical_point(self, value):
        self._critical_point = value

    @property
    def result(self):
        return self._result

    @property
    def world(self):
        return self._world

    @property
    def solvers(self):
        return self._solvers

    @property
    def state(self):
        return self.solvers.state

    @state.setter
    def state(self, val):
        self.solvers.state = val

    def resolve(self, plan, seg_index, mode):
        pass

    def integrate(self):
        system = self
        stepper = ly.dormand_prince(system, np.linspace(0, 10, 1000))
        stepper.time_tolerance = 1e-4
        stepper.relative_tolerance = 1e-5
        stepper.absolute_tolerance = 1e-2

        for jump, sigma, state, guard in lu.hybrid_integrate(stepper):
            print self.automata_state, jump, sigma, state, guard.name if guard else "no-guards"
            self.result.update(self.xi, self.xi_dot, self.t)

        return self.result.plan

import cPickle as pkl
import multi_contact.worlds.world9B as world
from MaxSpeedPlanner import MaxSpeedPlanner, original_plot, plot_all
import matplotlib.pyplot as plt
if __name__ == '__main__':

    if False:
        planner = MaxSpeedPlanner(world)
        plan = planner.integrate()
        segs = planner.result.all_segments_ever
        print segs
        for seg in segs:
            seg.delete_functions()
        with open("msp%s.pkl" % world.name, 'w') as fil:
            plan.delete_functions()
            pkl.dump((plan, segs), fil)
            plan.build_functions()
    else:
        with open("msp%s.pkl" % world.name, 'r') as fil:
            plan, segs = pkl.load(fil)
            plan.build_functions()

    for seg in segs:
        seg.build_functions()
    print segs
    from PlannerResult import TrajectorySegment
    for i, seg in enumerate(plan):
        print i, seg
        # cop = TrajectorySegment.copy(seg)
        # print cop
        # print cop.dstate
    chatter_resolver = ChatterResolver(world)
    new_plan = chatter_resolver.resolve(plan, 6, None)

    # original_plot(plan, world)
    # plot_all(segs)

    print plan.leaf(0.24).stance_index
    plt.show()