"""
Base multicontact simulation graphics module.

This module includes an animator which uses matplotlib to animate
a simplem multicontact robot abstract model as it moves in 2D among
several platforms in space.
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation
import matplotlib.patches as mpatches
from AnimationSubPlot import AnimationSubPlot
from PhaseSpacePlot import PhaseSpacePlot
from CenterOfMassGraphic import CenterOfMassGraphic
from HeightSurfaceGraphic import HeightSurfaceGraphic
from StaticVector import StaticVector
from PlanAnimatorPlot import PlanAnimatorPlot
import cPickle as pkl
from gu.colormaps import *


class BaseAnimator(object):
    """Base animation class, holds the figure and the graphic objects."""
    def __init__(self, sim):
        """Setup the figure, and hold the simulation."""
        self.sim = sim
        my_dpi = 93.343524156
        my_dpi = 100.0

        last = (1097, 925)
        want = (1024, 864)
        self.fig = plt.figure(
            figsize=(1024 / my_dpi, 864 / my_dpi), dpi=my_dpi)
        self.generate_figure()
        self.fig.tight_layout()
        self.ani = None
        self.save_frames = []

    def init(self):
        self.graphic_returns = []
        self.graphic_objects = []
        for plot in self.plots:
            graphic_objects, graphic_returns = plot.get_graphic_objects()
            self.graphic_returns.extend(graphic_returns)
            self.graphic_objects.extend(graphic_objects)
        return self.graphic_returns

    def generate_figure(self):
        pass

    def init_animation(self):
        for plot in self.plots:
            plot.reset()
        return self.graphic_returns

    def _hard_init_animation(self):
        for plot in self.plots:
            plot.hard_reset()
        return self.graphic_returns

    def increment_time(self):
        return self.sim.increment_time()

    def animate(self, i):
        i = self.increment_time()
        if (i == 0 or i == 1):
            for plot in self.plots:
                plot.reset()
        # print i
        # self.sim.robot(self.sim.ts[self.sim.i], self.sim.data[self.sim.i,:])
        graphic_returns = []
        for graphic in self.graphic_objects:
            graphic_returns.extend(graphic.update(self.sim))

        return graphic_returns

    def resim(self):
        self.sim.simulate()
        with open("saved_sim.pkl", 'rw') as fil:
            pkl.dump(self.sim, fil)

    def start(self):
        """Display animated plot."""
        # self.sim.simulate()
        self.init()

        self.ani = animation.FuncAnimation(self.fig, self.animate, np.arange(1, len(self.sim.times)),
                                           interval=40, blit=True, init_func=self.init_animation)
        plt.show()

    def total_frames(self):
        return len(self.sim.times)
    

    def movie(self, name, title, fps=30):
        """Generate a mmpeg movie and save it as [name]."""
        self.init()
        # self.sim.simulate()
        try:
            writer_class = animation.writers['ffmpeg']
        except KeyError:
            writer_class = animation.writers['avconv']
        writer = writer_class(
            fps=fps, metadata=dict(artist='Gray Thomas', title=title), bitrate=5000)
        self.ani = animation.FuncAnimation(
            self.fig, self.animate, np.arange(1, self.total_frames()),
            interval=40, blit=True, init_func=self.init_animation)
        self.ani.save(name, writer=writer)
        # plt.show()

class PlanAnimator(BaseAnimator):
    def __init__(self, sim, segs, plan):
        """Setup the figure, and hold the simulation."""
        self.plan=plan
        self.segs=segs
        super(PlanAnimator, self).__init__(sim)

    def increment_time(self):
        self.plan_subplot.increment_time()
        return self.sim.increment_time()

    def generate_figure(self):
        self.plan_subplot = PlanAnimatorPlot(self.fig, self.sim, self.segs, self.plan, 1, 1, 1)
        self.plots=[self.plan_subplot]

    def total_frames(self):
        return self.plan_subplot.total_frames()

class MultiContactSimulationAnimator(BaseAnimator):

    """Base animation class, holds the figure and the graphic objects."""

    def __init__(self, sim, xlims=[0.0, 4.0], ylims=[0.0, 2.0]):
        """Setup the figure, and hold the simulation."""
        self.xlims = xlims
        self.ylims = ylims
        super(MultiContactSimulationAnimator, self).__init__(sim)

    def generate_figure(self):
        """return the figure."""
        nplots = 1

        self.animation_plot = AnimationSubPlot(
            self.fig, self.sim, 1, nplots, 1, xlims=self.xlims, ylims=self.ylims)
        self.plots = [self.animation_plot]
        return self.fig

    # anim_plot_specific
    def show_platforms(self, platforms):
        """setup platform graphic objects."""
        self.platforms_args = platforms
        for platform in platforms:
            self.animation_plot.add_graphic(platform)

    # anim_plot_specific
    def show_com_path(self, com_path, num_tangents=100, line_scale=1.0):
        """setup height surface and tangent graphics."""
        self.com_path_args = (com_path, num_tangents)
        delta_xi = com_path.max_xi * (1.0 / (num_tangents - 1))
        for j in range(0, num_tangents):
            xi = j * delta_xi
            point = com_path.x(xi)
            vector_3 = 0.1 * com_path.normal(xi)
            static_vector_3 = StaticVector(point, vector_3, color=ut_burnt_orange)
            self.animation_plot.add_graphic(static_vector_3)

        self.animation_plot.add_graphic(HeightSurfaceGraphic(com_path, lw=4*line_scale))

    def _reboot_graphics(self):
        raise NotImplementedError()
        graphic_objects = self.animation_plot.graphics
        self._generate_figure()
        self.show_platforms(self.platforms_args)
        self.show_com_path(
            self.com_path_args[0], num_tangents=self.com_path_args[1])
        for graphic in graphic_objects:
            self.animation_plot.add_graphic(graphic)
        self.init()
        self._hard_init_animation()

    def plot_frame(self, save_frame):
        """Display animated plot."""
        self.init()
        self.sim.simulate()

        self.init_animation()
        self.sim.i = save_frame - 1
        self.animate(save_frame)
        print "saving frame", "frame" + str(save_frame) + ".pdf"
        self.fig.savefig("frame" + str(save_frame) + ".pdf")

class RevolvingSuperPhaseSpace(MultiContactSimulationAnimator):

    def __init__(self, sim, xlims=[0.0, 4.0], ylims=[0.0, 2.0]):
        """Setup the figure, and hold the simulation."""
        super(RevolvingSuperPhaseSpace, self).__init__(sim, xlims=xlims, ylims=ylims)

    def generate_figure(self):
        """return the figure."""
        nplots = 2
        self.animation_plot = AnimationSubPlot(
            self.fig, self.sim, 1, nplots, 1, xlims=self.xlims, ylims=self.ylims)
        self.phase_space_plot = PhaseSpacePlot(
            self.fig, self.sim, 1, nplots, 2,
            [(0.0, 30, 30), (0.2, 32.0, 28), (0.4, 28.0, 30), (0.7, 32.0, 28), (10.0, 40, 10)])
        self.plots = [self.animation_plot, self.phase_space_plot]
        return self.fig


def default_init(self, axis):
    """Ceed control to graphic objects list."""
    graphic_returns = []
    for graphic in self.graphic_objects:
        graphic_returns.extend(graphic.init(axis))
    return graphic_returns


def default_update(self, sim):
    """Ceed control to graphic objects list."""
    graphic_returns = []
    for graphic in self.graphic_objects:
        graphic_returns.extend(graphic.update(sim))
    return graphic_returns


def revolving_demo():
    import multi_contact.worlds.world9B as world
    from MultiContactRobotGraphic import MultiContactRobotGraphic
    with open("../demos/sim_res_%s.pkl"%world.name, 'r') as fil:
        sim_res = pkl.load(fil)  # So fast!!
    # from multi_contact.sim.MultiContactNumericSimulator import SimulationResult
    # # sim_res = SimulationResult.fall()
    # sim_res = world.sim_res()

    animator = RevolvingSuperPhaseSpace(
        sim_res, xlims=world.xlims, ylims=world.ylims)
    animator.show_platforms(world.platforms)
    animator.show_com_path(world.com_path, num_tangents=0)
    # animator.show_com_path(world.com_path, num_tangents=10)
    animator.animation_plot.add_graphic(
        MultiContactRobotGraphic(world))
    animator.phase_space_plot.setup_raw_phase_space()
    animator.phase_space_plot.setup_phase_space_projection()
    animator.phase_space_plot.setup_sister_space_projection()
    # animator.phase_space_plot.plot_integration_data(plan)

    animator.start()
    # animator.init()
    # animator.init_animation()
    # sim_res.i = 120
    # animator.animate(600)
    plt.show()

def plan_demo():
    import multi_contact.worlds.world9B as world
    from MultiContactRobotGraphic import MultiContactRobotGraphic
    with open("../demos/sim_res_%s.pkl"%world.name, 'r') as fil:
        sim_res = pkl.load(fil)  # So fast!!
    with open("../demos/all_segs_%s.pkl"%world.name, 'r') as fil:
        segs = pkl.load(fil)
        for seg in segs:
            seg.build_functions()
    with open("../demos/plan_%s.pkl"%world.name, 'r') as fil:
        plan = pkl.load(fil)
        plan.build_functions()
    # from multi_contact.sim.MultiContactNumericSimulator import SimulationResult
    # # sim_res = SimulationResult.fall()
    # sim_res = world.sim_res()

    animator = PlanAnimator(
        sim_res, segs, plan)


    animator.start()
    plt.show()
def main():
    plan_demo()

if __name__ == '__main__':
    main()