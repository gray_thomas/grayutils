from BaseArrowGraphic import BaseArrowGraphic
from ForceVectorGraphic import ForceVectorGraphic


class ProjectedPointGraphic(BaseArrowGraphic):

    """
    A graphic expressing the projection to the height surface point of the robot.
    """

    def __init__(self, com_path, scale=1.0, head_size=0.5, **kwargs):
        """
        Initialize the graphic.

        scale converts between acceleration units and length.
        head_size scales the vector's non-length components.
        """
        super(ProjectedPointGraphic, self).__init__(
            head_size=head_size, **kwargs)
        self.com_path = com_path
        self.scale = scale

    def update(self, sim):
        com_xdd, com_ydd = sim.list_of_robot_accel[sim.i]
        com_x, com_y, com_xd, com_yd = sim.data[sim.i, :]
        try:
            s = self.com_path.find_s(com_x)
            point = self.com_path._x(s)
            self.set_vertices(com_x, com_y, point[0, 0], point[1, 0])
        except LookupError:
            pass

        return [self.patch]
