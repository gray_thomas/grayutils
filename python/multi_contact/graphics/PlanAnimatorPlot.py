import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

from CenterOfMassGraphic import CenterOfMassGraphic
from HeightSurfaceGraphic import HeightSurfaceGraphic
from mpl_toolkits.mplot3d import axes3d
import multicontact_phase_space as mcps
from StaticVector import StaticVector
from matplotlib import cm
from multi_contact.planning.MaxSpeedPlanner import CommandEnum
from gu.colormaps import *


class SegGraphic(object):

    def __init__(self, seg, index):
        self.seg = seg
        self.index = index

    def init(self, ax):
        self.line2 = None
        seg = self.seg
        # xis = np.linspace(seg.xi_min, seg.xi_max, 30)
        xis=seg.xis
        max_xi = seg.xi_max
        xi_dots = [seg(xi) for xi in xis]
        max_xi_dot = max(xi_dots)
        self.xis = xis
        self.xi_dots = xi_dots
        self.forward = True
        fx=[-10]
        fy=[-10]

        if seg.command == CommandEnum.max:
            self.start, = ax.plot(
                fx, fy, color=ut_burnt_orange, marker='o', ms=8, mew=0.5, markeredgecolor='w')
            self.line, = ax.plot(
                fx, fy, lw=4, color=ut_burnt_orange, linestyle='-.', dash_capstyle='round', solid_capstyle='round')
            self.stop, = ax.plot(
                fx, fy, color=ut_burnt_orange, marker='o', ms=16, mew=0.5, markeredgecolor='w')

        if seg.command == CommandEnum.search:
            self.start, = ax.plot(
                fx, fy, color=ut_yellow, marker='o', ms=8, mew=0.5, markeredgecolor='w')
            self.line, = ax.plot(
                fx, fy, lw=4, color=ut_yellow, linestyle='--', dash_capstyle='round', solid_capstyle='round')
            self.stop, = ax.plot(
                fx, fy, color=ut_yellow, marker='o', ms=24, mew=0.5, markeredgecolor='w')

        if seg.command == CommandEnum.zero:
            self.start, = ax.plot(
                fx, fy, color=ut_green, marker='o', ms=8, mew=0.5, markeredgecolor='w')
            self.line, = ax.plot(
                fx, fy, lw=2, color=ut_green, linestyle='-', dash_capstyle='butt', solid_capstyle='butt')
            self.line2, = ax.plot(
                fx, fy, lw=5, color=ut_green, linestyle='--', dash_capstyle='butt', solid_capstyle='butt')
            self.stop, = ax.plot(
                fx, fy, color=ut_green, marker='o', ms=16, mew=0.5,  markeredgecolor='w')

        if seg.command == CommandEnum.chatter:
            self.start, = ax.plot(
                fx, fy, color=ut_brown, marker='o', ms=8, mew=0.5, markeredgecolor='w')
            self.line, = ax.plot(
                fx, fy, lw=2, color=ut_brown, linestyle='-', dash_capstyle='butt', solid_capstyle='butt')
            self.line2, = ax.plot(
                fx, fy, lw=5, color=ut_brown, linestyle='--', dash_capstyle='butt', solid_capstyle='butt')
            self.stop, = ax.plot(
                fx, fy, color=ut_brown, marker='o', ms=16, mew=0.5,  markeredgecolor='w')

        if seg.command == CommandEnum.min:
            self.xis = list(reversed(self.xis))
            self.xi_dots = list(reversed(self.xi_dots))
            self.stop, = ax.plot(
                fx, fy, color=ut_blue, marker='o', ms=16, mew=0.5, markeredgecolor='w')
            self.line, = ax.plot(
                fx, fy, lw=4, color=ut_blue, solid_capstyle='round')
            self.start, = ax.plot(
                fx, fy, color=ut_blue, marker='o', ms=8, mew=0.5, markeredgecolor='w')
            self.forward = False

        self.lines = [self.line]
        if self.line2 != None:
            self.lines.append(self.line2)
        return self.lines + [self.start, self.stop]

    def update(self, j):
        self.stop.set_data([self.xis[j], self.xi_dots[j]])
        self.start.set_data([self.xis[0], self.xi_dots[0]])
        for line in self.lines:
            if j==-1:
                line.set_data([self.xis[0:], self.xi_dots[0:]])
            else:
                line.set_data([self.xis[0:j+1], self.xi_dots[0:j+1]])
        return [self.stop, self.start]+self.lines


class PlanAnimatorPlot(object):

    def __init__(self, fig, sim, segs, plan, r, c, n):
        self.seg_ind = 0
        self.seg_part = 0
        self.segs = segs
        self.seg_graphics = [SegGraphic(seg, i) for i, seg in enumerate(segs)]

        self.ax = fig.add_subplot(
            r, c, n, autoscale_on=False)

        self.graphics = []
        self.ax.set_title(r"$\rmPhase\,Space$", fontsize=16)
        self.ax.set_xlabel(r"$\xi$", fontsize=14)
        self.ax.set_ylabel(r"$\dot\xi$", fontsize=14)
        print segs
        print "should be %d entries" % len(segs)
        # exit()
        max_xi = 0.0
        max_xi_dot = 0.0
        for seg in segs:
            print seg
            # print seg.stance_index
            # print seg.dstate
            xis = np.linspace(seg.xi_min, seg.xi_max, 1000)
            max_xi = max(max_xi, seg.xi_max)
            xi_dots = [seg(xi) for xi in xis]
            max_xi_dot = max(max(xi_dots), max_xi_dot)

        self.ax.axis([-0.1, max_xi + 0.1, -0.1, max_xi_dot + .1])
        self.graphics = [self]

    def init(self, axis):
        """Ceed control to segment graphics."""
        graphic_returns = []
        for graphic in self.seg_graphics:
            graphic_returns.extend(graphic.init(axis))
        return graphic_returns

    def update(self, sim):
        """Ceed control to segment graphics."""
        graphic_returns = []
        if self.seg_ind == 0 and self.seg_part == 0:
            for graphic in self.seg_graphics:
                graphic_returns.extend(graphic.update(0))
        else:
            for i in range(0, self.seg_ind):
                graphic_returns.extend(
                    self.seg_graphics[i].update(-1))
            graphic_returns.extend(
                self.seg_graphics[self.seg_ind].update(self.seg_part))
        return graphic_returns

    def increment_time(self):
        self.seg_ind += 0
        self.seg_part += 1
        if self.seg_part >= len(self.seg_graphics[self.seg_ind].xis):
            self.seg_part = 0
            self.seg_ind += 1
            if self.seg_ind >= len(self.segs):
                self.seg_ind = 0
        print self.seg_ind, self.seg_part

    def total_frames(self):
        return sum([len(g.xis) for g in self.seg_graphics])

    def get_graphic_objects(self):
        print "get_graphic_objects call"
        self.ax.set_title("Multi-Contact Phase Space")
        graphic_returns = []
        for graphic_object in self.graphics:
            graphic_returns.extend(graphic_object.init(self.ax))
        return self.graphics, graphic_returns

    def reset(self):
        self.ax.get_figure().canvas.draw()

    def hard_reset(self):
        for obj in self.graphics:
            obj.init(self.ax)
        self.ax.get_figure().canvas.draw()
