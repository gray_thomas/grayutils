from math import sqrt
screen_size = 23.6
resolution = (1920, 1080)
pixel_diagonal = sqrt(pow(resolution[0],2)+pow(resolution[1],2))
my_dpi = pixel_diagonal / screen_size
print my_dpi