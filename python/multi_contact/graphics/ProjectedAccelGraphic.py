from BaseArrowGraphic import BaseArrowGraphic
from ForceVectorGraphic import ForceVectorGraphic
from MultiContactGraphics import default_init, default_update


class ProjectedAccelGraphic(object):

    """
    A graphic expressing the acceleration required to keep the robot following the height surface.
    """

    def __init__(self, scale=1.0, normal_scale=1.0, normal_facecolor='navy', head_size=0.5, **kwargs):
        """
        Initialize the graphic.

        scale converts between acceleration units and length.
        head_size scales the vector's non-length components.
        """
        self.accel_graphic = BaseArrowGraphic(head_size=head_size, **kwargs)
        kwargs['facecolor'] = normal_facecolor
        self.normal_graphic = BaseArrowGraphic(head_size=head_size, **kwargs)
        self.graphic_objects = [self.normal_graphic, self.accel_graphic]
        self.normal_scale = normal_scale
        self.scale = scale

    def update(self, sim):
        com_xd, com_yd, com_xdd, com_ydd = sim.list_of_robot_accel[sim.i]
        com_x, com_y, com_xd, com_yd = sim.data[sim.i, :]
        i, path_s = sim.planner.height_surface.find(com_x)
        point = sim. planner.height_surface.point(i, path_s)
        normal_vector, projected_bias_accel = sim.planner.calculate_path_properties(
            sim.data[sim.i, :])
        self.normal_graphic.set_vertices(
            com_x, com_y, com_x + self.normal_scale * normal_vector[0, 0],
            com_y + self.normal_scale * normal_vector[1, 0])
        self.accel_graphic.set_vertices(
            com_x, com_y, com_x + self.scale * projected_bias_accel[0, 0],
            com_y + self.scale * projected_bias_accel[1, 0])

        return [self.normal_graphic.patch, self.accel_graphic.patch]
setattr(ProjectedAccelGraphic, "init", default_init)
