"""A line segment which can be moved around."""


class UpdatingLineSegment(object):

    """Base class for force vector, tanget, etc..."""

    def __init__(self, **kwargs):
        """Hold kwargs for line def."""
        self.kwargs = kwargs
        self.line = None

    def init(self, plt_ax):
        """setup graphics."""
        self.line, = plt_ax.plot([-999, -999], [-999, -999], *self.kwargs)
        return [self.line]

    def reposition(self, point_a, point_b):
        """move the line to span the two points."""
        self.line.set_data([
            point_a[0, 0],
            point_b[0, 0]], [
                point_a[1, 0],
                point_b[1, 0]])
        return [self.line]
