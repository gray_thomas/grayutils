"""A static object defined by a point, direction, and magnitude."""


class StaticVector(object):

    """Just a line in space."""

    def __init__(self, point, direction, **kwargs):
        """hold data."""
        self.kwargs = kwargs
        self.point = point
        self.direction = direction

    def init(self, plt_ax):
        """setup line."""
        plt_ax.plot([
            self.point[0, 0],
            self.point[0, 0] + self.direction[0, 0]], [
                self.point[1, 0],
                self.point[1, 0] + self.direction[1, 0]], **self.kwargs)
        return []

    def update(self, sim):
        """animate nothing."""
        return []
