import matplotlib.pyplot as plt
from gu.colormaps import *
from multi_contact.math.RegionSet import RegionSet

from MultiContactGraphics import default_init, default_update
from matplotlib.path import Path
import numpy as np
import matplotlib.patches as patches


class AnimatedStanceGraphic(object):

    def __init__(self, world, **kwargs):
        self.graphic_objects = []
        total_number = len(world.contact_data)
        for index, stance in enumerate(world.contact_data):
            p = index * 1.0 / (total_number - 1)
            color = tuple(
                [o * p + (1 - p) * b for b, o in zip(w2(ut_brown), w2(ut_burnt_orange))])
            line_color = tuple(
                [o * p + (1 - p) * b for b, o in zip(w0(ut_brown), w0(ut_burnt_orange))])
            self.graphic_objects.append(MultiContactStanceGraphic(
                stance, world.com_path, stance_index=index, color=color, line_color=line_color, **kwargs))

setattr(AnimatedStanceGraphic, "init", default_init)
setattr(AnimatedStanceGraphic, "update", default_update)


class MultiContactStanceGraphic(object):

    def __init__(self, stance, com_path, stance_index=0, color=w2(ut_brown), line_color=ut_brown, scale=(0.5 / (20 * 9.81)), size=3, s1=0.05, s2=0.075):
        self.stance = stance
        self.com_path = com_path
        # self.platforms = world.platforms
        self.scale = scale
        self.size = size
        self.triangles = []
        self.s1, self.s2 = s1, s2
        self.color = color
        self.line_color = line_color
        self.hideable_things = []
        self.stance_index = stance_index

    @classmethod
    def autocolor(cls, world, index, **kwargs):
        total_number = len(world.contact_data)
        p = index * 1.0 / (total_number - 1)
        color = tuple(
            [o * p + (1 - p) * b for b, o in zip(w2(ut_brown), w2(ut_burnt_orange))])
        line_color = tuple(
            [o * p + (1 - p) * b for b, o in zip(w0(ut_brown), w0(ut_burnt_orange))])
        return cls(world.contact_data[index], world.com_path, color=color, line_color=line_color, **kwargs)

    def get_points(self, contact):
        points = np.ones((3, 1)).dot(np.array([[contact.x_f, contact.y_f]]))
        basis1, basis2 = contact.basis()
        points[1, :] += self.scale * contact.max_force * np.array(basis1)[0:2]
        points[2, :] += self.scale * contact.max_force * np.array(basis2)[0:2]
        return points

    def init(self, ax, visible=False):
        # for platform in self.platforms:
        #     platform.init(ax)
        for contact in self.stance:
            self.hideable_things.extend(ax.plot([contact.x_f], [contact.y_f], 'o', color=self.color,
                                                ms=self.size * 4, markeredgewidth=self.size, markeredgecolor=self.line_color, linewidth=self.size))

            triangle = Triangle(
                facecolor=self.color, edgecolor=self.line_color, linewidth=self.size)
            self.hideable_things.extend(triangle.init(ax))
            triangle.anim_triangle(self.get_points(contact))
            # print contact.basis()
            # print contact.max_force
            # print dir(contact)
            self.triangles.append(triangle)

        xis = np.linspace(0.0, self.com_path.max_xi, 1000)
        xs, ys = [], []
        for xi in xis:
            x, y = self.com_path.x(xi)
            xs.append(x)
            ys.append(y)

        self.hideable_things.extend(
            ax.plot(xs, ys, linewidth=3 * self.size,
                    color=ut_ivory, solid_capstyle='round',zorder=-2)

        )
        for region in self.stance.xi_regions:
            # regions of valid path
            xis = np.linspace(float(region[0]), float(region[1]), 100)
            xs, ys = [], []
            for xi in xis:
                x, y = self.com_path.x(xi)
                xs.append(x)
                ys.append(y)
            self.hideable_things.extend(ax.plot(xs, ys, linewidth=3 * self.size,
                                                color=self.color, solid_capstyle='round',zorder=-1))
            # cute end tics
            for xi in region:
                x = self.com_path.x(xi)
                normal = -self.com_path.normal(xi)
                p0 = x + normal * self.s1
                p1 = x + normal * self.s2
                self.hideable_things.extend(
                    ax.plot([p0[0], p1[0]], [p0[1], p1[1]], color=self.line_color,
                            solid_capstyle='round', linewidth=self.size))
       
        for h in self.hideable_things:
            h.set_visible(visible)    
        return self.hideable_things

    def update(self, sim):
        visible = sim.stance_indicies[sim.i] == self.stance_index

        for h in self.hideable_things:
            h.set_visible(visible)
        return self.hideable_things

    def show_com_path(self, num_tangents=0, line_scale=1.0):
        """setup height surface and tangent graphics."""
        com_path = self.com_path
        self.com_path_args = (com_path, num_tangents)
        delta_xi = com_path.max_xi * (1.0 / (num_tangents - 1))
        graphics = []
        for j in range(0, num_tangents):
            xi = j * delta_xi
            point = com_path.x(xi)
            vector_3 = 0.1 * com_path.normal(xi)
            static_vector_3 = StaticVector(
                point, vector_3, color=ut_burnt_orange)
            graphics.append(static_vector_3)
        print
        graphics.append(HeightSurfaceGraphic(com_path, lw=4 * line_scale))
        return graphics

    @property
    def ylim(self):
        print[tri.ylim for tri in self.triangles]
        rs = RegionSet([tri.ylim for tri in self.triangles]).disjunction()
        return rs[0][0], rs[-1][1]

    @property
    def xlim(self):
        [tri.xlim for tri in self.triangles]
        rs = RegionSet([tri.xlim for tri in self.triangles]).disjunction()
        return rs[0][0], rs[-1][1]


class Triangle(object):

    def __init__(self, **kwargs):
        """Define the scale using the head_size parameter."""
        self.verts = np.ones((4, 2)) * -500

        self.patch = None
        self.kwargs = kwargs

    def init(self, plt_ax):
        """setup graphic objects."""
        self.plt_ax = plt_ax
        self.path = Path(self.verts,
                         [Path.MOVETO] + [Path.LINETO] * (2) + [Path.CLOSEPOLY])
        self.patch = patches.PathPatch(self.path, **self.kwargs)
        plt_ax.add_patch(self.patch)
        return [self.patch]

    def anim_triangle(self, points):
        """Move the points of the triangle. Redraw"""
        self.verts[0:3, :] = points
        self.verts[3, :] = points[0, :]
        # self.plt_ax.draw_artist(self.patch)
        return [self.patch]

    @property
    def xlim(self):
        print[min(self.verts[:, 0]), max(self.verts[:, 0])]
        return [min(self.verts[:, 0]), max(self.verts[:, 0])]

    @property
    def ylim(self):
        print[min(self.verts[:, 1]), max(self.verts[:, 1])]
        return [min(self.verts[:, 1]), max(self.verts[:, 1])]


if __name__ == '__main__':
    import multi_contact.worlds.world9B as world
    for index in range(0, len(world.contact_data)):
        print type(world.contact_data[0])
        fig = plt.figure(figsize=(5, 5))
        ax = fig.add_subplot(111, aspect='equal')
        graphic = MultiContactStanceGraphic.autocolor(world, index)
        print world.xlims
        for platform in world.platforms:
            platform.init(ax)
        graphic.init(ax, visible=True)
        print graphic.xlim
        ax.set_xlim(world.xlims_chibi)
        ax.set_ylim(world.ylims_chibi)
        plt.setp(ax.get_xticklabels(), visible=False)
        plt.setp(ax.get_yticklabels(), visible=False)
        # fig.savefig("world8_fig%d.pdf" % index)

        fig.savefig("../../../tex/MultiContactTAC2015/inkscape/World%sChibiSpace%d.pdf"%(world.name, index))
        # fig.savefig("../../../tex/MultiContactTAC2015/figures/World%sChibiSpace%d.png"%(world.name, index))
        # ax.margins(0.05)
    plt.show()
