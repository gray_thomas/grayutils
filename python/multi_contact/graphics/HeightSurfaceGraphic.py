"""Height surface graphic illustrates 2D bezier splines."""

import matplotlib.patches as mpatches
from gu.colormaps import ut_burnt_orange
from matplotlib.path import Path


class HeightSurfaceGraphic(object):

    """Bezier Line with tangents and normals."""

    def __init__(self, bezier2splines, lw=4):
        """hold onto bezier2splines object."""
        self.bezier2splines = bezier2splines
        self.verts = None
        self.codes = None
        self.path = None
        self.patch = None
        self.lw = lw

    def init(self, plt_ax):
        """setup graphics."""
        spline = self.bezier2splines
        num_points = len(spline.points)
        self.verts = [(spline.points[i, 0], spline.points[i, 1])
                      for i in range(0, num_points)]
        self.codes = [Path.MOVETO] + [Path.CURVE3] * (num_points - 1)
        self.path = Path(self.verts, self.codes)
        self.patch = mpatches.PathPatch(self.path,
                                        facecolor='none', lw=self.lw, edgecolor=ut_burnt_orange)
        plt_ax.add_patch(self.patch)
        return []

    def update(self, sim):
        """animate nothing."""
        return []
