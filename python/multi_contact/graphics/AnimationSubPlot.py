import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation
import matplotlib.patches as mpatches
from CenterOfMassGraphic import CenterOfMassGraphic
from HeightSurfaceGraphic import HeightSurfaceGraphic
from StaticVector import StaticVector


class AnimationSubPlot(object):

    def __init__(self, fig, sim, r, c, n, xlims, ylims):
        self.ax = fig.add_subplot(
            r, c, n, autoscale_on=False, xlim=xlims, ylim=ylims, aspect='equal')
        self.ax.grid()
        self.ax.set_title("2D Multi-Contact World")
        self.graphics = [self]
        self.sim = sim

    def get_graphic_objects(self):
        graphic_returns = []
        for graphic_object in self.graphics:
            graphic_returns.extend(graphic_object.init(self.ax))
        return self.graphics, graphic_returns

    def add_graphic(self, graphic):
        self.graphics.append(graphic)

    def init(self, axis):
        self.time_template = 'time = %.3fs'
        self.time_text = axis.text(0.05, 0.95, '', transform=axis.transAxes)
        self.time_text.set_text('')
        self.error_template = 'moment error = %.5f Nm'
        self.error_text = axis.text(0.05, 0.9, '', transform=axis.transAxes)
        self.error_text.set_text('')
        return [self.time_text, self.error_text]

    def update(self, sim):
        self.time_text.set_text(
            self.time_template % (self.sim.time))
        contact_records = sim.list_of_contact_record_lists[sim.i]
        if len(contact_records) == 0:
            self.error_text.set_text("Flying robot!")
        else:
            self.error_text.set_text(
                self.error_template % contact_records[0].error)

        return [self.time_text, self.error_text]

    def reset(self):
        self.ax.get_figure().canvas.draw()

    def hard_reset(self):
        for obj in self.graphics:
            obj.init(self.ax)
        self.ax.get_figure().canvas.draw()
