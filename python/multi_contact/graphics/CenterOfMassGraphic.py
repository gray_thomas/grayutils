from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.path import Path
from math import sqrt, atan2, pi


class CenterOfMassGraphic(object):

    """Shows the dowel pin symbol for the robot."""

    def __init__(self, size=0.0625, edge_thickness=1.35, **kwargs):
        """Setup empty variables."""
        self.wedge1 = None
        self.wedge2 = None
        self.circle = None
        self.origin = None
        self.size = size
        self.edge_thickness = edge_thickness
        self.kwargs= kwargs

    def init(self, axis):
        """Attach graphic object artists to the axis arg."""
        self.origin = [-10, -10]
        self.wedge1 = mpatches.Wedge(self.origin, self.size, 90, 180, color='k',**self.kwargs)
        self.wedge2 = mpatches.Wedge(self.origin, self.size, 270, 360, color='k',**self.kwargs)
        self.circle = mpatches.Circle(
            self.origin, self.size, facecolor='w', linewidth=self.edge_thickness, edgecolor='k',**self.kwargs)
        axis.add_patch(self.circle)
        axis.add_patch(self.wedge1)
        axis.add_patch(self.wedge2)
        return [self.circle, self.wedge1, self.wedge2]

    def update(self, sim):
        """Animate COM graphic to follow robot, using sim data."""
        center = (sim.data[sim.i, 0], sim.data[sim.i, 1])
        self.circle.center = center
        self.wedge1.center = center
        self.wedge1._recompute_path()
        self.wedge2.center = center
        self.wedge2._recompute_path()
        return [self.circle, self.wedge1, self.wedge2]
