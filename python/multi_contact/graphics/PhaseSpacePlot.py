import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

from CenterOfMassGraphic import CenterOfMassGraphic
from HeightSurfaceGraphic import HeightSurfaceGraphic
from mpl_toolkits.mplot3d import axes3d
import multicontact_phase_space as mcps
from StaticVector import StaticVector
from matplotlib import cm


class PhaseSpacePlot(object):

    def __init__(self, fig, sim, r, c, n, keyframes):
        self.ax = fig.add_subplot(
            r, c, n, autoscale_on=False, projection='3d')
        self.ax.grid()
        self.backwall = [-0.1, 0.0, -10.0]
        self.ax.set_xlim([self.backwall[0], 2])
        self.ax.set_ylim([self.backwall[1], 0.5])
        self.ax.set_zlim([self.backwall[2], 10])
        sd_wall, s_wall, sdd_wall = self.backwall
        self.ax.set_xlabel(r"$\dot s$", fontsize=16)
        self.ax.set_ylabel(r"$s$", fontsize=16)
        self.ax.set_zlabel(r"$\ddot s$", fontsize=16)
        # AnimatedPhaseSpaceDotGraphic
        # PhaseSpaceProjectionLineGraphic
        # AccelerationProjectionGraphic
        times, phis, thetas = zip(*keyframes)
        self.phi = interp1d(times, phis, 'cubic')
        self.theta = interp1d(times, thetas, 'cubic')
        self.graphics = [self]  # , mcps.Point(height_surface)
        self.sim = sim
        self.add_graphic(mcps.Point(projection=[s_wall, sd_wall, None]))
        self.add_graphic(mcps.Point(projection=[None, sd_wall, sdd_wall]))
        self.add_graphic(mcps.Point(projection=[s_wall, None, sdd_wall]))
        self.add_graphic(mcps.Point(projection=[s_wall, None, None]))
        self.add_graphic(mcps.Point(projection=[None, sd_wall, None]))
        self.add_graphic(
            mcps.Point(style='go', projection=[None, None, sdd_wall]))
        self.add_graphic(mcps.Point(style='bo', projection=[None, None, None]))

        self.add_graphic(
            mcps.Line(projectionA=[None, None, None], projectionB=[None, None, sdd_wall]))
        self.add_graphic(
            mcps.Line(projectionA=[None, None, None], projectionB=[None, sd_wall, None]))

    def get_graphic_objects(self):
        self.ax.set_title("Multi-Contact Phase Space")
        graphic_returns = []
        for graphic_object in self.graphics:
            graphic_returns.extend(graphic_object.init(self.ax))
        return self.graphics, graphic_returns

    def add_graphic(self, graphic):
        self.graphics.append(graphic)

    def init(self, axis):
        return []

    def update(self, sim):
        self.ax.view_init(self.phi(sim.time), self.theta(sim.time))
        return [self.ax]

    def reset(self):
        self.ax.get_figure().canvas.draw()

    def hard_reset(self):
        for obj in self.graphics:
            obj.init(self.ax)
        self.ax.get_figure().canvas.draw()

    def setup_phase_space_projection(self):
        dat = np.zeros(())
        s = self.sim.phase_space_data[:, 0]
        sdot = self.sim.phase_space_data[:, 1]
        # sddot = self.sim.phase_space_data[:,2]
        sddot = self.backwall[2] * np.ones(s.shape)
        self.ax.plot(sdot, s, sddot)

    def setup_sister_space_projection(self):
        dat = np.zeros(())
        s = self.sim.phase_space_data[:, 0]
        # sdot = self.sim.phase_space_data[:,1]
        sddot = self.sim.phase_space_data[:, 2]
        sdot = self.backwall[0] * np.ones(s.shape)
        self.ax.plot(sdot, s, sddot)

    def setup_raw_phase_space(self):
        dat = np.zeros(())
        s = self.sim.phase_space_data[:, 0]
        sdot = self.sim.phase_space_data[:, 1]
        sddot = self.sim.phase_space_data[:, 2]
        # sdot = self.backwall[0]*np.ones(s.shape)
        self.ax.plot(sdot, s, sddot)

    def plot_integration_data(self, plan):
        for seg in plan:
            self.ax.plot(np.array(seg.xi_dots), np.array(
                seg.xis), self.backwall[2] * np.ones(np.array(seg.xis).shape))

    def setup_3d_surface(self, surf_dat):
        surf_dat = np.array(surf_dat)
        for i in reversed(range(0, 4)):
            self.ax.plot_surface(surf_dat[i, :, :, 0], surf_dat[i, :, :, 1], surf_dat[i, :, :, 2],
                                 rstride=1, cstride=1, cmap=cm.CMRmap,
                                 vmin=-10, vmax=10, shade=True, alpha=0.85,
                                 linewidth=0, antialiased=False)
