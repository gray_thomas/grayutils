import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
from MultiContactGraphics import default_init, default_update
from math import sin, cos, pi, sqrt
import numpy as np
from BaseArrowGraphic import BaseArrowGraphic
from ForceVectorGraphic import ForceVectorGraphic
from COM_AccelVector import COM_AccelVector
from COM_VelocityVector import COM_VelocityVector
from ProjectedPointGraphic import ProjectedPointGraphic
from ProjectedAccelGraphic import ProjectedAccelGraphic
from CenterOfMassGraphic import CenterOfMassGraphic
from gu.colormaps import *
from StanceGraphic import AnimatedStanceGraphic


class MultiContactAppendageGraphic(object):

    """A graphic representing a noodly arm."""

    def __init__(self, name="", angle=0.0, scale=1.0, flip=0, lw=3):
        self.name = name
        self.angle = angle
        self.scale = scale
        self.flip = flip
        self.lw = lw

    def init(self, ax):
        th = pi / 180.0 * self.angle + 0.7
        self.def_verts = ((np.array([[0.25, 1.0], [0.75, 0.75], [0.50, 0.50]]) -
                           np.ones((3, 1)).dot(np.array([[0.25, 1.0]]))).dot(
            np.array([[cos(th), sin(th)], [-sin(th), cos(th)]]))) * self.scale
        if self.flip == 1:
            self.def_verts = self.def_verts.dot(np.diagflat([-1, 1]))
        self.verts = np.ones((3, 2)) * -5 + self.def_verts
        self.codes = [Path.MOVETO] + [Path.CURVE3] * (2)
        self.path = Path(self.verts, self.codes)
        self.patch = patches.PathPatch(self.path,
                                       facecolor='none', lw=self.lw, edgecolor='k')
        ax.add_patch(self.patch)
        return [self.patch]

    def update(self, sim):
        contacts = sim.contact_records

        x = sim.data[sim.i, 0]
        y = sim.data[sim.i, 1]
        self.verts[:, :] = self.def_verts + \
            np.ones((3, 1)).dot(np.array([[x, y]]))
        for contact in contacts:
            if contact.appendage == self.name:
                self.verts[2, :] = [contact.foot[0], contact.foot[1]]

        return [self.patch]


class MultiContactRobotGraphic(object):

    """A meta graphic-object comprising several limbs and contact forces."""

    def __init__(self, world, line_scale=1.0):
        """Generate everything."""
        self.graphic_objects = [
            AnimatedStanceGraphic(world),
            MultiContactAppendageGraphic(
                name="l_hand", angle=20.0, scale=0.6, flip=1, lw=3 * line_scale),
            MultiContactAppendageGraphic(
                name="r_hand", angle=170.0, scale=0.6, flip=1, lw=3 * line_scale),
            MultiContactAppendageGraphic(
                name="r_foot", angle=250.0, lw=3 * line_scale),
            MultiContactAppendageGraphic(
                name="l_foot", angle=290.0, lw=3 * line_scale),
            ForceVectorGraphic(0, scale=(0.5 / (20 * 9.81)), head_size=0.25),
            ForceVectorGraphic(1, scale=(0.5 / (20 * 9.81)), head_size=0.25),
            ForceVectorGraphic(2, scale=(0.5 / (20 * 9.81)), head_size=0.25),
            ForceVectorGraphic(3, scale=(0.5 / (20 * 9.81)), head_size=0.25),
            CenterOfMassGraphic(size=0.0625, edge_thickness=2.0 * line_scale),
            COM_AccelVector(scale=(0.5 / 9.81), head_size=0.25,
                            facecolor=ut_blue, alpha=0.9, edgecolor='none'),
            COM_VelocityVector(
                scale=0.1, head_size=0.25, facecolor=ut_yellow, alpha=0.9, edgecolor='none'),
            ProjectedPointGraphic(
                world.com_path, head_size=0.2, facecolor='pink', alpha=0.8, edgecolor='none'),
            # ProjectedAccelGraphic(scale=0.16, normal_scale=1.0, head_size=0.5, facecolor='maroon',alpha=0.8, edgecolor='none')
        ]

setattr(MultiContactRobotGraphic, "init", default_init)
setattr(MultiContactRobotGraphic, "update", default_update)
