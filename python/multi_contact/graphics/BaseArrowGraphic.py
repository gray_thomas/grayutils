"""A basic arrowhead graphic to be extended later."""


from matplotlib.path import Path
import numpy as np
import matplotlib.patches as patches


class BaseArrowGraphic(object):

    """A simple arrowhead graphic."""

    def __init__(self, head_size=0.5, **kwargs):
        """Define the scale using the head_size parameter."""
        self.verts = np.ones((8, 2)) * -500
        self.params = {
            'width_1': 0.2 * head_size,
            'height_1': 0.2 * head_size,
            'height_2': 0.15 * head_size,
            'width_2': 0.05 * head_size,
            'lmin': 0.4 * head_size}
        self.plt_ax = None
        self.path = None
        self.patch = None
        self.kwargs = kwargs

    def init(self, plt_ax):
        """setup graphic objects."""
        self.plt_ax = plt_ax
        self.path = Path(self.verts,
                         [Path.MOVETO] + [Path.LINETO] * (6) + [Path.CLOSEPOLY])
        self.patch = patches.PathPatch(self.path, **self.kwargs)
        plt_ax.add_patch(self.patch)
        return [self.patch]

    def set_vertices(self, x_1, y_1, x_2, y_2):
        """Move the vector to point from <x_1, y_1> to <x_2, y_2>."""
        delta_x = x_2 - x_1
        delta_y = y_2 - y_1
        length = np.sqrt(delta_x * delta_x + delta_y * delta_y)
        if length == 0.0:
            self.verts[:, :] = np.ones((8, 2)) * -50
            return
        cos = delta_x / length
        sin = delta_y / length
        width_1 = self.params['width_1']
        width_2 = self.params['width_2']
        height_1 = self.params['height_1']
        height_2 = self.params['height_2']
        lmin = self.params['lmin']
        if length > lmin:
            self.verts[0:7, :] = (
                np.array([
                    [0.0, -width_2 * 0.5],
                    [length - height_2, -width_2 * 0.5],
                    [length - height_1, -width_1 * 0.5],
                    [length, 0.0],
                    [length - height_1, width_1 * 0.5],
                    [length - height_2, width_2 * 0.5],
                    [0.0, width_2 * 0.5]
                ]).dot(np.array([[cos, sin], [-sin, cos]])) +
                np.ones((7, 1)).dot(np.array([[x_1, y_1]])))
        else:
            self.verts[0:7, :] = (
                (length / lmin) * np.array([
                    [0.0, -width_2 * 0.5],
                    [lmin - height_2, -width_2 * 0.5],
                    [lmin - height_1, -width_1 * 0.5],
                    [lmin, 0.0],
                    [lmin - height_1, width_1 * 0.5],
                    [lmin - height_2, width_2 * 0.5],
                    [0.0, width_2 * 0.5]
                ]).dot(np.array([[cos, sin], [-sin, cos]])) +
                np.ones((7, 1)).dot(np.array([[x_1, y_1]])))
