
"""A force vector graphic to display historic contact records from the sim."""

from BaseArrowGraphic import BaseArrowGraphic
from matplotlib.path import Path
import matplotlib.patches as patches
import numpy as np
from gu.colormaps import ut_green


class ForceVectorGraphic(BaseArrowGraphic):

    """Displays contact forces from simulation data."""

    def __init__(self, num, scale=1.0, head_size=0.5):
        """initialize using base arrow graphic."""
        super(ForceVectorGraphic, self).__init__(head_size=head_size,
                                                 facecolor=ut_green, alpha=0.9,
                                                 edgecolor='none')
        self.scale = scale
        self.num = num

    def update(self, sim):
        """animate graphic objects."""
        contact_records = sim.list_of_contact_record_lists[sim.i]
        if len(contact_records) > self.num:
            contact = contact_records[self.num]
            self.set_vertices(contact.foot[0, 0], contact.foot[1, 0],
                              contact.foot[0, 0] + self.scale *
                              contact.wrench[0, 0],
                              contact.foot[1, 0] + self.scale *
                              contact.wrench[1, 0])
            self.plt_ax.draw_artist(self.patch)
            # self.patch.set_xy(self.verts)
            if contact.error>1e-1:
                self.patch.set_facecolor('red')
            else:
                self.patch.set_facecolor(ut_green)
        else:
            self.verts[:, :] = np.ones((8, 2)) * -50

        return [self.patch]
