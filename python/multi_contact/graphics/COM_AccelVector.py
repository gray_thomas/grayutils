from BaseArrowGraphic import BaseArrowGraphic
from ForceVectorGraphic import ForceVectorGraphic


class COM_AccelVector(BaseArrowGraphic):

    """A graphic expressing the acceleration of the robot."""

    def __init__(self, scale=1.0, head_size=0.5, **kwargs):
        """
        Initialize the graphic.

        scale converts between acceleration units and length.
        head_size scales the vector's non-length components.
        """
        super(COM_AccelVector, self).__init__(
            head_size=head_size, **kwargs)
        self.scale = scale

    def update(self, sim):
        com_xdd, com_ydd = sim.list_of_robot_accel[sim.i]
        com_x, com_y, com_xd, com_yd = sim.data[sim.i, :]
        self.set_vertices(com_x, com_y,
                          com_x + self.scale * com_xdd,
                          com_y + self.scale * com_ydd)

        return [self.patch]
