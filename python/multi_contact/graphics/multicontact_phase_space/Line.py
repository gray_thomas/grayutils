from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.path import Path
from math import sqrt, atan2, pi


class Line(object):

    """Shows the point in phase space."""

    def __init__(self, projectionA=[None, None, None], projectionB=[0.0, 0.0, 0.0], style='k:', **kwargs):
        """Setup empty variables."""
        self.point = None
        self.psA, self.psdA, self.psddA = projectionA
        self.psB, self.psdB, self.psddB = projectionB
        self.kwargs = kwargs
        self.style = style

    def init(self, axis):
        """Attach graphic object artists to the axis arg."""
        self.point, = axis.plot(
            [-55], [-55], [-50], self.style, markersize=7, **self.kwargs)
        return [self.point]

    def update(self, sim):
        """Animate COM graphic to follow robot, using sim data."""
        sA, sdA, sddA = sim.phase_space
        sB, sdB, sddB = sA, sdA, sddA
        if self.psA != None:
            sA = self.psA
        if self.psdA != None:
            sdA = self.psdA
        if self.psddA != None:
            sddA = self.psddA
        if self.psB != None:
            sB = self.psB
        if self.psdB != None:
            sdB = self.psdB
        if self.psddB != None:
            sddB = self.psddB
        self.point.set_data([sdA,sdB], [sA,sB])
        self.point.set_3d_properties([sddA,sddB])
        return [self.point]
