from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.path import Path
from math import sqrt, atan2, pi


class Point(object):

    """Shows the point in phase space."""

    def __init__(self, projection=[None, None, None], style='r.', **kwargs):
        """Setup empty variables."""
        self.point = None
        self.ps, self.psd, self.psdd = projection
        self.kwargs = kwargs
        self.style = style

    def init(self, axis):
        """Attach graphic object artists to the axis arg."""
        self.point, = axis.plot(
            [-55], [-55], [-50], self.style, markersize=7, **self.kwargs)
        return [self.point]

    def update(self, sim):
        """Animate COM graphic to follow robot, using sim data."""
        s, sd, sdd = sim.phase_space
        if self.ps != None:
            s = self.ps
        if self.psd != None:
            sd = self.psd
        if self.psdd != None:
            sdd = self.psdd
        self.point.set_data([sd], [s])
        self.point.set_3d_properties([sdd])
        return [self.point]
