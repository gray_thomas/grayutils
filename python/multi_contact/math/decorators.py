import collections
import functools

class once_memoized(object):
   '''Decorator. Caches a function's most recent return value each time it is called.
   If called later with the same arguments, the cached value is returned
   (not reevaluated), but once the arguments change the cache is replaced.
   '''
   def __init__(self, func):
      self.func = func
      self.cached = None
      self.mem_return = None
   def __call__(self, *args):
      if args == self.cached:
         return self.mem_return
      else:
         value = self.func(*args)
         self.mem_return = value
         self.cached=args
         return value
   def __repr__(self):
      '''Return the function's docstring.'''
      return self.func.__doc__
   def __get__(self, obj, objtype):
      '''Support instance methods.'''
      return functools.partial(self.__call__, obj)
if __name__ == '__main__':
   @once_memoized
   def one_fibonacci(n):
      "Return the nth power of 2."
      if n == (0):
         return 1
      return one_fibonacci(n-1) + one_fibonacci(n-1)
   import time
   t=time.time()
   print [one_fibonacci(i) for i in range(0,25)]
   print time.time()-t, "seconds"
   for i in range(0,10):
      print one_fibonacci(20)
   print time.time()-t, "seconds"