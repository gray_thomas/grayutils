
OneRegionAtATimeError = type("OneRegionAtATimeError", (Exception,), {})
NoCurrentRegionException = type("NoCurrentRegionException", (Exception,), {})
BadRegionSetConstructor = type("BadRegionSetConstructor", (Exception,), {})
IncompleteRegionException = type("IncompleteRegionException", (Exception,), {})
class RegionSet(object):

    def __init__(self, base=None):
        if base == None:
            self.regions = []
        elif all([len(b) == 2 for b in base]):
            self.regions = base
        else:
            raise BadRegionSetConstructor()
        self.open = 0
        self.temp = None

    def __repr__(self):
        return self.__class__.__name__ + "(" + repr(self.regions) + '~' + repr(self.open) + ":" + repr(self.temp) + ")"

    def start_region(self, location):
        if self.open != 0:
            assert(location >= self.temp)
        else:
            self.temp = location
        self.open += 1

    def end_region(self, location):
        if self.open == 1:
            if location < self.temp:
                print location, self.temp
            assert (location >= self.temp)
            self.regions.append([self.temp, location])
            self.temp = None
        self.open -= 1
        if self.open < 0:
            raise NoCurrentRegionException()

    def __getitem__(self, index):
        return self.regions[index]

    def __add__(self, other):
        if self.open != 0:
            raise IncompleteRegionException()
        if other.open != 0:
            raise IncompleteRegionException()
        return RegionSet(base=self.regions + other.regions)

    def __len__(self):
        return len(self.regions)

    def __iter__(self):
        return iter(self.regions)

    def contains(self, s):
        for start, end in self.regions:
            if start <= s and end >= s:
                return True
        return False

    def strip(self):
        if self.open != 0:
            raise IncompleteRegionException()
        new_base = [[a, b] for a, b in self.regions if a != b]
        return RegionSet(base=new_base)

    def disjunction(self):
        events = []
        if self.open != 0:
            raise IncompleteRegionException()

        for region in self.regions:
            if region[1] > region[0]:
                events.append((region[0], True))
                events.append((region[1], False))
            else:
                events.append((region[0], False))
                events.append((region[1], True))
        # put opening events first, to resolve ties.
        events = sorted(events, key=lambda x: not x[1])
        events = sorted(events, key=lambda x: x[0])
        ret = RegionSet()
        for location, is_opening in events:
            if is_opening:
                ret.start_region(location)
            else:
                ret.end_region(location)
        return ret

    @classmethod
    def conjunction(cls, region_sets):
        events = []
        for i, region_set in enumerate(region_sets):
            assert(region_set.open == 0)
            for region in region_set:
                if region[1] > region[0]:
                    events.append((region[0], True, i))
                    events.append((region[1], False, i))
                else:
                    events.append((region[1], True, i))
                    events.append((region[0], False, i))
        events = sorted(events, key=lambda x: not x[1])
        events = sorted(events, key=lambda x: x[0])

        open_sets = [0] * len(region_sets)
        ret = cls()
        for location, is_opening, index in events:
            if is_opening:
                open_sets[index] += 1
                if all([o >= 1 for o in open_sets]) and open_sets[index] == 1:
                    ret.start_region(location)
            else:
                open_sets[index] -= 1
                if all([o >= 1 for i, o in enumerate(open_sets) if i != index]) and open_sets[index] == 0:
                    ret.end_region(location)
        return ret

    def negate(self, min_value=-float('inf'), max_value=float('inf')):
        ordered = self.disjunction()
        new_base = [[min_value]]
        for start, end in ordered.regions:
            start = max(min(start, max_value), min_value)
            end = max(min(end, max_value), min_value)
            new_base[-1].append(start)
            new_base.append([end])
        new_base[-1].append(max_value)
        return RegionSet(base=new_base)

    def invert(self, full_space):
        raise NotImplementedError()
if __name__ == "__main__":
    contact_1 = [[0.0, 1.2]]
    contact_2 = [[0.4, 0.6], [1.3, 1.5]]
    contact_3 = [[1.6, 2.6]]
    contact_4 = [[0.4, 0.6]]

    data = [contact_1, contact_2, contact_3, contact_4]

    def extended(x, y):
        x.extend(y)
        return x
    print reduce(extended, data, [])
    # print combine_regions(reduce(extended, data, []))

    region1 = RegionSet(base=contact_1)
    print region1.disjunction()
    print region1
    region1.start_region(1.0)
    print region1
    try:
        region1.disjunction()
    except IncompleteRegionException:
        print 'incomplete region'
    region1.end_region(2.0)
    print region1
    print region1.disjunction()
    print RegionSet.conjunction([region1, region1])
    print RegionSet.conjunction([region1, region1.negate()])
    print RegionSet.conjunction([region1, region1.negate()]).strip()
    print RegionSet.conjunction([region1, RegionSet(base=contact_4)])
    print RegionSet.conjunction([region1, RegionSet(base=contact_4), RegionSet(base=contact_3)])