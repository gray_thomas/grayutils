from Bezier2Splines import Bezier2Splines, convert_s
from math import sqrt
import numpy as np
from numpy import isnan
import lyapunov as ly
import ly_utils as lu
from math import sqrt
from CompositeFunctor import CompositeFunctor, wrapMonotonicallyIncreasingFunctions
from scipy.interpolate import interp1d
from decorators import once_memoized


@lu.named_state_system("ArcLengthState", ['xi'], xname="s_state", tname='s')
class ArcLengthGenerator(object):

    """Integrator for arclength."""

    def __init__(self, path):
        """Setup the system."""
        self.path = path
        self.max_s = self.path.max_path_s
        self.state = (0, (0.0,))

    def __call__(self):
        """Return expression for arc length differential."""
        dxds = self.path._x_prime(self.s)
        return sqrt(float(dxds.T.dot(dxds)[0,0])),

    def get_interpolating_functions(self, plot=False):
        logger = ly.Recorder(self)
        s2xi_list = []
        xi2s_list = []

        plot_data = []

        # range must include endpoint, range(0, 2) = [0, 1]
        for i in range(0, self.max_s):
            stepper = ly.dormand_prince(self, np.linspace(i, 1 + i, 200))
            stepper.relative_tolerance = 1e-7
            stepper.absolute_tolerance = 1e-5
            stepper.time_tolerance = 1e-7
            xi_vals, s_vals = [], []

            for path_s, events in stepper:
                assert path_s == self.s
                ly.check_NaN(self)
                xi_vals.append(self.xi)
                s_vals.append(self.s)

            plot_data.append(zip(s_vals, xi_vals))
            assert self.s == 1 + i
            s2xi_list.append( # TODO: optimizization candidate---50% of solver time at 0.766/1000
                interp1d(np.array(s_vals), np.array(xi_vals), kind='linear'))
            xi2s_list.append(
                interp1d(np.array(xi_vals), np.array(s_vals), kind='linear'))

        s2xi = once_memoized(CompositeFunctor(
            wrapMonotonicallyIncreasingFunctions(s2xi_list)))
        xi2s = once_memoized(CompositeFunctor(
            wrapMonotonicallyIncreasingFunctions(xi2s_list)))
        if plot:
            import matplotlib.pyplot as plt
            s_vec = np.linspace(0.0, self.max_s, 10000)
            xi_vec = np.vectorize(s2xi)(s_vec)
            plt.plot(s_vec, xi_vec)
            for li in plot_data:
                for s, xi in li:
                    plt.plot(s, xi, 'o')
            plt.show()

        return s2xi, xi2s


class ArcLength2Spline(Bezier2Splines):

    def __init__(self, points):
        super(ArcLength2Spline, self).__init__(points)
        self.s2xi, self.xi2s = self.get_arc_length_functions()

    @property
    def points(self):
        return self._points

    @points.setter
    def points(self, value):
        self._points = value
        self.max_path_s = (len(self._points) - 1) / 2
        self.s2xi, self.xi2s = self.get_arc_length_functions()



    @property
    def max_xi(self):
        return float(self.s2xi(self.max_path_s))

    @property
    def max_s(self):
        return float(self.max_path_s)

    def find_xi(self, x_line):
        """find the index and path variable by projection along y."""
        return self.s2xi(self.find_s(x_line))

    def get_arc_length_functions(self):
        arc_length_generator = ArcLengthGenerator(self)
        return arc_length_generator.get_interpolating_functions()

    def xi_dot2s_dot(self, xi, xi_dot):
        return self.xi2s(xi), xi_dot / np.linalg.norm(self.diff1(self.xi2s(xi)))

    def s_ddot2xi_ddot(self, s, s_dot, s_ddot):
        """convert from path_s state to arclength state."""
        xi, xi_dot = self.s_dot2xi_dot(s, s_dot)
        xp = self.dx_ds(xi)
        xpp = self.ddx_ds2(xi)
        xi_prime = float(sqrt(xp.T.dot(xp)))
        xi_dprime = float(xpp.T.dot(xp)) / xi_prime
        # assert xi_dot = xi_prime * s_dot
        xi_ddot = xi_dprime * pow(s_dot, 2) + xi_prime * s_ddot
        return xi, xi_dot, xi_ddot

    def xi_ddot2s_ddot(self, xi, xi_dot, xi_ddot):
        """convert form arclength state to path_s state."""
        s, s_dot = self.xi_dot2s_dot(xi, xi_dot)
        xp = self.x_prime(xi)
        xpp = self.diff2(xi)
        xi_prime = float(sqrt(xp.T.dot(xp)))
        xi_dprime = float(xpp.T.dot(xp)) / xi_prime
        s_ddot = (xi_ddot - xi_dprime * pow(s_dot, 2)) / xi_prime
        return s, s_dot, s_ddot

    def dx_ds(self, xi): # TODO: optimizization candidate---50% of solver time at 0.813/1000
        return self._x_prime(self.xi2s(xi))

    def ddx_ds2(self, xi):
        return self._x_double_prime(self.xi2s(xi))

    def x(self, xi):
        """calculate the location."""
        i, path_s = convert_s(self.xi2s(xi))
        p_0, p_1, p_2 = self.get_control_points(i)
        return p_0 * pow(1 - path_s, 2) + 2 * p_1 * (1 - path_s) * path_s + p_2 * pow(path_s, 2)

    def dxi_ds(self, xi):
        # sqrt ( x' . x')
        dx_ds = self.dx_ds(xi)
        return float(sqrt(dx_ds.T.dot(dx_ds)))

    def ddxi_ds2(self, xi):
        # 1/(2 sqrt ( dx_ds . dx_ds)) 2( ddx_ds2 . dx_ds)
        # ( x" . x')/sqrt ( x' . x')
        dx_ds = self.dx_ds(xi)
        ddx_ds2 = self.ddx_ds2(xi)
        ddxi_ds2 = float(ddx_ds2.T.dot(dx_ds) / sqrt(dx_ds.T.dot(dx_ds)))
        return ddxi_ds2

    def ds_dxi(self, xi):
        dxi_ds = self.dxi_ds(xi)
        return pow(dxi_ds, -1)

    def dds_dxi2(self, xi):
        dxi_ds = self.dxi_ds(xi)
        ds_dxi = self.ds_dxi(xi)
        ddxi_ds2 = self.ddxi_ds2(xi)
        dds_dxi2 = -1 * pow(dxi_ds, -2) * ddxi_ds2 * ds_dxi
        return dds_dxi2

    def dx_dxi(self, xi):
        """return the derivative vector wrt xi."""
        dx_ds = self.dx_ds(xi)
        ds_dxi = self.ds_dxi(xi)
        dx_dxi = dx_ds * ds_dxi
        return dx_dxi

    def ddx_dxi2(self, xi): # TODO: optimizization candidate---50% of solver time at 0.839/1000
        """return the second derivative vector wrt xi."""
        dx_ds = self.dx_ds(xi)
        ddx_ds2 = self.ddx_ds2(xi)
        ds_dxi = self.ds_dxi(xi)
        dds_dxi2 = self.dds_dxi2(xi)
        ddx_dxi2 = ddx_ds2 * pow(ds_dxi, 2) + dx_ds * dds_dxi2
        return ddx_dxi2

    def tangent(self, xi):
        """return a unit vector normal to the path."""
        dx_ds = self._x_prime(self.xi2s(xi))
        tangent = dx_ds / (np.linalg.norm(dx_ds))
        return tangent

    def normal(self, xi):
        """return a unit vector normal to the path."""
        dx_ds = self._x_prime(self.xi2s(xi))
        tangent = dx_ds / (np.linalg.norm(dx_ds))
        normal = np.array([[0, -1], [1, 0]]).dot(tangent)
        return normal

if __name__ == '__main__':
    _height_points = np.array([
        [0.25, 1.60],
        [0.50, 1.66],
        [0.75, 1.60],
        [0.80, 1.576],
        [0.95, 1.60]
    ])
    xlims = [-0.25, 1.25]
    ylims = [0, 2.5]
    com_path = ArcLength2Spline(_height_points)
    print com_path.s2xi(0.0)
    print com_path.s2xi(1.0)
    print com_path.s2xi(2.0)
    print com_path.xi2s(com_path.s2xi(0.0))
    print com_path.xi2s(com_path.s2xi(0.5))
    print com_path.xi2s(com_path.s2xi(1.0))
    print com_path.xi2s(com_path.s2xi(1.5))
    print com_path.xi2s(com_path.s2xi(2.0))
    print com_path.s2xi(0.0)
    print com_path.s2xi(com_path.max_s)
    print com_path.max_xi
