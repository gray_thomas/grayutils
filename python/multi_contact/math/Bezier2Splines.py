"""
Bezier spline class.

Define a way to represent height surfaces with differentiation.
"""

from math import sqrt
import numpy as np
from numpy import isnan
import lyapunov as ly
import ly_utils as lu
from math import sqrt
from scipy.interpolate import interp1d
from decorators import once_memoized

@once_memoized
def convert_s(s):
    i = int(s)
    path_s = s - i
    if i == s and i != 0:
        i -= 1
        path_s = 1.0
    return i, path_s


class Bezier2Splines(object):

    """A quadratic spline curve."""

    def __init__(self, points):
        """copy the points to memory."""
        self._points = points
        self.max_path_s = (len(self._points) - 1) / 2
        self._mem_i = None

    @property
    def points(self):
        return self._points

    @points.setter
    def points(self, value):
        self._points = value
        self.max_path_s = (len(self._points) - 1) / 2

    @once_memoized
    def find_s(self, x_line):
        """find s by projection along y."""
        for i in range(0, (len(self._points) - 1) / 2):
            p_0, p_1, p_2 = self.get_control_points(i)
            poly_a = p_2[0, 0] - 2 * p_1[0, 0] + p_0[0, 0]
            poly_b = 2 * p_1[0, 0] - 2 * p_0[0, 0]
            poly_c = p_0[0, 0] - x_line
            if abs(poly_a) < 1e-7:
                # non-quadratic case
                lin_s = -poly_c / poly_b
                if 0 <= lin_s and lin_s <= 1:
                    return i + lin_s
                continue
            det = poly_b * poly_b - 4 * poly_a * poly_c
            if det > 0:
                low_s = 0.5 * (-poly_b - sqrt(det)) / poly_a
                high_s = 0.5 * (-poly_b + sqrt(det)) / poly_a
                if 0 <= low_s and 1 >= low_s:
                    return i + low_s
                if 0 <= high_s and 1 >= high_s:
                    return i + high_s
        if x_line < min(self._points[0][0], self._points[-1][0]):
            return 0.0
        return self.max_path_s

    @once_memoized
    def get_control_points(self, i):
        """index the control point list."""
        if 2 * i + 2 > len(self._points):
            i = len(self._points) / 2 - 1
        if i < 0:
            i = 0
        p_0 = self._points[[2 * i], :].T
        p_1 = self._points[[2 * i + 1], :].T
        p_2 = self._points[[2 * i + 2], :].T
        return p_0, p_1, p_2

    @once_memoized
    def _x(self, s):
        """calculate the location."""
        i, path_s = convert_s(s)
        p_0, p_1, p_2 = self.get_control_points(i)
        return p_0 * pow(1 - path_s, 2) + 2 * p_1 * (1 - path_s) * path_s + p_2 * pow(path_s, 2)

    @once_memoized
    def _x_prime(self, s):
        """return the derivative vector wrt s."""
        i, path_s = convert_s(s)
        p_0, p_1, p_2 = self.get_control_points(i)
        return self._x_double_prime(s) * path_s + 2 * (p_1 - p_0)

    @once_memoized
    def _x_double_prime(self, s):
        """return the second derivative vector wrt s."""
        i, path_s = convert_s(s)
        p_0, p_1, p_2 = self.get_control_points(i)
        return (p_0 - 2 * p_1 + p_2) * 2
