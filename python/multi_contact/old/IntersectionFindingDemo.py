from intersection_demo.LinearLyapunovSystem import LinearLyapunovSystem
from IntersectionFindingSystem import IntersectionFindingSystem
def main():
	"""
	Test the intersection finding system.

	Generate two systems, simple linear systems.
	Integrate these two systems together and find the y at which they share an x
	the systems are parameterized with y acting like time.
	"""
	system1 = LinearLyapunovSystem(1.0)
	system2 = LinearLyapunovSystem(-1.0, sinit=1.0)
	system = IntersectionFindingSystem([system1, system2])

if __name__ == '__main__':
	main()