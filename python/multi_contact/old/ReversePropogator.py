import lyapunov as ly
import ly_utils as lu
import numpy as np
from exceptions import NotImplementedError
from numpy import sign
from math import sqrt
from OptimalMulticontactSolver import OptimalMulticontactSolver, InfeasibilityException
from enum import Enum
from collections import namedtuple


AutomataState = Enum(
    "AutomataState", "brake coast chatter search fail win")


# @lu.named_state_system("PathTimeState", ['xi', 'xi_dot', 't'], xname='xi_t_state', tname='sigma')
# class BaseSystem(object):

#     def __init__(self, world, forward=True):
#         self.state = 0.0, (0.0 if forward else world.com_path.max_path_s,
#                            0.0, 0.0)
#         self.world = world
#         self.forward = forward

#         self.active_index, xi_ddot = self.get_best_stance()
#         print[s.name for s in self.dstate.best_stance.contact_combination], "chosen at start!"
#         self.stance_start_state = State(
#             xi=self.xi, xi_dot=self.xi_dot, xi_ddot=float(xi_ddot), t=0.0)
#         self.hybrid_history = []
#         self.current_data = []
# self.lookup = None  # generate this after integrating
# self.termination = None  # based on how integration ends
#         self.max_jumps = 20
# self.switch_state = None  # method attached by owning HybridAutomata

# self.events = [
# lu.Guard(self.check_dstate, self.handle_dstate),
# lu.Guard(self.check_max_speed, self.handle_max_speed),
# lu.Guard(self.check_zero_speed, self.handle_zero_speed),
# lu.Guard(self.check_path_curvature, self.handle_path_curvature),
# lu.Guard(self.check_path_end, self.handle_path_end)]

#     @property
#     def speed(self):
#         return self.world.com_path.speed(self.s, self.s_dot)

#     def eval_stance(self, stance):
#         """Return s_ddot. Use an OptimalMulticontactSystem"""
#         self.solver.contact_combination = stance.contact_combination
#         try:
#             ret = self.solver.solve(self.s, self.s_dot)
#         except InfeasibilityException as e:
#             raise e
#         return ret.s_ddot

#     def record_datum(self):
#         self.current_data.append(
#             (self.s, self.s_dot, self.eval_stance(self.dstate.best_stance), self.t))

#     def __call__(self):
#         """Integrate the current stance."""
#         tp = 1.0 if self.forward_time else -1.0
#         try:
#             s_dotp = self.eval_stance(self.dstate.best_stance) * tp
#         except InfeasibilityException:
# print "infeasible call"
#             s_dotp = self.current_data[-1][2] * tp
# print "call", self.s, self.t
#         s_p = self.s_dot * tp
#         return s_p, s_dotp, tp

# Events should become negative when they trigger. They should start
# positive.

#     def check_max_speed(self):
#         return self.world.robot.max_speed - self.speed

#     def check_zero_speed(self):
#         return self.speed

#     def check_path_end(self):
# print "check path end"
#         return min(self.s, self.world.com_path.max_path_s - self.s)

#     def check_path_curvature(self):
# print "check path curvature"
#         path_i, path_s = convert_s(self.s)
#         if path_i % 2 == 1:
#             return 1.0
#         return -1.0

#     def check_dstate(self):
#         dstate, s_ddot = self.get_best_stance()
#         if dstate != self.dstate:
#             return -1.0
#         return 1.0

#     def punt_a_bit(self, dq=1e-6):
#         """Moves the system forward in q by a tiny increment."""
#         new_state = [float(s + dq * ds)
#                      for s, ds in zip(self.state[1], self())]
#         self.state = self.q + dq, new_state

#     def handle_max_speed(self):

#         raise StopIteration()

#     def handle_zero_speed(self):
#         raise ZeroSpeed()

#     def handle_path_end(self):
#         raise PathEnd()

#     def handle_new_active(self):
#         self.punt_a_bit()
#         new_active_index, new_s_ddot = self.get_best_stance()
#         assert new_active_index != self.active_index
#         if len(new_dstate.competitor_set) == 0:
#             self.termination = Terminations.infeasibility
#             raise Infeasibility()
#         if new_dstate.infeasible_set != self.dstate.infeasible_set:
#             self.termination = NonTerminationGuards.feasible_set
#             raise lu.StopContinuousIteration()
#         if new_dstate.best_stance != self.dstate.best_stance:
#             self.termination = NonTerminationGuards.best_stance
#             raise lu.StopContinuousIteration()
#         print "error handle dstate", new_dstate, self.dstate
#         raise NotImplementedError()

#     def handle_path_curvature(self):
#         self.termination = NonTerminationGuards.path_curve
#         raise lu.StopContinuousIteration()

# overly complex record keeping is getting axed.
#     def terminating_guard(self):
#         end_state = State(
#             s=self.s, s_dot=self.s_dot, s_ddot=self.current_data[-1][2],
#             t=self.t)
#         arc_data = [self.world.com_path.arcify_state(
#             s[0], s[1], s[2]) for s in self.current_data]
#         continuous_segment_record = ContinuousSegment(
#             start=self.stance_start_state, end=end_state, dstate=self.dstate,
#             data=np.array(self.current_data), arc_data=np.array(arc_data),
#             end_condition=self.termination)
#         self.hybrid_history.append(continuous_segment_record)
# print "this is where we would generate the interpolates for intersection
# finding."

#     def non_terminating_guard(self):
#         print "non_terminating_guard", self.termination
#         end_state = State(
#             s=self.s, s_dot=self.s_dot, s_ddot=self.current_data[-1][2],
#             t=self.t)
#         arc_data = [self.world.com_path.arcify_state(
#             s[0], s[1], s[2]) for s in self.current_data]
#         continuous_segment_record = ContinuousSegment(
#             start=self.stance_start_state, end=end_state, dstate=self.dstate,
#             data=np.array(self.current_data), arc_data=np.array(arc_data),
#             end_condition=self.termination)
#         self.hybrid_history.append(continuous_segment_record)
# self.punt_a_bit()  # cheats the system forward a tiny amount
#         self.dstate, s_ddot = self.get_best_stance()
#         self.stance_start_state = State(
#             s=self.s, s_dot=self.s_dot, s_ddot=float(s_ddot),
#             t=self.t)
#         self.current_data = []
#         self.record_datum()


# # # Define the maximum controllable speed. In the case of propogators,
# # # define the maximum speed which brings the robot to a speed where it is
# # # forced to decelerate even i


# Point of uncoastablility.


# Transitions are where the fastest that could be attained from the start by
# accelerating is equal to the fastest that could be brought to a stop at the
# end by braking and coasting.

# The trajectory which comes from accelerating from the start, and then decelerating or coasting


# the trajectories mark the lower speed boundary of the guaranteed unsalvagable speed set.

# anything above the max speed is unsalvagable

# any point above the fastest speed which could be brought to the end via max
# decel would force the system to overshoot the end and is thus bad

# any point above the fastest speed which can be attained through acceleration
# would require a windup and is thus also bad.

# for every point A at the maximum speed there are two lines: one line
# representing maximum acceleration, and another representing maximum
# deceleration. For any point B, B.xi< A.xi, if B.xi_dot >  The lines intersect at the point. Any point above the
# line

# if xi_dot = c (xi) is a solution to the maximum decel ordinary differential
# equation from A=<xi_A, xi_dot_A>, then any point B with xi_B < xi_A cannot
# help but exceed xi_dot_A in terms of speed if it is integrated from xi_B to
# xi_A under any circumstances. If A is a point at maximum speed then this
# means any integration including B is doomed to fail---it would naturally
# exceed the maximum speed!

# A similar property holds for points with greater xi than A: if C | xi_C>xi_A
# is a point, and xi_dot_C > max_curve(A, xi_C).xi_dot then this means there
# is no way to get to C from A, C is just too fast. Even if you accelerated
# from A, you would not reach C, you would be too slow. If you are at C, you
# must have come from a point with greater speed than A. If A is at maximum
# speed, then C is doomed to be excluded in the same sense as B.

# The maximum speed trajectory between two points is the upper boundary of the
# region which is achievable from those end points, given the maximum speed.

# This region itself is bounded by cruise, cruise-chatter, forward-max, and
# backwards-min trajectories. 

# If the maximum acceleration at maximum speed is greater than the
# acceleration required to remain at maximum speed, then while the restriction
# that all valid points with xi greater than xi_A have xi_dot <=
# forward_max(A).xi_dot, this requirement is superseded by the point
# immedately to its right. Since the maximum speed is a continuum, we cannot
# simply consider this condition for every one of its infinitely many points,
# we instead rely on the fact that only a couple of the trajectories matter.

# The maximum speed boundaries which matter most are the ones which are not
# made redundant by their neighbors: the trajectoires which come from points
# that just barely satisfy the conditions for redundancy. 

# brake
# coast
# chatter (like coast, really)
# search (search for a forward nucleation site)
# fail


AllStanceFeasibilityState = namedtuple(
    "AllStanceFeasibilityState", ["best_stance", "competitor_set",
                                  "infeasible_set", "out_of_range_set"])


@lu.named_state_system("PathTimeState", ['xi', 'xi_dot', 't'], xname='xi_t_state', tname='sigma')
class SolverListSystem(object):

    def __init__(self, world, forward=True):
        self.solvers = [OptimalMulticontactSolver(
            stance, world.com_path, world.robot) for stance in world.contact_data]

        self.state = 0, (0.0 if forward else world.path.max_xi, 0.0, 0.0)
        # maybe individually treat all the solvers as event throwing systems later.
        # self.events=lu.SelfReferentialParallelEvents(self.solvers,[self.path_curvature])
        self.path = world.com_path
        self.forward = forward
        self.path_count = self.path.xi2s.count(
            0 if forward else self.path.max_xi)

    @lu.Guard
    def path_curvature(self):
        return -1.0 if self.path_count == self.path.xi2s.count(self.xi) else 1.0
        # correct, given the new hack to Lyapunov (which ignores initial sign.
        # Positive throws!)

    @path_curvature.mapping
    def path_curvature(self):
        self.path_count += 1.0 if self.forward else -1.0

    @lu.Guard
    def best_brake(self):
        return lu.EventThrowIf[self.get_best_brake()[0] != self.previous_best_accel]

    @best_brake.mapping
    def best_brake(self):
        # Change the active solver.

        pass

    @lu.Guard
    def best_accel(self):
        return lu.EventThrowIf[self.get_best_accel()[0] != self.previous_best_accel]

    @best_accel.mapping
    def best_accel(self):
        pass  # it is enough to know it happened, I think.

    def get_best_brake(self):
        best_stance = None
        best_xi_ddot = float('inf')
        out_of_range_set, infeasible_set, competitor_set = [], [], []
        for i, solver in enumerate(self.solvers):
            if not solver.is_viable(self.xi):
                # print "out of range:", stance
                out_of_range_set.append(i)
                continue
            try:
                xi_ddot = solver.min_xi_ddot(self.xi, self.xi_dot)
            except InfeasibilityException:
                # print "infeasible of range:", stance
                infeasible_set.append(i)
                continue
            if xi_ddot < best_xi_ddot:
                """new best."""
                best_stance = i
                best_xi_ddot = xi_ddot
            competitor_set.append(i)
        dstate = AllStanceFeasibilityState(best_stance=best_stance, competitor_set=competitor_set,
                                           infeasible_set=infeasible_set, out_of_range_set=out_of_range_set)
        return dstate, best_xi_ddot

    def min_xi_ddot(self):
        return self.get_best_brake()[1]

    def get_coast_set(self, xi_ddot_desired):
        res = []
        for i, solver, stance in zip(range(0, len(self.solvers)), self.solvers, self.stances):
            if stance.viable.contains(self.xi):
                if solver.is_feasible(self.xi, self.xi_dot, xi_ddot_desired):
                    res.append(i)
        return res

    def is_achievable(self, xi_ddot_desired):
        res = self.get_coast_set(xi_ddot_desired)
        return res != []

    def get_chatter_sets(self, xi_ddot_desired):
        hi = []
        lo = []
        for i, solver, stance in zip(range(0, len(self.solvers)), self.solvers, self.stances):
            if stance.viable.contains(self.xi):
                if solver.max_accel(self.xi, self.xi_dot) > xi_ddot_desired:
                    hi.append(i)
                if solver.min_accel(self.xi, self.xi_dot) < xi_ddot_desired:
                    lo.append(i)
        return hi, lo

    def is_chatterable(self, xi_ddot_desired):
        hi, lo = self.get_chatter_sets(xi_ddot_desired)
        return len(hi) > 0 and len(lo) > 0


@lu.named_state_system("PathTimeState", ['xi', 'xi_dot', 't'], xname='xi_t_state', tname='sigma')
class BaseSystem(object):

    """System attributes based only on solver_list_system."""
    """the base system hold the real state. SolverListSystem is moved sometimes.."""

    def __init__(self, solver_list_system):
        self.solver_list_system = solver_list_system
        self._state = 0.0, (0.0, 0.0, 0.0)
        self.events = []
        self.xi_ddot = 0.0

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, val):
        self._state = val
        self.solver_list_system.state = val
        # sometimes we inch self.solver_list_system.state forward a smidge.

    def __call__(self):
        self.calculate_accel()
        return (-self.xi_dot, -self.xi_ddot, -1.0)


class BrakeSystem(BaseSystem):

    def __init__(self, solver_list_system):
        super(BrakeSystem, self).__init__(solver_list_system)
        self.events.append(self.zero_speed)

    def calculate_accel(self):
        self.xi_ddot = self.solver_list_system.min_xi_ddot()

    @lu.Guard
    def max_speed(self):
        return lu.EventThrowIf[self.xi_dot > self.xi_dot_max and self.min_xi_ddot() < 0.0]

    @max_speed.mapping
    def max_speed(self):
        self.solver_list_system.state = self.state
        xi_ddot_desired = 0.0  # constant max_velocity case only
        if self.solver_list_system.is_achievable(xi_ddot_desired):
            self.switch_state(AutomataState.coast)
        elif self.solver_list_system.is_chatter(xi_ddot_desired):
            self.switch_state(AutomataState.chatter)
        else:
            self.switch_state(AutomataState.search)

    @lu.Guard
    def infeasibility(self):
        """Change sign when minimum feasible speed is hit."""
        self.solver_list_system.state = self.state
        xi_dot_min = self.solver_list_system.min_feasible_speed()
        return lu.EventThrowIf[self.xi_dot < xi_dot_min]

    @infeasibility.mapping
    def infeasibility(self):
        self.switch_state(AutomataState.fail)

    # TODO: if I could differentiate the min_feasible_speed these two would be
    # the same.

    @lu.Guard
    def zero_speed(self):
        """Change sign on zero crossings in forced accel."""
        self.solver_list_system.state = self.state
        return lu.EventThrowIf[
            self.solver_list_system.min_xi_ddot() > 0.0 and self.xi_dot < 0]

    @zero_speed.mapping
    def zero_speed(self):
        self.switch_state(AutomataState.fail)


class CoastSystem(BaseSystem):

    def __init__(self, solver_list_system):
        super(CoastSystem, self).__init__(solver_list_system)

    def handle_cannot_coast(self):
        self.solver_list_system.state = self.state
        xi_ddot_desired = 0.0  # constant max_velocity case only
        self.solver_list_system.lookahead(xi_delta=-1e-7)
        if self.solver_list_system.is_chatter(xi_ddot_desired):
            self.switch_state(AutomataState.chatter)
        elif self.solver_list_system.is_forced_accel(xi_ddot_desired):
            self.switch_state(AutomataState.brake)
        else:
            self.switch_state(AutomataState.search)


class ChatterSystem(BaseSystem):

    def __init__(self, solver_list_system):
        super(ChatterSystem, self).__init__(solver_list_system)

    def __call__(self):
        return (0.0, 0.0, 0.0)  # fail for now


class FailSystem(BaseSystem):

    def __init__(self, solver_list_system):
        super(FailSystem, self).__init__(solver_list_system)

    def __call__(self):
        return (0.0, 0.0, 0.0)  # wait for sigma to end the sim.
        # since sigma also represents time, the sigma limit is like a
        # maximum duration for the integration as well.


class WinSystem(BaseSystem):

    def __init__(self, solver_list_system):
        super(WinSystem, self).__init__(solver_list_system)

    def __call__(self):
        return (0.0, 0.0, 0.0)  # wait for sigma to end the sim.
        # since sigma also represents time, the sigma limit is like a
        # maximum duration for the integration as well.


class SearchSystem(BaseSystem):

    def __init__(self, solver_list_system):
        super(SearchSystem, self).__init__(solver_list_system)
        self.events.append(self.forced_decel)
        self.events.append(self.coastable)
        self.events.append(self.chatterable)

    def __call__(self):
        # self.xi_dot = # get xi_dot from max speed
        xi_ddot = 0.0  # constant velocity max speed case
        return (-self.xi_dot, 0.0, 0.0)  # wait for sigma to end the sim.
        self.forced_decel = False
        # since sigma also represents time, the sigma limit is like a
        # maximum duration for the integration as well.

    @lu.Guard
    def forced_decel(self):
        return lu.EventThrowIf[(self.solver_list_system.min_xi_ddot > 0) == (not self.forced_decel)]

    @forced_decel.mapping
    def forced_decel(self):
        self.forced_decel = not self.forced_decel

    @lu.Guard
    def coastable(self):
        xi_ddot_desired = self.solver_list_system.max_xi_dot_dot
        return lu.EventThrowIf[(self.solver_list_system.is_achievable(xi_ddot_desired)) and self.forced_decel]

    @coastable.mapping
    def coastable(self):
        self.switch_state(AutomataState.coast)

    @lu.Guard
    def chatterable(self):
        xi_ddot_desired = self.solver_list_system.max_xi_dot_dot
        return lu.EventThrowIf[(self.solver_list_system.requires_chatter(xi_ddot_desired)) and self.forced_decel]

    @chatterable.mapping
    def chatterable(self):
        self.switch_state(AutomataState.chatter)


@lu.named_state_system("PathTimeState", ['xi', 'xi_dot', 't'], xname='xi_t_state', tname='sigma')
class AccelerateSystem(BaseSystem):

    def __init__(self, solver_list_system):
        super(AccelerateSystem, self).__init__(solver_list_system)

# data format:
# JumpIndexedList<TrajectoryRecord>


class TrajectoryRecord(object):

    def __init__(self, automata_state):
        self.xi_data = []
        self.xi_range = []
        self.alternate_min_xi_ddot_data = []  # one per contact
        self.alternate_max_xi_ddot_data = []  # one per contact
        # brake, coast, chatter, search, fail
        self.automata_state = automata_state
        # active stance index(brake), list of plausible stances (coast),
        # active-high and active-low (chatter)
        self.state_specific_data = None



    def __getitem__(self, index):
        return (self.xi_data[index], self.xi_dot_data[index], self.xi_ddot_data[index], self.t_data[index])


class BrakeRecord(TrajectoryRecord):

    def __init__(self):
        super(BrakeRecord, self).__init__(AutomataState.brake)
        self.xi_dot_data = []
        self.xi_ddot_data = []
        self.t_data = []

    def update(self, system):
        system()
        self.xi_data.append(system.xi)
        self.xi_dot_data.append(system.xi_dot)
        self.xi_ddot_data.append(system.xi_ddot)


class CoastRecord(TrajectoryRecord):

    def __init__(self):
        super(BrakeRecord, self).__init__(AutomataState.coast)
        self.xi_dot_function = None
        self.t_data = []


class ChatterRecord(TrajectoryRecord):

    def __init__(self):
        super(BrakeRecord, self).__init__(AutomataState.chatter)
        self.ideal_xi_dot_function = None
        self.hi_xi_ddot_data = []
        self.low_xi_ddot_data = []
        self.t_data = []


class SearchRecord(TrajectoryRecord):

    def __init__(self):
        super(BrakeRecord, self).__init__(AutomataState.search)
        # time, speed are meaningless


class FailRecord(TrajectoryRecord):

    def __init__(self):
        super(FailRecord, self).__init__(AutomataState.fail)


# HybridAutomata(brake, coast, chatter, search)
record_map = {
    BrakeSystem: BrakeRecord,
    CoastSystem: CoastRecord,
    ChatterSystem: ChatterRecord,
    SearchSystem: SearchRecord}


# 2-pass design:
# brake:
#   - reach max speed (while decelerating) and feasible -> coast
#   - reach max speed (while decelerating) and chatterable -> chatter
#   - reach max speed (while decelerating) and not feasible -> search
#   - solver state change -> brake (new state)
# coast:
#   - reaches chatter -> chatter
#   - reaches forced accel -> brake
#   - reaches infeasibility -> search
#   - solver state change -> coast (new state)
# chatter:
#   - reaches coastable region -> coast
#   - reaches forced accel -> brake
#   - reaches infeasibility -> search
#   - solver state change -> chatter (new state)
# search:
#   - reaches forced deceleration -> search (prepped)
#   - leaves forced deceleration but not into chatter or coast -> search (non prepped)
#   - reaches chatterable max and prepped -> chatter
#   - reaches coastable max and prepped -> coast

# # # 1 - pass design (MaxSpeedPlanner):
# accelerate_forward:
#   - solver state change -> accelerate_forward (new state)
#   - reach followable max speed (while accelerating) -> follow_speed_forward
#   - reach forced acceleration max speed -> search_ahead(prepped)
#   - reach zero/infeasibility (raise untraversable path exception)
# follow_speed_forward:
#   - chatter/non-chatter state change -> follow_speed_forward (new state)
#   - solver state change -> follow_speed_forward (new state)
#   - reach forced acceleration -> search_ahead(prepped)
#   - reach forced deceleration -> accelerate_forward
# decelerate_back:
#   - reach intersection with previous trajectory -> update history -> follow_speed_forward(start_of_followable)
#   - reach max speed (while decelerating) -> update history (no good path) -> follow_speed_forward(start_of_followable)
#   - solver state change -> decelerate_back (new state)
# search_ahead:
#   - reaches forced acceleration -> search_ahead (prepped)
#   - reaches forced decel while prepped -> search_ahead (not prepped)
#   - reaches followable region while prepped -> decelerate_back (start_of_followable = start_of_followable)




@lu.named_state_system("PathTimeState", ['xi', 'xi_dot', 't'], xname='xi_t_state', tname='sigma')
class ReversePropogator(lu.HybridAutomata):

    """Propagates max or min accel dynamics forward or backwards in time."""

    def __init__(self, world):
        solver_list_system = SolverListSystem(world)
        super(ReversePropogator, self).__init__(
            AutomataState.brake, BrakeSystem(solver_list_system))
        self.add_system(AutomataState.coast, CoastSystem(solver_list_system))
        self.add_system(
            AutomataState.chatter, ChatterSystem(solver_list_system))
        self.add_system(AutomataState.search, SearchSystem(solver_list_system))
        self.add_system(AutomataState.fail, FailSystem(solver_list_system))
        self.add_system(AutomataState.win, WinSystem(solver_list_system))
        self.records = []
        self.record = None

        for event in self.events:
            event()

    def integrate(self):
        stepper = ly.dormand_prince(self, 1e1)
        stepper.time_tolerance = 1e-7
        stepper.relative_tolerance = 1e-7
        stepper.absolute_tolerance = 1e-6

        self.record = record_map[type(self.active)]()
        self.record.update(self.active)
        try:
            for q, guards in stepper:
                self.record.update(self.active)
                for guard in guards:
                    # This is where scipy.interp1d is called:
                    self.record.finish()
                    self.records.append(self.record)
                    # state may change, self.active may change, but state changes
                    # are preserved in transition.
                    guard.map()
                    stepper.step_across(self.s_state)  # always step across
                    self.record = record_map[type(self.active)]()
        except Exception as e:
            print e
        self.record.finish()

if __name__ == '__main__':
    import multi_contact.worlds.demo8_world as world
    # path_s_decomposition, stances = MultiContactExplorer(world).compute()
    ReversePropogator(world).integrate()
