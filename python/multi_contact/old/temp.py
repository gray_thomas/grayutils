import cPickle as pkl
if False:  # re-integrate!
    print "Integrating!"
    real_planner = MultiContactPlanner(world, stances)
    integration_data = real_planner.propogate_dynamics(path_s_decomposition)
    with open("int.pkl", 'w') as fil:
        pkl.dump(integration_data, fil)
else:
    with open("int.pkl", 'r') as fil:
        integration_data = pkl.load(fil)

nucleus = 0

# print integration_data[nucleus].nucleus
# print integration_data[nucleus].termination
# print integration_data[nucleus].limits
# print integration_data[1].nucleus
# print integration_data[1].termination
# print integration_data[1].limits
print integration_data[nucleus].history[0].start
print integration_data[nucleus].history[0].end
print integration_data[nucleus].history[1].end
print integration_data[1].history[0].start
print integration_data[1].history[0].end
print integration_data[1].history[1].data
print integration_data[1].history[1].end
print integration_data[1].history[2].end
print integration_data[0].history[0]
# print integration_data[0]
