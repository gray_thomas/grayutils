import lyapunov as ly
import ly_utils as lu
import numpy as np
import cvxpy as cvx
from exceptions import NotImplementedError
from numpy import sign
from math import sqrt
from OptimalMulticontactSystem import OptimalMulticontactSystem, InfeasibilityException
from multi_contact.Bezier2Splines import convert_s
from collections import namedtuple
from gu.gutypes import mutabledata
from section_combination import combine_regions
from RegionSet import RegionSet
from traceback import print_stack
from enum import Enum

ContactDState = mutabledata("ContactDState", ["kinematic"])

ADZI = Enum("ADZI", "A D Z I")

LabeledDecompositionElement = namedtuple(
    "LabeledDecompositionElement", ["start", "end", "region_code"])
KinematicallyUntraversablePathException = type(
    "KinematicallyUntraversablePathException", (Exception,), {})


def repr_ADZI_decomp(list_of_decomposition_elements):
    repr_strings = []
    for start, end, region_code in list_of_decomposition_elements:
        repr_strings.append(
            "" + repr(region_code) + "~" + "%.3f:%.3f" % (start, end))
    return "[" + ",".join(repr_strings) + "]"

SimpleStance = namedtuple("SimpleStance", [
                          "viable", "ADZIDict", "valid_accel", "valid_decel", "contact_combination"])


class StanceWithLimits(object):

    """
    A stateless event system.

    A contact combination meta object builds the limits for several things.
    It builds limits for kinematics.
    It builds limits for problem feasibility.
    It buidls limits for min acceleration negativity
    It builds limits for max acceleration positivity
    However now it is essentially just a container for a list (events) of BaseLimitEventFunctors.
    """

    def __init__(self, path_s_provider, com_path, robot, contact_combination):

        self.com_path = com_path
        self.contact_combination = contact_combination
        self.kinematic_limits = KinematicLimits(
            path_s_provider, com_path, contact_combination)
        self.max_accel_limits = AccelEventFunctor(
            path_s_provider, com_path, robot, contact_combination, is_max_accel=True)
        self.min_accel_limits = AccelEventFunctor(
            path_s_provider, com_path, robot, contact_combination, is_max_accel=False)

        self.events = [self.kinematic_limits,
                       self.max_accel_limits,
                       self.min_accel_limits,
                       ]

    def debug_string(self):
        return (repr(self) + "\n\talso:" +
                " kinematic_limits" + repr(self.kinematic_limits) +
                ", max_accel_limits" + repr(self.max_accel_limits) +
                ", min_accel_limits" + repr(self.min_accel_limits))

    def __repr__(self):
        return (self.__class__.__name__ + "{contact=[" +
                ",".join([c.name for c in self.contact_combination]) +
                "], A=" + repr(self.ADZIDict[ADZI.A]) +
                ", D=" + repr(self.ADZIDict[ADZI.D]) +
                ", Z=" + repr(self.ADZIDict[ADZI.Z]) + "}"
                )

    def calculate_viable_accel_limits(self):
        on_path = RegionSet(base=[[0.0, self.com_path.max_path_s]])
        negate_args = {"min_value": 0.0, "max_value": self.com_path.max_path_s}

        self.viable = RegionSet.conjunction(
            [on_path, self.kinematic_limits]).strip()  # add more later?

        self.valid_accel = RegionSet.conjunction(
            [on_path, self.max_accel_limits, self.kinematic_limits]).strip()
        self.valid_decel = RegionSet.conjunction(
            [on_path, self.min_accel_limits, self.kinematic_limits]).strip()

        self.ADZIDict = decomposeADZI(
            self.valid_accel, self.valid_decel, negate_args)
        return SimpleStance(viable=self.viable, valid_accel=self.valid_accel,
                            valid_decel=self.valid_decel, ADZIDict=self.ADZIDict,
                            contact_combination=self.contact_combination)


class BaseLimitEventFunctor(RegionSet):

    def __init__(self, path_s_provider):
        super(BaseLimitEventFunctor, self).__init__()
        self.path_s_provider = path_s_provider
        self.is_true = (self() >= 0)
        if self.is_true:
            self.start_region(self.path_s)

    @property
    def path_s(self):
        return self.path_s_provider.path_s

    def handle(self):
        print "limit event!"
        if self.is_true:
            # end a viable region
            self.end_region(self.path_s)
            self.is_true = False
        else:
            # begin a viable region
            self.start_region(self.path_s)
            self.is_true = True

    def __call__(self):
        """Override with the relevant event. Positive is true."""
        return 1.0

    def handle_path_termination(self):
        if self.is_true:
            self.end_region(self.path_s)
        assert (self.open == 0)


class AccelEventFunctor(BaseLimitEventFunctor):

    def __init__(self, path_s_provider, com_path, robot, contact_combination, is_max_accel=True):
        self.maximizer = OptimalMulticontactSystem(
            contact_combination, com_path, robot, is_max_accel=is_max_accel)
        self.is_max_accel = is_max_accel
        super(AccelEventFunctor, self).__init__(path_s_provider)

    def __call__(self):
        """Event functor method. Changes sign on events."""
        dxds = self.maximizer.com_path.velocity_vector(
            *convert_s(self.path_s))
        path_s_dot = (self.maximizer.robot.max_speed /
                      sqrt(dxds.T.dot(dxds)))
        try:
            ret = self.maximizer.solve(self.path_s, path_s_dot)
        except InfeasibilityException:
            return -1.0
        if self.is_max_accel:
            return float(ret.l_ddot)
        else:
            return -float(ret.l_ddot)


def decomposeADZI(valid_accel, valid_decel, negate_args):
    """
    Perform a decomposition

    Identify each part of the a space as either:
        A:  Forced to accelerate
        D:  Forced to decelerate
        Z:  Can follow speed
        I:  Infeasible
    """
    notvalid_accel = valid_accel.negate(**negate_args)
    notvalid_decel = valid_decel.negate(**negate_args)

    Z = RegionSet.conjunction(
        [valid_accel, valid_decel]).strip()
    notZ = Z.negate(**negate_args).strip()
    A = RegionSet.conjunction([valid_accel, notZ]).strip()
    D = RegionSet.conjunction([valid_decel, notZ]).strip()
    I = RegionSet.conjunction(
        [notvalid_decel, notvalid_accel]).strip()

    assert (len(RegionSet.conjunction([Z, A]).strip()) == 0)
    assert (len(RegionSet.conjunction([Z, D]).strip()) == 0)
    assert (len(RegionSet.conjunction([Z, I]).strip()) == 0)
    assert (len(RegionSet.conjunction([A, D]).strip()) == 0)
    assert (len(RegionSet.conjunction([A, I]).strip()) == 0)
    assert (len(RegionSet.conjunction([D, I]).strip()) == 0)

    return {ADZI.A: A, ADZI.D: D, ADZI.Z: Z, ADZI.I: I}


class KinematicLimits(BaseLimitEventFunctor):

    def __init__(self, path_s_provider, com_path, contact_combination):
        self.contact_combination = contact_combination
        self.com_path = com_path
        super(KinematicLimits, self).__init__(path_s_provider)

    def __call__(self):
        """Return underextension of leg. Positive if feasible."""
        res = []
        for contact in self.contact_combination:
            x, y = self.com_path.point(*convert_s(self.path_s))
            xf, yf = contact.x_f, contact.y_f
            res.append(
                sqrt(pow(x - xf, 2) + pow(y - yf, 2)) - contact.max_leg)

        print "limit event functor return: ", max(res)
        return -max(res)


@lu.named_state_system("ArcLengthState", ['arc_s'], xname="s_state", tname='path_s')
class MultiContactExplorer(object):

    """
    A hybrid system used for planning. This system searches for kinematic limits,
    regions of positive max acceleration or regions of negative min acceleration.
    """

    def __init__(self, world):
        """
        Setup the system based on a problem definition (world).
        """

        self.world = world
        self.com_path = world.com_path
        self.state = (0, (0.0,))

        self.stances = []

        for contact_combination in world.contact_data:
            meta_object = StanceWithLimits(
                self, world.com_path, world.robot, contact_combination)
            self.stances.append(meta_object)
        self.events = ly.ParallelEvents(self.stances)

    def __repr__(self):
        return (self.__class__.__name__ + "{" +
                "], A=" + repr(self.ADZIDict[ADZI.A]) +
                ", D=" + repr(self.ADZIDict[ADZI.D]) +
                ", Z=" + repr(self.ADZIDict[ADZI.Z]) + "}"
                )

    def compute(self):
        switch_times = []
        system = self
        logger = ly.Recorder(system)
        # integrate from start to end of com_path, variable step size
        stepper = ly.dormand_prince(system, self.com_path.max_path_s)
        stepper.time_tolerance = 1e-3
        stepper.absolute_tolerance = 1e-3
        stepper.relative_tolerance = 1e-3
        for path_s, self_handling_events in stepper:
            logger.log(self_handling_events)
            print "<", self.path_s, ", ", self.arc_s, ">"
            if self_handling_events:
                try:
                    for self_handling_event in self_handling_events:
                        self_handling_event.handle()
                except StopIteration:
                    print "Stoping Exploration"
                    break
                except lu.StopContinuousIteration:
                    print "Continuous region ends. A new one begins."
                    stepper.step_over(self.state[1])
                # switch_energies.append(energy)
            try:
                ly.check_NaN(system)
            except ArithmeticError:
                break
        simple_stances = []
        for stance in self.stances:
            for event in stance.events:
                event.handle_path_termination()
            simple_stance = stance.calculate_viable_accel_limits()
            simple_stances.append(simple_stance)
            # print stance.debug_string()
            print stance

        accel_regions = reduce(
            lambda x, y: x + y, [c.valid_accel for c in self.stances])
        decel_regions = reduce(
            lambda x, y: x + y, [c.valid_decel for c in self.stances])
        self.valid_accel = accel_regions.disjunction()
        self.valid_decel = decel_regions.disjunction()
        negate_args = {"min_value": 0.0, "max_value": self.com_path.max_path_s}
        self.ADZIDict = decomposeADZI(
            self.valid_accel, self.valid_decel, negate_args)
        print self
        labeled_decomposition_elements = []
        for region_code, region_set in self.ADZIDict.iteritems():
            for start, end in region_set:
                labeled_decomposition_elements.append(
                    LabeledDecompositionElement(start=start, end=end, region_code=region_code))
        labeled_decomposition_elements = sorted(
            labeled_decomposition_elements, key=lambda x: x.start)
        print labeled_decomposition_elements
        for i in range(0, len(labeled_decomposition_elements) - 1):
            assert (labeled_decomposition_elements[
                    i].end == labeled_decomposition_elements[i + 1].start)
        assert (labeled_decomposition_elements[0].start == 0.0)
        assert (
            labeled_decomposition_elements[-1].end == self.com_path.max_path_s)
        self.decomposition = labeled_decomposition_elements
        print repr_ADZI_decomp(self.decomposition)

        kinematic_regions = reduce(
            lambda x, y: x + y, [c.kinematic_limits for c in self.stances])
        self.kinematic_region = kinematic_regions.disjunction()
        if len(self.kinematic_region) != 1:
            raise KinematicallyUntraversablePathException()
        if self.kinematic_region[0][0] != 0.0:
            raise KinematicallyUntraversablePathException()
        if self.kinematic_region[0][1] != self.com_path.max_path_s:
            raise KinematicallyUntraversablePathException()
        print kinematic_regions
        print self.kinematic_region
        # for stance in self.stances:
        #     stance.calculate_viable_accel_limits()
        accel_regions = reduce(
            lambda x, y: x + y, [c.max_accel_limits for c in self.stances])
        self.all_accel = accel_regions.disjunction()
        decel_regions = reduce(
            lambda x, y: x + y, [c.min_accel_limits for c in self.stances])
        self.all_decel = decel_regions.disjunction()

        print self.all_decel
        print self.all_accel
        return self.decomposition, simple_stances
        # raise NotImplementedError("hey")
        # print_stack()

    def __call__(self):
        """Return expression for arc length differential."""
        dxds = self.com_path.velocity_vector(*convert_s(self.path_s))
        return sqrt(dxds.T.dot(dxds)),
