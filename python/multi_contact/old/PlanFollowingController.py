"""
PlanFollowingController module.

Chooses contact forces. Uses existing, predefined switching times.
"""

import numpy as np
import cvxpy as cvx
from math import sin, cos, atan, sqrt
from logging.ContactRecord import ContactRecord
from intersection.OptimalMulticontactSystem import OptimalMulticontactSystem, InfeasibilityException
import ly_utils as lu
from collections import namedtuple

SimpleRobot = namedtuple("SimpleRobot", ["mass", "gravity_accel"])


@lu.namedstatesystem("RobotState", ['x', 'y', 'x_dot', 'y_dot'], xname="x_state", tname='t')
class PlanFollowingController(OptimalMulticontactSystem):

    """Does the real work eventually."""

    def __init__(self, world, switch_times=None, plan_switch_time=1000.0,
                 plan_switch_s=None, params=SimpleRobot(mass=20.0, gravity_accel=-9.81)):
        """Use a predefined switching time switch sequence for now."""
        super(PlanFollowingController, self).__init__(
            world.contact_data[0], world.com_path, params, is_max_accel=True)
        self.state = 0.0, (0.0, 0.0, 0.0, 0.0)
        self.contact_data = world.contact_data
        self.plan_switch_time = plan_switch_time
        self.plan_switch_s = plan_switch_s
        self.switch_times = switch_times

    def get_contacts(self, time):
        """lookup the current contact, based on known transition times."""
        for i, switching_time in enumerate(self.switch_times):
            if time < switching_time:
                return self.contact_data[i]
        raise Exception()

    def get_end_time(self, index):
        """lookup the time at which the contact state should switch."""
        return self.switch_times[index]

    def fall(self, contacts):
        num_contacts = len(contacts)
        for i in range(0, num_contacts):
            contacts[i].set_force(0, 0)
        print "Falling!"

    @property
    def s(self):
        """path variable s. goes to an integer."""
        i, path_s = self.com_path.find(self.x)
        return i + path_s

    @property
    def s_dot(self):
        """internal path variable derivative."""
        i, path_s = self.com_path.find(self.x)
        # print "this is it", np.array([self.x_state[2:]]).T
        path_s_dot = self.com_path.get_sd(
            i, path_s, np.array([self.x_state[2:]]).T)
        return path_s_dot

    @property
    def l(self):
        """arc length along the path."""
        return self.com_path.path_2_arc(self.s)

    @property
    def l_dot(self):
        """arc length derivative: speed."""
        i, path_s = self.com_path.find(self.x)
        dxds = self.com_path.velocity_vector(i, path_s)
        # print dir (np.linalg)
        return self.s_dot * np.linalg.norm(dxds)

    def l_ddot(self, xdd, ydd):
        """arc length acceleration. only a component of true accel."""
        i, path_s = self.com_path.find(self.x)
        dxds = self.com_path.velocity_vector(i, path_s)
        vhat = dxds/np.linalg.norm(dxds)
        accel = vhat[0]*xdd+vhat[1]*ydd
        return accel

    def calculate_multicontact_force(self, state_vector, time):
        """the robot controller."""
        # print "calculating multicontact force"
        self.contact_combination = self.get_contacts(time)
        self.state = time, state_vector

        try:
            i, path_s = self.com_path.find(self.x)
        except LookupError:
            print "Out of path!"
            self.fall(self.contact_combination)
            return

        if self.plan_switch_s:
            self.is_max_accel = self.s < self.plan_switch_s
            print "path_s:", i, path_s, "time", time
        else:
            self.is_max_accel = time < self.plan_switch_time

        if len(self.contact_combination) == 0:
            print "Flight phases. So bold!"
            return

        try:
            sol = self.solve(self.s, self.s_dot)

            for contact_result in sol.contacts:
                contact_result.contact.set_force(
                    contact_result.wrench[0, 0],
                    contact_result.wrench[1, 0])

            def sanity_check_forces(point_force_pairs, com_location):
                moment = 0
                for point, force in point_force_pairs:
                    relative_point = point - com_location
                    moment += relative_point[0, 0] * force[1, 0]
                    moment -= relative_point[1, 0] * force[0, 0]
                if not abs(moment) <= 1e-4:
                    print "wrong moment", moment
                # assert abs(moment) <= 2e-3
            records = [ContactRecord(contact)
                       for contact in self.contact_combination]
            point_force_pairs = [
                (record.foot, record.wrench[0:2, :]) for record in records]
            sanity_check_forces(point_force_pairs,
                                np.array([[state_vector[0]], [state_vector[1]]]))
        except ValueError as e:
            print dir(e)
            print e.message
            raise e
            print "value error"
            self.fall(self.contact_combination)
            return
        except cvx.error.SolverError:
            print "solver error"
            self.fall(self.contact_combination)
            return
        except InfeasibilityException:
            print "infeasibility!"
            self.fall(self.contact_combination)
            return
