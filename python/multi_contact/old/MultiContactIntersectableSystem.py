import lyapunov as ly
import ly_utils as lu
import numpy as np
from exceptions import NotImplementedError
from numpy import sign
from math import sqrt
from OptimalMulticontactSystem import OptimalMulticontactSystem, InfeasibilityException
from Bezier2Splines import convert_s
from enum import Enum
from collections import namedtuple

PropogationNuclei = namedtuple(
    "PropogationNuclei", ["s", "s_dot", "forward_time", "max_accel"])

Terminations = Enum(
    "Terminations", "max_speed zero_speed path_end max_jumps infeasibility")

NonTerminationGuards = Enum(
    "NonTerminationGuards", "best_stance feasible_set out_of_range_set path_curve")


class Guard(object):

    def __init__(self, call, callback):
        self.call = call
        self.callback = callback

    def __call__(self):
        return self.call()

    def handle(self):
        self.callback()


# MCPathData.history:
#   - ContinuousSegment 1: []
#   - ContinuousSegment 2: []
#   - ContinuousSegment 3: []

MCPathData = namedtuple(
    "MCPathData", ["history", "termination", "nucleus", "limits"])
ContinuousSegment = namedtuple(
    "ContinuousSegment", ["start", "end", "dstate", "data", "arc_data", "end_condition"])
State = namedtuple("State", ["s", "s_dot", "s_ddot", "t"])

DState = namedtuple(
    "DState", ["best_stance", "competitor_set", "infeasible_set", "out_of_range_set"])


@lu.namedstatesystem("PathTimeState", ['s', 's_dot', 't'], xname='s_state', tname='q')
class MCPropogator(object):

    """Propagates max or min accel dynamics forward or backwards in time."""

    def __init__(self, world, stances, nucleus):
        self.events = [
            Guard(self.check_dstate, self.handle_dstate),
            Guard(self.check_max_speed, self.handle_max_speed),
            Guard(self.check_zero_speed, self.handle_zero_speed),
            Guard(self.check_path_curvature, self.handle_path_curvature),
            Guard(self.check_path_end, self.handle_path_end)]
        self.state = 0.0, (nucleus.s, nucleus.s_dot, 0.0)
        self.max_accel = nucleus.max_accel
        self.forward_time = nucleus.forward_time
        self.world = world
        self.solver = OptimalMulticontactSystem(
            world.contact_data[0], world.com_path, world.robot, is_max_accel=nucleus.max_accel)
        # to be efficient this system should really have a few
        # OptimalMulticontactSystems, to deal with the multiple active sets it
        # will need to remember to check for a new contact configuration
        # dominating the current one.
        self.stances = stances
        self.dstate, s_ddot = self.get_best_stance()
        print[s.name for s in self.dstate.best_stance.contact_combination], "chosen at start!"
        self.stance_start_state = State(
            s=nucleus.s, s_dot=nucleus.s_dot, s_ddot=float(s_ddot), t=0.0)
        self.nucleus = nucleus
        self.hybrid_history = []
        self.current_data = []
        self.lookup = None  # generate this after integrating
        self.termination = None  # based on how integration ends
        self.max_jumps = 20

    @property
    def speed(self):
        return self.world.com_path.speed(self.s, self.s_dot)

    def eval_stance(self, stance):
        """Return s_ddot. Use an OptimalMulticontactSystem"""
        self.solver.contact_combination = stance.contact_combination
        try:
            ret = self.solver.solve(self.s, self.s_dot)
        except InfeasibilityException as e:
            raise e
        return ret.s_ddot

    def get_best_stance(self):
        # print "get_best_stance!"
        best_stance = None
        best_s_ddot = -float('inf') if self.max_accel else float('inf')
        out_of_range_set, infeasible_set, competitor_set = [], [], []
        for stance in self.stances:
            if not stance.viable.contains(self.s):
                # print "out of range:", stance
                out_of_range_set.append(stance)
                continue
            try:
                s_ddot = self.eval_stance(stance)
            except InfeasibilityException:
                # print "infeasible of range:", stance
                infeasible_set.append(stance)
                continue
            if ((self.max_accel and s_ddot > best_s_ddot)
                    or ((not self.max_accel) and s_ddot < best_s_ddot)):
                """new best."""
                best_stance = stance
                best_s_ddot = s_ddot
            competitor_set.append(stance)
        dstate = DState(best_stance=best_stance, competitor_set=competitor_set,
                        infeasible_set=infeasible_set, out_of_range_set=out_of_range_set)
        return dstate, best_s_ddot

    def record_datum(self):
        self.current_data.append(
            (self.s, self.s_dot, self.eval_stance(self.dstate.best_stance), self.t))

    def integrate(self):
        stepper = ly.dormand_prince(self, 1e1)
        stepper.time_tolerance = 1e-7
        stepper.relative_tolerance = 1e-7
        stepper.absolute_tolerance = 1e-6
        jumps = 0
        for q, autoevents in stepper:
            print self.s, [s.name for s in self.dstate.best_stance.contact_combination], self.dstate.best_stance.viable.contains(self.s)
            self.record_datum()
            if autoevents:
                do_break = False
                do_update = False
                for autoevent in autoevents:
                    try:
                        autoevent.handle()
                    except StopIteration:
                        do_break = True
                    except lu.StopContinuousIteration:
                        do_update = True
                        jumps += 1
                if do_break:
                    self.terminating_guard()
                    break
                if jumps >= self.max_jumps:
                    self.termination = Terminations.max_jumps
                    self.terminating_guard()
                    break
                if do_update:
                    self.non_terminating_guard()
                    stepper.step_across(self.s_state)
        limits = [
            self.hybrid_history[0].start.s, self.hybrid_history[-1].end.s]
        return MCPathData(
            history=self.hybrid_history, termination=self.termination,
            nucleus=self.nucleus, limits=limits)

    def __call__(self):
        """Integrate the current stance."""
        tp = 1.0 if self.forward_time else -1.0
        try:
            s_dotp = self.eval_stance(self.dstate.best_stance) * tp
        except InfeasibilityException:
            # print "infeasible call"
            s_dotp = self.current_data[-1][2] * tp
        # print "call", self.s, self.t
        s_p = self.s_dot * tp
        return s_p, s_dotp, tp

    # Events should become negative when they trigger. They should start
    # positive.

    def check_max_speed(self):
        return self.world.robot.max_speed - self.speed

    def check_zero_speed(self):
        return self.speed

    def check_path_end(self):
        # print "check path end"
        return min(self.s, self.world.com_path.max_path_s - self.s)

    def check_path_curvature(self):
        # print "check path curvature"
        path_i, path_s = convert_s(self.s)
        if path_i % 2 == 1:
            return 1.0
        return -1.0

    def check_dstate(self):
        dstate, s_ddot = self.get_best_stance()
        if dstate != self.dstate:
            return -1.0
        return 1.0

    def punt_a_bit(self, dq=1e-6):
        """Moves the system forward in q by a tiny increment."""
        new_state=[float(s + dq * ds) for s, ds in  zip(self.state[1], self())]
        self.state = self.q + dq, new_state

    def handle_max_speed(self):
        self.termination = Terminations.max_speed
        raise StopIteration()

    def handle_zero_speed(self):
        self.termination = Terminations.zero_speed
        raise StopIteration()

    def handle_path_end(self):
        self.termination = Terminations.path_end
        raise StopIteration()

    def handle_dstate(self):
        self.punt_a_bit()
        new_dstate, new_s_ddot = self.get_best_stance()
        assert new_dstate != self.dstate
        if len(new_dstate.competitor_set)==0:
            self.termination = Terminations.infeasibility
            raise StopIteration()
        if new_dstate.out_of_range_set != self.dstate.out_of_range_set:
            self.termination = NonTerminationGuards.out_of_range_set
            raise lu.StopContinuousIteration()
        if new_dstate.infeasible_set != self.dstate.infeasible_set:
            self.termination = NonTerminationGuards.feasible_set
            raise lu.StopContinuousIteration()
        if new_dstate.best_stance != self.dstate.best_stance:
            self.termination = NonTerminationGuards.best_stance
            raise lu.StopContinuousIteration()
        print "error handle dstate", new_dstate, self.dstate
        raise NotImplementedError()

    def handle_path_curvature(self):
        self.termination = NonTerminationGuards.path_curve
        raise lu.StopContinuousIteration()

    def terminating_guard(self):
        end_state = State(
            s=self.s, s_dot=self.s_dot, s_ddot=self.current_data[-1][2],
            t=self.t)
        arc_data = [self.world.com_path.arcify_state(s[0],s[1],s[2]) for s in self.current_data]
        continuous_segment_record = ContinuousSegment(
            start=self.stance_start_state, end=end_state, dstate=self.dstate,
            data=np.array(self.current_data), arc_data = np.array(arc_data),
            end_condition=self.termination)
        self.hybrid_history.append(continuous_segment_record)
        print "this is where we would generate the interpolates for intersection finding."

    def non_terminating_guard(self):
        print "non_terminating_guard", self.termination
        end_state = State(
            s=self.s, s_dot=self.s_dot, s_ddot=self.current_data[-1][2],
            t=self.t)
        arc_data = [self.world.com_path.arcify_state(s[0],s[1],s[2]) for s in self.current_data]
        continuous_segment_record = ContinuousSegment(
            start=self.stance_start_state, end=end_state, dstate=self.dstate,
            data=np.array(self.current_data), arc_data = np.array(arc_data),
            end_condition=self.termination)
        self.hybrid_history.append(continuous_segment_record)
        self.punt_a_bit()  # cheats the system forward a tiny amount
        self.dstate, s_ddot = self.get_best_stance()
        self.stance_start_state = State(
            s=self.s, s_dot=self.s_dot, s_ddot=float(s_ddot),
            t=self.t)
        self.current_data = []
        self.record_datum()
