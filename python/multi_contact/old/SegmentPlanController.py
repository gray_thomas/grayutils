"""
SegmentPlanController module.

Chooses contact forces. Uses existing, predefined switching times.
"""

import numpy as np
import cvxpy as cvx
from math import sin, cos, atan, sqrt
from logging.ContactRecord import ContactRecord
from propogation.OptimalMulticontactSolver import OptimalMulticontactSolver, InfeasibilityException
from propogation.SolverListSystem import SolverListSystem
import ly_utils as lu
from collections import namedtuple
from bisect import bisect
from CommandEnum import CommandEnum


class Segmentation(object):

    def __init__(self, list_of_classification_tuples):
        self.raw_tuples = list_of_classification_tuples
        self.search_list = []
        for t in self.raw_tuples:
            self.search_list.append(t.start)
        self.search_list.append(self.raw_tuples[-1].end)

    def __call__(self, value):
        index = bisect(self.search_list, value)
        return self.raw_tuples[index]


@lu.named_state_delete_functionssystem("RobotState", ['x', 'y', 'x_dot', 'y_dot'], xname="x_state", tname='t')
class SegmentPlanController(object):

    def __init__(self, world, plan):
        """Use a predefined switching time switch sequence for now."""
        super(SegmentPlanController, self).__init__(
            world.contact_data[0], world.com_path, world.robot, is_max_accel=True)
        self.state = 0.0, (0.0, 0.0, 0.0, 0.0)
        self.contact_data = world.contact_data
        self.contact_segmentation = Segmentation(contact_segmentation)
        self.goal_segmentation = Segmentation(goal_segmentation)

    @property
    def stance(self):
        """lookup the current contact, based on known transition times."""
        i = self.contact_segmentation(self.s).contact_index
        return self.contact_data[i]

    @property
    def goal(self):
        """lookup the current goal: accel=0, decel=1, or hold_speed=2."""
        return self.goal_segmentation(self.s).goal_code

    def fall(self, contacts):
        num_contacts = len(contacts)
        for i in range(0, num_contacts):
            contacts[i].set_force(0, 0)
        print "Falling!"

    @property
    def s(self):
        """path variable s. goes to an integer."""
        i, path_s = self.com_path.find(self.x)
        return i + path_s

    @property
    def s_dot(self):
        """internal path variable derivative."""
        # print "this is it", np.array([self.x_state[2:]]).T
        path_s_dot = self.com_path.get_sd(
            i, path_s, np.array([self.x_state[2:]]).T)
        return path_s_dot

    @property
    def xi(self):
        """arc length along the path."""
        return self.com_path.s2xi(self.s)

    @property
    def xi_dot(self):
        """arc length derivative: speed."""
        dx_dxi = self.com_path.dx_dxi(self.xi)
        return dx_dxi[0] * self.xd + dx_dxi[1] * self.yd

    def xi_ddot(self, xdd, ydd):
        """arc length acceleration. only a component of true accel."""

        dx_dxi = self.com_path.dx_dxi(self.xi)
        return dx_dxi[0] * xdd + dx_dxi[1] * ydd

    def calculate_multicontact_force(self, state_vector, time):
        """the robot controller."""
        self.state = time, state_vector

        try:
            i, path_s = self.com_path.find(self.x)
        except LookupError:
            print "Out of path!"
            self.fall(self.stance)
            return

        if len(self.stance) == 0:
            print "Flight phases. So bold!"
            return

        try:
            sol = self.solve(self.s, self.s_dot, goal=self.goal)

            for contact_result in sol.contacts:
                contact_result.contact.set_force(
                    contact_result.wrench[0, 0],
                    contact_result.wrench[1, 0])

            def sanity_check_forces(point_force_pairs, com_location):
                moment = 0
                for point, force in point_force_pairs:
                    relative_point = point - com_location
                    moment += relative_point[0, 0] * force[1, 0]
                    moment -= relative_point[1, 0] * force[0, 0]
                if not abs(moment) <= 1e-4:
                    print "wrong moment", moment
                # assert abs(moment) <= 2e-3
            records = [ContactRecord(contact)
                       for contact in self.stance]
            point_force_pairs = [
                (record.foot, record.wrench[0:2, :]) for record in records]
            sanity_check_forces(point_force_pairs,
                                np.array([[state_vector[0]], [state_vector[1]]]))
        except ValueError as e:
            print dir(e)
            print e.message
            raise e
            print "value error"
            self.fall(self.stance)
            return
        except cvx.error.SolverError:
            print "solver error"
            self.fall(self.stance)
            return
        except InfeasibilityException:
            print "infeasibility!"
            self.fall(self.stance)
            return
