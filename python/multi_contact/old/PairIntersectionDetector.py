import lyapunov as ly
import ly_utils as lu
import numpy as np


class PairIntersectionDetector(object):
    def __init__(self, system1, system2, time=0):
        self.sys_1 = system1
        self.sys_2 = system2
        self.state = time, tuple()
        self.events=[self.intersection_event]
    def intersection_event(self):
        return self.sys_1.state[1][0]-self.sys_2.state[1][0]
    def handle_events(self, event):
        raise StopIteration(self)
