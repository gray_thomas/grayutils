
def combine_regions(sections):
    """
    takes a list of lists of two-element lists
    """
    events = []

    for section in sections:
        if section[1] > section[0]:
            events.append((section[0], True))
            events.append((section[1], False))
        else:
            events.append((section[0], False))
            events.append((section[1], True))
    # put opening events first, to resolve ties.
    events = sorted(events, key=lambda x: not x[1])
    events = sorted(events, key=lambda x: x[0])
    combined = []
    current_region = []
    open_intervals = 0
    for event in events:
        if event[1]:
            if open_intervals == 0:
                current_region.append(event[0])
            open_intervals += 1
        if not event[1]:
            if open_intervals == 1:
                current_region.append(event[0])
                combined.append(current_region)
                current_region = []
            open_intervals -= 1
    return combined


region_or = combine_regions

if __name__ == "__main__":
    contact_1 = [[0.0, 1.2]]
    contact_2 = [[0.4, 0.6], [1.3, 1.5]]
    contact_3 = [[1.6, 2.6]]
    contact_4 = [[0.4, 0.6]]

    data = [contact_1, contact_2, contact_3, contact_4]

    def extended(x, y):
        x.extend(y)
        return x
    print reduce(extended, data, [])
    print combine_regions(reduce(extended, data, []))
