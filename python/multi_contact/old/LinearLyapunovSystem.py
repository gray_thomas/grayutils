import lyapunov as ly
import ly_utils as lu
import numpy as np
import humeID.vectorGeometry
from PairIntersectionDetector import PairIntersectionDetector


@lu.namedstatesystem("Energy_State", ['s'], xname="s_state", tname='E')
class LinearLyapunovSystem(object):

    """Dummy system. s dot = self.srate."""

    def __init__(self, srate, sinit=0.0):
        self.state = 0.0, (sinit,)
        self.srate = srate

    def __call__(self):
        return self.srate,

@lu.namedstatesystem("Energy_State", ['s'], xname="s_state", tname='E')
class ExpLyapunovSystem(object):

    """Dummy system. s dot = self.srate."""

    def __init__(self, srate, sinit=0.0):
        self.state = 0.0, (sinit,)
        self.srate = srate

    def __call__(self):
        return self.srate*self.s,



def main():
    import matplotlib.pyplot as plt
    systemA = ExpLyapunovSystem(-1.0, sinit=1.0)
    systemB = LinearLyapunovSystem(1.0, sinit=0.0)
    systemC = PairIntersectionDetector(systemA, systemB)
    system = lu.ParallelHybridSystems([systemA,systemB,systemC])
    logger = ly.Recorder(system)
    path_energies = np.linspace(0, 1.0, 101)
    stepper = ly.dormand_prince(system, path_energies)
    stepper.time_tolerance = 1e-4
    stepper.rel_tolerance = 1e-3

    for time, events in stepper:
        logger.log(events)
        if events:
                try:
                    system.handle_events(stepper, events)
                except StopIteration:
                    print "intersection found!"
                    break
        try:
            ly.check_NaN(system)
        except ArithmeticError:
            break
    plt.plot(logger.t, logger.x)
    plt.show()

if __name__ == "__main__":
    main()
