import lyapunov as ly
import ly_utils as lu
import numpy as np
import cvxpy as cvx
from exceptions import NotImplementedError
from numpy import sign
from math import sqrt
from OptimalMulticontactSystem import OptimalMulticontactSystem


class SpeedContourGenerator2(OptimalMulticontactSystem):


    def __init__(self, contact_combination, height_surface, robot):
        """
        Setup the system.

        contact_combination is a list of contacts, with friction, force limits, kinematic limits
        height_surface leads from start to goal.
        robot gives the mass, gravity, and leg_length
        default state is at the start of the path with no speed.
        active set variable stores each contact as [basis, basis, upperbound]
        true in the active set means it is active (creating a lambda)
        """
        super(SpeedContourGenerator2, self).__init__(
            contact_combination, height_surface, robot, is_max_accel=is_max_accel)
        self.speed = speed

    def get_accel_lim(self, speed, is_max_accel, path_s):
        """Use the base class, return the optimal accel, given the speed."""
        self.is_max_accel = is_max_accel
        res = self.solve(path_s, self.speed)
        # return sqrt(res.accel.T.dot(res.accel))
        normal_vector, projected_bias_accel, path_point, dxds = self.calculate_path_properties(path_s, self.speed)
        return sqrt(dxds.T.dot(dxds))*res.s_ddot

    def get_speed_lim(self, is_max_speed, accel, path_s):
        """Use the base class, return the optimal accel, given the speed."""
        self.is_max_accel = is_max_accel
        res = self.solve(path_s, self.speed)
        res = self.solve(path_s, self.speed)
        # return sqrt(res.accel.T.dot(res.accel))
        normal_vector, projected_bias_accel, path_point, dxds = self.calculate_path_properties(path_s, self.speed)
        return sqrt(dxds.T.dot(dxds))*res.s_ddot