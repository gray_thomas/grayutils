from intersection.OptimalMulticontactSystem import OptimalMulticontactSystem, InfeasibilityException
import numpy as np


class MultiContactPhaseSpaceSurfacer(object):

    def __init__(self, world):
        self.contact_data = world.contact_data
        solver = OptimalMulticontactSystem(
            world.contact_data[0], world.com_path, world.robot)

        self.data = []  # quadruple nested array
        for contact_config in self.contact_data:
            for max_speed in [True, False]:
                solver.contact_configuration = contact_config
                data1=[]
                for s in np.linspace(0, 0.5, 10):
                    data2=[]
                    for sdd in np.linspace(-7, 7, 10):
                        sd = solver.solve_speed(s, sdd, max_speed=max_speed)
                        data2.append([sd, s, sdd])
                    data1.append(data2)
                self.data.append(data1)

class Surfacer2(object):
    def __init__(self, world, s_vals = np.linspace(0,0.5,10), sd_vals = np.linspace(0, 1.5, 10)):
        self.contact_data = world.contact_data
        solver = OptimalMulticontactSystem(
            world.contact_data[1], world.com_path, world.robot)

        self.data = []  # quadruple nested array
        for i in range(0, len(world.contact_data)):
            solver.contact_combination=self.contact_data[i]
            for is_max_accel in [True, False]:
                solver.is_max_accel = is_max_accel
                data1=[]
                for s in s_vals:
                    data2=[]
                    for sd in sd_vals:
                        try:
                            res = solver.solve(*world.com_path.from_arc(s, sd))
                            sdd = res.l_ddot
                        except InfeasibilityException:
                            sdd = float('nan')
                        data2.append([sd, s, sdd])
                    data1.append(data2)
                self.data.append(data1)
        print "self.data size" , len(self.data)
