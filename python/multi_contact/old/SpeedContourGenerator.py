import lyapunov as ly
import ly_utils as lu
import numpy as np
import cvxpy as cvx
from exceptions import NotImplementedError
from numpy import sign
from math import sqrt
from OptimalMulticontactSystem import OptimalMulticontactSystem


@lu.hybridsystemfunctor
@lu.namedstatesystem("ArcLengthState", ['arc_s'], xname="s_state", tname='path_s')
class SpeedContourGenerator(OptimalMulticontactSystem):

    """Another hybrid system used for planning."""

    def __init__(self, contact_combination, height_surface, robot, speed=0.0, is_max_accel=True):
        """
        Setup the system.

        contact_combination is a list of contacts, with friction, force limits, kinematic limits
        height_surface leads from start to goal.
        robot gives the mass, gravity, and leg_length
        default state is at the start of the path with no speed.
        active set variable stores each contact as [basis, basis, upperbound]
        true in the active set means it is active (creating a lambda)
        """
        super(SpeedContourGenerator, self).__init__(
            contact_combination, height_surface, robot, is_max_accel=is_max_accel)
        self.speed = speed
        self.state = (0, (0.0,))
        self.events = [self.feasibility_event]

    def handle_events(self, events):
        for event in events:
            for case in lu.switch(event):
                if case(self.feasibility_event):
                    print "feasibility"
                    break
                if case(self.kinematic_feasibility_event):
                    print "kinematic"
                    break
                if case(self.end_of_path_event):
                    raise StopIteration()
                if case():
                    raise NotImplementedError()
        raise lu.StopContinuousIteration

    def get_accel(self):
        """Use the base class, return the optimal accel, given the speed."""
        res = self.solve(self.path_s, self.speed)
        # return sqrt(res.accel.T.dot(res.accel))
        normal_vector, projected_bias_accel, path_point, dxds = self.calculate_path_properties(self.path_s, self.speed)
        return sqrt(dxds.T.dot(dxds))*res.s_ddot

    def feasibility_event(self):
        """Changes sign when the system becomes feasible or becomes infeasible*."""
        return 1.0

    def kinematic_feasibility_event(self):
        """Change sign when the kinematics become feasible or infeasible."""
        return 1.0

    def end_of_path_event(self):
        """Changes sign when the system reaches the end of the path."""
        return self.path_s - height_surface.max_path_s

    def __call__(self):
        """Return expression for arc length differential."""
        normal_vector, projected_bias_accel, path_point, dxds = self.calculate_path_properties(self.path_s, self.speed)
        return sqrt(dxds.T.dot(dxds)),