import lyapunov as ly
import ly_utils as lu
import numpy as np
import cvxpy as cvx
from exceptions import NotImplementedError
from numpy import sign
from math import sqrt
from OptimalMulticontactSystem import OptimalMulticontactSystem


@lu.hybridsystemfunctor
@lu.namedstatesystem("Energy_State", ['s', 't'], xname="s_state", tname='E')
class MultiContactEnergyIntegrator(OptimalMulticontactSystem):

    """A hybrid system used for planning."""

    def __init__(self, world, starting_s=0.0, starting_energy=1e-7, is_forward=True, is_max_accel=True):
        """
        Setup the system based on a robot.

        contacts is the list of contacts, with friction
        com_path leads from start to goal.
        robot gives the mass, gravity, and leg_length
        default state is at the start of the path with no speed.
        active set variable stores each contact as [basis, basis, upperbound]
        true in the active set means it is active (creating a lambda)
        """
        super(MultiContactEnergyIntegrator, self).__init__(
            world.contact_data[0], world.com_path, world.robot, is_max_accel=is_max_accel)
        self.contacts = world.contact_data
        self.is_forward = is_forward

        self.state = (starting_energy, (starting_s, 0.0))
        if not is_forward:
            self.contact_combination = world.contact_data[-1]

        self.events = [self.peak_energy_event]

    def handle_events(self, stepper, event):
        print "max energy reached!"
        raise StopIteration()

    def peak_energy_event(self):
        ret = self()
        # print ret
        return ret[0]

    @property
    def s_dot(self):
        i = int(self.s)
        path_s = self.s - i
        if i == self.s and i != 0:
            i -= 1
            path_s = 1.0
        velocity_squared = 2.0 * self.E / self.robot.mass
        dxds = self.com_path.velocity_vector(i, path_s)
        s_dot_squared = velocity_squared / (dxds.T.dot(dxds))
        abs_s_dot = sqrt(s_dot_squared)
        if self.is_forward:
            s_dot = abs_s_dot
        else:
            s_dot = -abs_s_dot
        return s_dot

    @property
    def dxds(self):
        i = int(self.s)
        path_s = self.s - i
        if i == self.s and i != 0:
            i -= 1
            path_s = 1.0
        velocity_squared = 2.0 * self.E / self.robot.mass
        dxds = self.com_path.velocity_vector(i, path_s)
        return dxds

    def __call__(self):
        """
        Return the derivitive of the state, s, with respect to E.

        """

        mutli_res = self.solve(self.s, self.s_dot)
        net_wrench = mutli_res.net_wrench
        dxds = self.dxds
        kinetic_s = (net_wrench[0, 0] * dxds[0, 0] +
                     net_wrench[1, 0] * dxds[1, 0])

        s_kinetic = 1.0 / kinetic_s

        xx_ss = dxds.T .dot(dxds)
        s_time = sqrt((2.0 * self.E / self.robot.mass) / (xx_ss[0, 0]))

        kinetic_time = s_time * kinetic_s
        if np.isnan(kinetic_time):
            kinetic_time = 1e-7
        time_kinetic = 1.0 / kinetic_time

        if self.is_forward:
            s_kinetic *= sign(s_kinetic)
        else:
            s_kinetic *= - sign(s_kinetic)
        return (s_kinetic, time_kinetic)


