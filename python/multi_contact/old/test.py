from collections import namedtuple
class MyClass(object):

	def __init__(self):
		pass
	def call(self):
		return MyClass.TupleClass(x=2)
	TupleClass = namedtuple("TupleClass",["x"])

if __name__=="__main__":
	obj = MyClass()
	print obj.call()