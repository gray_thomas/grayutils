from scipy.interpolate import interp1d

class WrappedLeafFunction(object):

    def __init__(self, func):
        self.call = func

    def count(self, x):
        return 0.0

    def __repr__(self):
        return "{%.3f, %.3f}" % (self.x_min, self.x_max)

    @property
    def x_max(self):
        return self.call.x[-1]

    @property
    def x_min(self):
        return self.call.x[0]

    def __call__(self, x):
        return self.call(x)


class _CompositeFunctor(object):

    def __init__(self, wfuncs):
        self.d = len(wfuncs) / 2
        part1 = wfuncs[:self.d]
        part2 = wfuncs[self.d:]
        if len(part1) == 1:
            self.low = part1[0]
        else:
            self.low = _CompositeFunctor(part1)
        if len(part2) == 1:
            self.high = part2[0]
        else:
            self.high = _CompositeFunctor(part2)
        self.div = wfuncs[self.d].x_min

    def __repr__(self):
        return " ".join(["[l:", str(self.low), ", h:", str(self.high), "]"])

    def count(self, x):
        if x >= self.div:
            return self.high.count(x) + self.d
        return self.low.count(x)

    @property
    def x_max(self):
        return self.high.x_max

    @property
    def x_min(self):
        return self.low.x_min

    def __call__(self, x):
        if x >= self.div:
            assert not isnan(self.high(x))
            return self.high(x)
        assert not isnan(self.low(x))
        return self.low(x)


def wrapMonotonicallyIncreasingFunctions(funcs):
    for func in funcs:
        for x1, x2 in zip(func.x[0:-1], func.x[1:]):
            assert x1 <= x2
        for y1, y2 in zip(func.y[0:-1], func.y[1:]):
            assert y1 <= y2
    wfuncs = [WrappedLeafFunction(func) for func in funcs]
    return wfuncs


def wrapFunctions(funcs):
    for func in funcs:
        for x1, x2 in func.x[0:-1], func.x[1:]:
            assert x1 <= x2
    wfuncs = [WrappedLeafFunction(func) for func in funcs]
    return wfuncs


def CompositeFunctor(wfuncs):
    if len(wfuncs) == 0:
        raise NotImplementedError()
    if len(wfuncs) == 1:
        return wfuncs[0]
    wfuncs = sorted(wfuncs, key=lambda f: f.x_min)
    for f1, f2 in zip(wfuncs[0:-1], wfuncs[1:]):
        assert (f1.x_max == f2.x_min)
    return _CompositeFunctor(wfuncs)