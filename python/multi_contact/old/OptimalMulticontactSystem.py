import lyapunov as ly
import ly_utils as lu
import numpy as np
import cvxpy as cvx
from exceptions import NotImplementedError
from numpy import sign
from math import sqrt
from collections import namedtuple
from multi_contact.Bezier2Splines import convert_s
from enum import Enum


Goals = Enum('Goals', 'accel decel zero')


class OptimizationError(Exception):

    """Base class for errors in this module."""
    pass


class InfeasibilityException(OptimizationError):

    """Raise to represent problem infeasibility."""

    def __init__(self, problem_string):
        self.problem_string = problem_string


class OptimalMulticontactSolver(object):

    def __init__(self, stance, com_path, robot):

        self.stance = stance
        self.com_path = com_path
        self.robot = robot

        self.basis_vectors_array = self.setup_basis()
        self.num_contacts = len(stance)

    def is_viable(self, xi):
        return self.stance.xi_regions.contains(xi)

    def setup_basis(self):
        """setup the convex optimization matrices and variables."""
        num_contacts = len(stance)
        if num_contacts == 0:
            raise NotImplementedError()
        basis_vectors_list = []
        total_basis = sum([contact.num_basis for contact in stance])
        total_inequalities = sum(
            [contact.num_inequalities for contact in stance])
        # leq_mat_list = np.zeros((total_inequalities, total_basis))
        # leq_vec_list = np.zeros((total_inequalities, 1))
        for contact in stance:
            b1, b2 = contact.basis()
            # A, b = contact.inequality_constraints()
            basis_vectors_list.append(b1)
            basis_vectors_list.append(b2)
        basis_vectors_array = np.array(basis_vectors_list).T
        return basis_vectors_array

    def setup_constraints(self, phi, s, s_dot, basis_vectors_array, speed=False):
        mass = self.robot.mass
        gravity = self.robot.gravity_accel
        normal_vector, projected_bias_accel, path_point, dxds = self.calculate_path_properties(
            s, s_dot)
        com_x, com_y = path_point
        mult_rot = cvx.Constant(
            np.array([[com_y, -com_x, 1.0]]).dot(basis_vectors_array))
        bias_vert = cvx.Constant(
            mass * normal_vector.T.dot(
                projected_bias_accel - np.array([[0], [gravity]])
            ))
        mult_vert = normal_vector.T.dot(basis_vectors_array[0:2, :])

        constraints = [
            mult_rot * phi == cvx.Constant(0),
            mult_vert * phi == bias_vert,
            phi >= self.stance[0].min_phi,
        ]
        for i in range(0, len(self.stance)):
            constraints.append(
                phi[2 * i] + phi[2 * i + 1] <= self.stance[i].max_force)
        return constraints

    def setup_obj(self, s, phi, bias_wrench_array, basis_vectors_array):
        dxds = self.com_path.velocity_vector(*convert_s(s))
        bias_obj = dxds.T.dot(bias_wrench_array[0:2, :])
        mult_obj = dxds.T.dot(basis_vectors_array[0:2, :])
        if self.is_max_accel == None:
            return cvx.Minimize(abs(bias_obj + mult_obj * phi))
        if self.is_max_accel:
            obj = cvx.Maximize(bias_obj + mult_obj * phi)
        else:
            obj = cvx.Minimize(bias_obj + mult_obj * phi)
        return obj

    ContactResult = namedtuple("ContactResult", ["contact", "wrench"])
    OptimizationResult = namedtuple(
        "OptimizationResult", ["accel", "net_wrench", "contacts", "s_ddot", "l_ddot"])

    def solve(self, s, s_dot, goal=None):
        """Return a namedtuple solution."""
        if goal != None:
            if goal == Goals.accel:
                self.is_max_accel = True
            if goal == Goals.decel:
                self.is_max_accel = False
            if goal == Goals.zero:
                self.is_max_accel = None

        path_point = self.com_path.point(*convert_s(s))
        mass = self.robot.mass
        gravity = self.robot.gravity_accel

        bias_wrench_array = np.array([
            [0, gravity * mass, gravity * mass * path_point[0]]]).T

        phi = cvx.Variable(2 * num_contacts)
        constraints = self.setup_constraints(
            phi, s, s_dot, basis_vectors_array)
        obj = self.setup_obj(s, phi, bias_wrench_array, basis_vectors_array)

        prob = cvx.Problem(obj, constraints)

        return self.extract_result(prob, s, s_dot, phi, basis_vectors_array, bias_wrench_array, mass)

    def rotation_constraint(self):
        com_x, com_y = self.com_path.x(xi)
        mult_rot = cvx.Constant(
            np.array([[com_y, -com_x, 1.0]]).dot(basis_vectors_array))
        return mult_rot * phi == cvx.Constant(0)

    def solve_speed(self, s, s_ddot, max_speed=True):
        """Return a namedtuple solution."""
        num_contacts = len(self.stance)
        basis_vectors_array = self.setup_basis(self.stance)

        path_point = self.com_path.point(*convert_s(s))
        mass = self.robot.mass
        gravity = self.robot.gravity_accel

        bias_wrench_array = np.array([
            [0, gravity * mass, gravity * mass * path_point[0]]]).T

        phi = cvx.Variable(2 * num_contacts)

        mass = self.robot.mass
        gravity = self.robot.gravity_accel
        # normal_vector, projected_bias_accel, path_point, dxds = self.calculate_path_properties(
        #     s, s_dot)
        com_x, com_y = path_point
        mult_rot = cvx.Constant(
            np.array([[com_y, -com_x, 1.0]]).dot(basis_vectors_array))

        constraints = [
            mult_rot * phi == cvx.Constant(0),
            # mult_vert * phi == bias_vert, # now an optimization
            phi >= self.stance[0].min_phi,
        ]
        for i in range(0, len(self.stance)):
            constraints.append(
                phi[2 * i] + phi[2 * i + 1] <= self.stance[i].max_force)
        xp = self.com_path.x_prime(s)
        xpp = self.com_path.x_double_prime(s)
        # tangent, normal = self.com_path.frenet_serret(s)
        # tangent = xp / sqrt(xp.T.dot(xp))
        # normal = xpp-tangent.T.dot(xpp)

        normal_vector = self.com_path.normal_vector(*convert_s(s))

        sdot_squared = cvx.Variable(1)

        # new vector version
        g_bias = np.array([[0.0], [gravity]])
        bias_accels = basis_vectors_array[0:2, :] / mass
        coriolis_accel = self.com_path.centrifugal_force_vector(*convert_s(s))
        constraints.append(
            g_bias + bias_accels * phi - coriolis_accel * sdot_squared - xp * s_ddot == 0)

        if max_speed:
            prob = cvx.Problem(cvx.Maximize(sdot_squared), constraints)
        else:
            prob = cvx.Problem(cvx.Minimize(sdot_squared), constraints)
        obj_result = prob.solve(solver="CVXOPT")
        if prob.status != 'optimal':
            return float('nan')
        if obj_result < 0:
            return float('nan')
        return sqrt(obj_result)

    def extract_result(self, prob, s, s_dot, phi, basis_vectors_array, bias_wrench_array, mass):
        obj_result = prob.solve(solver="CVXOPT")

        if prob.status != 'optimal':
            if prob.status == "infeasible":
                raise InfeasibilityException(str(prob))
            print prob.status
            print prob

        res = np.array(phi.value)
        net_wrench = basis_vectors_array.dot(res) + bias_wrench_array
        accel = net_wrench[0:2, :] / mass
        # obj_result = dxds.T.dot(bias+ basis*phi)
        # obj_result = dxds.t.dot(mass*accel)
        xp = self.com_path.diff1(s)
        xpp = self.com_path.diff2(s)
        s_ddot1 = float((obj_result / mass / xp.T.dot(xp) -
                         xp.T.dot(xpp) / xp.T.dot(xp) * pow(s_dot, 2)))

        contact_results = []

        l_ddot = float(accel.T.dot(xp) / sqrt(xp.T.dot(xp)))
        s_ddot = float((accel.T.dot(xp) / xp.T.dot(xp) -
                        xp.T.dot(xpp) / xp.T.dot(xp) * pow(s_dot, 2)))

        assert (abs(s_ddot - s_ddot1) < 1e-5)

        for i, contact in enumerate(self.stance):
            contact_results.append(self.ContactResult(
                contact=contact, wrench=basis_vectors_array[:, 2 * i:2 * (i + 1)].dot(res[2 * i:2 * (i + 1), [0]])))
        return self.OptimizationResult(
            accel=accel,
            net_wrench=net_wrench,
            contacts=contact_results,
            s_ddot=s_ddot,
            l_ddot=l_ddot)

    def min_xi_ddot(self, xi, xi_dot):
        pass

if __name__ == '__main__':
    import multi_contact.worlds.demo8_world as world
    solver = OptimalMulticontactSystem(
        world.contact_data[0], world.com_path, world.robot)
    print solver.is_viable(0.4)
    print solver.min_xi_ddot(0.4, 0.2)
