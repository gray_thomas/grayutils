"""
MultiContactPlanner module.

Chooses contact forces, transition times, etc...
"""

import numpy as np
import cvxpy as cvx
from math import sin, cos, atan, sqrt
from collections import namedtuple
import lyapunov as ly
import ly_utils as lu
import matplotlib.pyplot as plt
from intersection.MultiContactEnergyIntegrator import MultiContactEnergyIntegrator
from intersection.PairIntersectionDetector import PairIntersectionDetector
from intersection.section_combination import combine_regions
from intersection.MultiContactExplorer import MultiContactExplorer, ADZI
from intersection.MultiContactIntersectableSystem import MCPropogator, PropogationNuclei, Terminations


class MultiContactPlanner(object):

    """Does the real work."""

    def __init__(self, world, stances):
        """
        Use a predefined switching time switch sequence for now.
        We assume the end of the height_surface is the goal.
        """

        self.world = world
        self.stances = stances

    def propogate_dynamics(self, path_s_decomposition):

        propogation_nuclei = [
            PropogationNuclei(
                s=0.0, s_dot=0.0, forward_time=True, max_accel=True),
            PropogationNuclei(
                s=self.world.com_path.max_path_s, s_dot=0.0, forward_time=False, max_accel=False)
        ]

        for i in range(0, len(path_s_decomposition) - 1):
            sec1, sec2 = path_s_decomposition[i: i + 2]
            s = sec2.start
            s_dot = (self.world.robot.max_speed /
                     self.world.com_path.speed(s, 1.0))

            if ADZI.A == sec1.region_code and ADZI.Z == sec2.region_code:
                propogation_nuclei.append(PropogationNuclei(
                    s=s, s_dot=s_dot, forward_time=False, max_accel=False))

            if ADZI.Z == sec1.region_code and ADZI.D == sec2.region_code:
                propogation_nuclei.append(PropogationNuclei(
                    s=s, s_dot=s_dot, forward_time=True, max_accel=True))

        integration_results=[]
        for nucleus in propogation_nuclei:
            prop = MCPropogator(self.world, self.stances, nucleus)
            print nucleus
            integration_results.append(prop.integrate())
        return integration_results
        # return contact_segmentation, goal_segmentation
    def plan_path(self, integration_results):

        # by case, recursion:
        # recursive_plan_path(self)
        # uses self.trajectory_up_to_this_point
        # ordered list of back facing integrations
        # these integrations are paired with forward facing integrations
        # plateau trajectories count as part of the back facing trajectories
        # ordered list of tuples
        # error code of None indicates the goal trajectory, which should be last as well.
        # Nucleus should thus have forward and reverse trajectories attached to it.
        # Also, sub-optimal zig-zag may be implemented here
        # Should the planning process be idealized, as to mach the model
        # Or should I corrupt it here to simplify the total process?
        # Really that's just the first idea I had. An alternative:
        # Include zero regions explicitly
        # Write an intersection function with switching trajectory/zero logic
        # Generate the ideal trajectory which includes zero following
        # Indicate to the user what is going on
        # Generate obvious, fast, switching
        # Intersect this switching (which is sub-optimal) with the perfect trajectory
        # Traversability is still going to happen
        # Unless the zero region has a low speed bound
        # 
        # Extension: Suppose every contact must be maintained for a set time
        # Does this disrupt the unambiguity of the optimal trajectory generation?
        # Yes.
        # It becomes a combinatorial problem to find the optimal trajectories now.
        # Between A and B and between B and C there may exist some optimal trajectory
        # But the optimal trajectory between A and C may be faster still.
        # Or rather, A, B, and C must all be extended in state to allow the timer variable
        # to recreate the conditions that lead to reusable solutions.
        # So we need to consider planning with an extra state variable in this case.
        # And an extra discrete state.
        # Well, naturally we are forced to integrate until we have the opportunity to switch states
        # We could exploit key-frames
        # We must avoid brute force searching.
        # The problem is not as easibly tractable as the free-switching case
        # And the free-switching case can be adjusted by an outer optimization to enforce such
        # time-related desires, using serialization and path-parameterized stance-limits
        # this approach can also eliminate the switching cases directly.
        # however it is not hard to find switching trajectories.

        # Building the objects:
        # we need lookup of s_dot(s)
        # we name it IntersectableTrajectory
        # we need self.next_forward() # = None if last path
        # it starts with empty lookup
        # it starts with next_forward() returns path start trajectory
        # we probably want everything to start as an intersectable trajectory
        # so step one is to define the class and construct it from the integration history?
        # we also need to classify zeros more specifically
        # perhaps Accel Decel Zero Infeasible Either
        # perhaps Pos Neg Zero nOn_zero Infeasible
        # perhaps Accel Decel Nonzero Zero Infeasible
        # perhaps positive, negative, dichotomy, free, infeasible
        # maybe with a graphic: table: big red x big green check
        # Does at least one contact permit zero acceleration along the path when traveling at max_speed? (only free)
        # Does at least one contact permit acceleration when traveling at max_speed? (positive, dichotomy, free)
        # Does at least one contact permit deceleration when traveling at max_speed? (negative, dichotomy, free)
        # For each dichotomy we can construct an intersectable line beforehand, assuming max_speed at either extreme.
        # all dichotomy regions are traversable if you switch fast enough.

        # deep thoughts: what is enforceable beyond optimal speed bang-bang?
        # what am I really doing, in an abstract sense?

        # I am formulating multicontact in a form which allows at ;least part
        # of the problem to be efficiently solved by a specially designed
        # algorithm. The formulation I have settled on is a time optimal
        # control problem for a linear system with state-based convex input
        # constraints and hybrid switching between a finite number of
        # alternative convex input constraint sets. The input constraints are
        # a series of linear inequalities, and the state influences these
        # linear constraints non-linearly, through a quadratic dependance on
        # the speed state. A relationship with more roots than this would
        # violate the guarantee of my algorithm: failure to find a path in my
        # sense would not imply the problem had no solutions. If the
        # dependance was more complex, I suspect the problem could not be
        # solved in comparable time, because the choice of "speed window"
        # would come down to a series of boolean choices, and the optimal
        # value and feasibility would depend on the boolean choices in a
        # dependant way where information about one case would not necessarily
        # eliminate other cases. The relationship between the first state
        # along which I integrate, arc-length, and the optimization
        # constraints is bilinear (in the equality constraint), but could be
        # far more general. This potentially allows for an alternate model,
        # not a center of mass path following, but a parametrically defined
        # kinematic motion. In this case the dynamics would still be in one
        # dimension and its derivatives. The choice of contact would reflect
        # the reaction forces and would potentially reflect a desire to force
        # a choice between alternate combinations of contacts with the
        # intention to modify the kinematic path later to remove those which
        # were not chosen. In this case the derivative of the parameterization
        # would similarly influence the system quadratically, through
        # centrifugal forces, and possibly a linear damping-related term. In
        # this case the explicit mapping between the sytem's position and its
        # full kinematic state could be exploited to provide additional
        # inequality constraints on the contact forces based on the  rigid
        # body dynamics and actuator torque limits upper speed limits could be
        # imposed additionally by the actuator speed limits this would lead to
        # a higly efficient way to check dynamic feasibility of a  kinematic
        # path. Potentially one which had had its center of mass path
        # validated by this algorithm already.

        # Another easy extension is to allow a path parameterized "rotation"
        # component, which will extend the center of mass model to permit non-
        # zero moment. In this case it would be necessary to define an
        # instantaneous rotational inertia as well as a continuous
        # relationship between path position and orientation. This will be
        # used to define a required moment which is linear in the speed state. Ultimately this
        # will translate to a modification of the equality constraint which represents
        # 

        raise NotImplementedError()
        return contact_segmentation, goal_segmentation
