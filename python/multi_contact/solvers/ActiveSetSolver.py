import lyapunov as ly
import ly_utils as lu
import numpy as np
import cvxpy as cvx
from exceptions import NotImplementedError
from numpy import sign
from math import sqrt
from collections import namedtuple
from enum import Enum
import time
from scipy import optimize


class InfeasibilityException(Exception):

    """Raise to represent problem infeasibility."""

    def __init__(self, problem_string):
        self.problem_string = problem_string


class ActiveSetException(Exception):

    """Raise to represent an active set change."""

RequestType = Enum('RequestType', 'opt_accel opt_speed feas_check')
_Request = namedtuple(
    "SolverRequest", ["type", "is_max", "xi", "xi_dot", "xi_ddot_desired"])

_num_solves = 0


def null(A, eps=1e-15):
    paddedA = np.vstack([A, np.zeros((A.shape[1] - A.shape[0], A.shape[1]))])
    u, s, vh = np.linalg.svd(paddedA)
    null_mask = (s <= eps)
    null_space = np.compress(null_mask, vh, axis=0)
    return np.transpose(null_space)

# if __name__ == '__main__':
#     A = np.array([[2, 3, 5], [-4, 2, 3]])
#     print A
#     print A.shape
#     paddedA = np.vstack([A, np.zeros((A.shape[1] - A.shape[0], A.shape[1]))])
#     print paddedA
#     print np.zeros((1, 3))
#     print null(A)
#     print A.dot(null(A))
#     # exit()


class ActiveSetMCS(object):

    def __init__(self, stance, com_path, robot, indices):
        self.indices = indices

        self.stance = stance
        self.com_path = com_path
        self.robot = robot

        self._setup_basis()
        self.num_contacts = len(stance)
        self._request = _Request(RequestType.opt_accel, True, 0.2, 0.5, None)
        self._result = None
        self.active_set = None
        self.active_set_compliment = None

    @property
    def xi(self):
        return self._request.xi

    @property
    def xi_dot(self):
        return self._request.xi_dot

    @property
    def is_max(self):
        return self._request.is_max

    @property
    def g(self):
        return self.robot.gravity_accel

    @property
    def m(self):
        return self.robot.mass

    def _setup_basis(self):
        """setup the convex optimization matrices and variables."""
        stance = self.stance
        num_contacts = len(stance)
        if num_contacts == 0:
            raise NotImplementedError()
        basis_vectors_list = []
        total_basis = sum([contact.num_basis for contact in stance])
        total_inequalities = sum(
            [contact.num_inequalities for contact in stance])
        # leq_mat_list = np.zeros((total_inequalities, total_basis))
        # leq_vec_list = np.zeros((total_inequalities, 1))
        for contact in stance:
            b1, b2 = contact.basis()
            # A, b = contact.inequality_constraints()
            basis_vectors_list.append(b1)
            basis_vectors_list.append(b2)
        self._basis_vectors_array = np.array(basis_vectors_list).T


        # inequalities never change!
        self.n = len(self.stance)
        self.n_ub = 3 * self.n
        self.n_var = 2 * self.n + 2
        self.A_ub = np.zeros((self.n_ub, self.n_var))
        self.b_ub = np.zeros((self.n_ub, ))
        self.c = np.zeros((self.n_var, ))
        for i in range(0, self.n):
            # iEq1: phi_i,0 >=0
            self.A_ub[3 * i + 0, 2 * i] = -1.0
            # iEq2: phi_i,1 >=0
            self.A_ub[3 * i + 1, 2 * i + 1] = -1.0
            # iEq3: phi_i,0+phi_i,1 <=max_force
            self.A_ub[3 * i + 2, 2 * i] = 1.0
            self.A_ub[3 * i + 2, 2 * i + 1] = 1.0
            self.b_ub[3 * i + 2] = self.stance[i].max_force

    def _rotation_constraint(self):
        com_x, com_y = self.com_path.x(self.xi)
        self.A_eq[
            0, 0:-2] = np.array([[com_y, -com_x, 1.0]]).dot(self._basis_vectors_array)
        self.b_eq[0] = 0.0
        # return np.hstack([mult_rot, np.array([[0, 0]])]), 0.0

    def _path_constraint(self):
        ddx_dxi2 = self.com_path.ddx_dxi2(self.xi)
        dx_dxi = self.com_path.dx_dxi(self.xi)
        A_rows = np.hstack([
            ((-1 / self.m) * self._basis_vectors_array[0:2, :]), ddx_dxi2, dx_dxi])
        return A_rows, np.array([0, self.g])

    def _xi_dot_squared_constraint(self):
        A_row = np.zeros((self.n_var, ))
        A_row[-2] = 1.0
        return A_row, pow(self.xi_dot, 2)

    def _setup_KKT(self):
        """
        Minimize:       c^T * x
        Subject to:     A_ub * x <= b_ub
                        A_eq * x == b_eq
        x' = [phi', xid^2, xidd]'
        Inequality Constraints:
            phi_i,0+phi_i,1 <= max_force   i=0,...,n_c-1
            -phi_i_0 <= 0   i=0,...,n_c-1
            -phi_i_1 <= 0   i=0,...,n_c-1
        Equality Constraints:
            _rotation_constraint
            _path_constraint
            opt_accel ? xi_dot_squared = xi_dot^2 :
        Goal:
            opt_accel ? (is_max?xi_ddot:-xi_ddot):
            opt_speed ? (is_max?xi_dot_squared:-xi_dot_squared):
        """
        n = len(self.stance)
        self.n_eq = 4 if self._request.type == RequestType.opt_accel else 3
        # number of changing equalities is known apriori
        self.n_ceq = 4 if self._request.type == RequestType.opt_accel else 3  # moment, position, xi_dot equalities change with xi
        n_var = 2 * n + 2
        self.n_var = n_var
        self.A_eq = np.zeros((self.n_eq, self.n_var))
        self.b_eq = np.zeros((self.n_eq, ))

        # Eq1: _rotation
        # self.A_eq[0, :], self.b_eq[0] =
        self._rotation_constraint()
        # Eq2: _path
        self.A_eq[1: 3, :], self.b_eq[1: 3] = self._path_constraint()
        # optional Eq3
        if self._request.type == RequestType.opt_accel:
            self.A_eq[3, :], self.b_eq[3] = self._xi_dot_squared_constraint()

        # Goal:
        self.c = np.zeros((self.n_var, ))
        if self._request.type == RequestType.opt_accel:
            # remember: this will be minimized!
            self.c[-1] = -1.0 if self.is_max else 1.0
        if self._request.type == RequestType.opt_speed:
            self.c[-2] = -1.0 if self.is_max else 1.0
        return self.c, self.A_eq, self.b_eq

    def _solve(self):
        global _num_solves
        _num_solves += 1
        if _num_solves % 100 == 0:
            print "solved", _num_solves, "times!"
        # print "solver ", self.indices
        self._setup_KKT()
        expect_asc = False
        if not self.active_set == None:
            try:
                phi, xi_dot_squared, xi_ddot, legit = self._solve_using_active_set()
            except np.linalg.LinAlgError:
                legit = False
            if legit:
                return phi, xi_dot_squared, xi_ddot
            else:
                print "active set change!"
                expect_asc = True

        # c, A_eq, b_eq =
        # print self.c
        res = optimize.linprog(self.c, A_ub=self.A_ub, b_ub=self.b_ub, A_eq=self.A_eq,
                               b_eq=self.b_eq, method='simplex', bounds=(None, None), callback=None, options=None)
        # print res
        if not res.success:
            print "infeasible!"
            raise InfeasibilityException(str(res))
        self._change_active_set(res)

        return res.x[0:2 * len(self.stance)], res.x[-2], res.x[-1]

    def _change_active_set(self, res):
        old_as = self.active_set
        self.saved_slack = res.slack
        self.active_set = [
            i for i, s in enumerate(res.slack) if abs(s) < 1e-7]
        self.active_set_compliment = [
            i for i, s in enumerate(res.slack) if not abs(s) < 1e-7]
        if not old_as == self.active_set:
            print "active set changed by simplex algorithm"
        else:
            print "expected active set change did not occur!"

        self.A_fixed = np.vstack(
            [self.A_eq[self.n_ceq:, :], self.A_ub[self.active_set, :]])
        self.b_fixed = np.hstack(
            [self.b_eq[self.n_ceq:], self.b_ub[self.active_set]])
        AfAfT= self.A_fixed.dot(self.A_fixed.T)
        self.x_fixed = self.A_fixed.T.dot(np.linalg.solve(AfAfT, self.b_fixed))
        self.K_A_fixed = null(self.A_fixed)
        self.l_fixed = np.linalg.solve(AfAfT,self.A_fixed.dot(-self.c))
        self.A_f_pinv = np.linalg.solve(AfAfT,self.A_fixed)
        self.P_fixed = self.A_fixed.T.dot(self.A_f_pinv)
        self.c_prime = self.A_fixed.T.dot(self.A_fixed.dot(self.c))-self.c

        # if len(self.active_set) + self.n_eq < self.n_var:
        #     A = np.vstack([self.A_ub[self.active_set, :], self.A_eq])
        #     G = null(A)

    def _solve_using_active_set(self):
        # solve for primals
        if True: #original solution
            A = np.vstack([self.A_eq, self.A_ub[self.active_set, :]])
            b = np.hstack([self.b_eq, self.b_ub[self.active_set]])
            for i in range(0,A.shape[0]):
                if i>=self.n_ceq:
                    for j in range(0, A.shape[1]):
                        assert(A[i,j]==self.A_fixed[i-self.n_ceq,j])
                    assert(b[i]==self.b_fixed[i-self.n_ceq])


            if A.shape[0] == A.shape[1]:
                x = np.linalg.solve(A, b)
                # print A.shape
                l = np.linalg.solve(A.T, -self.c)
            elif A.shape[0] >= A.shape[1]:
                x = np.linalg.solve(A.T.dot(A), A.T.dot(b))
                l = A.dot(np.linalg.solve(A.T.dot(A), -self.c))


        if False:
            # solve using fixed and changing components
            A_change = self.A_eq[0:self.n_ceq,:]
            b_change = self.b_eq[0:self.n_ceq]
            q = np.linalg.solve(A_change.dot(self.K_A_fixed), b_change - A_change.dot(self.x_fixed))
            print A_change.dot(self.K_A_fixed).shape
            x_approx = self.x_fixed + self.K_A_fixed.dot(q)
            primals_approx = (
            self.A_ub[self.active_set_compliment, :].dot(x_approx)
            - self.b_ub[self.active_set_compliment])
            legit_primal_approx = all([v < 1e-7 for v in primals_approx])
            Cs=A_change.dot(self.K_A_fixed)
            Gamma = Cs.dot(Cs.T)
            print Gamma.shape
            l_change = np.linalg.solve(Gamma,A_change.dot(self.c_prime))
            l_fixed = self.l_fixed-self.A_f_pinv.dot(A_change.T.dot(l_change))
            l_approx = np.hstack([l_change,l_fixed])
            
            # print self.P_fixed
            if False: # test accuracy
                assert sum([abs(e) for e in l-l_approx])< 1e-7
                assert sum([abs(e) for e in x-x_approx])< 1e-7
                assert sum([abs(e) for e in (self.A_fixed.dot(x)-self.b_fixed)])< 1e-7
            else:
                x=x_approx
                l=l_approx

        primals = (
            self.A_ub[self.active_set_compliment, :].dot(x)
            - self.b_ub[self.active_set_compliment])
        duals = l[-len(self.active_set):]
        legit_primal = all([v < 1e-7 for v in primals])
        legit_dual = all([v > -1e-7 for v in duals])
        return x[0:2 * len(self.stance)], x[-2], x[-1], all((legit_primal, legit_dual))

    def find_phi(self, xi, xi_dot, xi_ddot):
        self._request = _Request(
            type=RequestType.feas_check, is_max=False, xi=xi, xi_dot=xi_dot, xi_ddot_desired=xi_ddot)
        phi, xi_dot_squared, xi_ddot = self._solve()
        return phi

    def is_viable(self, xi):
        return self.stance.xi_regions.contains(xi)

    def max_xi_ddot(self, xi, xi_dot):
        self._request = _Request(
            type=RequestType.opt_accel, is_max=True, xi=xi, xi_dot=xi_dot, xi_ddot_desired=None)
        try:
            phi, xi_dot_squared, xi_ddot = self._solve()
        except InfeasibilityException:
            return 10.0
        return xi_ddot

    def min_xi_ddot(self, xi, xi_dot):
        self._request = _Request(
            type=RequestType.opt_accel, is_max=False, xi=xi, xi_dot=xi_dot, xi_ddot_desired=None)
        try:
            phi, xi_dot_squared, xi_ddot = self._solve()
        except InfeasibilityException:
            return -10.0
        return xi_ddot

    def max_xi_dot(self, xi):
        self._request = _Request(
            type=RequestType.opt_speed, is_max=True, xi=xi, xi_dot=None, xi_ddot_desired=None)
        phi, xi_dot_squared, xi_ddot = self._solve()
        if xi_dot_squared < 0.0:
            return 0.0
        return sqrt(xi_dot_squared)

    def min_xi_dot(self, xi):
        self._request = _Request(
            type=RequestType.opt_speed, is_max=False, xi=xi, xi_dot=None, xi_ddot_desired=None)
        phi, xi_dot_squared, xi_ddot = self._solve()
        if xi_dot_squared.value < 0.0:
            return 0.0
        return sqrt(xi_dot_squared)

class ActiveSetAccelMCS(object):

    def __init__(self, stance, com_path, robot, indices):
        self.indices = indices

        self.stance = stance
        self.com_path = com_path
        self.robot = robot

        self._request = _Request(RequestType.opt_accel, True, 0.2, 0.5, None)
        self._setup_basis()
        self.num_contacts = len(stance)
        self._result = None
        self.active_set = None
        self.active_set_compliment = None

    @property
    def xi(self):
        return self._request.xi

    @property
    def xi_dot(self):
        return self._request.xi_dot

    @property
    def is_max(self):
        return self._request.is_max

    @property
    def g(self):
        return self.robot.gravity_accel

    @property
    def m(self):
        return self.robot.mass

    def _setup_basis(self):
        """
        Minimize:       c^T * x
        Subject to:     A_ub * x <= b_ub
                        A_eq * x == b_eq
        x' = [phi', xid^2, xidd]'
        Inequality Constraints:
            phi_i,0+phi_i,1 <= max_force   i=0,...,n_c-1
            -phi_i_0 <= 0   i=0,...,n_c-1
            -phi_i_1 <= 0   i=0,...,n_c-1
        Equality Constraints:
            _rotation_constraint
            _path_constraint
            opt_accel ? xi_dot_squared = xi_dot^2 :
        Goal:
            opt_accel ? (is_max?xi_ddot:-xi_ddot):
            opt_speed ? (is_max?xi_dot_squared:-xi_dot_squared):
        """

        stance = self.stance
        num_contacts = len(stance)
        if num_contacts == 0:
            raise NotImplementedError()
        basis_vectors_list = []
        total_basis = sum([contact.num_basis for contact in stance])
        total_inequalities = sum(
            [contact.num_inequalities for contact in stance])
        # leq_mat_list = np.zeros((total_inequalities, total_basis))
        # leq_vec_list = np.zeros((total_inequalities, 1))
        for contact in stance:
            b1, b2 = contact.basis()
            # A, b = contact.inequality_constraints()
            basis_vectors_list.append(b1)
            basis_vectors_list.append(b2)
        self._basis_vectors_array = np.array(basis_vectors_list).T

        # inequalities never change!
        self.n = len(self.stance)
        self.n_ub = 3 * self.n
        self.n_var = 2 * self.n + 1
        self.n_eq = 3
        self.n_ceq = 3  # moment, position, xi_dot equalities change with xi

        self.A_eq = np.zeros((self.n_eq, self.n_var))
        self.b_eq = np.zeros((self.n_eq, ))
        self.A_ub = np.zeros((self.n_ub, self.n_var))
        self.b_ub = np.zeros((self.n_ub, ))
        self.c = np.zeros((self.n_var, ))
        for i in range(0, self.n):
            # iEq1: phi_i,0 >=0
            self.A_ub[3 * i + 0, 2 * i] = -1.0
            # iEq2: phi_i,1 >=0
            self.A_ub[3 * i + 1, 2 * i + 1] = -1.0
            # iEq3: phi_i,0+phi_i,1 <=max_force
            self.A_ub[3 * i + 2, 2 * i] = 1.0
            self.A_ub[3 * i + 2, 2 * i + 1] = 1.0
            self.b_ub[3 * i + 2] = self.stance[i].max_force

    def _rotation_constraint(self):
        com_x, com_y = self.com_path.x(self.xi)
        self.A_eq[
            0, 0:-1] = np.array([[com_y, -com_x, 1.0]]).dot(self._basis_vectors_array)
        self.b_eq[0] = 0.0
        # return np.hstack([mult_rot, np.array([[0, 0]])]), 0.0

    def _path_constraint(self):
        ddx_dxi2 = self.com_path.ddx_dxi2(self.xi)
        dx_dxi = self.com_path.dx_dxi(self.xi)
        A_rows = np.hstack([
            ((-1 / self.m) * self._basis_vectors_array[0:2, :]), dx_dxi])
        bias=np.array([0, self.g])-ddx_dxi2.reshape((-1,))*pow(self.xi_dot, 2)
        return A_rows, bias

    def _setup_KKT(self):
        self._rotation_constraint()
        self.A_eq[1: 3, :], self.b_eq[1: 3] = self._path_constraint()
        # Goal:
        self.c = np.zeros((self.n_var, ))
        self.c[-1] = -1.0 if self.is_max else 1.0

        return self.c, self.A_eq, self.b_eq

    def _solve(self):
        global _num_solves
        _num_solves += 1
        if _num_solves % 100 == 0:
            print "solved", _num_solves, "times!"
        # print "solver ", self.indices
        self._setup_KKT()
        expect_asc = False
        if not self.active_set == None:
            try:
                phi, xi_dot_squared, xi_ddot, legit = self._solve_using_active_set()
            except np.linalg.LinAlgError:
                legit = False
            if legit:
                return phi, xi_dot_squared, xi_ddot
            else:
                print "active set change!"
                expect_asc = True

        # c, A_eq, b_eq =
        # print self.c
        res = optimize.linprog(self.c, A_ub=self.A_ub, b_ub=self.b_ub, A_eq=self.A_eq,
                               b_eq=self.b_eq, method='simplex', bounds=(None, None), callback=None, options=None)
        # print res
        if not res.success:
            print "infeasible!"
            raise InfeasibilityException(str(res))
        self._change_active_set(res)

        return res.x[0:2 * len(self.stance)], res.x[-2], res.x[-1]

    def _change_active_set(self, res):
        old_as = self.active_set
        self.saved_slack = res.slack
        self.active_set = [
            i for i, s in enumerate(res.slack) if abs(s) < 1e-7]
        self.active_set_compliment = [
            i for i, s in enumerate(res.slack) if not abs(s) < 1e-7]
        if not old_as == self.active_set:
            print "active set changed by simplex algorithm"
        else:
            print "expected active set change did not occur!"

        self.A_fixed = np.vstack(
            [self.A_eq[self.n_ceq:, :], self.A_ub[self.active_set, :]])
        self.b_fixed = np.hstack(
            [self.b_eq[self.n_ceq:], self.b_ub[self.active_set]])
        AfAfT= self.A_fixed.dot(self.A_fixed.T)
        self.x_fixed = self.A_fixed.T.dot(np.linalg.solve(AfAfT, self.b_fixed))
        self.K_A_fixed = null(self.A_fixed)
        self.l_fixed = np.linalg.solve(AfAfT,self.A_fixed.dot(-self.c))
        self.A_f_pinv = np.linalg.solve(AfAfT,self.A_fixed)
        self.P_fixed = self.A_fixed.T.dot(self.A_f_pinv)
        self.c_prime = self.A_fixed.T.dot(self.A_fixed.dot(self.c))-self.c

        # if len(self.active_set) + self.n_eq < self.n_var:
        #     A = np.vstack([self.A_ub[self.active_set, :], self.A_eq])
        #     G = null(A)

    def _solve_using_active_set(self):
        # solve for primals
        if True: #original solution
            A = np.vstack([self.A_eq, self.A_ub[self.active_set, :]])
            b = np.hstack([self.b_eq, self.b_ub[self.active_set]])
            # for i in range(0,A.shape[0]):
            #     if i>=self.n_ceq:
            #         for j in range(0, A.shape[1]):
            #             assert(A[i,j]==self.A_fixed[i-self.n_ceq,j])
            #         assert(b[i]==self.b_fixed[i-self.n_ceq])


            if A.shape[0] == A.shape[1]:
                x = np.linalg.solve(A, b)
                # print A.shape
                l = np.linalg.solve(A.T, -self.c)
            elif A.shape[0] >= A.shape[1]:
                x = np.linalg.solve(A.T.dot(A), A.T.dot(b))
                l = A.dot(np.linalg.solve(A.T.dot(A), -self.c))


        if False:
            # solve using fixed and changing components
            A_change = self.A_eq[0:self.n_ceq,:]
            b_change = self.b_eq[0:self.n_ceq]
            q = np.linalg.solve(A_change.dot(self.K_A_fixed), b_change - A_change.dot(self.x_fixed))
            # print A_change.dot(self.K_A_fixed).shape
            x_approx = self.x_fixed + self.K_A_fixed.dot(q)
            primals_approx = (
            self.A_ub[self.active_set_compliment, :].dot(x_approx)
            - self.b_ub[self.active_set_compliment])
            # legit_primal_approx = all([v < 1e-7 for v in primals_approx])
            Cs=A_change.dot(self.K_A_fixed)
            Gamma = Cs.dot(Cs.T)
            # print Gamma.shape
            l_change = np.linalg.solve(Gamma,A_change.dot(self.c_prime))
            l_fixed = self.l_fixed-self.A_f_pinv.dot(A_change.T.dot(l_change))
            l_approx = np.hstack([l_change,l_fixed])

            # print self.P_fixed
            if False: # test accuracy
                assert sum([abs(e) for e in l-l_approx])< 1e-7
                assert sum([abs(e) for e in x-x_approx])< 1e-7
                assert sum([abs(e) for e in (self.A_fixed.dot(x)-self.b_fixed)])< 1e-7
            else:
                x=x_approx
                l=l_approx

        primals = (
            self.A_ub[self.active_set_compliment, :].dot(x)
            - self.b_ub[self.active_set_compliment])
        duals = l[-len(self.active_set):]
        legit_primal = all([v < 1e-7 for v in primals])
        legit_dual = all([v > -1e-7 for v in duals])
        return x[0:2 * len(self.stance)], x[-2], x[-1], all((legit_primal, legit_dual))

    def find_phi(self, xi, xi_dot, xi_ddot):
        self._request = _Request(
            type=RequestType.feas_check, is_max=False, xi=xi, xi_dot=xi_dot, xi_ddot_desired=xi_ddot)
        phi, xi_dot_squared, xi_ddot = self._solve()
        return phi

    def is_viable(self, xi):
        return self.stance.xi_regions.contains(xi)

    def max_xi_ddot(self, xi, xi_dot):
        self._request = _Request(
            type=RequestType.opt_accel, is_max=True, xi=xi, xi_dot=xi_dot, xi_ddot_desired=None)
        try:
            phi, xi_dot_squared, xi_ddot = self._solve()
        except InfeasibilityException:
            return 10.0
        return xi_ddot

    def min_xi_ddot(self, xi, xi_dot):
        self._request = _Request(
            type=RequestType.opt_accel, is_max=False, xi=xi, xi_dot=xi_dot, xi_ddot_desired=None)
        try:
            phi, xi_dot_squared, xi_ddot = self._solve()
        except InfeasibilityException:
            return -10.0
        return xi_ddot


class MemoizedMultiActiveSetSolver(object):

    def __init__(self, stance, com_path, robot, index):
        self.index = index
        self.stance = stance
        self.robot = robot
        self.com_path = com_path
        self.dict = {RequestType.feas_check: {True: 0, False: 0}, RequestType.opt_speed: {
            True: 1, False: 2}, RequestType.opt_accel: {True: 3, False: 4}}
        self.previous_requests = [None, None, None, None, None]
        self.solvers = [ActiveSetAccelMCS(stance, com_path, robot, (index, i))
                        for i in range(0, 5)]
        self.previous_returns = [None, None, None, None, None]
        self._request = None

    @property
    def r_type(self):
        return self.dict[self._request.type][self._request.is_max]

    def _solve(self):
        if self.previous_requests[self.r_type] != self._request:
            self.solvers[self.r_type]._request = self._request
            ret = self.solvers[self.r_type]._solve()
            self.previous_returns[self.r_type] = ret
        return self.previous_returns[self.r_type]

    def is_viable(self, xi):
        return self.stance.xi_regions.contains(xi)

    def max_xi_ddot(self, xi, xi_dot):
        self._request = _Request(
            type=RequestType.opt_accel, is_max=True, xi=xi, xi_dot=xi_dot, xi_ddot_desired=None)
        try:
            phi, xi_dot_squared, xi_ddot = self._solve()
        except InfeasibilityException:
            return 10.0
        return xi_ddot

    def min_xi_ddot(self, xi, xi_dot):
        self._request = _Request(
            type=RequestType.opt_accel, is_max=False, xi=xi, xi_dot=xi_dot, xi_ddot_desired=None)
        try:
            phi, xi_dot_squared, xi_ddot = self._solve()
        except InfeasibilityException:
            return -10.0
        return xi_ddot

    def max_xi_dot(self, xi):
        self._request = _Request(
            type=RequestType.opt_speed, is_max=True, xi=xi, xi_dot=None, xi_ddot_desired=None)
        phi, xi_dot_squared, xi_ddot = self._solve()
        if xi_dot_squared < 0.0:
            return 0.0
        return sqrt(xi_dot_squared)

    def min_xi_dot(self, xi):
        self._request = _Request(
            type=RequestType.opt_speed, is_max=False, xi=xi, xi_dot=None, xi_ddot_desired=None)
        phi, xi_dot_squared, xi_ddot = self._solve()
        if xi_dot_squared < 0.0:
            return 0.0
        return sqrt(xi_dot_squared)


if __name__ == '__main__':
    import multi_contact.worlds.world8 as world

    print optimize.linprog
    import scipy
    print scipy.__version__
    t = time.time()
    solver = MemoizedMultiActiveSetSolver(
        world.contact_data[1], world.com_path, world.robot, 2)
    for x in np.linspace(0.5, 0.7, 1000):
        xi_ddot = solver.max_xi_ddot(x, 0.0 + 0.4 / 0.9 * x)
        # print xi_ddot
        assert xi_ddot > 0
        xi_ddot = solver.min_xi_ddot(x, 0.0 + 0.4 / 0.9 * x)
        # print xi_ddot
        assert xi_ddot < 0
    print time.time() - t, "seconds"
