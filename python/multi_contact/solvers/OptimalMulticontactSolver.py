import lyapunov as ly
import ly_utils as lu
import numpy as np
import cvxpy as cvx
from exceptions import NotImplementedError
from numpy import sign
from math import sqrt
from collections import namedtuple
from enum import Enum


class InfeasibilityException(Exception):

    """Raise to represent problem infeasibility."""

    def __init__(self, problem_string):
        self.problem_string = problem_string

RequestType = Enum('RequestType', 'opt_accel opt_speed feas_check')
_Request = namedtuple(
    "SolverRequest", ["type", "is_max", "xi", "xi_dot", "xi_ddot_desired"])

_num_solves = 0


class OptimalMulticontactSolver(object):

    def __init__(self, stance, com_path, robot, index):
        self.index = index
        self.stance = stance
        self.com_path = com_path
        self.robot = robot

        self._setup_basis()
        self.num_contacts = len(stance)
        self._request = _Request(RequestType.opt_accel, True, 0.2, 0.5, None)
        self._result = None

    def is_viable(self, xi):
        return self.stance.xi_regions.contains(xi)

    @property
    def xi(self):
        return self._request.xi

    @property
    def xi_dot(self):
        return self._request.xi_dot

    @property
    def is_max(self):
        return self._request.is_max

    @property
    def g(self):
        return self.robot.gravity_accel

    @property
    def m(self):
        return self.robot.mass

    def _setup_basis(self):
        """setup the convex optimization matrices and variables."""
        stance = self.stance
        num_contacts = len(stance)
        if num_contacts == 0:
            raise NotImplementedError()
        basis_vectors_list = []
        total_basis = sum([contact.num_basis for contact in stance])
        total_inequalities = sum(
            [contact.num_inequalities for contact in stance])
        # leq_mat_list = np.zeros((total_inequalities, total_basis))
        # leq_vec_list = np.zeros((total_inequalities, 1))
        for contact in stance:
            b1, b2 = contact.basis()
            # A, b = contact.inequality_constraints()
            basis_vectors_list.append(b1)
            basis_vectors_list.append(b2)
        self._basis_vectors_array = np.array(basis_vectors_list).T

    def _rotation_constraint(self, phi):
        com_x, com_y = self.com_path.x(self.xi)
        mult_rot = cvx.Constant(
            np.array([[com_y, -com_x, 1.0]]).dot(self._basis_vectors_array))
        return mult_rot * phi == cvx.Constant(0)

    def _path_constraint(self, phi, xi_ddot, xi_dot_squared):
        ddx_dxi2 = self.com_path.ddx_dxi2(self.xi)
        dx_dxi = self.com_path.dx_dxi(self.xi)

        return ((ddx_dxi2 * xi_dot_squared)
                + (dx_dxi * xi_ddot)
                + (((-1 / self.m) * self._basis_vectors_array[0:2, :]) * phi)
                == cvx.Constant(np.array([[0], [self.g]])))

    def _solve(self):
        global _num_solves
        _num_solves += 1
        if _num_solves % 100 == 0:
            print "solved", _num_solves, "times!"
        phi = cvx.Variable(2 * self.num_contacts)
        xi_ddot, xi_dot_squared = cvx.Variable(), cvx.Variable()
        constraints = [
            self._rotation_constraint(phi),
            self._path_constraint(phi, xi_ddot, xi_dot_squared),
            phi >= 0.0,
        ]
        for i in range(0, len(self.stance)):
            constraints.append(
                phi[2 * i] + phi[2 * i + 1] <= self.stance[i].max_force)
        if self._request.type == RequestType.opt_accel:
            constraints.append(xi_dot_squared == pow(self.xi_dot, 2))
            obj = xi_ddot if self.is_max else -xi_ddot
        elif self._request.type == RequestType.opt_speed:
            obj = xi_dot_squared if self.is_max else -xi_dot_squared
        elif self._request.type == RequestType.feas_check:
            constraints.append(xi_dot_squared == pow(self.xi_dot, 2))
            constraints.append(xi_ddot == self._request.xi_ddot_desired)
            max_phi = cvx.Variable()
            for i in range(0, len(self.stance)):
                constraints.append(phi[2 * i] <= max_phi)
                constraints.append(phi[2 * i + 1] <= max_phi)
            obj = - max_phi
            constraints.append(xi_dot_squared == pow(self.xi_dot, 2))
        prob = cvx.Problem(cvx.Maximize(obj), constraints)
        obj_result = prob.solve(solver="CVXOPT")

        if prob.status != 'optimal':
            if prob.status == "infeasible":
                raise InfeasibilityException(str(prob))
            print prob.status
            print prob
        return phi, xi_ddot, xi_dot_squared

    def max_xi_ddot(self, xi, xi_dot):
        self._request = _Request(
            type=RequestType.opt_accel, is_max=True, xi=xi, xi_dot=xi_dot, xi_ddot_desired=None)
        try:
            phi, xi_ddot, xi_dot_squared = self._solve()
        except InfeasibilityException:
            return 10.0
        return xi_ddot.value

    def min_xi_ddot(self, xi, xi_dot):
        self._request = _Request(
            type=RequestType.opt_accel, is_max=False, xi=xi, xi_dot=xi_dot, xi_ddot_desired=None)
        try:
            phi, xi_ddot, xi_dot_squared = self._solve()
        except InfeasibilityException:
            return -10.0
        return xi_ddot.value

    def max_xi_dot(self, xi):
        self._request = _Request(
            type=RequestType.opt_speed, is_max=True, xi=xi, xi_dot=None, xi_ddot_desired=None)
        phi, xi_ddot, xi_dot_squared = self._solve()
        if xi_dot_squared.value < 0.0:
            return 0.0
        return sqrt(xi_dot_squared.value)

    def min_xi_dot(self, xi):
        self._request = _Request(
            type=RequestType.opt_speed, is_max=False, xi=xi, xi_dot=None, xi_ddot_desired=None)
        phi, xi_ddot, xi_dot_squared = self._solve()
        if xi_dot_squared.value < 0.0:
            return 0.0
        return sqrt(xi_dot_squared.value)

    def find_phi(self, xi, xi_dot, xi_ddot):
        self._request = _Request(
            type=RequestType.feas_check, is_max=False, xi=xi, xi_dot=xi_dot, xi_ddot_desired=xi_ddot)
        phi, xi_ddot, xi_dot_squared = self._solve()
        return phi.value

class MemoizedSolver(OptimalMulticontactSolver):
    def __init__(self, stance, com_path, robot, index):
        super(MemoizedSolver, self).__init__(stance, com_path, robot, index)
        self.dict = {RequestType.feas_check:{True:0,False:0},RequestType.opt_speed:{True:1,False:2},RequestType.opt_accel:{True:3,False:4}}
        self.previous_requests=[None, None, None, None, None]
        self.previous_returns=[None, None, None, None, None]

    @property
    def r_type(self):
        return self.dict[self._request.type][self._request.is_max]
    
    def _solve(self):
        if self.previous_requests[self.r_type] != self._request:
            ret = super(MemoizedSolver, self)._solve()
            self.previous_returns[self.r_type]=ret
        return self.previous_returns[self.r_type]

if __name__ == '__main__':
    import multi_contact.worlds.world8 as world
    solver = MemoizedSolver(
        world.contact_data[0], world.com_path, world.robot, 0)
    print "gravity", world.robot.gravity_accel
    print solver.is_viable(0.4)
    print solver.min_xi_ddot(0.4, 0.2)
    phi, xi_ddot, xi_dot_squared = solver._solve()
    print phi.value
    print xi_ddot.value
    print sqrt(xi_dot_squared.value)

    print solver.min_xi_ddot(0.3, 0.4)
    print solver.min_xi_ddot(0.4, 0.7)
    print solver.min_xi_ddot(0.5, 0.2)
    print solver.min_xi_ddot(0.6, 0.2)
    print solver.find_phi(0.6, 0.2, 3.28)
    import time
    t= time.time()
    for x in np.linspace(0.3,0.6,10000):
        solver.min_xi_ddot(x, 0.4)
    print time.time()-t, "seconds"
