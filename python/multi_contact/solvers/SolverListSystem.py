import ly_utils as ly
import ly_utils as lu
import numpy as np
from exceptions import NotImplementedError
from numpy import sign
from math import sqrt
from OptimalMulticontactSolver import MemoizedSolver, OptimalMulticontactSolver, InfeasibilityException
from ActiveSetSolver import MemoizedMultiActiveSetSolver
from enum import Enum
from collections import namedtuple

SolversState = namedtuple(
    "SolversState", ["best_stances", "competitor_set",
                     "infeasible_set", "out_of_range_set"])

FollowChatterState = namedtuple(
    "FollowChatterState", ["fastest_accel_stance", "fastest_decel_stance",
                     "slowest_accel_stance", "slowest_decel_stance",
                     "widest_range_followable_stance",
                     "accel_stances", "decel_stances", "followable_stances", "competitor_set",
                     "infeasible_set", "out_of_range_set"])


@lu.named_state_system("PathTimeState", ['xi', 'xi_dot', 't'], xname='xi_t_state', tname='sigma')
class SolverListSystem(object):

    def __init__(self, world):
        self.solvers = [MemoizedMultiActiveSetSolver(
            stance, world.com_path, world.robot, i) for (i, stance) in enumerate(world.contact_data)]

        self.state = 0, (0.0, 0.0, 0.0)
        # maybe individually treat all the solvers as event throwing systems later.
        # self.events=lu.SelfReferentialParallelEvents(self.solvers,[self.path_curvature])
        self.path = world.com_path

        self.debug_cause = False

    def __repr__(self):
        return "".join(["SolverListSystem(", repr(self.state), ", forward:", repr(self.forward), ", solvers:", repr(self.solvers), ")"])

    def _min_xi_ddot(self):
        best_xi_ddot = float('inf')
        best_stances, out_of_range_set, infeasible_set, competitor_set = [
        ], [], [], []
        for i, solver in enumerate(self.solvers):
            if not solver.is_viable(self.xi):
                out_of_range_set.append(i)
                continue
            try:
                xi_ddot = solver.min_xi_ddot(self.xi, self.xi_dot)
            except InfeasibilityException:
                infeasible_set.append(i)
                continue
            if xi_ddot < best_xi_ddot:
                """new best."""
                best_stances = [i]
                best_xi_ddot = xi_ddot
            elif xi_ddot == best_xi_ddot:
                best_stances.append(i)
            competitor_set.append(i)
        sstate = SolversState(best_stances=best_stances, competitor_set=competitor_set,
                              infeasible_set=infeasible_set, out_of_range_set=out_of_range_set)
        return best_xi_ddot, sstate

    def min_xi_ddot(self):
        xi_ddot, sstate = self._min_xi_ddot()
        return xi_ddot

    def min_xi_ddot_sstate(self):
        xi_ddot, sstate = self._min_xi_ddot()
        return sstate

    def _max_xi_ddot(self):
        best_xi_ddot = -float('inf')
        best_stances, out_of_range_set, infeasible_set, competitor_set = [
        ], [], [], []
        for i, solver in enumerate(self.solvers):
            if not solver.is_viable(self.xi):
                out_of_range_set.append(i)
                continue
            try:
                xi_ddot = solver.max_xi_ddot(self.xi, self.xi_dot)
            except InfeasibilityException:
                infeasible_set.append(i)
                continue
            if xi_ddot > best_xi_ddot:
                """new best."""
                best_stances = [i]
                best_xi_ddot = xi_ddot
            elif xi_ddot == best_xi_ddot:
                best_stances.append(i)
            competitor_set.append(i)
        temp_sstate = SolversState(best_stances=best_stances, competitor_set=competitor_set,
                                   infeasible_set=infeasible_set, out_of_range_set=out_of_range_set)
        return best_xi_ddot, temp_sstate

    def max_xi_ddot_sstate(self):
        xi_ddot, sstate = self._max_xi_ddot()
        return sstate

    def max_xi_ddot(self):
        xi_ddot, sstate = self._max_xi_ddot()
        return xi_ddot

    def _min_xi_dot(self):
        best_xi_dot = float('inf')
        best_stances, out_of_range_set, infeasible_set, competitor_set = [
        ], [], [], []
        for i, solver in enumerate(self.solvers):
            if not solver.is_viable(self.xi):
                out_of_range_set.append(i)
                continue
            try:
                xi_dot = solver.min_xi_dot(self.xi)
            except InfeasibilityException:
                infeasible_set.append(i)
                continue
            if xi_dot < best_xi_dot:
                """new best."""
                best_stances = [i]
                best_xi_dot = xi_dot
            elif xi_dot == best_xi_dot:
                best_stances.append(i)
            competitor_set.append(i)
        sstate = SolversState(best_stances=best_stances, competitor_set=competitor_set,
                              infeasible_set=infeasible_set, out_of_range_set=out_of_range_set)
        return best_xi_dot, sstate

    def min_xi_dot_sstate(self):
        xi_dot, sstate = self._min_xi_dot()
        return sstate

    def min_xi_dot(self):
        xi_dot, sstate = self._min_xi_dot()
        return xi_dot

    def _max_xi_dot(self):
        best_xi_dot = -float('inf')
        best_stances, out_of_range_set, infeasible_set, competitor_set = [
        ], [], [], []
        for i, solver in enumerate(self.solvers):
            if not solver.is_viable(self.xi):
                out_of_range_set.append(i)
                continue
            try:
                xi_dot = solver.max_xi_dot(self.xi)
            except InfeasibilityException:
                infeasible_set.append(i)
                continue
            if xi_dot > best_xi_dot:
                """new best."""
                best_stances = [i]
                best_xi_dot = xi_dot
            elif xi_dot == best_xi_dot:
                best_stances.append(i)
            competitor_set.append(i)
        sstate = SolversState(best_stances=best_stances, competitor_set=competitor_set,
                              infeasible_set=infeasible_set, out_of_range_set=out_of_range_set)
        return best_xi_dot, sstate

    def max_xi_dot(self):
        xi_dot, sstate = self._max_xi_dot()
        return xi_dot

    def max_xi_dot_sstate(self):
        xi_dot, sstate = self._max_xi_dot()
        return sstate

    def get_followable_sstate(self, xi_ddot_desired):
        fastest_accel_stance, fastest_accel = None, -0
        fastest_decel_stance, fastest_decel = None, 0
        slowest_accel_stance, slowest_accel = None, None
        slowest_decel_stance, slowest_decel = None, None
        widest_range_followable_stance, widest_range = None, 0
        accel_stances = []
        decel_stances = []
        followable_stances = []
        competitor_set = []
        infeasible_set = []
        out_of_range_set = []

        for i, solver in enumerate(self.solvers):

            # in range
            if not solver.is_viable(self.xi):
                out_of_range_set.append(i)
                continue

            # feasible
            try:
                xi_ddot_max = solver.max_xi_ddot(self.xi, self.xi_dot)
                xi_ddot_min = solver.min_xi_ddot(self.xi, self.xi_dot)
            except InfeasibilityException:
                infeasible_set.append(i)
                continue

            # feasible and in-range sets are competitors
            competitor_set.append(i)

            # followability
            if xi_ddot_max>xi_ddot_desired and xi_ddot_min < xi_ddot_desired:
                followable_stances.append(i)
                accel_range = min(xi_ddot_desired-xi_ddot_min,xi_ddot_max-xi_ddot_desired)
                if accel_range>widest_range:
                    widest_range=accel_range
                    widest_range_followable_stance=i

            # fastest_accel
            if xi_ddot_max > fastest_accel:
                """new best."""
                fastest_accel_stance = i
                fastest_accel = xi_ddot_max

            # slowest accel
            if (xi_ddot_min > xi_ddot_desired):
                accel_stances.append(i)
                if slowest_accel == None or slowest_accel > xi_ddot_min - xi_ddot_desired:
                    slowest_accel = xi_ddot_min - xi_ddot_desired
                    slowest_accel_stance = i

            # fastest_decel
            if xi_ddot_min < fastest_decel:
                fastest_decel_stance = i
                fastest_accel = xi_ddot_min

            # slowest_decel
            if xi_ddot_max < xi_ddot_desired:
                decel_stances.append(i)
                if slowest_decel == None or slowest_decel < xi_ddot_max - xi_ddot_desired:
                    slowest_decel = xi_ddot_max - xi_ddot_desired
                    slowest_decel_stance = i

        res = FollowChatterState(
            fastest_accel_stance=fastest_accel_stance,
            fastest_decel_stance=fastest_decel_stance,
            slowest_accel_stance=slowest_accel_stance,
            slowest_decel_stance=slowest_decel_stance,
            widest_range_followable_stance=widest_range_followable_stance,
            accel_stances=accel_stances,
            decel_stances=decel_stances,
            followable_stances=followable_stances,
            competitor_set=competitor_set,
            infeasible_set=infeasible_set,
            out_of_range_set=out_of_range_set)
        return res

    def get_followable_set(self, xi_ddot_desired):
        res = []
        for i, solver in enumerate(self.solvers):
            if solver.is_viable(self.xi):
                max_xi_ddot = solver.max_xi_ddot(self.xi, self.xi_dot)
                min_xi_ddot = solver.min_xi_ddot(self.xi, self.xi_dot)
                if max_xi_ddot >= xi_ddot_desired and min_xi_ddot <= xi_ddot_desired:
                    res.append(i)
        return res

    def is_followable(self, xi_ddot_desired):
        res = self.get_followable_set(xi_ddot_desired)
        return res != []

    def get_chatter_sets(self, xi_ddot_desired):
        hi = []
        lo = []
        for i, solver in enumerate(self.solvers):
            if solver.is_viable(self.xi):
                if solver.max_xi_ddot(self.xi, self.xi_dot) > xi_ddot_desired:
                    hi.append(i)
                if solver.min_xi_ddot(self.xi, self.xi_dot) < xi_ddot_desired:
                    lo.append(i)
        return hi, lo

    def is_chatterable(self, xi_ddot_desired):
        hi, lo = self.get_chatter_sets(xi_ddot_desired)
        return len(hi) > 0 and len(lo) > 0


class GuardedSolverList(SolverListSystem):

    def __init__(self, world, forward=True):
        super(GuardedSolverList, self).__init__(world)
        self.setup_guards()
        self.min_xi_ddot_prior_sstate = None
        self.max_xi_ddot_prior_sstate = None
        self.min_xi_dot_prior_sstate = None
        self.max_xi_dot_prior_sstate = None
        self.follow_prior_sstate = None
        self.most_recent_changed_state_mdd = None
        self.forward = forward
        self._delta = 1e-6
        self._far_state = None
        self._remembered_state = None
        self.path_count = 0
        self.most_recent_guard_xi = None

    def throw_if(self, throw):
        if throw:
            self._far_state = self.state
        return lu.EventThrowIf[throw]

    def reset_path_counter(self, xi, forward):
        self.path_count = self.path.xi2s(xi)
        self.forward = forward

    def skip(self, dt, dx):
        t, x = self.state
        self.state = t + dt, tuple([s + sd * dt for s, sd in zip(x, dx)])

    def setup_guards(self):

        def path_curvature_call():
            return self.throw_if(self.path_count != self.path.xi2s.count(self.xi))

        def path_curvature_jump():
            self.path_count += 1.0 if self.forward else -1.0
        self.path_curvature = ly.Guard2(call=path_curvature_call, jump=path_curvature_jump, name="path_curvature")

        #

        def max_xi_ddot_call():
            return self.throw_if(self.max_xi_ddot_sstate() != self.max_xi_ddot_prior_sstate)
        def max_xi_ddot_jump():
            self.shift_delta()
            assert self.max_xi_ddot_sstate() != self.max_xi_ddot_prior_sstate
            self.max_xi_ddot_prior_sstate = self.max_xi_ddot_sstate()
            self.shift_back()
        self.max_xi_ddot_guard = ly.Guard2(call=max_xi_ddot_call, jump=max_xi_ddot_jump, name="max_xi_ddot_sstate_change")

        #

        def min_xi_ddot_call():
            return self.throw_if(self.min_xi_ddot_sstate() != self.min_xi_ddot_prior_sstate)
        def min_xi_ddot_jump():
            self.shift_delta()
            assert self.min_xi_ddot_sstate() != self.min_xi_ddot_prior_sstate
            self.min_xi_ddot_prior_sstate = self.min_xi_ddot_sstate()
            self.shift_back()
        self.min_xi_ddot_guard = ly.Guard2(call=min_xi_ddot_call, jump=min_xi_ddot_jump, name="min_xi_ddot_sstate_change")

        #

        def max_xi_dot_call():
            return self.throw_if(self.max_xi_dot_sstate() != self.max_xi_dot_prior_sstate)
        def max_xi_dot_jump():
            self.shift_delta()
            assert self.max_xi_dot_prior_sstate != self.max_xi_dot_sstate()
            self.max_xi_dot_prior_sstate = self.max_xi_dot_sstate()
            self.shift_back()
        self.max_xi_dot_guard = ly.Guard2(call=max_xi_dot_call, jump=max_xi_dot_jump, name="max_xi_dot_sstate_change")

        #

        def min_xi_dot_call():
            return self.throw_if(self.min_xi_dot_sstate() != self.min_xi_dot_prior_sstate)
        def min_xi_dot_jump():
            self.shift_delta()
            assert self.min_xi_dot_prior_sstate != self.min_xi_dot_sstate()
            self.min_xi_dot_prior_sstate = self.min_xi_dot_sstate()
            self.shift_back()
        self.min_xi_dot_guard = ly.Guard2(call=min_xi_dot_call, jump=min_xi_dot_jump, name="min_xi_dot_sstate_change")

        #

        def follow_call():
            return self.throw_if(self.get_followable_sstate(0.0) != self.follow_prior_sstate)
        def follow_jump():
            self.shift_delta()
            assert self.follow_prior_sstate != self.get_followable_sstate(0.0)
            self.follow_prior_sstate = self.get_followable_sstate(0.0)
            self.shift_back()
        self.followable_guard = ly.Guard2(call=follow_call, jump=follow_jump, name="followable_or_chatter_sstate_change")

    def shift_delta(self):
        self._remembered_state = self.state
        self.state = self._far_state

    def shift_back(self):
        self.state = self._remembered_state

    def easy_max_xi_ddot(self):
        if self.max_xi_ddot_prior_sstate == None:
            self.max_xi_ddot_prior_sstate = self.max_xi_ddot_sstate()
        index = self.max_xi_ddot_prior_sstate.best_stances[0]
        return self.solvers[index].max_xi_ddot(self.xi, self.xi_dot)

    def easy_min_xi_ddot(self):
        if self.min_xi_ddot_prior_sstate == None:
            self.min_xi_ddot_prior_sstate = self.min_xi_ddot_sstate()
        index = self.min_xi_ddot_prior_sstate.best_stances[0]
        return self.solvers[index].min_xi_ddot(self.xi, self.xi_dot)

    def easy_max_xi_dot(self):
        if self.max_xi_dot_prior_sstate == None:
            self.max_xi_dot_prior_sstate = self.max_xi_dot_sstate()
        index = self.max_xi_dot_prior_sstate.best_stances[0]
        return self.solvers[index].max_xi_dot(self.xi, self.xi_dot)

    def easy_min_xi_dot(self):
        if self.min_xi_dot_prior_sstate == None:
            self.min_xi_dot_prior_sstate = self.min_xi_dot_sstate()
        index = self.min_xi_dot_prior_sstate.best_stances[0]
        return self.solvers[index].min_xi_dot(self.xi, self.xi_dot)


if __name__ == '__main__':
    import multi_contact.worlds.demo8_world as world
    solver = GuardedSolverList(world)
    print solver
    solver.reset_path_counter(0.3, False)
    print solver
    solver.state = 0.0, (0.8, 1.0, 0.0)
    print solver.get_chatter_sets(0.8)
    print solver.is_chatterable(0.8)
    print solver.is_followable(0.8)
    print solver.max_xi_ddot()
    print solver.max_xi_ddot_guard() > 0
    solver.max_xi_ddot_guard.jump()
    print solver.max_xi_ddot_guard() > 0
    for i in range(0, 100):
        i += 1
        solver.state = 0.0, (0.8 - 0.01 * i, 1.0, 0.0)
        if solver.max_xi_ddot_guard() > 0:
            print solver.state
            solver.max_xi_ddot_guard.jump()
