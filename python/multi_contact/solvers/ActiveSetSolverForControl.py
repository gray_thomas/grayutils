import lyapunov as ly
import ly_utils as lu
import numpy as np
import cvxpy as cvx
from exceptions import NotImplementedError
from numpy import sign
from math import sqrt
from collections import namedtuple
from enum import Enum
import time
from scipy import optimize



enable_active_set=True

class InfeasibilityException(Exception):

    """Raise to represent problem infeasibility."""

    def __init__(self, problem_string):
        self.problem_string = problem_string


class ActiveSetException(Exception):

    """Raise to represent an active set change."""


_num_solves = 0


class ActiveSetControlMCS(object):

    def __init__(self, stance, robot):

        self.stance = stance
        self.robot = robot

        self._setup_basis()
        self.num_contacts = len(stance)
        self.active_set = None
        self.active_set_compliment = None

    @property
    def g(self):
        return self.robot.gravity_accel

    @property
    def m(self):
        return self.robot.mass

    def _setup_basis(self):
        """setup the convex optimization matrices and variables."""
        stance = self.stance
        num_contacts = len(stance)
        if num_contacts == 0:
            raise NotImplementedError()
        basis_vectors_list = []
        total_basis = sum([contact.num_basis for contact in stance])
        total_inequalities = sum(
            [contact.num_inequalities for contact in stance])
        # leq_mat_list = np.zeros((total_inequalities, total_basis))
        # leq_vec_list = np.zeros((total_inequalities, 1))
        for contact in stance:
            b1, b2 = contact.basis()
            # A, b = contact.inequality_constraints()
            basis_vectors_list.append(b1)
            basis_vectors_list.append(b2)
        self._basis_vectors_array = np.array(basis_vectors_list).T

        # inequalities never change!
        n = len(self.stance)
        n_ub = 3 * n
        n_var = 2 * n
        self.A_ub = np.zeros((n_ub, n_var))
        self.b_ub = np.zeros((n_ub, ))
        for i in range(0, n):
            # iEq1: phi_i,0 >=0
            self.A_ub[3 * i + 0, 2 * i] = -1.0
            # iEq2: phi_i,1 >=0
            self.A_ub[3 * i + 1, 2 * i + 1] = -1.0
            # iEq3: phi_i,0+phi_i,1 <=max_force
            self.A_ub[3 * i + 2, 2 * i] = 1.0
            self.A_ub[3 * i + 2, 2 * i + 1] = 1.0
            self.b_ub[3 * i + 2] = self.stance[i].max_force

    def _rotation_constraint(self):
        com_x, com_y = self.bold_x
        return np.array([[com_y, -com_x, 1.0]]).dot(self._basis_vectors_array), self.bold_m
        # self.A_eq[0, :], self.b_eq[0] =

    def _path_constraint(self):
        x_ddot, y_ddot = self.bold_x_ddot
        self.A_eq[1: 3, :] = self._basis_vectors_array[0:2, :]
        self.b_eq[1: 3] = self.m * np.array([x_ddot, y_ddot - self.g])
        return self._basis_vectors_array[0:2, :], self.m * np.array([x_ddot, y_ddot - self.g])
        # self.A_eq[1: 3, :], self.b_eq[1: 3] =

    def get_accel(self, phi):
        return self._basis_vectors_array[0:2, :].dot(phi) / self.m + np.array([0, self.g])

    def get_moment(self, bold_x, phi):
        com_x, com_y = self.bold_x
        return np.array([[com_y, -com_x, 1.0]]).dot(self._basis_vectors_array).dot(phi)

    def _setup_KKT(self):
        """
        Minimize:       c^T * x
        Subject to:     A_ub * x <= b_ub
                        A_eq * x == b_eq
        x' = [phi']'
        Inequality Constraints:
            phi_i,0+phi_i,1 <= max_force   i=0,...,n_c-1
            -phi_i_0 <= 0   i=0,...,n_c-1
            -phi_i_1 <= 0   i=0,...,n_c-1
        Equality Constraints:
            bold_m(x) = bold_m_des  (n=1)
            bold_x_ddot = bold_x_ddot_des (n=2)
        Goal:
            Sum(phi) (minimize internal forces)
        """
        n = len(self.stance)
        n_eq = 3
        n_var = 2 * n
        self.n_var = n_var
        self.A_eq = np.zeros((n_eq, n_var))
        self.b_eq = np.zeros((n_eq, ))

        # Goal:
        self.c = np.ones((n_var, ))

        # Eq1: _rotation
        # self.A_eq[0, :], self.b_eq[0] =
        self.A_eq[0, :], self.b_eq[0] = self._rotation_constraint()
        # Eq2: _path
        self.A_eq[1: 3, :], self.b_eq[1: 3] = self._path_constraint()

    def _solve(self):
        global _num_solves
        _num_solves += 1
        if _num_solves % 100 == 0:
            print "solved", _num_solves, "times!"

        self._setup_KKT()
        if enable_active_set:
            if not self.active_set == None:
                phi, legit = self._solve_using_active_set()
                if legit:
                    return phi
                else:
                    print "active set change!"

        # c, A_eq, b_eq =
        res = optimize.linprog(self.c, A_ub=self.A_ub, b_ub=self.b_ub, A_eq=self.A_eq,
                               b_eq=self.b_eq, bounds=(None,None), method='simplex', callback=None, options=None)
        if not res.success:
            print "infeasible!"
            raise InfeasibilityException("Infeasible: x=%s, xiddot=%s" % (
                str(self.bold_x), str(self.bold_x_ddot)))
        old_as = self.active_set
        self.active_set = [i for i, s in enumerate(
            res.slack) if abs(s) < 1e-7]
        self.active_set_compliment = [
            i for i, s in enumerate(res.slack) if not abs(s) < 1e-7]
        if not old_as == self.active_set:
            print "active set changed by simplex algorithm"

        return res.x[:]

    def _solve_using_active_set(self):
        # solve for primals
        A = np.vstack([self.A_ub[self.active_set, :], self.A_eq])
        b = np.hstack([self.b_ub[self.active_set], self.b_eq])
        x = np.linalg.solve(A, b)
        # solve for duals
        n = len(self.active_set)
        l = np.linalg.solve(A.T, -self.c)
        # check signs
        legit_primal = all([v < 1e-7 for v in self.A_ub[self.active_set_compliment,
                                                        :].dot(x) - self.b_ub[self.active_set_compliment]])
        legit_dual = all([v > -1e-7 for v in l[:n]])
        return x[:], all((legit_primal, legit_dual))

    def attempt(self, bold_x, bold_x_ddot, bold_m):
        assert (bold_x.shape == (2,))
        assert (bold_x_ddot.shape == (2,))
        assert (bold_m.shape == (1,))
        self.bold_x = bold_x
        self.bold_x_ddot = bold_x_ddot
        self.bold_m = bold_m
        phi = self._solve()
        return phi

#### TODO: this should be switched to sacrifice xi_ddot, so that chatter can be approximated.

class ActiveSetFailControlMCS(object):
    """
    Minimize:       c^T * x
    Subject to:     A_ub * x <= b_ub
                    A_eq * x == b_eq
    x' = [phi', m_err]'
    Inequality Constraints:
        phi_i,0+phi_i,1 <= max_force   i=0,...,n_c-1
        -phi_i_0 <= 0   i=0,...,n_c-1
        -phi_i_1 <= 0   i=0,...,n_c-1
        tangent.T * bold_x_ddot -error <= tangent.T * bold_x_ddot_des (n=1)
        -tangent.T * bold_x_ddot -error <= -tangent.T * bold_x_ddot_des (n=1)
    Equality Constraints:
        normal.T * bold_x_ddot == normal.T * bold_x_ddot_des (n=1)
        bold_m(x) == bold_m_des
    Goal:
        0.001*sum(phi)+error (minimize it)
    """
    def __init__(self, stance, robot, path):

        self.stance = stance
        self.robot = robot
        self.path = path

        self._setup_basis()
        self.num_contacts = len(stance)
        self.active_set = None
        self.active_set_compliment = None

    @property
    def g(self):
        return self.robot.gravity_accel

    @property
    def m(self):
        return self.robot.mass

    def _setup_basis(self):
        """setup the convex optimization matrices and variables."""
        stance = self.stance
        num_contacts = len(stance)
        if num_contacts == 0:
            raise NotImplementedError()
        basis_vectors_list = []
        total_basis = sum([contact.num_basis for contact in stance])
        total_inequalities = sum(
            [contact.num_inequalities for contact in stance])
        # leq_mat_list = np.zeros((total_inequalities, total_basis))
        # leq_vec_list = np.zeros((total_inequalities, 1))
        for contact in stance:
            b1, b2 = contact.basis()
            # A, b = contact.inequality_constraints()
            basis_vectors_list.append(b1)
            basis_vectors_list.append(b2)
        self._basis_vectors_array = np.array(basis_vectors_list).T

        # these inequalities do not change!
        self.n = len(self.stance)
        self.n_ub = 3 * self.n + 2
        self.n_eq = 2
        self.n_var = 2 * self.n + 1
        self.A_eq = np.zeros((self.n_eq, self.n_var))
        self.b_eq = np.zeros((self.n_eq, ))
        self.A_ub = np.zeros((self.n_ub, self.n_var))
        self.b_ub = np.zeros((self.n_ub, ))
        for i in range(0, self.n):
            # iEq1: phi_i,0 >=0
            self.A_ub[3 * i + 0, 2 * i] = -1.0
            # iEq2: phi_i,1 >=0
            self.A_ub[3 * i + 1, 2 * i + 1] = -1.0
            # iEq3: phi_i,0+phi_i,1 <=max_force
            self.A_ub[3 * i + 2, 2 * i] = 1.0
            self.A_ub[3 * i + 2, 2 * i + 1] = 1.0
            self.b_ub[3 * i + 2] = self.stance[i].max_force

        # Goal also never changes!
        self.c=0.1*np.ones((self.n_var)) # share load
        self.c[-1]=100.0 # minimize error

    def get_accel(self, phi):
        return self._basis_vectors_array[0:2, :].dot(phi) / self.m + np.array([0, self.g])

    def get_moment(self, bold_x, phi):
        com_x, com_y=self.bold_x
        return np.array([[com_y, -com_x, 1.0]]).dot(self._basis_vectors_array).dot(phi)

    def _setup_KKT(self):
        # These change, based on position and desired accel.

        com_x, com_y = self.bold_x

        #slow:
        xi = self.path.find_xi(com_x)
        dx_dxi = self.path.dx_dxi(xi)
        assert dx_dxi.shape==(2,1)
        dx_dxi_perp = np.array([[dx_dxi[1,0]],[-dx_dxi[0,0]]])
        assert abs(dx_dxi.T.dot(dx_dxi_perp)[0,0])<1e-7

        moment = self.bold_m[0]
        x_ddot, y_ddot = self.bold_x_ddot

        """ 3*self.n+0:2         Moment """
        """ 3*self.n+2:6         Accel """
        z = 3 * self.n  # index of first equality-as-inequality equation
        p = 2 * self.n  # index of first non-phi variable

        A_xi_ddot = (1/self.m) * dx_dxi.T.dot(self._basis_vectors_array[0:2, :])
        b_xi_ddot = dx_dxi.T.dot(np.array([x_ddot, y_ddot - self.g]))[0]

        # Index z+0 -- moment
        self.A_ub[z + 0:z + 1, 0:p] = A_xi_ddot
        self.A_ub[z + 0, p] = -1
        self.b_ub[z + 0] = b_xi_ddot

        # Index z+1 -- moment (negative)
        self.A_ub[z + 1:z + 2, 0:p] = -A_xi_ddot
        self.A_ub[z + 1, p] = -1
        self.b_ub[z + 1] = -b_xi_ddot

        # Index 0 -- dx_dxi_perp accel equality
        self.A_eq[0:1, 0:p] = (1/self.m) * dx_dxi_perp.T.dot(self._basis_vectors_array[0:2, :])
        self.A_eq[0, p] = 0.0
        self.b_eq[0:1] = dx_dxi_perp.T.dot(np.array([x_ddot, y_ddot - self.g]))

        # Index 1 --- moment
        A_phi = np.array([[com_y, -com_x, 1.0]]).dot(self._basis_vectors_array)
        self.A_eq[1:2, 0:p] = A_phi
        self.A_eq[1, p] = 0.0
        self.b_eq[1] = moment


    def _solve(self):
        global _num_solves
        _num_solves += 1
        if _num_solves % 100 == 0:
            print "solved", _num_solves, "times!"

        self._setup_KKT()

        if enable_active_set:
            if not self.active_set == None:
                x, legit=self._solve_using_active_set()
                if legit:
                    return x[:2 * self.n]
                else:
                    # print x.shape, self.c.shape
                    self.value = self.c.dot(x)
                    # print "ActiveSetFailControlMCS: not legit! Value", self.value, "bold_x", str(self.bold_x)

        # c, A_eq, b_eq =
        res=optimize.linprog(self.c, A_ub = self.A_ub, b_ub = self.b_ub, A_eq = self.A_eq,
                               b_eq = self.b_eq, bounds=(None,None), method = 'simplex', callback = None, options = None)
        if not res.success:
            print "infeasible!"
            raise InfeasibilityException("Infeasible: x=%s, xiddot=%s" % (
                str(self.bold_x), str(self.bold_x_ddot)))
        old_as=self.active_set
        self.active_set=[i for i, s in enumerate(
            res.slack) if abs(s) < 1e-7]
        self.active_set_compliment=[
            i for i, s in enumerate(res.slack) if not abs(s) < 1e-7]
        if not old_as == self.active_set:
            print "active set changed by simplex algorithm, was", old_as, "now", self.active_set
        else:
            # print "re-usable active set unusued:", self.active_set
            pass

        self.value = self.c.dot(res.x)

        return res.x[:2 * self.n]

    def _solve_using_active_set(self):
        # solve for primals
        A = np.vstack([self.A_ub[self.active_set, :], self.A_eq])
        b = np.hstack([self.b_ub[self.active_set], self.b_eq])

        # solve for duals
        n = len(self.active_set)
        if A.shape[0]==A.shape[1]:
            x = np.linalg.solve(A, b)
            l = np.linalg.solve(A.T, -self.c)
        elif A.shape[0]>=A.shape[1]:
            x = np.linalg.solve(A.T.dot(A), A.T.dot(b))
            l = A.dot(np.linalg.solve(A.T.dot(A), -self.c))
        else:
            raise Exception()
        # check signs
        legit_primal = all([v < 1e-7 for v in self.A_ub[self.active_set_compliment,
                                                        :].dot(x) - self.b_ub[self.active_set_compliment]])
        legit_dual = all([v > -1e-7 for v in l[:n]])
        return x, all((legit_primal, legit_dual))

    def attempt(self, bold_x, bold_x_ddot, bold_m):
        assert (bold_x.shape == (2,))
        assert (bold_x_ddot.shape == (2,))
        assert (bold_m.shape == (1,))
        self.bold_x = bold_x
        self.bold_x_ddot = bold_x_ddot
        self.bold_m = bold_m
        phi = self._solve()
        return phi



class MemoizedSolver(ActiveSetControlMCS):

    def __init__(self, stance, robot):
        super(MemoizedSolver, self).__init__(stance, robot)
        self.previous_args = (None, None, None, None, None)
        self.previous_return = None

    @property
    def r_type(self):
        return self.dict[self._request.type][self._request.is_max]

    def _solve(self):
        args = (self.bold_x[0], self.bold_x[1], self.bold_x_ddot[0], self.bold_x_ddot[1], self.bold_m)
        if not all([ self.previous_args[i]==args[i] for i in range(0, len(args))]):
            ret = super(MemoizedSolver, self)._solve()
            self.previous_return = ret
            self.previous_args = args
        return self.previous_return

class MemoizedFailureSolver(ActiveSetFailControlMCS):

    def __init__(self, stance, robot, path):
        super(MemoizedFailureSolver, self).__init__(stance, robot, path)
        self.previous_args = (None, None, None, None, None)
        self.previous_return = None

    @property
    def r_type(self):
        return self.dict[self._request.type][self._request.is_max]

    def _solve(self):
        args = (self.bold_x[0], self.bold_x[1], self.bold_x_ddot[0], self.bold_x_ddot[1], self.bold_m)
        if not all([ self.previous_args[i]==args[i] for i in range(0, len(args))]):
            ret = super(MemoizedFailureSolver, self)._solve()
            self.previous_return = ret
            self.previous_args = args
        return self.previous_return


class ComboSolver(MemoizedFailureSolver):
    def __init__(self, stance, robot, path):
        super(ComboSolver, self).__init__(stance,robot, path)
        self.nice_solver = MemoizedSolver(stance,robot)
        self.nice_works = True
    def _solve(self, i=0):
        if self.nice_works:
            try:
                # print "solving with nice solver"
                return self.nice_solver.attempt(self.bold_x, self.bold_x_ddot, self.bold_m)
            except InfeasibilityException:
                self.nice_works=False
                return self._solve(i=i+1)
        else:
            try:
                # print "solving with backup"
                phi = super(ComboSolver, self)._solve()
                if abs(self.value)<1e-7 and i<2:
                    self.nice_works=True
                    return self._solve(i=i+1)
                return phi
            except np.linalg.LinAlgError:
                self.nice_works=True
                return self._solve(i=i+1)

if __name__ == '__main__':
    import multi_contact.worlds.world8 as world
    solver = MemoizedSolver(
        world.contact_data[0], world.robot)

    print optimize.linprog
    import scipy
    print scipy.__version__
    t = time.time()
    if True:
        solver1 = ComboSolver(
            world.contact_data[0], world.robot, world.com_path)
        for x in np.linspace(0.3, 0.9, 10000):
            solver1.attempt(np.array([x,0]),np.array([0,0]),np.array([0.0]))
    else:
        solver2 = MemoizedFailureSolver(
            world.contact_data[0], world.robot)
        for x in np.linspace(0.3, 0.7, 10000):
            solver2.attempt(np.array([x,0]),np.array([0,0]),np.array([0.0]))
    print time.time() - t, "seconds"
