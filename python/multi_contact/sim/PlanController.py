"""
SegmentPlanController module.

Chooses contact forces. Uses existing, predefined switching times.
"""

import numpy as np
import cvxpy as cvx
from math import sin, cos, atan, sqrt
from ContactRecord import ContactRecord
from multi_contact.solvers.OptimalMulticontactSolver import OptimalMulticontactSolver, InfeasibilityException, _Request, RequestType
from multi_contact.solvers.SolverListSystem import SolverListSystem
from multi_contact.solvers.ActiveSetSolverForControl import ComboSolver
from multi_contact.planning.MaxSpeedPlanner import CommandEnum
import ly_utils as lu
from collections import namedtuple
from bisect import bisect

@lu.named_state_system("RobotState", ['x', 'y', 'x_dot', 'y_dot'], xname="x_state", tname='t')
class PlanController(object):

    def __init__(self, world, plan):
        """Use a predefined switching time switch sequence for now."""
        self.solvers = SolverListSystem(world)
        self.plan = plan
        # self.goal = plan.command(0.0)
        self.world = world
        self.com_path = world.com_path
        x, y = self.com_path.x(0.0)
        self.state = 0.0, (x, y, 0.0, 0.0)
        # self.stance_index = 0
        self.init()

    def init(self):
        if self.goal == CommandEnum.max:
            self.solvers.max_xi_ddot_prior_sstate = self.solvers.max_xi_ddot_sstate()
            # self.stance_index = self.solvers.max_xi_ddot_prior_sstate.best_stances[0]
        elif self.goal == CommandEnum.min:
            self.solvers.min_xi_ddot_prior_sstate = self.solvers.min_xi_ddot_sstate()
            # self.stance_index = self.solvers.min_xi_ddot_prior_sstate.best_stances[0]
        elif self.goal == CommandEnum.zero:
            self.solvers.min_xi_ddot_prior_sstate = self.solvers.min_xi_ddot_sstate()
            self.solvers.max_xi_ddot_prior_sstate = self.solvers.max_xi_ddot_sstate()

            # self.stance_index = self.solvers.get_followable_set(0.0)[0]
        else:
            raise NotImplementedError()

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        t, (x, y, xd, yd) = value
        self._state = value
        self.xi = self.com_path.find_xi(self.x)
        dx_dxi = self.com_path.dx_dxi(self.xi)
        self.xi_dot = dx_dxi[0] * xd + dx_dxi[1] * yd
        self.solvers.state = t, (self.xi, self.xi_dot, t)

    @property
    def stance_index(self):
        return self.plan.leaf(self.xi).stance_index

    @property
    def stance(self):
        return self.world.contact_data[self.stance_index]

    @property
    def goal(self):
        return self.plan.command(self.xi)

    def fall(self):
        for contact in self.stance:
            contact.set_force(0, 0)
        print "Falling!"
        return self.stance

    def xi_ddot(self, xdd, ydd):
        """arc length acceleration. only a component of true accel."""

        dx_dxi = self.com_path.dx_dxi(self.xi)
        return dx_dxi[0] * xdd + dx_dxi[1] * ydd

    def calculate_stance(self):
        """the robot controller."""

        try:
            self.com_path.find_s(self.x)
        except LookupError:
            print "Out of path!"
            return self.fall()

        if len(self.stance) == 0:
            print "Flight phases. So bold!"
            return self.stance
        self.init()
        print "mcf xi=",self.xi, "xi_dot = ",self.xi_dot
        try:
            if self.goal == CommandEnum.max:
                solver = self.solvers.solvers[self.stance_index]
                solver._request = _Request(
                    type=RequestType.opt_accel,
                    is_max=True, xi=self.xi,
                    xi_dot=self.xi_dot, xi_ddot_desired=None)
                phi, xi_ddot, xi_dot_squared = solver._solve()
                for i, contact in enumerate(self.stance):
                    p1 = (i * 2)
                    p2 = (2 + i * 2)
                    basis = solver._basis_vectors_array[0:2, p1:p2]
                    force = basis.dot(phi.value[p1:p2, 0])
                    contact.set_force(force[0,0], force[1,0])

            elif self.goal == CommandEnum.min:
                solver = self.solvers.solvers[self.stance_index]
                solver._request = _Request(
                    type=RequestType.opt_accel,
                    is_max=False, xi=self.xi,
                    xi_dot=self.xi_dot, xi_ddot_desired=None)
                phi, xi_ddot, xi_dot_squared = solver._solve()
                for i, contact in enumerate(self.stance):
                    p1 = (i * 2)
                    p2 = (2 + i * 2)
                    basis = solver._basis_vectors_array[0:2, p1:p2]
                    force = basis.dot(phi.value[p1:p2, 0])
                    contact.set_force(force[0,0], force[1,0])
            elif self.goal == CommandEnum.zero:
                solver = self.solvers.solvers[self.stance_index]
                phi = solver.find_phi(self.xi, self.xi_dot, 0.0)
                for i, contact in enumerate(self.stance):
                    p1 = (i * 2)
                    p2 = (2 + i * 2)
                    basis = solver._basis_vectors_array[0:2, p1:p2]
                    force = basis.dot(phi[p1:p2, 0])
                    contact.set_force(force[0,0], force[1,0])
            else:
                raise NotImplementedError()

            def sanity_check_forces(point_force_pairs, com_location):
                moment = 0
                for point, force in point_force_pairs:
                    relative_point = point - com_location
                    moment += relative_point[0, 0] * force[1, 0]
                    moment -= relative_point[1, 0] * force[0, 0]
                # if not abs(moment) <= 1e-4:
                #     print "wrong moment", moment
                # assert abs(moment) <= 2e-3
            records = [ContactRecord(contact)
                       for contact in self.stance]
            point_force_pairs = [
                (record.foot, record.wrench[0:2, :]) for record in records]
            sanity_check_forces(point_force_pairs,
                                np.array([self.x, self.y]).T)
        except ValueError as e:
            print dir(e)
            print e.message
            raise e
            print "value error"
            return self.fall()
        except cvx.error.SolverError:
            print "solver error"
            return self.fall()
        except InfeasibilityException:
            print "infeasibility!"
            return self.fall()
        return self.stance


@lu.named_state_system("RobotState", ['x', 'y', 'x_dot', 'y_dot'], xname="x_state", tname='t')
class NewPlanController(object):

    def __init__(self, world, plan):
        """Use a predefined switching time switch sequence for now."""
        self.solver = ComboSolver(world.contact_data[plan[0].stance_index], world.robot, world.com_path)
        self.plan = plan
        self.eta=0.2
        self.l=0.1
        self.decel_chat=True

        self.world = world
        self.com_path = world.com_path
        x, y = self.com_path.x(0.0)
        self.state = 0.0, (x, y, 0.0, 0.0)

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        t, (x, y, xd, yd) = value
        self._state = value
        self.xi = self.com_path.find_xi(self.x)
        dx_dxi = self.com_path.dx_dxi(self.xi)
        self.xi_dot = dx_dxi[0] * xd + dx_dxi[1] * yd

    @property
    def leaf(self):
        t = self.t
        if self.t>self.plan.time(self.plan.xi_max):
            t = self.plan.time(self.plan.xi_max)
        return self.plan.leaf_time(t)

    @property
    def speed_difference(self):
        return sqrt(self.x_dot**2+self.y_dot**2)-self.leaf.xi_dot_by_xi(self.leaf.xi_by_t(self.t))

    @property
    def stance_index(self):
        if type(self.leaf.stance_index)!=int:
            print self.speed_difference
            if self.decel_chat and self.speed_difference<-1e-3:
                self.decel_chat = False
                print "Chatter accelerting!"
            if not self.decel_chat and self.speed_difference>0.0:
                self.decel_chat = True
                print "Chatter decelerating!"
            return self.leaf.stance_index[1 if self.decel_chat else 0]
        return self.leaf.stance_index

    @property
    def stance(self):
        # print self.plan.leaf(self.xi).dstate
        if self.plan.leaf(self.xi).stance_index != None:
            return self.world.contact_data[self.stance_index]
        else:
            leaf = self.plan.leaf(self.xi)
            # print dir(leaf.dstate)
            # print leaf.dstate
            raise NotImplementedError()

    def fall(self):
        for contact in self.stance:
            contact.set_force(0, 0)
        print "Falling!"
        return self.stance

    def xi_ddot(self, xdd, ydd):
        """arc length acceleration. only a component of true accel."""

        dx_dxi = self.com_path.dx_dxi(self.xi)
        return dx_dxi[0] * xdd + dx_dxi[1] * ydd

    def calculate_stance(self):
        """the robot controller."""
        # print "calculating, t=", self.t
        t = self.t
        if self.t>self.plan.time(self.plan.xi_max):
            t = self.plan.time(self.plan.xi_max)

        if self.world.contact_data[self.stance_index] != self.solver.stance:
            print "Switching stance to %d. Leaf stance is %s"%(self.stance_index, str(self.leaf.stance_index))
            self.solver = ComboSolver(self.world.contact_data[self.stance_index], self.world.robot, self.world.com_path)

        assert (self.world.contact_data[self.stance_index]==self.solver.stance)
        leaf = self.leaf
        xi_nom = leaf.xi_trajectory(t)
        xi_dot_nom = leaf.xi_dot_by_xi(xi_nom)
        xi_ddot_nom = leaf.xi_ddot_by_xi(xi_nom)
        if self.t>self.plan.time(self.plan.xi_max):
            xi_ddot_nom *= 0.0
            xi_dot_nom *= 0.0

        # linearize about nominal path, even if we fall behind.
        x_nom = self.com_path.x(xi_nom).reshape((-1,))
        dx_dxi = self.com_path.dx_dxi(xi_nom).reshape((-1,))
        ddx_dxi2 = self.com_path.ddx_dxi2(xi_nom).reshape((-1,))
        x_dot_nom = dx_dxi*xi_dot_nom
        x_true = np.array([self.x, self.y])
        dev = x_true-x_nom
        dev_dot = np.array([self.x_dot, self.y_dot])-x_dot_nom
        x_ddot_nom = ddx_dxi2*xi_dot_nom**2+dx_dxi*xi_ddot_nom
        control = - self.eta * (self.l*dev_dot+dev)
        x_ddot_cont = x_ddot_nom + control

        phi = self.solver.attempt(x_true, x_ddot_cont, np.array([0]))
        assert (phi!= None)

        # the physics should produce the same answer as below
        # print phi.shape
        x_ddot_expected = self.solver.get_accel(phi)
        mom_expected = self.solver.get_moment(x_true, phi)
        # print x_ddot_expected
        # print mom_expected
        # if not (np.linalg.norm(x_ddot_expected-x_ddot_cont)<1e-7 and abs(mom_expected)<1e-7):
        #     print "xdd error with norm", np.linalg.norm(x_ddot_expected-x_ddot_cont)
        #     print "moment error with norm", abs(mom_expected)

        # save forces to contacts
        for i, contact in enumerate(self.stance):
            p1 = (i * 2)
            p2 = (2 + i * 2)
            basis = self.solver._basis_vectors_array[0:2, p1:p2]
            force = basis.dot(phi[p1:p2])
            contact.set_force(force[0], force[1])

        # maybe fall this if dev becomes large?
        # try:
        #     s = self.com_path.find_s(self.x)
        # except LookupError:
        #     print "Out of path!"
        #     return self.fall()

        def sanity_check_forces(point_force_pairs, com_location):
            moment = 0
            com_location=com_location.reshape((2,-1))
            for point, force in point_force_pairs:
                relative_point = point - com_location
                moment += relative_point[0, 0] * force[1, 0]
                moment -= relative_point[1, 0] * force[0, 0]
            return abs(moment)

        records = [ContactRecord(contact)
                   for contact in self.stance]
        point_force_pairs = [
            (record.foot, record.wrench[0:2, :]) for record in records]
        moment_err = sanity_check_forces(point_force_pairs,
                            np.array([self.x, self.y]).T)
        if moment_err>0.1:
            print "hey, large moment error!", moment_err

        return self.stance

if __name__ == '__main__':
    from MultiContactNumericSimulator import MultiContactNumericSimulator
    from multi_contact.planning.MaxSpeedPlanner import MaxSpeedPlanner
    import numpy as np
    import matplotlib.pyplot as plt
    import cPickle as pkl
    import time
    import multi_contact.worlds.world9B as world

    
    """run the demo."""

    if True:  # re-plan!
        print "Planning!"
        plan = MaxSpeedPlanner(world).integrate()
        with open("plan.pkl", 'w') as fil:
            plan.delete_functions()
            pkl.dump(plan, fil)
            plan.build_functions()
        print plan
        exit()
    else:
        with open("plan.pkl", 'r') as fil:
            plan = pkl.load(fil)
            plan.build_functions()


    controller = NewPlanController(world, plan)
    print "simulating"
    t = time.time()
    sim = MultiContactNumericSimulator(controller,
                                       time=3.0, x0=world.x0)
    sim_res = sim.simulate()
    print sim_res
    print time.time() - t, "seconds"
