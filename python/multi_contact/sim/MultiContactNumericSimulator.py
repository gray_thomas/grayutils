import numpy as np
import lyapunov as ly
import ly_utils as lu
from numpy import sin, cos, pi, array
from math import sqrt, pow, atan2, pi, floor
from MultiContactRobot import MultiContactRobot
from ContactRecord import ContactRecord
from gu.gutypes import mutabledata

from mpl_toolkits.mplot3d import Axes3D
import random


class SimulationResult:

    def __init__(self, sim):
        self.list_of_contact_record_lists = sim.list_of_contact_record_lists
        self.data = sim.data
        self.list_of_robot_accel = sim.list_of_robot_accel
        self.phase_space_data = np.array(sim.phase_space_data)
        self.times = sim.times
        self.stance_indicies = sim.stance_indicies
        self.i = 0

    @classmethod
    def fall(cls):
        sim = mutabledata("FakeSim", [
                          "list_of_contact_record_lists", "data", "list_of_robot_accel", "phase_space_data", 'times'])()
        sim.list_of_contact_record_lists = [[]]*3
        sim.data = np.array([[0.0, 0.0, 0.0, 0.0]]*3)
        sim.list_of_robot_accel = [[0.0, -9.8]]*3
        sim.phase_space_data = [[0.0, 0.0, -9.8]]*3
        sim.times = [0.0]*3
        return cls(sim)

    @property
    def time(self):
        """return the active simulation time."""
        return self.times[self.i]

    @property
    def phase_space(self):
        return self.phase_space_data[self.i]

    @property
    def contact_records(self):
        return self.list_of_contact_record_lists[self.i]

    @property
    def state(self):
        return self.data[:, self.i]

    @property
    def accel(self):
        return self.list_of_robot_accel[self.i]

    def increment_time(self):
        self.i += 1
        if self.i >= len(self.times):
            # self.update_parameters()
            # self.simulate()
            self.i = 0
        return self.i


class MultiContactNumericSimulator:

    def __init__(self, controller, x0=(0.25, 1.0, 0, 0), time=1.5, dt=0.01):
        self.controller = controller
        self.i = 0
        self.initial_state = x0
        self.initial_time = 0.0  # seconds
        self.final_time = time  # seconds
        self.time_increment = dt
        self.list_of_contact_record_lists = []
        self.list_of_robot_accel = []
        self.phase_space_data = []
        self.stance_indicies = []

    def store_list_of_contact_record_lists(self):
        """add the current contact to the list of record lists."""
        stance = self.controller.stance
        contact_records_list = []
        for contact in stance:
            contact_records_list.append(ContactRecord(contact))
        self.list_of_contact_record_lists.append(contact_records_list)

    def simulate(self):
        """run the simulation from start to finish."""
        self.times = np.arange(
            self.initial_time, self.final_time, self.time_increment)

        system = MultiContactRobot(
            self.controller, x0=self.initial_state)

        switch_times = []
        logger = ly.Recorder(system)
        stepper = ly.runge_kutta4(system, self.times)
        stepper.time_tolerance = 1e-2
        stepper.rel_tolerance = 1e-1
        # try:
        for time, guards in stepper:
            logger.log(guards)
            if guards:
                for guard in guards:
                    guard.map()
                switch_times.append(time)
            else:
                self.store_list_of_contact_record_lists()
                xd, yd, xdd, ydd = system()
                self.list_of_robot_accel.append([xdd, ydd])
                self.phase_space_data.append(
                    [self.controller.xi, self.controller.xi_dot, self.controller.xi_ddot(xdd, ydd)])
                self.stance_indicies.append(self.controller.stance_index)
            try:
                ly.check_NaN(system)
            except ArithmeticError:
                print "ArithmeticError"
                break
        # except TypeError:
        #     pass

        self.phase_space_data = np.array(self.phase_space_data)
        self.data = np.array(logger.x)
        self.data_row_labels = ["x", "y", "xd", "yd"]
        self.i = len(self.times) - 1
        return SimulationResult(self)
    # return
