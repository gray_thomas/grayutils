import numpy as np
import lyapunov as ly
import ly_utils as lu
from numpy import sin, cos, pi, array
from math import sqrt, pow, atan2, pi, floor


@lu.named_state_system("COM_State", ['x', 'y', 'xd', 'yd'], xname="X")
class MultiContactRobot(object):

    def __init__(self, controller, x0=(0.25, 1.0, 0.0, 0.0)):
        self._state = 0.0, x0
        self.gravity_accel = -9.81  # acceleration due to gravity, in m/s^2
        self.mass = 20.0  # Kg
        self.controller = controller
        # self.reset()

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        self._state = value
        self.controller.state = value

    @property
    def events(self):
        return self.controller.events

    def __call__(self):
        """
        State time derivative is returned based on the discrete
        state of the system. Does simple physics.
        """
        stance = self.controller.calculate_stance()
        # stance = self.controller.stance
        x, y, xd, yd = self.X
        W = np.array([0.0, self.mass * self.gravity_accel, 0.0])
        for contact in stance:
            W += np.array([contact.F_x, contact.F_y,
                           contact.F_x * (y - contact.y_f) - contact.F_y * (x - contact.x_f)])
            # print "contact force ", contact.F_x, contact.F_y
        xdd = (W[0] / self.mass)
        ydd = (W[1] / self.mass)
        for contact in stance:
            contact.error = abs(W[2])

        return (xd, yd, xdd, ydd)
