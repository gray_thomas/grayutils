import numpy as np


class ContactRecord(object):

    """Holds the raw historic data for a contact."""

    def __init__(self, contact):
        """Save historic contact data."""
        self.foot = np.array([[contact.x_f, contact.y_f]]).T
        self.wrench = np.array([[contact.F_x, contact.F_y, contact.M]]).T
        self.error = contact.error
        self.appendage = contact.appendage
