import numpy as np 
maxint=9
base_voltage=15
for r1 in range(1,maxint):
	for r2 in range(1,maxint):
		for r3 in range(1,maxint):
			for r4 in range(1,maxint):
				for r5 in range(1,maxint):
					for r6 in range(1,maxint):
						s1=r5+r2+r4
						s2=r1+r3+r6
						c1=(r4*r4)%(s1)==0
						c2=(r3*r3)%(s2)==0
						c3=(base_voltage*r3)%(s2)==0
						s3=r3+r4-r3*r3/s2-r4*r4/s1
						c4=(base_voltage*r3/s2)%s3==0
						i2=(base_voltage*r3/s2)/s3
						c5=(r4*i2)%s1==0
						i1=(r4*i2)/s1
						c6=(base_voltage+r3*i2)%s2==0
						i3=(base_voltage+r3*i2)/s2

						if c1 and c2 and c3 and c4 and c5 and c6:
							print "resistances",r1,r2,r3,r4,r5,r6, "sums",s1, s2, "mesh", i1, i2, i3