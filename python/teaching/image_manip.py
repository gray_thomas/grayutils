import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.animation as ani 
import scipy.ndimage as ndimage
import scipy.misc as misc
from glob import glob

im = np.zeros((10,10))
im[6,8]=1
im[2:4,2:4]=np.ones((2,2))
im[2:4,2:3]=0


blurry_im = ndimage.gaussian_filter(im,2)

plt.figure(figsize=(10,5))
plt.subplot(121)
plt.imshow(im, cmap=plt.cm.hot, interpolation='none')
plt.legend()
plt.axis('off')
plt.colorbar()
plt.subplot(122)
plt.imshow(blurry_im, cmap=plt.cm.hot, interpolation='none')
plt.axis('off')
plt.colorbar()
plt.show()