import Tkinter as tk
import Tkconstants as tkc
root = tk.Tk()
frame = tk.Frame(root, relief=tkc.RIDGE, borderwidth=2)
frame.pack(fill=tkc.BOTH,expand=1)
label = tk.Label(frame, text="Hello, World")
label.pack(fill=tkc.X, expand=1)
button = tk.Button(frame,text="Exit",command=root.destroy)
button.pack(side=tkc.BOTTOM)
root.mainloop()