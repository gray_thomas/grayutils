import Tkinter as tk
import Tkconstants as tkc


class My_App:
	def __init__(self):
		self.root=tk.Tk()
		self.initialize_widgets()

	def hide_right_frame(self):
		print "hiding right frame!"
		self.right_frame.pack_forget()

	def show_right_frame(self):
		print "showing right frame!"
		self.right_frame.pack(side=tkc.LEFT)
		
	def initialize_widgets(self):
		recessed_frame=tk.Frame(master=self.root,
			relief=tkc.SUNKEN, borderwidth='1')

		self.generate_left_frame(recessed_frame).pack(side=tkc.LEFT)
		self.generate_right_frame(recessed_frame).pack(side=tkc.LEFT)

		# packing order doesn't matter since it's the only thing in root
		recessed_frame.pack() 

	def generate_left_frame(self,parent):

		left_frame=tk.Frame(master=parent, 
			relief=tkc.RAISED, borderwidth='1')

		tk.Button(master=left_frame, command=self.hide_right_frame,
			text="Hide").pack(side='bottom')

		tk.Button(master=left_frame, command=self.show_right_frame,
			font="courier 8 italic",
			text="Show").pack(side='bottom')

		return left_frame

	def generate_right_frame(self,parent):

		self.right_frame=tk.Frame(master=parent, 
			relief=tkc.RAISED, borderwidth='1')

		tk.Label(master=self.right_frame, text="hello world right 1!").pack()
		tk.Label(master=self.right_frame, text="hello world right 2!").pack()

		return self.right_frame

	def start_application(self):
		self.root.mainloop()

if __name__=='__main__':
	app=My_App()
	app.start_application()