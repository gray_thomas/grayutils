#---------Imports
from numpy import arange, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import Tkinter as tk
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
#---------End of imports

class Application(tk.Frame):

    def say_hi(self):
        print "hi there, everyone!"

    def createWidgets(self):    
        self.add_button_panel(self).pack({"side":"left"})
        self.add_animation_canvas(self).pack({"side":"left"})
        # self.fig.show()

    def add_button_panel(self,parent):
        button_pannel=tk.Frame(master=self)
        self.add_quit_button(button_pannel).pack({"side": "bottom"})
        self.add_quit_button(button_pannel).pack({"side": "bottom"})
        self.add_hello_button(button_pannel).pack({"side": "bottom"})
        return button_pannel

    def add_quit_button(self,parent):
        quit_button = tk.Button(parent)
        quit_button["text"] = "QUIT"
        quit_button["fg"]   = "red"
        quit_button["command"] =  self.quit
        return quit_button

    def add_hello_button(self,parent):
        hi_there = tk.Button(parent)
        hi_there["text"] = "Hello",
        hi_there["command"] = self.say_hi
        return hi_there

    # def run_new_simulation(self, parameters):
    #     self.data=

    def add_animation_canvas(self,parent):
        self.fig = plt.Figure()
        self.x = np.arange(0, 2*np.pi, 0.01)        # x-array
        self.label = tk.Label(self,text="SHM Simulation")
        self.canvas = FigureCanvasTkAgg(self.fig, master=self)
        self.ax = self.fig.add_subplot(111)
        self.line, = self.ax.plot(self.x, np.sin(self.x))
        self.ani = animation.FuncAnimation(self.fig, self.animate, np.arange(1, 200), interval=25, blit=False)
        return self.canvas.get_tk_widget()

    def animate(self,i):
        t=i/10.0
        self.line.set_ydata(np.sin(self.x+t))  # update the data
        return self.line

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.pack()
        self.createWidgets()

# root = tk.Tk()

def main():
    root = tk.Tk()
    app = Application(master=root)
    app.mainloop()
    root.destroy()
    

if __name__ == "__main__":
    main()

