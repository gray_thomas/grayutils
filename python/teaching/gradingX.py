import numpy as np
import matplotlib.pyplot as plt 
# grades=[70, 60, 45, 45, 55, 100, 75, 45, 98, 100, 70, 55, 55, 55, 100, 100, 94, 87, 45, 55, 65, 88, 55, 45, 83, 45, 91, 91, 90, 100, 55, 83, 55]
# grades = [100,100,100,100,100,    100,100,100,100,100,    100,100,100,100,100,    100,100,100,100,100,    100,100,100,100,96,     75,75,75,68,60,    60,55,55,55,50,    50]    
# s1, s2 = 95.0, 95.0
# grades = [100,100,100,100,100,    100,100,100,100,100,    100,100,100,96,96,     96,96,96,96,96,    95,95,90,90,87,    85,85,85,85,82,    82,82,82,80,80,   79]  
# s1, s2 = 93.0, 93.0
# grades = [100,100,100,100,98,   98,98,98,98,96,   96,95,95,94,93,   92,92,92,90,90,   90,91,91,89,88,   87,85,84,84,84,   84,82,81,80,80,   78,76, 42]  
# grades = [100]*29+[92]*8+[88]*1+[86]*7+ [83]*5+ [78]*15
# grades = [100]*14 + [95]*15 + [89]*7 + [86]*14 + [82]*13 + [78]*10

# grades = [100]*27 + [95]*7 + [93]*13 + [92]*6+ [91.6]*9+ [91.2]*4+[90]*3+[89]*4 +[70]# final
# s1, s2 = 95.0, 98.0

# grades = [100]*18+[95]*8+[80]*11+[0]*2# Q7 A 
# s1, s2 = 90.0, 91.0# Q7 A

# grades = [100]*5+[95]*7+[93]*10+[88]*5+[0]*3# Q7 B 
# s1, s2 = 90.0, 91.0# Q7 B

s1, s2 = 87.0/2, 87.0/2
grades = (
	[100]*12 # Perfect
	+[90]*2 # algebra
	+[92]*8 # sign
	+[92]*14 # not plotting t<0
	+[86]*8 # close
	+[84]*2 # close to close
	+[82]*11 # okish
	+[80]*17 # bad
	+[78]*8) # Terribad
grades = [0.5 *g for g in grades]
print "num students", len(grades)
grades.sort()
plt.plot(grades)


def draw_new_grades_plot(goal_average,midpoint,c='k'):
	grades_above=[g for g in grades if g>midpoint]
	grades_below=[g for g in grades if g<=midpoint]
	def unscale(ll, low, high):
		ol=low
		oh=high
		return [(l-ol)/(oh-ol) for l in ll]
	def scale(ll, low, high):
		nl=low
		nh=high
		return [nl+(l)*(nh-nl) for l in ll]
	unscaled_highs=unscale(grades_above,midpoint,100.0)
	unscaled_lows=unscale(grades_below,0.0,midpoint)
	inverted_unscaled_lows=scale(unscaled_lows,1.0,0.0)
	high_weight=sum(unscaled_highs)
	print "high weight", sum(unscaled_highs)
	low_weight=sum(inverted_unscaled_lows)
	print "low weight", sum(inverted_unscaled_lows)
	new_highs=scale(unscaled_highs,goal_average,100.0)

	highwidth=100.0-goal_average
	print "highwidth", highwidth
	lowwidth=(highwidth/low_weight)*high_weight
	print "lowwidth", lowwidth
	new_lows=scale(unscaled_lows,goal_average-lowwidth,goal_average)

	new_grades=[]
	new_grades.extend(new_lows)
	new_grades.extend(new_highs)
	new_grades.sort()

	grade_dict={}
	for i in range(0,len(grades)):
		grade_dict[grades[i]]=new_grades[i]
	keyss=grade_dict.keys()
	keyss.sort()
	print [(key,grade_dict[key]) for key in keyss]

	print "old average", sum(grades)*1.0/len(grades)
	print "new average", sum(new_grades)*1.0/len(new_grades)
	plt.plot([midpoint for n in grades],c)
	plt.plot([goal_average for n in grades],c)
	plt.plot(new_grades,c)


draw_new_grades_plot(s1,s2,'c')
plt.show()