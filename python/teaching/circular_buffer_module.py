#circular_buffer.py
BUF = []
HEAD = 0
TAIL = 0
IsEMPTY = True
SIZE=4
print "I'm a buffer module and I'm being imported!"

def Init():
	global HEAD, TAIL
	HEAD = 0
	TAIL = 0 
	for ii in range(SIZE):
		BUF.append(-1)

def ReInit():
	BUF[:] = []
	IsEMPTY = True
	Init()

def Add(datum):
	global HEAD, TAIL, IsEMPTY
	BUF[HEAD] = datum
	if HEAD == SIZE - 1 :
		HEAD = 0
		TAIL = 0 # unnecessary
	else:
		if HEAD == TAIL and not IsEMPTY:
			HEAD = HEAD + 1
			TAIL = HEAD
		else:
			HEAD = HEAD + 1

	IsEMPTY = False

def GetIndex(tail_index):
	return BUF[(tail_index+TAIL)%SIZE]

def GetNewest():
	return BUF[HEAD-1]

def GetOldest():
	return BUF[TAIL]

def PrintMe():
	print "buf: [%d,%d,%s]--[%+4.2f, %+4.2f, %+4.2f, %+4.2f] " % (
		HEAD, TAIL, str(IsEMPTY), BUF[0], BUF[1], BUF[2], BUF[3])
