#circular_buffer.py
ds_BUF = []
ds_HEAD = 0
ds_TAIL = 0
ds_IsEMPTY = True
ds_SIZE=4

def ds_Init():
	global ds_HEAD, ds_TAIL
	ds_HEAD = 0b
	ds_TAIL = 0 
	for ii in range(ds_SIZE):
		ds_BUF.append(-1)

def ds_ReInit():
	ds_BUF[:] = []
	ds_IsEMPTY = True
	ds_Init()

def ds_Add(datum):
	global ds_HEAD, ds_TAIL, ds_IsEMPTY
	ds_BUF[ds_HEAD] = datum
	if ds_HEAD == ds_SIZE - 1 :
		ds_HEAD = 0
		#ds_TAIL = 0 # unnecessary
	else:
		if ds_HEAD == ds_TAIL and not ds_IsEMPTY:
			ds_HEAD = ds_HEAD + 1
			ds_TAIL = ds_HEAD
		else:
			ds_HEAD = ds_HEAD + 1

	ds_IsEMPTY = False

def ds_GetIndex(tail_index):
	return ds_BUF[(tail_index+ds_TAIL)%ds_SIZE]

def ds_GetNewest():
	return ds_BUF[ds_HEAD-1]

def ds_GetOldest():
	return ds_BUF[ds_TAIL]

def ds_PrintMe():
	print "buf: [%d,%d,%s]--[%+4.2f, %+4.2f, %+4.2f, %+4.2f] " % (
		ds_HEAD, ds_TAIL, str(ds_IsEMPTY), ds_BUF[0], ds_BUF[1], ds_BUF[2], ds_BUF[3])

def main():
	ds_Init()
	ds_PrintMe()
	for i in range (0,100):
		ds_Add(i)
		# cb.print_me()
		ds_PrintMe()
		print "the result of cb.index(0) : "+str(ds_GetIndex(0)) + " the head is "+str(ds_GetNewest())

if __name__=="__main__":
	main()
