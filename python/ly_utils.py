from lyapunov import *
from collections import namedtuple
from math import isnan

EventThrowIf = {True: 1.0, False: -1.0}
EventGEQ0 = lambda x: x if not x == 0.0 else 1e-300
EventLEQ0 = lambda x: -x if not x == 0.0 else -1e-300
ThrowEventIf = EventThrowIf

class UnexpectedDoubleThrow(ValueError):

    """Throw to step_across(state)."""

    def __init__(self, string):
        super(UnexpectedDoubleThrow,self).__init__(string)

def hybrid_integrate(stepper):
    """hybrid_integrate(stepper) yields tuples of (jump, time, state, and either guard or NaN)."""
    jump = 0
    for e in stepper.system.events:
        if e() > 0:
            raise ValueError("Mistakenly Positive Guard %s at the start!"%e.name)
    try:
        for time, guards in stepper:
            if len(guards)>1:
                s="Multiple guards! %s"% ", ".join([g.name for g in guards])
                raise UnexpectedDoubleThrow(s)
            if any([isnan(s) for s in stepper.system.state[1]]):
                raise StopIntegration()
            if len(guards) == 0:
                yield jump, time, stepper.system.state[1], None

            # if len(guards) > 1:
            #     print("WARNING: simultaneous guards: {%s}"
            #           % ", ".join([g.name for g in guards]))

            for guard in guards:
                yield jump, time, stepper.system.state[1], guard
                try:
                    if hasattr(guard, "far_side"):
                        if guard.far_side:
                            stepper.step_across()
                    guard.jump()
                except Jump:
                    stepper.step_across()
                except StateJump:
                    stepper.reset()
                jump += 1
                for e in stepper.system.events:
                    if e() > 0:
                        raise UnexpectedDoubleThrow("Guard %s is throwing immediately after jump %s!"%(e.name,guard.name))
                yield jump, time, stepper.system.state[1], guard
    except StopIntegration:
        raise StopIteration()


def initialize_by_guards(system):
    for guard in system.events:
        if (guard() > 0):
            try:
                guard.jump()
            except (Jump, StateJump, StopIntegration):
                pass


class Jump(Exception):

    """Throw to step_across(state)."""

    def __init__(self):
        pass


class StateJump(Exception):

    """Throw to step_across(state)."""

    def __init__(self):
        pass


class StopIntegration(Exception):

    """The "StopIteration" for hybrid integration."""

    def __init__(self):
        pass


def SharedSuperObjectSubclass(super_class):
    def modify_class(cls):
        original_constructor = getattr(cls, "__init__")

        def new_constructor(self, super_instance, *args, **kwargs):
            self._pseudo_super = super_instance
            original_constructor(self, *args, **kwargs)
        setattr(cls, "__init__", new_constructor)

        def get_get_wrapper(name):
            def get_wrap(self):
                return getattr(self._pseudo_super, name)
            return get_wrap

        def get_set_wrapper(name):
            def set_wrap(self, value):
                return setattr(self._pseudo_super, name, value)
            return set_wrap

        for name in dir(super_class):
            if not hasattr(cls, name):
                def wrapped_get(self):
                    return getattr(self._pseudo_super, name)
                setattr(
                    cls, name, property(get_get_wrapper(name), get_set_wrapper(name)))
        return cls
    return modify_class


def named_state_system(state_name, state_names, xname='X', tname='t'):
    """
    This class descriptor leaves the state as a true tuple, but
    binds read-only alias properties to more convenient names
    for these internal variables.
    """
    def name_these_states(system):
        system.StateTuple = namedtuple(state_name, state_names)

        def Xget(obj):
            """ gets the state and converts it to a named tuple """
            return system.StateTuple._make(obj.state[1])
        setattr(system, xname, property(Xget))

        def tget(obj):
            """ gets the time """
            return obj.state[0]
        setattr(system, tname, property(tget))

        def make_fget(i):
            """ use this factory to avoid latebinding issues """
            def fget(obj):
                return obj.state[1][i]
            return fget

        for i, name in enumerate(state_names):
            setattr(system, name, property(make_fget(i)))

        return system
    return name_these_states


class HybridAutomata(object):

    def __init__(self, init_enum, initial_system):
        self.modes = {init_enum: initial_system}
        initial_system.switch_state = self.switch_state
        self.automata_state = init_enum
        self.active.init()

    def add_system(self, enum, system):
        self.modes[enum] = system
        system.switch_state = self.switch_state

    @property
    def __state_doc__(self):
        return "".join([node.__doc__ for node in self.modes.values()])

    @property
    def state(self):
        return self.active.state

    @state.setter
    def state(self, value):
        self.active.state = value

    @property
    def active(self):
        return self.modes[self.automata_state]

    def __call__(self):
        return self.active()

    @property
    def events(self):
        return self.active.events

    def switch_state(self, next_enum):
        self.modes[next_enum].state = self.active.state
        self.automata_state = next_enum
        self.active.init()


class Guard2(object):

    def __init__(self, call, jump, far_side=False, name=""):
        self.call = call
        self.name = name
        self.jump = jump
        self.far_side = far_side

    def __call__(self, **kwargs):
        return self.call(**kwargs)

    def __repr__(self):
        return "Guard, name=%s" % self.name


def intersect_dt(S, dSdX, t_limits=[0,1e2]):
    Xd = np.array(self())
    den = dSdX.dot(Xd)
    if den == 0.0:
        return 0.0
    dt = -S / den
    if dt < t_limits[0]:
        dt = t_limits[0]
    if dt > t_limits[1]:
        dt = t_limits[1]
    return dt

class Guard(object):

    def __init__(self, fcall=None, fjump=None, obj=None):
        self.fcall = fcall
        self.__doc__ = fcall.__doc__
        self.name = fcall.__name__
        self.fjump = fjump
        self.obj = obj

    def __get__(self, obj, objtype=None):
        self.obj = obj
        return self

    def __call__(self, **kwargs):
        if self.fcall == None:
            raise AttributeError("guard with no call")
        return self.fcall(self.obj, **kwargs)

    def jumper(self, fjump):
        return type(self)(fcall=self.fcall, fjump=fjump, obj=self.obj)

    def jump(self, **kwargs):
        if self.obj is None:
            raise AttributeError("no object!")
        if self.fjump == None:
            raise AttributeError(
                "jumpless guard. Define this guard's jumper with @guard.jumper above a def guard(self):")
        self.fjump(self.obj, **kwargs)
