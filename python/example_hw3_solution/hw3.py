# -*- coding: utf-8 -*-
"""
Created on Sun Sep 28 17:39:34 2014

@author: joser
@author: gray_thomas
"""


import Tkinter as tk

import numpy as np
import matplotlib
matplotlib.use('TkAgg')

from numpy import arange, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure


class Application(tk.Frame):

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.createWidgets()
        self.pack()
        
        #Parameters for the simulation
        self.g = -9.81 
        self.links = 2
        self.ic = np.zeros((self.links,0))
        

    def say_hi(self):
        print "hi there, everyone!"

    def show_values(self):
        print (self.w1.get(), self.w2.get())


    def createWidgets(self):

        #Create sliders       
        self.w1 = tk.Scale(self, from_=0.00, to=1.00, resolution=0.01)
        self.w1.grid(column=3, row = 1)
        self.w1.set(0.5)
        self.w2 = tk.Scale(self, from_=0.00, to=2.00, orient=tk.HORIZONTAL, resolution=0.01)
        self.w2.grid(column=5, row = 1)
        self.w2.set(1.0)
        
        #Create buttons                
        self.quit = tk.Button(self, text="Quit", command=self.quit).grid(column=0,row=0, pady=10)
        self.hi_there = tk.Button(self, text="Hello", command=self.say_hi).grid(column=0,row=1, pady=10)
        self.plot = tk.Button(self, text="Plot", command=self.plot).grid(column=0,row=2, pady=10)
        self.show = tk.Button(self, text='Show Sliders', command=self.show_values).grid(column=6, row=1)        

        #Create entry         
        self.entrythingy = tk.Entry()
        self.entrythingy.pack()

        #Create string variable
        self.contents = tk.StringVar()
        # set it to some value
        self.contents.set("this is a variable")
        
        # tell the entry widget to watch this variable
        self.entrythingy["textvariable"] = self.contents
        # and here we get a callback when the user hits return.
        # we will have the program print out the value of the
        # application variable when the user hits return
        self.entrythingy.bind('<Key-Return>', self.print_contents)


    def plot(self):
        fig = Figure(figsize=(5,4), dpi=100)
        amplitude = self.w1.get()
        freq = self.w2.get()
        a = fig.add_subplot(111)
        t = arange(0.0,3.0,0.01)
        s = amplitude*sin(2*pi*t*freq)
        a.plot(t,s)
        # a tk.DrawingArea
        canvas = FigureCanvasTkAgg(fig, master=self)
        canvas.show()
        canvas.get_tk_widget().grid(column=0, row= 15, columnspan=12)
        
    def print_contents(self, event):
        print "The contents of entry is now ---->", self.contents.get()


    #set number of links        
    def set_number_links(self):
        raise NotImplementedError
    
    #set the initial conditions
    def set_ic(self):
        raise NotImplementedError
    
    #Set the gravity
    def set_gravity(self):
        raise NotImplementedError
        
    #We can use the same function to change all the parameters (dictionary, id=>variable)
    def change_parameter(self):
        raise NotImplementedError

    #Re-run simulation
    def rerun(self):
        raise NotImplementedError
    
    # Function to run the forward dynamics simulation
    # if arguments are None the function will use the ic and gravity from the class
    def run_simulation(self, ic=None, gravity=None):
        raise NotImplementedError


root = tk.Tk()

app = Application(master=root)
app.master.title("Homework 3")
app.mainloop()

root.destroy()

