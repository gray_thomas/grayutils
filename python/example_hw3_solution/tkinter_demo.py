from Tkinter import *

class Application(Frame):
    def say_hi(self):
        print "hi there, everyone!"

    def newFunc(self):
        print "new function"

    def createWidgets(self):
        self.QUIT = Button(self)
        self.QUIT["text"] = "QUIT"
        self.QUIT["fg"]   = "red"
        self.QUIT["command"] =  self.quit

        self.QUIT.pack()

        self.hi_there = Button(self)
        self.hi_there["text"] = "Hello",
        self.hi_there["command"] = self.say_hi


        self.extra = Button(self)
        self.extra["text"] = "New Button"
        self.extra["command"] = self.newFunc
        self.extra.pack()
        self.hi_there.pack()


    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.createWidgets()

root = Tk()
app = Application(master=root)
app.mainloop()
root.destroy()