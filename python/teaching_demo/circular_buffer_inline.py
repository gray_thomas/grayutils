
ds_BUF = []
ds_HEAD = 0
ds_TAIL = 0
ds_IsEMPTY = False
ds_SIZE = 4

def ds_Init():
	global ds_HEAD, ds_TAIL, ds_IsEMPTY
	ds_HEAD=0
	ds_TAIL=0
	ds_IsEMPTY=True
	for ii in range(0,ds_SIZE):
		ds_BUF.append(-1)

def ds_PrintMe():
	print "buf: [%d,%d,%s]--[%d,%d,%d,%d]" % (ds_TAIL,ds_HEAD,str(ds_IsEMPTY), 
		ds_BUF[0],ds_BUF[1],ds_BUF[2],ds_BUF[3])
def ds_Add(datum):
	global ds_HEAD, ds_TAIL, ds_IsEMPTY
	ds_BUF[ds_HEAD]=datum
	if ds_HEAD == ds_SIZE-1:
		ds_HEAD = 0
		ds_TAIL = 0
	else:
		if ds_HEAD == ds_TAIL and not ds_IsEMPTY:
			ds_HEAD = ds_HEAD+1
			ds_TAIL = ds_HEAD
		else:
			ds_HEAD = ds_HEAD+1
	ds_IsEMPTY=False

def ds_GetIndex(index):
	return ds_BUF[(index+ds_TAIL)%ds_SIZE]
def ds_GetNewest():
	return ds_BUF[ds_HEAD-1]
def ds_GetOldest():
	a = ds_BUF
	return ds_BUF[ds_TAIL]

def main():
	ds_Init()
	ds_PrintMe()
	for ii in range(0,5):
		ds_Add(ii)
		ds_PrintMe()
		print ds_GetOldest()
	print "hello world"

if __name__ == '__main__':
	main()