function plot_many_pz(model,tests,name)
% Expect: tests is a cell of structures with perterbation, model, mag,
% phase, and roots. Roots is a structure with a, b, and c. nom is a nominal
% model.
utColors

%% Predictions

% pred100=pe(model,Data,100);
% pred10=pe(model,Data,10);

fig=figure();
hold on

resid
% original roots


for i=1:length(tests)
    plot(real(tests{i}.roots.b),imag(tests{i}.roots.b),...
        'O','MarkerSize',12,'LineWidth',3,'Color',utblue50)
    plot(real(tests{i}.roots.c),imag(tests{i}.roots.c),...
        'O','MarkerSize',12,'LineWidth',3,'Color',utyellow75)
    plot(real(tests{i}.roots.a),imag(tests{i}.roots.a),...
        'X','MarkerSize',12,'Color',utburntorange*0.75+[0.25,0.25,0.25],'LineWidth',3)
end

lar=real(roots(model.a));
lai=imag(roots(model.a));
lbr=real(roots(model.b));
lbi=imag(roots(model.b));
lcr=real(roots(model.c));
lci=imag(roots(model.c));
bplt=plot(lbr,lbi,'O','MarkerSize',12,'LineWidth',3,'Color',utblue75);
cplt=plot(lcr,lci,'O','MarkerSize',12,'LineWidth',3,'Color',utyellow);
aplt=plot(lar,lai,'X','MarkerSize',12,'Color',utburntorange,'LineWidth',3);

theta=linspace(0,2*pi,1000);
xs=cos(theta);
ys=sin(theta);
legend([bplt,cplt,aplt],{'B(z)=0', 'C(z)=0','A(z)=0'},'interpreter', 'latex');
plot(xs,ys,'-','LineWidth',1,'Color',utblack)
plot(xs,ys,':','LineWidth',3,'Color',utblack)
axis([-1.5,1.5,-1.5,1.5])
title(nameModelLatex(model),'Interpreter','latex')
figuresize(16,12,'centimeters')
fname=[ name 'multi_pz_plot'];
print(['../../tex/time_series_final_project/' fname],'-dpdf','-r300');
print(['../../tex/time_series_final_project/' fname],'-dpng','-r300');
% close(fig.Number)
fprintf(['{ %% multi PZ Plot for %s\n',...
	'\t\\usebackgroundtemplate{}\n',...
    '\t\\setbeamertemplate{navigation symbols}{}\n',...
    '\t\\begin{frame}[plain]\n',...
    '\t\t    \\begin{tikzpicture}[remember picture,overlay]\n',...
    '\t\t\t        \\node[at=(current page.center)] {\n',...
    '\t\t\t\t            \\includegraphics[width=\\paperwidth]{%s}\n',...
    '\t\t\t        };\n',...
    '\t\t    \\end{tikzpicture}\n',...
    '\t \\end{frame}\n',...
    '}\n'],nameModel(model),fname)

end