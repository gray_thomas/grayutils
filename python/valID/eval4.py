import getData as gd
import numpy as np 
import matplotlib.pyplot as plt 


ank=gd.AnkleData("data4.dat")
def forces_plot(lo=0, hi=5000):
    fig, axes = plt.subplots(3, sharex=True)
    axes[0].plot(1e-6*ank.t[lo:hi], ank.cur6[lo:hi])
    axes[0].set_ylabel("current (A)")
    axes[1].plot(1e-6*ank.t[lo:hi], ank.f6[lo:hi])
    axes[1].set_ylabel("force (N)")
    axes[2].plot(1e-6*ank.t[lo:hi], ank.x6[lo:hi])
    axes[2].set_xlabel("time (s)")
    axes[2].set_ylabel("pos (m)")

def big_plot(lo=0, hi=5000):
    fig, axes = plt.subplots(7, sharex=True)
    axes[0].plot(1e-6*ank.t[lo:hi], ank.cur6[lo:hi])
    axes[0].set_ylabel("current (A)")
    axes[1].plot(1e-6*ank.t[lo:hi], ank.f6[lo:hi])
    axes[1].set_ylabel("force (N)")
    axes[2].plot(1e-6*ank.t[lo:hi], ank.x6[lo:hi])
    axes[2].set_ylabel("pos (m)")
    axes[3].plot(1e-6*ank.t[lo:hi], ank.xd6[lo:hi])
    axes[3].set_ylabel("vel (m/s)")
    axes[4].plot(1e-6*ank.t[lo:hi], ank.TS1[lo:hi])
    axes[4].set_ylabel("TS1 (?)")
    axes[5].plot(1e-6*ank.t[lo:hi], ank.TS2[lo:hi])
    axes[5].set_ylabel("TS2 (?)")
    axes[6].plot(1e-6*ank.t[lo:hi], ank.TS3[lo:hi])
    axes[6].set_ylabel("TS3 (?)")
    axes[6].set_xlabel("time (s)")

def force_torque_plot(lo=0, hi=5000):
    cal_mat=np.array([
        [-171.138762092584, 164.262330318272, 1136.66933015971, -25528.3827006466, -892.147266185941, 25018.097320641],
        [-2393.11261214503, 29302.8374381793, 176.909372134555, -14538.0957864741, 1032.08177266801, -14648.9548591062 ],
        [41139.1938396664, 518.608163331987, 40227.7537078209, 385.007116653545, 41350.2621211616, 447.58447722459 ],
        [-21.8401291003204, 421.344052448737, -872.343264955442, -221.911677460517, 886.648930652221, -196.26924129588 ],
        [996.949904915404, 14.3104375478289, -505.787839958358, 358.430597717003, -495.101373345106, -364.002555342701 ],
        [33.9397030997519, -469.070679591882, 21.1841848707929, -445.411314827181, 16.1145946572983, -437.463633877703 ]])
    gauge_gains=np.array([75, 75, 75, 73, 75, 73])
    gauge_offsets=np.array([32858, 31310, 32988, 29358, 33420, 30796])

def spring_plot(lo=0, hi=5000):
    fig, axes = plt.subplots(1, sharex=True)
    axes.plot(ank.x6[lo:hi],ank.f6[lo:hi])
    axes.set_xlabel("position (m)")
    axes.set_ylabel("force sensor force(N)")
    axes.grid(True)



forces_plot(0,500)
big_plot(0,5000)
spring_plot(0,5000)
plt.show()