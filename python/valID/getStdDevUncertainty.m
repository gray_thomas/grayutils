function std_dev=getStdDevUncertainty(Model,Data,N)
sigma_a=getSigmaA(Model,Data);
green=getGreensFunction(Model,Data,N);
gsquared=green.^2;
temp=0;
variance=zeros(length(gsquared),1);
for i=1:length(gsquared)
    temp=temp+gsquared(i);
    variance(i)=temp;
end
variance=variance.*sigma_a^2;
std_dev=sqrt(variance);