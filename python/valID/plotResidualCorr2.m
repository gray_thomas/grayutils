function plotResidualCorr2(Data,model,name,title2,P)
utColors

%% Predictions

% pred100=pe(model,Data,100);
% pred10=pe(model,Data,10);
fig=figure();
y=pe(model,Data);
n=length(y.y);
yss=normal_ts(y.y);
ac=zeros(25,1);
aclags=1:25;
xclags=-25:25;
xc=zeros(51,1);
uss=normal_ts(Data.u);
cmpac=zeros(25,1);
cmpxc=zeros(51,1);
for i=aclags
    ac(i)=yss(i:end)'*yss(1:end+1-i)/(n+1-i);
    cmpac(i)=sqrt(chi2inv(P,1)*(n-i)/n/(n+2));
end
% for j=xclags
%     if j<=0
%         i=j+26;
%         xc(j+26)=yss(i:end)'*uss(1:end+1-i);        
%         cmpxc(j+26)=sqrt(chi2inv(0.95,1)*(n-i)/n/(n+2));
%     else
%         i=j;
%         xc(j+26)=uss(i:end)'*yss(1:end+1-i);
%         cmpxc(j+26)=sqrt(chi2inv(0.95,1)*(n-i)/n/(n+2));
%     end
% end

stem(aclags,ac,'filled','Color', utblack,'Marker','O','MarkerSize',9,'MarkerFaceColor',utyellow);
hold on
plot(cmpac)
% % resid(model,Data);
% gcf().Children(1).Children(1).set('Color', utblack,'Marker','O','MarkerSize',9,'MarkerFaceColor',utyellow)
% gcf().Children(2).Children(1).set('Color', utblack,'Marker','O','MarkerSize',9,'MarkerFaceColor',utyellow)
% gcf().Children(2).Children(2).set('FaceColor',utburntorange)
% fig=gcf();
% autocorrs=fig.Children(2).Children(1).YData();
% gcf().Children(2).set('YLim',2*[min(autocorrs(2:end)) max(autocorrs(2:end))])
% gcf().Children(1).Children(2).set('FaceColor',utburntorange)
% 
% % hold on
% % plot(pred1.y-Data.y,'Color',utblue,'linewidth',3)
% % plot(pred10.y-Data.y,'Color',utburntorange,'linewidth',3)
% % plot(pred100.y-Data.y,'Color',utyellow,'linewidth',3)
% % legend({'$X_t(1)-X_t$', '$X_t(10)-X_t$','$X_t(100)-X_t$'},'interpreter', 'latex')
% t=suptitle(title2);
% t.set('Interpreter','latex')
% figuresize(16,12,'centimeters')
% fname=[ name 'pred_resid'];
% print(['../../tex/time_series_final_project/' fname],'-dpdf','-r300');
% % close(fig.Number)
% fprintf(['{ %% Residual Correlations for %s\n',...
% 	'\t\\usebackgroundtemplate{}\n',...
%     '\t\\setbeamertemplate{navigation symbols}{}\n',...
%     '\t\\begin{frame}[plain]\n',...
%     '\t\t    \\begin{tikzpicture}[remember picture,overlay]\n',...
%     '\t\t\t        \\node[at=(current page.center)] {\n',...
%     '\t\t\t\t            \\includegraphics[width=\\paperwidth]{%s}\n',...
%     '\t\t\t        };\n',...
%     '\t\t    \\end{tikzpicture}\n',...
%     '\t \\end{frame}\n',...
%     '}\n'],nameModel(model),fname)

end