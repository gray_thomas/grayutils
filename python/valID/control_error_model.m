%% Setup the data
A=dataStruct('s1_pre-processed_data1');
B=dataStruct('s1_pre-processed_data2');
C=dataStruct('s1_pre-processed_data3');
D=dataStruct('s1_pre-processed_data4');

model1=PostulateARMAX(B.Data10,.999,[1,2]);
model2=PostulateARMAX(B.Data100,.999,[1,2]);
model3=PostulateARMAX(B.Data1,.999,[1,2]);
model4=PostulateARMAX(B.Data1,.95,[1,2,3]);
model5=PostulateARMAX(B.MISO1,.999,[1;1]);



%% B10 Model
plotResidualCorr(B.Data10,model1,'B10',[nameModelLatex(model1) ' roll'])
plotPredictions(A.Data10,model1,'B10_Aval',[nameModelLatex(model1) ' LF validation, roll'])
plotPredictions(C.Data10,model1,'B10_Cval',[nameModelLatex(model1) ' validation 1, roll'])
plotPredictions(D.Data10,model1,'B10_Dval',[nameModelLatex(model1) ' validation 2, roll'])

plotResiduals(B.Data10,model1,'B10_B',[nameModelLatex(model1) ' residuals of roll'])
plotResiduals(A.Data10,model1,'B10_Aval',[nameModelLatex(model1) ' LF validation, roll residuals'])
plotResiduals(C.Data10,model1,'B10_Cval',[nameModelLatex(model1) ' validation 1, roll residuals'])
plotResiduals(D.Data10,model1,'B10_Dval',[nameModelLatex(model1) ' validation 2, roll residuals'])
plotPZUTA(model1,'B10')
plotForecastConf(B.Data10,model1,'M1',[250;300],[35;135])

%% B100 Model
plotResidualCorr(B.Data100,model2,'B100',[nameModelLatex(model2) ' roll'])
plotPredictions(A.Data100,model2,'B100_Aval',[nameModelLatex(model2) ' LF validation, roll'])
plotPredictions(C.Data100,model2,'B100_Cval',[nameModelLatex(model2) ' validation 1, roll'])
plotPredictions(D.Data100,model2,'B100_Dval',[nameModelLatex(model2) ' validation 2, roll'])
plotResiduals(B.Data100,model2,'B100_B',[nameModelLatex(model2) ' residuals of roll'])
plotResiduals(A.Data100,model2,'B100_Aval',[nameModelLatex(model2) ' LF validation, roll residuals'])
plotPZUTA(model2,'B100')
plotForecastConf(B.Data100,model2,'M2',[25;30],[35;135])


%% B1 Model
plotResidualCorr(B.Data1,model3,'B1',[nameModelLatex(model3) ' roll'])
plotPredictions(B.Data1,model3,'B1_B',[nameModelLatex(model3) ' predictions of roll'])
plotPredictions(A.Data1,model3,'B1_Aval',[nameModelLatex(model3) ' LF validation, of roll'])
% plotPredictions(C.Data1,model3,'B1_Cval',[nameModelLatex(model3) ' validation 1, roll'])
% plotPredictions(D.Data1,model3,'B1_Dval',[nameModelLatex(model3) ' validation 2, roll'])
plotResiduals(A.Data1,model3,'B1_Aval',[nameModelLatex(model3) ' LF validation, roll residuals'])
plotResiduals(B.Data1,model3,'B1_Bval',[nameModelLatex(model3) ' roll residuals'])
plotPZUTA(model3,'B1')
plotForecastConf(B.Data1,model3,'M3',[2500;3000],[500;1350])

%% Fancy stuff
plotPredictions(B.MISO1,model5,'M5_B',[nameModelLatex(model5) ' predictions of roll'])
plotPredictions(B.Data100,model2,'B10_B',[nameModelLatex(model2) ' predictions of roll'])
plotResiduals(B.MISO1,model5,'M5_B',[nameModelLatex(model5) ' predictions of roll'])


%% Try simulation based training
opt=armaxOptions('Focus','simulation');
model6=PostulateARMAXO(B.Data100,.999,[1,2],opt);
figure()
pzplot(model6)
model7=PostulateARMAXO(A.Data100,.999,[1,2],opt);
figure()
pzplot(model7)
plotPredictions(A.Data100,model7,'A100_Aval',[nameModelLatex(model7) ' LF validation, of roll'])

