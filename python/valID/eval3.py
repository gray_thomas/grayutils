import getData as gd
import numpy as np 
import matplotlib.pyplot as plt 


ank=gd.AnkleData("data3.dat")
def forces_plot(lo=0, hi=5000):
	fig, axes = plt.subplots(3, sharex=True)
	axes[0].plot(1e-6*ank.t[lo:hi], ank.cur6[lo:hi])
	axes[0].set_ylabel("current (A)")
	axes[1].plot(1e-6*ank.t[lo:hi], ank.f6[lo:hi])
	axes[1].set_ylabel("force (N)")
	axes[2].plot(1e-6*ank.t[lo:hi], ank.x6[lo:hi])
	axes[2].set_xlabel("time (s)")
	axes[2].set_ylabel("pos (m)")

def big_plot(lo=0, hi=5000):
	fig, axes = plt.subplots(7, sharex=True)
	axes[0].plot(1e-6*ank.t[lo:hi], ank.cur6[lo:hi])
	axes[0].set_ylabel("current (A)")
	axes[1].plot(1e-6*ank.t[lo:hi], ank.f6[lo:hi])
	axes[1].set_ylabel("force (N)")
	axes[2].plot(1e-6*ank.t[lo:hi], ank.x6[lo:hi])
	axes[2].set_ylabel("pos (m)")
	axes[3].plot(1e-6*ank.t[lo:hi], ank.xd6[lo:hi])
	axes[3].set_ylabel("vel (m/s)")
	axes[4].plot(1e-6*ank.t[lo:hi], ank.TS1[lo:hi])
	axes[4].set_ylabel("TS1 (?)")
	axes[5].plot(1e-6*ank.t[lo:hi], ank.TS2[lo:hi])
	axes[5].set_ylabel("TS2 (?)")
	axes[6].plot(1e-6*ank.t[lo:hi], ank.TS3[lo:hi])
	axes[6].set_ylabel("TS3 (?)")
	axes[6].set_xlabel("time (s)")
def spring_plot(lo=0, hi=5000):
	fig, axes = plt.subplots(1, sharex=True)
	axes.plot(ank.x6[lo:hi],ank.f6[lo:hi])
	axes.set_xlabel("position (m)")
	axes.set_ylabel("force sensor force(N)")
	axes.grid(True)



forces_plot(0,500)
big_plot(0,5000)
spring_plot(0,5000)
plt.show()