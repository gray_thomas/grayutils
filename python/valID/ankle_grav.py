import numpy as np
ang5s=np.array([0.68,0.58,0.54,0.29,0.24,0.22,0,0,0,-0.19,-0.19]).reshape((11,1))
tau5s=np.array([-94.0,-68.0,-98.0,-69.0,-52.0,-42.0,-12.0,-38.0,-54.0,-34.0,-16.0]).reshape((11,1))
ang6s=np.array([0.028,-0.207,0.15,0.17,0.011,0.14,-0.22,-0.04,0.17,0.16,0.02]).reshape((11,1))
tau6s=np.array([-97.0,-95.0,-54.0,-37.0,-55.0,-56.0,-45.0,-25.0,-17.0,0.0,-23.0]).reshape((11,1))

R=(np.concatenate([ang5s,ang6s,np.ones((11,1))],axis=1))
b=np.concatenate([tau5s,tau6s],axis=1)
# print dir(np.linalg)
model, residuals, dim, sigmas= np.linalg.lstsq(R,b)
print "tau5 = %.2f + %.2f ang5 + %.2f ang6"% (model[2,0],model[0,0],model[1,0])
print "tau6 = %.2f + %.2f ang5 + %.2f ang6"% (model[2,1],model[0,1],model[1,1])
# print R, b

# Friday, December 5th 2014:
ang5s=np.array([-0.0675, -0.14,-0.15,0.053,0.057,0.146,  0.33, 0.31, 0.34,  0.52,  0.5, 0.54, 0.2,-0.07,-0.06,-0.17, 0.127]).reshape((-1,1))
tau5s=np.array([   31.6,   6.4,   26,-41.6,-13.6,  8.8, -20.4,-35.2,-54.4, -69.2,-43.0,  -40,   0, 28.4,-19.6,-13.6,   -60]).reshape((-1,1))
ang6s=np.array([ -0.283,0.0107,-0.16,0.232, 0.06,-0.23,-0.223,    0, 0.25, 0.247,-0.02,-0.23,-0.2,-0.22, 0.13, 0.35,  0.34]).reshape((-1,1))
tau6s=np.array([  -27.6,  13.8,   -3,  8.4, -6.0,-40.8, -52.8,-13.8,  1.8, -40.2,-47.4,-62.4, -30,-12.0, 18.6, 27.0,  23.4]).reshape((-1,1))
Xang5s=np.array([  0.44, 0.443, 0.088, 0.086,   0.00]).reshape((-1,1))
Xtau5s=np.array([ -14.0, -67.0, -30.0,  -4.0,  -6.24]).reshape((-1,1))
Xang6s=np.array([ -0.23, 0.174, 0.120, -0.145,  0.00]).reshape((-1,1))
Xtau6s=np.array([ -60.0, -6.52,   6.5,  -24.2, -1.30]).reshape((-1,1))




print ang5s.shape, tau5s.shape, ang6s.shape, tau6s.shape

R1=(np.concatenate([ang5s,ang6s,np.ones((17,1))],axis=1))
Rx=(np.concatenate([Xang5s,Xang6s,np.ones((5,1))],axis=1))
b1=np.concatenate([tau5s,tau6s],axis=1)
bx=np.concatenate([Xtau5s,Xtau6s], axis=1)
lambdaX=2.0
R=np.concatenate([R1,Rx*lambdaX])
b=np.concatenate([b1,bx*lambdaX])
model, residuals, dim, sigmas= np.linalg.lstsq(R,b)
print "tau5 = %.2f + %.2f ang5 + %.2f ang6"% (model[2,0],model[0,0],model[1,0])
print "tau6 = %.2f + %.2f ang5 + %.2f ang6"% (model[2,1],model[0,1],model[1,1])
print R.dot(model)-b

print np.array([-0.15,-0.16,1.0]).dot(model)
# print dir(np.linalg)