function A=dataStruct(fileName)
[A.des_x0,A.des_x1,A.roll, A.pitch,A.dataArray]=getData(fileName);

A.x2=de_mean(A.pitch);
A.x1=de_mean(A.roll);
A.u1=de_mean(A.des_x0);
A.u2=de_mean(A.des_x1);

A.Data1=iddata(A.x1,A.u1,0.001);
A.Data10=iddata(A.x1(1:10:end),A.u1(1:10:end),0.01);
A.Data100=iddata(A.x1(1:100:end),A.u1(1:100:end),0.1);

A.Data2=iddata(A.x2,A.u2,0.001);

A.MISO1=iddata(A.x1,[A.u1 A.u2],0.001);
A.MISO2=iddata(A.x2,[A.u1 A.u2],0.001);
A.MISO3=iddata(A.x1,[A.x2 A.u1 A.u2],0.001);
A.MISO4=iddata(A.x2,[A.x1 A.u1 A.u2],0.001);

end