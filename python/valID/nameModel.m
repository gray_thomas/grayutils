function dispstring=nameModel(system)
    if (size(system.a,1)==1 && size(system.b,1)==1)
        dispstring=['ARMAX(',num2str(system.na),' (',...
            num2str(system.nb),') ',num2str(system.nc),...
            ' k=',num2str(system.nk),')'];
    else
        error('system not handled.')
    end
end