
%% Setup the data
[des_x0,des_x1,roll, pitch,dataArray]=getData('pre-processed_data');
nr=normal_ts(roll);
nu=normal_ts(des_x0);
Data2_10=iddata(nr,nu,0.01);
e2s10Data=Data2_10;

[e2s1des_x0,e2s1des_x1,e2s1roll, e2s1pitch,e2s1dataArray]=getData('s1_pre-processed_data2');
e2s1nr=normal_ts(e2s1roll);
e2s1nu=normal_ts(e2s1des_x0);
e2s1Data=iddata(e2s1nr,e2s1nu,0.001);

[e1s1des_x0,e1s1des_x1,e1s1roll, e1s1pitch,e1s1dataArray]=getData('s1_pre-processed_data1');
e1s1nr=normal_ts(e1s1roll);
e1s1nu=normal_ts(e1s1des_x0);
e1s1Data=iddata(e1s1nr,e1s1nu,0.001);
e1s10Data=iddata(e1s1nr(1:10:end),e1s1nu(1:10:end),0.01);

[e3s1des_x0,e3s1des_x1,e3s1roll, e3s1pitch,e3s1dataArray]=getData('s1_pre-processed_data3');
e3s1nr=normal_ts(e3s1roll);
e3s1nu=normal_ts(e3s1des_x0);
e3s1Data=iddata(e3s1nr,e3s1nu,0.001);
e3s10Data=iddata(e3s1nr(1:10:end),e3s1nu(1:10:end),0.01);

[e4s1des_x0,e4s1des_x1,e4s1roll, e4s1pitch,e4s1dataArray]=getData('s1_pre-processed_data4');
e4s1nr=normal_ts(e4s1roll);
e4s1nu=normal_ts(e4s1des_x0);
e4s1Data=iddata(e4s1nr,e4s1nu,0.001);
e4s10Data=iddata(e4s1nr(1:10:end),e4s1nu(1:10:end),0.01);


%% PostulateARMAX.m step
[e2s10ARMAX,e2s10MyRss]=PostulateARMAX(Data2_10,.99,[0,1]);
e1s10MyRss=getRss(e1s10Data,e2s10ARMAX);
e3s10MyRss=getRss(e3s10Data,e2s10ARMAX);
e4s10MyRss=getRss(e4s10Data,e2s10ARMAX);

%% Postulate and validate the model without downsampling
[e2s1ARMAX,e2s1Rss1]=PostulateARMAX(Data2_1,.99,[0,1]);
e1s1Rss1=getRss(e1s1Data,e2s1ARMAX);
e3s1Rss1=getRss(e3s1Data,e2s1ARMAX);
e4s1Rss1=getRss(e4s1Data,e2s1ARMAX);

e1s10Sigma1=sqrt(e1s1Rss1/(e1s1Data.n-nparams(e2s1ARMAX)))*getsigma(e1s1roll);
e2s10Sigma1=sqrt(e2s1Rss1/(e2s1Data.n-nparams(e2s1ARMAX)))*getsigma(e2s1roll);
e3s10Sigma1=sqrt(e3s1Rss1/(e3s1Data.n-nparams(e2s1ARMAX)))*getsigma(e3s1roll);
e4s10Sigma1=sqrt(e4s1Rss1/(e4s1Data.n-nparams(e2s1ARMAX)))*getsigma(e4s1roll);

e1s1Rss100=getRssK(e1s1Data,e2s1ARMAX,100);
e2s1Rss100=getRssK(e2s1Data,e2s1ARMAX,100);
e3s1Rss100=getRssK(e3s1Data,e2s1ARMAX,100);
e4s1Rss100=getRssK(e4s1Data,e2s1ARMAX,100);
e1s1Sigma100=sqrt(e1s1Rss100/(e1s1Data.n-nparams(e2s1ARMAX)))*getsigma(e1s1roll);
e2s1Sigma100=sqrt(e2s1Rss100/(e2s1Data.n-nparams(e2s1ARMAX)))*getsigma(e2s1roll);
e3s1Sigma100=sqrt(e3s1Rss100/(e3s1Data.n-nparams(e2s1ARMAX)))*getsigma(e3s1roll);
e4s1Sigma100=sqrt(e4s1Rss100/(e4s1Data.n-nparams(e2s1ARMAX)))*getsigma(e4s1roll);


%% Visualize Residual Covariance

resid(e2s10ARMAX,e2s10Data)
suptitle([nameModel(e2s10ARMAX) '@10 ms'])
figuresize(16,12,'centimeters')
print('../../tex/time_series_final_project/e2s10ARMAXresid','-dpdf','-r300');


resid(e2s10ARMAX,e1s10Data)
suptitle([nameModel(e2s10ARMAX) '@10 ms, LF validation'])
figuresize(16,12,'centimeters')
print('../../tex/time_series_final_project/e2s10ARMAXresid1','-dpdf','-r300');


%% Define some colors
utburntorange=(1/256)*[ 191, 87, 0];
utblack=(1/256)*[51 63 72];
utblue=(1/256)*[0, 95, 134];
utgreen=(1/256)*[67, 105, 91];
utyellow=(1/256)*[242, 169, 0 ];
utbrown=(1/256)*[56, 47, 45];
utivory=(1/256)*[214, 210, 196];

%% Predictions

pred200=predict(e2s10ARMAX,e2s10Data,200);
pred100=predict(e2s10ARMAX,e2s10Data,100);
pred10=predict(e2s10ARMAX,e2s10Data,10);

close all
plot(e2s10Data.y,'Color',utblack,'linewidth',8)
hold on
plot(pred10.y,'Color',utbrown,'linewidth',6)
plot(pred100.y,'Color',utburntorange,'linewidth',4)
plot(pred200.y,'Color',utyellow,'linewidth',2)
legend({'X_t','X_t(10)', 'X_t(50)','X_t(100)'})
title('ARMAX(2,1,1) predictions (of variance scaled time series)')
figuresize(16,12,'centimeters')
print('../../tex/time_series_final_project/e2s10ARMAXpreds','-dpdf','-r300');


%% Visualize eigenvalues of the parameter covariance matrix

%% Visualize the controller error predicted by the ARMAX model.
plot(e2s10Data.y*getsigma(roll) - e2s10Data.u*getsigma(des_x0)...
    ,'Color',utblack,'linewidth',2)
hold on


plot(pred10.y*getsigma(roll)  - e2s10Data.u*getsigma(des_x0)...
    ,'Color',utbrown,'linewidth',2)
plot(pred100.y*getsigma(roll)  - e2s10Data.u*getsigma(des_x0)...
    ,'Color',utburntorange,'linewidth',2)
axis([1500 2000 -0.06 0.06])

%% Visualize the controller error predicted by the ARMAX model.
Data=e1s10Data;
uscale=getsigma(e1s1des_x0(1:10:end));
yscale=getsigma(e1s1roll(1:10:end));
% pred200=predict(e2s10ARMAX,Data,200);
pred100=predict(e2s10ARMAX,Data,100);
pred10=predict(e2s10ARMAX,Data,10);

plot(Data.y*yscale - Data.u*uscale...
    ,'Color',utblack,'linewidth',2)
hold on


plot(pred10.y*yscale  - Data.u*uscale...
    ,'Color',utburntorange,'linewidth',2)
plot(pred100.y*yscale  - Data.u*uscale...
    ,'Color',utyellow,'linewidth',2)
title([nameModelLatex(e2s10ARMAX) ' predictions of control error'], 'interpreter', 'latex')
legend({'$X_t$','$X_t(10)$','$X_t(100)$'},'interpreter','latex')
xlabel('tens of miliseconds in low frequency dataset')
ylabel('controller error in approximate radians')
figuresize(16,12,'centimeters')
print('../../tex/time_series_final_project/e2s10ARMAXcontrolerrorpredictions','-dpdf','-r300');

