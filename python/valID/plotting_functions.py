import getData as gd
import numpy as np 
import matplotlib.pyplot as plt 
import cPickle as pkl 
import random
import re
from networkUtils.dyNetUtils.convolutionFilter import ConvolutionFilter


baseSaveFolder="/home/gray/wk/grayutils/ValkyrieAnkleTester/"

def save_plot(baseName):
    special_random_number=random.randint(10000,99999)
    name1=baseSaveFolder+"plot_png/" + baseName+"_k%d.png"%special_random_number
    name2=baseSaveFolder+"plot_pdf/" + baseName+"_k%d.pdf"%special_random_number
    plt.savefig(name1, dpi=200, facecolor='w', edgecolor='w',
        orientation='portrait', papertype='letter', format='png',
        transparent=True, bbox_inches='tight', pad_inches=0.4)
    plt.savefig(name2, dpi=200, facecolor='w', edgecolor='w',
        orientation='portrait', papertype='letter', format='pdf',
        transparent=True, bbox_inches='tight', pad_inches=0.4)

def forces_plot(lo=0, hi=5000, sk=1):
    fig, axes = plt.subplots(3, sharex=True)
    axes[0].plot(1e-6*ank.t[lo:hi:sk], ank.tau6[lo:hi:sk])
    axes[0].set_ylabel("current (A)")
    axes[1].plot(1e-6*ank.t[lo:hi:sk], ank.f6[lo:hi:sk])
    axes[1].set_ylabel("force (N)")
    axes[2].plot(1e-6*ank.t[lo:hi:sk], ank.x6[lo:hi:sk])
    axes[2].set_xlabel("time (s)")
    axes[2].set_ylabel("pos (m)")

def big_plot(ank, lo=0, hi=5000, sk=1):
    fig, axes = plt.subplots(7, sharex=True)
    axes[0].plot(1e-6*ank.t[lo:hi:sk], ank.tau6[lo:hi:sk])
    axes[0].set_ylabel("current (A)")
    axes[1].plot(1e-6*ank.t[lo:hi:sk], ank.f6[lo:hi:sk])
    axes[1].set_ylabel("force (N)")
    axes[2].plot(1e-6*ank.t[lo:hi:sk], ank.x6[lo:hi:sk])
    axes[2].set_ylabel("pos (m)")
    axes[3].plot(1e-6*ank.t[lo:hi:sk], ank.xd6[lo:hi:sk])
    axes[3].set_ylabel("vel (m/s)")
    axes[4].plot(1e-6*ank.t[lo:hi:sk], ank.TS1[lo:hi:sk])
    axes[4].set_ylabel("TS1 (?)")
    axes[5].plot(1e-6*ank.t[lo:hi:sk], ank.TS2[lo:hi:sk])
    axes[5].set_ylabel("TS2 (?)")
    axes[6].plot(1e-6*ank.t[lo:hi:sk], ank.TS3[lo:hi:sk])
    axes[6].set_ylabel("TS3 (?)")
    axes[6].set_xlabel("time (s)")

def identify_steady_state_model(ank, lo=0, hi=5000, sk=1):
    fig, axes = plt.subplots(8, sharex=True)
    t=ank.t[lo:hi:sk]
    tau5=ank.tau5[lo:hi:sk]
    tau6=ank.tau6[lo:hi:sk]
    f5=ank.f5[lo:hi:sk]
    f6=ank.f6[lo:hi:sk]
    wrench=ank.force_torque[lo:hi:sk,:]
    unity=np.ones(f5.shape)

    R=np.hstack([tau5.reshape((-1,1)),tau6.reshape((-1,1))])
    b=np.hstack([f5.reshape((-1,1)),f6.reshape((-1,1)),wrench])
    x,residuals, rank, singular_values=np.linalg.lstsq(R,b)
    print "x", x
    print "residuals", residuals
    print "rank", rank
    print "singular_values", singular_values
    b_est=R.dot(x)
    labels=[r"$f_5 (N)$", r"$f_6 (N)$", r"$W_{fx} (N)$",r"$W_{fy} (N)$",r"$W_{fz} (N)$",
        r"$W_{\tau x} (N)$",r"$W_{\tau y} (N)$",r"$W_{\tau z} (N)$"]
    # for i in range(0,8):
        # axes[i].plot(1e-6*t, b[:,i], 'k', linewidth=5)
        # axes[i].plot(1e-6*t, b_est[:,i], 'b', linewidth=2)
        # axes[i].set_ylabel(labels[i])
    axes[7].set_xlabel(r"time (s)")
    # save_plot("model_1_exp_%d_lo_%d_hi_%d_sk_%d"%(ank.dataSetNumber,lo,hi,sk))

def identify_actuator_dynamics_model(ank, lo=0, hi=5000, sk=1):
    vD=5 #samples
    lo=lo+vD
    t=ank.t[lo:hi:sk]
    unity=np.ones(t.shape)
    timesteps=(t-ank.t[lo-vD:hi-vD:sk])
    reciprocal_of_timesteps=unity/timesteps

    # x - state
    e5=ank.e5[lo:hi:sk]
    e6=ank.e6[lo:hi:sk]
    ed5=(e5-ank.e5[lo-vD:hi-vD:sk]) * reciprocal_of_timesteps
    ed6=(e6-ank.e6[lo-vD:hi-vD:sk]) * reciprocal_of_timesteps
    # y - outputs
    tau5=ank.tau5[lo:hi:sk]
    tau6=ank.tau6[lo:hi:sk]
    f5=ank.f5[lo:hi:sk]
    fd5=(f5-ank.f5[lo-vD:hi-vD:sk]) * reciprocal_of_timesteps
    f6=ank.f6[lo:hi:sk]
    fd6=(f6-ank.f6[lo-vD:hi-vD:sk]) * reciprocal_of_timesteps
    x5=ank.x5[lo:hi:sk]
    x6=ank.x6[lo:hi:sk]
    xd5=ank.xd5[lo:hi:sk]
    xd6=ank.xd6[lo:hi:sk]
    states=2
    inputs=2
    R=np.hstack([
        e5.reshape((-1,1)), e6.reshape((-1,1)), 
        tau5.reshape((-1,1)), tau6.reshape((-1,1)),
        unity.reshape((-1,1))])
    b=np.hstack([
        ed5.reshape((-1,1)), ed6.reshape((-1,1)), 
        f5.reshape((-1,1)), f6.reshape((-1,1)),
        fd5.reshape((-1,1)), fd6.reshape((-1,1)),
        x5.reshape((-1,1)), x6.reshape((-1,1)),
        xd5.reshape((-1,1)), xd6.reshape((-1,1))])
    labels=[
        r"$\dot e_5 (N)$", r"$ \dot e_6 (N)$",
        r"$f_5 (N)$", r"$f_6 (N)$", 
        r"$\dot f_5 (N)$", r"$ \dot f_6 (N)$",
        r"$x_5 (N)$", r"$x_6 (N)$",
        r"$\dot x_5 (N)$", r"$\dot x_6 (N)$"]

    x,residuals, rank, singular_values=np.linalg.lstsq(R,b)
    print "xdot_0", x.T[0:states,-1]
    print "A", x.T[0:states,0:states]
    print "B", x.T[0:states,states:(states+inputs)]
    print "C", x.T[states:-1,0:states]
    print "D", x.T[states:-1,states:(states+inputs)]
    print "y_0", x.T[0:states,-1]
    print "residuals", residuals
    print "rank", rank
    print "singular_values", singular_values

    fig, axes = plt.subplots(len(labels), sharex=True)
    fig.suptitle("First Order Torque Dynamics Model")
    b_est=R.dot(x)
    print b[100,:]
    print b_est[100,:]
    for i in range(0,len(labels)):
        axes[i].plot(1e-6*t, b[:,i], 'k', linewidth=3)
        axes[i].plot(1e-6*t, b_est[:,i], 'm', linewidth=2)
        axes[i].set_ylabel(labels[i])
    axes[-1].set_xlabel(r"time (s)")
    save_plot("act_dyn_model1_exp_%d_lo_%d_hi_%d_sk_%d_derivs_%d"%(ank.dataSetNumber,lo,hi,sk,vD))

def identify_second_order_actuator_dynamics_model(ank, lo=0, hi=5000, sk=1):
    vD=5 # samples used to approximate velocity
    lo=lo+vD*2
    t=ank.t[lo:hi:sk]
    unity=np.ones(t.shape)
    timesteps=(t-ank.t[lo-vD:hi-vD:sk])
    reciprocal_of_timesteps=unity/timesteps

    # x - state
    e5=ank.e5[lo:hi:sk]
    e6=ank.e6[lo:hi:sk]
    ed5=(e5-ank.e5[lo-vD:hi-vD:sk]) * reciprocal_of_timesteps
    ed6=(e6-ank.e6[lo-vD:hi-vD:sk]) * reciprocal_of_timesteps
    print (reciprocal_of_timesteps * reciprocal_of_timesteps).shape
    print (-1.0*e5+2.0*ank.e5[lo-vD:hi-vD:sk]).shape
    print (-1.0*ank.e5[lo-2*vD:hi-2*vD:sk]).shape
    print ((-1.0*e5+2.0*ank.e5[lo-vD:hi-vD:sk])-ank.e5[lo-2*vD:hi-2*vD:sk]).shape
    print (-1.0*e5+2.0*ank.e5[lo-vD:hi-vD:sk]+-1.0*ank.e5[lo-2*vD:hi-2*vD:sk]).shape
    edd5=(-e5+2.0*ank.e5[lo-vD:hi-vD:sk]+ank.e5[lo-2*vD:hi-2*vD:sk]) * (reciprocal_of_timesteps * reciprocal_of_timesteps)
    edd6=(-e6+2.0*ank.e6[lo-vD:hi-vD:sk]+ank.e6[lo-2*vD:hi-2*vD:sk]) * reciprocal_of_timesteps * reciprocal_of_timesteps
    # y - outputs
    tau5=ank.tau5[lo:hi:sk]
    tau6=ank.tau6[lo:hi:sk]
    f5=ank.f5[lo:hi:sk]
    fd5=(f5-ank.f5[lo-vD:hi-vD:sk]) * reciprocal_of_timesteps
    f6=ank.f6[lo:hi:sk]
    fd6=(f6-ank.f6[lo-vD:hi-vD:sk]) * reciprocal_of_timesteps
    x5=ank.x5[lo:hi:sk]
    x6=ank.x6[lo:hi:sk]
    xd5=ank.xd5[lo:hi:sk]
    xd6=ank.xd6[lo:hi:sk]
    states=4
    inputs=2
    R=np.hstack([
        e5.reshape((-1,1)), e6.reshape((-1,1)),
        ed5.reshape((-1,1)), ed6.reshape((-1,1)),
        tau5.reshape((-1,1)), tau6.reshape((-1,1)),
        unity.reshape((-1,1))])
    b=np.hstack([
        ed5.reshape((-1,1)), ed6.reshape((-1,1)),
        edd5.reshape((-1,1)), edd6.reshape((-1,1)),
        f5.reshape((-1,1)), f6.reshape((-1,1)),
        fd5.reshape((-1,1)), fd6.reshape((-1,1)),
        x5.reshape((-1,1)), x6.reshape((-1,1)),
        xd5.reshape((-1,1)), xd6.reshape((-1,1))])
    labels=[
        r"$\dot e_5 (N)$", r"$ \dot e_6 (N)$",
        r"$\ddot e_5 (N)$", r"$ \ddot e_6 (N)$",
        r"$f_5 (N)$", r"$f_6 (N)$", 
        r"$\dot f_5 (N)$", r"$ \dot f_6 (N)$",
        r"$x_5 (N)$", r"$x_6 (N)$",
        r"$\dot x_5 (N)$", r"$\dot x_6 (N)$"]

    x,residuals, rank, singular_values=np.linalg.lstsq(R,b)
    print "xdot_0", x.T[0:states,-1]
    print "A", x.T[0:states,0:states]
    print "B", x.T[0:states,states:(states+inputs)]
    print "C", x.T[states:-1,0:states]
    print "D", x.T[states:-1,states:(states+inputs)]
    print "y_0", x.T[0:states,-1]
    print "residuals", residuals
    print "rank", rank
    print "singular_values", singular_values

    fig, axes = plt.subplots(len(labels), sharex=True)
    fig.suptitle("First Order Torque Dynamics Model")
    b_est=R.dot(x)
    print b[100,:]
    print b_est[100,:]
    for i in range(0,len(labels)):
        axes[i].plot(1e-6*t, b[:,i], 'k', linewidth=3)
        axes[i].plot(1e-6*t, b_est[:,i], 'm', linewidth=2)
        axes[i].set_ylabel(labels[i])
    axes[-1].set_xlabel(r"time (s)")
    save_plot("act_dyn_model2_exp_%d_lo_%d_hi_%d_sk_%d_derivs_%d"%(ank.dataSetNumber,lo,hi,sk,vD))

def calibrate_force_torque(ank):
    print "calibrating forces and torques"
    cal_mat=np.array([
        [-171.138762092584, 164.262330318272, 1136.66933015971, -25528.3827006466, -892.147266185941, 25018.097320641],
        [-2393.11261214503, 29302.8374381793, 176.909372134555, -14538.0957864741, 1032.08177266801, -14648.9548591062 ],
        [41139.1938396664, 518.608163331987, 40227.7537078209, 385.007116653545, 41350.2621211616, 447.58447722459 ],
        [-21.8401291003204, 421.344052448737, -872.343264955442, -221.911677460517, 886.648930652221, -196.26924129588 ],
        [996.949904915404, 14.3104375478289, -505.787839958358, 358.430597717003, -495.101373345106, -364.002555342701 ],
        [33.9397030997519, -469.070679591882, 21.1841848707929, -445.411314827181, 16.1145946572983, -437.463633877703 ]])
    gauge_gains=np.array([75, 75, 75, 73, 75, 73])
    gauge_offsets=np.array([32858, 31310, 32988, 29358, 33420, 30796])
    scale_factors=np.array([51882.0, 51882.0, 51882.0, 916.0, 916.0, 916.0])
    inverse_scale_factors=np.ones((6,))/scale_factors
    raw=np.concatenate([(ank.TS1).reshape((-1,1)),(ank.TS2).reshape((-1,1)),(ank.TS3).reshape((-1,1)),
                        (ank.TS4).reshape((-1,1)),(ank.TS5).reshape((-1,1)),(ank.TS6).reshape((-1,1))], axis=1)
    bias=np.ones(raw.shape).dot(np.diagflat(gauge_offsets))
    # print raw.shape, np.diagflat(gauge_offsets).shape, bias.shape, cal_mat.shape
    ank['force_torque'] = ((raw-bias).dot(cal_mat.T)).dot(np.diagflat(inverse_scale_factors)) * 1.0/1000000.0
    print "done"
def force_torque_plot(ank, lo=0, hi=-1, sk=1):
    fig, axes = plt.subplots(8, sharex=True, figsize=(22,12))
    fig.suptitle('Six Axis Results, Experiment %d'%ank.dataSetNumber, fontsize=14, fontweight='bold')
    axes[0].set_title("data range [%d:%d:%d]"%(lo,hi,sk))
    axes[0].plot(1e-6*ank.t[lo:hi:sk], ank.f5[lo:hi:sk])
    axes[0].set_ylabel(r"$f_5 (N)$")
    axes[1].plot(1e-6*ank.t[lo:hi:sk], ank.f6[lo:hi:sk])
    axes[1].set_ylabel(r"$f_6 (N)$")
    axes[2].plot(1e-6*ank.t[lo:hi:sk], ank.force_torque[lo:hi:sk,0])
    axes[2].set_ylabel(r"$f_x (N)$")
    axes[3].plot(1e-6*ank.t[lo:hi:sk], ank.force_torque[lo:hi:sk,1])
    axes[3].set_ylabel(r"$f_y (N)$")
    axes[4].plot(1e-6*ank.t[lo:hi:sk], ank.force_torque[lo:hi:sk,2])
    axes[4].set_ylabel(r"$f_z (N)$")
    axes[5].plot(1e-6*ank.t[lo:hi:sk], ank.force_torque[lo:hi:sk,3])
    axes[5].set_ylabel(r"$\tau_x (Nm)$")
    axes[6].plot(1e-6*ank.t[lo:hi:sk], ank.force_torque[lo:hi:sk,4])
    axes[6].set_ylabel(r"$\tau_y (Nm)$")
    axes[7].plot(1e-6*ank.t[lo:hi:sk], ank.force_torque[lo:hi:sk,5])
    axes[7].set_ylabel(r"$\tau_z (Nm)$")
    axes[7].set_xlabel("time (s)")
    save_plot("force_torque_overview_exp_%d_lo_%d_hi_%d_sk_%d"%(ank.dataSetNumber,lo,hi,sk))

def force_dynamics_plot(ank, lo=0, hi=-1, sk=1):
    fig, axes = plt.subplots(2, sharex=True, figsize=(22,12))
    fig.suptitle('Force Dynamics, Experiment %d'%ank.dataSetNumber, fontsize=14, fontweight='bold')
    axes[0].set_title("data range [%d:%d:%d]"%(lo,hi,sk))
    axes[0].plot(1e-6*ank.t[lo:hi:sk], ank.tau5[lo:hi:sk])
    axes[0].plot(1e-6*ank.t[lo:hi:sk], ank.eff5[lo:hi:sk])
    axes[0].set_ylabel(r"$Newtons$")
    # print [word for word in dir(axes[0]) if re.compile("legend").findall(word)]
    axes[0].legend([
        r"$a$",
        r"$b$"])
    axes[1].plot(1e-6*ank.t[lo:hi:sk], ank.tau6[lo:hi:sk])
    axes[1].plot(1e-6*ank.t[lo:hi:sk], ank.eff6[lo:hi:sk])
    axes[1].set_ylabel(r"$Newtons$")
    axes[1].legend([
        r"$a$",
        r"$b$"])
    axes[-1].set_xlabel("time (s)")
    save_plot("force_dynamics_exp_%d_lo_%d_hi_%d_sk_%d"%(ank.dataSetNumber,lo,hi,sk))


def spring_linearity_plot(ank, lo=0, hi=5000, sk=1, act=5):
    print "generating spring linearity plot for spring %d" % act
    fig, axes = plt.subplots(1, sharex=True)
    fig.suptitle('Spring %d Linearity, Experiment %d'%(act,ank.dataSetNumber), fontsize=14, fontweight='bold')
    
    axes.scatter(ank['eff%d'%act][lo:hi],ank['f%d'%act][lo:hi], alpha=0.05,  edgecolors='none')

    axes.set_title("Data range [%d:%d:%d]" % (lo,hi,sk) )
    axes.set_xlabel("spring encoder force (N)")
    axes.set_ylabel("linear force sensor force(N)")
    axes.grid(True)

    save_plot("spring_linearity_plot_exp_%d_lo_%d_hi_%d_sk_%d" % (ank.dataSetNumber, lo, hi,sk))
    print "done."

def timing_plot(ank):
    print "generating timing_plot"
    fig, axes = plt.subplots(1, sharex=True)
    fig.suptitle('Timing Accuracy, Experiment %d'%ank.dataSetNumber, fontsize=14, fontweight='bold')
    axes.plot(1e-6*ank.t[:-1], (ank.t[1:]-ank.t[:-1])/(ank.iters[1:]-ank.iters[:-1]))
    axes.set_xlabel("time (s)")
    axes.set_ylabel(r"$\mu \mathrm{s} / \rm{tic} $")
    axes.grid(True)
    save_plot("timing_plot_exp_%d"%ank.dataSetNumber)

def log_timing_plot(ank):
    print "generating timing_plot"
    fig, axes = plt.subplots(1, sharex=True)
    fig.suptitle('Timing Accuracy, Experiment %d'%ank.dataSetNumber, fontsize=14, fontweight='bold')
    axes.semilogy(1e-6*ank.t[:-1], (ank.t[1:]-ank.t[:-1])/(ank.iters[1:]-ank.iters[:-1]))
    axes.set_xlabel("time (s)")
    axes.set_ylabel(r"$\mu \mathrm{s} / \rm{tic} $")
    axes.grid(True)
    save_plot("timing_semilogy_exp_%d"%ank.dataSetNumber)

def fault_plot(ank,lo,hi):
    fig, axes = plt.subplots(4, sharex=True, figsize=(8,6))
    fig.suptitle('Cause of Motor 5 Fault, Experiment %d'%ank.dataSetNumber, fontsize=14, fontweight='bold')
    axes[0].plot(1e-6*ank.t[lo:hi], (ank.faults5)[lo:hi], linewidth=3)
    axes[0].set_ylabel(r"$\mathrm{faults}_5$",fontsize=14)

    axes[1].plot(1e-6*ank.t[lo:hi], ank.inc_pos_5[lo:hi], linewidth=3)
    axes[1].set_ylabel(r"$x_{\mathrm{incEnc}5}$",fontsize=14)
    axes[2].plot(1e-6*ank.t[lo:hi], ank.inc_vel_5[lo:hi], linewidth=3)
    axes[2].set_ylabel(r"$\dot x_{\mathrm{incEnc}5}$",fontsize=14)
    axes[3].plot(1e-6*ank.t[lo:hi], ank.it[lo:hi],'.', linewidth=3)
    axes[3].set_ylabel("iteration",fontsize=14)
    axes[3].set_xlabel("time (s)",fontsize=14)
    # axes.grid(True)
    save_plot("fault_plot_exp_%d_lo_%d_hi_%d" % (ank.dataSetNumber, lo, hi))


if __name__=="__main__":
    dataSetNumber=12
    data_file_folder="/home/gray/wk/largeDataFiles/"
    data_file_name="data%d.dat"%dataSetNumber

    print "generating ankle data object"
    ank=gd.AnkleData(data_file_name,data_file_folder)
    ank.dataSetNumber=dataSetNumber
    ank.data_file_folder=data_file_folder
    ank.data_file_name=data_file_name
    print "generated" 
    lo=0
    hi=33740
    # exp 11 : 0-33740 good 33750-34005 outage 34250-51482 probably back to being good
    # calibrate_force_torque(ank)
    # identify_second_order_actuator_dynamics_model(ank,lo=lo,hi=hi,sk=1)
    force_dynamics_plot(ank,lo=lo,hi=hi,sk=1)
    # forces_plot(0,5000,100)
    # big_plot(0,50000,100)
    # spring6_plot(10000,200000,1)
    # spring_linearity_plot(ank, lo=lo, hi=hi, sk=1, act=5)
    # spring_linearity_plot(ank, lo=lo, hi=hi, sk=1, act=6)
    # fault_plot(ank, -4900, -4675) # localization of fault in exp 10
    # fault_plot(ank, 348540, 348555) # localization of spike in exp 10
    # fault_plot(ank, 0, -1)
    # fault_plot(ank, 398500, 398660) # localization of an exp 9 lag spike
    # fault_plot(ank, 404200, 404450) # localization of exp 9 fault and response
    # fault_plot(ank, 404246, 404266) # localization of exp 9 vel spike
    # force_torque_plot(ank, lo=lo, hi=hi, sk=1)
    # log_timing_plot(ank)
    print "showtime"
    plt.show()