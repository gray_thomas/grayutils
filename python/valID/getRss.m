function Rss=getRss(Data,model)
r=pe(model,Data);
residuals=r.y;
Rss=sum(residuals.^2);
end