function ts=normal_ts(in)
    q=in-mean(in);
    inv_var=(size(in,1)-1)/(q'*q); % the most likely of all variances
    ts= q*sqrt(inv_var);
end
