%% Define some colors
utburntorange=(1/256)*[ 191, 87, 0];
utblack=(1/256)*[51 63 72];
utblue=(1/256)*[0, 95, 134];
utgreen=(1/256)*[67, 105, 91];
utyellow=(1/256)*[242, 169, 0 ];
utbrown=(1/256)*[56, 47, 45];
utivory=(1/256)*[214, 210, 196];


%% do some plotting


plot([normal_ts(roll),normal_ts(des_x)])
% e5 and f5 are basically the same
% e6 and f6 are basically the same
% e6 and t6 seem related through stable dynamics

[~,foundARMA,~]=PostulateARMA(normal_ts(roll),.95);

arma21roll=armax(normal_ts(roll),[2,1]);
arma32roll=armax(normal_ts(roll),[3,2]);
% compare(normal_ts(roll),arma21roll,10)
resid(foundARMA,normal_ts(roll))
size(normal_ts(roll),1)

P=pzoptions;
P.ConfidenceRegionNumberSD=2;

%% pole zeros for arma(10,9)
pzplot(foundARMA,P)
title('Pole-Zero Map: ARMA(10,9)')
figuresize(8,8,'centimeters')
print('../../tex/time_series_final_project/pzplot_arma109','-dpdf','-r300');

%% Residuals for arma(10,9)
resid(foundARMA,normal_ts(roll))
title('Correlation function of residuals, ARMA(10,9)')
figuresize(10,5,'centimeters')
print('../../tex/time_series_final_project/resid_arma109','-dpdf','-r300');

%% pole zeros for arma(3,2)
pzplot(arma32roll,P)
title('Pole-Zero Map: ARMA(3,2)')
figuresize(8,8,'centimeters')
print('../../tex/time_series_final_project/pzplot_arma32','-dpdf','-r300');

%% Residuals for arma(3,2)
resid(arma32roll,normal_ts(roll))
title('Correlation function of residuals, ARMA(3,2)')
figuresize(10,5,'centimeters')
print('../../tex/time_series_final_project/resid_arma32','-dpdf','-r300');

%% pole zeros for arma(2,1)
pzplot(arma21roll,P)
title('Pole-Zero Map: ARMA(3,2)')
figuresize(8,8,'centimeters')
print('../../tex/time_series_final_project/pzplot_arma21','-dpdf','-r300');

%% Test some predictions
nr=normal_ts(roll);
plot(nr)
hold all
for i=[10,50,100]
    plot(predict(arma32roll,nr,i),'linewidth',2)
end
legend({'X_t','X_t(10)', 'X_t(50)','X_t(100)'})
title('ARMA(3,2) predictions (of variance scaled time series)')
figuresize(16,12,'centimeters')
print('../../tex/time_series_final_project/predictions_arma32','-dpdf','-r300');
% plot(ts,'k')

%% Test some predictions, arma 10,9
nr=normal_ts(roll);
plot(nr)
hold all
for i=[10,50,100]
    plot(predict(foundARMA,nr,i),'linewidth',2)
end
legend({'X_t','X_t(10)', 'X_t(50)','X_t(100)'})
title('ARMA(10,9) predictions (of variance scaled time series)')
figuresize(16,12,'centimeters')
print('../../tex/time_series_final_project/predictions_arma109','-dpdf','-r300');
% plot(ts,'k')

%% Fit some ARMAX!!
nr=normal_ts(roll);
nu=normal_ts(des_x);

[n1,n2]=size(nr);
[n3,n4]=size(nu);
if (n2~=1)
    disp('failure: multiple outputs')
end
if (n1 ~= n3)
    disp('failure: mismatched time sample numbers')
end

Data=iddata(nr,nu);
na=3*ones(n2,n2);
nb=1*ones(n2,n4);
nc=1*ones(n2,n2);
%Initializing
CurrentModel=armax(Data,[na nb nc,0]);
res=pe(CurrentModel,Data);
rss=res.y'*res.y;

pzplot(CurrentModel)
pred200=predict(CurrentModel,Data,200);
pred100=predict(CurrentModel,Data,100);
pred10=predict(CurrentModel,Data,10);

close all
plot(Data.y,'Color',utblack,'linewidth',4)
hold on
plot(pred10.y,'Color',utgreen,'linewidth',4)
plot(pred100.y,'Color',utyellow,'linewidth',4)
plot(pred200.y,'Color',utburntorange,'linewidth',4)
legend({'X_t','X_t(10)', 'X_t(50)','X_t(100)'})
title('ARMAX(3,1,1) predictions (of variance scaled time series)')
figuresize(16,12,'centimeters')
print('../../tex/time_series_final_project/armax_3_1_1_preds,'-dpdf','-r300');

%% Fit an ARMAX(2,1,1)
nr=normal_ts(roll);
nu=normal_ts(des_x);

[n1,n2]=size(nr);
[n3,n4]=size(nu);
if (n2~=1)
    disp('failure: multiple outputs')
end
if (n1 ~= n3)
    disp('failure: mismatched time sample numbers')
end

Data=iddata(nr,nu);
na=2*ones(n2,n2);
nb=1*ones(n2,n4);
nc=1*ones(n2,n2);
%Initializing
CurrentModel=armax(Data,[na nb nc,0]);
res=pe(CurrentModel,Data);
rss=res.y'*res.y
%% PZ
pzplot(CurrentModel)
title('Pole-Zero Map: ARMAX(2,1,1)')
figuresize(8,8,'centimeters')
print('../../tex/time_series_final_project/armax_2_1_1_pz','-dpdf','-r300');
%% Predictions
pred200=predict(CurrentModel,Data,200);
pred100=predict(CurrentModel,Data,100);
pred10=predict(CurrentModel,Data,10);

close all
plot(Data.y,'Color',utblack,'linewidth',8)
hold on
plot(pred10.y,'Color',utbrown,'linewidth',6)
plot(pred100.y,'Color',utburntorange,'linewidth',4)
plot(pred200.y,'Color',utyellow,'linewidth',2)
legend({'X_t','X_t(10)', 'X_t(50)','X_t(100)'})
title('ARMAX(2,1,1) predictions (of variance scaled time series)')
figuresize(16,12,'centimeters')
print('../../tex/time_series_final_project/armax_2_1_1_preds','-dpdf','-r300');

%% Residuals
close all
resid(Data,CurrentModel )
suptitle('ARMAX(2,1,1) residuals')
figuresize(16,12,'centimeters')
print('../../tex/time_series_final_project/armax_2_1_1_residuals','-dpdf','-r300');






