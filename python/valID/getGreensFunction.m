function green=getGreensFunction(Model,Data,N)
noise_model=idpoly(Model.a,Model.c);
green=impulse(noise_model,1:N);
end