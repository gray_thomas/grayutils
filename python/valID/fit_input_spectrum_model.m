[~,I_model,~]=PostulateARMA(B.Data10.u,.995);
I_model.Ts=0.01;
figure();
spectrum(I_model);
ax=gca();
ax.Children(1).Children(1).set('LineWidth',3,'Color',utburntorange)
ydat=ax.Children(1).Children(1).YData;
xdat=ax.Children(1).Children(1).XData;

title('Input modeled as ARMA(4 3 $\Delta$=0.01)','interpreter', 'latex')
figuresize(16,12,'centimeters')
fname=[ 'M1_Input_Spectrum'];
print(['../../tex/time_series_final_project/' fname],'-dpdf','-r300');
% close(fig.Number)
fprintf(['{ %% Forecast for %s\n',...
	'\t\\usebackgroundtemplate{}\n',...
    '\t\\setbeamertemplate{navigation symbols}{}\n',...
    '\t\\begin{frame}[plain]\n',...
    '\t\t    \\begin{tikzpicture}[remember picture,overlay]\n',...
    '\t\t\t        \\node[at=(current page.center)] {\n',...
    '\t\t\t\t            \\includegraphics[width=\\paperwidth]{%s}\n',...
    '\t\t\t        };\n',...
    '\t\t    \\end{tikzpicture}\n',...
    '\t \\end{frame}\n',...
    '}\n'],nameModel(I_model),fname)

figure();
bode(model1)
ax=gca();
ax.Children(1).Children(1).set('LineWidth',3,'Color',utburntorange)
fig=gcf();
fig.Children(3).Children(1).Children(1).set('LineWidth',3,'Color',utburntorange)
magX=fig.Children(3).Children(1).Children(1).XData;
magY=fig.Children(3).Children(1).Children(1).YData;

phasX=ax.Children(1).Children(1).XData;
phasY=ax.Children(1).Children(1).YData;
title(['TF for ' nameModelLatex(model1) ' and what we actually tested.'],'interpreter', 'latex')

Ylims=[-40,10];
xs=[xdat,flip(xdat)];
% CData=[(1:198)',(1:198)',(1:198)']*(1/198);
% CDataF=[(1:97)',(1:97)',(1:97)']*(1/97);
r=ydat(2:end-1);
rmin=min(r);
rmax=max(r);
rnorm=(r-rmin)./(rmax-rmin);
invrnorm=ones(97,1)-rnorm';

CDataF=invrnorm*utblack+rnorm'*[1,1,1];
ys=[-40*ones(1,length(xdat)), 10*ones(1,length(xdat))];


vert3(2:2:2*length(xdat)+1,:)=[xdat',10*ones(length(xdat),1)];
vert3(1:2:2*length(xdat),:)=[xdat',-40*ones(length(xdat),1)];
fac=ones(97,1)*[1 2 4 3]+(1:97)'*2*ones(1,4);

vert4(2:2:2*length(xdat)+1,:)=[xdat',max(phasY)*ones(length(xdat),1)];
vert4(1:2:2*length(xdat),:)=[xdat',min(phasY)*ones(length(xdat),1)];

ADataF=0.5*invrnorm;

patch('Faces',fac,'Vertices',vert4,'FaceColor','flat','EdgeColor','None',...
    'FaceVertexCData',CDataF,'FaceVertexAlphaData',ADataF,'FaceAlpha','flat')
axes(fig.Children(3))
% patch('Faces',fac,'Vertices',vert3,'FaceColor','interp','FaceVertexCData',CData)
patch('Faces',fac,'Vertices',vert3,'FaceColor','flat','EdgeColor','None',...
    'FaceVertexCData',CDataF,'FaceVertexAlphaData',ADataF,'FaceAlpha','flat')


figuresize(16,12,'centimeters')
fname=[ 'M1_Known'];
print(['../../tex/time_series_final_project/' fname],'-dpdf','-r300');
% close(fig.Number)
fprintf(['{ %% AWESOME KNOWLEDGE PLOT\n',...
	'\t\\usebackgroundtemplate{}\n',...
    '\t\\setbeamertemplate{navigation symbols}{}\n',...
    '\t\\begin{frame}[plain]\n',...
    '\t\t    \\begin{tikzpicture}[remember picture,overlay]\n',...
    '\t\t\t        \\node[at=(current page.center)] {\n',...
    '\t\t\t\t            \\includegraphics[width=\\paperwidth]{%s}\n',...
    '\t\t\t        };\n',...
    '\t\t    \\end{tikzpicture}\n',...
    '\t \\end{frame}\n',...
    '}\n'],fname)


% plot(magX,magY,'LineWidth',3,'Color',utburntorange)

% redo phase
% plot(fig.Children(3).Children(1).Children().XData,fig.Children(3).Children(1).Children().YData,'LineWidth',3,'Color',utburntorange)
