function new_model=apply_update(model,update)
index=1;
if ~isempty(model.Structure.a)
    new_a=model.a;
    for i=1:length(model.a)
        if model.Structure.a.Free(i)
            new_a(i)=new_a(i)+update(index);
            index=index+1;
        end
    end
else
    new_a=1;
end
new_b=model.b;
for i=1:length(model.b)
    if model.Structure.b.Free(i)
        new_b(i)=new_b(i)+update(index);
        index=index+1;
    end
end
new_c=model.c;
for i=1:length(model.c)
    if model.Structure.c.Free(i)
        new_c(i)=new_c(i)+update(index);
        index=index+1;
    end
end
if ~isempty(model.Structure.d)
    new_d=model.d;
    for i=1:length(model.d)
        if model.Structure.d.Free(i)
            new_d(i)=new_d(i)+update(index);
            index=index+1;
        end
    end
else
    new_d=1;
end
if ~isempty(model.Structure.f)
    new_f=model.f;
    for i=1:length(model.f)
        if model.Structure.f.Free(i)
            new_f(i)=new_f(i)+update(index);
            index=index+1;
        end
    end
else
    new_f=1;
end
new_model=idpoly(new_a,new_b,new_c,new_d,new_f,...
    model.NoiseVariance, model.Ts);


end