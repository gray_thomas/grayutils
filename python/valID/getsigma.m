function sigma=getsigma(in)
    q=in-mean(in);
    var=(q'*q)/(size(in,1)-1); % the most likely of all variances
    sigma= sqrt(var);
end