function [Model,Rss]=PostulateARMAXO(Data,P,nkoptions,opt)
%Variable Data is an iddata object, and P is a significance threshold for
%statistical fisher testing. nkoptions specifies the search options for the
%dead time parameter nk. eg. [2,3,5,10], indicates to try all four options
%for each level of armax model and use the best to represent that model
%class.
%
%
%Model is the model object representing the ARMA model fitted to the Data time-series object.
%It contains all necessary information about this model.
%Type set(idpoly) to see all the model properties and how you can get them.
%
%m is the mean value of the Data time-series calculated using the simple sample mean.
%
%Created by Dragan Djurdjanovic, September 2002, Ann Arbor, MI. 
%Modified Mar. 19th 2013 in Austin, TX
%Modified from Dragan's PostulateARMA by Gray Thomas in 2015, Austin, TX


Cycle=1;

[N,nu]=size(Data.u);
[~,ny]=size(Data.y);
assert( ny==1) % I have not actually handled the other cases yet.

% begin with ARMAX(2,[1,1,1,1,...],1)
na=2*ones(ny,ny);
nb=ones(ny,nu);
nc=1*ones(ny,ny);
%Initializing
[CurrentModel, CurrentRSS]=...
        bestDelayedARMAXModel(Data, na,nb,nc,nkoptions);
n=1;

while Cycle
    n=n+1;
    OldModel=CurrentModel;
    OldRSS=CurrentRSS;
    na=(2*n)*ones(ny,ny);
    nb=(2*n-1)*ones(ny,nu);
    nc=(2*n-1)*ones(ny,ny);
    
    [CurrentModel, CurrentRSS]=...
        bestDelayedARMAXModel(Data, na,nb,nc,nkoptions);
    
    [TestRatio,Control]=testFischer(CurrentRSS,OldRSS,...
        1+nparams(CurrentModel),1+nparams(OldModel),N,P);
    
    if TestRatio<Control
        disp(['smaller model ',nameModel(OldModel),') wins!'])
        CurrentModel=OldModel;
        CurrentRSS=OldRSS;
        Cycle=0;
    end
end


%Now check if the odd valued model is good
OldModel=CurrentModel;
OldRSS=CurrentRSS;
[CurrentModel, CurrentRSS]=...
        bestDelayedARMAXModel(Data,...
        OldModel.na-ones(ny,ny), ...
        OldModel.nb-ones(ny,nu),...
        OldModel.nc-ones(ny,ny) ,nkoptions);
[TestRatio,Control]=testFischer(OldRSS,CurrentRSS,...
        1+nparams(OldModel),1+nparams(CurrentModel),N,P);

if TestRatio<Control
    disp(['odd model ',nameModel(CurrentModel),' wins by parsimony!'])
else
    disp(['even model: ',nameModel(OldModel),' would not have happened by chance!'])
    CurrentModel=OldModel;
    CurrentRSS=OldRSS;
end

%% Drop Input Order one at a time.
for y = 1:ny
    for u = 1:nu
        while 1
            OldModel=CurrentModel;
            OldRSS=CurrentRSS;
            mat=zeros(ny,nu);
            mat(y,u)=1;
            [CurrentModel, CurrentRSS]=...
                    bestDelayedARMAXModel(Data,...
                    OldModel.na, ...
                    OldModel.nb-mat,...
                    OldModel.nc,nkoptions);
            [TestRatio,Control]=testFischer(OldRSS,CurrentRSS,...
                    1+nparams(OldModel),1+nparams(CurrentModel),N,P);
            if TestRatio<Control
                disp(['smaller model ',nameModel(CurrentModel),' wins by parsimony!'])
            else
                disp(['model: ',nameModel(OldModel),' could not have happened by chance!'])
                CurrentModel=OldModel;
                CurrentRSS=OldRSS;
                break
            end
        end
    end
end

%% Drop MA Order one at a time.

while 1
    OldModel=CurrentModel;
    OldRSS=CurrentRSS;
    [CurrentModel, CurrentRSS]=...
            bestDelayedARMAXModel(Data,...
            OldModel.na, ...
            OldModel.nb, ...
            OldModel.nc-ones(ny,ny) ,nkoptions);
    [TestRatio,Control]=testFischer(OldRSS,CurrentRSS,...
            1+nparams(OldModel),1+nparams(CurrentModel),N,P);
    if TestRatio<Control
        disp(['smaller model ',nameModel(CurrentModel),' wins by parsimony!'])
    else
        disp(['model: ',nameModel(OldModel),' could not have happened by chance!'])
        CurrentModel=OldModel;
        CurrentRSS=OldRSS;
        break
    end
end
Model=CurrentModel;
Rss=CurrentRSS;