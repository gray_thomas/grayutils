import getData as gd
import numpy as np 
import matplotlib.pyplot as plt 
from plotting_functions import *
import statsmodels.api as sm

baseSaveFolder="/home/gray/wk/grayutils/ValkyrieAnkleTester/data/"

def print_data_ranges(ank):
    def print_dif(ank, what):
            print what, max(ank[what])-min(ank[what])
    print "time: it, and t"
    print 'des_x[0,:]', max(ank.des_x[0,:])-min(ank.des_x[0,:])
    print 'des_x[1,:]', max(ank.des_x[1,:])-min(ank.des_x[1,:])
    print_dif(ank, 'it')
    print_dif(ank, 't')
    print "inputs: t5, and t6"
    print_dif(ank, 't5')
    print_dif(ank, 't6')
    print "outputs: everyting else:"
    print_dif(ank, 'x5')
    print_dif(ank, 'xd5')
    print_dif(ank, 'e5')
    print_dif(ank, 'ex5')
    print_dif(ank, 'exd5')
    print_dif(ank, 'x6')
    print_dif(ank, 'xd6')
    print_dif(ank, 'e6')
    print_dif(ank, 'ex6')
    print_dif(ank, 'exd6')
    print_dif(ank, 'pitch')
    print_dif(ank, 'pitch_d')
    print_dif(ank, 'roll')
    print_dif(ank, 'roll_d')
    print_dif(ank, 'TS1')
    print_dif(ank, 'TS2')
    print_dif(ank, 'TS3')
    print_dif(ank, 'TS4')
    print_dif(ank, 'TS5')
    print_dif(ank, 'TS6')
    print_dif(ank, 'f5')
    print_dif(ank, 'f6')
from math import sin, cos, exp, log, pi
class Oscillator(object):
    def __init__(self, dt) :
            self.dT=dt
            self.value=0
            self.frequency=0

    def getNext(self):
        self.value += self.dT * self.frequency
        if (self.value >= 1.0):
            self.value -= 1.0
        if (self.value > 1.0):
            cerr << "overflow=" << self.value << endl
        return self.value

def oscTriangle(value):
    return  (2.0 * value) if (value < 0.5) else (2.0 - 2.0 * value)

def oscSine(value):
    return sin( value * 2 * pi);

def oscMappedSine(value, minv, maxv):
    return 0.5 * (maxv+ minv) + 0.5*oscSine(value) * (maxv - minv);

def oscMappedTriangle(value, minv, maxv):
    return minv + (maxv- minv) * oscTriangle(value)

def generate_command_sequence(n):
    osc1=Oscillator(0.001)
    osc2=Oscillator(0.001)
    osc3=Oscillator(0.001)
    osc4=Oscillator(0.001)
    osc1.frequency = 0.01;
    osc2.frequency = 0.2;
    osc3.frequency = 0.3;
    osc4.frequency = 1;
    minFreq = 0.1
    maxFreq = 10
    des_x=np.zeros((n,2))
    for i in range (0,n):
        frequency = exp(oscMappedTriangle(
            osc1.getNext(), log(minFreq), log(maxFreq)));
        osc2.frequency = 0.2*frequency;
        osc3.frequency = 0.3*frequency;
        des_x[i,:]=np.array([0.1*oscSine(osc2.getNext()),
                            0.1*oscSine(osc3.getNext())])
    return des_x



def recreate_control(des_x,pitch,pitch_vel,roll,roll_vel):
    print roll.shape
    q=np.array([roll, pitch])
    qd=np.array([roll_vel, pitch_vel])
    print q.shape
    Jsensing=np.eye(2)
    
    Jtorque=np.array([[0.75, 0.75],
                    [0.75, -0.75]])

    JtorqueInv=np.array([[0.75,0.75],
                        [0.75, -0.75]])
    x = Jsensing.dot(q)
    xd = Jsensing.dot(qd)
    
    ## gains from commit 53b01fa156145fa687c939b918499a70e6b12500
    Kdx_0 = 20.0
    Kdx_1 = 10.0

    Kd = np.array([[ Kdx_0, 0.0],
                    [ 0.0, Kdx_1]])

    Kpx_0 = 3600
    Kpx_1 = 1800
    Kp = np.array([ [Kpx_0,0.0],
                    [0.0,Kpx_1]])

    gravity_comp = np.array([   [ -87.46, -106.25],
                                [-71.83,  98.26]])

    gravity_bias =np.array([[ -6.42, -2.10]]).T

    grav_est = np.diagflat(gravity_bias).dot(np.ones(q.shape))+gravity_comp.dot(q)
    ag_factor = 0.0 
    anti_grav = gravity_bias + (1.0+ag_factor)*gravity_comp.dot(q)

    anti_grav-=ag_factor*gravity_comp.dot(des_x.T)



    des_accel = - Kp.dot(x-des_x.T) - Kd.dot(xd)
    gamma_desired = JtorqueInv.dot(des_accel)

    command5 = anti_grav[0,:] + gamma_desired[0,:]
    command6 = anti_grav[1,:] + gamma_desired[1,:]

    saturation=120.0
    for i in range(0,command5.shape[0]):
        if (command5[i] > saturation):
            command5[i]=saturation
        if (command5[i]< -saturation):
            command5[i]=-saturation
        if (command6[i] > saturation):
            command6[i]=saturation
        if (command6[i] < -saturation):
            command6[i]=-saturation

    return command5, command6, anti_grav, des_accel, gamma_desired, x, xd, q, qd

def compare_to_forward_generation(ank,lo,hi,sk,q,qd,gravity_estimate,x,xd,des_x):
    # recompute everything going forward
    des_x2 = generate_command_sequence(hi)
    des_xf =    des_x2[lo:hi:sk]
    pitch =     ank.pitch[lo:hi:sk]
    pitch_vel = ank.pitch_d[lo:hi:sk]
    roll =      ank.roll[lo:hi:sk]
    roll_vel =  ank.roll_d[lo:hi:sk]
    comm5, comm6, anti_grav, des_accelf, gamma_desiredf, xf, xdf ,qf,qdf = \
        recreate_control(des_xf, pitch, pitch_vel, roll, roll_vel)

    plt.plot(ank.t[lo:hi:sk],anti_grav.T)
    plt.plot(ank.t[lo:hi:sk],gravity_estimate.T)
    plt.title("reconstruction of anti-gravity")

    fig=plt.figure(figsize=(24,12))
    ax1=fig.add_subplot(411)
    ax1.plot(ank.t[lo:hi:sk],qf.T)
    ax1.plot(ank.t[lo:hi:sk],q.T)
    ax1.set_title("reconstruction of q")

    ax2=fig.add_subplot(412,sharex=ax1)
    ax2.plot(ank.t[lo:hi:sk],xf.T)
    ax2.plot(ank.t[lo:hi:sk],x.T)
    ax2.set_title("reconstruction of x")

    # figure to demonstrate that the reconstructed command is good
    ax3=fig.add_subplot(413,sharex=ax1)
    ax3.plot(ank.t[lo:hi:sk],comm5,label="recreation 5")
    ax3.plot(ank.t[lo:hi:sk],ank.t5[lo:hi:sk],label="5 from file")
    ax3.plot(ank.t[lo:hi:sk],comm6,label="recreation 6")
    ax3.plot(ank.t[lo:hi:sk],ank.t6[lo:hi:sk],label="6 from file")
    ax3.set_title("Test of command reconstruction")
    ax3.legend()

    ax4=fig.add_subplot(414,sharex=ax1)
    ax4.plot(ank.t[lo:hi:sk],des_x.T)
    ax4.plot(ank.t[lo:hi:sk],des_x2[lo:hi:sk])
    ax4.set_title("reconstruction of desired x position")
    fig.tight_layout() 

def backcalculate_desired_position(ank,region,sk=1):
    lo=region[0]
    hi=region[1]
    torque_jacobian=np.array([[0.75,0.75],[0.75,-0.75]])
    JtorqueInv=np.array([[0.75,0.75],
                        [0.75, -0.75]])
    torque_jacobian_inverse=np.array([[0.75,0.75],[0.75,-0.75]])
    Jsensing=np.eye(2)
    q=np.array([ank.roll[lo:hi:sk],ank.pitch[lo:hi:sk]])
    qd=np.array([ank.roll_d[lo:hi:sk],ank.pitch_d[lo:hi:sk]])
    x=Jsensing.dot(q)
    xd=Jsensing.dot(qd)
    Kd=np.array([[20.0,0.0],[0.0,10.0]])
    Kp=np.array([[3600.0,0.0],[0.0,1800.0]])
    gravity_comp=np.array([[-87.46, -106.25],[-71.83,  98.26]])
    gravity_bias=np.array([[-6.42], [-2.10]])
    gravity_estimate=np.diagflat(gravity_bias).dot(np.ones(q.shape))+gravity_comp.dot(q)
    
    command = np.array([ank.tau5[lo:hi:sk],ank.tau6[lo:hi:sk]])
    gamma_desired= command-gravity_estimate
    des_accel = np.linalg.inv(torque_jacobian_inverse).dot(gamma_desired)
    # des_accel = - Kp.dot(x)+Kp.dot(des_x.T) - Kd.dot(xd)
    des_x = x+np.linalg.inv(Kp).dot( des_accel + Kd.dot(xd) )
    ank['des_x']=des_x
    return q, qd, x, xd, gravity_comp, command, gamma_desired, des_accel, des_x

def save_data(ank,region,filename,sk=1):
    lo=region[0]
    hi=region[1]
    # print_data_ranges(ank)

    xs=np.array([
        ank.des_x[0,lo:hi:sk],
        ank.des_x[1,lo:hi:sk],
        ank.t5[lo:hi:sk], # command force
        ank.t6[lo:hi:sk],
        ank.x5[lo:hi:sk], # position
        ank.x6[lo:hi:sk],
        ank.xd5[lo:hi:sk],
        ank.xd6[lo:hi:sk],
        ank.e5[lo:hi:sk], # achieved force, with a different filter from the other force
        ank.e6[lo:hi:sk],
        ank.ex5[lo:hi:sk], # incremental encoders
        ank.ex6[lo:hi:sk],
        ank.exd5[lo:hi:sk],
        ank.exd6[lo:hi:sk],
        ank.pitch[lo:hi:sk], # angular positions and velocities
        ank.roll[lo:hi:sk],
        ank.pitch_d[lo:hi:sk],
        ank.roll_d[lo:hi:sk],
        ank.TS1[lo:hi:sk], # six axis force torque sensors
        ank.TS2[lo:hi:sk],
        ank.TS3[lo:hi:sk],
        ank.TS4[lo:hi:sk],
        ank.TS5[lo:hi:sk],
        ank.TS6[lo:hi:sk],
        ank.f5[lo:hi:sk], # force sensors
        ank.f6[lo:hi:sk]
        ]).T
    header_string="des_x des_x t5 t6 x5 x6 xd5 xd6 e5 e6 ex5 ex6 exd5 exd6 pitch roll pitch_d roll_d TS1 TS2 TS3 TS4 TS5 TS6 f5 f6"
    print ank.des_x.shape
    # for i in range (0, len(xs)):
    #     print xs[i].shape
    print xs.shape
    np.savetxt(filename,xs, header=header_string, comments="")

    # # armax = sm.tsa.ARMA(xs[0,:], order=(1, 1), exog=u).fit()
    # armax = sm.tsa.ARMA(xs[:,0], order=(2, 1)).fit()

if __name__=="__main__":
    dataSetNumber=35 ## gains from commit 53b01fa156145fa687c939b918499a70e6b12500
    ank=gd.AnkleData("data%d.dat"%dataSetNumber,"/home/gray/wk/largeDataFiles/")
    
    # plt.plot(ank.t[1:]-ank.t[:-1])# use to identify pauses and 
    # regions of nearly-even sampling intervals.
    all_the_data=[0,181946]
    semi_clean_region1=[5050,35080]
    semi_clean_region2=[35260,65140]
    semi_clean_region3=[65280,95180]
    semi_clean_region4=[95320,125500]

    backcalculate_desired_position(ank,all_the_data, sk=1) # takes 0.1 second
    save_data(ank,semi_clean_region2,"pre-processed_data.txt", sk=10)
    save_data(ank,semi_clean_region1,"pre-processed_data1.txt", sk=10)
    save_data(ank,semi_clean_region2,"pre-processed_data2.txt", sk=10)
    save_data(ank,semi_clean_region3,"pre-processed_data3.txt", sk=10)
    save_data(ank,semi_clean_region4,"pre-processed_data4.txt", sk=10)
    save_data(ank,semi_clean_region1,"s1_pre-processed_data1.txt", sk=1)
    save_data(ank,semi_clean_region2,"s1_pre-processed_data2.txt", sk=1)
    save_data(ank,semi_clean_region3,"s1_pre-processed_data3.txt", sk=1)
    save_data(ank,semi_clean_region4,"s1_pre-processed_data4.txt", sk=1)
    # save_data(ank,semi_clean_region5,"pre-processed_data5.txt", sk=10)
    plt.show()