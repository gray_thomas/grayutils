%% Define some colors
utburntorange=(1/256)*[ 191, 87, 0];


utblue=(1/256)*[0, 95, 134];
utblue75=(1/256)*[60, 135, 163];
utblue50=(1/256)*[126, 175, 197];
utblue25=(1/256)*[190, 215, 225];

utgreen=(1/256)*[67, 105, 91];
utgreen75=(1/256)*[115, 142, 131];
utgreen50=(1/256)*[162, 179, 173];
utgreen25=(1/256)*[208, 217, 214];

utyellow=(1/256)*[242, 169, 0 ];
utyellow75=(1/256)*[247, 189, 89  ];
utyellow50=(1/256)*[250, 211, 146 ];
utyellow25=(1/256)*[253, 233, 200 ];

utblack=(1/256)*[51 63 72];
utblack75=(1/256)*[102, 110, 117];
utblack50=(1/256)*[153, 158, 163];
utblack25=(1/256)*[204, 207, 209];

utbrown=(1/256)*[56, 47, 45];
utbrown75=(1/256)*[ 106, 99, 97];
utbrown50=(1/256)*[156, 151, 150];
utbrown25=(1/256)*[205, 203, 202];

utivory=(1/256)*[214, 210, 196];
utivory75=(1/256)*[225, 221, 210 ];
utivory50=(1/256)*[235, 232, 226];
utivory25=(1/256)*[245, 244, 240];

colorwheel1=[utburntorange;utyellow;utblue50;utbrown25];