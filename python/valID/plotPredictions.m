function plotPredictions(Data,model,name,title2)
utColors

%% Predictions

pred100=predict(model,Data,100);
pred10=predict(model,Data,10);
pred1=predict(model,Data,1);

fig=figure();
plot(Data.y,'Color',utblack,'linewidth',4)
hold on
plot(pred1.y,'Color',utblue,'linewidth',3)
plot(pred10.y,'Color',utburntorange,'linewidth',3)
plot(pred100.y,'Color',utyellow,'linewidth',3)
legend({'$X_t$','$X_t(1)$', '$X_t(10)$','$X_t(100)$'},'interpreter', 'latex')
title(title2,'interpreter', 'latex')
figuresize(16,12,'centimeters')
fname=[ name 'preds'];
print(['../../tex/time_series_final_project/' fname],'-dpdf','-r300');
% close(fig.Number)
fprintf(['{ %% Predictions for %s\n',...
	'\t\\usebackgroundtemplate{}\n',...
    '\t\\setbeamertemplate{navigation symbols}{}\n',...
    '\t\\begin{frame}[plain]\n',...
    '\t\t    \\begin{tikzpicture}[remember picture,overlay]\n',...
    '\t\t\t        \\node[at=(current page.center)] {\n',...
    '\t\t\t\t            \\includegraphics[width=\\paperwidth]{%s}\n',...
    '\t\t\t        };\n',...
    '\t\t    \\end{tikzpicture}\n',...
    '\t \\end{frame}\n',...
    '}\n'],nameModel(model),fname)

end