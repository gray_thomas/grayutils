import getData as gd
import numpy as np 
import matplotlib.pyplot as plt 

dataSetNumber=9
ank=gd.AnkleData("data%d.dat"%dataSetNumber,"/home/gray/wk/largeDataFiles/")
baseSaveFolder="/home/gray/wk/grayutils/ValkyrieAnkleTester/data/"
def forces_plot(lo=0, hi=5000, sk=1):
    fig, axes = plt.subplots(3, sharex=True)
    axes[0].plot(1e-6*ank.t[lo:hi:sk], ank.tau6[lo:hi:sk])
    axes[0].set_ylabel("current (A)")
    axes[1].plot(1e-6*ank.t[lo:hi:sk], ank.f6[lo:hi:sk])
    axes[1].set_ylabel("force (N)")
    axes[2].plot(1e-6*ank.t[lo:hi:sk], ank.x6[lo:hi:sk])
    axes[2].set_xlabel("time (s)")
    axes[2].set_ylabel("pos (m)")

def big_plot(lo=0, hi=5000, sk=1):
    fig, axes = plt.subplots(7, sharex=True)
    axes[0].plot(1e-6*ank.t[lo:hi:sk], ank.tau6[lo:hi:sk])
    axes[0].set_ylabel("current (A)")
    axes[1].plot(1e-6*ank.t[lo:hi:sk], ank.f6[lo:hi:sk])
    axes[1].set_ylabel("force (N)")
    axes[2].plot(1e-6*ank.t[lo:hi:sk], ank.x6[lo:hi:sk])
    axes[2].set_ylabel("pos (m)")
    axes[3].plot(1e-6*ank.t[lo:hi:sk], ank.xd6[lo:hi:sk])
    axes[3].set_ylabel("vel (m/s)")
    axes[4].plot(1e-6*ank.t[lo:hi:sk], ank.TS1[lo:hi:sk])
    axes[4].set_ylabel("TS1 (?)")
    axes[5].plot(1e-6*ank.t[lo:hi:sk], ank.TS2[lo:hi:sk])
    axes[5].set_ylabel("TS2 (?)")
    axes[6].plot(1e-6*ank.t[lo:hi:sk], ank.TS3[lo:hi:sk])
    axes[6].set_ylabel("TS3 (?)")
    axes[6].set_xlabel("time (s)")

def force_torque_plot(lo=0, hi=-1, sk=1):
    cal_mat=np.array([
        [-171.138762092584, 164.262330318272, 1136.66933015971, -25528.3827006466, -892.147266185941, 25018.097320641],
        [-2393.11261214503, 29302.8374381793, 176.909372134555, -14538.0957864741, 1032.08177266801, -14648.9548591062 ],
        [41139.1938396664, 518.608163331987, 40227.7537078209, 385.007116653545, 41350.2621211616, 447.58447722459 ],
        [-21.8401291003204, 421.344052448737, -872.343264955442, -221.911677460517, 886.648930652221, -196.26924129588 ],
        [996.949904915404, 14.3104375478289, -505.787839958358, 358.430597717003, -495.101373345106, -364.002555342701 ],
        [33.9397030997519, -469.070679591882, 21.1841848707929, -445.411314827181, 16.1145946572983, -437.463633877703 ]])
    gauge_gains=np.array([75, 75, 75, 73, 75, 73])
    gauge_offsets=np.array([32858, 31310, 32988, 29358, 33420, 30796])
    scale_factors=np.array([51882.0, 51882.0, 51882.0, 916.0, 916.0, 916.0])
    raw=np.concatenate([ank.TS1.reshape((-1,1)),ank.TS2.reshape((-1,1)),ank.TS3.reshape((-1,1)),
                        ank.TS4.reshape((-1,1)),ank.TS5.reshape((-1,1)),ank.TS6.reshape((-1,1))], axis=1)
    bias=np.ones(raw.shape).dot(np.diagflat(gauge_offsets))
    print raw.shape, np.diagflat(gauge_offsets).shape, bias.shape, cal_mat.shape
    ank['force_torque'] = (raw-bias).dot(cal_mat.T) * 1.0/208000000.0
    fig, axes = plt.subplots(8, sharex=True)
    axes[0].plot(1e-6*ank.t[lo:hi:sk], ank.tau5[lo:hi:sk])
    axes[0].set_ylabel(r"desired force 5 (Newtons)")
    axes[1].plot(1e-6*ank.t[lo:hi:sk], ank.tau6[lo:hi:sk])
    axes[1].set_ylabel(r"desired force 6 (Newtons)")
    axes[2].plot(1e-6*ank.t[lo:hi:sk], ank.force_torque[lo:hi:sk,0])
    axes[2].set_ylabel(r"f_x (N)")
    axes[3].plot(1e-6*ank.t[lo:hi:sk], ank.force_torque[lo:hi:sk,1])
    axes[3].set_ylabel(r"f_y (N)")
    axes[4].plot(1e-6*ank.t[lo:hi:sk], ank.force_torque[lo:hi:sk,2])
    axes[4].set_ylabel(r"f_z (N)")
    axes[5].plot(1e-6*ank.t[lo:hi:sk], ank.force_torque[lo:hi:sk,3])
    axes[5].set_ylabel(r"\tau_x (Nm)")
    axes[6].plot(1e-6*ank.t[lo:hi:sk], ank.force_torque[lo:hi:sk,4])
    axes[6].set_ylabel(r"\tau_y (Nm)")
    axes[7].plot(1e-6*ank.t[lo:hi:sk], ank.force_torque[lo:hi:sk,5])
    axes[7].set_ylabel(r"\tau_z (Nm)")

    plt.savefig(baseSaveFolder+"data%d_force_torque_plot.png"%dataSetNumber, dpi=600, facecolor='w', edgecolor='w',
        orientation='portrait', papertype='letter', format='png',
        transparent=True, bbox_inches='tight', pad_inches=0.4)
    plt.savefig(baseSaveFolder+"data%d_force_torque_plot.pdf"%dataSetNumber, dpi=600, facecolor='w', edgecolor='w',
        orientation='portrait', papertype='letter', format='pdf',
        transparent=True, bbox_inches='tight', pad_inches=0.4)

def gen_ss_model(lo=0, hi=5000, sk=1):
    inputs=[""]
    sensors=["eff5,eff6,f5,f6,x5,x6,xd5,xd6,force_torque"]
    ank.eff6[lo:hi]
    ank.f6[lo:hi]

def spring6_plot(lo=0, hi=5000, sk=1):
    fig, axes = plt.subplots(1, sharex=True)
    axes.plot(ank.eff6[lo:hi],ank.f6[lo:hi],'.')
    # axes.plot(ank.x6[lo:hi],ank.f6[lo:hi],'.')
    axes.set_xlabel("position (m)")
    axes.set_ylabel("force sensor force(N)")
    axes.grid(True)
    plt.savefig(baseSaveFolder+"data%d_spring6_plot_lo%d_hi%d_sk%d.png"%(dataSetNumber,lo,hi,sk), 
        dpi=600, facecolor='w', edgecolor='w',
        orientation='portrait', papertype='letter', format='png',
        transparent=True, bbox_inches='tight', pad_inches=0.4)
    plt.savefig(baseSaveFolder+"data%d_spring6_plot_lo%d_hi%d_sk%d.pdf"%(dataSetNumber,lo,hi,sk), 
        dpi=600, facecolor='w', edgecolor='w',
        orientation='portrait', papertype='letter', format='pdf',
        transparent=True, bbox_inches='tight', pad_inches=0.4)

def spring5_plot(lo=0, hi=5000, sk=1):
    fig, axes = plt.subplots(1, sharex=True)
    axes.plot(ank.eff5[lo:hi],ank.f5[lo:hi],'.')
    # axes.plot(ank.x5[lo:hi],ank.f5[lo:hi],'.')
    axes.set_xlabel("position (m)")
    axes.set_ylabel("force sensor force(N)")
    axes.grid(True)
    plt.savefig(baseSaveFolder+"data%d_spring5_plot_lo%d_hi%d_sk%d.png"%(dataSetNumber,lo,hi,sk), 
        dpi=600, facecolor='w', edgecolor='w',
        orientation='portrait', papertype='letter', format='png',
        transparent=True, bbox_inches='tight', pad_inches=0.4)
    plt.savefig(baseSaveFolder+"data%d_spring5_plot_lo%d_hi%d_sk%d.pdf"%(dataSetNumber,lo,hi,sk), 
        dpi=600, facecolor='w', edgecolor='w',
        orientation='portrait', papertype='letter', format='pdf',
        transparent=True, bbox_inches='tight', pad_inches=0.4)

def timing_plot():
    fig, axes = plt.subplots(1, sharex=True)
    axes.plot(1e-6*ank.t[:-1], (ank.t[1:]-ank.t[:-1])/(ank.iters[1:]-ank.iters[:-1]))
    axes.set_xlabel("time (s)")
    axes.set_ylabel(r"$\mu \mathrm{s} / \rm{tic} $")
    axes.grid(True)
    plt.savefig(baseSaveFolder+"data%d_timing_plot.png"%dataSetNumber, dpi=600, facecolor='w', edgecolor='w',
        orientation='portrait', papertype='letter', format='png',
        transparent=True, bbox_inches='tight', pad_inches=0.4)
    plt.savefig(baseSaveFolder+"data%d_timing_plot.pdf"%dataSetNumber, dpi=600, facecolor='w', edgecolor='w',
        orientation='portrait', papertype='letter', format='pdf',
        transparent=True, bbox_inches='tight', pad_inches=0.4)

def fault_plot(lo,hi):
    fig, axes = plt.subplots(4, sharex=True)
    axes[0].plot(1e-6*ank.t[lo:hi], ank.faults5[lo:hi])
    axes[0].set_ylabel(r"$\mathrm{faults}_5$")
    axes[1].plot(1e-6*ank.t[lo:hi], ank.faults6[lo:hi])
    axes[1].set_ylabel(r"$\mathrm{faults}_6$")
    axes[2].plot(1e-6*ank.t[lo:hi], ank.incX5[lo:hi])
    axes[2].set_ylabel(r"$x_{\mathrm{inc},5}$")
    axes[3].plot(1e-6*ank.t[lo:hi], ank.incX6[lo:hi])
    axes[3].set_ylabel(r"$x_{\mathrm{inc},6}$")
    axes[3].set_xlabel("time (s)")
    # axes.grid(True)
    plt.savefig(baseSaveFolder+"data%d_fault_plot.png"%dataSetNumber, dpi=600, facecolor='w', edgecolor='w',
        orientation='portrait', papertype='letter', format='png',
        transparent=True, bbox_inches='tight', pad_inches=0.4)
    plt.savefig(baseSaveFolder+"data%d_fault_plot.pdf"%dataSetNumber, dpi=600, facecolor='w', edgecolor='w',
        orientation='portrait', papertype='letter', format='pdf',
        transparent=True, bbox_inches='tight', pad_inches=0.4)



# forces_plot(0,5000,100)
big_plot(0,50000,100)
# spring6_plot(10000,200000,10)
# spring5_plot(10000,200000,10)
# fault_plot(-20000,-1)
# force_torque_plot()
# timing_plot()
plt.show()