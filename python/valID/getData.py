import numpy as np 
import io

goodNames={}
goodNames['iter']=['iteration','iter','iters','it']
goodNames['time_us']=['t','time','micro_time']
for j in [5,6]:
	goodNames['act%d_current_des'%j]=['current%d'%j,'i%d'%j,'cur_%d'%j,'cur%d'%j]
	goodNames['act%d_torque_des'%j]=['torque%d'%j,'t%d'%j,'tau_%d'%j,'tau%d'%j]
	goodNames['val.rightAnkle.getActuator%d().getPosition()'%j]=["pos%d"%j,"pos_%d"%j,"x%d"%j]
	goodNames['val.rightAnkle.getActuator%d().getVelocity()'%j]=["vel%d"%j,"vel_%d"%j,"xd%d"%j]
	goodNames['val.rightAnkle.getActuator%d().getEffort()'%j]=["eff%d"%j,"eff_%d"%j,"e%d"%j]
	goodNames['val.rightAnkle.getActuator%d().getIncrementalPosition()'%j]=["incX%d"%j,"inc_pos_%d"%j,"ex%d"%j]
	goodNames['val.rightAnkle.getActuator%d().getIncrementalVelocity()'%j]=["incXd%d"%j,"inc_vel_%d"%j,"exd%d"%j]
	goodNames['val.rightAnkle.getActuator%d().getFaults()'%j]=["faults%d"%j]
	goodNames['val.rightAnkle.getForce%d().force.get<float>()'%j]=["force%d"%j,"force_%d"%j,"f%d"%j]

	goodNames['test_ankle.getActuator%d().getPosition()'%j]=["pos%d"%j,"pos_%d"%j,"x%d"%j]
	goodNames['test_ankle.getActuator%d().getVelocity()'%j]=["vel%d"%j,"vel_%d"%j,"xd%d"%j]
	goodNames['test_ankle.getActuator%d().getEffort()'%j]=["eff%d"%j,"eff_%d"%j,"e%d"%j]
	goodNames['test_ankle.getActuator%d().getIncrementalPosition()'%j]=["incX%d"%j,"inc_pos_%d"%j,"ex%d"%j]
	goodNames['test_ankle.getActuator%d().getIncrementalVelocity()'%j]=["incXd%d"%j,"inc_vel_%d"%j,"exd%d"%j]
	goodNames['test_ankle.getActuator%d().getFaults()'%j]=["faults%d"%j]
	goodNames['test_ankle.getForce%d().force.get<float>()'%j]=["force%d"%j,"force_%d"%j,"f%d"%j]


goodNames['val.rightAnkle.getPitchJoint().getPosition()']=['pitch_o','pitch']
goodNames['val.rightAnkle.getPitchJoint().getVelocity()']=['pitch_od','pitch_d', 'pitchDot']
goodNames['val.rightAnkle.getRollJoint().getPosition()']=['roll_o','roll']
goodNames['val.rightAnkle.getRollJoint().getVelocity()']=['roll_od','roll_d', 'rollDot']

goodNames['test_ankle.getPitchJoint().getPosition()']=['pitch_o','pitch']
goodNames['test_ankle.getPitchJoint().getVelocity()']=['pitch_od','pitch_d', 'pitchDot']
goodNames['test_ankle.getRollJoint().getPosition()']=['roll_o','roll']
goodNames['test_ankle.getRollJoint().getVelocity()']=['roll_od','roll_d', 'rollDot']

for a in [1,2,3,4,5,6]:
	goodNames['val.rightAnkle.getFootLoadcell().a%d.get<float>()'%a]=['TS%d'%a]
	goodNames['test_ankle.getFootLoadcell().a%d.get<float>()'%a]=['TS%d'%a]


"""iter, time_us, act6_current_des, val.rightAnkle.getActuator6().getPosition(), val.rightAnkle.getActuator6().getVelocity(), 
val.rightAnkle.getActuator6().getEffort(), val.rightAnkle.getPitchJoint().getPosition(), 
val.rightAnkle.getPitchJoint().getVelocity(), val.rightAnkle.getRollJoint().getPosition(),
 val.rightAnkle.getRollJoint().getVelocity(), val.rightAnkle.getFootLoadcell().a1.get<float>(), 
 val.rightAnkle.getFootLoadcell().a2.get<float>(), val.rightAnkle.getFootLoadcell().a3.get<float>(),
  val.rightAnkle.getFootLoadcell().a4.get<float>(), val.rightAnkle.getFootLoadcell().a5.get<float>(), 
  val.rightAnkle.getFootLoadcell().a6.get<float>(), 
  val.rightAnkle.getForce5().force.get<float>(), val.rightAnkle.getForce6().force.get<float>()"
"""
class AnkleData:
	# nameing_lookup={}
	# for name in goodNames.keys():
	# 	for goodName in goodNames[name]:
	# 		nameing_lookup[goodName]=name


	def __init__(self, fileName, base_folder="/home/gray/wk/grayutils/ValkyrieAnkleTester/data/"):
		self.base_folder=base_folder
		fullPath=self.base_folder+fileName
		# print fullPath
		self.data=np.loadtxt(fullPath, skiprows=1, delimiter=', ')
		firstLineReader=io.FileIO(fullPath)
		self.lookupIndex={}
		# self['pos6']=self.data[:,3]
		for (i,name) in enumerate(firstLineReader.readline().split(', ')):
			name=name.strip()
			try:
				print i, "is", goodNames[name]
				for better in goodNames[name]:
					self.lookupIndex[better]=i
					setattr(AnkleData, better, self[better])
			except KeyError as e:
				print name, "has no alternate names"
	def __setitem__(self, item, value):
		self.__dict__[item]=value
	def __getitem__(self, key):
		try:
			return self.data[:,self.lookupIndex[key]]
		except KeyError:
			return self.__dict__[key]

	
	
def main():
	fileName="data3.dat"
	ankDat=AnkleData(fileName)
	print "shape of ankDat.pos6 =" , ankDat.pos6.shape

if __name__=="__main__":
	main()