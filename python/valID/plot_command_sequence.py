from data_pre_processing import generate_command_sequence, backcalculate_desired_position
import matplotlib.pyplot as plt
import getData as gd
import numpy as np 

def plot_input():
    N=100000
    des_x = generate_command_sequence(N)
    fig=plt.figure(figsize=(8,6),facecolor='w')
    ax=fig.add_subplot(211)
    ax.set_title("Setpoint Excitation Signal", family='monospace',fontsize=24)
    ax.plot(des_x[:,0],des_x[:,1],color='#333f48',lw=3)
    ax.set_xlabel(r"$x_{d,0}$ $(\approx rad)$", fontsize=24)
    ax.set_ylabel(r"$x_{d,1}$ $(\approx rad)$", fontsize=24)
    ax2=fig.add_subplot(212)
    ax2.plot(0.001*np.array(range(0,N)),des_x[:,0],color='#333f48',lw=3)
    ax2.plot(0.001*np.array(range(0,N)),des_x[:,1],color='#bf5700',lw=3)
    ax2.set_xlabel(r"$t$ $(s)$", fontsize=24)
    ax2.set_ylabel(r"$x_d$ $(\approx rad)$", fontsize=24)
    fig.tight_layout()
    fig.savefig("../../tex/time_series_final_project/command_sequence.pdf")
    print des_x.shape

def plot_measured_x_reg2():
    dataSetNumber=35 ## gains from commit 53b01fa156145fa687c939b918499a70e6b12500
    ank=gd.AnkleData("data%d.dat"%dataSetNumber,"/home/gray/wk/largeDataFiles/")
    semi_clean_region2=[35260,65140]
    q, qd, x, xd, gravity_comp, commmand, gamma_desired, des_accel, des_x = backcalculate_desired_position(ank, semi_clean_region2, sk=1)
    # des_x = generate_command_sequence(N)
    fig=plt.figure(figsize=(8,6),facecolor='w')
    ax=fig.add_subplot(211)
    ax.set_title("Achieved Position Signal", family='monospace',fontsize=24)
    ax.plot(x[0,:],x[1,:],'.',color='#333f48',lw=3, alpha=0.3)
    ax.set_xlabel(r"$\hat {x_0}$ $(\approx rad)$", fontsize=24)
    ax.set_ylabel(r"$\hat {x_1}$ $(\approx rad)$", fontsize=24)
    ax2=fig.add_subplot(212)
    t=0.001*np.array(range(semi_clean_region2[0],semi_clean_region2[1]))
    ax2.plot(t,x[0,:],color='#333f48',lw=3)
    ax2.plot(t,x[1,:],color='#bf5700',lw=3)
    ax2.set_xlabel(r"$t$ $(s)$", fontsize=24)
    ax2.set_ylabel(r"$\hat {x}$ $(\approx rad)$", fontsize=24)
    fig.tight_layout()
    fig.savefig("../../tex/time_series_final_project/achieved_x_reg2.pdf")
    print des_x.shape

def plot_force_command_reg2():
    dataSetNumber=35 ## gains from commit 53b01fa156145fa687c939b918499a70e6b12500
    ank=gd.AnkleData("data%d.dat"%dataSetNumber,"/home/gray/wk/largeDataFiles/")
    semi_clean_region2=[35260,65140]
    q, qd, x, xd, gravity_comp, commmand, gamma_desired, des_accel, des_x = backcalculate_desired_position(ank, semi_clean_region2, sk=1)
    # des_x = generate_command_sequence(N)
    fig=plt.figure(figsize=(8,6),facecolor='w')
    ax=fig.add_subplot(211)
    ax.set_title("Force Command Signal", family='monospace',fontsize=24)
    ax.plot(commmand[0,:],commmand[1,:],'.',color='#333f48',lw=3, alpha=0.3)
    ax.set_xlabel(r"$\hat {f_{c,0}}$ $(N)$", fontsize=24)
    ax.set_ylabel(r"$\hat {f_{c,1}}$ $(N)$", fontsize=24)
    ax2=fig.add_subplot(212)
    t=0.001*np.array(range(semi_clean_region2[0],semi_clean_region2[1]))
    ax2.plot(t,commmand[0,:],color='#333f48',lw=3)
    ax2.plot(t,commmand[1,:],color='#bf5700',lw=3)
    ax2.set_xlabel(r"$t$ $(s)$", fontsize=24)
    ax2.set_ylabel(r"$\hat {f_c}$ $(N)$", fontsize=24)
    fig.tight_layout()
    fig.savefig("../../tex/time_series_final_project/command_reg2.pdf")
    print des_x.shape

def plot_force_command_reg1():
    dataSetNumber=35 ## gains from commit 53b01fa156145fa687c939b918499a70e6b12500
    ank=gd.AnkleData("data%d.dat"%dataSetNumber,"/home/gray/wk/largeDataFiles/")
    semi_clean_region1=[5050,35080]
    q, qd, x, xd, gravity_comp, commmand, gamma_desired, des_accel, des_x = backcalculate_desired_position(ank, semi_clean_region1, sk=1)
    # des_x = generate_command_sequence(N)
    fig=plt.figure(figsize=(8,6),facecolor='w')
    ax=fig.add_subplot(211)
    ax.set_title("Force Command Signal", family='monospace',fontsize=24)
    ax.plot(commmand[0,:],commmand[1,:],'.',color='#333f48',lw=3, alpha=0.3)
    ax.set_xlabel(r"$\hat {f_{c,0}}$ $(N)$", fontsize=24)
    ax.set_ylabel(r"$\hat {f_{c,1}}$ $(N)$", fontsize=24)
    ax2=fig.add_subplot(212)
    t=0.001*np.array(range(semi_clean_region1[0],semi_clean_region1[1]))
    ax2.plot(t,commmand[0,:],color='#333f48',lw=3)
    ax2.plot(t,commmand[1,:],color='#bf5700',lw=3)
    ax2.set_xlabel(r"$t$ $(s)$", fontsize=24)
    ax2.set_ylabel(r"$\hat {f_c}$ $(N)$", fontsize=24)
    fig.tight_layout()
    fig.savefig("../../tex/time_series_final_project/command_reg1.pdf")
    print des_x.shape

def plot_saturated_force_input_region1():
    dataSetNumber=35 ## gains from commit 53b01fa156145fa687c939b918499a70e6b12500
    ank=gd.AnkleData("data%d.dat"%dataSetNumber,"/home/gray/wk/largeDataFiles/")
    semi_clean_region1=[5050,35080]
    q, qd, x, xd, gravity_comp, commmand, gamma_desired, des_accel, des_x = backcalculate_desired_position(ank, semi_clean_region1, sk=1)
    # des_x = generate_command_sequence(N)
    fig=plt.figure(figsize=(8,6),facecolor='w')
    ax=fig.add_subplot(211)
    ax.set_title("Achieved Position Signal", family='monospace',fontsize=24)
    ax.plot(x[0,:],x[1,:],'.',color='#333f48',lw=3, alpha=0.3)
    ax.set_xlabel(r"$\hat {x_0}$ $(\approx rad)$", fontsize=24)
    ax.set_ylabel(r"$\hat {x_1}$ $(\approx rad)$", fontsize=24)
    ax2=fig.add_subplot(212)
    t=0.001*np.array(range(semi_clean_region1[0],semi_clean_region1[1]))
    ax2.plot(t,x[0,:],color='#333f48',lw=3)
    ax2.plot(t,x[1,:],color='#bf5700',lw=3)
    ax2.set_xlabel(r"$t$ $(s)$", fontsize=24)
    ax2.set_ylabel(r"$\hat {x}$ $(\approx rad)$", fontsize=24)
    fig.tight_layout()
    fig.savefig("../../tex/time_series_final_project/achieved_x_1.pdf")
    print des_x.shape

def plot_saturated_command_position_region2():
    dataSetNumber=35 ## gains from commit 53b01fa156145fa687c939b918499a70e6b12500
    ank=gd.AnkleData("data%d.dat"%dataSetNumber,"/home/gray/wk/largeDataFiles/")
    semi_clean_region2=[35260,65140]
    q, qd, x, xd, gravity_comp, commmand, gamma_desired, des_accel, des_x = backcalculate_desired_position(ank, semi_clean_region2, sk=1)
    # des_x = generate_command_sequence(N)
    fig=plt.figure(figsize=(8,6),facecolor='w')
    ax=fig.add_subplot(211)
    ax.set_title("Backcalculated Desired Position", family='monospace',fontsize=24)
    ax.plot(des_x[0,:],des_x[1,:],'.',color='#333f48',lw=3, alpha=0.05)
    ax.set_xlabel(r"$\tilde {x_{d,0}}$ $(\approx rad)$", fontsize=24)
    ax.set_ylabel(r"$\tilde {x_{d,1}}$ $(\approx rad)$", fontsize=24)
    ax2=fig.add_subplot(212)
    t=0.001*np.array(range(semi_clean_region2[0],semi_clean_region2[1]))
    ax2.plot(t,des_x[0,:],color='#333f48',lw=3)
    ax2.plot(t,des_x[1,:],color='#bf5700',lw=3)
    ax2.set_xlabel(r"$t$ $(s)$", fontsize=24)
    ax2.set_ylabel(r"$\tilde {x_{d}}$ $(\approx rad)$", fontsize=24)
    fig.tight_layout()
    fig.savefig("../../tex/time_series_final_project/back_calculated_xdes_reg2.pdf")
    print des_x.shape

def plot_force_sensors_region2():
    dataSetNumber=35 ## gains from commit 53b01fa156145fa687c939b918499a70e6b12500
    ank=gd.AnkleData("data%d.dat"%dataSetNumber,"/home/gray/wk/largeDataFiles/")
    semi_clean_region2=[35260,65140]
    q, qd, x, xd, gravity_comp, commmand, gamma_desired, des_accel, des_x = backcalculate_desired_position(ank, semi_clean_region2, sk=1)
    # des_x = generate_command_sequence(N)
    f5=ank.f5[semi_clean_region2[0]:semi_clean_region2[1]]
    f6=ank.f6[semi_clean_region2[0]:semi_clean_region2[1]]
    fig=plt.figure(figsize=(8,6),facecolor='w')
    ax=fig.add_subplot(211)
    ax.set_title("Raw Force Sensor Values", family='monospace',fontsize=24)
    ax.plot(f5,f6,'.',color='#333f48',lw=3, alpha=0.3)
    ax.set_xlabel(r"$\hat {f_5}$ $(N)$", fontsize=24)
    ax.set_ylabel(r"$\hat {f_6}$ $(N)$", fontsize=24)
    ax2=fig.add_subplot(212)
    t=0.001*np.array(range(semi_clean_region2[0],semi_clean_region2[1]))
    ax2.plot(t,f5,color='#333f48',lw=3)
    ax2.plot(t,f6,color='#bf5700',lw=3)
    ax2.set_xlabel(r"$t$ $(s)$", fontsize=24)
    ax2.set_ylabel(r"$\hat {f}$ $(N)$", fontsize=24)
    fig.tight_layout()
    fig.savefig("../../tex/time_series_final_project/force_sensors_reg2.pdf")
    print des_x.shape

def plot_force_sensors_region1():
    dataSetNumber=35 ## gains from commit 53b01fa156145fa687c939b918499a70e6b12500
    ank=gd.AnkleData("data%d.dat"%dataSetNumber,"/home/gray/wk/largeDataFiles/")
    semi_clean_region1=[5050,35080]
    q, qd, x, xd, gravity_comp, commmand, gamma_desired, des_accel, des_x = backcalculate_desired_position(ank, semi_clean_region1, sk=1)
    # des_x = generate_command_sequence(N)
    f5=ank.f5[semi_clean_region1[0]:semi_clean_region1[1]]
    f6=ank.f6[semi_clean_region1[0]:semi_clean_region1[1]]
    fig=plt.figure(figsize=(8,6),facecolor='w')
    ax=fig.add_subplot(211)
    ax.set_title("Raw Force Sensor Values", family='monospace',fontsize=24)
    ax.plot(f5,f6,'.',color='#333f48',lw=3, alpha=0.05)
    ax.set_xlabel(r"$\hat {f_5}$ $(N)$", fontsize=24)
    ax.set_ylabel(r"$\hat {f_6}$ $(N)$", fontsize=24)
    ax2=fig.add_subplot(212)
    t=0.001*np.array(range(semi_clean_region1[0],semi_clean_region1[1]))
    ax2.plot(t,f5,color='#333f48',lw=3)
    ax2.plot(t,f6,color='#bf5700',lw=3)
    ax2.set_xlabel(r"$t$ $(s)$", fontsize=24)
    ax2.set_ylabel(r"$\hat {f}$ $(N)$", fontsize=24)
    fig.tight_layout()
    fig.savefig("../../tex/time_series_final_project/force_sensors_reg1.pdf")
    print des_x.shape

if __name__=="__main__":
    # plot_input()
    # plot_force_command_reg2()
    # plot_force_command_reg1()
    # plot_measured_x_reg2()
    # plot_saturated_force_input()
    # plot_saturated_force_input_region1()
    # plot_saturated_command_position_region2()
    plot_force_sensors_region2()
    plot_force_sensors_region1()
    plt.show()