
Sigma=getcov(model7);
[V,D]=eig(Sigma);
% V*D*V'-Sigma = 0
sqrtD=diag(sqrt(diag(D)));
sizeI=size(D,1);

bjtests={};
omegas=logspace(0,log10(pi*1000),1000);
for i=0:10
    test.perterbation=V*sqrtD*randn(sizeI,1);
    test.model=apply_update(model7,test.perterbation);
    [test.mag,test.phase]=bode(test.model,omegas);
    test.roots.a=roots(test.model.a);
    test.roots.b=roots(test.model.b);
    test.roots.c=roots(test.model.c);
    
    bjtests{end+1}=test;
end
figure();
bode(model7,'k.',...
bjtests{1}.model,'k:',...
bjtests{2}.model,'k:',...
bjtests{3}.model,'k:',...
bjtests{4}.model,'k:',...
bjtests{5}.model,'k:',...
bjtests{6}.model,'k:',...
bjtests{7}.model,'k:',...
bjtests{8}.model,'k:',...
bjtests{9}.model,'k:')

figure()
pzplot(test.model)
figure()
bode(test.model)
idpoly
imagesc(Sigma)


