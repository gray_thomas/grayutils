function plotPZUTA(model,name)
utColors

%% Predictions

% pred100=pe(model,Data,100);
% pred10=pe(model,Data,10);

fig=figure();
% pzplot(model,Data);
lar=real(roots(model.a));
lai=imag(roots(model.a));
lbr=real(roots(model.b));
lbi=imag(roots(model.b));
lcr=real(roots(model.c));
lci=imag(roots(model.c));
theta=linspace(0,2*pi,1000);
xs=cos(theta);
ys=sin(theta);
hold on
plot(lbr,lbi,'O','MarkerSize',12,'LineWidth',3,'Color',utblue75)
plot(lcr,lci,'O','MarkerSize',12,'LineWidth',3,'Color',utyellow)
plot(lar,lai,'X','MarkerSize',12,'Color',utburntorange,'LineWidth',3)

legend({'B(z)=0', 'C(z)=0','A(z)=0'},'interpreter', 'latex')
plot(xs,ys,'-','LineWidth',1,'Color',utblack)
plot(xs,ys,':','LineWidth',3,'Color',utblack)
title(nameModelLatex(model),'Interpreter','latex')
figuresize(16,12,'centimeters')
fname=[ name 'pz_plot'];
print(['../../tex/time_series_final_project/' fname],'-dpdf','-r300');
% close(fig.Number)
fprintf(['{ %% PZ Plot for %s\n',...
	'\t\\usebackgroundtemplate{}\n',...
    '\t\\setbeamertemplate{navigation symbols}{}\n',...
    '\t\\begin{frame}[plain]\n',...
    '\t\t    \\begin{tikzpicture}[remember picture,overlay]\n',...
    '\t\t\t        \\node[at=(current page.center)] {\n',...
    '\t\t\t\t            \\includegraphics[width=\\paperwidth]{%s}\n',...
    '\t\t\t        };\n',...
    '\t\t    \\end{tikzpicture}\n',...
    '\t \\end{frame}\n',...
    '}\n'],nameModel(model),fname)

end