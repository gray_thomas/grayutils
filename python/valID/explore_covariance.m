
Sigma=getcov(model3);
[V,D]=eig(Sigma);
% V*D*V'-Sigma = 0
sqrtD=diag(sqrt(diag(D)));
sizeI=size(D,1);

tests={};
omegas=logspace(0,log10(pi*1000),1000);
for i=0:100
    test.perterbation=V*sqrtD*randn(sizeI,1);
    test.model=apply_update(model3,test.perterbation);
    [test.mag,test.phase]=bode(test.model,omegas);
    test.roots.a=roots(test.model.a);
    test.roots.b=roots(test.model.b);
    test.roots.c=roots(test.model.c);
    
    tests{end+1}=test;
end


figure()
pzplot(test.model)
figure()
bode(test.model)
idpoly
imagesc(Sigma)


