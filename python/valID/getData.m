function [des_x0,des_x1,roll, pitch,dataArray]=getData(name)
filename = ['/home/gray/wk/grayutils/python/valID/' name '.txt'];
delimiter = ' ';
startRow = 2;


formatSpec = '%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';

fileID = fopen(filename,'r');

dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'MultipleDelimsAsOne', true, 'EmptyValue' ,NaN,'HeaderLines' ,startRow-1, 'ReturnOnError', false);

fclose(fileID);

des_x0 = dataArray{:, 1};
des_x1 = dataArray{:, 2};
t5 = dataArray{:, 3};
t6 = dataArray{:, 4};
x5 = dataArray{:, 5};
x6 = dataArray{:, 6};
xd5 = dataArray{:, 7};
xd6 = dataArray{:, 8};
e5 = dataArray{:, 9};
e6 = dataArray{:, 10};
ex5 = dataArray{:, 11};
ex6 = dataArray{:, 12};
exd5 = dataArray{:, 13};
exd6 = dataArray{:, 14};
pitch = dataArray{:, 15};
roll = dataArray{:, 16};
pitch_d = dataArray{:, 17};
roll_d = dataArray{:, 18};
TS1 = dataArray{:, 19};
TS2 = dataArray{:, 20};
TS3 = dataArray{:, 21};
TS4 = dataArray{:, 22};
TS5 = dataArray{:, 23};
TS6 = dataArray{:, 24};
f5 = dataArray{:, 25};
f6 = dataArray{:, 26};


end