import getData as gd
import numpy as np 
import matplotlib.pyplot as plt 
from plotting_functions import *
import statsmodels.api as sm

baseSaveFolder="/home/gray/wk/grayutils/ValkyrieAnkleTester/data/"

def print_data_ranges(ank):
	def print_dif(ank, what):
			print what, max(ank[what])-min(ank[what])
	print "time: it, and t"
	print_dif(ank, 'it')
	print_dif(ank, 't')
	print "inputs: t5, and t6"
	print_dif(ank, 't5')
	print_dif(ank, 't6')
	print "outputs: everyting else:"
	print_dif(ank, 'x5')
	print_dif(ank, 'xd5')
	print_dif(ank, 'e5')
	print_dif(ank, 'ex5')
	print_dif(ank, 'exd5')
	print_dif(ank, 'x6')
	print_dif(ank, 'xd6')
	print_dif(ank, 'e6')
	print_dif(ank, 'ex6')
	print_dif(ank, 'exd6')
	print_dif(ank, 'pitch')
	print_dif(ank, 'pitch_d')
	print_dif(ank, 'roll')
	print_dif(ank, 'roll_d')
	print_dif(ank, 'TS1')
	print_dif(ank, 'TS2')
	print_dif(ank, 'TS3')
	print_dif(ank, 'TS4')
	print_dif(ank, 'TS5')
	print_dif(ank, 'TS6')
	print_dif(ank, 'f5')
	print_dif(ank, 'f6')

def perform_subspace_id(ank,region,length=20,sk=1):
	lo=region[0]
	hi=region[1]
	torque_jacobian=np.array([[0.75,0.75],[0.75,-0.75]])
	torque_jacobian_inverse=np.array([[0.75,0.75],[0.75,-0.75]])
	q=np.array([ank.roll[lo:hi:sk],ank.pitch[lo:hi:sk]])
	qd=np.array([ank.roll_d[lo:hi:sk],ank.pitch_d[lo:hi:sk]])
	x=torque_jacobian_inverse.dot(q)
	xd=torque_jacobian_inverse.dot(qd)
	Kp=np.array([[20.0,0.0],[0.0,20.0]])
	Kd=np.array([[5000.0,0.0],[0.0,5000.0]])
	gravity_comp=np.array([[-87.46, -106.25],[-71.83,  98.26]])
	gravity_bias=np.array([[-6.42], [-2.10]])
	gravity_estimate=np.diagflat(gravity_bias).dot(np.ones(q.shape))+gravity_comp.dot(q)
	
	command = np.array([ank.tau5[lo:hi:sk],ank.tau6[lo:hi:sk]])
	gamma_desired= command-gravity_estimate
	des_accel = np.linalg.inv(torque_jacobian_inverse).dot(gamma_desired)
	des_x = np.linalg.inv(Kp).dot( des_accel + Kp.dot(x) + Kd.dot(xd) )

	# plt.plot(ank.f6[lo:hi:sk])
	# plt.plot(ank.t6[lo:hi:sk])
	# plt.plot(xd.T)
	# plt.plot(x.T)
	# plt.plot(gravity_estimate.T)
	plt.plot(des_x.T)



if __name__=="__main__":
	dataSetNumber=35
	ank=gd.AnkleData("data%d.dat"%dataSetNumber,"/home/gray/wk/largeDataFiles/")
	
	# plt.plot(ank.t[1:]-ank.t[:-1])# use to identify pauses and 
	# regions of nearly-even sampling intervals.
	all_the_data=[0,181946]
	semi_clean_region1=[5050,35080]
	semi_clean_region2=[35260,65140]
	semi_clean_region3=[65280,95180]
	semi_clean_region4=[95320,125500]
	perform_subspace_id(ank,semi_clean_region2, 20, sk=10)
	plt.show()