function plotForecastConf(Data,model,name,Ps,Ns)
utColors


fig=figure();
h=[plot(Data.y,'Color',utblack,'linewidth',3)];
hold on
legend_cell={'$X_t$'};
%% Predictions
for i=1:length(Ps)
    P=Ps(i);
    N=Ns(i);
    region=(P+1):(P+N);
    prev=iddata(Data.y(1:P,:),Data.u(1:P,:),'Ts',Data.Ts);
    opt=forecastOptions;
    opt.set('InitialCondition','d')
    res=forecast(model,prev,N,Data.u(region,:),opt);
    unc=getStdDevUncertainty(model,Data,N);
    h=[h,plot(region,res.y,'Color',colorwheel1(i,:),'linewidth',3)];
    
    
    nstd=sqrt(chi2inv(.95,1));
    h=[h,patch([region flip(region)],...
        [res.y+nstd*unc;flip(res.y-nstd*unc)],...
        colorwheel1(i,:),'FaceAlpha',0.6,'EdgeAlpha',0)];
    nstd=sqrt(chi2inv(.9999,1));
    h=[h,patch([region flip(region)],...
        [res.y+nstd*unc;flip(res.y-nstd*unc)],...
        colorwheel1(i,:),'FaceAlpha',0.4,'EdgeAlpha',0)];
%     plot(region,res.y+2*unc,':','Color',colorwheel1(i,:),'linewidth',2)
%     plot(region,res.y-2*unc,':','Color',colorwheel1(i,:),'linewidth',2)
    legend_cell=[legend_cell,{sprintf('$X_{%d}(t-%d)$',P,P)},...
        {'$95\%$ Confidence'}...
        ,{'$99.99\%$ Confidence'}];
end

x_axis=[min(Ps),max(Ns)+max(Ps)];
q=gca();
q.set('XLim',x_axis);

legend(h,legend_cell,'interpreter', 'latex','Location','best')
title(nameModelLatex(model),'interpreter', 'latex')
figuresize(16,12,'centimeters')
fname=[ name 'forcasts'];
print(['../../tex/time_series_final_project/' fname],'-dpdf','-r300');
% close(fig.Number)
fprintf(['{ %% Forecast for %s\n',...
	'\t\\usebackgroundtemplate{}\n',...
    '\t\\setbeamertemplate{navigation symbols}{}\n',...
    '\t\\begin{frame}[plain]\n',...
    '\t\t    \\begin{tikzpicture}[remember picture,overlay]\n',...
    '\t\t\t        \\node[at=(current page.center)] {\n',...
    '\t\t\t\t            \\includegraphics[width=\\paperwidth]{%s}\n',...
    '\t\t\t        };\n',...
    '\t\t    \\end{tikzpicture}\n',...
    '\t \\end{frame}\n',...
    '}\n'],nameModel(model),fname)

end