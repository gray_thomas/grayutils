function sigma_a=getSigmaA(Model, Data)
sigma_a=sqrt(getRss(Model,Data)/(Data.n-nparams(Model)));