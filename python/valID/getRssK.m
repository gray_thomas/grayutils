function Rss=getRssK(Data,model,k)
r=pe(model,Data,k);
residuals=r.y;
Rss=sum(residuals.^2);
end