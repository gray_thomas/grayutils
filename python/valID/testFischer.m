function [test,control]=testFischer(RssBig,RssSmall,pBig,pSmall,N,P)
    test=((RssSmall-RssBig)/(pBig-pSmall))/(RssBig/(N-pBig));
    control=finv(P,pBig-pSmall,N-pBig);
end