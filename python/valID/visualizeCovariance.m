function visualizeCovariance(model)
cov=getcov(model);
imagesc(cov);
colormap(flipud(gray)); 
set(gca,'XTick',1:5,...                         %# Change the axes tick marks
        'XTickLabel',{'$A_1$','B','C','D','E'},...  %#   and tick labels
        'YTick',1:5,...
        'YTickLabel',{'A','B','C','D','E'},...
        'TickLength',[0 0]);
    
%% Generate figure and remove ticklabels
close all;
plot(1:10);
set(gca,'yticklabel',[], 'xticklabel', []) %Remove tick labels
%% Get tick mark positions
yTicks = get(gca,'ytick');
xTicks = get(gca, 'xtick');
ax = axis; %Get left most x-position
HorizontalOffset = 0.1;
%% Reset the ytick labels in desired font
for i = 1:length(yTicks)
%Create text box and set appropriate properties
     text(ax(1) - HorizontalOffset,yTicks(i),['$' num2str( yTicks(i)) '$'],...
         'HorizontalAlignment','Right','interpreter', 'latex');   
end
%% Reset the xtick labels in desired font 
minY = min(yTicks);
verticalOffset = 0.2;
for xx = 1:length(xTicks)
%Create text box and set appropriate properties
     text(xTicks(xx), minY - verticalOffset, ['$' num2str( xTicks(xx)) '$'],...
         'HorizontalAlignment','Right','interpreter', 'latex');   
end