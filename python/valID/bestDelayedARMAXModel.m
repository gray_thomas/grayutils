function [CurrentModel, CurrentRss]=bestDelayedARMAXModel(Data, na,nb,nc...
    ,nkoptions,varargin)
% na=2*ones(ny,ny);
%     nb=ones(ny,nu);
%     nc=1*ones(ny,ny);
if nargin<6
opt=armaxOptions;
else
    opt=varargin{1};
end
    
BestModelRss=inf;
BestModel=0;
bestNk=-1;
% Brute force search for the best model fit over possible nk params
for i = 1:size(nkoptions,2)
    CurrentModel=armax(Data,[na, nb, nc, nkoptions(:,i)'],opt); 
    CurrentRSS=getRss(Data,CurrentModel);
    disp(['model ', nameModel(CurrentModel), ' Rss(1) = ',...
        num2str(CurrentRSS), ' Std[X_t(1)] = ',...
        num2str(sqrt(CurrentRSS/(Data.n-nparams(CurrentModel))))])
    if (CurrentRSS<BestModelRss)
        disp(['model ',nameModel(CurrentModel),'is an improvement'...
            ,' over model with nk=',num2str(bestNk),' (or is defualt'...
            , ' in the case where nk was -1).'])
        BestModelRss=CurrentRSS;
        BestModel=CurrentModel;
        bestNk=nkoptions(:,i)';
    end
end
CurrentModel=BestModel;
CurrentRss=BestModelRss;
end