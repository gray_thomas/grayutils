
dependancies={
"AttitudeEstimator":(None,None,None,["PhaseSpaceUDPReceiver","VectorGeometry",'Eigen']),
"PhaseSpaceUDPReceiver":(None,None,None,["GUThreads","GUTiming"]),
"JoystickReceiver":(None,None,None,["GUThreads"]),
"LinearControls":(None,None,None,["Eigen"]),
"VectorGeometry":(None,None,None,["Eigen"]),
"GUThreads":(None,None,None,[]),
"GUTiming":(None,None,None,["GUThreads"]),
"gtest":(None,None,None,[]),
"Eigen":(["/usr/include/eigen3"],[],[],[])}



include = ["/usr/include/eigen3","/usr/local/lib"]
library = ["/usr/include/eigen3","/usr/local/include"]


def setup(projects):
	include_dirs,library_dirs,libnames,explored_tree = set(), set(), set(), set()
	def recursive_broaden(deps):
		for project in deps:
			if project not in explored_tree:
				explored_tree.add(project)
				new_inc_dirs, new_lib_dirs, new_lib_names, new_deps = dependancies[project]
				if new_inc_dirs==None:
					new_inc_dirs=["/usr/local/include/%s"%project]
				if new_lib_dirs==None:
					new_lib_dirs=["/usr/local/lib"]
				if new_lib_names==None:
					new_lib_names=[project]
				include_dirs.update(new_inc_dirs)
				library_dirs.update(new_lib_dirs)
				libnames.update(new_lib_names)
				recursive_broaden( new_deps)

	recursive_broaden(projects)
	return list(include_dirs), list(library_dirs), list(libnames)