from math import tanh
import numpy as np
import matplotlib.pyplot as plt
from plot_setup import *

def conv(A, B):
    ret = [0] * (len(A) + len(B) - 1)
    for i, a in enumerate(A):
        for j, b in enumerate(B):
            ret[i + j] += a * b
    return ret


def deriv(A):
    ret = [0] * (len(A) - 1)
    for i, a in enumerate(A):
        if i == 0:
            continue
        ret[i - 1] += i * a
    return ret

def divide(A,B):
    Q = [a for a in A]
    for i in reversed(range(0, len(Q))):
        scale = Q[i]/B[-1]
        for j, b in enumerate(B):
            Q[i-len(B)+j+1]-=scale*B[j]
        if i-len(B)==-1:
            return [Q[i] for i in range(0, len(B)-1)]

def roots(A):
    sc = 1./A[-1]
    q = [-a * sc for a in A[0:-1]]
    n = len(q)
    M = np.zeros((n,n))
    M[0:-1,1:]=np.eye(n-1)
    M[-1,:]=q
    eigs, U =  np.linalg.eig(M)
    ret =  []+[[e.real, -1] for e in eigs if abs(e.imag)<1e-7]
    return ret


val = [[0,1]] # = tanh(x)
for i in range(0, 10):
    val.append( conv(deriv(val[i]), [1,0,-1]))

xs = np.linspace(-3,3,1001)

for i in range(0, 4):
    rs = [[-1,-1]]+[[1,-1]]+roots(val[i+1])
    opts = [divide(val[i], div)[0] for div in rs]
    print max(opts), min(opts)
    absmax = max([abs(max(opts)),abs(min(opts))])
    ys = [divide(val[i],[tanh(x),-1])[0] for x in xs]
    print "abs max ", absmax, max(ys)
    plt.plot(xs, np.array(ys)/absmax)
plt.show()




print divide(val[1], [0,-1])
print val 
