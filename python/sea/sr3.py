import ly_utils as ly
from collections import namedtuple
import numpy as np
from math import *
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from plot_setup import *
from BiChatterer import BiChatterer


def sg(x):
    return -1. if x<-1. else 1. if x>1. else x


def dsg(x):
    return 0.0 if x<-1. else 0.0 if x>1. else 1.


def ddsg(x):
    return 0.0


def dddsg(x):
    return (6 * x**2 - 2.0) / (1. + x**2)**3


def sg_inv(x):
    return tan(pi * x) / pi


def dsg_inv(x):
    return 1.0 / (cos(pi * x)**2)

def safe_reciprocal(x):
    if x<1e-7:
        return 1e7 if x>0 else -1e7
    return 1.0/x

def sg(x):
    return atan(pi * x) / pi


def dsg(x):
    return 1.0 / (1. + x**2)


def ddsg(x):
    return -2.0 * x / (1. + x**2)**2


def dddsg(x):
    return (6 * x**2 - 2.0) / (1. + x**2)**3


def sg_inv(x):
    return tan(pi * x) / pi


def dsg_inv(x):
    return 1.0 / (cos(pi * x)**2)

@ly.named_state_system("Integrator3State", ["x", "xd", "xdd"])
class Integrator3(object):

    """Simple Integrator"""

    def __init__(self, state0=(0, 0, 0), eta=100, k_s=1):
        self.state = 0.0, (state0)
        self.eta = eta
        self.k_s = k_s
        self.events = []
        self.chatterer = BiChatterer(
            self, self.sigma, self.del_sigma, "u")

    # def sigma(self):
    #     return sum([a * b for a, b in zip(self.state[1], self.del_sigma())])
    # def del_sigma(self):  # assume factorization exists: (s/k+1)^3
    #     return np.array([1, 2.0 / self.k_s, 1.0 / self.k_s**2])

    def s1(self):
        x, xd, xdd = self.state[1]
        k = self.k_s
        return (
            sg(x) + xd/k,
            dsg(x)*xd + xdd / k
        )

    def J1(self):
        x, xd, xdd = self.state[1]
        k = self.k_s
        return np.array([[dsg(x), 1 / k, 0],
                         [xd * ddsg(x), dsg(x), 1 / k]])

    def s2(self):
        s, sd = self.s1()
        k = self.k_s
        return ( sg(s) + sd / k)

    def J2(self):
        s, sd = self.s1()
        k = self.k_s
        return np.array([[dsg(s), 1 / k]])

    # def sigma(self):
    #     return self.s2()

    # def del_sigma(self):
    #     return self.J2().dot(self.J1()).reshape((-1))

    def sigma(self):
        x, xd, xdd = self.state[1]
        k = self.k_s
        return xdd/k**2 + dsg(x) * xd / k + sg(
            xd/k + sg(x))

    def sigma(self):
        x, xd, xdd = self.state[1]
        k = self.k_s
        return xdd/k**2 + sg(xd) / k + sg(x)

    def sigma(self):
        x, xd, xdd = self.state[1]
        k = self.k_s
        return xdd/k**2+sg(xd/k+sg(2*k*abs(x)*safe_reciprocal(xd)))

    def sigma(self): # Theoretically sound, crisp.
        x, xd, xdd = self.state[1]
        k = self.k_s
        return xdd+dsg(x)*xd+sg(xd+sg(x))

    def sigma(self): # scales easily, resonant poles.
        x, xd, xdd = self.state[1]
        k = self.k_s
        return xdd+sg(xd+sg(x))

    def sigma(self): # speed bound changes.
        x, xd, xdd = self.state[1]
        k = self.k_s
        return xdd+sg(2*xd+sg(x))

    def sigma(self): # speed bound fixed. Comparable to theoretic version, but lacking sharp corners.
        x, xd, xdd = self.state[1]
        k = self.k_s
        return xdd+sg(2*xd+2*sg(x/2.))

    def del_sigma(self):
        x, xd, xdd = self.state[1]
        k = self.k_s
        return [dsg(xd/k+sg(x))*dsg(x)+ddsg(x)*xd/k, dsg(x)/k, 1/k**2]

    def __call__(self, u=None):
        return (self.xd, self.xdd, -self.eta*sg(self.sigma()))

    def sim(self, tf=100, n=1001):
        sys = self
        stepper = ly.dormand_prince(sys, np.linspace(0, tf, n))
        try:
            for jump, time, state, guard in ly.hybrid_integrate(stepper):
                yield self
        except StopIteration as e:
            print e


def plot(tf=10, n=1001):
    fig = plt.figure(figsize=(5.5, 9))
    axs = [fig.add_subplot(4, 1, 1)]
    for i in range(1, 4):
        axs.append(fig.add_subplot(4, 1, 1 + i, sharex=axs[0]))
    for i in range(0, 4):
        axs[i].hold(True)
    axs[-1].set_xlabel(r"$t$", size=14)
    axs[0].set_ylabel(r"${\xi}$", size=14)
    axs[1].set_ylabel(r"$\dot{\xi}$", size=14)
    axs[2].set_ylabel(r"$\ddot{\xi}$", size=14)
    axs[3].set_ylabel(r"$\sigma{(\xi)}$", size=14)

    for x0 in [-0.5e1, -0.75e1, -1e1]:
        sys = Integrator3(state0=(x0, 0, 0))
        dat = np.array([[p.t] + list(p.state[1]) + [p.sigma()]
                        for p in sys.sim(tf=tf, n=n)])
        for i, ax in enumerate(axs):
            ax.plot(dat[:, 0], dat[:, i + 1])
    fig.tight_layout()
    return fig

save = "plots/sr3_%s.pdf"


def main():
    plot(tf=3e1, n=1001).savefig(save % "test")
    plt.show()

if __name__ == "__main__":
    main()
