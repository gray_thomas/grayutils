import matplotlib as mpl
from gu.colormaps import *
custom_cm = make_map("LinearL")
# print custom_cm(0.2)
# mpl.rcParams['axes.color_cycle'] = ['r', 'k', 'c']
# mpl.rcParams['axes.color_cycle'] = [ut_burnt_orange, ut_yellow, w2(ut_brown), ut_ivory, ut_burnt_orange, w1(ut_gray), w3(ut_blue)]
cap = lambda x: x if x <= 1 else 2 - x
new_cycle=[custom_cm(0.9*cap(x)) for x in np.linspace(0.0,2.0,11)][0:-1]

def color_cycle(ax, n=11):
       new_cycle = [custom_cm(0.9 * cap(x)) for x in np.linspace(0.0, 2.0, n)][0:-1]
       ax.set_color_cycle(new_cycle)

mpl.rcParams['axes.color_cycle'] = new_cycle
mpl.rcParams['lines.linewidth'] = 1.9
mpl.rcParams['lines.solid_capstyle']= 'round'
_text = '404040'
mpl.rcParams['text.color'] = _text
mpl.rcParams['axes.facecolor'] = 'ffffff'
mpl.rcParams['axes.edgecolor'] = '111111'
mpl.rcParams['axes.grid'] = True
mpl.rcParams['grid.color'] = 'b7b7b7'
mpl.rcParams['axes.labelcolor'] = _text
mpl.rcParams['xtick.color'] = _text
mpl.rcParams['ytick.color'] = _text
mpl.rcParams['figure.facecolor'] = 'ffffff'
mpl.rcParams['savefig.facecolor'] = 'ffffff'
mpl.rcParams['savefig.edgecolor'] = 'None'

#text.latex.preamble :  # IMPROPER USE OF THIS FEATURE WILL LEAD TO LATEX FAILURES
                            # AND IS THEREFORE UNSUPPORTED. PLEASE DO NOT ASK FOR HELP
                            # IF THIS FEATURE DOES NOT DO WHAT YOU EXPECT IT TO.
                            # preamble is a comma separated list of LaTeX statements
                            # that are included in the LaTeX document preamble.
                            # An example:
                            # text.latex.preamble : \usepackage{bm},\usepackage{euler}
                            # The following packages are always loaded with usetex, so
                            # beware of package collisions: color, geometry, graphicx,
                            # type1cm, textcomp. Adobe Postscript (PSSNFS) font packages
                            # may also be loaded, depending on your font settings

mpl.rcParams['text.usetex'] = True
mpl.rcParams['text.latex.preamble'] = [
r"\usepackage[cmex10]{amsmath}",
r"\usepackage{bm,upgreek}",
r"\usepackage{amsfonts}"
]