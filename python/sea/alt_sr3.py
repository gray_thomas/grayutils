import ly_utils as ly
from collections import namedtuple
import numpy as np
from math import *
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from plot_setup import *
from BiChatterer import BiChatterer



def sat(x):
    return -1. if x < -1. else 1. if x > 1. else x


def stg(x):
    return atan(pi * x) / pi


def sg(x):
    return -1. if x < -2. else 1. if x > 2. else (abs(x) - (x / 2.)**2) * np.sign(x)


@ly.named_state_system("Integrator3State", ["x", "xd", "xdd"])
class Integrator3(object):

    """Simple Integrator"""

    def __init__(self, state0=(0, 0, 0), eta=100, k_s=1):
        self.state = 0.0, (state0)
        self.eta = eta
        self.k_s = k_s
        self.events = []
    @property
    def full(self):
        x, xd, xdd = self.state[1]
        xd, xdd, xddd = self()
        return x, xd, xdd, xddd
    

    def __call__(self, u=None):
        x, xd, xdd = self.state[1]
        if xdd>1:
            return (xd, xdd, -1)
        if xdd<-1:
            return (xd, xdd, 1)
        if xd>1:
            return (xd, xdd, -1)
        if xd<-1:
            return (xd, xdd, 1)
        if x>1:
            return (xd, xdd, -1)
        if x<-1:
            return (xd, xdd, 1)

        return (xd, xdd, sat(-2*xdd - 2*xd-x)) 

    def __call__(self):
        x, xd, xdd = self.state[1]
        return (xd, xdd, sat(-2*xdd - 2*sat(xd + sat(0.5*x))))

    def __call__(self):
        x, xd, xdd = self.state[1]
        return (xd, xdd, sg(-2*xdd - 2*sg(xd + sg(0.5*x))))


    def __call__(self):
        x, xd, xdd = self.state[1]
        l=2.5
        e1=0.6
        e2=0.5
        return (xd, xdd, e1*sg(-2*xdd*l/e1 - 2*l/e1*e2*sg(xd*l/e2 + l/e2*sg(0.5*x*l))))
    def sim(self, tf=100, n=1001):
        sys = self
        stepper = ly.dormand_prince(sys, np.linspace(0, tf, n))
        try:
            for jump, time, state, guard in ly.hybrid_integrate(stepper):
                yield self
        except StopIteration as e:
            print e


def plot(tf=10, n=1001):
    fig = plt.figure(figsize=(5.5, 9))
    axs = [fig.add_subplot(4, 1, 1)]
    for i in range(1, 4):
        axs.append(fig.add_subplot(4, 1, 1 + i, sharex=axs[0]))
    for i in range(0, 4):
        axs[i].hold(True)
    axs[-1].set_xlabel(r"$t$", size=14)
    axs[0].set_ylabel(r"${\xi}$", size=14)
    axs[1].set_ylabel(r"$\dot{\xi}$", size=14)
    axs[2].set_ylabel(r"$\ddot{\xi}$", size=14)
    axs[3].set_ylabel(r"$\dddot{\xi}$", size=14)

    for x0 in [-30,0,30]:
        for xd0 in [-1.125,0,1.125]:
            for xdd0 in [-1.125,0,1.125]:
                sys = Integrator3(state0=(x0, xd0, xdd0))
                dat = np.array([[p.t] + list(p.full)
                                for p in sys.sim(tf=tf, n=n)])
                for i, ax in enumerate(axs):
                    ax.plot(dat[:, 0], dat[:, i + 1])
    fig.tight_layout()
    return fig

save = "plots/sr3_%s.pdf"


def main():
    plot(tf=1e2, n=1001).savefig(save % "test")
    plt.show()

if __name__ == "__main__":
    main()
