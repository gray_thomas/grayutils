import lyapunov as ly
import ly_utils as lu
import numpy as np
from collections import namedtuple

SEA_Params = namedtuple("SEA_Params", [
    "joint_inertia",
    "joint_bias_torque",
    "joint_damping",
    "spring_stiffness",
    "transmission_to_spring",
    "transmission_to_motor"])


@lu.named_state_system("SEA_State", ["x", 'xd', 't', 'td'])
class SEA_System(object):

    def __init__(self):
        self.state = 0, (0.0, 0.0, 0.0, 0.0)

    def __call__(self):
        np.array([
            [0.00, 0.00, 0.00, 0.00],
            [0.00, 0.00, 0.00, 0.00],
            [0.00, 0.00, 0.00, 0.00],
            [0.00, 0.00, 0.00, 0.00],
        ])
        return (0.0, 0.0, 0.0, 0.0)

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    sys = SEA_System()
    step = ly.runge_kutta4(sys, np.linspace(0, 2.0, 1000))
    step.relative_tolerance = 1e-8
    for i, t, c, d in lu.hybrid_integrate(step):
        print "hybrid time (t, j) = (%.3f, %d)" % (t, i)
