import ly_utils as ly
from collections import namedtuple
import numpy as np
from math import *
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from plot_setup import *
from BiChatterer import BiChatterer
from filters import *
from math import tanh
from testSEAController import SEAController

def sg(val):
    return tanh(val)



@ly.named_state_system("FourthOrderSystemState", ["x", "xd", "xdd", "xddd"])
class FourthOrderSystem(object):

    """Simple FourthOrderSystem"""

    def __init__(self, state0=(0, 0, 0, 0), sensor_noise=lambda :0.0, dynamics_noise=lambda :0.0):
        self.state = 0.0, (state0)

        k=.666
        eta = k*3e0
        self.coeffs = lp_coeffs(4,k)
        self.sn = lambda: sensor_noise(self.get_t())
        self.dn = lambda: dynamics_noise(self.get_t())
        self.sensor_noise = BWN(self.sn, n=2, k=(1.0 * pi * sensor_noise.freq_hz))
        self.dynamics_noise = BWN(self.dn, n=2, k=(1.0 * pi * dynamics_noise.freq_hz))
        self.est = BWN(lambda: self.measure(), n=4, k=k*30.0e0)
        self.maxes = [1.0,eta,eta**2,eta**3,eta**4]
        self.max_values = [1., 1., 1.]
        self.events = []
        self.par = ly.ParallelSystems([self, self.est, self.sensor_noise, self.dynamics_noise])
        self.controller = SEAController()
    def measure(self):
        return self.x+2e-2*self.sensor_noise.x
    def get_t(self):
        return self.t

    def control(self):
        # x, xd, xdd, xddd = self.state[1] # cheating
        x, xd, xdd, xddd = self.est.full[0:4]
        k0, k1, k2, k3, k4 = reversed(self.coeffs)
        k0p, k1p, k2p, k3p = k0/k4, k1/k4, k2/k4, k3/k4
        m0, m1, m2, m3, m4 = self.maxes
        b3 = m4
        a3 = k3p/b3
        b2 = m3*a3
        a2 = k2p /(b2*b3)
        b1 = m2*a2
        a1 = k1p / (b1*b2*b3)
        b0 = m1*a1
        a0 = k0p / (b0*b1*b2*b3)
        xd4 = - b3*sg(a3*xddd + b2*sg(a2*xdd + b1*sg(a1*xd + b0*sg(a0*x))))
        return xd4

    def control_no_saturation(self):
        # x, xd, xdd, xddd = self.state[1] # cheating
        x, xd, xdd, xddd = self.est.full[0:4]
        k0, k1, k2, k3, k4 = reversed(self.coeffs)
        k0p, k1p, k2p, k3p = k0/k4, k1/k4, k2/k4, k3/k4
        xd4 = - k3p*xddd - k2p*xdd - k1p*xd - k0p*x
        return xd4

    def __call__(self, u=None):
        return (self.xd, self.xdd, self.xddd, self.control()+self.dynamics_noise.x)

    def sim(self, tf=100, n=1001):
        sys = self.par
        stepper = ly.runge_kutta4(sys, np.linspace(0, tf, n))
        try:
            for jump, time, state, guard in ly.hybrid_integrate(stepper):
                yield self
        except StopIteration as e:
            print e


def plot(tf=10, n=1001):
    fig = plt.figure(figsize=(6.5, 9))
    axs = [fig.add_subplot(4, 1, 1)]
    for i in range(1, 4):
        axs.append(fig.add_subplot(4, 1, 1 + i, sharex=axs[0]))
    for i in range(0, 4):
        axs[i].hold(True)
    axs[-1].set_xlabel(r"$t$", size=14)
    axs[0].set_ylabel(r"${\xi}$", size=14)
    axs[1].set_ylabel(r"$\dot{\xi}$", size=14)
    axs[2].set_ylabel(r"$\ddot{\xi}$", size=14)
    axs[3].set_ylabel(r"$\dddot{\xi}$", size=14)


    sys = FourthOrderSystem(state0=(-0.0, 20.0, 0, 0), sensor_noise = noise(tf, 1e1, 1.0), dynamics_noise = noise(tf, 1e0, 1.0e0))
    dat = np.array([[p.t] + list(p.state[1]) + list(p.est.full)+[p.measure()]
                    for p in sys.sim(tf=tf, n=n)])
    for i, ax in enumerate(axs):
        ax.plot([0,tf],[p.maxes[i]]*2, color = 'k')
        ax.plot([0,tf],[-p.maxes[i]]*2, color = 'k')
        ax.plot(dat[: , 0], dat[: , i + 1], zorder = 10)
        ax.autoscale(False)
        ax.plot(dat[: , 0], dat[: , i + 5])
    axs[0].plot(dat[:,0], dat[:, -1])
    fig.tight_layout()
    return fig

save = "plots/ident_%s.pdf"


def main():
    print "running"
    rand.seed(25)
    plot(tf=50.0, n=10001).savefig(save % "1")
    plt.show()

if __name__ == "__main__":
    main()
