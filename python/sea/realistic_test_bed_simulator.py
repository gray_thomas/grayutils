import ly_utils as ly
from collections import namedtuple
import numpy as np
from math import *
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from plot_setup import *
from BiChatterer import BiChatterer
from filters import *
from math import tanh
from testSEAController import SEAController


#define ARM_STARTING_POS_DEG 53.0
##define SPRING_SENSOR_ZERO_OFFSET 2.513  #adjust this if there is force/torque offset, increase this value if there is an upwards torque for gravity compensation, decrease if there is a downward torque
##define ARM_V_PI_RAD 1.000 #sensor voltage for an arm angle of pi radians (change if abs. angle measurement is off)

# ADDED_MASS_LBS=0.0 #0.0 10.25
ADDED_MASS=0.0 #kg
ADDED_MASS_DIST=.211 #m from arm pivot along arm axis
  # for 0.0                pdobfc = 35 maxforce = 500  maxtorque = 8  chirp_a=15 chirp_f=3 spline_a=30 spline_f=2
  # for 2.475  dist = .190 pdobfc = 13 maxforce = 800  maxtorque = 17 chirp_a=15 chirp_f=2 spline_a=30 spline_f=1.5
  # for 5.314  dist = .190 pdobfc = 13 maxforce = 1200 maxtorque = 30 a=?  f=? spline_a=30 spline_f=1.25
  # for 9.793  dist = .190 pdobfc = 9  maxforce = 1200 maxtorque = 35 a=?  f=? spline_a=30 spline_f=1.0
  # for 19.586 dist = .197 pdobfc = 5  maxforce = 1500 maxtorque = 45 a=10 f=1.2

#define ENABLE_SIMPLE_DERIVATIVE_MEASUREMENT 0
PI = pi
#position limits
HARD_MAX_ARM_ANGLE_DEG=168.0
HARD_MIN_ARM_ANGLE_DEG=53.0
MID_ARM_ANGLE_DEG=((HARD_MAX_ARM_ANGLE_DEG + HARD_MIN_ARM_ANGLE_DEG)/2)
#define ARM_ANGLE_SAFETY_ZONE_DEG 5 #-10 or 5?
#define SOFT_MAX_ARM_ANGLE_DEG (HARD_MAX_ARM_ANGLE_DEG-ARM_ANGLE_SAFETY_ZONE_DEG)
#define SOFT_MIN_ARM_ANGLE_DEG (HARD_MIN_ARM_ANGLE_DEG+ARM_ANGLE_SAFETY_ZONE_DEG)
#define MID_POS_M (armAngle_rad2Length_m(deg2rad(MID_ARM_ANGLE_DEG)))

#force limits
#define MAX_TORQUE_NM 10  #max: 70
MAX_FORCE_N=100  #max: 2750


#spring displacement params
SPRING_PULLEY1_DIAM_MM=7.239 #absolute sensor
SPRING_THROW_MM=(PI*SPRING_PULLEY1_DIAM_MM)
SUPPLY_V_SPRING=3.9337 #effective supply voltage (tuning param)
SPRING_PULLEY2_DIAM_MM=7.239 #incremental sensor
SPRING_CONST_NpMM=350 #was 277.78
SPRING_CONST_NpM=(SPRING_CONST_NpMM*1000)

#drivetrain params
BIG_PULLEY_TEETH=60.0
SMALL_PULLEY_TEETH=15.0
PULLEY_REDUCTION=(BIG_PULLEY_TEETH/SMALL_PULLEY_TEETH)
BALLSCREW_LEAD=.003 #m
DRIVETRAIN_EFF=0.9 #efficiency
SPEED_REDUCTION=(PULLEY_REDUCTION*2*PI/BALLSCREW_LEAD)

# #mechanical params
C_LENGTH_M=0.15436
B_LENGTH_M=0.025
SPRUNG_MASS=.706 #kg
ROTOR_INERTIA=0.00000344900 #kgm^2
BALL_NUT_INERTIA=0.000017523 #kgm^2
EFFECTIVE_ROTOR_INERTIA=(ROTOR_INERTIA + BALL_NUT_INERTIA/PULLEY_REDUCTION/PULLEY_REDUCTION)
G=9.80665
ARM_MASS=0.665 #kg
ARM_X_COM_DIST=0.157
ARM_Y_COM_DIST=-0.001 
ARM_COM_ANG_OFFSET =  (atan2(ARM_Y_COM_DIST,ARM_X_COM_DIST)) #rad
ARM_GEOM_VERT_OFFSET=0.0 #m, distace arm axis is from rotation point
COMBINED_ARM_COM_X=(ARM_X_COM_DIST*ARM_MASS + ADDED_MASS_DIST*ADDED_MASS)/(ARM_MASS+ADDED_MASS)
COMBINED_ARM_COM_Y=(ARM_Y_COM_DIST*ARM_MASS + ARM_GEOM_VERT_OFFSET*ADDED_MASS)/(ARM_MASS+ADDED_MASS)
COMBINED_ARM_COM_ANG_OFFSET=(atan2(COMBINED_ARM_COM_Y,COMBINED_ARM_COM_X))
COMBINED_ARM_COM_DIST=(sqrt(COMBINED_ARM_COM_Y*COMBINED_ARM_COM_Y + COMBINED_ARM_COM_X*COMBINED_ARM_COM_X))
COMBINED_ARM_COM_MASS=(ARM_MASS+ADDED_MASS)
MOUNT_ANG_OFFSET=0.0389 #rad, angle from actuator mount pivot to joint pivot from horizontal

# #position control
ARM_INERTIA=(COMBINED_ARM_COM_MASS*COMBINED_ARM_COM_DIST*COMBINED_ARM_COM_DIST) #test this first
ARM_FRICTION=0.065 #Nm*s/rad (viscous)

# #thermal params
#define MAX_CONTINUOUS_MOTOR_CURRENT 4.7 #A
#define MAX_CONTINUOUS_MOTOR_TORQUE motorCurrent2Torque(MAX_CONTINUOUS_MOTOR_CURRENT) #Nm

# #motor and electrical params
#define DAC_VOLTAGE 10.0
ELMO_PEAK_CURRENT=30.0
TORQUE_CONSTANT=.0276 #nm/A
#define SPEED_CONSTANT 345.99 #rpm/v
#define WINDING_RESISTANCE 0.386 #ohms

# #conversions
motorAng2bsPos_m = lambda (mang): ((float)(mang)*BALLSCREW_LEAD/2/PI/PULLEY_REDUCTION)
sensorAng2springPos_mm = lambda : (sang) (SPRING_ENC_DIRECTION*(float)(sang)*SPRING_PULLEY2_DIAM_MM/2)
springPos_mm2springForce_N = lambda (pos) : ((float)(pos)*SPRING_CONST_NpMM)
springForce_N2springPos_mm = lambda (force) : ((float)(force)/SPRING_CONST_NpMM)

armAngle_rad2Length_m = lambda (angle_rad) : sqrt( B_LENGTH_M*B_LENGTH_M + C_LENGTH_M*C_LENGTH_M - 2*B_LENGTH_M*C_LENGTH_M*cos((float)(angle_rad)) ) #finds actuator length given arm angle using law of cosines
len2ArmAngle = lambda (len) : (acos((-((float)(len)*(float)(len)) + B_LENGTH_M*B_LENGTH_M + C_LENGTH_M*C_LENGTH_M)/(2*B_LENGTH_M*C_LENGTH_M) ))
motorTorque2current = lambda (torque) :  ((float)(torque)/TORQUE_CONSTANT)
BsForce2motorTorque = lambda (force) : ((float)(force)*BALLSCREW_LEAD/(PULLEY_REDUCTION*2*PI*DRIVETRAIN_EFF))
motorTorque2BsForce = lambda (torque) : ((float)(torque)*PULLEY_REDUCTION*2*PI*DRIVETRAIN_EFF/BALLSCREW_LEAD)
linkage1LeverArm = lambda (angle_rad) : ( (B_LENGTH_M*C_LENGTH_M*sin((float)(angle_rad)))/sqrt( B_LENGTH_M*B_LENGTH_M + C_LENGTH_M*C_LENGTH_M - 2*B_LENGTH_M*C_LENGTH_M*cos((float)(angle_rad))) )
armTorque_nm2BsForce_N = lambda (torque, angle_rad) : ((float)(torque) / linkage1LeverArm(angle_rad)) #TODO: will be slightly off for actuator 2
actuatorForce2ArmTorque = lambda (force, angle_rad) : ((float)(force) * linkage1LeverArm(angle_rad)) #TODO: will be slightly off for actuator 2
armGravityTorque = lambda (angle_rad) : (COMBINED_ARM_COM_MASS*G*COMBINED_ARM_COM_DIST*sin( (float)(angle_rad)+COMBINED_ARM_COM_ANG_OFFSET+MOUNT_ANG_OFFSET) ) #negative from theta+180, negative from torque direction
motorCurrent2Torque = lambda (current) :  ((float)(current)*TORQUE_CONSTANT)
BsForce2motorCurrent = lambda (force) : motorTorque2current(BsForce2motorTorque((float)(force)))
motorCurrent2BsForce = lambda (current) : motorTorque2BsForce(motorCurrent2Torque((float)(current)))
voltage2LoadCellForce_N = lambda (voltage) : (((((float)(voltage) - LC_VOFFSET)/LC_GAIN)*LC_RATED_FORCE_N*-1)/(LC_SENSITIVITY*LC_VEXCITE))
voltage2EPower_W = lambda (voltage) : ( ((float)(voltage) - EPOWER_VOFFSET)*V_BUS / (EPOWER_GAIN*R_SENSE_OHMS))
voltage2Current = lambda (voltage) : ( ((float)(voltage) - EPOWER_VOFFSET) / (EPOWER_GAIN*R_SENSE_OHMS))
gravityCompForce = lambda (arm_angle_rad) : (armTorque_nm2BsForce_N( armGravityTorque(arm_angle_rad), (arm_angle_rad) ))



@ly.named_state_system("SEAState", ["d", "dd", "m", "md"])
class SEA(object):

    """SEA testbed model.
    base parameters:
    d = spring deflection (meters)
    m = reflected motor deflection (meters)
    x = arm angle, f = ball screw force.
        """

    def __init__(self, state0=(0, 0, 0, 0), sensor_noise=lambda :0.0, dynamics_noise=lambda :0.0):
        self.state = 0.0, (state0)

        self.sn = lambda: sensor_noise(self.get_t())
        self.dn = lambda: dynamics_noise(self.get_t())
        self.sensor_noise = BWN(self.sn, n=2, k=(1.0 * pi * sensor_noise.freq_hz))
        self.dynamics_noise = BWN(self.dn, n=2, k=(1.0 * pi * dynamics_noise.freq_hz))
        self.events = []
        self.par = ly.ParallelSystems([self, self.sensor_noise, self.dynamics_noise])

    def measure(self):
        return self.x, self.f

    def get_t(self):
        return self.t

    def control(self):
        x, f = self.measure()
        self.controller.arm_ang_rad=x;
        self.controller.actuator_force_n=f;
        self.controller.do_control()
        return self.controller.motor_current_des_amps

    def __call__(self, u=None):
        # let tau represent torques on the arm
        # let gamma represent torques on the motor's rotor
        tau_external = 0
        tau_friction = -ARM_FRICTION * xd
        tau_actuator = linkage1LeverArm(x) * f
        movement_1_inertia = 
        xdd = (tau_external+tau_friction+tau_actuator)/movement_1_inertia
        fdd = 0.0

        return (self.xd, xdd, self.fd, fdd)

    def sim(self, tf=100, n=1001):
        sys = self.par
        stepper = ly.runge_kutta4(sys, np.linspace(0, tf, n))
        try:
            for jump, time, state, guard in ly.hybrid_integrate(stepper):
                yield self
        except StopIteration as e:
            print e


def plot(tf=10, n=1001):
    fig = plt.figure(figsize=(6.5, 9))
    axs = [fig.add_subplot(4, 1, 1)]
    for i in range(1, 4):
        axs.append(fig.add_subplot(4, 1, 1 + i, sharex=axs[0]))
    for i in range(0, 4):
        axs[i].hold(True)
    axs[-1].set_xlabel(r"$t$", size=14)
    axs[0].set_ylabel(r"${x}$", size=14)
    axs[1].set_ylabel(r"$\dot{x}$", size=14)
    axs[2].set_ylabel(r"${f}$", size=14)
    axs[3].set_ylabel(r"$\dot{f}$", size=14)


    sys = SEA(state0=(-0.0, 20.0, 0, 0), sensor_noise = noise(tf, 1e1, 1.0), dynamics_noise = noise(tf, 1e0, 1.0e0))
    dat = np.array([[p.t] + list(p.state[1]) + list(p.est.full)+[p.measure()]
                    for p in sys.sim(tf=tf, n=n)])
    for i, ax in enumerate(axs):
        ax.plot([0,tf],[p.maxes[i]]*2, color = 'k')
        ax.plot([0,tf],[-p.maxes[i]]*2, color = 'k')
        ax.plot(dat[: , 0], dat[: , i + 1], zorder = 10)
        ax.autoscale(False)
        ax.plot(dat[: , 0], dat[: , i + 5])
    axs[0].plot(dat[:,0], dat[:, -1])
    fig.tight_layout()
    return fig

save = "plots/ident_%s.pdf"


def main():
    print "running"
    rand.seed(25)
    plot(tf=50.0, n=10001).savefig(save % "1")
    plt.show()

if __name__ == "__main__":
    main()
