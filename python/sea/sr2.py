import ly_utils as ly
from collections import namedtuple
import numpy as np
from math import *
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from plot_setup import *
from BiChatterer import BiChatterer
from filters import *


def sat(x):
    return -1. if x < -1. else 1. if x > 1. else x


def sg(x):
    return atan(pi * x) / pi


def sg(x):
    return -1. if x < -2. else 1. if x > 2. else (abs(x) - (x / 2.)**2) * np.sign(x)


def dsg(x):
    return 0. if x < -2. else 0. if x > 2. else (1. - abs(x) / 2.)

# xs = np.linspace(-3, 3, 1000)
# plt.plot(xs, [sg(x) for x in xs])
# plt.plot(xs, [dsg(x) for x in xs])
# plt.show()
# exit()


@ly.named_state_system("Integrator2State", ["x", "xd"])
class Integrator2(object):

    """Simple Integrator"""

    def __init__(self, state0=(0, 0), eta=100, k=1):
        self.state = 0.0, (state0)
        self.eta = eta
        self.k = k
        self.events = []
        self.char = butterworth_coeffs(2, k)

    def __call__(self, u=None):
        x, xd = self.state[1]
        k = self.k
        c = self.char
        xdd = -(xd * c[1] + x * c[0]) / c[2]
        return (xd, xdd)

    def __call__(self, u=None):
        x, xd = self.state[1]
        k = self.k
        c = self.char
        xdd = -(xd * c[1] + sg(x / 5.) * 5. * c[0]) / c[2]
        return (xd, xdd)

    def __call__(self, u=None):
        x, xd = self.state[1]
        k = self.k
        l1, l2 = 5.0, 1.4
        e1, e2 = 2.0, 1.45
        xdd = e2 * sat((- dsg(l1 * x / e1) * xd - e2 *
                        sg(l2 / e2 * (xd + e1 * sg(l1 / e1 * x)))) / e2)
        return (xd, xdd)

    def __call__(self):
        x, xd = self.state[1]
        k = self.k
        return (xd, sat(-xd - sat(x)))

    def __call__(self):
        x, xd = self.state[1]
        k = self.k
        return (xd, sg(-xd - sg(x)))

    def __call__(self):
        x, xd = self.state[1]
        k = self.k
        return (xd, np.sign(-xd - sg(x)))

    def __call__(self):
        x, xd = self.state[1]
        k = self.k
        return (xd, sat(-xd - sg(x)))
    def __call__(self):
        x, xd = self.state[1]
        k = self.k
        return (xd, np.sign(-xd - np.sign(x)))

    def __call__(self):
        x, xd = self.state[1]
        k = 8.0
        e=4.0
        v = 2.0
        return (xd, e*sat(-xd*k/e - v*k/e*sat(x*k/v)))

    @property
    def report(self):
        x, xd = self.state[1]
        xd, xdd = self()
        return x, xd, xdd

    def sim(self, tf=100, n=1001):
        sys = self
        stepper = ly.dormand_prince(sys, np.linspace(0, tf, n))
        try:
            for jump, time, state, guard in ly.hybrid_integrate(stepper):
                yield self
        except StopIteration as e:
            print e


def plot(tf=10, n=1001):
    fig = plt.figure(figsize=(5.5, 9))
    axs = [fig.add_subplot(4, 1, 1)]
    for i in range(1, 3):
        axs.append(fig.add_subplot(4, 1, 1 + i, sharex=axs[0]))
    axs[-1].set_xlabel(r"$t$", size=14)
    axs.append(fig.add_subplot(4, 1, 4))
    for i in range(0, 4):
        axs[i].hold(True)
        color_cycle(axs[i], n=9)
    axs[0].set_ylabel(r"${\xi}$", size=14)
    axs[1].set_ylabel(r"$\dot{\xi}$", size=14)
    axs[2].set_ylabel(r"$\ddot{\xi}$", size=14)
    axs[3].set_ylabel(r"$\dot{\xi}$", size=14)
    axs[3].set_xlabel(r"$\xi$", size=14)
    for xd0 in [-1.5, 0, 1.5]:
	    for x0 in np.linspace(-10, 10, 3):
	        sys = Integrator2(state0=(x0, xd0))
	        dat = np.array([[p.t] + list(p.report)
	                        for p in sys.sim(tf=tf, n=n)])
	        for i, ax in enumerate(axs):
	            if i == 3:
	                ax.plot(dat[:, 1], dat[:, 2])
	            else:
	                ax.plot(dat[:, 0], dat[:, i + 1])
    fig.tight_layout()
    return fig

save = "plots/sr3_%s.pdf"


def main():
    plot(tf=2e1, n=1001).savefig(save % "test")
    plt.show()

if __name__ == "__main__":
    main()
