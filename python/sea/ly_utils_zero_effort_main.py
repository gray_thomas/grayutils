import lyapunov as ly
import ly_utils as lu


@lu.named_state_system("SEA_State", ["x", 'xd', 't', 'td'])
class SEA_System(object):

    def __init__(self):
        self.state = 0, (0.0, 0.0, 0.0, 0.0)

    def __call__(self):
        return (0.0, 0.0, 0.0, 0.0)

if __name__ == '__main__':
	import matplotlib.pyplot as plt
	sys = SEA_System()
	step = ly.dormand_prince(sys, 1.0)
	step.relative_tolerance = 1e-8
	for i,j,c,d in lu.hybrid_integrate(step):
		print i,j,c,d

