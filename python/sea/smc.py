import ly_utils as ly
from collections import namedtuple
import numpy as np
from math import *
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from plot_setup import *
from BiChatterer import BiChatterer
from filters import *


def sg(x):
    return atan(pi * x) / pi


def dsg(x):
    return 1.0 / (1. + x**2)


def ddsg(x):
    return -2.0 * x / (1. + x**2)**2


def dddsg(x):
    return (6 * x**2 - 2.0) / (1. + x**2)**3


def sg_inv(x):
    return tan(pi * x) / pi


def dsg_inv(x):
    return 1.0 / (cos(pi * x)**2)

def sg(x):
    return -1. if x<-1. else 1. if x>1. else x


def dsg(x):
    return 0.0 if x<-1. else 0.0 if x>1. else 1.


def ddsg(x):
    return 0.0


def dddsg(x):
    return (6 * x**2 - 2.0) / (1. + x**2)**3


def sg_inv(x):
    return tan(pi * x) / pi


def dsg_inv(x):
    return 1.0 / (cos(pi * x)**2)

def safe_reciprocal(x):
    if x<1e-7:
        return 1e7 if x>0 else -1e7
    return 1.0/x


@ly.named_state_system("WrongIntegratorState", ["x", "xd", "xdd", "xddd"])
class WrongIntegrator(object):

    """Simple Integrator"""

    def __init__(self, state0=(0, 0, 0, 0), eta=100, k_s=0.75):
        self.state = 0.0, (state0)
        self.eta = eta
        self.k_s = k_s

        self.coeffs = butterworth_coeffs(3,k_s)
        self.max_values = [1., 1., 1.]
        self.events = []
        # self.chatterer = BiChatterer(
        #     self, self.sigma, self.del_sigma, "u")

    def sigma(self):
        return sum([a * b for a, b in zip(self.state[1], self.del_sigma())])

    def s1(self):
        x, xd, xdd, xddd = self.state[1]
        k = self.k_s
        return (
            x + sg(xd) / k,
            xd + dsg(xd) * xdd / k,
            xdd + dsg(xd) * xddd / k + ddsg(xd) * xdd**2 / k
            )

    # def J1(self):
    #     return np.array([1, dsg(self.xd) / self.k_s, 0, 0],
    #                     [0, 1, ddsg(self.xd) / self.k_s, 0],
    #                     [0, 0, 1, dddsg(self.xd) / self.k_s]])

    def del_sigma(self):  # assume factorization exists: (s/k+1)^3
        return np.array([1, 3.0 / self.k_s, 3.0 / self.k_s**2, 1. / self.k_s**3])

    def __call__(self, u=None):
        return (self.xd, self.xdd, self.xddd, -self.eta if u else self.eta)

    def sigma(self):
        x, xd, xdd, xddd = self.state[1]
        k = self.k_s
        c = self.coeffs
        m = self.max_values
        return self.xddd*c[3]+ sg(self.xdd*c[2]+sg(self.xd*c[1] + sg(self.x*c[0])))

    def sigma(self):
        x, xd, xdd, xddd = self.state[1]
        k = self.k_s
        c = self.coeffs
        m = self.max_values
        return self.xddd*c[3]+ (self.xdd*c[2]+(self.xd*c[1] + (self.x*c[0])))

    def __call__(self, u=None):
        return (self.xd, self.xdd, self.xddd, -self.eta *sg(self.sigma()))

    def sim(self, tf=100, n=1001):
        sys = self
        stepper = ly.runge_kutta4(sys, np.linspace(0, tf, n))
        try:
            for jump, time, state, guard in ly.hybrid_integrate(stepper):
                yield self
        except StopIteration as e:
            print e


def plot(tf=10, n=1001):
    fig = plt.figure(figsize=(5.5, 9))
    axs = [fig.add_subplot(5, 1, 1)]
    for i in range(1, 5):
        axs.append(fig.add_subplot(5, 1, 1 + i, sharex=axs[0]))
    for i in range(0, 5):
        axs[i].hold(True)
    axs[-1].set_xlabel(r"$t$", size=14)
    axs[0].set_ylabel(r"${\xi}$", size=14)
    axs[1].set_ylabel(r"$\dot{\xi}$", size=14)
    axs[2].set_ylabel(r"$\ddot{\xi}$", size=14)
    axs[3].set_ylabel(r"$\dddot{\xi}$", size=14)
    axs[4].set_ylabel(r"$\sigma{(\xi)}$", size=14)

    sys = WrongIntegrator(state0=(-1.0, 0, 0, 0))
    dat = np.array([[p.t] + list(p.state[1]) + [p.sigma()]
                    for p in sys.sim(tf=tf, n=n)])
    for i, ax in enumerate(axs):
        ax.plot(dat[: , 0], dat[: , i + 1])
    fig.tight_layout()
    return fig

save = "plots/smc_%s.pdf"


def main():
    plot(tf=10.0).savefig(save % "test")
    plt.show()

if __name__ == "__main__":
    main()
