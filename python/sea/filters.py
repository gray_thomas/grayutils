import ly_utils as ly
import matplotlib.pyplot as plt
from plot_setup import *
from BiChatterer import BiChatterer
from functools import partial
from math import sqrt
import random as rand
from math import cos, pi, sin


class noise(object):

    def __init__(self, tf, freq_hz, value):
        num = int(tf * freq_hz + 1)
        self.freq_hz = freq_hz
        n = lambda x: int(x * freq_hz)
        self.indexer = lambda x: (n(x) if n(x) > 0 else 0) if n(
            x) < num - 1 else num - 1
        self.dat = [value * (1. - 2. * rand.random()) for i in range(0, num)]

    def __call__(self, value):
        return self.dat[self.indexer(value)]


@ly.named_state_system("PFState", ["x", "xd", "xdd"])
class BW3(object):

    def __init__(self, func, k=10000):
        self.k = k
        self.state = 0, (func(), 0, 0)
        self.func = func

    @property
    def xddd(self):
        return self.k**3 * (self.func() - self.x - 2 *
                            (self.xd / self.k) - 2 * (self.xdd / self.k**2))

    @property
    def full(self):
        return self.x, self.xd, self.xdd, self.xddd

    def __call__(self):
        return self.xd, self.xdd, self.xddd

@ly.named_state_system("LPFState", ["x"])
class LPF(object):

    def __init__(self, func, k=1000):
        self.k = k
        self.state = 0, (func(),)
        self.func = func

    def __call__(self):
        return self.xd,

    @property
    def xd(self):
        return self.k * (self.func() - self.x)

@ly.named_state_system("PFState", ["x", "xd", "xdd", "xddd"])
class BW4(object):

    def __init__(self, func, k=10000):
        self.k = k
        self.state = 0, (func(), 0, 0, 0)
        self.func = func
        coeffs = [1 / k / k, 2 * cos(1 * pi / 8) / k, 1]
        coeffs2 = [1 / k / k, 2 * cos(3 * pi / 8) / k, 1]
        coeffs3 = [0] * 5
        for i in range(0, 3):
            for j in range(0, 3):
                coeffs3[i + j] += coeffs2[j] * coeffs[i]
        self.coeffs = coeffs3

    @property
    def xdddd(self):
        return (
            - self.coeffs[4] * (self.x - self.func())
            - self.coeffs[3] * self.xd
            - self.coeffs[2] * self.xdd
            - self.coeffs[1] * self.xddd) / self.coeffs[0]

    # @property
    # def full(self):
    #     return self.x, self.xd, self.xdd, self.xddd, self.xdddd

    @property
    def full(self):
        return self.state[1]

    def __call__(self):
        return self.xd, self.xdd, self.xddd, self.xdddd


def poly_mult(coeffs1, coeffs2):
    coeffs3 = [0] * (len(coeffs1) + len(coeffs2) - 1)
    for i in range(0, len(coeffs1)):
        for j in range(0, len(coeffs2)):
            coeffs3[i + j] += coeffs2[j] * coeffs1[i]
    return coeffs3

def butterworth_coeffs(n,k):
    if n % 2 == 0:
        coeffs_list = [
            [1 / k / k, 2 * cos((2 * l + 1) * pi / (2 * n)) / k, 1] for l in range(0, n / 2)]
    else:
        coeffs_list = [[1 / k / k, 2 * cos((l + 1) * pi / n) / k, 1]
                       for l in range(0, (n - 1) / 2)] + [[1 / k, 1]]
    return reduce(poly_mult, coeffs_list)

def lp_coeffs(n,k):
    coeffs_list = [[1 / k ,  1]]*n
    return reduce(poly_mult, coeffs_list)

class BWN(object):
    """ Nth order butterworth filter in companion form. """

    def __init__(self, func, n=4, k=10000):
        self.k = k
        self.n = n
        self.state = 0, tuple([func()] + [0] * (n - 1))
        self.func = func
        self.coeffs = butterworth_coeffs(n,k)

    @property
    def x(self):
        return self.state[1][0]

    @property
    def t(self):
        return self.state[0]

    @property
    def max_der(self):
        return (
            self.func() * self.coeffs[-1]
            - sum([self.coeffs[self.n - i] * self.state[1][i]
                   for i in range(0, self.n)])
        ) / self.coeffs[0]

    @property
    def full(self):
        return self.state[1]

    def __call__(self):
        return tuple([x for i, x in enumerate(self.state[1]) if i > 0] + [self.max_der])





@ly.named_state_system("DiffHDState", ["o_a", "o_f"])
class DiffHD(object):

    def __init__(self, hd, la, lf):
        self.state = 0, (0, 0)
        self.la = la
        self.lf = lf
        self.hd = hd

    @property
    def est(self):
        o_a, o_f, i_a, i_f = self.hd.measurement()
        return i_a, o_a, self.la * (o_a - self.o_a), i_f, o_f, self.lf * (o_f - self.o_f)

    def __call__(self):
        o_a, o_f, i_a, i_f = self.hd.measurement()
        return self.la * (o_a - self.o_a), self.lf * (o_f - self.o_f)


@ly.named_state_system("PFState", ["i_a", "o_a", "w_a", "i_f", "o_f", "w_f"])
class ELO(object):

    def __init__(self, sys, F):
        self.sys = sys
        self.F = F
        o_a, o_f, i_a, i_f = sys.measurement()
        t, (i_a, o_a, w_a, i_f, o_f, w_f) = sys.state  # start cheating.
        self.state = t, (i_a, o_a + 1.0, w_a + 6.0, i_f, o_f, w_f + 6.0)
        self.meas = lambda: (self.o_a, self.o_f, self.i_a, self.i_f)

    @property
    def est(self):
        return model_hd.convert_state(self.state[1])

    @property
    def orig(self):
        return self.state[1]

    def __call__(self):
        err = np.array(
            [z_est - z for z, z_est in zip(self.sys.measurement(), self.meas())]).reshape((-1, 1))
        update = self.F.T.dot(err)
        diff = model_hd.get_dynamics(
            self.orig, (0, 0), self.sys.voltages(), sign_w_a=self.w_a > 0)
        return tuple([Ax - kcx for Ax, kcx in zip(diff, update.reshape((-1)))])

def main():
    print butterworth_coeffs(4,1.0)
    print lp_coeffs(4,1.0)
    print butterworth_coeffs(6,1.0)
    sys = BWN(lambda: 2.0, n=6, k=10.0e0)
    print sys.state
    print sys()
    sys.state = 0.0, (0,0,0,0,0,0)
    print sys()
    print sys.full
    print sys.coeffs

if __name__ == '__main__':
    main()
