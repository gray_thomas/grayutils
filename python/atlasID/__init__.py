import os
import numpy as np
import matplotlib.pyplot as plt


plt.rc('text', usetex=True)
plt.rc('font', family='serif')

data = os.environ['GRAY_UTILS_DATA'] + '/atlasID/l_leg_kny_tr.csv'
dat = np.loadtxt(data, delimiter=", ", skiprows=1, usecols=range(0, 5))
#  IN_p, IN_v, IN_f, IN_tr, OUT_cmd
IN_p = dat[:, 0]
IN_v = dat[:, 1]
IN_f = dat[:, 2]
IN_tr = dat[:, 3]
OUT_cmd = dat[:, 4]
pistonVelocity = np.array([v * tr for (v, tr) in zip(IN_v, IN_tr)]).reshape((-1, 1))
# pistonVelocity = IN_v.reshape((-1, 1))
pistonTorque = IN_f.reshape((-1, 1))
# pistonTorque = np.array([v * tr for (v, tr) in zip(IN_f, IN_tr)]).reshape((-1, 1))
# pistonTorque = np.array([f*1.0/150.0 / tr for (f, tr) in zip(IN_f, IN_tr)]).reshape((-1, 1))
jointPosition=IN_p.reshape((-1,1))
commandU = OUT_cmd.reshape((-1, 1))
unity = np.ones((pistonVelocity.shape[0], 1))
print pistonVelocity.shape
R = np.concatenate((jointPosition,pistonVelocity, pistonTorque, unity), axis=1)
sol = np.linalg.solve(((R.T).dot(R)), (R.T).dot(commandU))
print sol
plt.figure()
plt.setp(plt.plot(commandU, "b-"), linewidth=2.0)
# plt.setp(plt.plot(IN_p, "g-"), linewidth=2.0)
# plt.setp(plt.plot(IN_v, "y-"), linewidth=2.0)
# plt.setp(plt.plot(IN_f*1.0/150.0, "m-"), linewidth=2.0) #huge
# plt.setp(plt.plot(IN_tr, "k-"), linewidth=2.0)
plt.setp(plt.plot(R.dot(sol), "g-"), linewidth=2.0)
plt.ylabel(r"$\rm{Force}$")
plt.xlabel(r"$\rm{Control\;Tic}$")
plt.legend((r"u",r"u estimate"))

plt.show()
print sol
