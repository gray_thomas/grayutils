import math
import numpy as np

def integrate(A,x0,tf=1.0,n=100,subn=100):
    xdim=A.shape[0]
    t=np.linspace(0.0,tf,n)
    dt=t[1]-t[0]
    Aint=np.eye(xdim)
    for i in range(0,subn):
        Aint+=(1.0/subn)*dt*A.dot(Aint)
    x=np.zeros((xdim,n))
    x[:,0]=x0
    for i in range(1,n):
        x[:,i]= Aint.dot(x[:,i-1])
    return t,x

def simulate_state_feedback_A(A,B,K,x0,tf=1.0,n=100,subn=100):
    xdim=A.shape[0]
    udim=B.shape[1]
    t=np.linspace(0.0,tf,n)
    dt=t[1]-t[0]
    Aint=np.eye(xdim)
    Bint=np.zeros((xdim,udim))
    for i in range(0,subn):
        Aint+=(1.0/subn)*dt*A.dot(Aint)
        Bint+=(1.0/subn)*dt*Aint.dot(B)
    x=np.zeros((xdim,n))
    x[:,0]=x0
    for i in range(1,n):
        x[:,i]= Aint.dot(x[:,i-1])+Bint.dot(K).dot(x[:,i-1])
    return t,x,u

def simulate_state_feedback_B(A,B,K,x0,tf=1.0,n=100,subn=100):
    t,x=integrate(A+B.dot(K),x0,tf,n,subn)
    return t,x,K.dot(x)

def simulate_state_feedback_with_reference(
    A,B,K,x0,get_x_ref,get_u_ref,tf=1.0,n=100):
    xdim=A.shape[0]
    udim=B.shape[1]
    ts=np.linspace(0.0,tf,n)
    dt=ts[1]-ts[0]
    Aint=np.eye(xdim)
    x=np.zeros((xdim,n))
    xref=np.zeros((xdim,n-1))
    dx=np.zeros((xdim,n-1))
    u=np.zeros((udim,n-1))
    uref=np.zeros((udim,n-1))
    du=np.zeros((udim,n-1))
    x[:,[0]]=x0
    for i in range(1,n):
        t = ts[i-1]
        xref[:,[i-1]] = get_x_ref(t)
        uref[:,[i-1]] = get_u_ref(t)
        dx[:,[i-1]] = x[:,[i-1]] - xref[:,[i-1]]
        du[:,[i-1]] = -K.dot(dx[:,[i-1]])
        u[:,[i-1]] = uref[:,[i-1]] + du[:,[i-1]]
        xdot = A.dot(x[:,[i-1]]) + B.dot(u[:,[i-1]])
        x[:,[i]] = x[:,[i-1]] + dt*xdot
    return ts,x,u,xref,uref,dx,du