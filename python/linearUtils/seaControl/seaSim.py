import control  
import numpy as np
import scipy
import matplotlib.pyplot as plt

def get_state_space_model(a1,a2,k1,k2,k3,k4):

    l1=-a1*k1
    l2=-a1*k2
    l3=a1-a1*k3
    l4=a2-a1*k4

    matrixA = np.array([
        [0.0, 1.0, 0.0, 0.0], 
        [0.0, 0.0, 1.0, 0.0], 
        [0.0, 0.0, 0.0, 1.0], 
        [-l1, -l2, -l3, -l4]
        ])
    matrixB = np.array([
        [0.0, 0.0], 
        [0.0, 1.0], 
        [0.0, 0.0], 
        [a1, 0.0]
        ])
    matrixC = np.array([
        [1.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 1.0, 0.0], 
        [ k1,  k2,  k3,  k4]
        ])
    matrixD = np.zeros((3, 2))
    stateSpaceSEA = control.ss(matrixA, matrixB, matrixC, matrixD)
    print stateSpaceSEA
    return stateSpaceSEA

def get_open_loop_model(a1,a2):
    matrixA = np.array([
        [0.0, 1.0, 0.0, 0.0], 
        [0.0, 0.0, 1.0, 0.0], 
        [0.0, 0.0, 0.0, 1.0], 
        [0.0, 0.0, -a1, -a2]
        ])
    matrixB = np.array([
        [0.0], 
        [0.0], 
        [0.0], 
        [ a1]
        ])
    matrixC = np.array([
        [1.0, 0.0, 0.0, 0.0]
        ])
    matrixD = np.zeros((1, 1))
    stateSpaceSEA = control.ss(matrixA, matrixB, matrixC, matrixD)
    print stateSpaceSEA
    return stateSpaceSEA
def get_open_full_state_model(a1,a2):
    matrixA = np.array([
        [0.0, 1.0, 0.0, 0.0], 
        [0.0, 0.0, 1.0, 0.0], 
        [0.0, 0.0, 0.0, 1.0], 
        [0.0, 0.0, -a1, -a2]
        ])
    matrixB = np.array([
        [0.0], 
        [0.0], 
        [0.0], 
        [ a1]
        ])
    matrixC = np.eye(4)
    matrixD = np.zeros((4, 1))
    stateSpaceSEA = control.ss(matrixA, matrixB, matrixC, matrixD)
    print stateSpaceSEA
    return stateSpaceSEA

def get_perfect_force_model():
    matrixA = np.array([
        [0.0, 1.0], 
        [0.0, 0.0]        
        ])
    matrixB = np.array([ 
        [0.0], 
        [1.0]
        ])
    matrixC = np.array([
        [1.0, 0.0]
        ])
    matrixD = np.zeros((1, 1))
    stateSpaceSEA = control.ss(matrixA, matrixB, matrixC, matrixD)
    print stateSpaceSEA
    return stateSpaceSEA

def get_perfect_force_model_fb(kp,kd):
    matrixA = np.array([
        [0.0, 1.0], 
        [-kp, -kd]        
        ])
    matrixB = np.array([ 
        [0.0], 
        [1.0]
        ])
    matrixC = np.array([
        [1.0, 0.0]
        ])
    matrixD = np.zeros((1, 1))
    stateSpaceSEA = control.ss(matrixA, matrixB, matrixC, matrixD)
    print stateSpaceSEA
    return stateSpaceSEA

def static_matrix(D):
    outs,ins=D.shape
    return control.ss(np.zeros((0,0)),np.zeros((0,ins)),np.zeros((outs,0)),D)

def get_act_dynamics_alone(a1,a2):
    matrixA = np.array([
        [0.0, 1.0], 
        [-a1, -a2]        
        ])
    matrixB = np.array([ 
        [0.0], 
        [a1]
        ])
    matrixC = np.array([
        [1.0, 0.0]
        ])
    matrixD = np.zeros((1, 1))
    actuatorSS = control.ss(matrixA, matrixB, matrixC, matrixD)
    print actuatorSS
    return actuatorSS

def bode_plot(ax_mag, ax_phase, tf, **kwargs):
    w, mag, phase = scipy.signal.bode(tf, **kwargs)
    ax_mag.semilogx(w,mag)
    phase=[p if p<=0.0 else p-360.0 for p in phase]
    pdiff=[pd if pd <200.0 else pd-360.0 for pd in [p1 - p2 for (p1, p2 ) in zip(phase[1:],phase[:-1])]]
    for i in range(1,len(phase)):
        phase[i]=phase[i-1]+pdiff[i-1]
    ax_phase.semilogx(w,phase)

def pz_plot(ax, tf):
    z, p, k = scipy.signal.ss2zpk(tf.A,tf.B,tf.C,tf.D)
    ax.plot(z.real, z.imag, 'bo', fillstyle='none')
    ax.plot(p.real, p.imag, 'bx')

def step_plot(ax, tf, **kwargs):
    T, yout = scipy.signal.step2(tf, **kwargs)
    ax.plot(T, yout)

def main():
    lambda_1 = 2.0
    lambda_2 = 3.20
    ol_model=get_open_full_state_model(lambda_1*lambda_2,lambda_1+lambda_2)
    act_model=get_act_dynamics_alone(lambda_1*lambda_2,lambda_1+lambda_2)
    ol_simple=get_perfect_force_model()
    Q=np.array([
        [1.0,0.0,0.0,0.0],
        [0.0,0.0,0.0,0.0],
        [0.0,0.0,0.0,0.0],
        [0.0,0.0,0.0,0.0]
        ])
    Q_simple=np.zeros((2,2))
    Q_simple[0,0]=1.0
    R=np.array([
        [1.1]
    ])
    # print dir(ol_model)
    X_full,Lambda_full,K_full=control.care(ol_model.A,ol_model.B, Q, R)
    print "eigenvalues of the open loop matrix", np.linalg.eig(ol_model.A)[0]
    print "eigenvalues of the open loop actuators", np.linalg.eig(act_model.A)[0]
    print "X_full" , X_full
    print "Lambda_full", Lambda_full
    print "K_full", K_full
    X_simple,Lambda_simple, K_simple = control.care(ol_simple.A,ol_simple.B, Q_simple, R)
    print "X_simple" , X_simple
    print "Lambda_simple", Lambda_simple
    print "K_simple", K_simple

    cls1=ol_model.feedback(static_matrix(K_full))
    # cls2=ol_model.feedback(K_simple)
    closed_loop_full_system=get_state_space_model(lambda_1*lambda_2,lambda_1+lambda_2,
        -K_full[0,0], -K_full[0,1], -K_full[0,2], -K_full[0,3])

    print "cls1", cls1.returnScipySignalLti()[0][0]
    print "closed_loop_full_system", closed_loop_full_system.returnScipySignalLti()[0][0]
    closed_loop_simple_model=get_state_space_model(lambda_1*lambda_2,lambda_1+lambda_2,
        -K_simple[0,0], -K_simple[0,1], 0.0,0.0)

    cl_simp=get_perfect_force_model_fb(K_simple[0,0], K_simple[0,1])

    fig=plt.figure(figsize=(20,15))

    axs=[fig.add_subplot(4,3,0+1)]
    for i in range (1,3):
        axs.append(fig.add_subplot(4,3,i+1,sharex=axs[0],sharey=axs[0]))
    axs.append(fig.add_subplot(4,3,3+1,sharex=axs[0]))
    for i in range (4,6):
        axs.append(fig.add_subplot(4,3,i+1,sharex=axs[0],sharey=axs[3]))
    axs.append(fig.add_subplot(4,3,6+1))
    for i in range (7,9):
        axs.append(fig.add_subplot(4,3,i+1,sharex=axs[6],sharey=axs[6]))
    axs.append(fig.add_subplot(4,3,9+1))
    for i in range (10,12):
        axs.append(fig.add_subplot(4,3,i+1,sharex=axs[9],sharey=axs[9]))
    # axs=[fig.add_subplot(4,3,i) for i in range(1,13)]

    bode_plot(axs[0],axs[3],cl_simp.returnScipySignalLti()[0][0])
    bode_plot(axs[1],axs[4],closed_loop_simple_model.returnScipySignalLti()[0][0],w=np.logspace(-1,1,1000))
    bode_plot(axs[2],axs[5],closed_loop_full_system.returnScipySignalLti()[0][0])
    axs[0].set_autoscaley_on(False)
    axs[0].set_ylim([-80.0,20.0])
    axs[0].set_xlim([.1,10.0])
    axs[0].set_ylabel("Closed Loop Magnitude (dB)")
    axs[3].set_ylim([-360.0,0.0])
    axs[3].set_ylabel("Closed Loop Phase (degrees)")
    pz_plot(axs[6], cl_simp.returnScipySignalLti()[0][0])
    pz_plot(axs[7], closed_loop_simple_model.returnScipySignalLti()[0][0])
    pz_plot(axs[8], closed_loop_full_system.returnScipySignalLti()[0][0])
    axs[6].set_autoscaley_on(False)
    axs[6].set_ylim([-1.5,1.5])
    axs[6].set_xlim([-5.0,1.0])
    axs[6].set_ylabel("Closed Loop Poles")
    step_plot(axs[9], cl_simp.returnScipySignalLti()[0][0])
    step_plot(axs[10], closed_loop_simple_model.returnScipySignalLti()[0][0],T=np.linspace(0,10,1000))
    step_plot(axs[11], closed_loop_full_system.returnScipySignalLti()[0][0])
    axs[9].set_autoscaley_on(False)
    axs[9].set_ylim([0.0,1.5])
    axs[9].set_xlim([0.0,10.0])
    axs[9].set_ylabel("Closed Loop Step Response")
    axs[0].set_title("WBC with perfect actuators")
    axs[1].set_title("WBC with second order actuators")
    axs[2].set_title("AA-WBC with second order actuators")
    fig.tight_layout()
    plt.show()
    fig.savefig("AA-WBC.pdf")
    fig.savefig("AA-WBC.png")

    # print scipy.signal.bode(tf_impedance)

if __name__=="__main__":
    main()