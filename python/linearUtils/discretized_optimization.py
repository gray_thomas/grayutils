import numpy as np
import cvxpy as cvx
import math


def discretized_optimal_control(A,B,C,r,x0,tf=1.0,n=100):
    xdim=A.shape[0]
    udim=B.shape[1]
    R=r.T .dot(r)
    Q=C.T .dot(C)
    t=np.linspace(0.0,tf,n)
    dt=t[1]-t[0]
    subn=100
    Aint=np.eye(xdim)
    Bint=np.zeros((xdim,udim))
    for i in range(0,subn):
        Aint+=(1.0/subn)*dt*A.dot(Aint)
        Bint+=(1.0/subn)*dt*Aint.dot(B)
    A=cvx.Constant(A)
    Aint=cvx.Constant(Aint)
    B=cvx.Constant(B)
    Bint=cvx.Constant(Bint)
    Q=cvx.Constant(Q)
    R=cvx.Constant(R)
    x0=cvx.Constant(x0)
    x=cvx.Variable(xdim,n)
    u=cvx.Variable(udim,n)

    dt=cvx.Constant(dt)

    expQ=cvx.quad_form(x[:,0],Q)
    expR=cvx.quad_form(u[:,0],R)
    physical_constraints=[x[:,0]==x0]
    for i in range(1,n):
        expQ+=cvx.quad_form(x[:,i],Q)
        expR+=cvx.quad_form(u[:,i],R)
        constraint=x[:,i] == Aint*x[:,i-1]+Bint*u[:,i-1]
        # constraint=x[:,i] == x[:,i-1]+dt*(A*x[:,i-1]+B*u[:,i-1])
        physical_constraints.append(constraint )
    obj=cvx.Minimize(expQ+expR)
    prob=cvx.Problem(obj,physical_constraints)
    return prob.solve(),t,x,u

def discretized_optimal_control_end(A,B,C,r,x0,Ce,xde,tf=1.0,n=100):
    """ this is a function I had to mess up a bit to make work """
    """ this function returns the discretized input signal which 
        optimally fulfils a quadratic performance index with a 
        terminal cost on the state """
    """ my modification, which I don't really know how to standardize
        to proper programming form is to include an inequality on 
        a seemingly arbitrary component of the state vector
        this component represent one row of the A matrix
        and approximates a constraint that the height should
        always decrease at least 1.7 ft/second """
    xdim=A.shape[0]
    udim=B.shape[1]
    R=r.T .dot(r)
    Q=C.T .dot(C)
    Qe=Ce.T.dot(Ce)
    t=np.linspace(0.0,tf,n)
    dt=t[1]-t[0]
    subn=100
    Aint=np.eye(xdim)
    Bint=np.zeros((xdim,udim))
    for i in range(0,subn):
        Aint+=(1.0/subn)*dt*A.dot(Aint)
        Bint+=(1.0/subn)*dt*Aint.dot(B)
    A=cvx.Constant(A)
    Aint=cvx.Constant(Aint)
    B=cvx.Constant(B)
    Bint=cvx.Constant(Bint)
    Q=cvx.Constant(Q)
    Qe=cvx.Constant(Qe)
    R=cvx.Constant(R)
    x0=cvx.Constant(x0)
    xde=cvx.Constant(xde)
    x=cvx.Variable(xdim,n)
    u=cvx.Variable(udim,n)

    dt=cvx.Constant(dt)

    expQ=cvx.quad_form(x[:,0],Q)
    expR=cvx.quad_form(u[:,0],R)
    physical_constraints=[x[:,0]==x0]
    for i in range(1,n):
        expQ+=cvx.quad_form(x[:,i],Q)
        expR+=cvx.quad_form(u[:,i],R)
        constraint=x[:,i] == Aint*x[:,i-1]+Bint*u[:,i-1]
        # constraint=x[:,i] == x[:,i-1]+dt*(A*x[:,i-1]+B*u[:,i-1])
        physical_constraints.append(constraint )
        physical_constraints.append(
            -1.0 *x[1,i]+2.21*x[3,i] <= -1.7
            )
    obj=cvx.Minimize(dt*expR+cvx.quad_form(x[:,-1]-xde,Qe))
    prob=cvx.Problem(obj,physical_constraints)
    return prob.solve(),t,x,u

def find_K(x,u):
    # print np.linalg.lstsq.__doc__
    K,res,dim,sv= np.linalg.lstsq( x.T,u.T)
    print sv
    return -K.T

def approximate_K(A1,B1,C1,r1,tf,n):
    bigx=[]
    bigu=[]
    size_X=A1.shape[0]
    for i in range(0,size_X):
        x0t=np.zeros((size_X,1))
        x0t[i,0]=1.0
        valt,t,xt,ut=discretized_optimal_control(A1,B1,C1,r1,x0t,tf=tf,n=n)
        bigx.append(xt.value)
        bigu.append(ut.value)
    biggx=np.concatenate(bigx,axis=1)
    biggu=np.concatenate(bigu,axis=1)
    print find_K(biggx,biggu)
    print K
    return K