import matplotlib.pyplot as plt
import math
import numpy as np
def discretized_plot(ax,t,u,fmt="",**kwargs):
    nt=[t[0]]
    nu=[u[0,0]]
    for i in range(1,len(t)):
        nt.append(t[i])
        nu.append(u[i-1,0])
        nt.append(t[i])
        nu.append(u[i,0])
    ax.plot(nt,nu,fmt,**kwargs)
def discretized_plot_2(ax,t,u,fmt="",**kwargs):
    """ for cases when u is shorter than t """
    nt=[t[0]]
    nu=[u[0,0]]
    for i in range(1,len(t)):
        nt.append(t[i-1])
        nu.append(u[i-1,0])
        nt.append(t[i])
        nu.append(u[i-1,0])
    ax.plot(nt,nu,fmt,**kwargs)



def generic_solution_plot(t,x,u,ncols=2,figsize=(18,10),fmt="",**kwargs):
    fontsize=15
    xdim=x.size[0]
    udim=u.size[0]
    fig, axs, rows=gen_axes(xdim,udim,ncols,figsize)
    r=0
    c=0
    for i in range(0,xdim+udim):
        # print r,c, axs[r]
        ax=axs[r][c]
        if i<xdim:
            discretized_plot(ax,t,np.array(x.value[i,:]).T,fmt,**kwargs)
            # ax.plot(t,np.array(x.value[i,:]).T)
            ax.set_xlabel("$t$",fontsize=fontsize)
            ax.set_ylabel("$x_%s$"%i,fontsize=fontsize)
        else:
            discretized_plot(ax,t,np.array(u.value[i-xdim,:]).T,fmt,**kwargs)
            ax.set_xlabel("$t$",fontsize=fontsize)
            ax.set_ylabel("$u_%s$"%i,fontsize=fontsize)
        r+=1
        if r>=rows:
            r-=rows
            c+=1
    plt.tight_layout()
    return fig

def np_solution_plot(t,x,u,ncols=2,minrows=1,figsize=(18,10),fmt="",**kwargs):
    fontsize=15
    xdim=x.shape[0]
    udim=u.shape[0]
    fig, axs, rows=gen_axes(xdim,udim,ncols,figsize,minrows=minrows)
    r=0
    c=0
    for i in range(0,xdim+udim):
        # print r,c, axs[r]
        ax=axs[r][c]
        if i<xdim:
            discretized_plot(ax,t,x[[i],:].T,fmt,**kwargs)

            # ax.plot(t,np.array(x.value[i,:]).T)
            ax.set_xlabel("$t$",fontsize=fontsize)
            ax.set_ylabel("$x_%s$"%i,fontsize=fontsize)
        else:
            discretized_plot_2(ax,t,u[[i-xdim],:].T,fmt,**kwargs)
            ax.set_xlabel("$t$",fontsize=fontsize)
            ax.set_ylabel("$u_%s$"%i,fontsize=fontsize)
        r+=1
        if r>=rows:
            r=0
            c+=1
    plt.tight_layout()
    return fig, axs

def gen_axes(xdim,udim,ncols,figsize,minrows=1):
    rows=max(int(math.ceil((xdim+udim)/(1.0*ncols))), minrows)
    numplots=xdim+udim
    fig=plt.figure(figsize=figsize,facecolor="white")
    axs=[[None for n in range(0,ncols)] for q in range(0,rows)]
    axs[0][0]=fig.add_subplot(rows,ncols,1)
    r=0
    c=0
    for i in range(1,numplots):
        r+=1
        if r>=rows:
            r=0
            c+=1
        axs[r][c]=fig.add_subplot(rows,ncols,1+ncols*r+c,sharex=axs[0][0])


    return fig,axs,rows

def comparison_solution_plot(t,x1,x2,u1,u2,ncols=2,fmt1="b",fmt2="r",figsize=(18,10),**kwargs):
    fontsize=15
    xdim=max(x1.size[0],x2.size[0])
    udim=max(u1.size[0],u2.size[0])
    fig, axs, rows=gen_axes(xdim,udim,ncols,figsize)
    r=0
    c=0
    for i in range(0,xdim+udim):
        print r,c, axs[r]
        ax=axs[r][c]
        if i<xdim:
            try:
                discretized_plot(ax,t,np.array(x1.value[i,:]).T,fmt1,**kwargs)
            except IndexError as e:
                pass
            try:
                discretized_plot(ax,t,np.array(x2.value[i,:]).T,fmt2,**kwargs)
            except IndexError as e:
                pass
            # ignore error
            # ax.plot(t,np.array(x.value[i,:]).T)
            ax.set_xlabel("$t$",fontsize=fontsize)
            ax.set_ylabel("$x_%s$"%i,fontsize=fontsize)
        else:
            try:
                discretized_plot(ax,t,np.array(u1.value[i-xdim,:]).T,fmt1,**kwargs)
            except IndexError as e:
                pass
            try:
                discretized_plot(ax,t,np.array(u2.value[i-xdim,:]).T,fmt2,**kwargs)
            except IndexError as e:
                pass
            ax.set_xlabel("$t$",fontsize=fontsize)
            ax.set_ylabel("$u_%s$"%i,fontsize=fontsize)
        r+=1
        if r>=rows:
            r-=rows
            c+=1
    plt.tight_layout()
    return fig

def plot_ref_actual_comparison(ts, x, u, xref, uref, x_names, u_names,
        ncols=2, figsize=(18,10), fontsize=14):
    xdim=x.shape[0]
    print xdim
    udim=u.shape[0]
    fig, axs, rows=gen_axes(xdim,udim,ncols,figsize)
    r=0
    c=0
    for i in range(0,xdim+udim):
        # print r,c, axs[r]
        ax=axs[r][c]
        if i<xdim:
            ax.plot(ts,x[i,:],linewidth=3)
            ax.plot(ts[:-1],xref[i,:],linewidth=3)
            # ax.plot(t,np.array(x.value[i,:]).T)
            ax.set_xlabel("$t$",fontsize=fontsize)
            ax.set_ylabel(x_names[i],fontsize=fontsize)
            ax.legend(["actual","reference"],loc='best')
        else:
            discretized_plot_2(ax,ts,np.array(u[[i-xdim],:]).T,linewidth=3)
            discretized_plot_2(ax,ts,np.array(uref[[i-xdim],:]).T,linewidth=3)
            ax.set_xlabel("$t$",fontsize=fontsize)
            ax.set_ylabel(u_names[i-xdim],fontsize=fontsize)
            ax.legend(["actual","reference"],loc='best')
        r+=1
        if r>=rows:
            r-=rows
            c+=1
    plt.tight_layout()
    return fig
def plot_bonus_comparison(ts, xs, us, xref, uref, type_names, x_names, u_names,
        ncols=2, figsize=(18,10), fontsize=14):
    xdim=xs[0].shape[0]
    print xdim
    appended_type_names=type_names[:]
    appended_type_names.append("reference")
    udim=us[0].shape[0]
    fig, axs, rows=gen_axes(xdim,udim,ncols,figsize)
    r=0
    c=0
    for i in range(0,xdim+udim):
        # print r,c, axs[r]
        ax=axs[r][c]
        if i<xdim:
            for x in xs:
                ax.plot(ts,x[i,:],linewidth=3)
            ax.plot(ts[:-1],xref[i,:],linewidth=3)
            # ax.plot(t,np.array(x.value[i,:]).T)
            ax.set_xlabel("$t$",fontsize=fontsize)
            ax.set_ylabel(x_names[i],fontsize=fontsize)
            # ax.legend(appended_type_names,loc='best')
        else:
            for u in us:
                discretized_plot_2(ax,ts,np.array(u[[i-xdim],:]).T,linewidth=3)
            discretized_plot_2(ax,ts,np.array(uref[[i-xdim],:]).T,linewidth=3)
            ax.set_xlabel("$t$",fontsize=fontsize)
            ax.set_ylabel(u_names[i-xdim],fontsize=fontsize)
            if i == xdim+udim-1:
                ax.legend(appended_type_names,bbox_to_anchor=(0.8, -0.25))

        r+=1
        if r>=rows:
            r-=rows
            c+=1
    plt.tight_layout()
    return fig


def latex_matrix(mat):
    mat_latex=r"\begin{bmatrix}"
    for row in range(0,mat.shape[0]):
        if row != 0:
            mat_latex=mat_latex+(r" \\ ")
        for col in range(0,mat.shape[1]):
            if col != 0:
                mat_latex=mat_latex+(r" & ")
            mat_latex=mat_latex+(str(mat[row,col]))
    mat_latex=mat_latex+(r"\end{bmatrix}")
    return mat_latex