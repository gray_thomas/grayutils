
import control  
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import sympy as sym
import sympy.matrices as mat
from cmath import *
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import math as realMath

def reverseComplexArrayAndCastToReal(array):
    ret = np.empty(len(array))
    for i in range(0, len(array)):
        ret[i] = (array[len(array) - 1 - i]).real
    return ret

def roots2poly(rootsArray):
    roots = np.array(rootsArray, complex)
    order = len(roots) + 1
    poly = np.zeros(order, complex)
    temp = np.zeros(order - 1, complex)
    poly[0] = 1.0
    for i in range(0, len(roots)):
        if roots[i] != 0:
            temp[0:i + 1] = poly[0:i + 1]
            poly[1:i + 2] -= temp[0:i + 1] / roots[i]
        else:
            temp[0:i + 1] = poly[0:i + 1]
            poly[1:i + 2] = temp[0:i + 1]
            poly[0] = 0
    return reverseComplexArrayAndCastToReal(poly)

def pzk2tf(pArray, zArray, gain):
    G = control.tf(roots2poly(zArray), roots2poly(pArray))
    k = control.tf(gain, 1)
    return G * k

def rootsOfQuadratic(a, b, c):
    det = sqrt(b * b - 4 * a * c)
    root1 = (-b + det) / (2.0 * a)
    root2 = (-b - det) / (2.0 * a)
    return [root1, root2]

def bisection(test, trueVal, falseVal, tol):
    assert(test(trueVal))
    assert(not test(falseVal))
    testVal = (trueVal + falseVal) / 2
    if (abs(testVal - trueVal) < tol):
        return testVal
    if test(testVal):
        return bisectionNoTest(test, testVal, falseVal, tol)
    else:
        return bisectionNoTest(test, trueVal, testVal, tol)
        
def bisectionNoTest(test, trueVal, falseVal, tol):
    testVal = (trueVal + falseVal) / 2
    if (abs(testVal - trueVal) < tol):
        return testVal
    if test(testVal):
        return bisection(test, testVal, falseVal, tol)
    else:
        return bisection(test, trueVal, testVal, tol)

def isHurwitz(transfer):
    poles = transfer.pole()
    test = [x.real < 0.0 for x in poles]
    return all(test)
def howHurwitz(transfer):
    poles = transfer.pole()
    test = [x.real for x in poles]
    return max(test)

def getStep():
    return pzk2tf([0], [], 1)

def nyquistSamples(transferFunction, timePeriodLowerBound, timePeriodUpperBound):
    freqMax = max([abs(p)for p in transferFunction.pole()])
    nyquistTimeStep = 0.5 / freqMax
    nyquistSampleNumber = ((timePeriodUpperBound - timePeriodLowerBound) / nyquistTimeStep)
    return nyquistSampleNumber

def evalTFTimeDomain(transferFunction, timePoints):
    scipySignal = transferFunction.returnScipySignalLti()[0][0]
    plottable = sp.signal.impulse(scipySignal, T=timePoints)
    return plottable

def plotTF(transferFunction, maxTime):
    samples = 100 * nyquistSamples(transferFunction, 0, maxTime)
    timePoints = np.linspace(0, maxTime, samples)
    plottable = evalTFTimeDomain(transferFunction, timePoints)
    plt.plot(plottable[0], plottable[1])
    
def evalAtTime(transferFunction, time):
    return evalTFTimeDomain(transferFunction, np.array([time]))[1][0]
    

def stepSettlingTime(transferFunction, minTime, maxTime):
    samples = nyquistSamples(transferFunction, minTime, maxTime)
    timePoints = np.linspace(minTime, maxTime, samples)
    stepResponse = transferFunction * getStep()
    plottable = evalTFTimeDomain(stepResponse, timePoints)
    dcGain = evalAtTime(stepResponse, maxTime * 100)
    # TODO: final value calculation using l'hopital's rule
    firstEscape = 0
    for i in np.flipud(range(0, len(plottable[1]))):
        if abs(plottable[1][i] - dcGain) >= 0.02 * dcGain:
            firstEscape = i
            break
    settlingTime = plottable[0][firstEscape]
    if (firstEscape <= len(plottable[1])):
        toutside = plottable[0][firstEscape]
        tinside = plottable[0][firstEscape + 1]
        test = lambda(t): abs(evalAtTime(stepResponse, t) - dcGain) >= 0.02 * dcGain
        settlingTime = bisection(test, toutside, tinside, 1e-6)
    print "settling time is", settlingTime
    return settlingTime

def debugTestBisection(test, t1, t2, num):
    tests = np.linspace(min(t1, t2), max(t1, t2), 100)
    plt.figure()
    plt.plot(tests, [test(t) for t in tests])
    plt.show()

def calcStepOvershoot(transferFunction, maxTime):
    samples = nyquistSamples(transferFunction, 0, maxTime)
    timePoints = np.linspace(0, maxTime, samples)
    stepResponse = transferFunction * getStep()
    plottable = evalTFTimeDomain(stepResponse, timePoints)
    dcGain = evalAtTime(stepResponse, maxTime * 100)
    # TODO: final value calculation using l'hopital's rule
    maxValue = max(plottable[1])
    maxIndex = 0
    for i in range(0, len(plottable[1])):
        if plottable[1][i] == maxValue:
            maxIndex = i
            break

    tpos = plottable[0][maxIndex - 1]
    tneg = plottable[0][maxIndex + 1]
    test = lambda(t): evalAtTime(transferFunction, t) >= 0 
    maximumTime = bisection(test, tpos, tneg, 1e-6)
    maxValue = evalAtTime(stepResponse, maximumTime)
    overshoot = maxValue / dcGain
    print "maximumTime time is", maximumTime, "max value is", maxValue
    print "percent overshoot is", overshoot * 100.0, "%"
    return (overshoot, maximumTime, maxValue)

def plotSettlingTimeGuides(transferFunction, maxTime):
    settlingTime = stepSettlingTime(transferFunction, 0, maxTime)
    exitValue = evalAtTime(transferFunction * getStep(), settlingTime)
    dcGain = evalAtTime(transferFunction * getStep(), maxTime * 100)
    plt.plot([0, maxTime], [dcGain * 0.98, dcGain * 0.98], "k:")
    plt.plot([0, maxTime], [dcGain * 1.02, dcGain * 1.02], "k:")
    plt.plot([settlingTime], [exitValue], "k.")
    
def plotOvershootGuides(transferFunction, maxTime):
    (overshoot, maximumTime, maxValue) = calcStepOvershoot(transferFunction, maxTime)
    dcGain = evalAtTime(transferFunction * getStep(), maxTime * 100)
    plt.plot([0, maxTime], [dcGain, dcGain], "k--")
    plt.plot([0, maxTime], [maxValue, maxValue], "k--")
    plt.plot([maximumTime], [maxValue], "k.")
    
def getStateSpaceSystem(tf):
    num = tf.num[0][0]
    den = tf.den[0][0]
    nsize = len(num)
    dsize = len(den)
    assert nsize < dsize
    A = np.zeros((dsize - 1, dsize - 1))
    B = np.zeros((dsize - 1, 1))
    C = np.zeros((1, dsize - 1))
    D = np.zeros((1, 1))
    assert abs(den[0]) > 1e-7
    scale = den[0]
    num = [(1.0 * n) / scale for n in num]
    den = [(1.0 * d) / scale for d in den]
    for i in range(0, dsize - 2):
        A[i + 1, i] = 1.0
    for i in range(0, dsize - 1):
        A[i, dsize - 2] = -den[dsize - 1 - i]
    B[:len(num), 0] = np.flipud(num)
    C[0, -1] = 1.0
    ssSystem = control.ss(A, B, C, D)
    return ssSystem