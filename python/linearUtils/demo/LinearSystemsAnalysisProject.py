'''
Created on Dec 16, 2013

@author: Gray Thomas
'''

import control  
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import sympy as sym

from cmath import *
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter 
import math as realMath
import linearUtils 

def getGraphicsExtension():
    return ".pdf"
def reverseComplexArrayAndCastToReal(array):
    ret = np.empty(len(array))
    for i in range(0, len(array)):
        ret[i] = (array[len(array) - 1 - i]).real
    return ret

def roots2poly(rootsArray):
    roots = np.array(rootsArray, complex)
    order = len(roots) + 1
    poly = np.zeros(order, complex)
    temp = np.zeros(order - 1, complex)
    poly[0] = 1.0
    for i in range(0, len(roots)):
        if roots[i] != 0:
            temp[0:i + 1] = poly[0:i + 1]
            poly[1:i + 2] -= temp[0:i + 1] / roots[i]
        else:
            temp[0:i + 1] = poly[0:i + 1]
            poly[1:i + 2] = temp[0:i + 1]
            poly[0] = 0
    return reverseComplexArrayAndCastToReal(poly)

def pzk2tf(pArray, zArray, gain):
    G = control.tf(roots2poly(zArray), roots2poly(pArray))
    k = control.tf(gain, 1)
    return G * k

def rootsOfQuadratic(a, b, c):
    det = sqrt(b * b - 4 * a * c)
    root1 = (-b + det) / (2.0 * a)
    root2 = (-b - det) / (2.0 * a)
    return [root1, root2]
    
def getAltitudeDynamics(beta):
    altitudeDynamics = pzk2tf([-1.0 / 20., (-1.0 / beta), -1.0 / 0.5], [], 1.0)
    return altitudeDynamics
def getControllerDynamics(gain):
    return pzk2tf([0], rootsOfQuadratic(1.0, 1.5, 0.5), 0.5 * gain)

def getForwardDynamics():
    return pzk2tf(rootsOfQuadratic(1.0, 4.5, 9.0), [], 10.0 / 9.0)
def getForwardController():
    return control.tf([1, 1], [1, 0])
def getOpenLoop(beta):
    altitudeDynamics = getAltitudeDynamics(beta)
    pidAltitudeController = pzk2tf([0], rootsOfQuadratic(1.0, 1.5, 0.5), 0.5)
    return  pidAltitudeController * altitudeDynamics

def getTF(beta, gain=1):
    forwardChain = getOpenLoop(beta) * gain
    closedLoop = forwardChain.feedback(1, -1)
    return closedLoop
def getDisturbanceRejectionTF(beta, gain):
    forwardChain = getAltitudeDynamics(beta)
    feedbackChain = getControllerDynamics(gain)
    return forwardChain.feedback(feedbackChain, -1)
def getBaseFolder():
    return "/home/gray/wk/grayutils/python/linearUtils/demo/"


    


def bisection(test, trueVal, falseVal, tol):
    assert(test(trueVal))
    assert(not test(falseVal))
    testVal = (trueVal + falseVal) / 2
    if (abs(testVal - trueVal) < tol):
        return testVal
    if test(testVal):
        return bisectionNoTest(test, testVal, falseVal, tol)
    else:
        return bisectionNoTest(test, trueVal, testVal, tol)
def bisectionNoTest(test, trueVal, falseVal, tol):
    testVal = (trueVal + falseVal) / 2
    if (abs(testVal - trueVal) < tol):
        return testVal
    if test(testVal):
        return bisection(test, testVal, falseVal, tol)
    else:
        return bisection(test, trueVal, testVal, tol)

def isHurwitz(transfer):
    poles = transfer.pole()
    test = [x.real < 0.0 for x in poles]
    return all(test)
def howHurwitz(transfer):
    poles = transfer.pole()
    test = [x.real for x in poles]
    return max(test)


def stabilityTestMinBeta(k):
    return isHurwitz(getTF(10.0, k))
def posNegLocus(function, minVal, maxVal, name, prefix, useneg=True, axes=[-2.0, 0.1, -2.0, 2.0]):
    plt.figure()
    plt.clf()
    print function

    control.root_locus(function, np.logspace(minVal, maxVal, 1000))
    plt.title("PositiveLocusOf" + name)
    plt.axis(axes)
    plt.savefig(getBaseFolder() + prefix + "PositiveLocusOf" + name + getGraphicsExtension(), dpi=100)
    if useneg:
        plt.figure()
        plt.clf()
        control.root_locus(function, -np.logspace(minVal, maxVal, 1000))
        plt.title("NegativeLocusOf" + name)
        plt.axis(axes)
        plt.savefig(getBaseFolder() + prefix + "NegativeLocusOf" + name + getGraphicsExtension(), dpi=100)

def makeStabilityPlot(transferFunctionFactoryForGains=lambda(k):getTF(10, k)):
    ktest = np.logspace(-5, 5, 1000)
#     testPoints=np.concatenate([-np.flipud(ktest),ktest])
    vals = np.array([howHurwitz(transferFunctionFactoryForGains(k)) for k in ktest])
    plt.figure()
    plt.semilogx(ktest, vals)
    plt.hold(True)
    plt.semilogx(ktest, np.array([1 if v > 0 else 0 for v in vals]))
    plt.title("Stability due to K")
    plt.ylabel("Maximum realpart of any closed loop pole")
    plt.xlabel("K")
    plt.legend(("Maximum real pole part", "One if sign is positive"))
    plt.axis([1e-3, 100000.0, -1, 1.4])    
    plt.show()
    plt.savefig(getBaseFolder() + "partB_PosStabilityAt10Beta" + getGraphicsExtension(), dpi=100)

def getStep():
    return pzk2tf([0], [], 1)

def nyquistSamples(transferFunction, timePeriodLowerBound, timePeriodUpperBound):
    freqMax = max([abs(p)for p in transferFunction.pole()])
    nyquistTimeStep = 0.5 / freqMax
    nyquistSampleNumber = ((timePeriodUpperBound - timePeriodLowerBound) / nyquistTimeStep)
    return nyquistSampleNumber

def evalTFTimeDomain(transferFunction, timePoints):
    scipySignal = transferFunction.returnScipySignalLti()[0][0]
    plottable = sp.signal.impulse(scipySignal, T=timePoints)
    return plottable

def plotTF(transferFunction, maxTime):
    samples = 100 * nyquistSamples(transferFunction, 0, maxTime)
    timePoints = np.linspace(0, maxTime, samples)
    plottable = evalTFTimeDomain(transferFunction, timePoints)
    plt.plot(plottable[0], plottable[1])
    
def evalAtTime(transferFunction, time):
    return evalTFTimeDomain(transferFunction, np.array([time]))[1][0]
    

def stepSettlingTime(transferFunction, minTime, maxTime):
    samples = nyquistSamples(transferFunction, minTime, maxTime)
    timePoints = np.linspace(minTime, maxTime, samples)
    stepResponse = transferFunction * getStep()
    plottable = evalTFTimeDomain(stepResponse, timePoints)
    dcGain = evalAtTime(stepResponse, maxTime * 100)
    # TODO: final value calculation using l'hopital's rule
    firstEscape = 0
    for i in np.flipud(range(0, len(plottable[1]))):
        if abs(plottable[1][i] - dcGain) >= 0.02 * dcGain:
            firstEscape = i
            break
    settlingTime = plottable[0][firstEscape]
    if (firstEscape <= len(plottable[1])):
        toutside = plottable[0][firstEscape]
        tinside = plottable[0][firstEscape + 1]
        test = lambda(t): abs(evalAtTime(stepResponse, t) - dcGain) >= 0.02 * dcGain
        settlingTime = bisection(test, toutside, tinside, 1e-6)
    print "settling time is", settlingTime
    return settlingTime

def debugTestBisection(test, t1, t2, num):
    tests = np.linspace(min(t1, t2), max(t1, t2), 100)
    plt.figure()
    plt.plot(tests, [test(t) for t in tests])
    plt.show()

def calcStepOvershoot(transferFunction, maxTime):
    samples = nyquistSamples(transferFunction, 0, maxTime)
    timePoints = np.linspace(0, maxTime, samples)
    stepResponse = transferFunction * getStep()
    plottable = evalTFTimeDomain(stepResponse, timePoints)
    dcGain = evalAtTime(stepResponse, maxTime * 100)
    # TODO: final value calculation using l'hopital's rule
    maxValue = max(plottable[1])
    maxIndex = 0
    for i in range(0, len(plottable[1])):
        if plottable[1][i] == maxValue:
            maxIndex = i
            break

    tpos = plottable[0][maxIndex - 1]
    tneg = plottable[0][maxIndex + 1]
    test = lambda(t): evalAtTime(transferFunction, t) >= 0 
    maximumTime = bisection(test, tpos, tneg, 1e-6)
    maxValue = evalAtTime(stepResponse, maximumTime)
    overshoot = maxValue / dcGain
    print "maximumTime time is", maximumTime, "max value is", maxValue
    print "percent overshoot is", overshoot * 100.0, "%"
    return (overshoot, maximumTime, maxValue)

def plotSettlingTimeGuides(transferFunction, maxTime):
    settlingTime = stepSettlingTime(transferFunction, 0, maxTime)
    exitValue = evalAtTime(transferFunction * getStep(), settlingTime)
    dcGain = evalAtTime(transferFunction * getStep(), maxTime * 100)
    plt.plot([0, maxTime], [dcGain * 0.98, dcGain * 0.98], "k:")
    plt.plot([0, maxTime], [dcGain * 1.02, dcGain * 1.02], "k:")
    plt.plot([settlingTime], [exitValue], "k.")
    
def plotOvershootGuides(transferFunction, maxTime):
    (overshoot, maximumTime, maxValue) = calcStepOvershoot(transferFunction, maxTime)
    dcGain = evalAtTime(transferFunction * getStep(), maxTime * 100)
    plt.plot([0, maxTime], [dcGain, dcGain], "k--")
    plt.plot([0, maxTime], [maxValue, maxValue], "k--")
    plt.plot([maximumTime], [maxValue], "k.")
    
def getStateSpaceSystem(tf):
    num = tf.num[0][0]
    den = tf.den[0][0]
    nsize = len(num)
    dsize = len(den)
    assert nsize < dsize
    A = np.zeros((dsize - 1, dsize - 1))
    B = np.zeros((dsize - 1, 1))
    C = np.zeros((1, dsize - 1))
    D = np.zeros((1, 1))
    assert abs(den[0]) > 1e-7
    scale = den[0]
    num = [(1.0 * n) / scale for n in num]
    den = [(1.0 * d) / scale for d in den]
    for i in range(0, dsize - 2):
        A[i + 1, i] = 1.0
    for i in range(0, dsize - 1):
        A[i, dsize - 2] = -den[dsize - 1 - i]
    B[:len(num), 0] = np.flipud(num)
    C[0, -1] = 1.0
    ssSystem = control.ss(A, B, C, D)
    return ssSystem


def solvePartA(prefilter=1, namePrefix="PartA"):
    print "===================Project part A:=================="
    closedLoop = getTF(10, 280) * prefilter
    print "closed loop poles:"
    print closedLoop.pole()
    plt.figure()
    plotTF(closedLoop, 100)
    plt.title(namePrefix + " closed loop impulse response")
    plt.ylabel("altitude")
    plt.xlabel("time")
    plt.savefig(getBaseFolder() + namePrefix + "Fig1_impulseResponse" + getGraphicsExtension(), dpi=100)
    plt.show()
    plt.figure()
    plotTF(getAltitudeDynamics(10), 100)
    plt.title(namePrefix + " Altitude Impulse Response")
    plt.ylabel("Altitude")
    plt.xlabel("Time")
    plt.savefig(getBaseFolder() + namePrefix + "AltitudeResponse" + getGraphicsExtension(), dpi=100)
    plt.show()
    plt.figure()
    plotTF(getAltitudeDynamics(10)*getControllerDynamics(1), 100)
    plt.title(namePrefix + " Forward Chain Impulse Response")
    plt.ylabel("Altitude")
    plt.xlabel("Time")
    plt.savefig(getBaseFolder() + namePrefix + "ForwardChainAltitudeResponse" + getGraphicsExtension(), dpi=100)
    plt.show()
    # TODO: Write polynomial partial fraction decomposition
    # TODO: Write inverse Laplace transform function
    altitudeDynamics = getAltitudeDynamics(10)
    ssAltDyn = control.tf2ss(altitudeDynamics)
    print "Altitude dynamics as state space equation"
    print ssAltDyn
    print "=====================End part A====================="

def doStabilityBisection(beta,prefilter=1):
    print "bisection result "+str(int(beta))+"Beta zero bound"
    print bisection((lambda(x): isHurwitz(prefilter * getTF(beta, x))), 0.01, -3.0, 1e-6)
    print "bisection result "+str(int(beta))+"Beta low bound"
    print bisection((lambda(x): isHurwitz(prefilter * getTF(beta, x))), 0.01, 10.0, 1e-6)
    print "bisection result "+str(int(beta))+"Beta high bound"
    print bisection((lambda(x): isHurwitz(prefilter * getTF(beta, x))), 10000.0, 10.0, 1e-6)

def doStabilityBisections(prefilter=1):
    doStabilityBisection(8,prefilter)
    doStabilityBisection(10, prefilter)
    doStabilityBisection(12, prefilter)
    print "bisection result 8Beta zero bound"
    print bisection((lambda(x): isHurwitz(prefilter * getTF(8, x))), 0.01, -3.0, 1e-6)
    print "bisection result 12Beta zero bound"
    print bisection((lambda(x): isHurwitz(prefilter * getTF(12, x))), 0.01, -3.0, 1e-6)
    print "bisection result 8Beta low bound"
    print bisection((lambda(x): isHurwitz(prefilter * getTF(8, x))), 0.01, 10.0, 1e-6)
    print "bisection result 12Beta low bound"
    print bisection((lambda(x): isHurwitz(prefilter * getTF(12, x))), 0.01, 10.0, 1e-6)
    print "bisection result 8Beta high bound"
    print bisection((lambda(x): isHurwitz(prefilter * getTF(8, x))), 10000.0, 10.0, 1e-6)
    print "bisection result 12Beta high bound"
    print bisection((lambda(x): isHurwitz(prefilter * getTF(12, x))), 10000.0, 10.0, 1e-6)
    
def makeTheRootLocusPlots(namePrefix):
    print "openLoop8:"
    print getOpenLoop(8.0)
    print "openLoop8Poles:"
    print getOpenLoop(8.0).pole()
    print "openLoop12:"
    print getOpenLoop(12.0)
    print "openLoop12Poles:"
    print getOpenLoop(12.0).pole()
    posNegLocus(getOpenLoop(8.0), -5, 4, "8BetaOpen", namePrefix, False)
    posNegLocus(getOpenLoop(12.0), -5, 4, "12BetaOpen", namePrefix, False)

def make3Dplot(minEigGramLogGain, nameprefix, lowGains):
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    betasX = np.linspace(8, 12, 20)
    gainsLogY = np.linspace(-6, log10(0.4), 27)
    if not lowGains:
        gainsLogY = np.linspace(log10(300), 7, 27)
    betasX, gainsLogY = np.meshgrid(betasX, gainsLogY)
    R = np.sqrt(betasX ** 2 + gainsLogY ** 2)
    Z = np.zeros(betasX.shape)
    for i in range (0, Z.shape[0]):
        for j in range (0, Z.shape[1]):
            Z[i, j] = minEigGramLogGain((betasX[i, j], gainsLogY[i, j]))
    surf = ax.plot_surface(betasX, gainsLogY, Z, rstride=1, cstride=1, cmap=cm.get_cmap("coolwarm"), linewidth=0, antialiased=False)
       
    ax.set_zscale('log')

    ax.zaxis.set_label("minimum eigenvalue of grammian")
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.xlabel("Beta")
    plt.ylabel("Log of gain")
    if lowGains:
        plt.title("minimum eigenvalue of controllability grammian for low gains")
        plt.savefig(getBaseFolder() + nameprefix + "controllabilityLowGain" + getGraphicsExtension(), dpi=100)
    else:
        plt.title("minimum eigenvalue of controllability grammian for high gains")
        plt.savefig(getBaseFolder() + nameprefix + "controllabilityHighGain" + getGraphicsExtension(), dpi=100)
    
    
    plt.show()

def exploreControllability(prefilter, nameprefix):
    print "the three grammian determinats are:"
    print min(np.linalg.eigvals(control.gram(control.tf2ss(prefilter * getTF(8.0, 0.1)), 'c')))
    print np.linalg.det(control.gram(control.tf2ss(prefilter * getTF(8.0, 0.1)), 'c'))
    print np.linalg.det(control.gram(control.tf2ss(prefilter * getTF(8.0, 0.05)), 'c'))
    print np.linalg.det(control.gram(control.tf2ss(prefilter * getTF(8.0, 200.0)), 'c'))
#     bisection result 8Beta low bound
#     0.632464199662
#     bisection result 12Beta low bound
#     0.403026441932
#     bisection result 8Beta high bound
#     95.2379056212
#     bisection result 12Beta high bound
#     178.474166921

    gram = lambda ((beta, k)):control.gram(getStateSpaceSystem(prefilter * getTF(beta, k)), 'c')
    minEigGram = lambda((beta, k)): min(np.linalg.eigvals(gram((beta, k))))
    minEigGramLogGain = lambda((beta, logK)): minEigGram((beta, realMath.pow(10, logK)))
    
    make3Dplot(minEigGramLogGain, nameprefix, False)
    make3Dplot(minEigGramLogGain, nameprefix, True)
    
    
    
    betas = np.linspace(8, 12, 20)
    plt.figure()
    plt.hold(True)

    for k in np.logspace(-6, log10(0.4), 27):
        plt.plot(betas, [minEigGram((beta, k))for beta in betas])
    for k in np.logspace(log10(180), log10(5000), 27):
        plt.plot(betas, [minEigGram((beta, k))for beta in betas])
    plt.title("Closed loop controllability sensitivity to beta")
    plt.xlabel("beta")
    plt.ylabel("minimum eigenvalue of the controllability grammian")
        
    print min(np.linalg.eigvals(control.gram(control.tf2ss(prefilter * getTF(8.0, 0.05)), 'c')))
    print min(np.linalg.eigvals(control.gram(control.tf2ss(prefilter * getTF(8.0, 1000.0)), 'c')))
    print min(np.linalg.eigvals(control.gram(control.tf2ss(prefilter * getTF(10.0, 1000.0)), 'c')))
    print min(np.linalg.eigvals(control.gram(control.tf2ss(prefilter * getTF(12.0, 1000.0)), 'c')))
    
    plt.show()
def solvePartB(prefilter=1, namePrefix="PartB"):
    print "===================Project part B:=================="
    makeStabilityPlot(lambda(k):prefilter * getTF(10, k))
    doStabilityBisections(prefilter)
    makeTheRootLocusPlots(namePrefix)
    exploreControllability(prefilter, namePrefix)
    
    print "=====================End part B====================="
def solvePartC(prefilter=1, namePrefix="PartC"):
    print "===================Project part C:=================="
    tf = getTF(10, 280)
    print tf
    maxTime = 100
    print "plotting step response for this function"
    plt.figure()
    plotTF(prefilter * tf * getStep(), maxTime)
    plotSettlingTimeGuides(prefilter * tf, maxTime)
    plotOvershootGuides(prefilter * tf, maxTime)
    plt.title(namePrefix + ": Step Response")
    plt.xlabel("time")
    plt.ylabel("closed loop altitude")
    plt.savefig(getBaseFolder() + namePrefix + "StepResponse"  + getGraphicsExtension(), dpi=100)
    plt.show()
    print "=====================End part C====================="
def solvePartD():
    print "===================Project part D:=================="
    tf = getDisturbanceRejectionTF(10, 280)
    maxTime = 100
    plt.figure()
    plotTF(tf * getStep(), maxTime)
    print "Disturbance rejection formula", tf * getStep()
#     plotSettlingTimeGuides(tf, maxTime)
#     plotOvershootGuides(tf, maxTime)
    plt.title("Step disturbance responce")
    plt.xlabel("time")
    plt.ylabel("altitude")
    plt.savefig(getBaseFolder() + "partD_StepResponse"  + getGraphicsExtension(), dpi=100)
    plt.show()
    print "=====================End part D====================="
def solvePartE():
    print "===================Project part E:=================="
    solvePartA(control.tf([0.5], [1.0, 1.5, 0.5]), "PartE-A")
    solvePartB(control.tf([0.5], [1.0, 1.5, 0.5]), "PartE-B")
    solvePartC(control.tf([0.5], [1.0, 1.5, 0.5]), "PartE-C")
    print "The prefilter does not effect disturance rejection discussed in part D"
    print "=====================End part E====================="
    
def symbolicGrammian():
    L1 = sym.Symbol("L1")
    L2 = sym.Symbol("L2")
    L3 = sym.Symbol("L3")
    L4 = sym.Symbol("L4")
    g11 = sym.Symbol("g11")
    g12 = sym.Symbol("g12")
    g13 = sym.Symbol("g13")
    g14 = sym.Symbol("g14")
    g22 = sym.Symbol("g22")
    g23 = sym.Symbol("g23")
    g24 = sym.Symbol("g24")
    g33 = sym.Symbol("g33")
    g34 = sym.Symbol("g34")
    g44 = sym.Symbol("g44")
    a = sym.Symbol("a")
    u = sym.Symbol("u")
    syms = [g11, g12, g13, g14, g22, g23, g24, g33, g34, g44]
    row1 = [0, 0, 0, 2 * L1, 0, 0, 0, 0, 0, 0, -1 / (4 * a)]
    row2 = [u, 0, 0, L2, 0, 0, L1, 0, 0, 0, -(a * 3) / 4]
    row3 = [0, u, 0, L3, 0, 0, 0, 0, L1, 0, -(a * 1) / 2]
    row4 = [0, 0, u, L4, 0, 0, 0, 0, 0, L1, 0]
    row5 = [0, 2 * u, 0, 0, 0, 0, 2 * L2, 0, 0, 0, -(a * 9) / 4]
    row6 = [0, 0, u, 0, u, 0, L3, 0, L2, 0, -(a * 3) / 2]
    row7 = [0, 0, 0, u, 0, u, L4, 0, 0, L2, 0]
    row8 = [0, 0, 0, 0, 0, 2 * u, 0, 0, 2 * L3, 0, -a]
    row9 = [0, 0, 0, 0, 0, 0, u, u, L4, L3, 0]
    row10 = [0, 0, 0, 0, 0, 0, 0, 0, 2 * u, 2 * L4, 0]
    matrix = sym.Matrix((row1, row2, row3, row4, row5, row6, row7, row8, row9, row10))
    
    B = sym.Symbol("B")
    K = sym.Symbol("K")
    simp = lambda x: sym.simplify(x)
    matrix = matrix.subs([(a, K * K), (L1, -1 / 2 * K * (10 * B))])
    matrix = matrix.subs([(L2, -(1 + 3 / 2 * K) * (10 * B)), (L3, -(20 + 1 / 2 + B + K) * (10 * B))])
    matrix = matrix.subs([(L4, -(20 + 1 / 2 * B + 10) * (10 * B)), (u, 100 * B * B)])
    matrix.applyfunc(simp)
    sol = sym.solvers.solve_linear_system_LU(matrix, syms)
    row1 = [sol[g11], sol[g12], sol[g13], sol[g14]]
    row2 = [sol[g12], sol[g22], sol[g23], sol[g24]]
    row3 = [sol[g13], sol[g23], sol[g33], sol[g34]]
    row4 = [sol[g14], sol[g24], sol[g34], sol[g44]]
    grammian = sym.Matrix((row1, row2, row3, row4))
    print grammian.det()


def solvePartF(zeta):
    fd = getForwardDynamics()
    fc = getForwardController()
    print (fd * fc).pole()
    tf = lambda k: control.feedback((fd * fc * k), 1, -1)
    
    plt.figure()
    plt.clf()

    control.root_locus((fd * fc), np.logspace(-5, 5, 1000))
    plt.title("PartF: Root Locus of Forward Velocity Controller with Zeta=" + str(zeta))
    plt.axis([-6.0, 6.0, -6.0, 6.0])

    test = control.tf([1], [-zeta * 2.0, 4.5 - 1.0 + 4.0 * zeta * zeta, -zeta * 2.0 * 4.5, 9.0])
    print test.pole()
    w = test.pole()[0].real
    q = 4.5 - 2.0 * zeta * w
    k = .1 * w * w * q
    otherSide = realMath.sqrt(1 - zeta ** 2)
    print "preferred damping gain is", k
    expectedPole = complex(-w * zeta, otherSide * w)
    print expectedPole
    
    poles = tf(k).pole()
    plt.plot([pole.real for pole in poles], [pole.imag for pole in poles], "kx")
    
    plt.plot([0, expectedPole.real], [0, expectedPole.imag], "k:.")
    plt.plot([0, expectedPole.real], [0, -expectedPole.imag], "k:.")
    plt.plot([-q], [0], "k.")
    plt.savefig(getBaseFolder() + "PartF_PositiveLocusWithZeta" + str(int(realMath.ceil(1000*zeta)))  + getGraphicsExtension(), dpi=100)
    plt.show()
    return (k, tf(k))

def solvePartG(k, tf):
    plt.figure()
    maxTime = 20
    plotTF(tf * getStep(), maxTime)
    plotSettlingTimeGuides(tf, maxTime)
    plotOvershootGuides(tf, maxTime)
    plt.title("PartG" + ": Step Response at K=" + str(k))
    plt.xlabel("time")
    plt.ylabel("closed loop altitude")
    plt.savefig(getBaseFolder() + "PartG" + "StepResponse" + str(int(realMath.ceil(1000*k))) + getGraphicsExtension(), dpi=100)
    plt.show()
    
def solvePartI():
    fd = getForwardDynamics()
    fc = control.tf([1, 2, 2], [1, 0])
    print (fd * fc).pole()
    tf = lambda k: control.feedback((fd * fc * k), 1, -1)
    
    plt.figure()
    plt.clf()

    control.root_locus((fd * fc), np.logspace(-5, 5, 1000))
    plt.title("PartF: Root Locus of Forward Velocity Controller with Zeta=")
    plt.axis([-6.0, 6.0, -6.0, 6.0])

   
    k = 6.0
    
    poles = tf(k).pole()
    plt.plot([pole.real for pole in poles], [pole.imag for pole in poles], "kx")
    
    plt.savefig(getBaseFolder() + "PartI_PositiveLocus" + getGraphicsExtension(), dpi=100)
    plt.show()

    tf=tf(k)
    plt.figure()
    maxTime = 20
    plotTF(tf * getStep(), maxTime)
    plotSettlingTimeGuides(tf, maxTime)
#     plotOvershootGuides(tf, maxTime)
    plt.title("PartG" + ": Step Response at K=" + str(k))
    plt.xlabel("time")
    plt.ylabel("closed loop altitude")
    plt.savefig(getBaseFolder() + "PartG" + "StepResponse" + str(int(realMath.ceil(1000*k))) + getGraphicsExtension(), dpi=100)
    plt.show()
    
def demoLinearSystemAnalysisFinalProject():
    solvePartA()
    solvePartB()
    solvePartC()
    solvePartD()
    solvePartE()
    (k, tf) = solvePartF(0.6)
    solvePartG(k, tf)
    (k, tf) = solvePartF(realMath.sqrt(2) - 1)
    solvePartG(k, tf)
    (k, tf) = solvePartF(.2)
    solvePartG(k, tf)
#     solvePartI()
if __name__=="__main__":
    demoLinearSystemAnalysisFinalProject()
