'''
Created on Dec 4, 2013

@author: Gray Thomas
'''
import control  
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import sympy as sym
from cmath import *
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import math as realMath
import linearUtils

def demoSeriesElasticActuator(K=1.0, J0=1.2, J1=300.0, R0=0.02, R1=0.02, G0=1.0, G1=1.0):
    matrixA = np.array([[0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 1.0], [-K / J0, K / J0, -R0 / J0, 0.0], [K / J1, -K / J1, 0.0, -R1 / J1]])
    matrixB = np.array([[0.0, 0.0], [0.0, 0.0], [G0 / J0, 0.0], [0.0, G1 / J1]])
    matrixC = np.array([[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 0.0], [1.0, -1.0, 0.0, 0.0]])
    matrixD = np.zeros((3, 2))
    stateSpaceSEA = control.ss(matrixA, matrixB, matrixC, matrixD)
    print stateSpaceSEA
    return stateSpaceSEA
    
def feedbackSeriesElasticActuatorSymbolic():
    K = sym.Symbol("K", positive=True)
    J0 = sym.Symbol("J0", positive=True)
    J1 = sym.Symbol("J1", positive=True)
    R0 = sym.Symbol("R0", positive=True)
    R1 = sym.Symbol("R1", positive=True)
    k0 = sym.Symbol("k0", real=True)
    k1 = sym.Symbol("k1", real=True)
    k2 = sym.Symbol("k2", real=True)
    k3 = sym.Symbol("k3", real=True)
    s = sym.Symbol("s", complex=True)
    z = sym.Symbol("z", complex=True)
    symA = sym.Matrix(4, 4,
                      [s, 0, -1, 0, 
                       0, s, 0, -1, 
                       K / J0 + z * k0 / J0, (z * k1 - K) / J0, (J0 * s + R0 + z * k2) / J0, z * k3 / J0, 
                       -K / J1, K / J1, 0, s + R1 / J1])
    charPoly = symA.det().as_poly(s)
    terms = charPoly.as_dict()
    print terms
    a1 = sym.Symbol("a1", positive=True)
    a2 = sym.Symbol("a2", positive=True)
    a3 = sym.Symbol("a3", positive=True)
    a4 = sym.Symbol("a4", positive=True)
    poleLocationPoly = (s + a1) * (s + a2) * (s + a3) * (s + a4)
    poleTerms = poleLocationPoly.as_poly(s).as_dict()
    print poleTerms
    return symA

def feedbackSeriesElasticActuator(K=1.0, J0=1.2, J1=300.0, R0=0.02, R1=0.02, G0=1.0, G1=1.0, kP1=1.0, kP2=1.0, kP3=0.0):
    A = np.array([[0.0, 0.0, 1.0, 0.0], [0.0, 0.0, 0.0, 1.0], [-K / J0, K / J0, -R0 / J0, 0.0], [K / J1, -K / J1, 0.0, -R1 / J1]])
    B = np.array([[0.0, 0.0], [0.0, 0.0], [G0 / J0, 0.0], [0.0, G1 / J1]])
    C = np.array([[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 0.0], [1.0, -1.0, 0.0, 0.0]])
    D = np.zeros((3, 2))
    F = np.array([[kP1, kP2, kP3], [0.0, 0.0, 0.0]])
    matrixAPrime = A - np.dot(np.dot(B, F), C)
    closedLoop = control.ss(matrixAPrime, B, C, D)
    return closedLoop

def showSomeClosedLoopPoles():
    plt.figure()
    plt.title("EffectOfKp2ClosedLoopPoleLocations")
    size = 200
    for kp in np.logspace(-2, 1, 10):
        poleSigmas = np.zeros((4, size))
        poleJOmegas = np.zeros((4, size))
        for i, J1 in enumerate(np.linspace(5, 30, size)):
            closedLoop = feedbackSeriesElasticActuator(K=1.0, J0=1.2, J1=J1, R0=0.050, R1=0.050, G0=1.0, G1=1.0, kP1=1.0, kP2=kp, kP3=0.0)
            poleSigmas[:, i] = [x.real for x in closedLoop.pole()]
            poleJOmegas[:, i] = [x.imag for x in closedLoop.pole()]
        for i in range (0, 4):
            plt.plot(poleSigmas[i, :], poleJOmegas[i, :], 'x:b')
    plt.show()
    
def main():
    feedbackSeriesElasticActuatorSymbolic()

if __name__ == '__main__':
    main()
