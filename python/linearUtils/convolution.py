import numpy as np
import math
def setupConvolutionFiltersOld(data, axs, to, tf):
    nConv = 100
    tmax = nConv * 1.0
    sigma = 0.002 * 2000  # seconds to tics
    kosigma=  0.016 * 2000
    ts = np.linspace(-tmax, tmax, nConv * 2 + 1)
    convolutionKernel = np.array([math.exp(-(t / sigma) * (t / sigma)) for t in ts])
    convolutionKernel = convolutionKernel * (1.0 / (np.sum(convolutionKernel)))
    diffKernel = np.array([-t * math.exp(-(t / sigma) * (t / sigma)) for t in ts])
    diffKernel = -diffKernel * (1.0 / (np.dot(diffKernel, ts)))
    ddifKernal = np.array([(2 * t * t / sigma / sigma - 1) * math.exp(-(t / sigma) * (t / sigma)) for t in ts])
    ddifKernal = ddifKernal * (1.0 / np.dot(ddifKernal, np.array([0.5 * t * t for t in ts])))
    
    knockoutGaussian = np.array([math.exp(-(t / kosigma) * (t / kosigma)) for t in ts])
    knockoutGaussian = knockoutGaussian * (1.0 / (np.sum(knockoutGaussian)))
    knockoutGaussian = 1 - convolutionKernel / convolutionKernel[nConv]
    
    # weighting to account for ommitted data
    countingErrors = np.array([(1 + data[to + i - 1, 4] - tS) % 100 for (i, tS) in enumerate(data[to:tf, 4]) ])
    wrappedCountingErrors = np.array([d - 100 if d > 50 else d for d in countingErrors])
    synchWeighting = np.array([0 if dC != 0 else 1 for dC in wrappedCountingErrors]).reshape((-1, 1))
    paddedWeighting = np.concatenate((np.ones((nConv,)), synchWeighting.reshape((-1,)), np.ones((nConv,))), axis=0)
    indexes = []
    start = np.ones(paddedWeighting.shape)
    for i in range(0, len(paddedWeighting)):
        if paddedWeighting[i] == 0:
            indexes = [indexes, i]
            for i, j in enumerate(range(i - nConv, i + nConv + 1)):
                start[j] = knockoutGaussian[i] * start[j]
    print synchWeighting.shape
    print convolutionKernel.shape
    convolvedWeighting = start[nConv:-nConv]
#     weightedAccel = np.multiply(convolvedMotorAcceleration, convolvedWeighting)
#     simpleVelocity = (data[to + 1:tf + 1, 1] - data[to - 1:tf - 1, 1]) * 0.5
#     plt.setp(axs[4, 0].plot(simpleVelocity, "b-"), linewidth=2.0)
    
#     simpleAccel = (data[to + 10:tf + 10, 1] - 2 * data[to:tf, 1] + data[to - 10:tf - 10, 1]) / (10.0 * 10.0)
#     plt.setp(axs[5, 0].plot(simpleAccel, "b-"), linewidth=2.0)
#     axs[5,0].properties()['yaxis'].properties()['label'].set_rotation("horizontal")
#     axs[4, 1].plot(np.multiply(np.convolve(simpleVelocity - convolvedMotorVelocity, convolutionKernel, mode='same'), convolvedWeighting), "b-")
    
#     axs[5, 0].plot(weightedAccel, "g")
#     axs[5, 1].plot(np.multiply((simpleAccel) - convolvedMotorAcceleration, convolvedWeighting), "b-")
#     axs[5, 1].plot(np.multiply((simpleAccel) - weightedAccel, convolvedWeighting), "g-")
    
    axs[0, 1].plot(wrappedCountingErrors)
    axs[0, 1].plot(convolvedWeighting, 'm')
    axs[0, 1].set_ylabel("Synch Errors", rotation="horizontal")
    return nConv, convolutionKernel, convolvedWeighting, diffKernel, ddifKernal

def setupConvolutionFilters(dt, sigma_in_time, nConv=100):
    nConv = 100
    tmax = nConv * 1.0
    sigma = sigma_in_time * 1.0/dt  # seconds to tics
    ts = np.linspace(-tmax, tmax, nConv * 2 + 1)
    convolutionKernel = np.array([math.exp(-(t / sigma) * (t / sigma)) for t in ts])
    convolutionKernel = convolutionKernel * (1.0 / (np.sum(convolutionKernel)))
    diffKernel = np.array([-t * math.exp(-(t / sigma) * (t / sigma)) for t in ts])
    diffKernel = -diffKernel * (1.0 / (np.dot(diffKernel, ts)))
    ddifKernal = np.array([(2 * t * t / sigma / sigma - 1) * math.exp(-(t / sigma) * (t / sigma)) for t in ts])
    ddifKernal = ddifKernal * (1.0 / np.dot(ddifKernal, np.array([0.5 * t * t for t in ts])))

    return convolutionKernel, diffKernel, ddifKernal