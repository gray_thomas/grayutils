import matplotlib.pyplot as plt 
import numpy as np 
from linearUtils.discrete_simulation import *
from linearUtils.discrete_plots import *
from linearUtils.convolution import *
import random
import scipy

num=4000
starting_points=[1000]
tf=100.0
dt = tf/num
A_t=np.array([[0.0,1.0],[-10.0,-1.0]])
intA_t = np.array([[1.0, dt],[-dt, 1.0-0.2*dt]])
B_t=np.array([[0.0],[10.0]])
intB_t = np.array([0.0,dt])
C_t=np.array([[0.1,0.0]])
# given y-, y--, u-, u--: yd-- must be knowable, and thus y
# y- -> x- = 10.0*y-
# y-- -> x-- = 10.0*y--
# x- = x-- + dt*xd-- -> xd-- = (x- - x--)/dt
# -> xd- = -dt*x-- + (1-0.2*dt)*xd-- + dt*u-- 
# x = x- + dt*xd- -> y = 0.1*(x-+dt*xd-)
# y = 0.1*(10.0*y- + dt*(-dt*x-- + (1-0.2*dt)*xd-- + dt*u--))
# y = y- -dt*dt*y-- + 0.1*dt*(1-0.2*dt)*xd-- + 0.1*dt*dt*u--
# y = y- -dt*dt*y-- + 0.1*dt*(1-0.2*dt)*(x- - x--)/dt + 0.1*dt*dt*u--
# y = y- -dt*dt*y-- + (1-0.2*dt)*(y- - y--) + 0.1*dt*dt*u--
# y = (2.0-0.2*dt)y- +(-dt*dt*+-1+0.2*dt)y-- + (0.1*dt*dt)*u--

K_t=np.array([[0.0,0.0]])
x0=np.array([[0.0],[1.0]])
def get_x_ref(t):
	return np.array([[0.0],[0.0]])

input_steps_per_second=1.0

input_set=[np.array([[random.random()]]) for i in range(0,int(tf*input_steps_per_second))]
def get_u_ref(t):
	return input_set[int(t*input_steps_per_second)]
ts,x,u,xref,uref,dx,du=simulate_state_feedback_with_reference(
	A_t,B_t,K_t,x0,get_x_ref,get_u_ref,tf=tf,n=num)
np_solution_plot(ts,x,u)


fir_order=300
### model 1
plot_model1=True #FIR no low pass filter
plot_model2=False #IIR: [y-, y--, y---, u-, u--, u---], works!
plot_model3=False
plot_model4=True #FIR with low pass prefilter
plot_model5=False #IIR: [y-, y--, u--], works with minimal set. Perfect model fit, filtering does nothing.

if plot_model1:
	# this model has worked when the system is stable:
	# y= b1 u-, b2 u--, b3 u-- ... 
	order=fir_order
	times=fir_order
	phi=np.zeros((num-times,order))
	b=np.zeros((num-times,1))
	for i in range(times,num):
		for z in range(0,times):
			phi[i-times,z]=u[0,i-1-z]
		b[i-times,0]=C_t.dot(x[:,i])
	nConv=100
	sigma_in_time=1.0
	model,residuals,rank,singular_values=np.linalg.lstsq(phi,b)
	# prediction= phi.dot(model)
	for init in starting_points:
		sim_y=np.zeros((num,1))
		for i in range(0,init):
			sim_y[i,0]=C_t.dot(x[:,i])
		sim_u=u.T

		for i in range(init,num):
			phi_i=np.zeros((1,order))
			for z in range(0,times):
				phi_i[0,z]=sim_u[i-1-z,0]
			sim_y[i,0]=phi_i.dot(model)
		ax1=plt.gcf().get_axes()[0]
		ax1.set_autoscaley_on(False)
		ax1.plot(ts[init:-1],10.0*sim_y[init:-1,0])
		# print ts.shape, prediction.shape
		# ax1.plot(ts[init:-1],10.0*prediction[init-order:-1,0])
		ax4=plt.gcf().add_subplot(224,sharex=ax1)
		print model.shape, ts.shape
		ax4.plot(ts[0:model.shape[0]],model[:,0])
		# print dir(plt.gcf().get_axes()[0])


if plot_model2:
	# this model works:
	# y^ = a1 y- + a2 y-- + a3 y--- + b1 u- + b2 u-- + b3 u---
	order=6
	times=3
	phi=np.zeros((num-times,order))
	b=np.zeros((num-times,1))
	for i in range(times,num):
		phi[i-times,:]=np.array([
			-C_t.dot(x[:,i-1]),
			-C_t.dot(x[:,i-2]),
			-C_t.dot(x[:,i-3]),
			u[0,i-1],
			u[0,i-2],
			u[0,i-3]
			])
		b[i-times,0]=C_t.dot(x[:,i])
	nConv=100
	sigma_in_time=1.0
	dt=ts[1]-ts[0]
	convolutionKernel, diffKernel, ddifKernal = setupConvolutionFilters(
		dt, sigma_in_time, nConv=nConv)
	new_phi=np.zeros((num-times,order))
	for i in range(0,phi.shape[1]):
		new_phi[:,i]=np.convolve(phi[:,i], convolutionKernel)[nConv:-nConv]
	new_b=np.zeros((num-times,1))
	for i in range(0,b.shape[1]):
		new_b[:,i]=np.convolve(b[:,i], convolutionKernel)[nConv:-nConv]
	# b=np.convolve(b.T, convolutionKernel)
	model,residuals,rank,singular_values=np.linalg.lstsq(phi,b)
	print model
	for init in starting_points:
		sim_y=np.zeros((num,1))
		for i in range(0,init):
			sim_y[i,0]=C_t.dot(x[:,i])
		sim_u=u.T
		for i in range(init,num):
			y_hat=(np.array([[
				-sim_y[i-1,0],
				-sim_y[i-2,0],
				-sim_y[i-3,0],
				sim_u[i-1,0],
				sim_u[i-2,0],
				sim_u[i-3,0]
				]])).dot(model)
			sim_y[i,0]=y_hat[0,0]
		plt.gcf().get_axes()[0].set_autoscaley_on(False)
		plt.gcf().get_axes()[0].plot(ts[init:-1],10.0*sim_y[init:-1,0])
		# print dir(plt.gcf().get_axes()[0])

if plot_model3:
	# y^ = a1 y- + a2 y-- + a3 y--- + b1 u- + b2 u-- + b3 u---
	# no filtering the equations.
	order=6
	times=3
	phi=np.zeros((num-times,order))
	b=np.zeros((num-times,1))
	for i in range(times,num):
		phi[i-times,:]=np.array([
			-C_t.dot(x[:,i-1]),
			-C_t.dot(x[:,i-2]),
			-C_t.dot(x[:,i-3]),
			u[0,i-1],
			u[0,i-2],
			u[0,i-3]
			])
		b[i-times,0]=C_t.dot(x[:,i])
	nConv=100
	sigma_in_time=1.0
	dt=ts[1]-ts[0]
	convolutionKernel, diffKernel, ddifKernal = setupConvolutionFilters(
		dt, sigma_in_time, nConv=nConv)
	new_phi=np.zeros((num-times,order))
	for i in range(0,phi.shape[1]):
		new_phi[:,i]=np.convolve(phi[:,i], convolutionKernel)[nConv:-nConv]
	new_b=np.zeros((num-times,1))
	for i in range(0,b.shape[1]):
		new_b[:,i]=np.convolve(b[:,i], convolutionKernel)[nConv:-nConv]
	# b=np.convolve(b.T, convolutionKernel)
	model,residuals,rank,singular_values=np.linalg.lstsq(new_phi,new_b)
	print model
	for init in starting_points:
		sim_y=np.zeros((num,1))
		for i in range(0,init):
			sim_y[i,0]=C_t.dot(x[:,i])
		sim_u=u.T
		for i in range(init,num):
			y_hat=(np.array([[
				-sim_y[i-1,0],
				-sim_y[i-2,0],
				-sim_y[i-3,0],
				sim_u[i-1,0],
				sim_u[i-2,0],
				sim_u[i-3,0]
				]])).dot(model)
			sim_y[i,0]=y_hat[0,0]
		plt.gcf().get_axes()[0].set_autoscaley_on(False)
		plt.gcf().get_axes()[0].plot(ts[init:-1],10.0*sim_y[init:-1,0])
		# print dir(plt.gcf().get_axes()[0])

if plot_model1:
	# this model has worked when the system is stable:
	# y= b1 u-, b2 u--, b3 u-- ... 
	order=fir_order
	times=fir_order
	phi=np.zeros((num-times,order))
	b=np.zeros((num-times,1))
	for i in range(times,num):
		for z in range(0,times):
			phi[i-times,z]=u[0,i-1-z]
		b[i-times,0]=C_t.dot(x[:,i])
	nConv=100
	sigma_in_time=2.0 
	dt=ts[1]-ts[0]
	convolutionKernel, diffKernel, ddifKernal = setupConvolutionFilters(
		dt, sigma_in_time, nConv=nConv)
	new_phi=np.zeros((num-times,order))
	for i in range(0,phi.shape[1]):
		new_phi[:,i]=np.convolve(phi[:,i], convolutionKernel)[nConv:-nConv]
	new_b=np.zeros((num-times,1))
	for i in range(0,b.shape[1]):
		new_b[:,i]=np.convolve(b[:,i], convolutionKernel)[nConv:-nConv]
	# b=np.convolve(b.T, convolutionKernel)
	model,residuals,rank,singular_values=np.linalg.lstsq(new_phi,new_b)
	# prediction= phi.dot(model)
	for init in starting_points:
		sim_y=np.zeros((num,1))
		for i in range(0,init):
			sim_y[i,0]=C_t.dot(x[:,i])
		sim_u=u.T

		for i in range(init,num):
			phi_i=np.zeros((1,order))
			for z in range(0,times):
				phi_i[0,z]=sim_u[i-1-z,0]
			sim_y[i,0]=phi_i.dot(model)
		ax1=plt.gcf().get_axes()[0]
		ax1.set_autoscaley_on(False)
		ax1.plot(ts[init:-1],10.0*sim_y[init:-1,0])
		# print ts.shape, prediction.shape
		# ax1.plot(ts[init:-1],10.0*prediction[init-order:-1,0])
		ax4=plt.gcf().add_subplot(224,sharex=ax1)
		print model.shape, ts.shape
		ax4.plot(ts[0:model.shape[0]],model[:,0])
		# print dir(plt.gcf().get_axes()[0])

if plot_model5:
	# this model works:
	# y^ = a1 y- + a2 y-- + b2 u--
	order=3
	times=3
	phi=np.zeros((num-times,order))
	b=np.zeros((num-times,1))
	for i in range(times,num):
		phi[i-times,:]=np.array([
			C_t.dot(x[:,i-1]),
			C_t.dot(x[:,i-2]),
			u[0,i-2],
			])
		b[i-times,0]=C_t.dot(x[:,i])
	nConv=100
	sigma_in_time=0.8
	dt=ts[1]-ts[0]
	convolutionKernel, diffKernel, ddifKernal = setupConvolutionFilters(
		dt, sigma_in_time, nConv=nConv)
	new_phi=np.zeros((num-times,order))
	for i in range(0,phi.shape[1]):
		new_phi[:,i]=np.convolve(phi[:,i], convolutionKernel)[nConv:-nConv]
	new_b=np.zeros((num-times,1))
	for i in range(0,b.shape[1]):
		new_b[:,i]=np.convolve(b[:,i], convolutionKernel)[nConv:-nConv]
	# b=np.convolve(b.T, convolutionKernel)
	model,residuals,rank,singular_values=np.linalg.lstsq(new_phi,new_b)
	print model
	for init in starting_points:
		sim_y=np.zeros((num,1))
		for i in range(0,init):
			sim_y[i,0]=C_t.dot(x[:,i])
		sim_u=u.T
		for i in range(init,num):
			y_hat=(np.array([[
				sim_y[i-1,0],
				sim_y[i-2,0],
				sim_u[i-2,0]
				]])).dot(model)
			sim_y[i,0]=y_hat[0,0]
		plt.gcf().get_axes()[0].set_autoscaley_on(False)
		plt.gcf().get_axes()[0].plot(ts[init:-1],10.0*sim_y[init:-1,0])
		# print dir(plt.gcf().get_axes()[0])
plt.show()

