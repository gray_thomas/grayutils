import matplotlib.pyplot as plt 
import numpy as np 
from linearUtils.discrete_simulation import *
from linearUtils.discrete_plots import *
from linearUtils.convolution import *
import random
import scipy
import control as c
from sysID.arx import Abstract_ARX

num=4000
starting_points=[1000]
tf=100.0
step_response_tf=20.0
dt = tf/num
step_response_points=int(step_response_tf/dt)
A_t=np.array([[0.0,1.0],[-10.0,-1.0]])
intA_t = np.array([[1.0, dt],[-dt, 1.0-0.2*dt]])
B_t=np.array([[0.0],[10.0]])
intB_t = np.array([[0.0],[dt]])
C_t=np.array([[0.1,0.0]])

ref_sys=c.StateSpace(A_t,B_t,C_t,np.zeros((1,1)),0.0)
dref_sys=c.StateSpace(intA_t,intB_t,C_t,np.zeros((1,1)),0.0)
# given y-, y--, u-, u--: yd-- must be knowable, and thus y
# y- -> x- = 10.0*y-
# y-- -> x-- = 10.0*y--
# x- = x-- + dt*xd-- -> xd-- = (x- - x--)/dt
# -> xd- = -dt*x-- + (1-0.2*dt)*xd-- + dt*u-- 
# x = x- + dt*xd- -> y = 0.1*(x-+dt*xd-)
# y = 0.1*(10.0*y- + dt*(-dt*x-- + (1-0.2*dt)*xd-- + dt*u--))
# y = y- -dt*dt*y-- + 0.1*dt*(1-0.2*dt)*xd-- + 0.1*dt*dt*u--
# y = y- -dt*dt*y-- + 0.1*dt*(1-0.2*dt)*(x- - x--)/dt + 0.1*dt*dt*u--
# y = y- -dt*dt*y-- + (1-0.2*dt)*(y- - y--) + 0.1*dt*dt*u--
# y = (2.0-0.2*dt)y- + (-dt*dt*+-1+0.2*dt)y-- + (0.1*dt*dt)*u--

K_t=np.array([[0.0,0.0]])
x0=np.array([[0.0],[1.0]])
def get_x_ref(t):
	return np.array([[0.0],[0.0]])

input_steps_per_second=1.0

input_set=[np.array([[random.random()]]) for i in range(0,int(tf*input_steps_per_second))]
def get_u_ref(t):
	return input_set[int(t*input_steps_per_second)]
ts,x,u,xref,uref,dx,du=simulate_state_feedback_with_reference(
	A_t,B_t,K_t,x0,get_x_ref,get_u_ref,tf=tf,n=num)

def get_u_ref_step(t):
	return np.array([[1.0]])

x0_step=np.array([[0.0],[0.0]])
ts_step, xs_step, us_step, xref_step, uref_step, dx_step, du_step = simulate_state_feedback_with_reference(
	A_t,B_t,K_t,x0_step,get_x_ref,get_u_ref_step,tf=step_response_tf,n=step_response_points)
y_step=C_t.dot(xs_step)
step_padding=step_response_points
y_step_padded=np.concatenate([np.zeros((1,step_padding)),y_step],axis=1)
u_step_padded=np.concatenate([np.zeros((1,step_padding)),us_step],axis=1)
print "geoffrey", u_step_padded.shape, y_step_padded.shape
t_step_padded=np.concatenate([np.zeros((step_padding,)),ts_step])


fig,axs=np_solution_plot(ts,x,u,ncols=3,minrows=2)
axs[1][2]=fig.add_subplot(236,sharex=axs[0][0])
axs[0][2]=fig.add_subplot(233)

output_overlay_axis=axs[1][2]
step_response_axis=axs[0][2]
y=C_t.dot(x)
output_overlay_axis.plot(ts,y[0,:])

step_response_axis.plot(ts_step,y_step[0,:])
step_response_axis.set_xlabel("$t$")
step_response_axis.set_title("step response")
step_response_axis.set_ylabel("$y$")


fir_order=500
### model 1
plot_model1=True #FIR no low pass filter
plot_model2=True #IIR: [y-, y--, y---, u-, u--, u---], works!
plot_model3=False
plot_model4=False #FIR with low pass prefilter
plot_model5=False #IIR: [y-, y--, u--], works with minimal set. Perfect model fit, filtering does nothing.

ys=C_t.dot(x[:,:])
us=u[:,:]

if plot_model1:
	# this model has worked when the system is stable:
	# y= b1 u-, b2 u--, b3 u-- ... 
	class Model1(Abstract_ARX):
		def __init__(self):
			Abstract_ARX.__init__(self,num_params=fir_order, num_outputs=0, num_inputs=fir_order)
		def gen_regressor_block(self,y,u):
			return np.array(u[:,:])
		def gen_estimate(self,y,u):
			return np.array([[0.0]])
	model1=Model1()
	
	
	order=fir_order
	times=fir_order
	phi=np.zeros((num-times,order))
	b=np.zeros((num-times,1))
	for i in range(times,num):
		for z in range(0,times):
			phi[i-times,z]=u[0,i-1-z]
		b[i-times,0]=C_t.dot(x[:,i])
	phi = phi[1:,:]
	b = b[1:,:]
	nConv=100
	sigma_in_time=1.0
	model1.setup_regressor(ys,us)
	sum_diff = 0
	sum_phi =0
	sum_b_diff=0
	for r in range (0,phi.shape[0]):
		for c in range (0,phi.shape[1]):
			sum_diff += abs(phi[r,c]-model1.regressor[r,c])
			sum_phi += abs(phi[r,c])
		sum_b_diff+=abs(b[r,0]-model1.ideal_output[r,0])
	print sum_diff, sum_phi, sum_b_diff

	model,residuals,rank,singular_values=np.linalg.lstsq(phi,b)
	model1.simple_lstsq(ys,us)
	model1_step_ys=model1.filter(y_step_padded,u_step_padded)
	print y_step_padded.shape, t_step_padded.shape,  model1_step_ys.shape
	step_response_axis.plot(t_step_padded,model1_step_ys[0,:])


	for init in starting_points:
		sim_y=np.zeros((num,1))
		for i in range(0,init):
			sim_y[i,0]=C_t.dot(x[:,i])
		sim_u=u.T
		sim_y1=model1.filter(ys[:,init:],us[:,init:])
		output_overlay_axis.plot(ts[init:],sim_y1[0,:])
		for i in range(init,num):
			phi_i=np.zeros((1,order))
			for z in range(0,times):
				phi_i[0,z]=sim_u[i-1-z,0]
			sim_y[i,0]=phi_i.dot(model)
		# output_overlay_axis.plot(ts[init:-1],sim_y[init:-1,0])
		# step_response_axis.plot(ts[0:model.shape[0]],model[:,0])
		# step_response_axis.plot(ts[0:model.shape[0]],model1.params[:,0])


if plot_model2:
	# this model works:
	# y^ = a1 y- + a2 y-- + a3 y--- + b1 u- + b2 u-- + b3 u---
	# order=6

	# nConv=100
	# sigma_in_time=1.0
	# dt=ts[1]-ts[0]
	# conv, diffKernel, ddifKernal = setupConvolutionFilters(
	# 	dt, sigma_in_time, nConv=nConv)

	# times=3
	class Model2(Abstract_ARX):
		def __init__(self):
			Abstract_ARX.__init__(self,num_params=6, num_outputs=3, num_inputs=3)
		def gen_regressor_block(self,y,u):
			return np.concatenate([-y[:,:],u[:,:]],axis=1)
		def gen_estimate(self,y,u):
			return np.array([[0.0]])
	model2=Model2()
	model2.simple_lstsq(ys,us)
	model2_step_ys=model2.filter(y_step_padded,u_step_padded)
	step_response_axis.set_autoscaley_on(False)
	step_response_axis.plot(t_step_padded,model2_step_ys[0,:])

	for init in starting_points:
		sim_y2=model2.filter(ys[:,init:],us[:,init:])
		output_overlay_axis.set_autoscaley_on(False)
		output_overlay_axis.plot(ts[init:],sim_y2[0,:])

if plot_model3:
	# y^ = a1 y- + a2 y-- + a3 y--- + b1 u- + b2 u-- + b3 u---
	# no filtering the equations.
	order=6
	times=3
	phi=np.zeros((num-times,order))
	b=np.zeros((num-times,1))
	for i in range(times,num):
		phi[i-times,:]=np.array([
			-C_t.dot(x[:,i-1]),
			-C_t.dot(x[:,i-2]),
			-C_t.dot(x[:,i-3]),
			u[0,i-1],
			u[0,i-2],
			u[0,i-3]
			])
		b[i-times,0]=C_t.dot(x[:,i])
	nConv=100
	sigma_in_time=1.0
	dt=ts[1]-ts[0]
	convolutionKernel, diffKernel, ddifKernal = setupConvolutionFilters(
		dt, sigma_in_time, nConv=nConv)
	new_phi=np.zeros((num-times,order))
	for i in range(0,phi.shape[1]):
		new_phi[:,i]=np.convolve(phi[:,i], convolutionKernel)[nConv:-nConv]
	new_b=np.zeros((num-times,1))
	for i in range(0,b.shape[1]):
		new_b[:,i]=np.convolve(b[:,i], convolutionKernel)[nConv:-nConv]
	# b=np.convolve(b.T, convolutionKernel)
	model,residuals,rank,singular_values=np.linalg.lstsq(new_phi,new_b)
	print model
	for init in starting_points:
		sim_y=np.zeros((num,1))
		for i in range(0,init):
			sim_y[i,0]=C_t.dot(x[:,i])
		sim_u=u.T
		for i in range(init,num):
			y_hat=(np.array([[
				-sim_y[i-1,0],
				-sim_y[i-2,0],
				-sim_y[i-3,0],
				sim_u[i-1,0],
				sim_u[i-2,0],
				sim_u[i-3,0]
				]])).dot(model)
			sim_y[i,0]=y_hat[0,0]
		output_overlay_axis.set_autoscaley_on(False)
		output_overlay_axis.plot(ts[init:-1],10.0*sim_y[init:-1,0])
		# print dir(plt.gcf().get_axes()[0])

if plot_model4:
	# this model has worked when the system is stable:
	# y= b1 u-, b2 u--, b3 u-- ... 
	order=fir_order
	times=fir_order
	phi=np.zeros((num-times,order))
	b=np.zeros((num-times,1))
	for i in range(times,num):
		for z in range(0,times):
			phi[i-times,z]=u[0,i-1-z]
		b[i-times,0]=C_t.dot(x[:,i])
	nConv=100
	sigma_in_time=2.0 
	dt=ts[1]-ts[0]
	convolutionKernel, diffKernel, ddifKernal = setupConvolutionFilters(
		dt, sigma_in_time, nConv=nConv)
	new_phi=np.zeros((num-times,order))
	for i in range(0,phi.shape[1]):
		new_phi[:,i]=np.convolve(phi[:,i], convolutionKernel)[nConv:-nConv]
	new_b=np.zeros((num-times,1))
	for i in range(0,b.shape[1]):
		new_b[:,i]=np.convolve(b[:,i], convolutionKernel)[nConv:-nConv]
	# b=np.convolve(b.T, convolutionKernel)
	model,residuals,rank,singular_values=np.linalg.lstsq(new_phi,new_b)
	# prediction= phi.dot(model)
	for init in starting_points:
		sim_y=np.zeros((num,1))
		for i in range(0,init):
			sim_y[i,0]=C_t.dot(x[:,i])
		sim_u=u.T

		for i in range(init,num):
			phi_i=np.zeros((1,order))
			for z in range(0,times):
				phi_i[0,z]=sim_u[i-1-z,0]
			sim_y[i,0]=phi_i.dot(model)
		output_overlay_axis.set_autoscaley_on(False)
		output_overlay_axis.plot(ts[init:-1],10.0*sim_y[init:-1,0])
		step_response_axis.plot(ts[0:model.shape[0]],model[:,0])

if plot_model5:
	# this model works:
	# y^ = a1 y- + a2 y-- + b2 u--
	order=3
	times=3
	phi=np.zeros((num-times,order))
	b=np.zeros((num-times,1))
	for i in range(times,num):
		phi[i-times,:]=np.array([
			C_t.dot(x[:,i-1]),
			C_t.dot(x[:,i-2]),
			u[0,i-2],
			])
		b[i-times,0]=C_t.dot(x[:,i])
	nConv=100
	sigma_in_time=0.8
	dt=ts[1]-ts[0]
	convolutionKernel, diffKernel, ddifKernal = setupConvolutionFilters(
		dt, sigma_in_time, nConv=nConv)
	new_phi=np.zeros((num-times,order))
	for i in range(0,phi.shape[1]):
		new_phi[:,i]=np.convolve(phi[:,i], convolutionKernel)[nConv:-nConv]
	new_b=np.zeros((num-times,1))
	for i in range(0,b.shape[1]):
		new_b[:,i]=np.convolve(b[:,i], convolutionKernel)[nConv:-nConv]
	# b=np.convolve(b.T, convolutionKernel)
	model,residuals,rank,singular_values=np.linalg.lstsq(new_phi,new_b)
	print model
	for init in starting_points:
		sim_y=np.zeros((num,1))
		for i in range(0,init):
			sim_y[i,0]=C_t.dot(x[:,i])
		sim_u=u.T
		for i in range(init,num):
			y_hat=(np.array([[
				sim_y[i-1,0],
				sim_y[i-2,0],
				sim_u[i-2,0]
				]])).dot(model)
			sim_y[i,0]=y_hat[0,0]
		output_overlay_axis.plot(ts[init:-1],10.0*sim_y[init:-1,0])
		# print dir(plt.gcf().get_axes()[0])
plt.show()

