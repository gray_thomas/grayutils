import numpy as np 
class Abstract_ARX:
	def __init__(self, num_params, num_inputs, num_outputs):
		self.num_params=num_params
		self.num_inputs=num_inputs
		self.num_outputs=num_outputs
		self.params=np.zeros((self.num_params,1))
	def gen_regressor_block(self,y,u):
		""" generates regressor for numpy arrays u and y with 
		shapes [udim,time_elements] and [ydim, time_elements]
		phi has dimension [ydim, self.num_params] and 
		y_estimate = model.gen_phi(u,y).dot(parameters)
		model specific.
		this method basically describes the model. This class is
		just making it easier to think a little more generally
		about the various models."""
		pass
	def gen_estimate(self,y,u):
		""" this just returns the portion of ytilde that is not
		based on paramters. For example, in a second order system
		the terms necessary to express that the model estimates the
		second derivative in terms of first order expansions. """
		pass
	def get_parameter_size(self):
		""" return the self.number of parameters in the model """
		return self.num_params
	def get_input_depth(self):
		""" return the self.number of inputs the model uses """
		return self.num_inputs
	def get_output_depth(self):
		""" return the self.number of outputs the model uses """
		return self.num_outputs
	def filter(self, output_history,inputs):
		""" output_history ~ [ydim, self.times] 
			inputs ~ [udim, self.times-1]
			returns: outputs ~ [ydim, self.times] 
			starts simulating at t=max(self.num_inputs,self.num_outputs)"""
		ys=np.zeros(output_history.shape)
		us=inputs
		t_start=max(self.num_inputs,self.num_outputs)+1
		ys[:,0:t_start]=output_history[:,0:t_start]
		for k in range(t_start,ys.shape[1]):
			ys_block=ys[:,(k-1):(k-1-self.num_outputs):-1]
			us_block=us[:,(k-1):(k-1-self.num_inputs):-1]
			ys[:,k]=self.gen_regressor_block(
				ys_block,us_block).dot(self.params)+self.gen_estimate(ys_block,us_block)
		return ys

	def setup_regressor(self,ys,us):
		""" ys [ysize,self.times] us [usize, self.times-1]"""
		print "hey", ys.shape, us.shape
		self.num=ys.shape[1]
		self.output_size=ys.shape[0]
		self.input_size=us.shape[0]
		self.times=max(self.num_inputs,self.num_outputs)+1
		""" regressor blocks for each vector of outputs. """
		self.regressor=np.zeros(((self.num-self.times)*self.output_size,self.num_params))
		self.ideal_output=np.zeros(((self.num-self.times)*self.output_size,1))
		""" block equation: y - get_est = get_regressor_block * params """
		for i in range (0,self.num-self.times):
			k=self.times+i
			block_start=self.output_size*i 
			block_end=self.output_size*(i+1)
			output_history=ys[:,(k-1):(k-1-self.num_outputs):-1]
			input_history=us[:,(k-1):(k-1-self.num_inputs):-1]
			self.regressor[block_start:block_end,:]=self.gen_regressor_block(
														output_history, input_history)
			self.ideal_output[block_start:block_end,0]=ys[:,k]-self.gen_estimate(
														output_history, input_history)
		return self.regressor , self.ideal_output
	def simple_lstsq(self,ys,us):
		reg, out = self.setup_regressor(ys,us)
		self.params, self.id_residuals, self.id_rank, self.id_singular_values=np.linalg.lstsq(reg,out)
	def filt_lstsq(self,ys,us, conv):
		""" as simple lstsq, but conv [nconv*2+1,] is used to convolve the regressor and ideal output  
			from a theoretical standpoint, conv is the (bidirectional) impulse response of the inverse
			of the expected noise transfer function. """
		reg, out = self.setup_regressor(ys,us)
		nConv=(conv.shape[0]-1)/2
		OnConv = self.output_size*nConv
		new_num =reg.shape[0]+(-conv.shape[0]+1)*self.output_size
		conv.reshape((1,-1)) # transpose, make it a row vector
		f_reg = np.zeros((new_num,self.num_params))
		f_io = np.zeros((new_num,1))
		print reg.shape
		print conv.shape,reg[0:0+self.output_size*conv.shape[1]:self.output_size,:].shape
		for r in range (0,new_num):
			f_reg[r,:]=conv.dot(reg[r:r+self.output_size*conv.shape[1]:self.output_size,:])

		self.params, self.id_residuals, self.id_rank, self.id_singular_values=np.linalg.lstsq(reg,out)
