from Transform import *
import numpy as np
#Circle of radius radius_ centered at (0,0)
class Circle:
	def __init__(self,name_,radius_,transform_=Transform(np.zeros(2),0.0)):
		self.name = name_
		self.T0 = transform_

		self.radius = radius_
		self.center = np.zeros(2)
		self.update()

	def getStaticTransform(self):
		return self.T0

	def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
		T0 = baseTransform_.getWorldSE2(),self.T0.getWorldSE2()
		self.updateFromMatrix(T0)

	def updateFromMatrix(self, baseTMatrix_):
		T0 = np.dot(baseTMatrix_,self.T0.getWorldSE2())
		self.center = T0[0:2,2]		
		print "Circle ",self.name,"is ", self.center

	# def draw(self,ax_,color_='r'):
	# 	circ = plt.Circle((self.center[0],self.center[1]),radius=self.radius,color=color_)
	# 	ax_.add_patch(circ)