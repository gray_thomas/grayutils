#Set of classes to enable dynamic simulations of serial mult-body
#systems in 2d for HW4 of ME397
import numpy as np
from math import atan2

#Encapuslation of a 2d rigid body transform from F0 to F1
#d_ is offset from F0 to F1, represented in F0
#theta_ parametrizes rotation from F0 to F1 
#s.t. x_1 {represented in F0} = R(theta_) x_0
class Transform:
	def __init__(self,d_=np.zeros(2),theta_=0.0):
		self.d = d_[:]
		self.theta = theta_
		self.R = np.zeros((2,2))
		self.T_1to0 = np.zeros((3,3))
		self.T_1to0[2,2] = 1.0
		self.T_1to0[0:2,2] = d_
		self.T_0to1 = np.zeros((3,3))
		self.T_0to1[2,2] = 1.0
		self.T_0to1[0:2,2] = -self.R.T.dot(d_)
		self.__update()

	# def __mul__(self,other):
	# 	ret=Transform()
	# 	ret.T_1to0=self.T_1to0.dot(other.T_1to0)
	# 	ret.theta=atan2(ret.T_1to0[1,0],ret.T_1to0[0,0])
	# 	ret.d=ret.T_1to0[[0,1],2]
	# 	ret.__update()
		
	def updateMatrix(self,mat):
		self.T_1to0=mat
		self.theta=atan2(self.T_1to0[1,0],self.T_1to0[0,0])
		self.d=self.T_1to0[0:2,2]
		self.__update()

	def __update(self):
		#set rotation matrix
		c = np.cos(self.theta)
		s = np.sin(self.theta)
		self.R = np.array([[c,-s],[s,c]])
		#transforms a point represented in 1 to 0
		self.T_1to0[0:2,0:2] = self.R
		self.T_1to0[0:2,2] = self.d
		#transforms a point represented in 0 to 1
		self.T_0to1[0:2,0:2] = np.transpose(self.R)
		self.T_0to1[0:2,2] = np.dot(-np.transpose(self.R),self.d)

	def updateTheta(self,theta_):	
		self.theta = theta_
		self.__update()

	def updateDisplacement(self,d_):
		self.d = d_.squeeze()
		self.__update()

	def bodyifyPoint(self, point):
		augmented_point=np.ones((3,1))
		augmented_point[0:2,0]=point
		new_aug_point=self.T_1to0.dot(augmented_point)
		return new_aug_point[0:2,0]

	def bodyifyPoints(self, points):
		num_points=points.shape[1]
		aug_points=np.ones((3,num_points))
		aug_points[0:2,:]=points
		new_aug_points=self.T_1to0.dot(aug_points)
		return new_aug_points[0:2,:]

	def worldifyPoint(self, point):
		augmented_point=np.ones((3,1))
		augmented_point[0:2,0]=point
		new_aug_point=self.T_1to0.dot(augmented_point)
		return new_aug_point[0:2,0]

	def worldifyPoints(self, points):
		num_points=points.shape[1]
		aug_points=np.ones((3,num_points))
		aug_points[0:2,:]=points
		new_aug_points=self.T_1to0.dot(aug_points)
		return new_aug_points[0:2,:]

	def transformVector(self, vector):
		return self.T_1to0[0:2,0:2].dot(vector)
	def getWorldSE2(self):
		return self.T_0to1
	def getBodySE2(self):
		return self.T_1to0
