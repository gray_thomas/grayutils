import numpy as np
import exceptions as exc
from Transform import *
import matplotlib.pyplot as plt
import matplotlib.animation as animation 
import thread
from math import sin, cos, pi

class AnimatedPlot:
	""" a matplotlib animated figure with various animated primitives """
	def __init__(self):
		print "hello shapes!"
		self.__fig=plt.figure()
		self.__ax=self.__fig.add_subplot(
	            111, autoscale_on=False, xlim=(-10, 10), ylim=(-10, 10))
		self.__ax.axis('equal')
		self.__ax.axis([-10,10,-10,10])
		self.__objects=[]

	def __anim(self, i):
		for obj in self.__objects:
			obj.animate()

	def test_animate(self):
		self.__anim(0)

	def attach_animated_primitive(self, object):
		""" attach a primitive. That primitive must have an animate method """
		self.__objects.append(object)

	@property
	def axis(self):
		return self.__ax;

	def begin_animation(self, range_, **kwargs):
		""" starts animating. Call this right before you show the plot """
		self.__ani = animation.FuncAnimation(self.__fig, self.__anim, range_,
            **kwargs)


class Line:
	""" Plots a line based on a matrix of points """
	def __init__(self, ani_plot, points, **kwargs):
		self.__data = points[:,:]
		self.graphical_line, = ani_plot.axis.plot(points[0,:],points[1,:],'k-')
		tf = Transform(theta_=0.0)
		self.graphical_line.set_data(tf.worldifyPoints(points))
		ani_plot.attach_animated_primitive(self)

	@property
	def fetch_transform(self, fetch_transform_):
		""" the function used to obtain the artist transform """
		return self.__fetch_transform

	@fetch_transform.setter
	def fetch_transform(self, fetch_transform_):
		""" don't forget to set this function """
		self.__fetch_transform=fetch_transform_

	def animate(self):
		temp_data= self.fetch_transform().worldifyPoints(self.__data)
		self.graphical_line.set_data(temp_data)
		return self.graphical_line

#Dot of of radius_ centered at (0,0)
class Dot:
	""" plots a point. Forwards keyword arguments to the plot function """
	def __init__(self, ani_plot, x_, y_, **kwargs):
		self.center = np.array([x_,y_])
		# axis.plot returns a list, but by putting a comma 
		# after self.graphical_point I subtly save 
		# the first element, rather than the list itself
		self.graphical_point, =ani_plot.axis.plot([x_],[y_],'o',**kwargs)
		ani_plot.attach_animated_primitive(self)

	@property
	def fetch_transform(self, fetch_transform_):
		""" the function used to obtain the artist transform """
		return self.__fetch_transform

	@fetch_transform.setter
	def fetch_transform(self, fetch_transform_):
		""" don't forget to set this function """
		self.__fetch_transform=fetch_transform_
	
	def animate(self):
		temp_center= self.fetch_transform().worldifyPoint(self.center)
		self.graphical_point.set_data(temp_center)
		return self.graphical_point

class FakeDataGenerator:
	""" used in place of a real simulation """
	def __init__(self,plot,joint_angles):
		plot.attach_animated_primitive(self)
		self.i=0
		self._joint_angles=joint_angles
		self.T0 = Transform()
		self.T1 = Transform()
	def animate(self):
		self.i+=1
		self._joint_angles[0]=0.03*self.i+1
		self._joint_angles[1]=0.05*cos(self.i*0.1+1)


def rect (width_, height_):
	w=width_*0.5
	h=height_*0.5
	return np.array([[-w,w,w,-w,-w],[-h,-h,h,h,-h]])

def rotate(angle, points):
	r=np.array([[cos(angle),-sin(angle)],[sin(angle),cos(angle)]])
	return r.dot(points)

def translate(x,y,points):
	f=np.ones(points.shape)
	d=np.array([[x,0],[0,y]])
	return points + d.dot(f)

def draw_awesome_link(plot, length_):
	th_3=pi*2./3.
	th_4=pi*0.25
	th_5=pi*2./5.
	link_drawing=[]
	link_drawing.append(Dot(plot, x_=0, y_=0, ms=8, color='black'))
	link_drawing.append(Dot(plot, x_=length_, y_=0, ms=4, color='red'))
	l=length_*0.1
	link_drawing.append(Line(plot, l*np.array([[0., 0., 1., 9., 9., 10.],[0., 1., 1., 0.5, 0.5, 0.]])))
	link_drawing.append(Line(plot, l*np.array([[0., 0., 1., 9., 9., 10.],[0., -1., -1., -0.5, -0.5, 0.]])))
	for i in range(0,5):
		link_drawing.append(Line(plot, rotate(i*th_5,translate(l*2,0,rect(l,l)))))
		link_drawing.append(Line(plot, rotate(i*th_5+th_5*0.5,translate(l*1.5,0,rect(l,l)))))
	for i in range(1,3):
		link_drawing.append(Line(plot, translate(10*l,0,rotate(th_3*i,translate(l*0.5,0,rect(l,l*0.5) )))))
	link_drawing.append(Line(plot, translate(5*l,0,rect(4*l,l))))
	for i in range(4,7):
		link_drawing.append(Line(plot, translate(i*l,0,rect(0.333*l,l))))
	return link_drawing

def draw_awesome_link_2(plot, my_x, my_y):
	th_3=pi*2./3.
	th_4=pi*0.25
	th_5=pi*2./5.
	omeg=atan2(my_y,my_x)
	# magnitude=sqrt(my_y**2+my_x**2)
	trans=np.array([[my_x,-my_y],[my_y,my_x]])

	link_drawing=[]
	link_drawing.append(Dot(plot, x_=0, y_=0, ms=8, color='black'))
	link_drawing.append(Dot(plot, x_=my_x, y_=my_y, ms=4, color='red'))
	# l=magnitude*0.1
	link_drawing.append(Line(plot, 0.1*trans.dot(
		np.array([[0., 0., 1., 9., 9., 10.],
			[0., 1., 1., 0.5, 0.5, 0.]]))))
	link_drawing.append(Line(plot, 0.1*trans.dot(
		np.array([[0., 0., 1., 9., 9., 10.],
			[0., -1., -1., -0.5, -0.5, 0.]]))))
	for i in range(0,5):
		link_drawing.append(Line(plot, 0.1*trans.dot(
			rotate(i*th_5,translate(1*2,0,rect(1,1))))))
		link_drawing.append(Line(plot, 0.1*trans.dot(
			rotate(i*th_5+th_5*0.5,translate(1.5,0,rect(1,1))))))
	for i in range(1,3):
		link_drawing.append(Line(plot, 
			0.1*trans.dot(
				translate(10,0,
					rotate(th_3*i,
						translate(0.5,0,
							rect(1,0.5) 
						)
					)
				)
			)
		))
	link_drawing.append(Line(plot, 0.1*trans.dot(
		translate(5.0,0.0,rect(4,1.0)))))
	for i in range(4,7):
		link_drawing.append(Line(plot, 0.1*trans.dot(
			translate(i*1.0,0.0,rect(0.333,1.0)))))
	return link_drawing

def testShapes():
	print "hello shapes"
	plot=AnimatedPlot()

	drawing_of_body_1 = [
			Line(plot, np.array([[1., 1., 2., 3.],[0.,1.,1.,2.]])),
			Line(plot, rect(1.5,2)),
			Line(plot, translate(0.2,0,rotate(0.1,rect(1.0,0.5)))),
			Dot(plot, x_=2, y_=2, ms=5, color='red'),
			Dot(plot, x_=0, y_=0, ms=8, color='blue')]

	link2_drawing=draw_awesome_link(plot, 4.)
	joint_angles=np.array([1.0,1.0])

	def fetch_tf1():
		return Transform(theta_=joint_angles[0])
	def fetch_tf2():
		return Transform(theta_=joint_angles[1])
	# fetch_tf1=lambda : Transform(theta_=joint_angles[0])
	for obj in drawing_of_body_1:
		obj.fetch_transform=fetch_tf1
	for obj in link2_drawing:
		obj.fetch_transform=fetch_tf2
	
	FakeDataGenerator(plot,joint_angles)
	print np.arange(1,30)
	plot.begin_animation(np.arange(1, 300), interval=30, blit=False)
	plt.show()


if __name__=="__main__":
	testShapes()