""" Python script to solve numerical problem """
import numpy as np 
import cvxpy as cvx 
import time
import os
GRAY_UTILS=os.getenv("GRAY_UTILS_HOME") # environment variable set by that script
proj_folder=GRAY_UTILS+"tex/homework-ConvexOptimization/hw2/"
print "outputing to " + proj_folder
start_time=time.time()

""" start doing math """
n=70
m=1
r0=cvx.Constant(np.array([[3e3],[3e3]]))
v0=cvx.Constant(np.array([[-20.0],[-80.0]]))
rf=cvx.Constant(np.array([[0.0],[0.0]]))
vf=cvx.Constant(np.array([[0.0],[0.0]]))
g=cvx.Constant(np.array([[0.0],[-3.0]]))
r=cvx.Variable(2,n)
v=cvx.Variable(2,n)
u=cvx.Variable(2,n-1)
cons=[]
cons.append(r[:,0]==r0)
cons.append(r[:,n-1]==rf)
cons.append(v[:,0]==v0)
cons.append(v[:,n-1]==vf)
identity2=cvx.Constant(np.eye(2))
optimization_condition=cvx.Constant(0)
for k in range(1,n):
	cons.append(r[:,k]==r[:,k-1]+v[:,k-1]+0.5*(u[:,k-1]+g))
	cons.append(v[:,k]==v[:,k-1]+u[:,k-1]+g)
	cons.append(cvx.norm2(u[:,k-1])<=10)
	cons.append(r[1,k]>=0.0)
	cons.append(r[0,k]>=-500.0)
	if m==1:
		optimization_condition+=cvx.norm2(u[:,k-1])
	else:
		optimization_condition+=cvx.quad_form(u[:,k-1],identity2)
prob=cvx.Problem(cvx.Minimize(optimization_condition),cons)
optimal_cost=prob.solve(solver=cvx.CVXOPT)
print optimal_cost
""" start plotting """
import matplotlib.pyplot as plt
fig=plt.figure()
axs=[]
axs.append(fig.add_subplot(3,2,1))
for i in range(1,6):
	axs.append(fig.add_subplot(3,2,i+1,sharex=axs[0]))
vals=[]
for e in [u,r,v]:
	for i in [0,1]:
		vals.append(e.value[[i],:].T)
names=[]
for e in ["u","r","v"]:
	for i in ["0","1"]:
		names.append("$"+e+"_"+i+"$")
for i in range(0,6):
	axs[i].plot(vals[i])
	axs[i].set_ylabel(names[i],fontsize=18)
	axs[i].set_xlabel("$k$",fontsize=18)
axs[0].set_title("n=%d, m=%d"% (n,m),fontsize=14)
axs[1].set_title("(%s seconds)"% (time.time()-start_time),fontsize=14)
plt.tight_layout()
plt.savefig(proj_folder+"num_m_%d_n_%d.pdf"%(m,n))

plt.show()
# print r.value


