""" Aircraft_LQR.py """
""" This file generates all figures used in the report. """
from linearUtils.discretized_optimization import * # My library, uses cvxpy
from linearUtils.discrete_plots import * # Another library of mine. 
from linearUtils.discrete_simulation import * # Also mine, does euler integration


import numpy as np
import matplotlib.pyplot as plt
import math
import control # requires a fortran complier to get working, but solves AREs
from math import sin, cos
import os

# Dr. Bakolas if you want to run this then you will need to download the following
# git clone http://bitbucket.org/gray_thomas/grayutils.git
# cd grayutils
# source ./thisFolderShallBeGrayUtilsHome.sh
# you will also need to install the dependancy libraries 
# (python control, cloned within the main branch; 
# pycvx, and its dependencies).
# This would be hard, and pretty low reward, 
# but if you want to run it I can help you set it up.
# This stuff runs on 2 machines, but I haven't standardized the 
# install process for the convex solvers
# I'm pretty sure I can get the ARE solver to work on windows through cygwin

GRAY_UTILS=os.getenv("GRAY_UTILS_HOME") # environment variable set by that script
proj_folder=GRAY_UTILS+"tex/homework-OptimalControl/project-LQR/figs/"
print "outputing to " + proj_folder

""" Define all the important project data """
# names used by plotting code. problem specific, so I keep them with the data.
x_names=[
    r"$u(t)\;(\frac{ft}{s})$",
    r"$w(t)\;(\frac{ft}{s})$",
    r"$q(t)\;(\frac{crad}{s})$",
    r"$\theta(t)\;(crad)$",
    r"$h(t)\;(ft)$",
    r"$\delta t(t)\;(\frac{ft}{s^2})$ "
    ]
u_names=[
    r"$\delta e(t) \; (crad)$",
    r"$\delta t_c(t) \; (\frac{ft}{s^2})$"
    ]

A1=np.array([
        [-0.021,  0.122,    0.0, -0.322, 0.0,    1.0],
        [-0.209, -0.530,   2.21,    0.0, 0.0, -0.044],
        [ 0.017, -0.164, -0.412,    0.0, 0.0,  0.544],
        [   0.0,    0.0,    1.0,    0.0, 0.0,    0.0],
        [   0.0,   -1.0,    0.0,   2.21, 0.0,    0.0],
        [   0.0,    0.0,    0.0,    0.0, 0.0,  -0.25]
    ])

B1=np.array([
        [  0.01,   0.0],
        [-0.064,   0.0],
        [   0.0,   0.0],
        [   0.0,   0.0],
        [   0.0,   0.0],
        [   0.0,  0.25]
    ])


""" Setup the pilot's terrible landing trajectory """
h0=58.0 # feet off the ground
xref0=np.array([[0.0, 0.4471, 0.0, -5.0337, 58.0, -1.6497]]).T
uref0=np.array([[-2.5682, -1.6497]]).T
xreff=np.array([[-15.0, 6.3628, 0.0, 2.8791, -7.0, -0.128]]).T
ureff=np.array([[-3.6194, -0.128]]).T
a=1/6.5
tf = math.log(58.0/7.0+1)/a
n=500 # number used in discretized simulations
def get_x_ref(t):
    return xreff+math.exp(-a*t)*(xref0-xreff)
def get_u_ref(t):
    return ureff+math.exp(-a*t)*(uref0-ureff)

""" Solve part 1 of the project """
check_controllability=False
if check_controllability:
    Wc=np.concatenate([B1,A1.dot(B1),A1.dot(A1).dot(B1)],axis=1)
    print latex_matrix(Wc)
    eigs,matU=np.linalg.eig(Wc)
    print np.divide((Wc).dot(matU) ,(matU))
    print latex_matrix(eigs.reshape((6,1)))


def solve_the_ARE_mu_and_nu(mu):
    """ Convenience function, generates K for a given mu
    Assumes the simple relationship from question a """
    r1=np.array([
        [math.sqrt(   0.05),0.0],
        [0.0,math.sqrt(1.0/3.0)]
    ])
    C1=math.sqrt(mu) * np.array([
            [math.sqrt( 1.0),0.0,0.0,0.0,0.0,0.0],
            [0.0,math.sqrt(0.05),0.0,0.0,0.0,0.0],
            [0.0,0.0,math.sqrt(0.05),0.0,0.0,0.0],
            [0.0,0.0,0.0,math.sqrt(0.05),0.0,0.0],
            [0.0,0.0,0.0,0.0,math.sqrt( 1.0),0.0],
            [0.0,0.0,0.0,0.0,0.0,math.sqrt( 0.2)]
        ])
    R1=r1.T.dot(r1)
    Q1=C1.T.dot(C1)
    # control.care solves the continuous algebraic riccati equation
    return control.care(A1,B1,Q1,R1) # K=R^{-1} B^T P

generate_pole_locus=False
if generate_pole_locus:
    plt.figure(figsize=(10,3))
    for mu in np.logspace(-4,4,500):
        P,lambda_A_cl,K=solve_the_ARE_mu_and_nu(mu) # K=R^{-1} B^T P
        plt.plot([z.real for z in lambda_A_cl],[z.imag for z in lambda_A_cl],'b.')
    plt.xlabel(r"$\sigma$",fontsize=14)
    plt.ylabel(r"$j\omega$",fontsize=14)
    plt.title("Pole Locus of the closed loop system\n$\\mu\\in[0.0001,1000]$")
    plt.axis("equal")
    plt.tight_layout()
    plt.savefig(proj_folder+"pole_locus.pdf")

generate_default_comparison=False
if generate_default_comparison:
    mu=100.0
    P,lambda_A_cl,K=solve_the_ARE_mu_and_nu(mu=mu)
    ts,x,u,xref,uref,dx,du=simulate_state_feedback_with_reference(
        A1,B1,K,xref0,get_x_ref,get_u_ref,tf=tf,n=n)
    plot_ref_actual_comparison(ts, x, u, xref, uref, x_names, u_names, figsize=(12,10))
    plt.savefig(proj_folder+("default_comparison_mu_%d.pdf" % int(math.ceil(mu*1000))))

def solve_the_ARE_mu_beta(mu, beta):
    """ Used in part2, this offers a choice of input to prioritize """
    r1=np.array([
        [math.sqrt(1.0/beta),0.0],
        [0.0,math.sqrt(    beta)]
    ])
    C1=math.sqrt(mu) * np.array([
            [math.sqrt( 1.0),0.0,0.0,0.0,0.0,0.0],
            [0.0,math.sqrt(0.05),0.0,0.0,0.0,0.0],
            [0.0,0.0,math.sqrt(0.05),0.0,0.0,0.0],
            [0.0,0.0,0.0,math.sqrt(0.05),0.0,0.0],
            [0.0,0.0,0.0,0.0,math.sqrt( 1.0),0.0],
            [0.0,0.0,0.0,0.0,0.0,math.sqrt( 0.2)]
        ])
    R1=r1.T.dot(r1)
    Q1=C1.T.dot(C1)
    # control.care solves the continuous algebraic riccati equation
    return control.care(A1,B1,Q1,R1) # K=R^{-1} B^T P
compare_Ks=[]
compare_K_names=[]
compare_poles=[]
compare_xs=[]
compare_us=[]
compare_ts=[]
compare_xref=[]
compare_uref=[]
generate_beta_comparison_plot=False


def test_mu_beta(mu,beta):
    """ dirty convenience function. Hides side effect of 
        building the comparison values. """
    P,lambda_A_cl,K=solve_the_ARE_mu_beta(mu=mu,beta=beta)
    compare_Ks.append(K)
    compare_K_names.append("$\\mu=%.2f,\\beta=%.2f$"%(mu,beta))
    compare_poles.append(lambda_A_cl)
    ts,x,u,xref,uref,dx,du=simulate_state_feedback_with_reference(
        A1,B1,K,xref0,get_x_ref,get_u_ref,tf=tf,n=n)
    compare_xs.append(x)
    compare_us.append(u)
    compare_ts.append(ts)
    compare_xref.append(xref)
    compare_uref.append(uref)
    return P,lambda_A_cl,K

def get_is_above_ground(xs):
    return lambda i: xs[int(i)]>0

def test_hand_picked():

    r1=np.array([
        [math.sqrt(1.0/5.0),0.0],
        [0.0,math.sqrt(5.0)]
    ])
    C1=np.array([
            [math.sqrt( 10.0),0.0,0.0,0.0,0.0,0.0],
            [0.0,math.sqrt(20.00),0.0,0.0,0.0,0.0],
            [0.0,0.0,math.sqrt(0.2),0.0,0.0,0.0],
            [0.0,0.0,0.0,math.sqrt(0.04),0.0,0.0],
            [0.0,0.0,0.0,0.0,math.sqrt( 10.0),0.0],
            [0.0,0.0,0.0,0.0,0.0,math.sqrt( 0.01)]
        ])
    R1=r1.T.dot(r1)
    Q1=C1.T.dot(C1)
    print latex_matrix(R1)
    print latex_matrix(Q1)
    P,lambda_A_cl,K = control.care(A1,B1,Q1,R1)
    compare_Ks.append(K)
    compare_K_names.append("hand picked")
    compare_poles.append(lambda_A_cl)
    ts,x,u,xref,uref,dx,du=simulate_state_feedback_with_reference(
        A1,B1,K,xref0,get_x_ref,get_u_ref,tf=tf,n=n)
    compare_xs.append(x)
    compare_us.append(u)
    compare_ts.append(ts)
    compare_xref.append(xref)
    compare_uref.append(uref)
    return P,lambda_A_cl,K
import linearUtils
compare_6=False
if compare_6:
    test_mu_beta(mu=10.0,beta=1.0)
    test_mu_beta(mu=10.0,beta=4.0)
    test_mu_beta(mu=10.0,beta=0.25)
    test_mu_beta(mu=40.0,beta=1.0)
    test_mu_beta(mu=2.5,beta=1.0)
    test_hand_picked()
    plt.figure(figsize=(10,10))
    for i in range(0,len(compare_Ks)):
        zs=compare_poles[i]
        plt.plot([z.real for z in zs],[z.imag for z in zs],'x', ms=10)
    plt.legend(compare_K_names)
    plt.xlabel(r"$\sigma$",fontsize=14)
    plt.ylabel(r"$j\omega$",fontsize=14)
    plt.title("Pole Locus of Various Optimal Systems$")
    plt.axis("equal")
    plt.tight_layout()
    plt.savefig(proj_folder+"compare_6.pdf")
    plot_bonus_comparison(compare_ts[0], compare_xs, compare_us, compare_xref[0], compare_uref[0],
        compare_K_names, x_names, u_names, figsize=(12,10),ncols=3)
    for xs,ts in zip(compare_xs,compare_ts):
        var=xs[4,:]
        is_above_ground=get_is_above_ground(var)
        print ts[linearUtils.bisection(is_above_ground, 0, len(var)-1, 1)]

    plt.savefig(proj_folder+"bonus_compare_6.pdf")




if generate_beta_comparison_plot:
    mu=10.0
    beta=0.1 # high beta penalizes thrust
    P,lambda_A_cl,K=test_mu_beta(mu=mu,beta=beta)
    ts,x,u,xref,uref,dx,du=simulate_state_feedback_with_reference(
        A1,B1,K,xref0,get_x_ref,get_u_ref,tf=tf,n=n)
    plot_ref_actual_comparison(ts, x, u, xref, uref, x_names, u_names, figsize=(12,10))
    plt.savefig(proj_folder+("mu_%d_beta_%d.pdf" % 
        (int(math.ceil(mu*1000)) , int(math.ceil(beta*1000)))
        ) )

# ts,x,u,xref,uref,dx,du=simulate_state_feedback_with_reference(
#     A1,B1,np.zeros(K.shape),xref0,get_x_ref,get_u_ref,tf=tf,n=n)
# plot_ref_actual_comparison(ts, x, u, xref, uref, x_names, u_names, figsize=(12,10))
# plt.savefig(proj_folder+"pure_reference.pdf")
# plt.savefig("")
""" use discretization and a sneaky inequality constraint to make this new autopilot """
optimize_pilot_trajectory=True
if optimize_pilot_trajectory:
    tf=14.5
    n=60
    rd=np.array([
            [math.sqrt(10),0.0],
            [0.0,math.sqrt(5.0)]
        ])
    Cd=1.0*np.array([
            [0.1,0.0,0.0,0.0,0.0,0.0],
            [0.0,0.1,0.0,0.0,0.0,0.0],
            [0.0,0.0,0.1,0.0,0.0,0.0],
            [0.0,0.0,0.0,0.1,0.0,0.0],
            [0.0,0.0,0.0,0.0,0.1,0.0],
            [0.0,0.0,0.0,0.0,0.0,0.1]
            ])
    Ce=np.array([
            [math.sqrt( 1.0),0.0,0.0,0.0,0.0,0.0],
            [0.0,math.sqrt(60.00),0.0,0.0,0.0,0.0],
            [0.0,0.0,math.sqrt( 1.0),0.0,0.0,0.0],
            [0.0,0.0,0.0,math.sqrt( 1.0),0.0,0.0],
            [0.0,0.0,0.0,0.0,math.sqrt( 50.0),0.0],
            [0.0,0.0,0.0,0.0,0.0,math.sqrt(  1.0)]
            ])
    def disc_convert(t):
        return int(t*n/tf)
    def disc_interp(t,vals):
        lo = math.floor(t*n/tf)
        hi = math.ceil(t*n/tf)
        if hi>=vals.shape[1]:
            hi=vals.shape[1]-1
        alpha1=t*n/tf - lo
        alpha2=hi-t*n/tf
        if lo==hi:
            return vals[:,[lo]]
        return vals[:,[lo]]*alpha2+vals[:,[hi]]*alpha1
    # xref0=np.array([[0.0, 0.4471, 0.0, -5.0337, 58.0, -1.6497]]).T
    # xreff=np.array([[-15.0, 6.3628, 0.0, 2.8791, -7.0, -0.128]]).T
    xrefl=np.array([[-15.0, 4.0, 0.0, 1.0, 0.0, 0.0]]).T
    print tf
    """  I hacked this function, it now mandates that d height/ dt is less then -1.7 """
    vald,td,xd,ud=discretized_optimal_control_end(A1,B1,Cd,rd,xref0,Ce,xrefl,tf=tf,n=n)
    # generic_solution_plot(td,xd,ud,3,lw=2)
    xd= np.array(xd.value)
    ud= np.array(ud.value)
    """ define my new classy autopilot's reference trajectory """
    def get_x_ref2(t):
        # return xd[:,[disc_convert(t)]]
        return disc_interp(t,xd)
    def get_u_ref2(t):
        # return ud[:,[disc_convert(t)]]
        return disc_interp(t,ud)
    Rd=rd.T.dot(rd)
    Qd=Cd.T.dot(Cd)
    # P,lambda_A_cl,K = control.care(A1,B1,Qd,Rd)
    P,lambda_A_cl,K=test_hand_picked()
    n2=8000
    ts,x,u,xref,uref,dx,du=simulate_state_feedback_with_reference(
        A1,B1,K,xref0,get_x_ref2,get_u_ref2,tf=tf,n=n2)
    plot_ref_actual_comparison(ts, x, u, xref, uref, x_names, u_names, figsize=(12,10))
    plt.savefig(proj_folder+"epic_pilot.pdf")



plt.show()
# \end{listing}