from linearUtils.discretized_optimization import discretized_optimal_control
from linearUtils.discretized_optimization import generic_solution_plot
from linearUtils.discretized_optimization import comparison_solution_plot
import numpy as np
import matplotlib.pyplot as plt
import math
from math import sin, cos
m=1.0
ovm=1.0/m
theta=0.2
b=ovm*np.array([
        [ cos(theta), sin(theta)],
        [-sin(theta), cos(theta)]
    ])
b11=b[0,0]
b12=b[0,1]
b21=b[1,0]
b22=b[1,1]

p1=0.9 *10.0
p2=1.1 *10.0

y1=p1*p2
y2=p1+p2
# s(s+y2)+y1=0
# s^2+y2 s +y1 = 0
# s^2 + (p1+p2)s + (p1*p2)=0

A1=np.array([
        [0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0],
        [0.0,0.0,b11,0.0,0.0,0.0,b12,0.0],
        [0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0],
        [0.0,0.0,-y1,-y2,0.0,0.0,0.0,0.0],
        [0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0],
        [0.0,0.0,b21,0.0,0.0,0.0,b22,0.0],
        [0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0],
        [0.0,0.0,0.0,0.0,0.0,0.0,-y1,-y2]
    ])

A2=np.array([
        [0.0,1.0,0.0,0.0],
        [0.0,0.0,0.0,0.0],
        [0.0,0.0,0.0,1.0],
        [0.0,0.0,0.0,0.0]
    ])

B1=np.array([
        [0.0,0.0],
        [0.0,0.0],
        [0.0,0.0],
        [ y1,0.0],
        [0.0,0.0],
        [0.0,0.0],
        [0.0,0.0],
        [0.0, y1]
    ])
B2=np.array([
        [0.0,0.0],
        [b11,b12],
        [0.0,0.0],
        [b21,b22]
    ])

C1=np.array([
        [1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
        [0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0]
    ])

C2=np.array([
        [1.0,0.0,0.0,0.0],
        [0.0,0.0,1.0,0.0]
    ])

r1=np.array([
        [1.0,0.0],
        [0.0,1.0]
    ])

r2=np.array([
        [1.0,0.0],
        [0.0,1.0]
    ])

x10=np.array([
        [1.0],
        [0.0],
        [0.0],
        [0.0],
        [0.0],
        [0.0],
        [0.0],
        [0.0]
    ])

x20=np.array([
        [1.0],
        [0.0],
        [0.0],
        [0.0]
    ])

n=30
tf=10.0*math.sqrt(m)

val1,t,x1,u1=discretized_optimal_control(A1,B1,C1,r1,x10,tf=tf,n=n)
val2,t,x2,u2=discretized_optimal_control(A2,B2,C2,r2,x20,tf=tf,n=n)


# generic_solution_plot(t,x1,u1,3,lw=2)
comparison_solution_plot(t,x1,x2,u1,u2,3,'b','r', lw=2)
plt.show()
