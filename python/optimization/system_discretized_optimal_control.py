from linearUtils.discretized_optimization import discretized_optimal_control, generic_solution_plot
import numpy as np
import matplotlib.pyplot as plt

A=np.array([
        [0.0,1.0,0.0],
        [0.0,0.0,1.0],
        [0.0,0.0,0.0]
    ])


B=np.array([
        [0.0],
        [0.0],
        [1.0]
    ])

C=np.array([
        [1.0,0.3,0.3]
    ])

r=np.array([
        [1.0]
    ])
n=100
tf=10.0
x0=np.array([
        [1.0],
        [0.0],
        [0.0]
    ])
val,t,x,u=discretized_optimal_control(A,B,C,r,x0,tf=tf,n=n)
generic_solution_plot(t,x,u,lw=2)
plt.show()
