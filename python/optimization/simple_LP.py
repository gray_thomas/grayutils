import numpy as np 
import cvxpy as cvx
A=np.array([
        [1.0,0.0,0.0],
        [0.0,1.0,1.0]
    ])
b=np.array([
        [3.5],
        [0.5]
    ])
c=np.array([
        [1.0],
        [2.0],
        [1.0]
    ])
def func(x):
    """ minimize over positive x: func(x) """
    return c.T.dot(x)

def constraints(x):
    """ s.t. constraints(x)=0 """
    return A.dot(x)-b

def func_x():
    """ gradient of func with respect to x """
    return c.T

def contraints_x():
    """ gradient of constraints with respect to x """
    return A

x=np.array([
        [1.0],
        [1.0],
        [1.0]
    ])
s=np.array([
        [1.0],
        [1.0],
        [1.0]
    ])
l=np.array([
        [1.0],
        [1.0]
    ])

print "x=",x
print "func(x)=",func(x)
print "constraints(x)=",constraints(x)

def getCVXSolution():
    x = cvx.Variable(3,1)
    prob=cvx.Problem(cvx.Minimize(c.T * (x)),[ x>=0 , A * x==b ])
    prob.solve()
    return prob.solve(), x.value
print getCVXSolution()

# constraints(x+dx)=constraints(x)+constraints_x()*dx
# 0=constraints(x)+constraints_x()*dx
# -constraints = constraints_x()*dx
# constraints_x().T.dot(-constraints(x)) = constraints_x().T.dot(constraints_x())*dx