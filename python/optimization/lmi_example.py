import cvxpy as cvx 
import numpy as np 

np.random.seed(21)
n=10 # state size
k=5 # max possible rank of A
A = np.random.rand(n,k).dot(np.random.rand(k,n))
npq = 10
nu = 2 
nw = 4
Bp = np.random.rand(n,npq)
Bu = np.random.rand(n,nu)
Bw = np.random.rand(n,nw)
Cq = np.random.rand(npq, n)
P

print A