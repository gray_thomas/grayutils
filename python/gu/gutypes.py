"""
This modules includes only the mutabledata class for now.

The mutabledata class factory is designed to generate classes
which allow member get and set, for a known list of allowable members.
These members can also be specified in the constructor, i.e.
MyClass = mutabledata("MyClassName",["member1","y","foo"])
obj = MyClass(member1=21, foo=lambda x: x*x)
>>> obj.y
None
>>> obj.member1
21
>>> obj.foo(3)
9
>>> obj.member1=452
>>> obj.member1
452
>>> obj.x = 5
TypeError: Cannot set name 'x' on object of type MyClassName
>>> obj2 = MyClass(x=5)
TypeError: Cannot set name 'x' on object of type MyClassName
"""

def mutabledata(name, fields):
    def new_init(self, **kwargs):
        for field in fields:
            object.__setattr__(self, field, None)
        for key, value in kwargs.iteritems():
            setattr(self, key, value)

    def new_set_attr(self, name, value):
        if hasattr(self, name):
            object.__setattr__(self, name, value)
        else:
            raise TypeError(
                'Cannot set name %r on object of type %s' % (name, self.__class__.__name__))

    def new_repr(self):
    	return self.__class__.__name__+repr(self.__dict__)

    newclass = type(name, (object, ), {"__init__": new_init, "__setattr__":new_set_attr, "__repr__":new_repr})
    return newclass

if __name__ == "__main__":
	MyObject = mutabledata("MyObject", ["x", "y", "z"])
	obj = MyObject(x=5)
	obj.y=5
	obj.q = 8
	print obj

