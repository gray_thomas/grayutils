


<!DOCTYPE html>
<html lang="en" class=" is-copy-enabled">
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta name="viewport" content="width=1020">
    
    
    <title>colormap/option_b.py at master · BIDS/colormap · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub">
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png">
    <meta property="fb:app_id" content="1401488693436528">

      <meta content="@github" name="twitter:site" /><meta content="summary" name="twitter:card" /><meta content="BIDS/colormap" name="twitter:title" /><meta content="colormap - Colormap recommendation" name="twitter:description" /><meta content="https://avatars1.githubusercontent.com/u/8398105?v=3&amp;s=400" name="twitter:image:src" />
      <meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="https://avatars1.githubusercontent.com/u/8398105?v=3&amp;s=400" property="og:image" /><meta content="BIDS/colormap" property="og:title" /><meta content="https://github.com/BIDS/colormap" property="og:url" /><meta content="colormap - Colormap recommendation" property="og:description" />
      <meta name="browser-stats-url" content="https://api.github.com/_private/browser/stats">
    <meta name="browser-errors-url" content="https://api.github.com/_private/browser/errors">
    <link rel="assets" href="https://assets-cdn.github.com/">
    
    <meta name="pjax-timeout" content="1000">
    

    <meta name="msapplication-TileImage" content="/windows-tile.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="selected-link" value="repo_source" data-pjax-transient>

        <meta name="google-analytics" content="UA-3769691-2">

    <meta content="collector.githubapp.com" name="octolytics-host" /><meta content="collector-cdn.github.com" name="octolytics-script-host" /><meta content="github" name="octolytics-app-id" /><meta content="4672C349:15D3:2788D36:55B7AC07" name="octolytics-dimension-request_id" />
    
    <meta content="Rails, view, blob#show" data-pjax-transient="true" name="analytics-event" />
    <meta class="js-ga-set" name="dimension1" content="Logged Out">
      <meta class="js-ga-set" name="dimension4" content="Current repo nav">
    <meta name="is-dotcom" content="true">
        <meta name="hostname" content="github.com">
    <meta name="user-login" content="">

      <link rel="icon" sizes="any" mask href="https://assets-cdn.github.com/pinned-octocat.svg">
      <meta name="theme-color" content="#4078c0">
      <link rel="icon" type="image/x-icon" href="https://assets-cdn.github.com/favicon.ico">

    <!-- </textarea> --><!-- '"` --><meta content="authenticity_token" name="csrf-param" />
<meta content="45ZXISCxIoD+6eAq5fU8de6+rel2xQTka8Bf174qEuEz5GVFe33FcvNeFGgGYsACfPeduMrb2vjSufc+UXLN6A==" name="csrf-token" />
    

    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/github/index-2de9f2a0dcbebdb929c6fddf7fd94aa06a736743201b9c57db4c19cb74757211.css" media="all" rel="stylesheet" />
    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/github2/index-feb5b225655f913840ce7b017a801ec6d6677ccb67b2965548c4965063ac724c.css" media="all" rel="stylesheet" />
    
    


    <meta http-equiv="x-pjax-version" content="e23fc3be5b07883ddbcb7f8f44ee77e2">

      
  <meta name="description" content="colormap - Colormap recommendation">
  <meta name="go-import" content="github.com/BIDS/colormap git https://github.com/BIDS/colormap.git">

  <meta content="8398105" name="octolytics-dimension-user_id" /><meta content="BIDS" name="octolytics-dimension-user_login" /><meta content="36763792" name="octolytics-dimension-repository_id" /><meta content="BIDS/colormap" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="36763792" name="octolytics-dimension-repository_network_root_id" /><meta content="BIDS/colormap" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/BIDS/colormap/commits/master.atom" rel="alternate" title="Recent Commits to colormap:master" type="application/atom+xml">

  </head>


  <body class="logged_out  env-production linux vis-public page-blob">
    <a href="#start-of-content" tabindex="1" class="accessibility-aid js-skip-to-content">Skip to content</a>
    <div class="wrapper">
      
      
      



        
        <div class="header header-logged-out" role="banner">
  <div class="container clearfix">

    <a class="header-logo-wordmark" href="https://github.com/" data-ga-click="(Logged out) Header, go to homepage, icon:logo-wordmark">
      <span class="mega-octicon octicon-logo-github"></span>
    </a>

    <div class="header-actions" role="navigation">
        <a class="btn btn-primary" href="/join" data-ga-click="(Logged out) Header, clicked Sign up, text:sign-up">Sign up</a>
      <a class="btn" href="/login?return_to=%2FBIDS%2Fcolormap%2Fblob%2Fmaster%2Foption_b.py" data-ga-click="(Logged out) Header, clicked Sign in, text:sign-in">Sign in</a>
    </div>

    <div class="site-search repo-scope js-site-search" role="search">
      <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/BIDS/colormap/search" class="js-site-search-form" data-global-search-url="/search" data-repo-search-url="/BIDS/colormap/search" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
  <label class="js-chromeless-input-container form-control">
    <div class="scope-badge">This repository</div>
    <input type="text"
      class="js-site-search-focus js-site-search-field is-clearable chromeless-input"
      data-hotkey="s"
      name="q"
      placeholder="Search"
      data-global-scope-placeholder="Search GitHub"
      data-repo-scope-placeholder="Search"
      tabindex="1"
      autocapitalize="off">
  </label>
</form>
    </div>

      <ul class="header-nav left" role="navigation">
          <li class="header-nav-item">
            <a class="header-nav-link" href="/explore" data-ga-click="(Logged out) Header, go to explore, text:explore">Explore</a>
          </li>
          <li class="header-nav-item">
            <a class="header-nav-link" href="/features" data-ga-click="(Logged out) Header, go to features, text:features">Features</a>
          </li>
          <li class="header-nav-item">
            <a class="header-nav-link" href="https://enterprise.github.com/" data-ga-click="(Logged out) Header, go to enterprise, text:enterprise">Enterprise</a>
          </li>
          <li class="header-nav-item">
            <a class="header-nav-link" href="/blog" data-ga-click="(Logged out) Header, go to blog, text:blog">Blog</a>
          </li>
      </ul>

  </div>
</div>



      <div id="start-of-content" class="accessibility-aid"></div>
          <div class="site" itemscope itemtype="http://schema.org/WebPage">
    <div id="js-flash-container">
      
    </div>
    <div class="pagehead repohead instapaper_ignore readability-menu ">
      <div class="container">

        <div class="clearfix">
          
<ul class="pagehead-actions">

  <li>
      <a href="/login?return_to=%2FBIDS%2Fcolormap"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to watch a repository" rel="nofollow">
    <span class="octicon octicon-eye"></span>
    Watch
  </a>
  <a class="social-count" href="/BIDS/colormap/watchers">
    11
  </a>

  </li>

  <li>
      <a href="/login?return_to=%2FBIDS%2Fcolormap"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to star a repository" rel="nofollow">
    <span class="octicon octicon-star"></span>
    Star
  </a>

    <a class="social-count js-social-count" href="/BIDS/colormap/stargazers">
      14
    </a>

  </li>

    <li>
      <a href="/login?return_to=%2FBIDS%2Fcolormap"
        class="btn btn-sm btn-with-count tooltipped tooltipped-n"
        aria-label="You must be signed in to fork a repository" rel="nofollow">
        <span class="octicon octicon-repo-forked"></span>
        Fork
      </a>
      <a href="/BIDS/colormap/network" class="social-count">
        3
      </a>
    </li>
</ul>

          <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
            <span class="mega-octicon octicon-repo"></span>
            <span class="author"><a href="/BIDS" class="url fn" itemprop="url" rel="author"><span itemprop="title">BIDS</span></a></span><!--
         --><span class="path-divider">/</span><!--
         --><strong><a href="/BIDS/colormap" data-pjax="#js-repo-pjax-container">colormap</a></strong>

            <span class="page-context-loader">
              <img alt="" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
            </span>

          </h1>
        </div>

      </div>
    </div>

      <div class="container">
        <div class="repository-with-sidebar repo-container new-discussion-timeline ">
          <div class="repository-sidebar clearfix">
              

<nav class="sunken-menu repo-nav js-repo-nav js-sidenav-container-pjax js-octicon-loaders"
     role="navigation"
     data-pjax="#js-repo-pjax-container"
     data-issue-count-url="/BIDS/colormap/issues/counts">
  <ul class="sunken-menu-group">
    <li class="tooltipped tooltipped-w" aria-label="Code">
      <a href="/BIDS/colormap" aria-label="Code" aria-selected="true" class="js-selected-navigation-item selected sunken-menu-item" data-hotkey="g c" data-selected-links="repo_source repo_downloads repo_commits repo_releases repo_tags repo_branches /BIDS/colormap">
        <span class="octicon octicon-code"></span> <span class="full-word">Code</span>
        <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>    </li>

      <li class="tooltipped tooltipped-w" aria-label="Issues">
        <a href="/BIDS/colormap/issues" aria-label="Issues" class="js-selected-navigation-item sunken-menu-item" data-hotkey="g i" data-selected-links="repo_issues repo_labels repo_milestones /BIDS/colormap/issues">
          <span class="octicon octicon-issue-opened"></span> <span class="full-word">Issues</span>
          <span class="js-issue-replace-counter"></span>
          <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

    <li class="tooltipped tooltipped-w" aria-label="Pull requests">
      <a href="/BIDS/colormap/pulls" aria-label="Pull requests" class="js-selected-navigation-item sunken-menu-item" data-hotkey="g p" data-selected-links="repo_pulls /BIDS/colormap/pulls">
          <span class="octicon octicon-git-pull-request"></span> <span class="full-word">Pull requests</span>
          <span class="js-pull-replace-counter"></span>
          <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>    </li>

  </ul>
  <div class="sunken-menu-separator"></div>
  <ul class="sunken-menu-group">

    <li class="tooltipped tooltipped-w" aria-label="Pulse">
      <a href="/BIDS/colormap/pulse" aria-label="Pulse" class="js-selected-navigation-item sunken-menu-item" data-selected-links="pulse /BIDS/colormap/pulse">
        <span class="octicon octicon-pulse"></span> <span class="full-word">Pulse</span>
        <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>    </li>

    <li class="tooltipped tooltipped-w" aria-label="Graphs">
      <a href="/BIDS/colormap/graphs" aria-label="Graphs" class="js-selected-navigation-item sunken-menu-item" data-selected-links="repo_graphs repo_contributors /BIDS/colormap/graphs">
        <span class="octicon octicon-graph"></span> <span class="full-word">Graphs</span>
        <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>    </li>
  </ul>


</nav>

                <div class="only-with-full-nav">
                    
<div class="js-clone-url clone-url open"
  data-protocol-type="http">
  <h3><span class="text-emphasized">HTTPS</span> clone URL</h3>
  <div class="input-group js-zeroclipboard-container">
    <input type="text" class="input-mini input-monospace js-url-field js-zeroclipboard-target"
           value="https://github.com/BIDS/colormap.git" readonly="readonly" aria-label="HTTPS clone URL">
    <span class="input-group-button">
      <button aria-label="Copy to clipboard" class="js-zeroclipboard btn btn-sm zeroclipboard-button tooltipped tooltipped-s" data-copied-hint="Copied!" type="button"><span class="octicon octicon-clippy"></span></button>
    </span>
  </div>
</div>

  
<div class="js-clone-url clone-url "
  data-protocol-type="subversion">
  <h3><span class="text-emphasized">Subversion</span> checkout URL</h3>
  <div class="input-group js-zeroclipboard-container">
    <input type="text" class="input-mini input-monospace js-url-field js-zeroclipboard-target"
           value="https://github.com/BIDS/colormap" readonly="readonly" aria-label="Subversion checkout URL">
    <span class="input-group-button">
      <button aria-label="Copy to clipboard" class="js-zeroclipboard btn btn-sm zeroclipboard-button tooltipped tooltipped-s" data-copied-hint="Copied!" type="button"><span class="octicon octicon-clippy"></span></button>
    </span>
  </div>
</div>



  <div class="clone-options">You can clone with
    <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/users/set_protocol?protocol_selector=http&amp;protocol_type=clone" class="inline-form js-clone-selector-form " data-remote="true" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="1iIqB5TZj/9d/JXL2MFH/e62ygNvIoSv0fTCx3AD9eWBLeabDrESnAbQnjxpQ7E1MbHO5D9IOw/qmPF9IftEag==" /></div><button class="btn-link js-clone-selector" data-protocol="http" type="submit">HTTPS</button></form> or <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/users/set_protocol?protocol_selector=subversion&amp;protocol_type=clone" class="inline-form js-clone-selector-form " data-remote="true" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="n/xMbh8bX9SWRPdtMLBMz4LRtF7fFRDi7LwD+G9jpkG7/6qw2U31A4VVYga//2HIwjB164/uHAhwMFDG8PzumQ==" /></div><button class="btn-link js-clone-selector" data-protocol="subversion" type="submit">Subversion</button></form>.
    <a href="https://help.github.com/articles/which-remote-url-should-i-use" class="help tooltipped tooltipped-n" aria-label="Get help on which URL is right for you.">
      <span class="octicon octicon-question"></span>
    </a>
  </div>


                  <a href="/BIDS/colormap/archive/master.zip"
                     class="btn btn-sm sidebar-button"
                     aria-label="Download the contents of BIDS/colormap as a zip file"
                     title="Download the contents of BIDS/colormap as a zip file"
                     rel="nofollow">
                    <span class="octicon octicon-cloud-download"></span>
                    Download ZIP
                  </a>
                </div>
          </div>
          <div id="js-repo-pjax-container" class="repository-content context-loader-container" data-pjax-container>

            

<a href="/BIDS/colormap/blob/bc7d2700f150afb8a6fa8df8c49c9a7f18e84435/option_b.py" class="hidden js-permalink-shortcut" data-hotkey="y">Permalink</a>

<!-- blob contrib key: blob_contributors:v21:fc513bea7203dea130995e1bcd8cc3ec -->

  <div class="file-navigation js-zeroclipboard-container">
    
<div class="select-menu js-menu-container js-select-menu left">
  <span class="btn btn-sm select-menu-button js-menu-target css-truncate" data-hotkey="w"
    data-ref="master"
    title="master"
    role="button" aria-label="Switch branches or tags" tabindex="0" aria-haspopup="true">
    <i>Branch:</i>
    <span class="js-select-button css-truncate-target">master</span>
  </span>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax aria-hidden="true">

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <span class="select-menu-title">Switch branches/tags</span>
        <span class="octicon octicon-x js-menu-close" role="button" aria-label="Close"></span>
      </div>

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" data-filter-placeholder="Filter branches/tags" class="js-select-menu-tab" role="tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" data-filter-placeholder="Find a tag…" class="js-select-menu-tab" role="tab">Tags</a>
            </li>
          </ul>
        </div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches" role="menu">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/BIDS/colormap/blob/gh-pages/option_b.py"
               data-name="gh-pages"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="gh-pages">
                gh-pages
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open selected"
               href="/BIDS/colormap/blob/master/option_b.py"
               data-name="master"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="master">
                master
              </span>
            </a>
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div>

    </div>
  </div>
</div>

    <div class="btn-group right">
      <a href="/BIDS/colormap/find/master"
            class="js-show-file-finder btn btn-sm empty-icon tooltipped tooltipped-nw"
            data-pjax
            data-hotkey="t"
            aria-label="Quickly jump between files">
        <span class="octicon octicon-list-unordered"></span>
      </a>
      <button aria-label="Copy file path to clipboard" class="js-zeroclipboard btn btn-sm zeroclipboard-button tooltipped tooltipped-s" data-copied-hint="Copied!" type="button"><span class="octicon octicon-clippy"></span></button>
    </div>

    <div class="breadcrumb js-zeroclipboard-target">
      <span class="repo-root js-repo-root"><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/BIDS/colormap" class="" data-branch="master" data-pjax="true" itemscope="url"><span itemprop="title">colormap</span></a></span></span><span class="separator">/</span><strong class="final-path">option_b.py</strong>
    </div>
  </div>


  <div class="commit file-history-tease">
    <div class="file-history-tease-header">
        <img alt="@stefanv" class="avatar" height="24" src="https://avatars1.githubusercontent.com/u/45071?v=3&amp;s=48" width="24" />
        <span class="author"><a href="/stefanv" rel="contributor">stefanv</a></span>
        <time datetime="2015-06-03T01:02:09Z" is="relative-time">Jun 2, 2015</time>
        <div class="commit-title">
            <a href="/BIDS/colormap/commit/2f621839e177bc3de4806443ee7809daed7a5cc5" class="message" data-pjax="true" title="Add new versions of scripts to correspond to labeling on website">Add new versions of scripts to correspond to labeling on website</a>
        </div>
    </div>

    <div class="participation">
      <p class="quickstat">
        <a href="#blob_contributors_box" rel="facebox">
          <strong>1</strong>
           contributor
        </a>
      </p>
      
    </div>
    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list">
          <li class="facebox-user-list-item">
            <img alt="@stefanv" height="24" src="https://avatars1.githubusercontent.com/u/45071?v=3&amp;s=48" width="24" />
            <a href="/stefanv">stefanv</a>
          </li>
      </ul>
    </div>
  </div>

<div class="file">
  <div class="file-header">
    <div class="file-actions">

      <div class="btn-group">
        <a href="/BIDS/colormap/raw/master/option_b.py" class="btn btn-sm " id="raw-url">Raw</a>
          <a href="/BIDS/colormap/blame/master/option_b.py" class="btn btn-sm js-update-url-with-hash">Blame</a>
        <a href="/BIDS/colormap/commits/master/option_b.py" class="btn btn-sm " rel="nofollow">History</a>
      </div>


          <button type="button" class="octicon-btn disabled tooltipped tooltipped-n" aria-label="You must be signed in to make or propose changes">
            <span class="octicon octicon-pencil"></span>
          </button>

        <button type="button" class="octicon-btn octicon-btn-danger disabled tooltipped tooltipped-n" aria-label="You must be signed in to make or propose changes">
          <span class="octicon octicon-trashcan"></span>
        </button>
    </div>

    <div class="file-info">
        283 lines (275 sloc)
        <span class="file-info-divider"></span>
      17.049 kB
    </div>
  </div>
  

  <div class="blob-wrapper data type-python">
      <table class="highlight tab-size js-file-line-container" data-tab-size="8">
      <tr>
        <td id="L1" class="blob-num js-line-number" data-line-number="1"></td>
        <td id="LC1" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L2" class="blob-num js-line-number" data-line-number="2"></td>
        <td id="LC2" class="blob-code blob-code-inner js-file-line"><span class="pl-k">from</span> matplotlib.colors <span class="pl-k">import</span> LinearSegmentedColormap</td>
      </tr>
      <tr>
        <td id="L3" class="blob-num js-line-number" data-line-number="3"></td>
        <td id="LC3" class="blob-code blob-code-inner js-file-line"><span class="pl-k">from</span> numpy <span class="pl-k">import</span> nan, inf</td>
      </tr>
      <tr>
        <td id="L4" class="blob-num js-line-number" data-line-number="4"></td>
        <td id="LC4" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L5" class="blob-num js-line-number" data-line-number="5"></td>
        <td id="LC5" class="blob-code blob-code-inner js-file-line"><span class="pl-c"># Used to reconstruct the colormap in pycam02ucs.cm.viscm</span></td>
      </tr>
      <tr>
        <td id="L6" class="blob-num js-line-number" data-line-number="6"></td>
        <td id="LC6" class="blob-code blob-code-inner js-file-line">parameters <span class="pl-k">=</span> {<span class="pl-s"><span class="pl-pds">&#39;</span>xp<span class="pl-pds">&#39;</span></span>: [<span class="pl-c1">0.62912107182941668</span>, <span class="pl-c1">5.9459613496072166</span>, <span class="pl-c1">109.50795214009284</span>, <span class="pl-k">-</span><span class="pl-c1">21.828857638709152</span>, <span class="pl-k">-</span><span class="pl-c1">6.5743399496759416</span>],</td>
      </tr>
      <tr>
        <td id="L7" class="blob-num js-line-number" data-line-number="7"></td>
        <td id="LC7" class="blob-code blob-code-inner js-file-line">              <span class="pl-s"><span class="pl-pds">&#39;</span>yp<span class="pl-pds">&#39;</span></span>: [<span class="pl-k">-</span><span class="pl-c1">9.0711805555555429</span>, <span class="pl-k">-</span><span class="pl-c1">75.1519097222222</span>, <span class="pl-c1">50.040035674319967</span>, <span class="pl-c1">52.94104954493605</span>, <span class="pl-c1">24.552723501963708</span>],</td>
      </tr>
      <tr>
        <td id="L8" class="blob-num js-line-number" data-line-number="8"></td>
        <td id="LC8" class="blob-code blob-code-inner js-file-line">              <span class="pl-s"><span class="pl-pds">&#39;</span>min_JK<span class="pl-pds">&#39;</span></span>: <span class="pl-c1">1.1436170212765937</span>,</td>
      </tr>
      <tr>
        <td id="L9" class="blob-num js-line-number" data-line-number="9"></td>
        <td id="LC9" class="blob-code blob-code-inner js-file-line">              <span class="pl-s"><span class="pl-pds">&#39;</span>max_JK<span class="pl-pds">&#39;</span></span>: <span class="pl-c1">98.21808510638297</span>}</td>
      </tr>
      <tr>
        <td id="L10" class="blob-num js-line-number" data-line-number="10"></td>
        <td id="LC10" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L11" class="blob-num js-line-number" data-line-number="11"></td>
        <td id="LC11" class="blob-code blob-code-inner js-file-line">cm_data <span class="pl-k">=</span> [[  <span class="pl-c1">1.46159096e-03</span>,   <span class="pl-c1">4.66127766e-04</span>,   <span class="pl-c1">1.38655200e-02</span>],</td>
      </tr>
      <tr>
        <td id="L12" class="blob-num js-line-number" data-line-number="12"></td>
        <td id="LC12" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.26726368e-03</span>,   <span class="pl-c1">1.26992553e-03</span>,   <span class="pl-c1">1.85703520e-02</span>],</td>
      </tr>
      <tr>
        <td id="L13" class="blob-num js-line-number" data-line-number="13"></td>
        <td id="LC13" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.29899092e-03</span>,   <span class="pl-c1">2.24934863e-03</span>,   <span class="pl-c1">2.42390508e-02</span>],</td>
      </tr>
      <tr>
        <td id="L14" class="blob-num js-line-number" data-line-number="14"></td>
        <td id="LC14" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.54690615e-03</span>,   <span class="pl-c1">3.39180156e-03</span>,   <span class="pl-c1">3.09092475e-02</span>],</td>
      </tr>
      <tr>
        <td id="L15" class="blob-num js-line-number" data-line-number="15"></td>
        <td id="LC15" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.00552565e-03</span>,   <span class="pl-c1">4.69194561e-03</span>,   <span class="pl-c1">3.85578980e-02</span>],</td>
      </tr>
      <tr>
        <td id="L16" class="blob-num js-line-number" data-line-number="16"></td>
        <td id="LC16" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.67578856e-03</span>,   <span class="pl-c1">6.13611626e-03</span>,   <span class="pl-c1">4.68360336e-02</span>],</td>
      </tr>
      <tr>
        <td id="L17" class="blob-num js-line-number" data-line-number="17"></td>
        <td id="LC17" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.56051094e-03</span>,   <span class="pl-c1">7.71344131e-03</span>,   <span class="pl-c1">5.51430756e-02</span>],</td>
      </tr>
      <tr>
        <td id="L18" class="blob-num js-line-number" data-line-number="18"></td>
        <td id="LC18" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.16634769e-02</span>,   <span class="pl-c1">9.41675403e-03</span>,   <span class="pl-c1">6.34598080e-02</span>],</td>
      </tr>
      <tr>
        <td id="L19" class="blob-num js-line-number" data-line-number="19"></td>
        <td id="LC19" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.39950388e-02</span>,   <span class="pl-c1">1.12247138e-02</span>,   <span class="pl-c1">7.18616890e-02</span>],</td>
      </tr>
      <tr>
        <td id="L20" class="blob-num js-line-number" data-line-number="20"></td>
        <td id="LC20" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.65605595e-02</span>,   <span class="pl-c1">1.31362262e-02</span>,   <span class="pl-c1">8.02817951e-02</span>],</td>
      </tr>
      <tr>
        <td id="L21" class="blob-num js-line-number" data-line-number="21"></td>
        <td id="LC21" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.93732295e-02</span>,   <span class="pl-c1">1.51325789e-02</span>,   <span class="pl-c1">8.87668094e-02</span>],</td>
      </tr>
      <tr>
        <td id="L22" class="blob-num js-line-number" data-line-number="22"></td>
        <td id="LC22" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.24468865e-02</span>,   <span class="pl-c1">1.71991484e-02</span>,   <span class="pl-c1">9.73274383e-02</span>],</td>
      </tr>
      <tr>
        <td id="L23" class="blob-num js-line-number" data-line-number="23"></td>
        <td id="LC23" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.57927373e-02</span>,   <span class="pl-c1">1.93306298e-02</span>,   <span class="pl-c1">1.05929835e-01</span>],</td>
      </tr>
      <tr>
        <td id="L24" class="blob-num js-line-number" data-line-number="24"></td>
        <td id="LC24" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.94324251e-02</span>,   <span class="pl-c1">2.15030771e-02</span>,   <span class="pl-c1">1.14621328e-01</span>],</td>
      </tr>
      <tr>
        <td id="L25" class="blob-num js-line-number" data-line-number="25"></td>
        <td id="LC25" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.33852235e-02</span>,   <span class="pl-c1">2.37024271e-02</span>,   <span class="pl-c1">1.23397286e-01</span>],</td>
      </tr>
      <tr>
        <td id="L26" class="blob-num js-line-number" data-line-number="26"></td>
        <td id="LC26" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.76684211e-02</span>,   <span class="pl-c1">2.59207864e-02</span>,   <span class="pl-c1">1.32232108e-01</span>],</td>
      </tr>
      <tr>
        <td id="L27" class="blob-num js-line-number" data-line-number="27"></td>
        <td id="LC27" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.22525554e-02</span>,   <span class="pl-c1">2.81385015e-02</span>,   <span class="pl-c1">1.41140519e-01</span>],</td>
      </tr>
      <tr>
        <td id="L28" class="blob-num js-line-number" data-line-number="28"></td>
        <td id="LC28" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.69146287e-02</span>,   <span class="pl-c1">3.03236129e-02</span>,   <span class="pl-c1">1.50163867e-01</span>],</td>
      </tr>
      <tr>
        <td id="L29" class="blob-num js-line-number" data-line-number="29"></td>
        <td id="LC29" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.16437624e-02</span>,   <span class="pl-c1">3.24736172e-02</span>,   <span class="pl-c1">1.59254277e-01</span>],</td>
      </tr>
      <tr>
        <td id="L30" class="blob-num js-line-number" data-line-number="30"></td>
        <td id="LC30" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.64491009e-02</span>,   <span class="pl-c1">3.45691867e-02</span>,   <span class="pl-c1">1.68413539e-01</span>],</td>
      </tr>
      <tr>
        <td id="L31" class="blob-num js-line-number" data-line-number="31"></td>
        <td id="LC31" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.13397200e-02</span>,   <span class="pl-c1">3.65900213e-02</span>,   <span class="pl-c1">1.77642172e-01</span>],</td>
      </tr>
      <tr>
        <td id="L32" class="blob-num js-line-number" data-line-number="32"></td>
        <td id="LC32" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.63312620e-02</span>,   <span class="pl-c1">3.85036268e-02</span>,   <span class="pl-c1">1.86961588e-01</span>],</td>
      </tr>
      <tr>
        <td id="L33" class="blob-num js-line-number" data-line-number="33"></td>
        <td id="LC33" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.14289181e-02</span>,   <span class="pl-c1">4.02939095e-02</span>,   <span class="pl-c1">1.96353558e-01</span>],</td>
      </tr>
      <tr>
        <td id="L34" class="blob-num js-line-number" data-line-number="34"></td>
        <td id="LC34" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.66367560e-02</span>,   <span class="pl-c1">4.19053329e-02</span>,   <span class="pl-c1">2.05798788e-01</span>],</td>
      </tr>
      <tr>
        <td id="L35" class="blob-num js-line-number" data-line-number="35"></td>
        <td id="LC35" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.19620773e-02</span>,   <span class="pl-c1">4.33278666e-02</span>,   <span class="pl-c1">2.15289113e-01</span>],</td>
      </tr>
      <tr>
        <td id="L36" class="blob-num js-line-number" data-line-number="36"></td>
        <td id="LC36" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.74113897e-02</span>,   <span class="pl-c1">4.45561662e-02</span>,   <span class="pl-c1">2.24813479e-01</span>],</td>
      </tr>
      <tr>
        <td id="L37" class="blob-num js-line-number" data-line-number="37"></td>
        <td id="LC37" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.29901526e-02</span>,   <span class="pl-c1">4.55829503e-02</span>,   <span class="pl-c1">2.34357604e-01</span>],</td>
      </tr>
      <tr>
        <td id="L38" class="blob-num js-line-number" data-line-number="38"></td>
        <td id="LC38" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.87024972e-02</span>,   <span class="pl-c1">4.64018731e-02</span>,   <span class="pl-c1">2.43903700e-01</span>],</td>
      </tr>
      <tr>
        <td id="L39" class="blob-num js-line-number" data-line-number="39"></td>
        <td id="LC39" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.04550936e-01</span>,   <span class="pl-c1">4.70080541e-02</span>,   <span class="pl-c1">2.53430300e-01</span>],</td>
      </tr>
      <tr>
        <td id="L40" class="blob-num js-line-number" data-line-number="40"></td>
        <td id="LC40" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.10536084e-01</span>,   <span class="pl-c1">4.73986708e-02</span>,   <span class="pl-c1">2.62912235e-01</span>],</td>
      </tr>
      <tr>
        <td id="L41" class="blob-num js-line-number" data-line-number="41"></td>
        <td id="LC41" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.16656423e-01</span>,   <span class="pl-c1">4.75735920e-02</span>,   <span class="pl-c1">2.72320803e-01</span>],</td>
      </tr>
      <tr>
        <td id="L42" class="blob-num js-line-number" data-line-number="42"></td>
        <td id="LC42" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.22908126e-01</span>,   <span class="pl-c1">4.75360183e-02</span>,   <span class="pl-c1">2.81624170e-01</span>],</td>
      </tr>
      <tr>
        <td id="L43" class="blob-num js-line-number" data-line-number="43"></td>
        <td id="LC43" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.29284984e-01</span>,   <span class="pl-c1">4.72930838e-02</span>,   <span class="pl-c1">2.90788012e-01</span>],</td>
      </tr>
      <tr>
        <td id="L44" class="blob-num js-line-number" data-line-number="44"></td>
        <td id="LC44" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.35778450e-01</span>,   <span class="pl-c1">4.68563678e-02</span>,   <span class="pl-c1">2.99776404e-01</span>],</td>
      </tr>
      <tr>
        <td id="L45" class="blob-num js-line-number" data-line-number="45"></td>
        <td id="LC45" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.42377819e-01</span>,   <span class="pl-c1">4.62422566e-02</span>,   <span class="pl-c1">3.08552910e-01</span>],</td>
      </tr>
      <tr>
        <td id="L46" class="blob-num js-line-number" data-line-number="46"></td>
        <td id="LC46" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.49072957e-01</span>,   <span class="pl-c1">4.54676444e-02</span>,   <span class="pl-c1">3.17085139e-01</span>],</td>
      </tr>
      <tr>
        <td id="L47" class="blob-num js-line-number" data-line-number="47"></td>
        <td id="LC47" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.55849711e-01</span>,   <span class="pl-c1">4.45588056e-02</span>,   <span class="pl-c1">3.25338414e-01</span>],</td>
      </tr>
      <tr>
        <td id="L48" class="blob-num js-line-number" data-line-number="48"></td>
        <td id="LC48" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.62688939e-01</span>,   <span class="pl-c1">4.35542881e-02</span>,   <span class="pl-c1">3.33276678e-01</span>],</td>
      </tr>
      <tr>
        <td id="L49" class="blob-num js-line-number" data-line-number="49"></td>
        <td id="LC49" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.69575148e-01</span>,   <span class="pl-c1">4.24893149e-02</span>,   <span class="pl-c1">3.40874188e-01</span>],</td>
      </tr>
      <tr>
        <td id="L50" class="blob-num js-line-number" data-line-number="50"></td>
        <td id="LC50" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.76493202e-01</span>,   <span class="pl-c1">4.14017089e-02</span>,   <span class="pl-c1">3.48110606e-01</span>],</td>
      </tr>
      <tr>
        <td id="L51" class="blob-num js-line-number" data-line-number="51"></td>
        <td id="LC51" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.83428775e-01</span>,   <span class="pl-c1">4.03288858e-02</span>,   <span class="pl-c1">3.54971391e-01</span>],</td>
      </tr>
      <tr>
        <td id="L52" class="blob-num js-line-number" data-line-number="52"></td>
        <td id="LC52" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.90367453e-01</span>,   <span class="pl-c1">3.93088888e-02</span>,   <span class="pl-c1">3.61446945e-01</span>],</td>
      </tr>
      <tr>
        <td id="L53" class="blob-num js-line-number" data-line-number="53"></td>
        <td id="LC53" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">1.97297425e-01</span>,   <span class="pl-c1">3.84001825e-02</span>,   <span class="pl-c1">3.67534629e-01</span>],</td>
      </tr>
      <tr>
        <td id="L54" class="blob-num js-line-number" data-line-number="54"></td>
        <td id="LC54" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.04209298e-01</span>,   <span class="pl-c1">3.76322609e-02</span>,   <span class="pl-c1">3.73237557e-01</span>],</td>
      </tr>
      <tr>
        <td id="L55" class="blob-num js-line-number" data-line-number="55"></td>
        <td id="LC55" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.11095463e-01</span>,   <span class="pl-c1">3.70296488e-02</span>,   <span class="pl-c1">3.78563264e-01</span>],</td>
      </tr>
      <tr>
        <td id="L56" class="blob-num js-line-number" data-line-number="56"></td>
        <td id="LC56" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.17948648e-01</span>,   <span class="pl-c1">3.66146049e-02</span>,   <span class="pl-c1">3.83522415e-01</span>],</td>
      </tr>
      <tr>
        <td id="L57" class="blob-num js-line-number" data-line-number="57"></td>
        <td id="LC57" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.24762908e-01</span>,   <span class="pl-c1">3.64049901e-02</span>,   <span class="pl-c1">3.88128944e-01</span>],</td>
      </tr>
      <tr>
        <td id="L58" class="blob-num js-line-number" data-line-number="58"></td>
        <td id="LC58" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.31538148e-01</span>,   <span class="pl-c1">3.64052511e-02</span>,   <span class="pl-c1">3.92400150e-01</span>],</td>
      </tr>
      <tr>
        <td id="L59" class="blob-num js-line-number" data-line-number="59"></td>
        <td id="LC59" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.38272961e-01</span>,   <span class="pl-c1">3.66209949e-02</span>,   <span class="pl-c1">3.96353388e-01</span>],</td>
      </tr>
      <tr>
        <td id="L60" class="blob-num js-line-number" data-line-number="60"></td>
        <td id="LC60" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.44966911e-01</span>,   <span class="pl-c1">3.70545017e-02</span>,   <span class="pl-c1">4.00006615e-01</span>],</td>
      </tr>
      <tr>
        <td id="L61" class="blob-num js-line-number" data-line-number="61"></td>
        <td id="LC61" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.51620354e-01</span>,   <span class="pl-c1">3.77052832e-02</span>,   <span class="pl-c1">4.03377897e-01</span>],</td>
      </tr>
      <tr>
        <td id="L62" class="blob-num js-line-number" data-line-number="62"></td>
        <td id="LC62" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.58234265e-01</span>,   <span class="pl-c1">3.85706153e-02</span>,   <span class="pl-c1">4.06485031e-01</span>],</td>
      </tr>
      <tr>
        <td id="L63" class="blob-num js-line-number" data-line-number="63"></td>
        <td id="LC63" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.64809649e-01</span>,   <span class="pl-c1">3.96468666e-02</span>,   <span class="pl-c1">4.09345373e-01</span>],</td>
      </tr>
      <tr>
        <td id="L64" class="blob-num js-line-number" data-line-number="64"></td>
        <td id="LC64" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.71346664e-01</span>,   <span class="pl-c1">4.09215821e-02</span>,   <span class="pl-c1">4.11976086e-01</span>],</td>
      </tr>
      <tr>
        <td id="L65" class="blob-num js-line-number" data-line-number="65"></td>
        <td id="LC65" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.77849829e-01</span>,   <span class="pl-c1">4.23528741e-02</span>,   <span class="pl-c1">4.14392106e-01</span>],</td>
      </tr>
      <tr>
        <td id="L66" class="blob-num js-line-number" data-line-number="66"></td>
        <td id="LC66" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.84321318e-01</span>,   <span class="pl-c1">4.39325787e-02</span>,   <span class="pl-c1">4.16607861e-01</span>],</td>
      </tr>
      <tr>
        <td id="L67" class="blob-num js-line-number" data-line-number="67"></td>
        <td id="LC67" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.90763373e-01</span>,   <span class="pl-c1">4.56437598e-02</span>,   <span class="pl-c1">4.18636756e-01</span>],</td>
      </tr>
      <tr>
        <td id="L68" class="blob-num js-line-number" data-line-number="68"></td>
        <td id="LC68" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">2.97178251e-01</span>,   <span class="pl-c1">4.74700293e-02</span>,   <span class="pl-c1">4.20491164e-01</span>],</td>
      </tr>
      <tr>
        <td id="L69" class="blob-num js-line-number" data-line-number="69"></td>
        <td id="LC69" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.03568182e-01</span>,   <span class="pl-c1">4.93958927e-02</span>,   <span class="pl-c1">4.22182449e-01</span>],</td>
      </tr>
      <tr>
        <td id="L70" class="blob-num js-line-number" data-line-number="70"></td>
        <td id="LC70" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.09935342e-01</span>,   <span class="pl-c1">5.14069729e-02</span>,   <span class="pl-c1">4.23720999e-01</span>],</td>
      </tr>
      <tr>
        <td id="L71" class="blob-num js-line-number" data-line-number="71"></td>
        <td id="LC71" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.16281835e-01</span>,   <span class="pl-c1">5.34901321e-02</span>,   <span class="pl-c1">4.25116277e-01</span>],</td>
      </tr>
      <tr>
        <td id="L72" class="blob-num js-line-number" data-line-number="72"></td>
        <td id="LC72" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.22609671e-01</span>,   <span class="pl-c1">5.56335178e-02</span>,   <span class="pl-c1">4.26376869e-01</span>],</td>
      </tr>
      <tr>
        <td id="L73" class="blob-num js-line-number" data-line-number="73"></td>
        <td id="LC73" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.28920763e-01</span>,   <span class="pl-c1">5.78265505e-02</span>,   <span class="pl-c1">4.27510546e-01</span>],</td>
      </tr>
      <tr>
        <td id="L74" class="blob-num js-line-number" data-line-number="74"></td>
        <td id="LC74" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.35216916e-01</span>,   <span class="pl-c1">6.00598734e-02</span>,   <span class="pl-c1">4.28524320e-01</span>],</td>
      </tr>
      <tr>
        <td id="L75" class="blob-num js-line-number" data-line-number="75"></td>
        <td id="LC75" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.41499828e-01</span>,   <span class="pl-c1">6.23252772e-02</span>,   <span class="pl-c1">4.29424503e-01</span>],</td>
      </tr>
      <tr>
        <td id="L76" class="blob-num js-line-number" data-line-number="76"></td>
        <td id="LC76" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.47771086e-01</span>,   <span class="pl-c1">6.46156100e-02</span>,   <span class="pl-c1">4.30216765e-01</span>],</td>
      </tr>
      <tr>
        <td id="L77" class="blob-num js-line-number" data-line-number="77"></td>
        <td id="LC77" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.54032169e-01</span>,   <span class="pl-c1">6.69246832e-02</span>,   <span class="pl-c1">4.30906186e-01</span>],</td>
      </tr>
      <tr>
        <td id="L78" class="blob-num js-line-number" data-line-number="78"></td>
        <td id="LC78" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.60284449e-01</span>,   <span class="pl-c1">6.92471753e-02</span>,   <span class="pl-c1">4.31497309e-01</span>],</td>
      </tr>
      <tr>
        <td id="L79" class="blob-num js-line-number" data-line-number="79"></td>
        <td id="LC79" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.66529195e-01</span>,   <span class="pl-c1">7.15785403e-02</span>,   <span class="pl-c1">4.31994185e-01</span>],</td>
      </tr>
      <tr>
        <td id="L80" class="blob-num js-line-number" data-line-number="80"></td>
        <td id="LC80" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.72767575e-01</span>,   <span class="pl-c1">7.39149211e-02</span>,   <span class="pl-c1">4.32400419e-01</span>],</td>
      </tr>
      <tr>
        <td id="L81" class="blob-num js-line-number" data-line-number="81"></td>
        <td id="LC81" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.79000659e-01</span>,   <span class="pl-c1">7.62530701e-02</span>,   <span class="pl-c1">4.32719214e-01</span>],</td>
      </tr>
      <tr>
        <td id="L82" class="blob-num js-line-number" data-line-number="82"></td>
        <td id="LC82" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.85228383e-01</span>,   <span class="pl-c1">7.85914864e-02</span>,   <span class="pl-c1">4.32954973e-01</span>],</td>
      </tr>
      <tr>
        <td id="L83" class="blob-num js-line-number" data-line-number="83"></td>
        <td id="LC83" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.91452659e-01</span>,   <span class="pl-c1">8.09267058e-02</span>,   <span class="pl-c1">4.33108763e-01</span>],</td>
      </tr>
      <tr>
        <td id="L84" class="blob-num js-line-number" data-line-number="84"></td>
        <td id="LC84" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">3.97674379e-01</span>,   <span class="pl-c1">8.32568129e-02</span>,   <span class="pl-c1">4.33182647e-01</span>],</td>
      </tr>
      <tr>
        <td id="L85" class="blob-num js-line-number" data-line-number="85"></td>
        <td id="LC85" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.03894278e-01</span>,   <span class="pl-c1">8.55803445e-02</span>,   <span class="pl-c1">4.33178526e-01</span>],</td>
      </tr>
      <tr>
        <td id="L86" class="blob-num js-line-number" data-line-number="86"></td>
        <td id="LC86" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.10113015e-01</span>,   <span class="pl-c1">8.78961593e-02</span>,   <span class="pl-c1">4.33098056e-01</span>],</td>
      </tr>
      <tr>
        <td id="L87" class="blob-num js-line-number" data-line-number="87"></td>
        <td id="LC87" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.16331169e-01</span>,   <span class="pl-c1">9.02033992e-02</span>,   <span class="pl-c1">4.32942678e-01</span>],</td>
      </tr>
      <tr>
        <td id="L88" class="blob-num js-line-number" data-line-number="88"></td>
        <td id="LC88" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.22549249e-01</span>,   <span class="pl-c1">9.25014543e-02</span>,   <span class="pl-c1">4.32713635e-01</span>],</td>
      </tr>
      <tr>
        <td id="L89" class="blob-num js-line-number" data-line-number="89"></td>
        <td id="LC89" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.28767696e-01</span>,   <span class="pl-c1">9.47899342e-02</span>,   <span class="pl-c1">4.32411996e-01</span>],</td>
      </tr>
      <tr>
        <td id="L90" class="blob-num js-line-number" data-line-number="90"></td>
        <td id="LC90" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.34986885e-01</span>,   <span class="pl-c1">9.70686417e-02</span>,   <span class="pl-c1">4.32038673e-01</span>],</td>
      </tr>
      <tr>
        <td id="L91" class="blob-num js-line-number" data-line-number="91"></td>
        <td id="LC91" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.41207124e-01</span>,   <span class="pl-c1">9.93375510e-02</span>,   <span class="pl-c1">4.31594438e-01</span>],</td>
      </tr>
      <tr>
        <td id="L92" class="blob-num js-line-number" data-line-number="92"></td>
        <td id="LC92" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.47428382e-01</span>,   <span class="pl-c1">1.01597079e-01</span>,   <span class="pl-c1">4.31080497e-01</span>],</td>
      </tr>
      <tr>
        <td id="L93" class="blob-num js-line-number" data-line-number="93"></td>
        <td id="LC93" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.53650614e-01</span>,   <span class="pl-c1">1.03847716e-01</span>,   <span class="pl-c1">4.30497898e-01</span>],</td>
      </tr>
      <tr>
        <td id="L94" class="blob-num js-line-number" data-line-number="94"></td>
        <td id="LC94" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.59874623e-01</span>,   <span class="pl-c1">1.06089165e-01</span>,   <span class="pl-c1">4.29845789e-01</span>],</td>
      </tr>
      <tr>
        <td id="L95" class="blob-num js-line-number" data-line-number="95"></td>
        <td id="LC95" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.66100494e-01</span>,   <span class="pl-c1">1.08321923e-01</span>,   <span class="pl-c1">4.29124507e-01</span>],</td>
      </tr>
      <tr>
        <td id="L96" class="blob-num js-line-number" data-line-number="96"></td>
        <td id="LC96" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.72328255e-01</span>,   <span class="pl-c1">1.10546584e-01</span>,   <span class="pl-c1">4.28334320e-01</span>],</td>
      </tr>
      <tr>
        <td id="L97" class="blob-num js-line-number" data-line-number="97"></td>
        <td id="LC97" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.78557889e-01</span>,   <span class="pl-c1">1.12763831e-01</span>,   <span class="pl-c1">4.27475431e-01</span>],</td>
      </tr>
      <tr>
        <td id="L98" class="blob-num js-line-number" data-line-number="98"></td>
        <td id="LC98" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.84789325e-01</span>,   <span class="pl-c1">1.14974430e-01</span>,   <span class="pl-c1">4.26547991e-01</span>],</td>
      </tr>
      <tr>
        <td id="L99" class="blob-num js-line-number" data-line-number="99"></td>
        <td id="LC99" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.91022448e-01</span>,   <span class="pl-c1">1.17179219e-01</span>,   <span class="pl-c1">4.25552106e-01</span>],</td>
      </tr>
      <tr>
        <td id="L100" class="blob-num js-line-number" data-line-number="100"></td>
        <td id="LC100" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">4.97257069e-01</span>,   <span class="pl-c1">1.19379132e-01</span>,   <span class="pl-c1">4.24487908e-01</span>],</td>
      </tr>
      <tr>
        <td id="L101" class="blob-num js-line-number" data-line-number="101"></td>
        <td id="LC101" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.03492698e-01</span>,   <span class="pl-c1">1.21575414e-01</span>,   <span class="pl-c1">4.23356110e-01</span>],</td>
      </tr>
      <tr>
        <td id="L102" class="blob-num js-line-number" data-line-number="102"></td>
        <td id="LC102" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.09729541e-01</span>,   <span class="pl-c1">1.23768654e-01</span>,   <span class="pl-c1">4.22155676e-01</span>],</td>
      </tr>
      <tr>
        <td id="L103" class="blob-num js-line-number" data-line-number="103"></td>
        <td id="LC103" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.15967304e-01</span>,   <span class="pl-c1">1.25959947e-01</span>,   <span class="pl-c1">4.20886594e-01</span>],</td>
      </tr>
      <tr>
        <td id="L104" class="blob-num js-line-number" data-line-number="104"></td>
        <td id="LC104" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.22205646e-01</span>,   <span class="pl-c1">1.28150439e-01</span>,   <span class="pl-c1">4.19548848e-01</span>],</td>
      </tr>
      <tr>
        <td id="L105" class="blob-num js-line-number" data-line-number="105"></td>
        <td id="LC105" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.28444192e-01</span>,   <span class="pl-c1">1.30341324e-01</span>,   <span class="pl-c1">4.18142411e-01</span>],</td>
      </tr>
      <tr>
        <td id="L106" class="blob-num js-line-number" data-line-number="106"></td>
        <td id="LC106" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.34682523e-01</span>,   <span class="pl-c1">1.32533845e-01</span>,   <span class="pl-c1">4.16667258e-01</span>],</td>
      </tr>
      <tr>
        <td id="L107" class="blob-num js-line-number" data-line-number="107"></td>
        <td id="LC107" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.40920186e-01</span>,   <span class="pl-c1">1.34729286e-01</span>,   <span class="pl-c1">4.15123366e-01</span>],</td>
      </tr>
      <tr>
        <td id="L108" class="blob-num js-line-number" data-line-number="108"></td>
        <td id="LC108" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.47156706e-01</span>,   <span class="pl-c1">1.36928959e-01</span>,   <span class="pl-c1">4.13510662e-01</span>],</td>
      </tr>
      <tr>
        <td id="L109" class="blob-num js-line-number" data-line-number="109"></td>
        <td id="LC109" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.53391649e-01</span>,   <span class="pl-c1">1.39134147e-01</span>,   <span class="pl-c1">4.11828882e-01</span>],</td>
      </tr>
      <tr>
        <td id="L110" class="blob-num js-line-number" data-line-number="110"></td>
        <td id="LC110" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.59624442e-01</span>,   <span class="pl-c1">1.41346265e-01</span>,   <span class="pl-c1">4.10078028e-01</span>],</td>
      </tr>
      <tr>
        <td id="L111" class="blob-num js-line-number" data-line-number="111"></td>
        <td id="LC111" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.65854477e-01</span>,   <span class="pl-c1">1.43566769e-01</span>,   <span class="pl-c1">4.08258132e-01</span>],</td>
      </tr>
      <tr>
        <td id="L112" class="blob-num js-line-number" data-line-number="112"></td>
        <td id="LC112" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.72081108e-01</span>,   <span class="pl-c1">1.45797150e-01</span>,   <span class="pl-c1">4.06369246e-01</span>],</td>
      </tr>
      <tr>
        <td id="L113" class="blob-num js-line-number" data-line-number="113"></td>
        <td id="LC113" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.78303656e-01</span>,   <span class="pl-c1">1.48038934e-01</span>,   <span class="pl-c1">4.04411444e-01</span>],</td>
      </tr>
      <tr>
        <td id="L114" class="blob-num js-line-number" data-line-number="114"></td>
        <td id="LC114" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.84521407e-01</span>,   <span class="pl-c1">1.50293679e-01</span>,   <span class="pl-c1">4.02384829e-01</span>],</td>
      </tr>
      <tr>
        <td id="L115" class="blob-num js-line-number" data-line-number="115"></td>
        <td id="LC115" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.90733615e-01</span>,   <span class="pl-c1">1.52562977e-01</span>,   <span class="pl-c1">4.00289528e-01</span>],</td>
      </tr>
      <tr>
        <td id="L116" class="blob-num js-line-number" data-line-number="116"></td>
        <td id="LC116" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">5.96939751e-01</span>,   <span class="pl-c1">1.54848232e-01</span>,   <span class="pl-c1">3.98124897e-01</span>],</td>
      </tr>
      <tr>
        <td id="L117" class="blob-num js-line-number" data-line-number="117"></td>
        <td id="LC117" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.03138930e-01</span>,   <span class="pl-c1">1.57151161e-01</span>,   <span class="pl-c1">3.95891308e-01</span>],</td>
      </tr>
      <tr>
        <td id="L118" class="blob-num js-line-number" data-line-number="118"></td>
        <td id="LC118" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.09330184e-01</span>,   <span class="pl-c1">1.59473549e-01</span>,   <span class="pl-c1">3.93589349e-01</span>],</td>
      </tr>
      <tr>
        <td id="L119" class="blob-num js-line-number" data-line-number="119"></td>
        <td id="LC119" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.15512627e-01</span>,   <span class="pl-c1">1.61817111e-01</span>,   <span class="pl-c1">3.91219295e-01</span>],</td>
      </tr>
      <tr>
        <td id="L120" class="blob-num js-line-number" data-line-number="120"></td>
        <td id="LC120" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.21685340e-01</span>,   <span class="pl-c1">1.64183582e-01</span>,   <span class="pl-c1">3.88781456e-01</span>],</td>
      </tr>
      <tr>
        <td id="L121" class="blob-num js-line-number" data-line-number="121"></td>
        <td id="LC121" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.27847374e-01</span>,   <span class="pl-c1">1.66574724e-01</span>,   <span class="pl-c1">3.86276180e-01</span>],</td>
      </tr>
      <tr>
        <td id="L122" class="blob-num js-line-number" data-line-number="122"></td>
        <td id="LC122" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.33997746e-01</span>,   <span class="pl-c1">1.68992314e-01</span>,   <span class="pl-c1">3.83703854e-01</span>],</td>
      </tr>
      <tr>
        <td id="L123" class="blob-num js-line-number" data-line-number="123"></td>
        <td id="LC123" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.40135447e-01</span>,   <span class="pl-c1">1.71438150e-01</span>,   <span class="pl-c1">3.81064906e-01</span>],</td>
      </tr>
      <tr>
        <td id="L124" class="blob-num js-line-number" data-line-number="124"></td>
        <td id="LC124" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.46259648e-01</span>,   <span class="pl-c1">1.73913876e-01</span>,   <span class="pl-c1">3.78358969e-01</span>],</td>
      </tr>
      <tr>
        <td id="L125" class="blob-num js-line-number" data-line-number="125"></td>
        <td id="LC125" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.52369348e-01</span>,   <span class="pl-c1">1.76421271e-01</span>,   <span class="pl-c1">3.75586209e-01</span>],</td>
      </tr>
      <tr>
        <td id="L126" class="blob-num js-line-number" data-line-number="126"></td>
        <td id="LC126" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.58463166e-01</span>,   <span class="pl-c1">1.78962399e-01</span>,   <span class="pl-c1">3.72748214e-01</span>],</td>
      </tr>
      <tr>
        <td id="L127" class="blob-num js-line-number" data-line-number="127"></td>
        <td id="LC127" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.64539964e-01</span>,   <span class="pl-c1">1.81539111e-01</span>,   <span class="pl-c1">3.69845599e-01</span>],</td>
      </tr>
      <tr>
        <td id="L128" class="blob-num js-line-number" data-line-number="128"></td>
        <td id="LC128" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.70598572e-01</span>,   <span class="pl-c1">1.84153268e-01</span>,   <span class="pl-c1">3.66879025e-01</span>],</td>
      </tr>
      <tr>
        <td id="L129" class="blob-num js-line-number" data-line-number="129"></td>
        <td id="LC129" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.76637795e-01</span>,   <span class="pl-c1">1.86806728e-01</span>,   <span class="pl-c1">3.63849195e-01</span>],</td>
      </tr>
      <tr>
        <td id="L130" class="blob-num js-line-number" data-line-number="130"></td>
        <td id="LC130" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.82656407e-01</span>,   <span class="pl-c1">1.89501352e-01</span>,   <span class="pl-c1">3.60756856e-01</span>],</td>
      </tr>
      <tr>
        <td id="L131" class="blob-num js-line-number" data-line-number="131"></td>
        <td id="LC131" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.88653158e-01</span>,   <span class="pl-c1">1.92238994e-01</span>,   <span class="pl-c1">3.57602797e-01</span>],</td>
      </tr>
      <tr>
        <td id="L132" class="blob-num js-line-number" data-line-number="132"></td>
        <td id="LC132" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">6.94626769e-01</span>,   <span class="pl-c1">1.95021500e-01</span>,   <span class="pl-c1">3.54387853e-01</span>],</td>
      </tr>
      <tr>
        <td id="L133" class="blob-num js-line-number" data-line-number="133"></td>
        <td id="LC133" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.00575937e-01</span>,   <span class="pl-c1">1.97850703e-01</span>,   <span class="pl-c1">3.51112900e-01</span>],</td>
      </tr>
      <tr>
        <td id="L134" class="blob-num js-line-number" data-line-number="134"></td>
        <td id="LC134" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.06499709e-01</span>,   <span class="pl-c1">2.00728196e-01</span>,   <span class="pl-c1">3.47776863e-01</span>],</td>
      </tr>
      <tr>
        <td id="L135" class="blob-num js-line-number" data-line-number="135"></td>
        <td id="LC135" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.12396345e-01</span>,   <span class="pl-c1">2.03656029e-01</span>,   <span class="pl-c1">3.44382594e-01</span>],</td>
      </tr>
      <tr>
        <td id="L136" class="blob-num js-line-number" data-line-number="136"></td>
        <td id="LC136" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.18264447e-01</span>,   <span class="pl-c1">2.06635993e-01</span>,   <span class="pl-c1">3.40931208e-01</span>],</td>
      </tr>
      <tr>
        <td id="L137" class="blob-num js-line-number" data-line-number="137"></td>
        <td id="LC137" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.24102613e-01</span>,   <span class="pl-c1">2.09669834e-01</span>,   <span class="pl-c1">3.37423766e-01</span>],</td>
      </tr>
      <tr>
        <td id="L138" class="blob-num js-line-number" data-line-number="138"></td>
        <td id="LC138" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.29909422e-01</span>,   <span class="pl-c1">2.12759270e-01</span>,   <span class="pl-c1">3.33861367e-01</span>],</td>
      </tr>
      <tr>
        <td id="L139" class="blob-num js-line-number" data-line-number="139"></td>
        <td id="LC139" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.35683432e-01</span>,   <span class="pl-c1">2.15905976e-01</span>,   <span class="pl-c1">3.30245147e-01</span>],</td>
      </tr>
      <tr>
        <td id="L140" class="blob-num js-line-number" data-line-number="140"></td>
        <td id="LC140" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.41423185e-01</span>,   <span class="pl-c1">2.19111589e-01</span>,   <span class="pl-c1">3.26576275e-01</span>],</td>
      </tr>
      <tr>
        <td id="L141" class="blob-num js-line-number" data-line-number="141"></td>
        <td id="LC141" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.47127207e-01</span>,   <span class="pl-c1">2.22377697e-01</span>,   <span class="pl-c1">3.22855952e-01</span>],</td>
      </tr>
      <tr>
        <td id="L142" class="blob-num js-line-number" data-line-number="142"></td>
        <td id="LC142" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.52794009e-01</span>,   <span class="pl-c1">2.25705837e-01</span>,   <span class="pl-c1">3.19085410e-01</span>],</td>
      </tr>
      <tr>
        <td id="L143" class="blob-num js-line-number" data-line-number="143"></td>
        <td id="LC143" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.58422090e-01</span>,   <span class="pl-c1">2.29097492e-01</span>,   <span class="pl-c1">3.15265910e-01</span>],</td>
      </tr>
      <tr>
        <td id="L144" class="blob-num js-line-number" data-line-number="144"></td>
        <td id="LC144" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.64009940e-01</span>,   <span class="pl-c1">2.32554083e-01</span>,   <span class="pl-c1">3.11398734e-01</span>],</td>
      </tr>
      <tr>
        <td id="L145" class="blob-num js-line-number" data-line-number="145"></td>
        <td id="LC145" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.69556038e-01</span>,   <span class="pl-c1">2.36076967e-01</span>,   <span class="pl-c1">3.07485188e-01</span>],</td>
      </tr>
      <tr>
        <td id="L146" class="blob-num js-line-number" data-line-number="146"></td>
        <td id="LC146" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.75058888e-01</span>,   <span class="pl-c1">2.39667435e-01</span>,   <span class="pl-c1">3.03526312e-01</span>],</td>
      </tr>
      <tr>
        <td id="L147" class="blob-num js-line-number" data-line-number="147"></td>
        <td id="LC147" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.80517023e-01</span>,   <span class="pl-c1">2.43326720e-01</span>,   <span class="pl-c1">2.99522665e-01</span>],</td>
      </tr>
      <tr>
        <td id="L148" class="blob-num js-line-number" data-line-number="148"></td>
        <td id="LC148" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.85928794e-01</span>,   <span class="pl-c1">2.47055968e-01</span>,   <span class="pl-c1">2.95476756e-01</span>],</td>
      </tr>
      <tr>
        <td id="L149" class="blob-num js-line-number" data-line-number="149"></td>
        <td id="LC149" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.91292674e-01</span>,   <span class="pl-c1">2.50856232e-01</span>,   <span class="pl-c1">2.91389943e-01</span>],</td>
      </tr>
      <tr>
        <td id="L150" class="blob-num js-line-number" data-line-number="150"></td>
        <td id="LC150" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">7.96607144e-01</span>,   <span class="pl-c1">2.54728485e-01</span>,   <span class="pl-c1">2.87263585e-01</span>],</td>
      </tr>
      <tr>
        <td id="L151" class="blob-num js-line-number" data-line-number="151"></td>
        <td id="LC151" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.01870689e-01</span>,   <span class="pl-c1">2.58673610e-01</span>,   <span class="pl-c1">2.83099033e-01</span>],</td>
      </tr>
      <tr>
        <td id="L152" class="blob-num js-line-number" data-line-number="152"></td>
        <td id="LC152" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.07081807e-01</span>,   <span class="pl-c1">2.62692401e-01</span>,   <span class="pl-c1">2.78897629e-01</span>],</td>
      </tr>
      <tr>
        <td id="L153" class="blob-num js-line-number" data-line-number="153"></td>
        <td id="LC153" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.12239008e-01</span>,   <span class="pl-c1">2.66785558e-01</span>,   <span class="pl-c1">2.74660698e-01</span>],</td>
      </tr>
      <tr>
        <td id="L154" class="blob-num js-line-number" data-line-number="154"></td>
        <td id="LC154" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.17340818e-01</span>,   <span class="pl-c1">2.70953688e-01</span>,   <span class="pl-c1">2.70389545e-01</span>],</td>
      </tr>
      <tr>
        <td id="L155" class="blob-num js-line-number" data-line-number="155"></td>
        <td id="LC155" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.22385784e-01</span>,   <span class="pl-c1">2.75197300e-01</span>,   <span class="pl-c1">2.66085445e-01</span>],</td>
      </tr>
      <tr>
        <td id="L156" class="blob-num js-line-number" data-line-number="156"></td>
        <td id="LC156" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.27372474e-01</span>,   <span class="pl-c1">2.79516805e-01</span>,   <span class="pl-c1">2.61749643e-01</span>],</td>
      </tr>
      <tr>
        <td id="L157" class="blob-num js-line-number" data-line-number="157"></td>
        <td id="LC157" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.32299481e-01</span>,   <span class="pl-c1">2.83912516e-01</span>,   <span class="pl-c1">2.57383341e-01</span>],</td>
      </tr>
      <tr>
        <td id="L158" class="blob-num js-line-number" data-line-number="158"></td>
        <td id="LC158" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.37165425e-01</span>,   <span class="pl-c1">2.88384647e-01</span>,   <span class="pl-c1">2.52987700e-01</span>],</td>
      </tr>
      <tr>
        <td id="L159" class="blob-num js-line-number" data-line-number="159"></td>
        <td id="LC159" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.41968959e-01</span>,   <span class="pl-c1">2.92933312e-01</span>,   <span class="pl-c1">2.48563825e-01</span>],</td>
      </tr>
      <tr>
        <td id="L160" class="blob-num js-line-number" data-line-number="160"></td>
        <td id="LC160" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.46708768e-01</span>,   <span class="pl-c1">2.97558528e-01</span>,   <span class="pl-c1">2.44112767e-01</span>],</td>
      </tr>
      <tr>
        <td id="L161" class="blob-num js-line-number" data-line-number="161"></td>
        <td id="LC161" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.51383572e-01</span>,   <span class="pl-c1">3.02260213e-01</span>,   <span class="pl-c1">2.39635512e-01</span>],</td>
      </tr>
      <tr>
        <td id="L162" class="blob-num js-line-number" data-line-number="162"></td>
        <td id="LC162" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.55992130e-01</span>,   <span class="pl-c1">3.07038188e-01</span>,   <span class="pl-c1">2.35132978e-01</span>],</td>
      </tr>
      <tr>
        <td id="L163" class="blob-num js-line-number" data-line-number="163"></td>
        <td id="LC163" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.60533241e-01</span>,   <span class="pl-c1">3.11892183e-01</span>,   <span class="pl-c1">2.30606009e-01</span>],</td>
      </tr>
      <tr>
        <td id="L164" class="blob-num js-line-number" data-line-number="164"></td>
        <td id="LC164" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.65005747e-01</span>,   <span class="pl-c1">3.16821833e-01</span>,   <span class="pl-c1">2.26055368e-01</span>],</td>
      </tr>
      <tr>
        <td id="L165" class="blob-num js-line-number" data-line-number="165"></td>
        <td id="LC165" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.69408534e-01</span>,   <span class="pl-c1">3.21826685e-01</span>,   <span class="pl-c1">2.21481734e-01</span>],</td>
      </tr>
      <tr>
        <td id="L166" class="blob-num js-line-number" data-line-number="166"></td>
        <td id="LC166" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.73740530e-01</span>,   <span class="pl-c1">3.26906201e-01</span>,   <span class="pl-c1">2.16885699e-01</span>],</td>
      </tr>
      <tr>
        <td id="L167" class="blob-num js-line-number" data-line-number="167"></td>
        <td id="LC167" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.78000715e-01</span>,   <span class="pl-c1">3.32059760e-01</span>,   <span class="pl-c1">2.12267762e-01</span>],</td>
      </tr>
      <tr>
        <td id="L168" class="blob-num js-line-number" data-line-number="168"></td>
        <td id="LC168" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.82188112e-01</span>,   <span class="pl-c1">3.37286663e-01</span>,   <span class="pl-c1">2.07628326e-01</span>],</td>
      </tr>
      <tr>
        <td id="L169" class="blob-num js-line-number" data-line-number="169"></td>
        <td id="LC169" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.86301795e-01</span>,   <span class="pl-c1">3.42586137e-01</span>,   <span class="pl-c1">2.02967696e-01</span>],</td>
      </tr>
      <tr>
        <td id="L170" class="blob-num js-line-number" data-line-number="170"></td>
        <td id="LC170" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.90340885e-01</span>,   <span class="pl-c1">3.47957340e-01</span>,   <span class="pl-c1">1.98286080e-01</span>],</td>
      </tr>
      <tr>
        <td id="L171" class="blob-num js-line-number" data-line-number="171"></td>
        <td id="LC171" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.94304553e-01</span>,   <span class="pl-c1">3.53399363e-01</span>,   <span class="pl-c1">1.93583583e-01</span>],</td>
      </tr>
      <tr>
        <td id="L172" class="blob-num js-line-number" data-line-number="172"></td>
        <td id="LC172" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">8.98192017e-01</span>,   <span class="pl-c1">3.58911240e-01</span>,   <span class="pl-c1">1.88860212e-01</span>],</td>
      </tr>
      <tr>
        <td id="L173" class="blob-num js-line-number" data-line-number="173"></td>
        <td id="LC173" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.02002544e-01</span>,   <span class="pl-c1">3.64491949e-01</span>,   <span class="pl-c1">1.84115876e-01</span>],</td>
      </tr>
      <tr>
        <td id="L174" class="blob-num js-line-number" data-line-number="174"></td>
        <td id="LC174" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.05735448e-01</span>,   <span class="pl-c1">3.70140419e-01</span>,   <span class="pl-c1">1.79350388e-01</span>],</td>
      </tr>
      <tr>
        <td id="L175" class="blob-num js-line-number" data-line-number="175"></td>
        <td id="LC175" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.09390090e-01</span>,   <span class="pl-c1">3.75855533e-01</span>,   <span class="pl-c1">1.74563472e-01</span>],</td>
      </tr>
      <tr>
        <td id="L176" class="blob-num js-line-number" data-line-number="176"></td>
        <td id="LC176" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.12965874e-01</span>,   <span class="pl-c1">3.81636138e-01</span>,   <span class="pl-c1">1.69754764e-01</span>],</td>
      </tr>
      <tr>
        <td id="L177" class="blob-num js-line-number" data-line-number="177"></td>
        <td id="LC177" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.16462251e-01</span>,   <span class="pl-c1">3.87481044e-01</span>,   <span class="pl-c1">1.64923826e-01</span>],</td>
      </tr>
      <tr>
        <td id="L178" class="blob-num js-line-number" data-line-number="178"></td>
        <td id="LC178" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.19878710e-01</span>,   <span class="pl-c1">3.93389034e-01</span>,   <span class="pl-c1">1.60070152e-01</span>],</td>
      </tr>
      <tr>
        <td id="L179" class="blob-num js-line-number" data-line-number="179"></td>
        <td id="LC179" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.23214783e-01</span>,   <span class="pl-c1">3.99358867e-01</span>,   <span class="pl-c1">1.55193185e-01</span>],</td>
      </tr>
      <tr>
        <td id="L180" class="blob-num js-line-number" data-line-number="180"></td>
        <td id="LC180" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.26470039e-01</span>,   <span class="pl-c1">4.05389282e-01</span>,   <span class="pl-c1">1.50292329e-01</span>],</td>
      </tr>
      <tr>
        <td id="L181" class="blob-num js-line-number" data-line-number="181"></td>
        <td id="LC181" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.29644083e-01</span>,   <span class="pl-c1">4.11479007e-01</span>,   <span class="pl-c1">1.45366973e-01</span>],</td>
      </tr>
      <tr>
        <td id="L182" class="blob-num js-line-number" data-line-number="182"></td>
        <td id="LC182" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.32736555e-01</span>,   <span class="pl-c1">4.17626756e-01</span>,   <span class="pl-c1">1.40416519e-01</span>],</td>
      </tr>
      <tr>
        <td id="L183" class="blob-num js-line-number" data-line-number="183"></td>
        <td id="LC183" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.35747126e-01</span>,   <span class="pl-c1">4.23831237e-01</span>,   <span class="pl-c1">1.35440416e-01</span>],</td>
      </tr>
      <tr>
        <td id="L184" class="blob-num js-line-number" data-line-number="184"></td>
        <td id="LC184" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.38675494e-01</span>,   <span class="pl-c1">4.30091162e-01</span>,   <span class="pl-c1">1.30438175e-01</span>],</td>
      </tr>
      <tr>
        <td id="L185" class="blob-num js-line-number" data-line-number="185"></td>
        <td id="LC185" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.41521384e-01</span>,   <span class="pl-c1">4.36405243e-01</span>,   <span class="pl-c1">1.25409440e-01</span>],</td>
      </tr>
      <tr>
        <td id="L186" class="blob-num js-line-number" data-line-number="186"></td>
        <td id="LC186" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.44284543e-01</span>,   <span class="pl-c1">4.42772199e-01</span>,   <span class="pl-c1">1.20354038e-01</span>],</td>
      </tr>
      <tr>
        <td id="L187" class="blob-num js-line-number" data-line-number="187"></td>
        <td id="LC187" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.46964741e-01</span>,   <span class="pl-c1">4.49190757e-01</span>,   <span class="pl-c1">1.15272059e-01</span>],</td>
      </tr>
      <tr>
        <td id="L188" class="blob-num js-line-number" data-line-number="188"></td>
        <td id="LC188" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.49561766e-01</span>,   <span class="pl-c1">4.55659658e-01</span>,   <span class="pl-c1">1.10163947e-01</span>],</td>
      </tr>
      <tr>
        <td id="L189" class="blob-num js-line-number" data-line-number="189"></td>
        <td id="LC189" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.52075421e-01</span>,   <span class="pl-c1">4.62177656e-01</span>,   <span class="pl-c1">1.05030614e-01</span>],</td>
      </tr>
      <tr>
        <td id="L190" class="blob-num js-line-number" data-line-number="190"></td>
        <td id="LC190" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.54505523e-01</span>,   <span class="pl-c1">4.68743522e-01</span>,   <span class="pl-c1">9.98735931e-02</span>],</td>
      </tr>
      <tr>
        <td id="L191" class="blob-num js-line-number" data-line-number="191"></td>
        <td id="LC191" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.56851903e-01</span>,   <span class="pl-c1">4.75356048e-01</span>,   <span class="pl-c1">9.46952268e-02</span>],</td>
      </tr>
      <tr>
        <td id="L192" class="blob-num js-line-number" data-line-number="192"></td>
        <td id="LC192" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.59114397e-01</span>,   <span class="pl-c1">4.82014044e-01</span>,   <span class="pl-c1">8.94989073e-02</span>],</td>
      </tr>
      <tr>
        <td id="L193" class="blob-num js-line-number" data-line-number="193"></td>
        <td id="LC193" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.61292850e-01</span>,   <span class="pl-c1">4.88716345e-01</span>,   <span class="pl-c1">8.42893891e-02</span>],</td>
      </tr>
      <tr>
        <td id="L194" class="blob-num js-line-number" data-line-number="194"></td>
        <td id="LC194" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.63387110e-01</span>,   <span class="pl-c1">4.95461806e-01</span>,   <span class="pl-c1">7.90731907e-02</span>],</td>
      </tr>
      <tr>
        <td id="L195" class="blob-num js-line-number" data-line-number="195"></td>
        <td id="LC195" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.65397031e-01</span>,   <span class="pl-c1">5.02249309e-01</span>,   <span class="pl-c1">7.38591143e-02</span>],</td>
      </tr>
      <tr>
        <td id="L196" class="blob-num js-line-number" data-line-number="196"></td>
        <td id="LC196" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.67322465e-01</span>,   <span class="pl-c1">5.09077761e-01</span>,   <span class="pl-c1">6.86589199e-02</span>],</td>
      </tr>
      <tr>
        <td id="L197" class="blob-num js-line-number" data-line-number="197"></td>
        <td id="LC197" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.69163264e-01</span>,   <span class="pl-c1">5.15946092e-01</span>,   <span class="pl-c1">6.34881971e-02</span>],</td>
      </tr>
      <tr>
        <td id="L198" class="blob-num js-line-number" data-line-number="198"></td>
        <td id="LC198" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.70919277e-01</span>,   <span class="pl-c1">5.22853259e-01</span>,   <span class="pl-c1">5.83674890e-02</span>],</td>
      </tr>
      <tr>
        <td id="L199" class="blob-num js-line-number" data-line-number="199"></td>
        <td id="LC199" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.72590351e-01</span>,   <span class="pl-c1">5.29798246e-01</span>,   <span class="pl-c1">5.33237243e-02</span>],</td>
      </tr>
      <tr>
        <td id="L200" class="blob-num js-line-number" data-line-number="200"></td>
        <td id="LC200" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.74176327e-01</span>,   <span class="pl-c1">5.36780059e-01</span>,   <span class="pl-c1">4.83920090e-02</span>],</td>
      </tr>
      <tr>
        <td id="L201" class="blob-num js-line-number" data-line-number="201"></td>
        <td id="LC201" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.75677038e-01</span>,   <span class="pl-c1">5.43797733e-01</span>,   <span class="pl-c1">4.36177922e-02</span>],</td>
      </tr>
      <tr>
        <td id="L202" class="blob-num js-line-number" data-line-number="202"></td>
        <td id="LC202" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.77092313e-01</span>,   <span class="pl-c1">5.50850323e-01</span>,   <span class="pl-c1">3.90500131e-02</span>],</td>
      </tr>
      <tr>
        <td id="L203" class="blob-num js-line-number" data-line-number="203"></td>
        <td id="LC203" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.78421971e-01</span>,   <span class="pl-c1">5.57936911e-01</span>,   <span class="pl-c1">3.49306227e-02</span>],</td>
      </tr>
      <tr>
        <td id="L204" class="blob-num js-line-number" data-line-number="204"></td>
        <td id="LC204" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.79665824e-01</span>,   <span class="pl-c1">5.65056600e-01</span>,   <span class="pl-c1">3.14091591e-02</span>],</td>
      </tr>
      <tr>
        <td id="L205" class="blob-num js-line-number" data-line-number="205"></td>
        <td id="LC205" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.80823673e-01</span>,   <span class="pl-c1">5.72208516e-01</span>,   <span class="pl-c1">2.85075931e-02</span>],</td>
      </tr>
      <tr>
        <td id="L206" class="blob-num js-line-number" data-line-number="206"></td>
        <td id="LC206" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.81895311e-01</span>,   <span class="pl-c1">5.79391803e-01</span>,   <span class="pl-c1">2.62497353e-02</span>],</td>
      </tr>
      <tr>
        <td id="L207" class="blob-num js-line-number" data-line-number="207"></td>
        <td id="LC207" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.82880522e-01</span>,   <span class="pl-c1">5.86605627e-01</span>,   <span class="pl-c1">2.46613416e-02</span>],</td>
      </tr>
      <tr>
        <td id="L208" class="blob-num js-line-number" data-line-number="208"></td>
        <td id="LC208" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.83779081e-01</span>,   <span class="pl-c1">5.93849168e-01</span>,   <span class="pl-c1">2.37702263e-02</span>],</td>
      </tr>
      <tr>
        <td id="L209" class="blob-num js-line-number" data-line-number="209"></td>
        <td id="LC209" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.84590755e-01</span>,   <span class="pl-c1">6.01121626e-01</span>,   <span class="pl-c1">2.36063833e-02</span>],</td>
      </tr>
      <tr>
        <td id="L210" class="blob-num js-line-number" data-line-number="210"></td>
        <td id="LC210" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.85315301e-01</span>,   <span class="pl-c1">6.08422211e-01</span>,   <span class="pl-c1">2.42021174e-02</span>],</td>
      </tr>
      <tr>
        <td id="L211" class="blob-num js-line-number" data-line-number="211"></td>
        <td id="LC211" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.85952471e-01</span>,   <span class="pl-c1">6.15750147e-01</span>,   <span class="pl-c1">2.55921853e-02</span>],</td>
      </tr>
      <tr>
        <td id="L212" class="blob-num js-line-number" data-line-number="212"></td>
        <td id="LC212" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.86502013e-01</span>,   <span class="pl-c1">6.23104667e-01</span>,   <span class="pl-c1">2.78139496e-02</span>],</td>
      </tr>
      <tr>
        <td id="L213" class="blob-num js-line-number" data-line-number="213"></td>
        <td id="LC213" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.86963670e-01</span>,   <span class="pl-c1">6.30485011e-01</span>,   <span class="pl-c1">3.09075459e-02</span>],</td>
      </tr>
      <tr>
        <td id="L214" class="blob-num js-line-number" data-line-number="214"></td>
        <td id="LC214" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.87337182e-01</span>,   <span class="pl-c1">6.37890424e-01</span>,   <span class="pl-c1">3.49160639e-02</span>],</td>
      </tr>
      <tr>
        <td id="L215" class="blob-num js-line-number" data-line-number="215"></td>
        <td id="LC215" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.87622296e-01</span>,   <span class="pl-c1">6.45320152e-01</span>,   <span class="pl-c1">3.98857472e-02</span>],</td>
      </tr>
      <tr>
        <td id="L216" class="blob-num js-line-number" data-line-number="216"></td>
        <td id="LC216" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.87818759e-01</span>,   <span class="pl-c1">6.52773439e-01</span>,   <span class="pl-c1">4.55808037e-02</span>],</td>
      </tr>
      <tr>
        <td id="L217" class="blob-num js-line-number" data-line-number="217"></td>
        <td id="LC217" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.87926330e-01</span>,   <span class="pl-c1">6.60249526e-01</span>,   <span class="pl-c1">5.17503867e-02</span>],</td>
      </tr>
      <tr>
        <td id="L218" class="blob-num js-line-number" data-line-number="218"></td>
        <td id="LC218" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.87944783e-01</span>,   <span class="pl-c1">6.67747641e-01</span>,   <span class="pl-c1">5.83286889e-02</span>],</td>
      </tr>
      <tr>
        <td id="L219" class="blob-num js-line-number" data-line-number="219"></td>
        <td id="LC219" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.87873910e-01</span>,   <span class="pl-c1">6.75267000e-01</span>,   <span class="pl-c1">6.52570167e-02</span>],</td>
      </tr>
      <tr>
        <td id="L220" class="blob-num js-line-number" data-line-number="220"></td>
        <td id="LC220" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.87713535e-01</span>,   <span class="pl-c1">6.82806802e-01</span>,   <span class="pl-c1">7.24892330e-02</span>],</td>
      </tr>
      <tr>
        <td id="L221" class="blob-num js-line-number" data-line-number="221"></td>
        <td id="LC221" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.87463516e-01</span>,   <span class="pl-c1">6.90366218e-01</span>,   <span class="pl-c1">7.99897176e-02</span>],</td>
      </tr>
      <tr>
        <td id="L222" class="blob-num js-line-number" data-line-number="222"></td>
        <td id="LC222" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.87123759e-01</span>,   <span class="pl-c1">6.97944391e-01</span>,   <span class="pl-c1">8.77314215e-02</span>],</td>
      </tr>
      <tr>
        <td id="L223" class="blob-num js-line-number" data-line-number="223"></td>
        <td id="LC223" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.86694229e-01</span>,   <span class="pl-c1">7.05540424e-01</span>,   <span class="pl-c1">9.56941797e-02</span>],</td>
      </tr>
      <tr>
        <td id="L224" class="blob-num js-line-number" data-line-number="224"></td>
        <td id="LC224" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.86174970e-01</span>,   <span class="pl-c1">7.13153375e-01</span>,   <span class="pl-c1">1.03863324e-01</span>],</td>
      </tr>
      <tr>
        <td id="L225" class="blob-num js-line-number" data-line-number="225"></td>
        <td id="LC225" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.85565739e-01</span>,   <span class="pl-c1">7.20782460e-01</span>,   <span class="pl-c1">1.12228756e-01</span>],</td>
      </tr>
      <tr>
        <td id="L226" class="blob-num js-line-number" data-line-number="226"></td>
        <td id="LC226" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.84865203e-01</span>,   <span class="pl-c1">7.28427497e-01</span>,   <span class="pl-c1">1.20784651e-01</span>],</td>
      </tr>
      <tr>
        <td id="L227" class="blob-num js-line-number" data-line-number="227"></td>
        <td id="LC227" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.84075129e-01</span>,   <span class="pl-c1">7.36086521e-01</span>,   <span class="pl-c1">1.29526579e-01</span>],</td>
      </tr>
      <tr>
        <td id="L228" class="blob-num js-line-number" data-line-number="228"></td>
        <td id="LC228" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.83195992e-01</span>,   <span class="pl-c1">7.43758326e-01</span>,   <span class="pl-c1">1.38453063e-01</span>],</td>
      </tr>
      <tr>
        <td id="L229" class="blob-num js-line-number" data-line-number="229"></td>
        <td id="LC229" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.82228463e-01</span>,   <span class="pl-c1">7.51441596e-01</span>,   <span class="pl-c1">1.47564573e-01</span>],</td>
      </tr>
      <tr>
        <td id="L230" class="blob-num js-line-number" data-line-number="230"></td>
        <td id="LC230" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.81173457e-01</span>,   <span class="pl-c1">7.59134892e-01</span>,   <span class="pl-c1">1.56863224e-01</span>],</td>
      </tr>
      <tr>
        <td id="L231" class="blob-num js-line-number" data-line-number="231"></td>
        <td id="LC231" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.80032178e-01</span>,   <span class="pl-c1">7.66836624e-01</span>,   <span class="pl-c1">1.66352544e-01</span>],</td>
      </tr>
      <tr>
        <td id="L232" class="blob-num js-line-number" data-line-number="232"></td>
        <td id="LC232" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.78806183e-01</span>,   <span class="pl-c1">7.74545028e-01</span>,   <span class="pl-c1">1.76037298e-01</span>],</td>
      </tr>
      <tr>
        <td id="L233" class="blob-num js-line-number" data-line-number="233"></td>
        <td id="LC233" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.77497453e-01</span>,   <span class="pl-c1">7.82258138e-01</span>,   <span class="pl-c1">1.85923357e-01</span>],</td>
      </tr>
      <tr>
        <td id="L234" class="blob-num js-line-number" data-line-number="234"></td>
        <td id="LC234" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.76108474e-01</span>,   <span class="pl-c1">7.89973753e-01</span>,   <span class="pl-c1">1.96017589e-01</span>],</td>
      </tr>
      <tr>
        <td id="L235" class="blob-num js-line-number" data-line-number="235"></td>
        <td id="LC235" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.74637842e-01</span>,   <span class="pl-c1">7.97691563e-01</span>,   <span class="pl-c1">2.06331925e-01</span>],</td>
      </tr>
      <tr>
        <td id="L236" class="blob-num js-line-number" data-line-number="236"></td>
        <td id="LC236" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.73087939e-01</span>,   <span class="pl-c1">8.05409333e-01</span>,   <span class="pl-c1">2.16876839e-01</span>],</td>
      </tr>
      <tr>
        <td id="L237" class="blob-num js-line-number" data-line-number="237"></td>
        <td id="LC237" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.71467822e-01</span>,   <span class="pl-c1">8.13121725e-01</span>,   <span class="pl-c1">2.27658046e-01</span>],</td>
      </tr>
      <tr>
        <td id="L238" class="blob-num js-line-number" data-line-number="238"></td>
        <td id="LC238" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.69783146e-01</span>,   <span class="pl-c1">8.20825143e-01</span>,   <span class="pl-c1">2.38685942e-01</span>],</td>
      </tr>
      <tr>
        <td id="L239" class="blob-num js-line-number" data-line-number="239"></td>
        <td id="LC239" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.68040817e-01</span>,   <span class="pl-c1">8.28515491e-01</span>,   <span class="pl-c1">2.49971582e-01</span>],</td>
      </tr>
      <tr>
        <td id="L240" class="blob-num js-line-number" data-line-number="240"></td>
        <td id="LC240" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.66242589e-01</span>,   <span class="pl-c1">8.36190976e-01</span>,   <span class="pl-c1">2.61533898e-01</span>],</td>
      </tr>
      <tr>
        <td id="L241" class="blob-num js-line-number" data-line-number="241"></td>
        <td id="LC241" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.64393924e-01</span>,   <span class="pl-c1">8.43848069e-01</span>,   <span class="pl-c1">2.73391112e-01</span>],</td>
      </tr>
      <tr>
        <td id="L242" class="blob-num js-line-number" data-line-number="242"></td>
        <td id="LC242" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.62516656e-01</span>,   <span class="pl-c1">8.51476340e-01</span>,   <span class="pl-c1">2.85545675e-01</span>],</td>
      </tr>
      <tr>
        <td id="L243" class="blob-num js-line-number" data-line-number="243"></td>
        <td id="LC243" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.60625545e-01</span>,   <span class="pl-c1">8.59068716e-01</span>,   <span class="pl-c1">2.98010219e-01</span>],</td>
      </tr>
      <tr>
        <td id="L244" class="blob-num js-line-number" data-line-number="244"></td>
        <td id="LC244" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.58720088e-01</span>,   <span class="pl-c1">8.66624355e-01</span>,   <span class="pl-c1">3.10820466e-01</span>],</td>
      </tr>
      <tr>
        <td id="L245" class="blob-num js-line-number" data-line-number="245"></td>
        <td id="LC245" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.56834075e-01</span>,   <span class="pl-c1">8.74128569e-01</span>,   <span class="pl-c1">3.23973947e-01</span>],</td>
      </tr>
      <tr>
        <td id="L246" class="blob-num js-line-number" data-line-number="246"></td>
        <td id="LC246" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.54997177e-01</span>,   <span class="pl-c1">8.81568926e-01</span>,   <span class="pl-c1">3.37475479e-01</span>],</td>
      </tr>
      <tr>
        <td id="L247" class="blob-num js-line-number" data-line-number="247"></td>
        <td id="LC247" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.53215092e-01</span>,   <span class="pl-c1">8.88942277e-01</span>,   <span class="pl-c1">3.51368713e-01</span>],</td>
      </tr>
      <tr>
        <td id="L248" class="blob-num js-line-number" data-line-number="248"></td>
        <td id="LC248" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.51546225e-01</span>,   <span class="pl-c1">8.96225909e-01</span>,   <span class="pl-c1">3.65627005e-01</span>],</td>
      </tr>
      <tr>
        <td id="L249" class="blob-num js-line-number" data-line-number="249"></td>
        <td id="LC249" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.50018481e-01</span>,   <span class="pl-c1">9.03409063e-01</span>,   <span class="pl-c1">3.80271225e-01</span>],</td>
      </tr>
      <tr>
        <td id="L250" class="blob-num js-line-number" data-line-number="250"></td>
        <td id="LC250" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.48683391e-01</span>,   <span class="pl-c1">9.10472964e-01</span>,   <span class="pl-c1">3.95289169e-01</span>],</td>
      </tr>
      <tr>
        <td id="L251" class="blob-num js-line-number" data-line-number="251"></td>
        <td id="LC251" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.47594362e-01</span>,   <span class="pl-c1">9.17399053e-01</span>,   <span class="pl-c1">4.10665194e-01</span>],</td>
      </tr>
      <tr>
        <td id="L252" class="blob-num js-line-number" data-line-number="252"></td>
        <td id="LC252" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.46809163e-01</span>,   <span class="pl-c1">9.24168246e-01</span>,   <span class="pl-c1">4.26373236e-01</span>],</td>
      </tr>
      <tr>
        <td id="L253" class="blob-num js-line-number" data-line-number="253"></td>
        <td id="LC253" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.46391536e-01</span>,   <span class="pl-c1">9.30760752e-01</span>,   <span class="pl-c1">4.42367495e-01</span>],</td>
      </tr>
      <tr>
        <td id="L254" class="blob-num js-line-number" data-line-number="254"></td>
        <td id="LC254" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.46402951e-01</span>,   <span class="pl-c1">9.37158971e-01</span>,   <span class="pl-c1">4.58591507e-01</span>],</td>
      </tr>
      <tr>
        <td id="L255" class="blob-num js-line-number" data-line-number="255"></td>
        <td id="LC255" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.46902568e-01</span>,   <span class="pl-c1">9.43347775e-01</span>,   <span class="pl-c1">4.74969778e-01</span>],</td>
      </tr>
      <tr>
        <td id="L256" class="blob-num js-line-number" data-line-number="256"></td>
        <td id="LC256" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.47936825e-01</span>,   <span class="pl-c1">9.49317522e-01</span>,   <span class="pl-c1">4.91426053e-01</span>],</td>
      </tr>
      <tr>
        <td id="L257" class="blob-num js-line-number" data-line-number="257"></td>
        <td id="LC257" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.49544830e-01</span>,   <span class="pl-c1">9.55062900e-01</span>,   <span class="pl-c1">5.07859649e-01</span>],</td>
      </tr>
      <tr>
        <td id="L258" class="blob-num js-line-number" data-line-number="258"></td>
        <td id="LC258" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.51740304e-01</span>,   <span class="pl-c1">9.60586693e-01</span>,   <span class="pl-c1">5.24203026e-01</span>],</td>
      </tr>
      <tr>
        <td id="L259" class="blob-num js-line-number" data-line-number="259"></td>
        <td id="LC259" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.54529281e-01</span>,   <span class="pl-c1">9.65895868e-01</span>,   <span class="pl-c1">5.40360752e-01</span>],</td>
      </tr>
      <tr>
        <td id="L260" class="blob-num js-line-number" data-line-number="260"></td>
        <td id="LC260" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.57896053e-01</span>,   <span class="pl-c1">9.71003330e-01</span>,   <span class="pl-c1">5.56275090e-01</span>],</td>
      </tr>
      <tr>
        <td id="L261" class="blob-num js-line-number" data-line-number="261"></td>
        <td id="LC261" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.61812020e-01</span>,   <span class="pl-c1">9.75924241e-01</span>,   <span class="pl-c1">5.71925382e-01</span>],</td>
      </tr>
      <tr>
        <td id="L262" class="blob-num js-line-number" data-line-number="262"></td>
        <td id="LC262" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.66248822e-01</span>,   <span class="pl-c1">9.80678193e-01</span>,   <span class="pl-c1">5.87205773e-01</span>],</td>
      </tr>
      <tr>
        <td id="L263" class="blob-num js-line-number" data-line-number="263"></td>
        <td id="LC263" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.71161622e-01</span>,   <span class="pl-c1">9.85282161e-01</span>,   <span class="pl-c1">6.02154330e-01</span>],</td>
      </tr>
      <tr>
        <td id="L264" class="blob-num js-line-number" data-line-number="264"></td>
        <td id="LC264" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.76510983e-01</span>,   <span class="pl-c1">9.89753437e-01</span>,   <span class="pl-c1">6.16760413e-01</span>],</td>
      </tr>
      <tr>
        <td id="L265" class="blob-num js-line-number" data-line-number="265"></td>
        <td id="LC265" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.82257307e-01</span>,   <span class="pl-c1">9.94108844e-01</span>,   <span class="pl-c1">6.31017009e-01</span>],</td>
      </tr>
      <tr>
        <td id="L266" class="blob-num js-line-number" data-line-number="266"></td>
        <td id="LC266" class="blob-code blob-code-inner js-file-line">       [  <span class="pl-c1">9.88362068e-01</span>,   <span class="pl-c1">9.98364143e-01</span>,   <span class="pl-c1">6.44924005e-01</span>]]</td>
      </tr>
      <tr>
        <td id="L267" class="blob-num js-line-number" data-line-number="267"></td>
        <td id="LC267" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L268" class="blob-num js-line-number" data-line-number="268"></td>
        <td id="LC268" class="blob-code blob-code-inner js-file-line">test_cm <span class="pl-k">=</span> LinearSegmentedColormap.from_list(<span class="pl-c1">__file__</span>, cm_data)</td>
      </tr>
      <tr>
        <td id="L269" class="blob-num js-line-number" data-line-number="269"></td>
        <td id="LC269" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L270" class="blob-num js-line-number" data-line-number="270"></td>
        <td id="LC270" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L271" class="blob-num js-line-number" data-line-number="271"></td>
        <td id="LC271" class="blob-code blob-code-inner js-file-line"><span class="pl-k">if</span> <span class="pl-c1">__name__</span> <span class="pl-k">==</span> <span class="pl-s"><span class="pl-pds">&quot;</span>__main__<span class="pl-pds">&quot;</span></span>:</td>
      </tr>
      <tr>
        <td id="L272" class="blob-num js-line-number" data-line-number="272"></td>
        <td id="LC272" class="blob-code blob-code-inner js-file-line">    <span class="pl-k">import</span> matplotlib.pyplot <span class="pl-k">as</span> plt</td>
      </tr>
      <tr>
        <td id="L273" class="blob-num js-line-number" data-line-number="273"></td>
        <td id="LC273" class="blob-code blob-code-inner js-file-line">    <span class="pl-k">import</span> numpy <span class="pl-k">as</span> np</td>
      </tr>
      <tr>
        <td id="L274" class="blob-num js-line-number" data-line-number="274"></td>
        <td id="LC274" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L275" class="blob-num js-line-number" data-line-number="275"></td>
        <td id="LC275" class="blob-code blob-code-inner js-file-line">    <span class="pl-k">try</span>:</td>
      </tr>
      <tr>
        <td id="L276" class="blob-num js-line-number" data-line-number="276"></td>
        <td id="LC276" class="blob-code blob-code-inner js-file-line">        <span class="pl-k">from</span> pycam02ucs.cm.viscm <span class="pl-k">import</span> viscm</td>
      </tr>
      <tr>
        <td id="L277" class="blob-num js-line-number" data-line-number="277"></td>
        <td id="LC277" class="blob-code blob-code-inner js-file-line">        viscm(test_cm)</td>
      </tr>
      <tr>
        <td id="L278" class="blob-num js-line-number" data-line-number="278"></td>
        <td id="LC278" class="blob-code blob-code-inner js-file-line">    <span class="pl-k">except</span> <span class="pl-c1">ImportError</span>:</td>
      </tr>
      <tr>
        <td id="L279" class="blob-num js-line-number" data-line-number="279"></td>
        <td id="LC279" class="blob-code blob-code-inner js-file-line">        <span class="pl-k">print</span>(<span class="pl-s"><span class="pl-pds">&quot;</span>pycam02ucs not found, falling back on simple display<span class="pl-pds">&quot;</span></span>)</td>
      </tr>
      <tr>
        <td id="L280" class="blob-num js-line-number" data-line-number="280"></td>
        <td id="LC280" class="blob-code blob-code-inner js-file-line">        plt.imshow(np.linspace(<span class="pl-c1">0</span>, <span class="pl-c1">100</span>, <span class="pl-c1">256</span>)[<span class="pl-c1">None</span>, :], <span class="pl-smi">aspect</span><span class="pl-k">=</span><span class="pl-s"><span class="pl-pds">&#39;</span>auto<span class="pl-pds">&#39;</span></span>,</td>
      </tr>
      <tr>
        <td id="L281" class="blob-num js-line-number" data-line-number="281"></td>
        <td id="LC281" class="blob-code blob-code-inner js-file-line">                   <span class="pl-smi">cmap</span><span class="pl-k">=</span>test_cm)</td>
      </tr>
      <tr>
        <td id="L282" class="blob-num js-line-number" data-line-number="282"></td>
        <td id="LC282" class="blob-code blob-code-inner js-file-line">    plt.show()</td>
      </tr>
</table>

  </div>

</div>

<a href="#jump-to-line" rel="facebox[.linejump]" data-hotkey="l" style="display:none">Jump to Line</a>
<div id="jump-to-line" style="display:none">
  <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="" class="js-jump-to-line-form" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
    <input class="linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" autofocus>
    <button type="submit" class="btn">Go</button>
</form></div>

          </div>
        </div>
        <div class="modal-backdrop"></div>
      </div>
  </div>


    </div><!-- /.wrapper -->

      <div class="container">
  <div class="site-footer" role="contentinfo">
    <ul class="site-footer-links right">
        <li><a href="https://status.github.com/" data-ga-click="Footer, go to status, text:status">Status</a></li>
      <li><a href="https://developer.github.com" data-ga-click="Footer, go to api, text:api">API</a></li>
      <li><a href="https://training.github.com" data-ga-click="Footer, go to training, text:training">Training</a></li>
      <li><a href="https://shop.github.com" data-ga-click="Footer, go to shop, text:shop">Shop</a></li>
        <li><a href="https://github.com/blog" data-ga-click="Footer, go to blog, text:blog">Blog</a></li>
        <li><a href="https://github.com/about" data-ga-click="Footer, go to about, text:about">About</a></li>
        <li><a href="https://help.github.com" data-ga-click="Footer, go to help, text:help">Help</a></li>

    </ul>

    <a href="https://github.com" aria-label="Homepage">
      <span class="mega-octicon octicon-mark-github" title="GitHub"></span>
</a>
    <ul class="site-footer-links">
      <li>&copy; 2015 <span title="0.04592s from github-fe133-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="https://github.com/site/terms" data-ga-click="Footer, go to terms, text:terms">Terms</a></li>
        <li><a href="https://github.com/site/privacy" data-ga-click="Footer, go to privacy, text:privacy">Privacy</a></li>
        <li><a href="https://github.com/security" data-ga-click="Footer, go to security, text:security">Security</a></li>
        <li><a href="https://github.com/contact" data-ga-click="Footer, go to contact, text:contact">Contact</a></li>
    </ul>
  </div>
</div>


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-suggester-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="fullscreen-contents js-fullscreen-contents" placeholder=""></textarea>
      <div class="suggester-container">
        <div class="suggester fullscreen-suggester js-suggester js-navigation-container"></div>
      </div>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped tooltipped-w" aria-label="Exit Zen Mode">
      <span class="mega-octicon octicon-screen-normal"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped tooltipped-w"
      aria-label="Switch themes">
      <span class="octicon octicon-color-mode"></span>
    </a>
  </div>
</div>



    
    

    <div id="ajax-error-message" class="flash flash-error">
      <span class="octicon octicon-alert"></span>
      <a href="#" class="octicon octicon-x flash-close js-ajax-error-dismiss" aria-label="Dismiss error"></a>
      Something went wrong with that request. Please try again.
    </div>


      <script crossorigin="anonymous" src="https://assets-cdn.github.com/assets/frameworks-01a8766fbba2df04677b68fb73599499b1ff20bcc969342c28d30f95fa836d0c.js"></script>
      <script async="async" crossorigin="anonymous" src="https://assets-cdn.github.com/assets/github/index-0a6831eed7acaf8b173663c8626749024571c7030a13c7539f3f0a17b217a99f.js"></script>
      
      
  </body>
</html>

