import numpy as np 
def cross(array):
    if array.shape!=(3,1):
        raise NotImplemented("cross applies only to 3 vectors")
    x,y,z=array[0,0],array[1,0],array[2,0]
    return np.array([
        [ 0, -z,   y],
        [ z,  0,  -x],
        [-y,  x,   0]])