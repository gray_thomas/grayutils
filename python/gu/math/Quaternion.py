import numpy as np
from math import pow, sqrt, pi, atan2,sin,cos
from collections import namedtuple
from VectorTricks import *

Quaternion = namedtuple("Quaternion",['w','x','y','z'])

def imag_quat(x,y,z):
    return Quaternion(0.0,x,y,z)

def quat_exponent(x,y,z):
    s=pow(x,2)+pow(y,2)+pow(z,2)
    if abs(s)<1e-7:
        return Quaternion(1.0,0.0,0.0,0.0)
    m=sqrt(s)
    return Quaternion(cos(m),sin(m)*x/m,sin(m)*y/m,sin(m)*z/m)

def axis_angle(x,y,z):
    m=sqrt(pow(x,2)+pow(y,2)+pow(z,2))
    o=m/2
    return Quaternion(cos(o),sin(o)*x/m,sin(o)*y/m,sin(o)*z/m)

def DCM(quat):
    r = np.array([[quat.x, quat.y, quat.z]]).T
    outert=2.0*r.dot(r.T)
    crosst=2.0*quat.w*cross(r)
    eyet=(pow(2.0,0.5)*quat.w-1.0)*(pow(2.0,0.5)*quat.w+1.0)*np.eye(3)
    DCM = outert+crosst+eyet
    return DCM

def norm(quat):
    return sqrt(pow(quat.w,2)+pow(quat.x,2)+pow(quat.y,2)+pow(quat.z,2))

def normalize(quat,value=1.0):
    scale=value/norm(quat)
    return Quaternion(quat.w*scale,quat.x*scale,quat.y*scale,quat.z*scale)

def versor(quat):
    return normalize(quat,1.0)

def q_mult(self,quat):
    w=self.w*quat.w-self.x*quat.x-self.y*quat.y-self.z*quat.z
    x=(self.w*quat.x+self.x*quat.w)+(self.y*quat.z-self.z*quat.y)
    y=(self.w*quat.y+self.y*quat.w)+(self.z*quat.x-self.x*quat.z)
    z=(self.w*quat.z+self.z*quat.w)+(self.x*quat.y-self.y*quat.x)
    return Quaternion(w,x,y,z)

def q_add(self,quat):
    w=self.w+quat.w
    x=self.x+quat.x
    y=self.y+quat.y
    z=self.z+quat.z
    return Quaternion(w,x,y,z)

def scale(self,scalar):
    w=self.w*scalar
    x=self.x*scalar
    y=self.y*scalar
    z=self.z*scalar
    return Quaternion(w,x,y,z)
    
def deriv(self,ox,oy,oz):
    return imag_quat(0.5*ox, 0.5*oy, 0.5*oz)*self

def stable_deriv(self, ox,oy,oz,kp):
    q1=deriv(self,ox,oy,oz)
    u = kp * (1.0-self.norm)
    q2=Quaternion(self.w*u,self.x*u,self.y*u,self.z*u)
    return q1+q2

def spherical_derivative_update(self,ox,oy,oz):
    return quat_exponent(0.5*ox, 0.5*oy, 0.5*oz)*self

def quaternion_exp(self):
    e=exp(self.w)
    return e*quat_exponent(self.x,self.y,self.z)

def quat_log(quat):
    n=sqrt(pow(quat.x,2)+pow(quat.y,2)+pow(quat.z,2))
    if n==0.0:
        return np.zeros((3,1))
    angle=atan2(n,quat.w)
    ret=np.array([[quat.x, quat.y, quat.z]]).T * (angle/n)
    return ret

def conjugate(quat):
    return Quaternion(quat.w, -quat.x, -quat.y, -quat.z)


setattr(Quaternion, "DCM", DCM)
setattr(Quaternion, "norm", property(norm))
setattr(Quaternion, "normalize", normalize)
setattr(Quaternion, "versor", property(versor))
setattr(Quaternion, "ln", property(quat_log))
setattr(Quaternion, "conjugate", property(conjugate))
setattr(Quaternion, "__mul__", q_mult)
setattr(Quaternion, "__add__", q_add)
setattr(Quaternion, "bar", property(conjugate))
setattr(Quaternion, "deriv", deriv)
setattr(Quaternion, "stable_deriv", stable_deriv)
setattr(Quaternion, "spherical_derivative_update", spherical_derivative_update)
setattr(Quaternion, "exp", quaternion_exp)
setattr(Quaternion, "scale", scale)


# Quaternion.DCM=DCM
if __name__=="__main__":
    a = Quaternion(1.0,2.0,3.0,4.0)
    print a, a.norm
    print a.normalize()
    print a.normalize().DCM()
    print a.versor == a.normalize()
    b = Quaternion(1.0, 0.0,0.0,0.0)
    print b.versor.DCM()
    c = Quaternion(1.0, 0.0,0.0,1.0)
    print c.versor
    print c.versor.DCM()
    print np.linalg.det(c.versor.DCM())
    print "hey"
    print axis_angle(0.0,0.0,pi/2)
    print axis_angle(0.0,0.0,pi/2).DCM()
    print axis_angle(0.0,0.0,pi/8).DCM()
    assert (abs(quat_exponent(0.0,0.0,pi/8).ln[2,0]-pi/8)<1e-7)
    scale=2*pi/(3*sqrt(3))
    print scale
    demo_quat=axis_angle(scale,scale,scale)
    print "demo quat", demo_quat, demo_quat.norm
    print axis_angle(scale,scale,scale).DCM()
    imquat=imag_quat(1.0,0.0,0.0)
    sub1=imquat*demo_quat.conjugate 
    print demo_quat
    print imquat
    print sub1
    print demo_quat*(imag_quat(0.0,0.0,1.0)*demo_quat.conjugate)
    q0=Quaternion(1.01,0.0,0.0,0.0)
    ts=np.linspace(0,1,1000)
    for i,t in enumerate(ts[1:]):
        q0=q0+q0.stable_deriv(0.0,0.0,1.0,10.0).scale((t-ts[i]))
        print q0,q0.norm
    print Quaternion(1.01,0.0,0.0,0.0).stable_deriv(0.0,0.0,1.0,1)
    print Quaternion(0.0,-1.0,1.0,0.0).versor.DCM()
    A=Quaternion(0.9,0.8,0.5,0.5).versor
    B=Quaternion(0.5,-0.5,0.5,-0.5)
    print A.DCM().dot(B.DCM())
    print (A*B).DCM()
    print np.array_repr(Quaternion(0,-1,0,-1).versor.DCM(),suppress_small=True)
    print np.array_repr(Quaternion(0,1,0,-1).versor.DCM(),suppress_small=True)
    print np.array_repr(Quaternion(1,0,0,-1).versor.DCM(),suppress_small=True)
    print np.array_repr(Quaternion(1,0,1,0).versor.DCM(),suppress_small=True)
    print np.array_repr(Quaternion(1,1,0,0).versor.DCM(),suppress_small=True)
    print np.array_repr(Quaternion(1,1,-1,-1).versor.DCM(),suppress_small=True)
    
