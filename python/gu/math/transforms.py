import numpy as np 
from math import pow, sqrt, pi
from gu.math import Quaternion, quat_exponent, cross
from gu.math.frames import *
from gu.math.thin_spatial_algebra import *
from gu.math.vector_geometry import *

class Transform(object):
    def __init__(self, base_frame, frame, 
        vec=np.array([[0.0,0.0,0.0]]).T, quat=Quaternion(1.0,0.0,0.0,0.0)):
        self.base_frame=base_frame
        self.frame=frame
        assert vec.shape == (3,1)
        self.vec=np.array(vec)
        self.quat=quat.versor
        self.DCM=self.quat.DCM()
    @property 
    def forwardDCM(self):
        return self.DCM
    @forwardDCM.setter
    def forwardDCM(self,newDCM):
        dxx = newDCM[0,0]
        dxy = newDCM[0,1]
        dxz = newDCM[0,2]
        dyx = newDCM[1,0]
        dyy = newDCM[1,1]
        dyz = newDCM[1,2]
        dzx = newDCM[2,0]
        dzy = newDCM[2,1]
        dzz = newDCM[2,2]
        directionTest2=np.array([
            [(dxx + dyy + dzz), (dyz - dzy), (dzx - dxz), (dxy - dyx)],
            [(dyz - dzy), (dxx - dyy - dzz), (dyx + dxy), (dzx + dxz)],
            [(dzx - dxz), (dyx + dxy), (dyy - dxx - dzz), (dzy + dyz)],
            [(dxy - dyx), (dzx + dxz), (dzy + dyz), (dzz - dxx - dyy)]
            ])
        directionTest2 *= (1.0 / 3.0)
        eigs, eigvecs = np.linalg.eigh(directionTest2)
        l1=[(eigs[i],eigvecs[:,i]) for i in range(0,4)]
        l1.sort(key=lambda (x): -x[0])
        best=l1[0]
        q=best[1]
        ret = Quaternion(q[0],-q[1],-q[2],-q[3])
        self.quat=ret
        self.DCM=self.quat.DCM()

    @property
    def x(self):
        return self.vec[0,0]
    @property
    def y(self):
        return self.vec[1,0]
    @property
    def z(self):
        return self.vec[2,0]
    @property
    def qw(self):
        return self.quat.w 
    @property
    def qx(self):
        return self.quat.x
    @property 
    def qy(self):
        return self.quat.y 
    @property
    def qz(self):
        return self.quat.z

    
            

    @property
    def reverseDCM(self):
        return self.DCM.T
    @reverseDCM.setter
    def reverseDCM(self, value):
        self.forwardDCM=value.T

    @property
    def forwardQuat(self):
        return self.quat

    @forwardQuat.setter
    def forwardQuat(self, value):
        self.quat = value.versor
        self.DCM=self.quat.DCM()

    @property
    def reverseQuat(self):
        return self.quat.conjugate

    @reverseQuat.setter
    def reverseQuat(self, value):
        self.forwardQuat = value.conjugate

    def move(self, motion):
        if not motion.type==GU_VG_MOTION:
            raise NotImplemented("only valid for motion vectors")
        new_vec = self.vec+motion.eval(self.vec)
        new_quat = self.forwardQuat.spherical_derivative_update(
            motion.rx, motion.ry, motion.rz)
        return Transform(self.base_frame, self.frame, vec=new_vec, quat=new_quat)

    def spatial_motion_transform(self):
        D=self.forwardDCM
        Z=np.zeros((3,3))
        vxD=cross(self.vec).dot(D)
        mat = np.bmat([[D,vxD],[Z,D]])
        return ThinSpatialMapping.new_motion_transform(
            self.base_frame, self.frame, mat)

    def inverse_motion_transform(self):
        Dt=self.reverseDCM
        Z=np.zeros((3,3))
        Dtvx=Dt.dot(cross(self.vec))
        mat = np.bmat([[Dt,-Dtvx],[Z,Dt]])
        return ThinSpatialMapping.new_motion_transform(
            self.frame, self.base_frame, mat)

    def spatial_force_transform(self):
        D=self.forwardDCM
        Z=np.zeros((3,3))
        vxD=cross(self.vec).dot(D)
        mat = np.bmat([[D,Z],[vxD,D]])
        return ThinSpatialMapping.new_force_transform(
            self.base_frame, self.frame, mat)
        
    def inverse_force_transform(self):
        Dt=self.reverseDCM
        Z=np.zeros((3,3))
        Dtvx=Dt.dot(cross(self.vec))
        mat = np.bmat([[Dt,Z],[-Dtvx,Dt]])
        return ThinSpatialMapping.new_force_transform(
            self.frame, self.base_frame, mat)
    def forward_transform_fat_vector_matrix(self,fat_matrix):
        assert fat_matrix.shape[0]==3
        return self.forwardDCM.dot(fat_matrix)

    def reverse_transform_fat_vector_matrix(self,fat_matrix):
        assert fat_matrix.shape[0]==3
        return self.reverseDCM.dot(fat_matrix)

    def forward_transform_fat_point_matrix(self,fat_matrix):
        vecs=self.forward_transform_fat_vector_matrix(fat_matrix)
        origins=np.diagflat(self.vec).dot(np.ones(fat_matrix.shape))
        return vecs+origins

    def reverse_transform_fat_point_matrix(self,fat_matrix):
        vecs=self.reverse_transform_fat_vector_matrix(fat_matrix)
        origins=np.diagflat(-self.reverseDCM.dot(self.vec)).dot(np.ones(fat_matrix.shape))
        return vecs+origins

    @property
    def f_trans(self):
        return self.spatial_force_transform()
    @property
    def m_trans(self):
        return self.spatial_motion_transform()

    @property
    def f_inv(self):
        return self.inverse_force_transform()
    @property
    def m_inv(self):
        return self.inverse_motion_transform()
    

    def transform_vector(self,vec):
        self.frame.assert_match(vec.base_frame)
        return FrameVector(self.base_frame, self.DCM.dot(vec.vec))

    def transform_point(self,point):
        self.frame.assert_match(point.base_frame)
        return FramePoint(self.base_frame, self.vec+self.DCM.dot(point.vec))

    def transform_transform(self,other):
        self.frame.assert_match(other.base_frame)
        qtot=self.quat*other.quat
        vec = self.vec+self.forwardDCM.dot(other.vec)
        return Transform(self.base_frame, other.frame, vec=vec,quat=qtot)

    def __mul__(self, other):
        if isinstance(other, FramePoint):
            return self.transform_point(other)
        if isinstance(other, FrameVector):
            return self.transform_vector(other)
        if isinstance(other, Transform):
            return self.transform_transform(other)
        raise NotImplemented("Class untransformable %s "% type(other))

    def __invert__(self):
        return Transform(self.frame,self.base_frame, vec=-self.reverseDCM.dot(self.vec), quat=self.reverseQuat)

    def __repr__(self):
        return "[%s]{%s}:<<%.2f,%.2f,%.2f> + <%.2f,%.2f,%.2f>i + <%.2f,%.2f,%.2f>j + <%.2f,%.2f,%.2f>k>"%(
            self.base_frame,self.frame,
            self.vec[0],self.vec[1],self.vec[2],
            self.DCM[0,0],self.DCM[1,0],self.DCM[2,0],
            self.DCM[0,1],self.DCM[1,1],self.DCM[2,1],
            self.DCM[0,2],self.DCM[1,2],self.DCM[2,2])

from math import sin, cos
if __name__=="__main__":
    world=Frame("world")
    body=Frame("body")
    trans=Transform(world,body)
    for theta in np.linspace (-pi,pi,8):
        for phi in np.linspace( -pi,pi,8):
            for psy in np.linspace(-pi,pi,8):
                rz=np.array([
                    [cos(theta),-sin(theta),0.0],
                    [sin(theta),cos(theta),0.0],
                    [0.0,0.0,1.0]])
                ry=np.array([
                    [cos(phi),0,sin(phi)],
                    [0.0,1.0,0.0],
                    [-sin(phi),0,cos(phi)]])
                rx=np.array([
                    [1.0,0.0,0.0],
                    [0.0,cos(psy),-sin(psy)],
                    [0.0,sin(psy),cos(psy)]])
                mat=rz.dot(ry.dot(rx))
                trans.forwardDCM=mat
                # print trans.m_inv*trans.m_trans
                diff = trans.forwardDCM-mat
                for r in range(0,3):
                    for c in range(0,3):
                        assert abs(diff[r,c])<1e-7
    print "hey, transform.forwardDCM=mat must work, because the assertions passed"
    theta=pi/2
    trans.forwardQuat=quat_exponent(0.0,0.0,theta)
    rz=np.array([
        [cos(theta),-sin(theta),0.0],
        [sin(theta),cos(theta),0.0],
        [0.0,0.0,1.0]])
    # print rz
    # print trans.forwardDCM


