from gu.math import Quaternion, quat_exponent, cross
from gu.math.Frames import *
import numpy as np 
GU_VG_FORCE = True
GU_VG_MOTION = False
def standard_mat_repr(mat):
    return np.array_repr(mat, max_line_width=80, precision=3, suppress_small=True)

class NonDualMultiplicationException(Exception):
    def __init__(self):
        pass
    def __str__(self):
        return "Disallowed"
class DualAdditionException(Exception):
    def __init__(self):
        pass
    def __str__(self):
        return "Disallowed"
class InsaneDualityExcepion(Exception):
    def __init__(self):
        pass
    def __str__(self):
        return "Disallowed"
def sanity_check(typ):
    if typ==GU_VG_FORCE or typ==GU_VG_MOTION:
        return 
    raise InsaneDualityExcepion()

def mul_check(type1,type2):
    if type1!=type2:
        return
    raise NonDualMultiplicationException()

def add_check(type1,type2):
    if type1==type2:
        return 
    raise DualAdditionException()


class ThinSpatialVector(object):
    def __init__(self, typ, base_frame, vec):
        assert vec.shape==(6,1)
        sanity_check(typ)
        self.type=typ
        self.base_frame=base_frame
        self.vec=np.array(vec)

    @classmethod
    def new_force(cls, base_frame, vec):
        return cls(GU_VG_FORCE, base_frame, vec)

    @classmethod
    def new_motion(cls, base_frame, vec):
        return cls(GU_VG_MOTION, base_frame, vec)

    def dot(self, other):
        if isinstance(other, ThinSpatialVector):
            mul_check(other.type,self.type)
            return self.vec.T.dot(other.vec)
        return NotImplemented()

    def __mul__(self, other):
        if isinstance(other, ThinSpatialVector):
            raise NotImplemented("vector vector mul")
        return ThinSpatialVector(self.type, self.base_frame, self.vec*other)

    def __rmul__(self, other):
        if isinstance(other, ThinSpatialVector):
            raise NotImplemented("vector vector mul")
        return ThinSpatialVector(self.type, self.base_frame, other*self.vec)

    def eval(self, point):
        if self.type==GU_VG_MOTION:
            return self.lin+cross(self.ang).dot(point)
        if self.type==GU_VG_FORCE:
            return cross(self.lin).dot(point)+self.ang
        raise NotImplemented("must be either force or motion")

    def eval_deriv(self, point, spacial_velocity):
        if self.type==GU_VG_MOTION:
            return self.lin+cross(self.ang).dot(point)+cross(spacial_velocity.ang).dot(spacial_velocity.eval(point))
        if self.type==GU_VG_FORCE:
            raise NotImplemented("differentiation of spacial force evaluations")
        raise NotImplemented("must be either force or motion")

    def __add__(self,other):
        if isinstance(other, ThinSpatialVector):
            Frame.check(self,other)
            add_check(self.type, other.type)
            return ThinSpatialVector(self.type, self.base_frame, self.vec+other.vec)
        return ThinSpatialVector(self.type, self.base_frame, self.vec+other)

    def __radd__(self,other):
        return self+other

    def __neg__(self):
        return ThinSpatialVector(self.type, self.base_frame, -self.vec)

    def __sub__(self,other):
        return self + (-other)

    def __rsub__(self,other):
        return other+(-self)

    @property
    def x(self):
        return self.vec[0,0]
    @property
    def y(self):
        return self.vec[1,0]
    @property
    def z(self):
        return self.vec[2,0]
    @property
    def rx(self):
        return self.vec[3,0]
    @property
    def ry(self):
        return self.vec[4,0]
    @property
    def rz(self):
        return self.vec[5,0]
    @property
    def lin(self):
        return self.vec[0:3,:]
    @property
    def ang(self):
        return self.vec[3:6,:]

    def __repr__(self):
        if self.type==GU_VG_FORCE:
            return "[%s]force:<"%self.base_frame+standard_mat_repr(self.vec.T)+".T>"
        return "[%s]motion:<"%self.base_frame+standard_mat_repr(self.vec.T)+".T>"
def evaluate_acceleration(a,v,p):
    assert v.type==GU_VG_MOTION
    assert a.type==GU_VG_MOTION
    Frame.check(a,v)
    return a.eval(p)+cross(v.ang).dot(v.eval(p))
class ThinSpatialMapping(object):
    def __init__(self, base_type, base_frame, input_type, input_frame, mat):
        assert mat.shape==(6,6)
        assert base_type==GU_VG_FORCE or base_type==GU_VG_MOTION
        assert input_type==GU_VG_FORCE or input_type==GU_VG_MOTION
        self.base_type=base_type
        self.base_frame=base_frame
        self.input_type = input_type
        self.input_frame = input_frame
        self.mat=np.array(mat)

    @classmethod
    def new_inertia(cls,frame,mat):
        return cls(GU_VG_FORCE,frame,GU_VG_FORCE,frame,mat)
    @classmethod
    def new_inverse_inertia(cls,frame,mat):
        return cls(GU_VG_MOTION,frame, GU_VG_MOTION,frame,mat)
    @classmethod
    def new_motion_transform(cls,base_frame,frame,mat):
        return cls(GU_VG_MOTION,base_frame,GU_VG_FORCE,frame,mat)
    @classmethod
    def new_force_transform(cls,base_frame,frame, mat):
        return cls(GU_VG_FORCE,base_frame,GU_VG_MOTION,frame, mat)

    def __mul__(self,other):
        if isinstance(other, ThinSpatialMapping):
            self.input_frame.assert_match(other.base_frame)
            assert self.input_type != other.base_type
            return ThinSpatialMapping(self.base_type, self.base_frame, other.input_type, other.input_frame,
                self.mat.dot(other.mat))
        if isinstance(other, ThinSpatialVector):
            self.input_frame.assert_match(other.base_frame)
            assert self.input_type != other.type
            return ThinSpatialVector(self.base_type, self.base_frame, self.mat.dot(other.vec))
        return ThinSpatialMapping(self.base_type,self.base_frame, self.input_type,self.input_frame,
            self.mat*other)

    def __rmul__(self,other):
        return ThinSpatialMapping(self.base_type,self.base_frame, self.input_type,self.input_frame,
            other*self.mat)

    def __add__(self,other):
        if isinstance(other, ThinSpatialMapping):
            self.input_frame.assert_match(other.input_frame)
            Frame.check(self,other)
            assert self.base_type == other.base_type
            assert self.input_type == other.input_type
            return ThinSpatialMapping(self.base_type,self.base_frame, self.input_type,self.input_frame,
            self.mat+other.mat)
        return ThinSpatialMapping(self.base_type,self.base_frame, self.input_type,self.input_frame,
            self.mat+other)

    def __sub__(self,other):
        if isinstance(other, ThinSpatialMapping):
            self.input_frame.assert_match(other.input_frame)
            Frame.check(self,other)
            assert self.base_type == other.base_type
            assert self.input_type == other.input_type
            return ThinSpatialMapping(self.base_type,self.base_frame, self.input_type,self.input_frame,
            self.mat-other.mat)
        return ThinSpatialMapping(self.base_type,self.base_frame, self.input_type,self.input_frame,
            self.mat-other)

    def __radd__(self,other):
        return ThinSpatialMapping(self.base_type,self.base_frame, self.input_type,self.input_frame,
            other+self.mat)

    def __rsub__(self,other):
        return ThinSpatialMapping(self.base_type,self.base_frame, self.input_type,self.input_frame,
            other-self.mat)

    def __neg__(self):
        return ThinSpatialMapping(self.base_type,self.base_frame, self.input_type,self.input_frame,
            -self.mat)

    @property 
    def T(self):
        return ThinSpatialMapping(self.input_type,self.input_frame,self.base_type,self.base_frame, 
            self.mat.T)

    def __invert__(self):
        return ThinSpatialMapping(not self.input_type,self.input_frame,not self.base_type,self.base_frame, 
            np.linalg.inv(self.mat))

    def __repr__(self):
        typ={(GU_VG_FORCE,GU_VG_FORCE):"inertia",
         (GU_VG_FORCE,GU_VG_MOTION):"force-transform",
         (GU_VG_MOTION,GU_VG_MOTION):"inverse-inertia",
         (GU_VG_MOTION,GU_VG_FORCE):"motion-transform"}[(self.base_type,self.input_type)]
        return "\t<<\t"+"\t[%s]"%self.base_frame+typ + "[%s]\n"%self.input_frame+ standard_mat_repr(self.mat)+"\n\t>>"

def spatial_motion_cross_product(spatial_vector):
    lin_cross=cross(spatial_vector.vec[0:3,:])
    rot_cross=cross(spatial_vector.vec[3:6,:])
    mat = np.bmat([[rot_cross, lin_cross],[np.zeros((3,3)),rot_cross]])
    return ThinSpatialMapping.new_motion_transform(spatial_vector.base_frame,spatial_vector.base_frame,mat)

def spatial_force_cross_product(spatial_vector):
    lin_cross=cross(spatial_vector.vec[0:3,:])
    rot_cross=cross(spatial_vector.vec[3:6,:])
    mat = np.bmat([[rot_cross, np.zeros((3,3))],[lin_cross,rot_cross]])
    return ThinSpatialMapping.new_force_transform(spatial_vector.base_frame,spatial_vector.base_frame,mat)

def assert_spatial_vectors_equal(vec1, vec2, tol=1e-7):
    b=vec1-vec2
    for i in range(0,6):
        assert abs(b.vec[i,0])<tol

setattr(ThinSpatialVector,'m_cross',spatial_motion_cross_product)
setattr(ThinSpatialVector,'f_cross',spatial_force_cross_product)
setattr(ThinSpatialVector,'assert_equal',assert_spatial_vectors_equal)

if __name__=="__main__":
    W=Frame('world')
    A=ThinSpatialVector.new_force(W,np.array([[0.0,0.0,1.0,0.0,0.0,0.0]]).T)
    A.assert_equal(A)
    B=ThinSpatialVector.new_motion(W,np.array([[0.0,1.0,0.0,0.0,0.0,0.0]]).T)
    B.assert_equal(B)
    print A
    print B
    print (-A)
    print A+A
    print B+B
    bIb=ThinSpatialMapping.new_inertia(W,10*np.eye(6,6))
    print bIb+bIb
    print ~bIb
    print bIb*B
    print "hye"
    print A.f_cross()
    print A.m_cross()