import numpy as np 
from math import pow, sqrt, pi
from gu.math import Quaternion, quat_exponent, cross
from gu.math.frames import *
from gu.math.thin_spatial_algebra import *

if __name__=="__main__":
    a = Quaternion(1.0,2.0,3.0,4.0)
    print a, a.norm
    print a.normalize()
    print a.normalize().DCM()

class FramePoint(object):
    def __init__(self, base_frame, vec):
        self.base_frame=base_frame
        assert vec.shape==(3,1)
        self.vec = vec

    def add_vector(self,other):
        Frame.check(self,other)
        return FramePoint(self.base_frame, self.vec+other.vec)
        
    def __add__(self, other):
        if isinstance(other, FramePoint):
            raise NonsenseException("Points are never added. Perhaps you meant to add a point and a vector?")
        if isinstance(other, FrameVector):
            return self.add_vector(other)
        raise NotImplemented("Class un-addable to FramePoint objects: %s "% type(other))

    # def __sub__(self,other):

    def __repr__(self):
        return "[%s](%.5f,%.5f,%.5f)"%(self.base_frame.name,self.vec[0,0],self.vec[1,0],self.vec[2,0])

class FrameVector(object):
    def __init__(self, base_frame, vec):
        self.base_frame=base_frame
        assert vec.shape==(3,1)
        self.vec=vec
    def add_vector(self,other):
        Frame.check(self,other)
        return FrameVector(self.base_frame, self.vec+other.vec)
    def add_point(self,other):
        Frame.check(self,other)
        return FramePoint(self.base_frame, self.vec+other.vec)
    def cross(self, other):
        Frame.check(self,other)
        return FrameVector(self.base_frame, cross(self.vec).dot(other.vec) )
    def __add__(self,other):
        if isinstance(other, FramePoint):
            return self.add_point(other)
        if isinstance(other, FrameVector):
            return self.add_vector(other)
        raise NotImplemented("Class un-addable to FrameVector objects: %s "% type(other))
    def __repr__(self):
        return "[%s]<%.5f,%.5f,%.5f>"%(self.base_frame.name,self.vec[0,0],self.vec[1,0],self.vec[2,0])

    
def force_from_point_force(point, force):
    Frame.check(point,force)
    return ThinSpatialVector.new_force(
        point.base_frame, np.bmat([[force.vec],[cross(point.vec).dot(force.vec)]]))

if __name__=="__main__":
    print "spatial vector test"
    W=Frame("world")
    B=Frame("body")
    p=FramePoint(W,np.array([[1.0,0.0,0.0]]).T)
    f=FrameVector(W,np.array([[0.0,1.0,0.0]]).T)
    sf=SpatialForceVector.from_point_force(p,f)
    Ia=ArticulatedBodyInertia(W,10.0*np.eye(6,6))
    print sf
    print Ia
    print Ia.inverse()
    print Ia.inverse().inverse()


if __name__=="__main__":
    worldFrame=Frame("world")
    bodyFrame=Frame("body")
    print worldFrame
    w_a=FramePoint(worldFrame, np.array([[0.0,1.0,0.0]]).T)
    w_c=FrameVector(worldFrame, np.array([[1.0,0.0,0.0]]).T)
    b_b=FramePoint(bodyFrame, np.array([[2.3,3.0,0.2]]).T)
    b_d=FrameVector(bodyFrame, np.array([[2.3,3.0,0.2]]).T)
    b_e=FrameVector(bodyFrame, np.array([[2.0,10.0,0.0]]).T)
    print w_a,w_c,b_b
    try:
        w_a+b_b
    except NonsenseException as e:
        print "expected Exception:",e
    try:
        w_a+b_d
    except FrameException as e:
        print "expected Exception:",e
    w_d=w_a+w_c
    print w_d,type(w_d)
    b_f = b_d+b_e
    print b_f,type(b_f)


