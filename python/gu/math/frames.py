
class FrameException(Exception):
    def __init__(self,A,B):
        self.a=A.name
        self.b=B.name
    def __str__(self):
        return "FrameException: %s is not %s"%(self.a, self.b)

class NonsenseException(Exception):
    def __init__(self, reason):
        self.reason=reason
    def __str__(self):
        return "NonsenseException:%s"%self.reason

class Frame(object):
    def __init__(self,name):
        self.name=name
    def __repr__(self):
        return "Frame:%s"%self.name
    def assert_match(self,other):
        if not other is self:
            raise FrameException(self,other)
    @staticmethod
    def check(a,b):
        a.base_frame.assert_match(b.base_frame)