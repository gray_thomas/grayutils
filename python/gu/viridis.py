


<!DOCTYPE html>
<html lang="en" class=" is-copy-enabled">
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta name="viewport" content="width=1020">
    
    
    <title>colormap/option_d.py at master · BIDS/colormap · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub">
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png">
    <meta property="fb:app_id" content="1401488693436528">

      <meta content="@github" name="twitter:site" /><meta content="summary" name="twitter:card" /><meta content="BIDS/colormap" name="twitter:title" /><meta content="colormap - Colormap recommendation" name="twitter:description" /><meta content="https://avatars1.githubusercontent.com/u/8398105?v=3&amp;s=400" name="twitter:image:src" />
      <meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="https://avatars1.githubusercontent.com/u/8398105?v=3&amp;s=400" property="og:image" /><meta content="BIDS/colormap" property="og:title" /><meta content="https://github.com/BIDS/colormap" property="og:url" /><meta content="colormap - Colormap recommendation" property="og:description" />
      <meta name="browser-stats-url" content="https://api.github.com/_private/browser/stats">
    <meta name="browser-errors-url" content="https://api.github.com/_private/browser/errors">
    <link rel="assets" href="https://assets-cdn.github.com/">
    
    <meta name="pjax-timeout" content="1000">
    

    <meta name="msapplication-TileImage" content="/windows-tile.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="selected-link" value="repo_source" data-pjax-transient>

        <meta name="google-analytics" content="UA-3769691-2">

    <meta content="collector.githubapp.com" name="octolytics-host" /><meta content="collector-cdn.github.com" name="octolytics-script-host" /><meta content="github" name="octolytics-app-id" /><meta content="4672C349:4141:85BFA2:55B7ABE4" name="octolytics-dimension-request_id" />
    
    <meta content="Rails, view, blob#show" data-pjax-transient="true" name="analytics-event" />
    <meta class="js-ga-set" name="dimension1" content="Logged Out">
      <meta class="js-ga-set" name="dimension4" content="Current repo nav">
    <meta name="is-dotcom" content="true">
        <meta name="hostname" content="github.com">
    <meta name="user-login" content="">

      <link rel="icon" sizes="any" mask href="https://assets-cdn.github.com/pinned-octocat.svg">
      <meta name="theme-color" content="#4078c0">
      <link rel="icon" type="image/x-icon" href="https://assets-cdn.github.com/favicon.ico">

    <!-- </textarea> --><!-- '"` --><meta content="authenticity_token" name="csrf-param" />
<meta content="UP9xPBl97h8LHOAO4F8MYJ2bT1W3xA06a4vR10ty2PaINgO8zYPLtX+AYmOYJQ6h0qkwe+DcQM2p66QE0JzkEA==" name="csrf-token" />
    

    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/github/index-2de9f2a0dcbebdb929c6fddf7fd94aa06a736743201b9c57db4c19cb74757211.css" media="all" rel="stylesheet" />
    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/github2/index-feb5b225655f913840ce7b017a801ec6d6677ccb67b2965548c4965063ac724c.css" media="all" rel="stylesheet" />
    
    


    <meta http-equiv="x-pjax-version" content="e23fc3be5b07883ddbcb7f8f44ee77e2">

      
  <meta name="description" content="colormap - Colormap recommendation">
  <meta name="go-import" content="github.com/BIDS/colormap git https://github.com/BIDS/colormap.git">

  <meta content="8398105" name="octolytics-dimension-user_id" /><meta content="BIDS" name="octolytics-dimension-user_login" /><meta content="36763792" name="octolytics-dimension-repository_id" /><meta content="BIDS/colormap" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="36763792" name="octolytics-dimension-repository_network_root_id" /><meta content="BIDS/colormap" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/BIDS/colormap/commits/master.atom" rel="alternate" title="Recent Commits to colormap:master" type="application/atom+xml">

  </head>


  <body class="logged_out  env-production linux vis-public page-blob">
    <a href="#start-of-content" tabindex="1" class="accessibility-aid js-skip-to-content">Skip to content</a>
    <div class="wrapper">
      
      
      



        
        <div class="header header-logged-out" role="banner">
  <div class="container clearfix">

    <a class="header-logo-wordmark" href="https://github.com/" data-ga-click="(Logged out) Header, go to homepage, icon:logo-wordmark">
      <span class="mega-octicon octicon-logo-github"></span>
    </a>

    <div class="header-actions" role="navigation">
        <a class="btn btn-primary" href="/join" data-ga-click="(Logged out) Header, clicked Sign up, text:sign-up">Sign up</a>
      <a class="btn" href="/login?return_to=%2FBIDS%2Fcolormap%2Fblob%2Fmaster%2Foption_d.py" data-ga-click="(Logged out) Header, clicked Sign in, text:sign-in">Sign in</a>
    </div>

    <div class="site-search repo-scope js-site-search" role="search">
      <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/BIDS/colormap/search" class="js-site-search-form" data-global-search-url="/search" data-repo-search-url="/BIDS/colormap/search" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
  <label class="js-chromeless-input-container form-control">
    <div class="scope-badge">This repository</div>
    <input type="text"
      class="js-site-search-focus js-site-search-field is-clearable chromeless-input"
      data-hotkey="s"
      name="q"
      placeholder="Search"
      data-global-scope-placeholder="Search GitHub"
      data-repo-scope-placeholder="Search"
      tabindex="1"
      autocapitalize="off">
  </label>
</form>
    </div>

      <ul class="header-nav left" role="navigation">
          <li class="header-nav-item">
            <a class="header-nav-link" href="/explore" data-ga-click="(Logged out) Header, go to explore, text:explore">Explore</a>
          </li>
          <li class="header-nav-item">
            <a class="header-nav-link" href="/features" data-ga-click="(Logged out) Header, go to features, text:features">Features</a>
          </li>
          <li class="header-nav-item">
            <a class="header-nav-link" href="https://enterprise.github.com/" data-ga-click="(Logged out) Header, go to enterprise, text:enterprise">Enterprise</a>
          </li>
          <li class="header-nav-item">
            <a class="header-nav-link" href="/blog" data-ga-click="(Logged out) Header, go to blog, text:blog">Blog</a>
          </li>
      </ul>

  </div>
</div>



      <div id="start-of-content" class="accessibility-aid"></div>
          <div class="site" itemscope itemtype="http://schema.org/WebPage">
    <div id="js-flash-container">
      
    </div>
    <div class="pagehead repohead instapaper_ignore readability-menu ">
      <div class="container">

        <div class="clearfix">
          
<ul class="pagehead-actions">

  <li>
      <a href="/login?return_to=%2FBIDS%2Fcolormap"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to watch a repository" rel="nofollow">
    <span class="octicon octicon-eye"></span>
    Watch
  </a>
  <a class="social-count" href="/BIDS/colormap/watchers">
    11
  </a>

  </li>

  <li>
      <a href="/login?return_to=%2FBIDS%2Fcolormap"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to star a repository" rel="nofollow">
    <span class="octicon octicon-star"></span>
    Star
  </a>

    <a class="social-count js-social-count" href="/BIDS/colormap/stargazers">
      14
    </a>

  </li>

    <li>
      <a href="/login?return_to=%2FBIDS%2Fcolormap"
        class="btn btn-sm btn-with-count tooltipped tooltipped-n"
        aria-label="You must be signed in to fork a repository" rel="nofollow">
        <span class="octicon octicon-repo-forked"></span>
        Fork
      </a>
      <a href="/BIDS/colormap/network" class="social-count">
        3
      </a>
    </li>
</ul>

          <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
            <span class="mega-octicon octicon-repo"></span>
            <span class="author"><a href="/BIDS" class="url fn" itemprop="url" rel="author"><span itemprop="title">BIDS</span></a></span><!--
         --><span class="path-divider">/</span><!--
         --><strong><a href="/BIDS/colormap" data-pjax="#js-repo-pjax-container">colormap</a></strong>

            <span class="page-context-loader">
              <img alt="" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
            </span>

          </h1>
        </div>

      </div>
    </div>

      <div class="container">
        <div class="repository-with-sidebar repo-container new-discussion-timeline ">
          <div class="repository-sidebar clearfix">
              

<nav class="sunken-menu repo-nav js-repo-nav js-sidenav-container-pjax js-octicon-loaders"
     role="navigation"
     data-pjax="#js-repo-pjax-container"
     data-issue-count-url="/BIDS/colormap/issues/counts">
  <ul class="sunken-menu-group">
    <li class="tooltipped tooltipped-w" aria-label="Code">
      <a href="/BIDS/colormap" aria-label="Code" aria-selected="true" class="js-selected-navigation-item selected sunken-menu-item" data-hotkey="g c" data-selected-links="repo_source repo_downloads repo_commits repo_releases repo_tags repo_branches /BIDS/colormap">
        <span class="octicon octicon-code"></span> <span class="full-word">Code</span>
        <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>    </li>

      <li class="tooltipped tooltipped-w" aria-label="Issues">
        <a href="/BIDS/colormap/issues" aria-label="Issues" class="js-selected-navigation-item sunken-menu-item" data-hotkey="g i" data-selected-links="repo_issues repo_labels repo_milestones /BIDS/colormap/issues">
          <span class="octicon octicon-issue-opened"></span> <span class="full-word">Issues</span>
          <span class="js-issue-replace-counter"></span>
          <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

    <li class="tooltipped tooltipped-w" aria-label="Pull requests">
      <a href="/BIDS/colormap/pulls" aria-label="Pull requests" class="js-selected-navigation-item sunken-menu-item" data-hotkey="g p" data-selected-links="repo_pulls /BIDS/colormap/pulls">
          <span class="octicon octicon-git-pull-request"></span> <span class="full-word">Pull requests</span>
          <span class="js-pull-replace-counter"></span>
          <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>    </li>

  </ul>
  <div class="sunken-menu-separator"></div>
  <ul class="sunken-menu-group">

    <li class="tooltipped tooltipped-w" aria-label="Pulse">
      <a href="/BIDS/colormap/pulse" aria-label="Pulse" class="js-selected-navigation-item sunken-menu-item" data-selected-links="pulse /BIDS/colormap/pulse">
        <span class="octicon octicon-pulse"></span> <span class="full-word">Pulse</span>
        <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>    </li>

    <li class="tooltipped tooltipped-w" aria-label="Graphs">
      <a href="/BIDS/colormap/graphs" aria-label="Graphs" class="js-selected-navigation-item sunken-menu-item" data-selected-links="repo_graphs repo_contributors /BIDS/colormap/graphs">
        <span class="octicon octicon-graph"></span> <span class="full-word">Graphs</span>
        <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif" width="16" />
</a>    </li>
  </ul>


</nav>

                <div class="only-with-full-nav">
                    
<div class="js-clone-url clone-url open"
  data-protocol-type="http">
  <h3><span class="text-emphasized">HTTPS</span> clone URL</h3>
  <div class="input-group js-zeroclipboard-container">
    <input type="text" class="input-mini input-monospace js-url-field js-zeroclipboard-target"
           value="https://github.com/BIDS/colormap.git" readonly="readonly" aria-label="HTTPS clone URL">
    <span class="input-group-button">
      <button aria-label="Copy to clipboard" class="js-zeroclipboard btn btn-sm zeroclipboard-button tooltipped tooltipped-s" data-copied-hint="Copied!" type="button"><span class="octicon octicon-clippy"></span></button>
    </span>
  </div>
</div>

  
<div class="js-clone-url clone-url "
  data-protocol-type="subversion">
  <h3><span class="text-emphasized">Subversion</span> checkout URL</h3>
  <div class="input-group js-zeroclipboard-container">
    <input type="text" class="input-mini input-monospace js-url-field js-zeroclipboard-target"
           value="https://github.com/BIDS/colormap" readonly="readonly" aria-label="Subversion checkout URL">
    <span class="input-group-button">
      <button aria-label="Copy to clipboard" class="js-zeroclipboard btn btn-sm zeroclipboard-button tooltipped tooltipped-s" data-copied-hint="Copied!" type="button"><span class="octicon octicon-clippy"></span></button>
    </span>
  </div>
</div>



  <div class="clone-options">You can clone with
    <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/users/set_protocol?protocol_selector=http&amp;protocol_type=clone" class="inline-form js-clone-selector-form " data-remote="true" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="j4jwBOrG9u0JKig/GMyAMokL+T/CEJHVI/MR8hoHSXymJSjlUIG72MOW4oZAU2QejXV7A2W3jz0FTMaac95g9w==" /></div><button class="btn-link js-clone-selector" data-protocol="http" type="submit">HTTPS</button></form> or <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/users/set_protocol?protocol_selector=subversion&amp;protocol_type=clone" class="inline-form js-clone-selector-form " data-remote="true" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="Et/wrtCBuoG2EKJlhOtUeHu2xRCB/cj1X36uSZ2n50ymGiZf4v+wqLe0M/srNPoGDzKWEocla9qiIJJ9gZ4HKA==" /></div><button class="btn-link js-clone-selector" data-protocol="subversion" type="submit">Subversion</button></form>.
    <a href="https://help.github.com/articles/which-remote-url-should-i-use" class="help tooltipped tooltipped-n" aria-label="Get help on which URL is right for you.">
      <span class="octicon octicon-question"></span>
    </a>
  </div>


                  <a href="/BIDS/colormap/archive/master.zip"
                     class="btn btn-sm sidebar-button"
                     aria-label="Download the contents of BIDS/colormap as a zip file"
                     title="Download the contents of BIDS/colormap as a zip file"
                     rel="nofollow">
                    <span class="octicon octicon-cloud-download"></span>
                    Download ZIP
                  </a>
                </div>
          </div>
          <div id="js-repo-pjax-container" class="repository-content context-loader-container" data-pjax-container>

            

<a href="/BIDS/colormap/blob/bc7d2700f150afb8a6fa8df8c49c9a7f18e84435/option_d.py" class="hidden js-permalink-shortcut" data-hotkey="y">Permalink</a>

<!-- blob contrib key: blob_contributors:v21:1d1995ff43b10a0fc6178f193c8ad24f -->

  <div class="file-navigation js-zeroclipboard-container">
    
<div class="select-menu js-menu-container js-select-menu left">
  <span class="btn btn-sm select-menu-button js-menu-target css-truncate" data-hotkey="w"
    data-ref="master"
    title="master"
    role="button" aria-label="Switch branches or tags" tabindex="0" aria-haspopup="true">
    <i>Branch:</i>
    <span class="js-select-button css-truncate-target">master</span>
  </span>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax aria-hidden="true">

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <span class="select-menu-title">Switch branches/tags</span>
        <span class="octicon octicon-x js-menu-close" role="button" aria-label="Close"></span>
      </div>

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" data-filter-placeholder="Filter branches/tags" class="js-select-menu-tab" role="tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" data-filter-placeholder="Find a tag…" class="js-select-menu-tab" role="tab">Tags</a>
            </li>
          </ul>
        </div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches" role="menu">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/BIDS/colormap/blob/gh-pages/option_d.py"
               data-name="gh-pages"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="gh-pages">
                gh-pages
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open selected"
               href="/BIDS/colormap/blob/master/option_d.py"
               data-name="master"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="master">
                master
              </span>
            </a>
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div>

    </div>
  </div>
</div>

    <div class="btn-group right">
      <a href="/BIDS/colormap/find/master"
            class="js-show-file-finder btn btn-sm empty-icon tooltipped tooltipped-nw"
            data-pjax
            data-hotkey="t"
            aria-label="Quickly jump between files">
        <span class="octicon octicon-list-unordered"></span>
      </a>
      <button aria-label="Copy file path to clipboard" class="js-zeroclipboard btn btn-sm zeroclipboard-button tooltipped tooltipped-s" data-copied-hint="Copied!" type="button"><span class="octicon octicon-clippy"></span></button>
    </div>

    <div class="breadcrumb js-zeroclipboard-target">
      <span class="repo-root js-repo-root"><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/BIDS/colormap" class="" data-branch="master" data-pjax="true" itemscope="url"><span itemprop="title">colormap</span></a></span></span><span class="separator">/</span><strong class="final-path">option_d.py</strong>
    </div>
  </div>


  <div class="commit file-history-tease">
    <div class="file-history-tease-header">
        <img alt="@stefanv" class="avatar" height="24" src="https://avatars1.githubusercontent.com/u/45071?v=3&amp;s=48" width="24" />
        <span class="author"><a href="/stefanv" rel="contributor">stefanv</a></span>
        <time datetime="2015-06-03T21:56:52Z" is="relative-time">Jun 3, 2015</time>
        <div class="commit-title">
            <a href="/BIDS/colormap/commit/ffdc2050b034fce28190aadb33d556788bc26ba7" class="message" data-pjax="true" title="Add option d">Add option d</a>
        </div>
    </div>

    <div class="participation">
      <p class="quickstat">
        <a href="#blob_contributors_box" rel="facebox">
          <strong>1</strong>
           contributor
        </a>
      </p>
      
    </div>
    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list">
          <li class="facebox-user-list-item">
            <img alt="@stefanv" height="24" src="https://avatars1.githubusercontent.com/u/45071?v=3&amp;s=48" width="24" />
            <a href="/stefanv">stefanv</a>
          </li>
      </ul>
    </div>
  </div>

<div class="file">
  <div class="file-header">
    <div class="file-actions">

      <div class="btn-group">
        <a href="/BIDS/colormap/raw/master/option_d.py" class="btn btn-sm " id="raw-url">Raw</a>
          <a href="/BIDS/colormap/blame/master/option_d.py" class="btn btn-sm js-update-url-with-hash">Blame</a>
        <a href="/BIDS/colormap/commits/master/option_d.py" class="btn btn-sm " rel="nofollow">History</a>
      </div>


          <button type="button" class="octicon-btn disabled tooltipped tooltipped-n" aria-label="You must be signed in to make or propose changes">
            <span class="octicon octicon-pencil"></span>
          </button>

        <button type="button" class="octicon-btn octicon-btn-danger disabled tooltipped tooltipped-n" aria-label="You must be signed in to make or propose changes">
          <span class="octicon octicon-trashcan"></span>
        </button>
    </div>

    <div class="file-info">
        283 lines (275 sloc)
        <span class="file-info-divider"></span>
      13.226 kB
    </div>
  </div>
  

  <div class="blob-wrapper data type-python">
      <table class="highlight tab-size js-file-line-container" data-tab-size="8">
      <tr>
        <td id="L1" class="blob-num js-line-number" data-line-number="1"></td>
        <td id="LC1" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L2" class="blob-num js-line-number" data-line-number="2"></td>
        <td id="LC2" class="blob-code blob-code-inner js-file-line"><span class="pl-k">from</span> matplotlib.colors <span class="pl-k">import</span> LinearSegmentedColormap</td>
      </tr>
      <tr>
        <td id="L3" class="blob-num js-line-number" data-line-number="3"></td>
        <td id="LC3" class="blob-code blob-code-inner js-file-line"><span class="pl-k">from</span> numpy <span class="pl-k">import</span> nan, inf</td>
      </tr>
      <tr>
        <td id="L4" class="blob-num js-line-number" data-line-number="4"></td>
        <td id="LC4" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L5" class="blob-num js-line-number" data-line-number="5"></td>
        <td id="LC5" class="blob-code blob-code-inner js-file-line"><span class="pl-c"># Used to reconstruct the colormap in pycam02ucs.cm.viscm</span></td>
      </tr>
      <tr>
        <td id="L6" class="blob-num js-line-number" data-line-number="6"></td>
        <td id="LC6" class="blob-code blob-code-inner js-file-line">parameters <span class="pl-k">=</span> {<span class="pl-s"><span class="pl-pds">&#39;</span>xp<span class="pl-pds">&#39;</span></span>: [<span class="pl-c1">22.674387857633945</span>, <span class="pl-c1">11.221508276482126</span>, <span class="pl-k">-</span><span class="pl-c1">14.356589454756971</span>, <span class="pl-k">-</span><span class="pl-c1">47.18817758739222</span>, <span class="pl-k">-</span><span class="pl-c1">34.59001004812521</span>, <span class="pl-k">-</span><span class="pl-c1">6.0516291196352654</span>],</td>
      </tr>
      <tr>
        <td id="L7" class="blob-num js-line-number" data-line-number="7"></td>
        <td id="LC7" class="blob-code blob-code-inner js-file-line">              <span class="pl-s"><span class="pl-pds">&#39;</span>yp<span class="pl-pds">&#39;</span></span>: [<span class="pl-k">-</span><span class="pl-c1">20.102530541012214</span>, <span class="pl-k">-</span><span class="pl-c1">33.08246073298429</span>, <span class="pl-k">-</span><span class="pl-c1">42.24476439790574</span>, <span class="pl-k">-</span><span class="pl-c1">5.595549738219887</span>, <span class="pl-c1">42.5065445026178</span>, <span class="pl-c1">40.13395157135497</span>],</td>
      </tr>
      <tr>
        <td id="L8" class="blob-num js-line-number" data-line-number="8"></td>
        <td id="LC8" class="blob-code blob-code-inner js-file-line">              <span class="pl-s"><span class="pl-pds">&#39;</span>min_JK<span class="pl-pds">&#39;</span></span>: <span class="pl-c1">18.8671875</span>,</td>
      </tr>
      <tr>
        <td id="L9" class="blob-num js-line-number" data-line-number="9"></td>
        <td id="LC9" class="blob-code blob-code-inner js-file-line">              <span class="pl-s"><span class="pl-pds">&#39;</span>max_JK<span class="pl-pds">&#39;</span></span>: <span class="pl-c1">92.5</span>}</td>
      </tr>
      <tr>
        <td id="L10" class="blob-num js-line-number" data-line-number="10"></td>
        <td id="LC10" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L11" class="blob-num js-line-number" data-line-number="11"></td>
        <td id="LC11" class="blob-code blob-code-inner js-file-line">cm_data <span class="pl-k">=</span> [[ <span class="pl-c1">0.26700401</span>,  <span class="pl-c1">0.00487433</span>,  <span class="pl-c1">0.32941519</span>],</td>
      </tr>
      <tr>
        <td id="L12" class="blob-num js-line-number" data-line-number="12"></td>
        <td id="LC12" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.26851048</span>,  <span class="pl-c1">0.00960483</span>,  <span class="pl-c1">0.33542652</span>],</td>
      </tr>
      <tr>
        <td id="L13" class="blob-num js-line-number" data-line-number="13"></td>
        <td id="LC13" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.26994384</span>,  <span class="pl-c1">0.01462494</span>,  <span class="pl-c1">0.34137895</span>],</td>
      </tr>
      <tr>
        <td id="L14" class="blob-num js-line-number" data-line-number="14"></td>
        <td id="LC14" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27130489</span>,  <span class="pl-c1">0.01994186</span>,  <span class="pl-c1">0.34726862</span>],</td>
      </tr>
      <tr>
        <td id="L15" class="blob-num js-line-number" data-line-number="15"></td>
        <td id="LC15" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27259384</span>,  <span class="pl-c1">0.02556309</span>,  <span class="pl-c1">0.35309303</span>],</td>
      </tr>
      <tr>
        <td id="L16" class="blob-num js-line-number" data-line-number="16"></td>
        <td id="LC16" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27380934</span>,  <span class="pl-c1">0.03149748</span>,  <span class="pl-c1">0.35885256</span>],</td>
      </tr>
      <tr>
        <td id="L17" class="blob-num js-line-number" data-line-number="17"></td>
        <td id="LC17" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27495242</span>,  <span class="pl-c1">0.03775181</span>,  <span class="pl-c1">0.36454323</span>],</td>
      </tr>
      <tr>
        <td id="L18" class="blob-num js-line-number" data-line-number="18"></td>
        <td id="LC18" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27602238</span>,  <span class="pl-c1">0.04416723</span>,  <span class="pl-c1">0.37016418</span>],</td>
      </tr>
      <tr>
        <td id="L19" class="blob-num js-line-number" data-line-number="19"></td>
        <td id="LC19" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.2770184</span> ,  <span class="pl-c1">0.05034437</span>,  <span class="pl-c1">0.37571452</span>],</td>
      </tr>
      <tr>
        <td id="L20" class="blob-num js-line-number" data-line-number="20"></td>
        <td id="LC20" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27794143</span>,  <span class="pl-c1">0.05632444</span>,  <span class="pl-c1">0.38119074</span>],</td>
      </tr>
      <tr>
        <td id="L21" class="blob-num js-line-number" data-line-number="21"></td>
        <td id="LC21" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27879067</span>,  <span class="pl-c1">0.06214536</span>,  <span class="pl-c1">0.38659204</span>],</td>
      </tr>
      <tr>
        <td id="L22" class="blob-num js-line-number" data-line-number="22"></td>
        <td id="LC22" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.2795655</span> ,  <span class="pl-c1">0.06783587</span>,  <span class="pl-c1">0.39191723</span>],</td>
      </tr>
      <tr>
        <td id="L23" class="blob-num js-line-number" data-line-number="23"></td>
        <td id="LC23" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28026658</span>,  <span class="pl-c1">0.07341724</span>,  <span class="pl-c1">0.39716349</span>],</td>
      </tr>
      <tr>
        <td id="L24" class="blob-num js-line-number" data-line-number="24"></td>
        <td id="LC24" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28089358</span>,  <span class="pl-c1">0.07890703</span>,  <span class="pl-c1">0.40232944</span>],</td>
      </tr>
      <tr>
        <td id="L25" class="blob-num js-line-number" data-line-number="25"></td>
        <td id="LC25" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28144581</span>,  <span class="pl-c1">0.0843197</span> ,  <span class="pl-c1">0.40741404</span>],</td>
      </tr>
      <tr>
        <td id="L26" class="blob-num js-line-number" data-line-number="26"></td>
        <td id="LC26" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28192358</span>,  <span class="pl-c1">0.08966622</span>,  <span class="pl-c1">0.41241521</span>],</td>
      </tr>
      <tr>
        <td id="L27" class="blob-num js-line-number" data-line-number="27"></td>
        <td id="LC27" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28232739</span>,  <span class="pl-c1">0.09495545</span>,  <span class="pl-c1">0.41733086</span>],</td>
      </tr>
      <tr>
        <td id="L28" class="blob-num js-line-number" data-line-number="28"></td>
        <td id="LC28" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28265633</span>,  <span class="pl-c1">0.10019576</span>,  <span class="pl-c1">0.42216032</span>],</td>
      </tr>
      <tr>
        <td id="L29" class="blob-num js-line-number" data-line-number="29"></td>
        <td id="LC29" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28291049</span>,  <span class="pl-c1">0.10539345</span>,  <span class="pl-c1">0.42690202</span>],</td>
      </tr>
      <tr>
        <td id="L30" class="blob-num js-line-number" data-line-number="30"></td>
        <td id="LC30" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28309095</span>,  <span class="pl-c1">0.11055307</span>,  <span class="pl-c1">0.43155375</span>],</td>
      </tr>
      <tr>
        <td id="L31" class="blob-num js-line-number" data-line-number="31"></td>
        <td id="LC31" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28319704</span>,  <span class="pl-c1">0.11567966</span>,  <span class="pl-c1">0.43611482</span>],</td>
      </tr>
      <tr>
        <td id="L32" class="blob-num js-line-number" data-line-number="32"></td>
        <td id="LC32" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28322882</span>,  <span class="pl-c1">0.12077701</span>,  <span class="pl-c1">0.44058404</span>],</td>
      </tr>
      <tr>
        <td id="L33" class="blob-num js-line-number" data-line-number="33"></td>
        <td id="LC33" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28318684</span>,  <span class="pl-c1">0.12584799</span>,  <span class="pl-c1">0.44496</span>   ],</td>
      </tr>
      <tr>
        <td id="L34" class="blob-num js-line-number" data-line-number="34"></td>
        <td id="LC34" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.283072</span>  ,  <span class="pl-c1">0.13089477</span>,  <span class="pl-c1">0.44924127</span>],</td>
      </tr>
      <tr>
        <td id="L35" class="blob-num js-line-number" data-line-number="35"></td>
        <td id="LC35" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28288389</span>,  <span class="pl-c1">0.13592005</span>,  <span class="pl-c1">0.45342734</span>],</td>
      </tr>
      <tr>
        <td id="L36" class="blob-num js-line-number" data-line-number="36"></td>
        <td id="LC36" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28262297</span>,  <span class="pl-c1">0.14092556</span>,  <span class="pl-c1">0.45751726</span>],</td>
      </tr>
      <tr>
        <td id="L37" class="blob-num js-line-number" data-line-number="37"></td>
        <td id="LC37" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28229037</span>,  <span class="pl-c1">0.14591233</span>,  <span class="pl-c1">0.46150995</span>],</td>
      </tr>
      <tr>
        <td id="L38" class="blob-num js-line-number" data-line-number="38"></td>
        <td id="LC38" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28188676</span>,  <span class="pl-c1">0.15088147</span>,  <span class="pl-c1">0.46540474</span>],</td>
      </tr>
      <tr>
        <td id="L39" class="blob-num js-line-number" data-line-number="39"></td>
        <td id="LC39" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28141228</span>,  <span class="pl-c1">0.15583425</span>,  <span class="pl-c1">0.46920128</span>],</td>
      </tr>
      <tr>
        <td id="L40" class="blob-num js-line-number" data-line-number="40"></td>
        <td id="LC40" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28086773</span>,  <span class="pl-c1">0.16077132</span>,  <span class="pl-c1">0.47289909</span>],</td>
      </tr>
      <tr>
        <td id="L41" class="blob-num js-line-number" data-line-number="41"></td>
        <td id="LC41" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28025468</span>,  <span class="pl-c1">0.16569272</span>,  <span class="pl-c1">0.47649762</span>],</td>
      </tr>
      <tr>
        <td id="L42" class="blob-num js-line-number" data-line-number="42"></td>
        <td id="LC42" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27957399</span>,  <span class="pl-c1">0.17059884</span>,  <span class="pl-c1">0.47999675</span>],</td>
      </tr>
      <tr>
        <td id="L43" class="blob-num js-line-number" data-line-number="43"></td>
        <td id="LC43" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27882618</span>,  <span class="pl-c1">0.1754902</span> ,  <span class="pl-c1">0.48339654</span>],</td>
      </tr>
      <tr>
        <td id="L44" class="blob-num js-line-number" data-line-number="44"></td>
        <td id="LC44" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27801236</span>,  <span class="pl-c1">0.18036684</span>,  <span class="pl-c1">0.48669702</span>],</td>
      </tr>
      <tr>
        <td id="L45" class="blob-num js-line-number" data-line-number="45"></td>
        <td id="LC45" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27713437</span>,  <span class="pl-c1">0.18522836</span>,  <span class="pl-c1">0.48989831</span>],</td>
      </tr>
      <tr>
        <td id="L46" class="blob-num js-line-number" data-line-number="46"></td>
        <td id="LC46" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27619376</span>,  <span class="pl-c1">0.19007447</span>,  <span class="pl-c1">0.49300074</span>],</td>
      </tr>
      <tr>
        <td id="L47" class="blob-num js-line-number" data-line-number="47"></td>
        <td id="LC47" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27519116</span>,  <span class="pl-c1">0.1949054</span> ,  <span class="pl-c1">0.49600488</span>],</td>
      </tr>
      <tr>
        <td id="L48" class="blob-num js-line-number" data-line-number="48"></td>
        <td id="LC48" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27412802</span>,  <span class="pl-c1">0.19972086</span>,  <span class="pl-c1">0.49891131</span>],</td>
      </tr>
      <tr>
        <td id="L49" class="blob-num js-line-number" data-line-number="49"></td>
        <td id="LC49" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27300596</span>,  <span class="pl-c1">0.20452049</span>,  <span class="pl-c1">0.50172076</span>],</td>
      </tr>
      <tr>
        <td id="L50" class="blob-num js-line-number" data-line-number="50"></td>
        <td id="LC50" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27182812</span>,  <span class="pl-c1">0.20930306</span>,  <span class="pl-c1">0.50443413</span>],</td>
      </tr>
      <tr>
        <td id="L51" class="blob-num js-line-number" data-line-number="51"></td>
        <td id="LC51" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27059473</span>,  <span class="pl-c1">0.21406899</span>,  <span class="pl-c1">0.50705243</span>],</td>
      </tr>
      <tr>
        <td id="L52" class="blob-num js-line-number" data-line-number="52"></td>
        <td id="LC52" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.26930756</span>,  <span class="pl-c1">0.21881782</span>,  <span class="pl-c1">0.50957678</span>],</td>
      </tr>
      <tr>
        <td id="L53" class="blob-num js-line-number" data-line-number="53"></td>
        <td id="LC53" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.26796846</span>,  <span class="pl-c1">0.22354911</span>,  <span class="pl-c1">0.5120084</span> ],</td>
      </tr>
      <tr>
        <td id="L54" class="blob-num js-line-number" data-line-number="54"></td>
        <td id="LC54" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.26657984</span>,  <span class="pl-c1">0.2282621</span> ,  <span class="pl-c1">0.5143487</span> ],</td>
      </tr>
      <tr>
        <td id="L55" class="blob-num js-line-number" data-line-number="55"></td>
        <td id="LC55" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.2651445</span> ,  <span class="pl-c1">0.23295593</span>,  <span class="pl-c1">0.5165993</span> ],</td>
      </tr>
      <tr>
        <td id="L56" class="blob-num js-line-number" data-line-number="56"></td>
        <td id="LC56" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.2636632</span> ,  <span class="pl-c1">0.23763078</span>,  <span class="pl-c1">0.51876163</span>],</td>
      </tr>
      <tr>
        <td id="L57" class="blob-num js-line-number" data-line-number="57"></td>
        <td id="LC57" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.26213801</span>,  <span class="pl-c1">0.24228619</span>,  <span class="pl-c1">0.52083736</span>],</td>
      </tr>
      <tr>
        <td id="L58" class="blob-num js-line-number" data-line-number="58"></td>
        <td id="LC58" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.26057103</span>,  <span class="pl-c1">0.2469217</span> ,  <span class="pl-c1">0.52282822</span>],</td>
      </tr>
      <tr>
        <td id="L59" class="blob-num js-line-number" data-line-number="59"></td>
        <td id="LC59" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.25896451</span>,  <span class="pl-c1">0.25153685</span>,  <span class="pl-c1">0.52473609</span>],</td>
      </tr>
      <tr>
        <td id="L60" class="blob-num js-line-number" data-line-number="60"></td>
        <td id="LC60" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.25732244</span>,  <span class="pl-c1">0.2561304</span> ,  <span class="pl-c1">0.52656332</span>],</td>
      </tr>
      <tr>
        <td id="L61" class="blob-num js-line-number" data-line-number="61"></td>
        <td id="LC61" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.25564519</span>,  <span class="pl-c1">0.26070284</span>,  <span class="pl-c1">0.52831152</span>],</td>
      </tr>
      <tr>
        <td id="L62" class="blob-num js-line-number" data-line-number="62"></td>
        <td id="LC62" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.25393498</span>,  <span class="pl-c1">0.26525384</span>,  <span class="pl-c1">0.52998273</span>],</td>
      </tr>
      <tr>
        <td id="L63" class="blob-num js-line-number" data-line-number="63"></td>
        <td id="LC63" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.25219404</span>,  <span class="pl-c1">0.26978306</span>,  <span class="pl-c1">0.53157905</span>],</td>
      </tr>
      <tr>
        <td id="L64" class="blob-num js-line-number" data-line-number="64"></td>
        <td id="LC64" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.25042462</span>,  <span class="pl-c1">0.27429024</span>,  <span class="pl-c1">0.53310261</span>],</td>
      </tr>
      <tr>
        <td id="L65" class="blob-num js-line-number" data-line-number="65"></td>
        <td id="LC65" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.24862899</span>,  <span class="pl-c1">0.27877509</span>,  <span class="pl-c1">0.53455561</span>],</td>
      </tr>
      <tr>
        <td id="L66" class="blob-num js-line-number" data-line-number="66"></td>
        <td id="LC66" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.2468114</span> ,  <span class="pl-c1">0.28323662</span>,  <span class="pl-c1">0.53594093</span>],</td>
      </tr>
      <tr>
        <td id="L67" class="blob-num js-line-number" data-line-number="67"></td>
        <td id="LC67" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.24497208</span>,  <span class="pl-c1">0.28767547</span>,  <span class="pl-c1">0.53726018</span>],</td>
      </tr>
      <tr>
        <td id="L68" class="blob-num js-line-number" data-line-number="68"></td>
        <td id="LC68" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.24311324</span>,  <span class="pl-c1">0.29209154</span>,  <span class="pl-c1">0.53851561</span>],</td>
      </tr>
      <tr>
        <td id="L69" class="blob-num js-line-number" data-line-number="69"></td>
        <td id="LC69" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.24123708</span>,  <span class="pl-c1">0.29648471</span>,  <span class="pl-c1">0.53970946</span>],</td>
      </tr>
      <tr>
        <td id="L70" class="blob-num js-line-number" data-line-number="70"></td>
        <td id="LC70" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.23934575</span>,  <span class="pl-c1">0.30085494</span>,  <span class="pl-c1">0.54084398</span>],</td>
      </tr>
      <tr>
        <td id="L71" class="blob-num js-line-number" data-line-number="71"></td>
        <td id="LC71" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.23744138</span>,  <span class="pl-c1">0.30520222</span>,  <span class="pl-c1">0.5419214</span> ],</td>
      </tr>
      <tr>
        <td id="L72" class="blob-num js-line-number" data-line-number="72"></td>
        <td id="LC72" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.23552606</span>,  <span class="pl-c1">0.30952657</span>,  <span class="pl-c1">0.54294396</span>],</td>
      </tr>
      <tr>
        <td id="L73" class="blob-num js-line-number" data-line-number="73"></td>
        <td id="LC73" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.23360277</span>,  <span class="pl-c1">0.31382773</span>,  <span class="pl-c1">0.54391424</span>],</td>
      </tr>
      <tr>
        <td id="L74" class="blob-num js-line-number" data-line-number="74"></td>
        <td id="LC74" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.2316735</span> ,  <span class="pl-c1">0.3181058</span> ,  <span class="pl-c1">0.54483444</span>],</td>
      </tr>
      <tr>
        <td id="L75" class="blob-num js-line-number" data-line-number="75"></td>
        <td id="LC75" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.22973926</span>,  <span class="pl-c1">0.32236127</span>,  <span class="pl-c1">0.54570633</span>],</td>
      </tr>
      <tr>
        <td id="L76" class="blob-num js-line-number" data-line-number="76"></td>
        <td id="LC76" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.22780192</span>,  <span class="pl-c1">0.32659432</span>,  <span class="pl-c1">0.546532</span>  ],</td>
      </tr>
      <tr>
        <td id="L77" class="blob-num js-line-number" data-line-number="77"></td>
        <td id="LC77" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.2258633</span> ,  <span class="pl-c1">0.33080515</span>,  <span class="pl-c1">0.54731353</span>],</td>
      </tr>
      <tr>
        <td id="L78" class="blob-num js-line-number" data-line-number="78"></td>
        <td id="LC78" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.22392515</span>,  <span class="pl-c1">0.334994</span>  ,  <span class="pl-c1">0.54805291</span>],</td>
      </tr>
      <tr>
        <td id="L79" class="blob-num js-line-number" data-line-number="79"></td>
        <td id="LC79" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.22198915</span>,  <span class="pl-c1">0.33916114</span>,  <span class="pl-c1">0.54875211</span>],</td>
      </tr>
      <tr>
        <td id="L80" class="blob-num js-line-number" data-line-number="80"></td>
        <td id="LC80" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.22005691</span>,  <span class="pl-c1">0.34330688</span>,  <span class="pl-c1">0.54941304</span>],</td>
      </tr>
      <tr>
        <td id="L81" class="blob-num js-line-number" data-line-number="81"></td>
        <td id="LC81" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.21812995</span>,  <span class="pl-c1">0.34743154</span>,  <span class="pl-c1">0.55003755</span>],</td>
      </tr>
      <tr>
        <td id="L82" class="blob-num js-line-number" data-line-number="82"></td>
        <td id="LC82" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.21620971</span>,  <span class="pl-c1">0.35153548</span>,  <span class="pl-c1">0.55062743</span>],</td>
      </tr>
      <tr>
        <td id="L83" class="blob-num js-line-number" data-line-number="83"></td>
        <td id="LC83" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.21429757</span>,  <span class="pl-c1">0.35561907</span>,  <span class="pl-c1">0.5511844</span> ],</td>
      </tr>
      <tr>
        <td id="L84" class="blob-num js-line-number" data-line-number="84"></td>
        <td id="LC84" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.21239477</span>,  <span class="pl-c1">0.35968273</span>,  <span class="pl-c1">0.55171011</span>],</td>
      </tr>
      <tr>
        <td id="L85" class="blob-num js-line-number" data-line-number="85"></td>
        <td id="LC85" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.2105031</span> ,  <span class="pl-c1">0.36372671</span>,  <span class="pl-c1">0.55220646</span>],</td>
      </tr>
      <tr>
        <td id="L86" class="blob-num js-line-number" data-line-number="86"></td>
        <td id="LC86" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.20862342</span>,  <span class="pl-c1">0.36775151</span>,  <span class="pl-c1">0.55267486</span>],</td>
      </tr>
      <tr>
        <td id="L87" class="blob-num js-line-number" data-line-number="87"></td>
        <td id="LC87" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.20675628</span>,  <span class="pl-c1">0.37175775</span>,  <span class="pl-c1">0.55311653</span>],</td>
      </tr>
      <tr>
        <td id="L88" class="blob-num js-line-number" data-line-number="88"></td>
        <td id="LC88" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.20490257</span>,  <span class="pl-c1">0.37574589</span>,  <span class="pl-c1">0.55353282</span>],</td>
      </tr>
      <tr>
        <td id="L89" class="blob-num js-line-number" data-line-number="89"></td>
        <td id="LC89" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.20306309</span>,  <span class="pl-c1">0.37971644</span>,  <span class="pl-c1">0.55392505</span>],</td>
      </tr>
      <tr>
        <td id="L90" class="blob-num js-line-number" data-line-number="90"></td>
        <td id="LC90" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.20123854</span>,  <span class="pl-c1">0.38366989</span>,  <span class="pl-c1">0.55429441</span>],</td>
      </tr>
      <tr>
        <td id="L91" class="blob-num js-line-number" data-line-number="91"></td>
        <td id="LC91" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.1994295</span> ,  <span class="pl-c1">0.38760678</span>,  <span class="pl-c1">0.55464205</span>],</td>
      </tr>
      <tr>
        <td id="L92" class="blob-num js-line-number" data-line-number="92"></td>
        <td id="LC92" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.1976365</span> ,  <span class="pl-c1">0.39152762</span>,  <span class="pl-c1">0.55496905</span>],</td>
      </tr>
      <tr>
        <td id="L93" class="blob-num js-line-number" data-line-number="93"></td>
        <td id="LC93" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.19585993</span>,  <span class="pl-c1">0.39543297</span>,  <span class="pl-c1">0.55527637</span>],</td>
      </tr>
      <tr>
        <td id="L94" class="blob-num js-line-number" data-line-number="94"></td>
        <td id="LC94" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.19410009</span>,  <span class="pl-c1">0.39932336</span>,  <span class="pl-c1">0.55556494</span>],</td>
      </tr>
      <tr>
        <td id="L95" class="blob-num js-line-number" data-line-number="95"></td>
        <td id="LC95" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.19235719</span>,  <span class="pl-c1">0.40319934</span>,  <span class="pl-c1">0.55583559</span>],</td>
      </tr>
      <tr>
        <td id="L96" class="blob-num js-line-number" data-line-number="96"></td>
        <td id="LC96" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.19063135</span>,  <span class="pl-c1">0.40706148</span>,  <span class="pl-c1">0.55608907</span>],</td>
      </tr>
      <tr>
        <td id="L97" class="blob-num js-line-number" data-line-number="97"></td>
        <td id="LC97" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.18892259</span>,  <span class="pl-c1">0.41091033</span>,  <span class="pl-c1">0.55632606</span>],</td>
      </tr>
      <tr>
        <td id="L98" class="blob-num js-line-number" data-line-number="98"></td>
        <td id="LC98" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.18723083</span>,  <span class="pl-c1">0.41474645</span>,  <span class="pl-c1">0.55654717</span>],</td>
      </tr>
      <tr>
        <td id="L99" class="blob-num js-line-number" data-line-number="99"></td>
        <td id="LC99" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.18555593</span>,  <span class="pl-c1">0.4185704</span> ,  <span class="pl-c1">0.55675292</span>],</td>
      </tr>
      <tr>
        <td id="L100" class="blob-num js-line-number" data-line-number="100"></td>
        <td id="LC100" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.18389763</span>,  <span class="pl-c1">0.42238275</span>,  <span class="pl-c1">0.55694377</span>],</td>
      </tr>
      <tr>
        <td id="L101" class="blob-num js-line-number" data-line-number="101"></td>
        <td id="LC101" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.18225561</span>,  <span class="pl-c1">0.42618405</span>,  <span class="pl-c1">0.5571201</span> ],</td>
      </tr>
      <tr>
        <td id="L102" class="blob-num js-line-number" data-line-number="102"></td>
        <td id="LC102" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.18062949</span>,  <span class="pl-c1">0.42997486</span>,  <span class="pl-c1">0.55728221</span>],</td>
      </tr>
      <tr>
        <td id="L103" class="blob-num js-line-number" data-line-number="103"></td>
        <td id="LC103" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.17901879</span>,  <span class="pl-c1">0.43375572</span>,  <span class="pl-c1">0.55743035</span>],</td>
      </tr>
      <tr>
        <td id="L104" class="blob-num js-line-number" data-line-number="104"></td>
        <td id="LC104" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.17742298</span>,  <span class="pl-c1">0.4375272</span> ,  <span class="pl-c1">0.55756466</span>],</td>
      </tr>
      <tr>
        <td id="L105" class="blob-num js-line-number" data-line-number="105"></td>
        <td id="LC105" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.17584148</span>,  <span class="pl-c1">0.44128981</span>,  <span class="pl-c1">0.55768526</span>],</td>
      </tr>
      <tr>
        <td id="L106" class="blob-num js-line-number" data-line-number="106"></td>
        <td id="LC106" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.17427363</span>,  <span class="pl-c1">0.4450441</span> ,  <span class="pl-c1">0.55779216</span>],</td>
      </tr>
      <tr>
        <td id="L107" class="blob-num js-line-number" data-line-number="107"></td>
        <td id="LC107" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.17271876</span>,  <span class="pl-c1">0.4487906</span> ,  <span class="pl-c1">0.55788532</span>],</td>
      </tr>
      <tr>
        <td id="L108" class="blob-num js-line-number" data-line-number="108"></td>
        <td id="LC108" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.17117615</span>,  <span class="pl-c1">0.4525298</span> ,  <span class="pl-c1">0.55796464</span>],</td>
      </tr>
      <tr>
        <td id="L109" class="blob-num js-line-number" data-line-number="109"></td>
        <td id="LC109" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.16964573</span>,  <span class="pl-c1">0.45626209</span>,  <span class="pl-c1">0.55803034</span>],</td>
      </tr>
      <tr>
        <td id="L110" class="blob-num js-line-number" data-line-number="110"></td>
        <td id="LC110" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.16812641</span>,  <span class="pl-c1">0.45998802</span>,  <span class="pl-c1">0.55808199</span>],</td>
      </tr>
      <tr>
        <td id="L111" class="blob-num js-line-number" data-line-number="111"></td>
        <td id="LC111" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.1666171</span> ,  <span class="pl-c1">0.46370813</span>,  <span class="pl-c1">0.55811913</span>],</td>
      </tr>
      <tr>
        <td id="L112" class="blob-num js-line-number" data-line-number="112"></td>
        <td id="LC112" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.16511703</span>,  <span class="pl-c1">0.4674229</span> ,  <span class="pl-c1">0.55814141</span>],</td>
      </tr>
      <tr>
        <td id="L113" class="blob-num js-line-number" data-line-number="113"></td>
        <td id="LC113" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.16362543</span>,  <span class="pl-c1">0.47113278</span>,  <span class="pl-c1">0.55814842</span>],</td>
      </tr>
      <tr>
        <td id="L114" class="blob-num js-line-number" data-line-number="114"></td>
        <td id="LC114" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.16214155</span>,  <span class="pl-c1">0.47483821</span>,  <span class="pl-c1">0.55813967</span>],</td>
      </tr>
      <tr>
        <td id="L115" class="blob-num js-line-number" data-line-number="115"></td>
        <td id="LC115" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.16066467</span>,  <span class="pl-c1">0.47853961</span>,  <span class="pl-c1">0.55811466</span>],</td>
      </tr>
      <tr>
        <td id="L116" class="blob-num js-line-number" data-line-number="116"></td>
        <td id="LC116" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.15919413</span>,  <span class="pl-c1">0.4822374</span> ,  <span class="pl-c1">0.5580728</span> ],</td>
      </tr>
      <tr>
        <td id="L117" class="blob-num js-line-number" data-line-number="117"></td>
        <td id="LC117" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.15772933</span>,  <span class="pl-c1">0.48593197</span>,  <span class="pl-c1">0.55801347</span>],</td>
      </tr>
      <tr>
        <td id="L118" class="blob-num js-line-number" data-line-number="118"></td>
        <td id="LC118" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.15626973</span>,  <span class="pl-c1">0.4896237</span> ,  <span class="pl-c1">0.557936</span>  ],</td>
      </tr>
      <tr>
        <td id="L119" class="blob-num js-line-number" data-line-number="119"></td>
        <td id="LC119" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.15481488</span>,  <span class="pl-c1">0.49331293</span>,  <span class="pl-c1">0.55783967</span>],</td>
      </tr>
      <tr>
        <td id="L120" class="blob-num js-line-number" data-line-number="120"></td>
        <td id="LC120" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.15336445</span>,  <span class="pl-c1">0.49700003</span>,  <span class="pl-c1">0.55772371</span>],</td>
      </tr>
      <tr>
        <td id="L121" class="blob-num js-line-number" data-line-number="121"></td>
        <td id="LC121" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.1519182</span> ,  <span class="pl-c1">0.50068529</span>,  <span class="pl-c1">0.55758733</span>],</td>
      </tr>
      <tr>
        <td id="L122" class="blob-num js-line-number" data-line-number="122"></td>
        <td id="LC122" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.15047605</span>,  <span class="pl-c1">0.50436904</span>,  <span class="pl-c1">0.55742968</span>],</td>
      </tr>
      <tr>
        <td id="L123" class="blob-num js-line-number" data-line-number="123"></td>
        <td id="LC123" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.14903918</span>,  <span class="pl-c1">0.50805136</span>,  <span class="pl-c1">0.5572505</span> ],</td>
      </tr>
      <tr>
        <td id="L124" class="blob-num js-line-number" data-line-number="124"></td>
        <td id="LC124" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.14760731</span>,  <span class="pl-c1">0.51173263</span>,  <span class="pl-c1">0.55704861</span>],</td>
      </tr>
      <tr>
        <td id="L125" class="blob-num js-line-number" data-line-number="125"></td>
        <td id="LC125" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.14618026</span>,  <span class="pl-c1">0.51541316</span>,  <span class="pl-c1">0.55682271</span>],</td>
      </tr>
      <tr>
        <td id="L126" class="blob-num js-line-number" data-line-number="126"></td>
        <td id="LC126" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.14475863</span>,  <span class="pl-c1">0.51909319</span>,  <span class="pl-c1">0.55657181</span>],</td>
      </tr>
      <tr>
        <td id="L127" class="blob-num js-line-number" data-line-number="127"></td>
        <td id="LC127" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.14334327</span>,  <span class="pl-c1">0.52277292</span>,  <span class="pl-c1">0.55629491</span>],</td>
      </tr>
      <tr>
        <td id="L128" class="blob-num js-line-number" data-line-number="128"></td>
        <td id="LC128" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.14193527</span>,  <span class="pl-c1">0.52645254</span>,  <span class="pl-c1">0.55599097</span>],</td>
      </tr>
      <tr>
        <td id="L129" class="blob-num js-line-number" data-line-number="129"></td>
        <td id="LC129" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.14053599</span>,  <span class="pl-c1">0.53013219</span>,  <span class="pl-c1">0.55565893</span>],</td>
      </tr>
      <tr>
        <td id="L130" class="blob-num js-line-number" data-line-number="130"></td>
        <td id="LC130" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.13914708</span>,  <span class="pl-c1">0.53381201</span>,  <span class="pl-c1">0.55529773</span>],</td>
      </tr>
      <tr>
        <td id="L131" class="blob-num js-line-number" data-line-number="131"></td>
        <td id="LC131" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.13777048</span>,  <span class="pl-c1">0.53749213</span>,  <span class="pl-c1">0.55490625</span>],</td>
      </tr>
      <tr>
        <td id="L132" class="blob-num js-line-number" data-line-number="132"></td>
        <td id="LC132" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.1364085</span> ,  <span class="pl-c1">0.54117264</span>,  <span class="pl-c1">0.55448339</span>],</td>
      </tr>
      <tr>
        <td id="L133" class="blob-num js-line-number" data-line-number="133"></td>
        <td id="LC133" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.13506561</span>,  <span class="pl-c1">0.54485335</span>,  <span class="pl-c1">0.55402906</span>],</td>
      </tr>
      <tr>
        <td id="L134" class="blob-num js-line-number" data-line-number="134"></td>
        <td id="LC134" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.13374299</span>,  <span class="pl-c1">0.54853458</span>,  <span class="pl-c1">0.55354108</span>],</td>
      </tr>
      <tr>
        <td id="L135" class="blob-num js-line-number" data-line-number="135"></td>
        <td id="LC135" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.13244401</span>,  <span class="pl-c1">0.55221637</span>,  <span class="pl-c1">0.55301828</span>],</td>
      </tr>
      <tr>
        <td id="L136" class="blob-num js-line-number" data-line-number="136"></td>
        <td id="LC136" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.13117249</span>,  <span class="pl-c1">0.55589872</span>,  <span class="pl-c1">0.55245948</span>],</td>
      </tr>
      <tr>
        <td id="L137" class="blob-num js-line-number" data-line-number="137"></td>
        <td id="LC137" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.1299327</span> ,  <span class="pl-c1">0.55958162</span>,  <span class="pl-c1">0.55186354</span>],</td>
      </tr>
      <tr>
        <td id="L138" class="blob-num js-line-number" data-line-number="138"></td>
        <td id="LC138" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12872938</span>,  <span class="pl-c1">0.56326503</span>,  <span class="pl-c1">0.55122927</span>],</td>
      </tr>
      <tr>
        <td id="L139" class="blob-num js-line-number" data-line-number="139"></td>
        <td id="LC139" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12756771</span>,  <span class="pl-c1">0.56694891</span>,  <span class="pl-c1">0.55055551</span>],</td>
      </tr>
      <tr>
        <td id="L140" class="blob-num js-line-number" data-line-number="140"></td>
        <td id="LC140" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12645338</span>,  <span class="pl-c1">0.57063316</span>,  <span class="pl-c1">0.5498411</span> ],</td>
      </tr>
      <tr>
        <td id="L141" class="blob-num js-line-number" data-line-number="141"></td>
        <td id="LC141" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12539383</span>,  <span class="pl-c1">0.57431754</span>,  <span class="pl-c1">0.54908564</span>],</td>
      </tr>
      <tr>
        <td id="L142" class="blob-num js-line-number" data-line-number="142"></td>
        <td id="LC142" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12439474</span>,  <span class="pl-c1">0.57800205</span>,  <span class="pl-c1">0.5482874</span> ],</td>
      </tr>
      <tr>
        <td id="L143" class="blob-num js-line-number" data-line-number="143"></td>
        <td id="LC143" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12346281</span>,  <span class="pl-c1">0.58168661</span>,  <span class="pl-c1">0.54744498</span>],</td>
      </tr>
      <tr>
        <td id="L144" class="blob-num js-line-number" data-line-number="144"></td>
        <td id="LC144" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12260562</span>,  <span class="pl-c1">0.58537105</span>,  <span class="pl-c1">0.54655722</span>],</td>
      </tr>
      <tr>
        <td id="L145" class="blob-num js-line-number" data-line-number="145"></td>
        <td id="LC145" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12183122</span>,  <span class="pl-c1">0.58905521</span>,  <span class="pl-c1">0.54562298</span>],</td>
      </tr>
      <tr>
        <td id="L146" class="blob-num js-line-number" data-line-number="146"></td>
        <td id="LC146" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12114807</span>,  <span class="pl-c1">0.59273889</span>,  <span class="pl-c1">0.54464114</span>],</td>
      </tr>
      <tr>
        <td id="L147" class="blob-num js-line-number" data-line-number="147"></td>
        <td id="LC147" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12056501</span>,  <span class="pl-c1">0.59642187</span>,  <span class="pl-c1">0.54361058</span>],</td>
      </tr>
      <tr>
        <td id="L148" class="blob-num js-line-number" data-line-number="148"></td>
        <td id="LC148" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12009154</span>,  <span class="pl-c1">0.60010387</span>,  <span class="pl-c1">0.54253043</span>],</td>
      </tr>
      <tr>
        <td id="L149" class="blob-num js-line-number" data-line-number="149"></td>
        <td id="LC149" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.11973756</span>,  <span class="pl-c1">0.60378459</span>,  <span class="pl-c1">0.54139999</span>],</td>
      </tr>
      <tr>
        <td id="L150" class="blob-num js-line-number" data-line-number="150"></td>
        <td id="LC150" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.11951163</span>,  <span class="pl-c1">0.60746388</span>,  <span class="pl-c1">0.54021751</span>],</td>
      </tr>
      <tr>
        <td id="L151" class="blob-num js-line-number" data-line-number="151"></td>
        <td id="LC151" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.11942341</span>,  <span class="pl-c1">0.61114146</span>,  <span class="pl-c1">0.53898192</span>],</td>
      </tr>
      <tr>
        <td id="L152" class="blob-num js-line-number" data-line-number="152"></td>
        <td id="LC152" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.11948255</span>,  <span class="pl-c1">0.61481702</span>,  <span class="pl-c1">0.53769219</span>],</td>
      </tr>
      <tr>
        <td id="L153" class="blob-num js-line-number" data-line-number="153"></td>
        <td id="LC153" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.11969858</span>,  <span class="pl-c1">0.61849025</span>,  <span class="pl-c1">0.53634733</span>],</td>
      </tr>
      <tr>
        <td id="L154" class="blob-num js-line-number" data-line-number="154"></td>
        <td id="LC154" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12008079</span>,  <span class="pl-c1">0.62216081</span>,  <span class="pl-c1">0.53494633</span>],</td>
      </tr>
      <tr>
        <td id="L155" class="blob-num js-line-number" data-line-number="155"></td>
        <td id="LC155" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12063824</span>,  <span class="pl-c1">0.62582833</span>,  <span class="pl-c1">0.53348834</span>],</td>
      </tr>
      <tr>
        <td id="L156" class="blob-num js-line-number" data-line-number="156"></td>
        <td id="LC156" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12137972</span>,  <span class="pl-c1">0.62949242</span>,  <span class="pl-c1">0.53197275</span>],</td>
      </tr>
      <tr>
        <td id="L157" class="blob-num js-line-number" data-line-number="157"></td>
        <td id="LC157" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12231244</span>,  <span class="pl-c1">0.63315277</span>,  <span class="pl-c1">0.53039808</span>],</td>
      </tr>
      <tr>
        <td id="L158" class="blob-num js-line-number" data-line-number="158"></td>
        <td id="LC158" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12344358</span>,  <span class="pl-c1">0.63680899</span>,  <span class="pl-c1">0.52876343</span>],</td>
      </tr>
      <tr>
        <td id="L159" class="blob-num js-line-number" data-line-number="159"></td>
        <td id="LC159" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12477953</span>,  <span class="pl-c1">0.64046069</span>,  <span class="pl-c1">0.52706792</span>],</td>
      </tr>
      <tr>
        <td id="L160" class="blob-num js-line-number" data-line-number="160"></td>
        <td id="LC160" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12632581</span>,  <span class="pl-c1">0.64410744</span>,  <span class="pl-c1">0.52531069</span>],</td>
      </tr>
      <tr>
        <td id="L161" class="blob-num js-line-number" data-line-number="161"></td>
        <td id="LC161" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.12808703</span>,  <span class="pl-c1">0.64774881</span>,  <span class="pl-c1">0.52349092</span>],</td>
      </tr>
      <tr>
        <td id="L162" class="blob-num js-line-number" data-line-number="162"></td>
        <td id="LC162" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.13006688</span>,  <span class="pl-c1">0.65138436</span>,  <span class="pl-c1">0.52160791</span>],</td>
      </tr>
      <tr>
        <td id="L163" class="blob-num js-line-number" data-line-number="163"></td>
        <td id="LC163" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.13226797</span>,  <span class="pl-c1">0.65501363</span>,  <span class="pl-c1">0.51966086</span>],</td>
      </tr>
      <tr>
        <td id="L164" class="blob-num js-line-number" data-line-number="164"></td>
        <td id="LC164" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.13469183</span>,  <span class="pl-c1">0.65863619</span>,  <span class="pl-c1">0.5176488</span> ],</td>
      </tr>
      <tr>
        <td id="L165" class="blob-num js-line-number" data-line-number="165"></td>
        <td id="LC165" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.13733921</span>,  <span class="pl-c1">0.66225157</span>,  <span class="pl-c1">0.51557101</span>],</td>
      </tr>
      <tr>
        <td id="L166" class="blob-num js-line-number" data-line-number="166"></td>
        <td id="LC166" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.14020991</span>,  <span class="pl-c1">0.66585927</span>,  <span class="pl-c1">0.5134268</span> ],</td>
      </tr>
      <tr>
        <td id="L167" class="blob-num js-line-number" data-line-number="167"></td>
        <td id="LC167" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.14330291</span>,  <span class="pl-c1">0.66945881</span>,  <span class="pl-c1">0.51121549</span>],</td>
      </tr>
      <tr>
        <td id="L168" class="blob-num js-line-number" data-line-number="168"></td>
        <td id="LC168" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.1466164</span> ,  <span class="pl-c1">0.67304968</span>,  <span class="pl-c1">0.50893644</span>],</td>
      </tr>
      <tr>
        <td id="L169" class="blob-num js-line-number" data-line-number="169"></td>
        <td id="LC169" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.15014782</span>,  <span class="pl-c1">0.67663139</span>,  <span class="pl-c1">0.5065889</span> ],</td>
      </tr>
      <tr>
        <td id="L170" class="blob-num js-line-number" data-line-number="170"></td>
        <td id="LC170" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.15389405</span>,  <span class="pl-c1">0.68020343</span>,  <span class="pl-c1">0.50417217</span>],</td>
      </tr>
      <tr>
        <td id="L171" class="blob-num js-line-number" data-line-number="171"></td>
        <td id="LC171" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.15785146</span>,  <span class="pl-c1">0.68376525</span>,  <span class="pl-c1">0.50168574</span>],</td>
      </tr>
      <tr>
        <td id="L172" class="blob-num js-line-number" data-line-number="172"></td>
        <td id="LC172" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.16201598</span>,  <span class="pl-c1">0.68731632</span>,  <span class="pl-c1">0.49912906</span>],</td>
      </tr>
      <tr>
        <td id="L173" class="blob-num js-line-number" data-line-number="173"></td>
        <td id="LC173" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.1663832</span> ,  <span class="pl-c1">0.69085611</span>,  <span class="pl-c1">0.49650163</span>],</td>
      </tr>
      <tr>
        <td id="L174" class="blob-num js-line-number" data-line-number="174"></td>
        <td id="LC174" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.1709484</span> ,  <span class="pl-c1">0.69438405</span>,  <span class="pl-c1">0.49380294</span>],</td>
      </tr>
      <tr>
        <td id="L175" class="blob-num js-line-number" data-line-number="175"></td>
        <td id="LC175" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.17570671</span>,  <span class="pl-c1">0.6978996</span> ,  <span class="pl-c1">0.49103252</span>],</td>
      </tr>
      <tr>
        <td id="L176" class="blob-num js-line-number" data-line-number="176"></td>
        <td id="LC176" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.18065314</span>,  <span class="pl-c1">0.70140222</span>,  <span class="pl-c1">0.48818938</span>],</td>
      </tr>
      <tr>
        <td id="L177" class="blob-num js-line-number" data-line-number="177"></td>
        <td id="LC177" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.18578266</span>,  <span class="pl-c1">0.70489133</span>,  <span class="pl-c1">0.48527326</span>],</td>
      </tr>
      <tr>
        <td id="L178" class="blob-num js-line-number" data-line-number="178"></td>
        <td id="LC178" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.19109018</span>,  <span class="pl-c1">0.70836635</span>,  <span class="pl-c1">0.48228395</span>],</td>
      </tr>
      <tr>
        <td id="L179" class="blob-num js-line-number" data-line-number="179"></td>
        <td id="LC179" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.19657063</span>,  <span class="pl-c1">0.71182668</span>,  <span class="pl-c1">0.47922108</span>],</td>
      </tr>
      <tr>
        <td id="L180" class="blob-num js-line-number" data-line-number="180"></td>
        <td id="LC180" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.20221902</span>,  <span class="pl-c1">0.71527175</span>,  <span class="pl-c1">0.47608431</span>],</td>
      </tr>
      <tr>
        <td id="L181" class="blob-num js-line-number" data-line-number="181"></td>
        <td id="LC181" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.20803045</span>,  <span class="pl-c1">0.71870095</span>,  <span class="pl-c1">0.4728733</span> ],</td>
      </tr>
      <tr>
        <td id="L182" class="blob-num js-line-number" data-line-number="182"></td>
        <td id="LC182" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.21400015</span>,  <span class="pl-c1">0.72211371</span>,  <span class="pl-c1">0.46958774</span>],</td>
      </tr>
      <tr>
        <td id="L183" class="blob-num js-line-number" data-line-number="183"></td>
        <td id="LC183" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.22012381</span>,  <span class="pl-c1">0.72550945</span>,  <span class="pl-c1">0.46622638</span>],</td>
      </tr>
      <tr>
        <td id="L184" class="blob-num js-line-number" data-line-number="184"></td>
        <td id="LC184" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.2263969</span> ,  <span class="pl-c1">0.72888753</span>,  <span class="pl-c1">0.46278934</span>],</td>
      </tr>
      <tr>
        <td id="L185" class="blob-num js-line-number" data-line-number="185"></td>
        <td id="LC185" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.23281498</span>,  <span class="pl-c1">0.73224735</span>,  <span class="pl-c1">0.45927675</span>],</td>
      </tr>
      <tr>
        <td id="L186" class="blob-num js-line-number" data-line-number="186"></td>
        <td id="LC186" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.2393739</span> ,  <span class="pl-c1">0.73558828</span>,  <span class="pl-c1">0.45568838</span>],</td>
      </tr>
      <tr>
        <td id="L187" class="blob-num js-line-number" data-line-number="187"></td>
        <td id="LC187" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.24606968</span>,  <span class="pl-c1">0.73890972</span>,  <span class="pl-c1">0.45202405</span>],</td>
      </tr>
      <tr>
        <td id="L188" class="blob-num js-line-number" data-line-number="188"></td>
        <td id="LC188" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.25289851</span>,  <span class="pl-c1">0.74221104</span>,  <span class="pl-c1">0.44828355</span>],</td>
      </tr>
      <tr>
        <td id="L189" class="blob-num js-line-number" data-line-number="189"></td>
        <td id="LC189" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.25985676</span>,  <span class="pl-c1">0.74549162</span>,  <span class="pl-c1">0.44446673</span>],</td>
      </tr>
      <tr>
        <td id="L190" class="blob-num js-line-number" data-line-number="190"></td>
        <td id="LC190" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.26694127</span>,  <span class="pl-c1">0.74875084</span>,  <span class="pl-c1">0.44057284</span>],</td>
      </tr>
      <tr>
        <td id="L191" class="blob-num js-line-number" data-line-number="191"></td>
        <td id="LC191" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.27414922</span>,  <span class="pl-c1">0.75198807</span>,  <span class="pl-c1">0.4366009</span> ],</td>
      </tr>
      <tr>
        <td id="L192" class="blob-num js-line-number" data-line-number="192"></td>
        <td id="LC192" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28147681</span>,  <span class="pl-c1">0.75520266</span>,  <span class="pl-c1">0.43255207</span>],</td>
      </tr>
      <tr>
        <td id="L193" class="blob-num js-line-number" data-line-number="193"></td>
        <td id="LC193" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.28892102</span>,  <span class="pl-c1">0.75839399</span>,  <span class="pl-c1">0.42842626</span>],</td>
      </tr>
      <tr>
        <td id="L194" class="blob-num js-line-number" data-line-number="194"></td>
        <td id="LC194" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.29647899</span>,  <span class="pl-c1">0.76156142</span>,  <span class="pl-c1">0.42422341</span>],</td>
      </tr>
      <tr>
        <td id="L195" class="blob-num js-line-number" data-line-number="195"></td>
        <td id="LC195" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.30414796</span>,  <span class="pl-c1">0.76470433</span>,  <span class="pl-c1">0.41994346</span>],</td>
      </tr>
      <tr>
        <td id="L196" class="blob-num js-line-number" data-line-number="196"></td>
        <td id="LC196" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.31192534</span>,  <span class="pl-c1">0.76782207</span>,  <span class="pl-c1">0.41558638</span>],</td>
      </tr>
      <tr>
        <td id="L197" class="blob-num js-line-number" data-line-number="197"></td>
        <td id="LC197" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.3198086</span> ,  <span class="pl-c1">0.77091403</span>,  <span class="pl-c1">0.41115215</span>],</td>
      </tr>
      <tr>
        <td id="L198" class="blob-num js-line-number" data-line-number="198"></td>
        <td id="LC198" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.3277958</span> ,  <span class="pl-c1">0.77397953</span>,  <span class="pl-c1">0.40664011</span>],</td>
      </tr>
      <tr>
        <td id="L199" class="blob-num js-line-number" data-line-number="199"></td>
        <td id="LC199" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.33588539</span>,  <span class="pl-c1">0.7770179</span> ,  <span class="pl-c1">0.40204917</span>],</td>
      </tr>
      <tr>
        <td id="L200" class="blob-num js-line-number" data-line-number="200"></td>
        <td id="LC200" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.34407411</span>,  <span class="pl-c1">0.78002855</span>,  <span class="pl-c1">0.39738103</span>],</td>
      </tr>
      <tr>
        <td id="L201" class="blob-num js-line-number" data-line-number="201"></td>
        <td id="LC201" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.35235985</span>,  <span class="pl-c1">0.78301086</span>,  <span class="pl-c1">0.39263579</span>],</td>
      </tr>
      <tr>
        <td id="L202" class="blob-num js-line-number" data-line-number="202"></td>
        <td id="LC202" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.36074053</span>,  <span class="pl-c1">0.78596419</span>,  <span class="pl-c1">0.38781353</span>],</td>
      </tr>
      <tr>
        <td id="L203" class="blob-num js-line-number" data-line-number="203"></td>
        <td id="LC203" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.3692142</span> ,  <span class="pl-c1">0.78888793</span>,  <span class="pl-c1">0.38291438</span>],</td>
      </tr>
      <tr>
        <td id="L204" class="blob-num js-line-number" data-line-number="204"></td>
        <td id="LC204" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.37777892</span>,  <span class="pl-c1">0.79178146</span>,  <span class="pl-c1">0.3779385</span> ],</td>
      </tr>
      <tr>
        <td id="L205" class="blob-num js-line-number" data-line-number="205"></td>
        <td id="LC205" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.38643282</span>,  <span class="pl-c1">0.79464415</span>,  <span class="pl-c1">0.37288606</span>],</td>
      </tr>
      <tr>
        <td id="L206" class="blob-num js-line-number" data-line-number="206"></td>
        <td id="LC206" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.39517408</span>,  <span class="pl-c1">0.79747541</span>,  <span class="pl-c1">0.36775726</span>],</td>
      </tr>
      <tr>
        <td id="L207" class="blob-num js-line-number" data-line-number="207"></td>
        <td id="LC207" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.40400101</span>,  <span class="pl-c1">0.80027461</span>,  <span class="pl-c1">0.36255223</span>],</td>
      </tr>
      <tr>
        <td id="L208" class="blob-num js-line-number" data-line-number="208"></td>
        <td id="LC208" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.4129135</span> ,  <span class="pl-c1">0.80304099</span>,  <span class="pl-c1">0.35726893</span>],</td>
      </tr>
      <tr>
        <td id="L209" class="blob-num js-line-number" data-line-number="209"></td>
        <td id="LC209" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.42190813</span>,  <span class="pl-c1">0.80577412</span>,  <span class="pl-c1">0.35191009</span>],</td>
      </tr>
      <tr>
        <td id="L210" class="blob-num js-line-number" data-line-number="210"></td>
        <td id="LC210" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.43098317</span>,  <span class="pl-c1">0.80847343</span>,  <span class="pl-c1">0.34647607</span>],</td>
      </tr>
      <tr>
        <td id="L211" class="blob-num js-line-number" data-line-number="211"></td>
        <td id="LC211" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.44013691</span>,  <span class="pl-c1">0.81113836</span>,  <span class="pl-c1">0.3409673</span> ],</td>
      </tr>
      <tr>
        <td id="L212" class="blob-num js-line-number" data-line-number="212"></td>
        <td id="LC212" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.44936763</span>,  <span class="pl-c1">0.81376835</span>,  <span class="pl-c1">0.33538426</span>],</td>
      </tr>
      <tr>
        <td id="L213" class="blob-num js-line-number" data-line-number="213"></td>
        <td id="LC213" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.45867362</span>,  <span class="pl-c1">0.81636288</span>,  <span class="pl-c1">0.32972749</span>],</td>
      </tr>
      <tr>
        <td id="L214" class="blob-num js-line-number" data-line-number="214"></td>
        <td id="LC214" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.46805314</span>,  <span class="pl-c1">0.81892143</span>,  <span class="pl-c1">0.32399761</span>],</td>
      </tr>
      <tr>
        <td id="L215" class="blob-num js-line-number" data-line-number="215"></td>
        <td id="LC215" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.47750446</span>,  <span class="pl-c1">0.82144351</span>,  <span class="pl-c1">0.31819529</span>],</td>
      </tr>
      <tr>
        <td id="L216" class="blob-num js-line-number" data-line-number="216"></td>
        <td id="LC216" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.4870258</span> ,  <span class="pl-c1">0.82392862</span>,  <span class="pl-c1">0.31232133</span>],</td>
      </tr>
      <tr>
        <td id="L217" class="blob-num js-line-number" data-line-number="217"></td>
        <td id="LC217" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.49661536</span>,  <span class="pl-c1">0.82637633</span>,  <span class="pl-c1">0.30637661</span>],</td>
      </tr>
      <tr>
        <td id="L218" class="blob-num js-line-number" data-line-number="218"></td>
        <td id="LC218" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.5062713</span> ,  <span class="pl-c1">0.82878621</span>,  <span class="pl-c1">0.30036211</span>],</td>
      </tr>
      <tr>
        <td id="L219" class="blob-num js-line-number" data-line-number="219"></td>
        <td id="LC219" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.51599182</span>,  <span class="pl-c1">0.83115784</span>,  <span class="pl-c1">0.29427888</span>],</td>
      </tr>
      <tr>
        <td id="L220" class="blob-num js-line-number" data-line-number="220"></td>
        <td id="LC220" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.52577622</span>,  <span class="pl-c1">0.83349064</span>,  <span class="pl-c1">0.2881265</span> ],</td>
      </tr>
      <tr>
        <td id="L221" class="blob-num js-line-number" data-line-number="221"></td>
        <td id="LC221" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.5356211</span> ,  <span class="pl-c1">0.83578452</span>,  <span class="pl-c1">0.28190832</span>],</td>
      </tr>
      <tr>
        <td id="L222" class="blob-num js-line-number" data-line-number="222"></td>
        <td id="LC222" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.5455244</span> ,  <span class="pl-c1">0.83803918</span>,  <span class="pl-c1">0.27562602</span>],</td>
      </tr>
      <tr>
        <td id="L223" class="blob-num js-line-number" data-line-number="223"></td>
        <td id="LC223" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.55548397</span>,  <span class="pl-c1">0.84025437</span>,  <span class="pl-c1">0.26928147</span>],</td>
      </tr>
      <tr>
        <td id="L224" class="blob-num js-line-number" data-line-number="224"></td>
        <td id="LC224" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.5654976</span> ,  <span class="pl-c1">0.8424299</span> ,  <span class="pl-c1">0.26287683</span>],</td>
      </tr>
      <tr>
        <td id="L225" class="blob-num js-line-number" data-line-number="225"></td>
        <td id="LC225" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.57556297</span>,  <span class="pl-c1">0.84456561</span>,  <span class="pl-c1">0.25641457</span>],</td>
      </tr>
      <tr>
        <td id="L226" class="blob-num js-line-number" data-line-number="226"></td>
        <td id="LC226" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.58567772</span>,  <span class="pl-c1">0.84666139</span>,  <span class="pl-c1">0.24989748</span>],</td>
      </tr>
      <tr>
        <td id="L227" class="blob-num js-line-number" data-line-number="227"></td>
        <td id="LC227" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.59583934</span>,  <span class="pl-c1">0.84871722</span>,  <span class="pl-c1">0.24332878</span>],</td>
      </tr>
      <tr>
        <td id="L228" class="blob-num js-line-number" data-line-number="228"></td>
        <td id="LC228" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.60604528</span>,  <span class="pl-c1">0.8507331</span> ,  <span class="pl-c1">0.23671214</span>],</td>
      </tr>
      <tr>
        <td id="L229" class="blob-num js-line-number" data-line-number="229"></td>
        <td id="LC229" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.61629283</span>,  <span class="pl-c1">0.85270912</span>,  <span class="pl-c1">0.23005179</span>],</td>
      </tr>
      <tr>
        <td id="L230" class="blob-num js-line-number" data-line-number="230"></td>
        <td id="LC230" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.62657923</span>,  <span class="pl-c1">0.85464543</span>,  <span class="pl-c1">0.22335258</span>],</td>
      </tr>
      <tr>
        <td id="L231" class="blob-num js-line-number" data-line-number="231"></td>
        <td id="LC231" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.63690157</span>,  <span class="pl-c1">0.85654226</span>,  <span class="pl-c1">0.21662012</span>],</td>
      </tr>
      <tr>
        <td id="L232" class="blob-num js-line-number" data-line-number="232"></td>
        <td id="LC232" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.64725685</span>,  <span class="pl-c1">0.85839991</span>,  <span class="pl-c1">0.20986086</span>],</td>
      </tr>
      <tr>
        <td id="L233" class="blob-num js-line-number" data-line-number="233"></td>
        <td id="LC233" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.65764197</span>,  <span class="pl-c1">0.86021878</span>,  <span class="pl-c1">0.20308229</span>],</td>
      </tr>
      <tr>
        <td id="L234" class="blob-num js-line-number" data-line-number="234"></td>
        <td id="LC234" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.66805369</span>,  <span class="pl-c1">0.86199932</span>,  <span class="pl-c1">0.19629307</span>],</td>
      </tr>
      <tr>
        <td id="L235" class="blob-num js-line-number" data-line-number="235"></td>
        <td id="LC235" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.67848868</span>,  <span class="pl-c1">0.86374211</span>,  <span class="pl-c1">0.18950326</span>],</td>
      </tr>
      <tr>
        <td id="L236" class="blob-num js-line-number" data-line-number="236"></td>
        <td id="LC236" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.68894351</span>,  <span class="pl-c1">0.86544779</span>,  <span class="pl-c1">0.18272455</span>],</td>
      </tr>
      <tr>
        <td id="L237" class="blob-num js-line-number" data-line-number="237"></td>
        <td id="LC237" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.69941463</span>,  <span class="pl-c1">0.86711711</span>,  <span class="pl-c1">0.17597055</span>],</td>
      </tr>
      <tr>
        <td id="L238" class="blob-num js-line-number" data-line-number="238"></td>
        <td id="LC238" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.70989842</span>,  <span class="pl-c1">0.86875092</span>,  <span class="pl-c1">0.16925712</span>],</td>
      </tr>
      <tr>
        <td id="L239" class="blob-num js-line-number" data-line-number="239"></td>
        <td id="LC239" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.72039115</span>,  <span class="pl-c1">0.87035015</span>,  <span class="pl-c1">0.16260273</span>],</td>
      </tr>
      <tr>
        <td id="L240" class="blob-num js-line-number" data-line-number="240"></td>
        <td id="LC240" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.73088902</span>,  <span class="pl-c1">0.87191584</span>,  <span class="pl-c1">0.15602894</span>],</td>
      </tr>
      <tr>
        <td id="L241" class="blob-num js-line-number" data-line-number="241"></td>
        <td id="LC241" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.74138803</span>,  <span class="pl-c1">0.87344918</span>,  <span class="pl-c1">0.14956101</span>],</td>
      </tr>
      <tr>
        <td id="L242" class="blob-num js-line-number" data-line-number="242"></td>
        <td id="LC242" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.75188414</span>,  <span class="pl-c1">0.87495143</span>,  <span class="pl-c1">0.14322828</span>],</td>
      </tr>
      <tr>
        <td id="L243" class="blob-num js-line-number" data-line-number="243"></td>
        <td id="LC243" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.76237342</span>,  <span class="pl-c1">0.87642392</span>,  <span class="pl-c1">0.13706449</span>],</td>
      </tr>
      <tr>
        <td id="L244" class="blob-num js-line-number" data-line-number="244"></td>
        <td id="LC244" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.77285183</span>,  <span class="pl-c1">0.87786808</span>,  <span class="pl-c1">0.13110864</span>],</td>
      </tr>
      <tr>
        <td id="L245" class="blob-num js-line-number" data-line-number="245"></td>
        <td id="LC245" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.78331535</span>,  <span class="pl-c1">0.87928545</span>,  <span class="pl-c1">0.12540538</span>],</td>
      </tr>
      <tr>
        <td id="L246" class="blob-num js-line-number" data-line-number="246"></td>
        <td id="LC246" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.79375994</span>,  <span class="pl-c1">0.88067763</span>,  <span class="pl-c1">0.12000532</span>],</td>
      </tr>
      <tr>
        <td id="L247" class="blob-num js-line-number" data-line-number="247"></td>
        <td id="LC247" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.80418159</span>,  <span class="pl-c1">0.88204632</span>,  <span class="pl-c1">0.11496505</span>],</td>
      </tr>
      <tr>
        <td id="L248" class="blob-num js-line-number" data-line-number="248"></td>
        <td id="LC248" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.81457634</span>,  <span class="pl-c1">0.88339329</span>,  <span class="pl-c1">0.11034678</span>],</td>
      </tr>
      <tr>
        <td id="L249" class="blob-num js-line-number" data-line-number="249"></td>
        <td id="LC249" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.82494028</span>,  <span class="pl-c1">0.88472036</span>,  <span class="pl-c1">0.10621724</span>],</td>
      </tr>
      <tr>
        <td id="L250" class="blob-num js-line-number" data-line-number="250"></td>
        <td id="LC250" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.83526959</span>,  <span class="pl-c1">0.88602943</span>,  <span class="pl-c1">0.1026459</span> ],</td>
      </tr>
      <tr>
        <td id="L251" class="blob-num js-line-number" data-line-number="251"></td>
        <td id="LC251" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.84556056</span>,  <span class="pl-c1">0.88732243</span>,  <span class="pl-c1">0.09970219</span>],</td>
      </tr>
      <tr>
        <td id="L252" class="blob-num js-line-number" data-line-number="252"></td>
        <td id="LC252" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.8558096</span> ,  <span class="pl-c1">0.88860134</span>,  <span class="pl-c1">0.09745186</span>],</td>
      </tr>
      <tr>
        <td id="L253" class="blob-num js-line-number" data-line-number="253"></td>
        <td id="LC253" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.86601325</span>,  <span class="pl-c1">0.88986815</span>,  <span class="pl-c1">0.09595277</span>],</td>
      </tr>
      <tr>
        <td id="L254" class="blob-num js-line-number" data-line-number="254"></td>
        <td id="LC254" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.87616824</span>,  <span class="pl-c1">0.89112487</span>,  <span class="pl-c1">0.09525046</span>],</td>
      </tr>
      <tr>
        <td id="L255" class="blob-num js-line-number" data-line-number="255"></td>
        <td id="LC255" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.88627146</span>,  <span class="pl-c1">0.89237353</span>,  <span class="pl-c1">0.09537439</span>],</td>
      </tr>
      <tr>
        <td id="L256" class="blob-num js-line-number" data-line-number="256"></td>
        <td id="LC256" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.89632002</span>,  <span class="pl-c1">0.89361614</span>,  <span class="pl-c1">0.09633538</span>],</td>
      </tr>
      <tr>
        <td id="L257" class="blob-num js-line-number" data-line-number="257"></td>
        <td id="LC257" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.90631121</span>,  <span class="pl-c1">0.89485467</span>,  <span class="pl-c1">0.09812496</span>],</td>
      </tr>
      <tr>
        <td id="L258" class="blob-num js-line-number" data-line-number="258"></td>
        <td id="LC258" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.91624212</span>,  <span class="pl-c1">0.89609127</span>,  <span class="pl-c1">0.1007168</span> ],</td>
      </tr>
      <tr>
        <td id="L259" class="blob-num js-line-number" data-line-number="259"></td>
        <td id="LC259" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.92610579</span>,  <span class="pl-c1">0.89732977</span>,  <span class="pl-c1">0.10407067</span>],</td>
      </tr>
      <tr>
        <td id="L260" class="blob-num js-line-number" data-line-number="260"></td>
        <td id="LC260" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.93590444</span>,  <span class="pl-c1">0.8985704</span> ,  <span class="pl-c1">0.10813094</span>],</td>
      </tr>
      <tr>
        <td id="L261" class="blob-num js-line-number" data-line-number="261"></td>
        <td id="LC261" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.94563626</span>,  <span class="pl-c1">0.899815</span>  ,  <span class="pl-c1">0.11283773</span>],</td>
      </tr>
      <tr>
        <td id="L262" class="blob-num js-line-number" data-line-number="262"></td>
        <td id="LC262" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.95529972</span>,  <span class="pl-c1">0.90106534</span>,  <span class="pl-c1">0.11812832</span>],</td>
      </tr>
      <tr>
        <td id="L263" class="blob-num js-line-number" data-line-number="263"></td>
        <td id="LC263" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.96489353</span>,  <span class="pl-c1">0.90232311</span>,  <span class="pl-c1">0.12394051</span>],</td>
      </tr>
      <tr>
        <td id="L264" class="blob-num js-line-number" data-line-number="264"></td>
        <td id="LC264" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.97441665</span>,  <span class="pl-c1">0.90358991</span>,  <span class="pl-c1">0.13021494</span>],</td>
      </tr>
      <tr>
        <td id="L265" class="blob-num js-line-number" data-line-number="265"></td>
        <td id="LC265" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.98386829</span>,  <span class="pl-c1">0.90486726</span>,  <span class="pl-c1">0.13689671</span>],</td>
      </tr>
      <tr>
        <td id="L266" class="blob-num js-line-number" data-line-number="266"></td>
        <td id="LC266" class="blob-code blob-code-inner js-file-line">       [ <span class="pl-c1">0.99324789</span>,  <span class="pl-c1">0.90615657</span>,  <span class="pl-c1">0.1439362</span> ]]</td>
      </tr>
      <tr>
        <td id="L267" class="blob-num js-line-number" data-line-number="267"></td>
        <td id="LC267" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L268" class="blob-num js-line-number" data-line-number="268"></td>
        <td id="LC268" class="blob-code blob-code-inner js-file-line">test_cm <span class="pl-k">=</span> LinearSegmentedColormap.from_list(<span class="pl-c1">__file__</span>, cm_data)</td>
      </tr>
      <tr>
        <td id="L269" class="blob-num js-line-number" data-line-number="269"></td>
        <td id="LC269" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L270" class="blob-num js-line-number" data-line-number="270"></td>
        <td id="LC270" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L271" class="blob-num js-line-number" data-line-number="271"></td>
        <td id="LC271" class="blob-code blob-code-inner js-file-line"><span class="pl-k">if</span> <span class="pl-c1">__name__</span> <span class="pl-k">==</span> <span class="pl-s"><span class="pl-pds">&quot;</span>__main__<span class="pl-pds">&quot;</span></span>:</td>
      </tr>
      <tr>
        <td id="L272" class="blob-num js-line-number" data-line-number="272"></td>
        <td id="LC272" class="blob-code blob-code-inner js-file-line">    <span class="pl-k">import</span> matplotlib.pyplot <span class="pl-k">as</span> plt</td>
      </tr>
      <tr>
        <td id="L273" class="blob-num js-line-number" data-line-number="273"></td>
        <td id="LC273" class="blob-code blob-code-inner js-file-line">    <span class="pl-k">import</span> numpy <span class="pl-k">as</span> np</td>
      </tr>
      <tr>
        <td id="L274" class="blob-num js-line-number" data-line-number="274"></td>
        <td id="LC274" class="blob-code blob-code-inner js-file-line">
</td>
      </tr>
      <tr>
        <td id="L275" class="blob-num js-line-number" data-line-number="275"></td>
        <td id="LC275" class="blob-code blob-code-inner js-file-line">    <span class="pl-k">try</span>:</td>
      </tr>
      <tr>
        <td id="L276" class="blob-num js-line-number" data-line-number="276"></td>
        <td id="LC276" class="blob-code blob-code-inner js-file-line">        <span class="pl-k">from</span> pycam02ucs.cm.viscm <span class="pl-k">import</span> viscm</td>
      </tr>
      <tr>
        <td id="L277" class="blob-num js-line-number" data-line-number="277"></td>
        <td id="LC277" class="blob-code blob-code-inner js-file-line">        viscm(test_cm)</td>
      </tr>
      <tr>
        <td id="L278" class="blob-num js-line-number" data-line-number="278"></td>
        <td id="LC278" class="blob-code blob-code-inner js-file-line">    <span class="pl-k">except</span> <span class="pl-c1">ImportError</span>:</td>
      </tr>
      <tr>
        <td id="L279" class="blob-num js-line-number" data-line-number="279"></td>
        <td id="LC279" class="blob-code blob-code-inner js-file-line">        <span class="pl-k">print</span>(<span class="pl-s"><span class="pl-pds">&quot;</span>pycam02ucs not found, falling back on simple display<span class="pl-pds">&quot;</span></span>)</td>
      </tr>
      <tr>
        <td id="L280" class="blob-num js-line-number" data-line-number="280"></td>
        <td id="LC280" class="blob-code blob-code-inner js-file-line">        plt.imshow(np.linspace(<span class="pl-c1">0</span>, <span class="pl-c1">100</span>, <span class="pl-c1">256</span>)[<span class="pl-c1">None</span>, :], <span class="pl-smi">aspect</span><span class="pl-k">=</span><span class="pl-s"><span class="pl-pds">&#39;</span>auto<span class="pl-pds">&#39;</span></span>,</td>
      </tr>
      <tr>
        <td id="L281" class="blob-num js-line-number" data-line-number="281"></td>
        <td id="LC281" class="blob-code blob-code-inner js-file-line">                   <span class="pl-smi">cmap</span><span class="pl-k">=</span>test_cm)</td>
      </tr>
      <tr>
        <td id="L282" class="blob-num js-line-number" data-line-number="282"></td>
        <td id="LC282" class="blob-code blob-code-inner js-file-line">    plt.show()</td>
      </tr>
</table>

  </div>

</div>

<a href="#jump-to-line" rel="facebox[.linejump]" data-hotkey="l" style="display:none">Jump to Line</a>
<div id="jump-to-line" style="display:none">
  <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="" class="js-jump-to-line-form" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
    <input class="linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" autofocus>
    <button type="submit" class="btn">Go</button>
</form></div>

          </div>
        </div>
        <div class="modal-backdrop"></div>
      </div>
  </div>


    </div><!-- /.wrapper -->

      <div class="container">
  <div class="site-footer" role="contentinfo">
    <ul class="site-footer-links right">
        <li><a href="https://status.github.com/" data-ga-click="Footer, go to status, text:status">Status</a></li>
      <li><a href="https://developer.github.com" data-ga-click="Footer, go to api, text:api">API</a></li>
      <li><a href="https://training.github.com" data-ga-click="Footer, go to training, text:training">Training</a></li>
      <li><a href="https://shop.github.com" data-ga-click="Footer, go to shop, text:shop">Shop</a></li>
        <li><a href="https://github.com/blog" data-ga-click="Footer, go to blog, text:blog">Blog</a></li>
        <li><a href="https://github.com/about" data-ga-click="Footer, go to about, text:about">About</a></li>
        <li><a href="https://help.github.com" data-ga-click="Footer, go to help, text:help">Help</a></li>

    </ul>

    <a href="https://github.com" aria-label="Homepage">
      <span class="mega-octicon octicon-mark-github" title="GitHub"></span>
</a>
    <ul class="site-footer-links">
      <li>&copy; 2015 <span title="0.03878s from github-fe120-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="https://github.com/site/terms" data-ga-click="Footer, go to terms, text:terms">Terms</a></li>
        <li><a href="https://github.com/site/privacy" data-ga-click="Footer, go to privacy, text:privacy">Privacy</a></li>
        <li><a href="https://github.com/security" data-ga-click="Footer, go to security, text:security">Security</a></li>
        <li><a href="https://github.com/contact" data-ga-click="Footer, go to contact, text:contact">Contact</a></li>
    </ul>
  </div>
</div>


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-suggester-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="fullscreen-contents js-fullscreen-contents" placeholder=""></textarea>
      <div class="suggester-container">
        <div class="suggester fullscreen-suggester js-suggester js-navigation-container"></div>
      </div>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped tooltipped-w" aria-label="Exit Zen Mode">
      <span class="mega-octicon octicon-screen-normal"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped tooltipped-w"
      aria-label="Switch themes">
      <span class="octicon octicon-color-mode"></span>
    </a>
  </div>
</div>



    
    

    <div id="ajax-error-message" class="flash flash-error">
      <span class="octicon octicon-alert"></span>
      <a href="#" class="octicon octicon-x flash-close js-ajax-error-dismiss" aria-label="Dismiss error"></a>
      Something went wrong with that request. Please try again.
    </div>


      <script crossorigin="anonymous" src="https://assets-cdn.github.com/assets/frameworks-01a8766fbba2df04677b68fb73599499b1ff20bcc969342c28d30f95fa836d0c.js"></script>
      <script async="async" crossorigin="anonymous" src="https://assets-cdn.github.com/assets/github/index-0a6831eed7acaf8b173663c8626749024571c7030a13c7539f3f0a17b217a99f.js"></script>
      
      
  </body>
</html>

