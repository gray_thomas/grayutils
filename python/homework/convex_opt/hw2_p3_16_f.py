import numpy as np
import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from matplotlib import cm
from math import pow
def bad_fig():
	x=np.linspace(0,10,1000)
	# print x
	plt.figure()
	plt.plot(x,[xi**(-1) for xi in x])
	a=0.5
	c=5.0
	plt.plot(x,[c**(-1.0/(a-1.0))*xi**(-a/(a-1.0)) for xi in x])
	plt.axis('equal')
	plt.axis([0,5,0,5])

def getF_data(xs,ys,alpha):
	X, Y = np.meshgrid(xs,ys)
	Z = np.zeros(X.shape)
	for i in range(0,Z.shape[0]):
		for j in range(0,Z.shape[1]):
			x = X[i,j]
			y = Y[i,j]
			Z[i,j]=pow(x,(alpha)) * pow(y,(1.0-alpha))
	return X,Y,Z





def plot_for_alpha(alpha):

	xs=np.linspace(0.01,2.0,80)
	ys=np.linspace(0.01,2.0,80)
	fig = plt.figure()
	fig.suptitle(r"$\alpha = "+str(alpha)+"$", fontsize=20)
	ax = fig.gca(projection='3d')
	X, Y, Z = getF_data(xs,ys,alpha)

	ax.plot_surface(X, Y, Z, rstride=8, cstride=8, alpha=0.3)
	cset = ax.contour(X, Y, Z, zdir='z', offset=-2.0, cmap=cm.coolwarm)
	cset = ax.contour(X, Y, Z, zdir='x', offset=0.0, cmap=cm.coolwarm)
	cset = ax.contour(X, Y, Z, zdir='y', offset=3.0, cmap=cm.coolwarm)

	ax.set_xlabel('X')
	ax.set_xlim(-0.0, 2.0)
	ax.set_ylabel('Y')
	ax.set_ylim(-0.0, 3.0)
	ax.set_zlabel('Z')
	ax.set_zlim(-2.0, 2.0)
	return fig



def plot_for_alpha_tight(alpha):

	xs=np.linspace(0.01,2.0,40)
	ys=np.linspace(0.01,2.0,40)
	fig = plt.figure()
	ax = fig.gca(projection='3d')
	X, Y, Z = getF_data(xs,ys,alpha)

	ax.plot_surface(X, Y, Z, rstride=4, cstride=4, alpha=0.3)
	cset = ax.contour(X, Y, Z, zdir='z', offset=0.0, cmap=cm.coolwarm)
	cset = ax.contour(X, Y, Z, zdir='x', offset=0.0, cmap=cm.coolwarm)
	cset = ax.contour(X, Y, Z, zdir='y', offset=0.0, cmap=cm.coolwarm)

	ax.set_xlabel('X')
	ax.set_xlim(-0.0, 2.0)
	ax.set_ylabel('Y')
	ax.set_ylim(-0.0, 2.0)
	ax.set_zlabel('Z')
	ax.set_zlim(-0.0, 2.0)
	return fig

# plot_for_alpha(0.5)
plot_for_alpha(0.2).savefig("alpha_0_2.pdf")
plot_for_alpha(0.9).savefig("alpha_0_9.pdf")
plt.show()