import numpy as np

X1=np.array([[3.0,1.0],[1.0,6.0]])*1.1
X2=np.array([[1.0,0.0],[0.0,1.0]])
X1inv=np.linalg.inv(X1)
X2inv=np.linalg.inv(X2)

print np.linalg.eig(X1)[0]
print np.linalg.eig(X2)[0]
# T=X2.dot(X1inv).dot(X2)+X1 - 2*X2
T=X1*3-X2*3 + X2.dot(X1inv).dot(X2) - X1.dot(X2inv).dot(X1)
print np.linalg.eig(T)[0]
# alphas=np.linspace(0.0,1.0,100)
# for alpha in alphas:
# 	X3=X1*alpha+X2*(1.0-alpha)
# 	X3inv=np.linalg.inv(X3)
# 	inner1=alpha*X2inv+(1-3*alpha)*X1inv
# 	inner2=(3.0*alpha-2.0)*X2inv+(1.0-alpha)*X1inv
# 	A=alpha * (1.0-alpha)*(X2 *inner2 * X2+X1*inner1*X1)
# 	# print alpha, (1-3*alpha)
# 	print np.linalg.eig(inner2)[0]