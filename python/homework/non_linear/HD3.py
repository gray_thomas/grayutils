import ly_utils as ly
import matplotlib.pyplot as plt
from plot_setup import *
from BiChatterer import BiChatterer
from functools import partial
from math import sqrt
from harddrive_constants import *
import random as rand
import numpy as np

Jastar = NOMINAL_ACTUATOR_INERTIA + NOMINAL_FLEXTURE_INERTIA + 0.01**2 * 0.005
B = CONSTANT_B
K = CONSTANT_K
beta = NOMINAL_BETA
alpha_k = ALPHA_K
alpha_b = ALPHA_B
Ja = NOMINAL_ACTUATOR_INERTIA
Jf = NOMINAL_FLEXTURE_INERTIA
Ra = ACTUATOR_RESISTANCE
Ia = ACTUATOR_INDUCTANCE
Rf = FLEXURE_RESISTANCE
If = FLEXURE_INDUCTANCE
Ka = ACTUATOR_RESISTANCE
Kf = FLEXURE_CONSTANT


def convert_state(regular_state):
    i_a, o_a, w_a, i_f, o_f, w_f = regular_state
    y = o_a + o_f
    yd = w_a + w_f
    s = o_a - o_f
    sd = w_a - w_f
    ydd = (
        (1 / Jastar - 1 / Ja) * Ka * i_a
        - 1 / Jastar * beta * np.sign(w_a)
        + 1 / Jf * Kf * i_f
        - B * (1 + alpha_b * abs(w_f)) * w_f
        - K * (1 + alpha_k * abs(o_f)**2) * o_f
    )
    sdd = (
        (1 / Jastar + 1 / Ja) * Ka * i_a
        - 1 / Jastar * beta * np.sign(w_a)
        - 1 / Jf * Kf * i_f
        + B * (1 + alpha_b * abs(w_f)) * w_f
        + K * (1 + alpha_k * abs(o_f)**2) * o_f
    )
    standard_state = y, yd, ydd, s, sd, sdd
    return standard_state


def de_convert_state(standard_state):
    y, yd, ydd, s, sd, sdd = standard_state
    o_a = 0.5 * (y + s)
    o_f = 0.5 * (y - s)
    w_a = 0.5 * (yd + sd)
    w_f = 0.5 * (yd - sd)
    i_a = Jastar / (2 * Ka) * (
        ydd + sdd
        + 2 / Jastar * (beta * np.sign(w_a))
    )
    i_f = -Jf / (2 * Kf) * (
        -ydd + sdd
        - 2 / Ja * Ka * i_a
        - 2 * B * (1 + alpha_b * abs(w_f)) * w_f
        - 2 * K * (1 + alpha_k * abs(o_f)**2) * o_f
    )
    regular_state = i_a, o_a, w_a, i_f, o_f, w_f
    return regular_state


@ly.named_state_system("StandardFormHardDriveState", ["y", "yd", "ydd", "s", "sd", "sdd"])
class StandardFormHardDrive(object):
    """A hard drive control mechanism with an actuator and a flexure."""

    def __init__(self, i_a0=0, o_a0=0, w_a0=0, i_f0=0, o_f0=0, w_f0=0):
        self.state = 0, convert_state((i_a0, o_a0, w_a0, i_f0, o_f0, w_f0))
        self.J_a = NOMINAL_ACTUATOR_INERTIA
        self.J_f = NOMINAL_FLEXTURE_INERTIA
        self.B = CONSTANT_B
        self.K = CONSTANT_K
        self.events = []
        self.fric = BiChatterer(
            self, lambda: self.w_a, lambda: (0, 0.5, 0, 0, 0.5, 0),
            "sign_w_a", name="friction", eps_time=1e-12)

    @property
    def system(self):
        return self.fric

    def w_a(self):
        return 0.5 * (self.yd + self.sd)

    def a_f(self):
        return 1.0 / Jf * (
            Kf * self.i_f
            - Jf / Ja * Ka * i_a
            - Jf * B * (1 + alpha_b * abs(w_f)) * w_f
            - Jf * K * (1 + alpha_k * o_f**2) * o_f
            + d_f)

    def a_a(self, sign_w_a=None):
        return 1 / Jastar * (
            Ka * i_a
            - beta * sign_w_a
            - (
                Kf * self.i_f
                - Jf / Ja * Ka * i_a
                - Jf * B * (1 + alpha_b * abs(w_f)) * w_f
                - Jf * K * (1 + alpha_k * o_f**2) * o_f
                + d_f))
            + d_a

    def yddd(self, dirac_w_a, V_a, V_f, dd_f, dd_a):
        return (
            (1 / Jastar - 1 / Ja) * Ka / La * (V_a - Ra * i_a)
            # - 1/Jastar * beta*dirac_w_a*a_a
            + 1 / Jf / Lf * Kf(V_f - Rf * i_f)
            - B * (1 + 2 * alpha_b * abs(w_f)) * a_f
            - K * (1 + 3 * alpha_k * o_f**2) * w_f
            + 1 / Jf * dd_f + 1 / Ja * dd_a
        )

    def sddd(self, dirac_w_a, V_a, V_f, dd_f, dd_a):
        return (
            (1 / Jastar + 1 / Ja) * Ka / La * (V_a - Ra * i_a)
            # - 1/Jastar * beta*dirac_w_a*a_a
            + 1 / Jf / Lf * Kf(V_f - Rf * i_f)
            + B * (1 + 2 * alpha_b * abs(w_f)) * a_f
            + K * (1 + 3 * alpha_k * o_f**2) * w_f
            - 1 / Jf * dd_f + 1 / Ja * dd_a
        )

    def __call__(self, sign_w_a=None):
        return (self.id_a, self.w_a, self.a_a(sign_w_a=sign_w_a), self.id_f, self.w_f, self.a_f)

    def sim(self, tf=100, n=1001):
        sys = self.system
        stepper = ly.dormand_prince(sys, np.linspace(self.t, tf, n))
        stepper.absolute_tolerance = 1e-8
        stepper.relative_tolerance = 1e-8
        stepper.time_tolerance = 1e-7 * tf
        for jump, time, state, guard in ly.hybrid_integrate(stepper):
            ly.check_NaN(self)
            yield self


def main():
    for i in range(0, 20):
        ori0 = tuple([5 - 10 * rand.random() for x in range(0, 6)])
        sta0 = convert_state(ori0)
        reg1 = de_convert_state(sta0)
        sta1 = convert_state(reg1)
        reg2 = de_convert_state(sta1)
        for j, k in zip(ori0, reg2):
            if abs(j - k) > 1e-6:
                print "\n".join([", ".join(["%+.4e"] * 6)] * 5) % (ori0 + sta0 + reg1 + sta1 + reg2)

                assert False


if __name__ == '__main__':
    main()
