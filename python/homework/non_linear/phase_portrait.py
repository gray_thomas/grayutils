import matplotlib.pyplot as plt
import numpy as np 
import scipy.integrate as integ
from math import sin,cos,atan, pi
import violin
import lyapunov as ly
def phase_portrait(derivatives, IC_range, times, title):
	OutICs=[np.array(I) for I in IC_range]
	for IC in OutICs:
		state=IC
		states=[np.array(IC)]
		for i,t in enumerate(times[1:]):
			state+=(t-times[i])*derivatives(state)
			states.append(np.array(state))
		# states = integ.odeint(derivatives,IC,times)
		plt.plot([state[0] for state in states], [state[1] for state in states],'k')
	plt.title(title)
	return OutICs

def ly_phase_portrait(system, IC_range, times, title, 
	explode_threshold=1e20):
	for IC in IC_range:
		system.state=0.0,tuple(IC)
		system.reset()
		logger=ly.Recorder(system)
		stepper=ly.runge_kutta4(system, times)
		# logger.clear()
		# stepper.reset()
		for t, events in stepper:
			logger.log()
			if events:
				if system.handle_events(stepper,events):
					break
				plt.plot([system.x[0]],[system.x[1]],'k.')
			if abs(system.x[0])>explode_threshold or abs(system.x[0])>explode_threshold:
				break
		plt.plot(np.array(logger.x)[:,0],np.array(logger.x)[:,1],'k')


def overlay_arrows(derivatives, ICs, **kwargs):
	derivs=[]
	for I in ICs:
		derivs.append(np.array(derivatives(I)))
		# print derivs[-1]
	xs=[I[0] for I in ICs]
	ys=[I[1] for I in ICs]
	xds=[d[0] for d in derivs]
	yds=[d[1] for d in derivs]
	plt.quiver(xs,ys,xds,yds,units='dots',width=2,angles='xy',**kwargs)
	# units='x', pivot='tail', width=0.00002, scale=4e1, alpha=0.3
def scipy_overlay_arrows(sys, ICs):
	derivs=[]
	for I in ICs:
		derivs.append(sys(I,0))
		# print derivs[-1]
	xs=[I[0] for I in ICs]
	ys=[I[1] for I in ICs]
	xds=[d[0] for d in derivs]
	yds=[d[1] for d in derivs]
	plt.quiver(xs,ys,xds,yds,pivot='tip')
def even_polar_range(center_x, center_y, r_max, n_r, n_o):
	ICs=[np.array([center_x, center_y])]
	for r in np.linspace(0.0, r_max, n_r)[1:]:
		for o in np.linspace(-pi, pi, n_o+1)[1:]:
			ICs.append(np.array([r*cos(o)+center_x,r*sin(o)+center_y]))
	return ICs
def scaled_polar_range(center_x, center_y, r_max, n_r, n_o,scale_y):
	ICs=[np.array([center_x, center_y])]
	for r in np.linspace(0.0, r_max, n_r)[1:]:
		for o in np.linspace(-pi, pi, n_o+1)[1:]:
			ICs.append(np.array([r*cos(o)+center_x,(r*sin(o)*scale_y+center_y)]))
	return ICs
def xy_polar_range(center_x, center_y, x_s, y_s, n_r, n_o):
	ICs=[np.array([center_x, center_y])]
	for r in np.linspace(0.0, 1, n_r)[1:]:
		for o in np.linspace(-pi, pi, n_o+1)[1:]:
			ICs.append(np.array([x_s*r*cos(o)+center_x,(y_s*r*sin(o)+center_y)]))
	return ICs
def xy_range(center_x, center_y, x_s, y_s, n_x, n_y):
	ICs=[np.array([center_x, center_y])]
	for x in np.linspace(center_x-x_s,center_x+x_s,n_x):
		for y in np.linspace(center_y-y_s,center_y+y_s,n_y):
			ICs.append(np.array([x,y]))
	return ICs
