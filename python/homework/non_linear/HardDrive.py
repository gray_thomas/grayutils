import ly_utils as ly
import matplotlib.pyplot as plt
from plot_setup import *
from BiChatterer import BiChatterer


ACTUATOR_RESISTANCE = 1.0  # Ohm
ACTUATOR_INDUCTANCE = 0.001  # Henry
ACTUATOR_CONSTANT = 60  # Newton meters per ampere
ALPHA_K = 0.05  # Radians squared
CONSTANT_K = 3.55e8  # Newton meter per radian
NOMINAL_ACTUATOR_INERTIA = 1.0  # Kilogram meters squared
NOMINAL_FLEXTURE_INERTIA = 2.4e-7  # Kilogram meters squared
# NOMINAL_FLEXTURE_INERTIA = 1.1e-7  # Kilogram meters squared
NOMINAL_BETA = 10.0  # Newton meters
FLEXURE_RESISTANCE = 2.0  # Ohm
FLEXURE_INDUCTANCE = 0.005  # Henry
FLEXURE_CONSTANT = 60.0  # Newton meters per ampere
ALPHA_B = 0.01  # Radians per second
CONSTANT_B = 11310.0  # Newton meter second radian
DISTURBANCE_TO_ACTUATOR_INERTIA = 0.1  # Kilogram meter squared
DISTURBANCE_TO_FLEXURE_INERTIA = 0.1e-7  # Kilogram meter squared
DISTURBANCE_TO_ACTUATOR = 0.1  # Newton meter
DISTURBANCE_TO_FLEXURE = 0.1e-3  # Newton meter
DISTURBANCE_TO_BETA = 5.0  # Newton meter


@ly.named_state_system("HardDriveState", ["i_a", "o_a", "w_a", "i_f", "o_f", "w_f"])
class HardDrive(object):
    """A hard drive control mechanism with an actuator and a flexure."""

    def __init__(self, i_a0=0, o_a0=0, w_a0=0, i_f0=0, o_f0=0, w_f0=0):
        self.state = 0, (i_a0, o_a0, w_a0, i_f0, o_f0, w_f0)
        self.J_a = NOMINAL_ACTUATOR_INERTIA
        self.J_f = NOMINAL_FLEXTURE_INERTIA
        if w_a0 == 0:
            self.enter_chatter()
        elif w_a0 > 0:
            self.enter_pos()
        else:
            self.enter_neg()

    def control(self):
        return (0, 0)

    def disturbance(self):
        return (0, 0)

    def measurement(self):
        return (self.o_a, self.o_f, self.i_a, self.i_f)

    def enter_chatter(self):
        self.events = [self.escape_pos, self.escape_neg]
        self.sign_w_a = 0.0

    def enter_pos(self):
        self.events = [self.neg]
        self.sign_w_a = 1.0

    def enter_neg(self):
        self.events = [self.pos]
        self.sign_w_a = -1.0

    @property
    def y(self):
        return self.o_a + self.o_f

    def __call__(self, sign_w_a=None, friction_can_hold=True):
        if sign_w_a == None:
            sign_w_a = self.sign_w_a
        v_a, v_f = self.control()
        d_a, d_f = self.disturbance()
        id_a = 1 / ACTUATOR_INDUCTANCE * (v_a - ACTUATOR_RESISTANCE * self.i_a)
        id_f = 1 / FLEXURE_INDUCTANCE * (v_f - FLEXURE_RESISTANCE * self.i_f)
        print self.state

        spring_force = CONSTANT_K * (1 + pow(self.o_f, 2)) * self.o_f
        damping_force = CONSTANT_B * (1 + ALPHA_B * abs(self.w_f)) * self.w_f
        friction_force = NOMINAL_BETA * sign_w_a
        flexture_force = FLEXURE_CONSTANT * self.i_f
        actuator_force = ACTUATOR_CONSTANT * self.i_a
        back_torque = self.J_f / self.J_a * actuator_force

        a_f = 1 / self.J_f * (
            flexture_force
            - back_torque
            - damping_force
            - spring_force
            + d_f)

        print  damping_force, self.w_f, (1 + ALPHA_B * abs(self.w_f)), CONSTANT_B
        a_a = 1 / self.J_a * (
            actuator_force
            - friction_force
            - flexture_force
            + spring_force
            + damping_force
            + back_torque
            + d_a
            - d_f)

        if sign_w_a == 0 and friction_can_hold:
            a_a = 0.0
        # state = "i_a", "o_a", "w_a", "i_f", "o_f", "w_f"
        return (id_a, self.w_a, a_a, id_f, self.w_f, a_f)

    def intersect_dt(self):
        id_a, w_a, a_a, id_f, w_f, a_f = self()
        if abs(a_a) < 1e-7:
            return 0.0
        return -w_a / a_a

    def skip(self, dt):
        t, Ss = self.state
        Sds = self()
        Ss = tuple((s + dt * sd for s, sd in zip(Ss, Sds)))
        self.state = t + dt, Ss

    @ly.Guard
    def pos(self):
        return ly.EventThrowIf[self.w_a >= 0]

    @pos.jumper
    def pos(self, init=False):
        if not init:
            # check for capture
            # capture if pos-mode acceleration pushes state neg-mode:
            if self(sign_w_a=1.0)[2] <= 0.0:
                # move closer to exact line
                self.skip(self.intersect_dt())
                self.enter_chatter()
                raise ly.StateJump()
        self.enter_pos()
        raise ly.Jump()

    @ly.Guard
    def neg(self):
        return ly.EventThrowIf[self.w_a <= 0]

    @neg.jumper
    def neg(self, init=False):
        if not init:
            # currently in pos-mode, entering neg-mode
            # if neg-mode would push pos-mode, then chatter.
            if self(sign_w_a=-1.0)[2] >= 0.0:
                # move to exact transition.
                self.skip(self.intersect_dt())
                # setup capture mode
                self.enter_chatter()
                raise ly.StateJump()

        self.enter_neg()
        raise ly.Jump()

    def sign_required_to_hold(self):
        id_a, w_a, a_a, id_f, w_f, a_f = self(
            sign_w_a=0.0, friction_can_hold=False)
        sign_w_a = (a_a * self.J_a) / NOMINAL_BETA
        return sign_w_a

    @ly.Guard
    def escape_neg(self):
        return ly.EventThrowIf[self.sign_required_to_hold() < -1.0]
        # return ly.EventThrowIf[self.xdd_neg() < self.xdd_follow()]

    @ly.Guard
    def escape_pos(self):
        return ly.EventThrowIf[self.sign_required_to_hold() > 1.0]
        # return ly.EventThrowIf[self.xdd_pos() > self.xdd_follow()]

    @escape_neg.jumper
    def escape_neg(self):
        self.events = [self.pos]
        self.sign_w_a = -1.0
        dt = self.intersect_dt() + 1e-7
        self.skip(dt)
        raise ly.StateJump()

    @escape_pos.jumper
    def escape_pos(self):
        self.events = [self.neg]
        self.sign_w_a = 1.0
        dt = self.intersect_dt() + 1e-7
        self.skip(dt)
        raise ly.StateJump()

    def sim(self, tf=100):
        stepper = ly.runge_kutta4(self, np.linspace(0, tf, 1e4))
        # stepper = ly.dormand_prince(self, tf)
        stepper.absolute_tolerance=1e-9
        stepper.relative_tolerance = 1e-9
        stepper.time_tolerance = 1e-7*tf
        for jump, time, state, guard in ly.hybrid_integrate(stepper):
            if guard:
                if guard()>=0:
                    print "True %s" % guard.name
                else:
                    print "False %s" % guard.name
            yield self


def main():
    print "hi"
    hd = HardDrive(w_a0=0.1, w_f0=0.0)
    dat = np.array(
        # [[h.t, h.o_a, h.o_f] for h in hd.sim(tf=5.564465e-10)]
        [[h.t, h.o_a, h.o_f] for h in hd.sim(tf=.1)]
        )
    plt.plot(dat[:,0],dat[:,[1,2]])
    # plt.show()

if __name__ == '__main__':
    main()
