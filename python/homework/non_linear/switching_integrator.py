import ly_utils as ly
from collections import namedtuple
import numpy as np
from math import *
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from plot_setup import *


@ly.named_state_system("IntegratorState", ["x", "xd"])
class SwitchedVSBIntegrator(object):

    """Benito's Integrator"""

    def __init__(self, x0=0, xd0=1e-7, kp=2.0, kn=0.5, lambda_=0.1):
        self.state = 0.0, (x0, xd0)
        self.kp = kp
        self.kn = kn
        self.k = kp
        self.lambda_ = lambda_
        self.l = 2.0
        self.g = 10.0
        self.m = 5.0
        try:
            if self.sigma() > 0:
                self.pos.jump(init=True)
            else:
                self.neg.jump(init=True)
        except ly.Jump:
            pass

        # self.events=[self.capture, self.]
    def sigma(self):
        return self.lambda_ * self.x + self.xd

    def del_sigma(self):
        return np.array([self.lambda_, 1.0])

    def sigma_dot(self):
        return sum([x*d for x,d in zip(self.del_sigma(),[self.x, self.xd])])

    # def xdd_physics(self):
    #     return -self.g / self.l * sin(self.x)

    def xdd_physics(self): # simplified
        return 0.0# -self.g / self.l * sin(self.x)

    def xdd_neg(self):
        return self.xdd_physics() - self.kn * self.x

    def xdd_pos(self):
        return self.xdd_physics() - self.kp * self.x

    # def xdd_neg(self):
    #     return self.xdd_physics() + self.eta

    # def xdd_pos(self):
    #     return self.xdd_physics() - self.eta

    def xdd_follow(self):
        sig_x, sig_xd = self.del_sigma()
        return -self.xd * sig_x / sig_xd

    def stability_estimate(self, alpha=0.1):
        kdelt = 0.5 * (-self.kp + self.kn)
        k0 = 0.5 * (-self.kp - self.kn)
        if -kdelt > abs(1 + alpha * k0) + abs(alpha * self.g / self.l):
            return True
        return False

    @ly.Guard
    def pos(self):
        return ly.EventThrowIf[self.sigma() > 0 and self.sigma_dot() > 0]

    def intersect_dt(self):
        del_sigma=self.del_sigma().reshape((1,2))
        sigma=self.sigma()
        x_dot = np.array([[self.xd],[self.xdd()]])
        den = del_sigma.dot(x_dot)[0,0]
        return -sigma/den

    def skip(self, dt):
        t, (x, xd) = self.state
        xdd = self.xdd()
        t+= dt
        x+= xd*dt
        xd += xdd*dt
        self.state = t, (x, xd)

    @pos.jumper
    def pos(self, init=False):
        if not init:
            if self.xdd_pos() <= self.xdd_follow():
                self.events = [self.escape_pos, self.escape_neg]
                dt=self.intersect_dt()
                self.skip(dt)
                self.xdd = self.xdd_follow
                raise ly.StateJump()
            # self.check_capture()
        # self.k = self.kp
        self.events = [self.neg]
        self.xdd = self.xdd_pos
        raise ly.Jump()

    @ly.Guard
    def neg(self):
        return ly.EventThrowIf[self.sigma() < 0 and self.sigma_dot() < 0]

    @neg.jumper
    def neg(self, init=False):
        if not init:
            if self.xdd_neg() >= self.xdd_follow():
                self.events = [self.escape_pos, self.escape_neg]
                dt=self.intersect_dt()
                self.skip(dt)
                self.xdd = self.xdd_follow
                raise ly.StateJump()

        self.events = [self.pos]
        self.xdd = self.xdd_neg
        raise ly.Jump()

    @ly.Guard
    def escape_neg(self):
        return ly.EventThrowIf[self.xdd_neg() < self.xdd_follow()]

    @ly.Guard
    def escape_pos(self):
        return ly.EventThrowIf[self.xdd_pos() > self.xdd_follow()]

    @escape_neg.jumper
    def escape_neg(self):
        self.events = [self.pos]
        self.xdd = self.xdd_neg
        dt=self.intersect_dt()+1e-7
        self.skip(dt)
        raise ly.Jump()

    @escape_pos.jumper
    def escape_pos(self):
        self.events = [self.neg]
        self.xdd = self.xdd_pos
        dt=self.intersect_dt()+1e-7
        self.skip(dt)
        raise ly.Jump()

    def __call__(self):
        return (self.xd, self.xdd())


def sim(pendulum,tf=100):
    stepper = ly.runge_kutta4(pendulum, np.linspace(0, tf, 10001))
    times, mag, xs, xds = [], [], [], []
    l, m, g = (pendulum.l, pendulum.m, pendulum.g)
    try:
        for jump, time, state, guard in ly.hybrid_integrate(stepper):
            # print time, state[0] / pi
            # if guard:
            #     print "%+0.2f"%state[0], "%+0.12f"%pendulum.sigma() ,guard.name
            times.append(time)
            mag.append(sqrt(state[0] ** 2 + state[1] ** 2))
            xs.append(pendulum.x)
            xds.append(pendulum.xd)
    except StopIteration as e:
        print e
    return xs, xds


def phase(kp, kn, lambda_, tf=10):
    fig = plt.figure(figsize=(4.5, 3))
    plt.hold(True)

    n1 = 2
    n2 = 20
    cap = lambda x: x if x <= 1 else 2 - x
    new_cycle = [custom_cm(0.9 * cap(x))
                 for x in np.linspace(0.0, 2.0, 11)][0:-1]
    plt.gca().set_color_cycle(new_cycle)
    mpl.rcParams['lines.linewidth'] = 1.9
    mpl.rcParams['lines.solid_capstyle'] = 'round'
    for xd0 in np.linspace(-2, 2, 2):
        for x0 in np.linspace(-4, 4, 20):
            p = SwitchedVSBIntegrator(x0=x0, xd0=xd0, kp=kp, kn=kn, lambda_=lambda_)
            x, xd = sim(p,tf=tf)
            color = next(plt.gca()._get_lines.color_cycle)
            plt.plot(x, xd, '-', color=color)

    plt.xlabel(r"$x$", size=12)
    plt.ylabel(r"$\dot{x}$", size=12)
    plt.title(r"Integrator---[$k_p$, $k_n$, $\lambda$]=[%.2f, %.2f, %.2f] Control" %
              (kp, kn, lambda_), family='cmb10', size=14)
    fig.tight_layout()
    return fig

def stability():
    fig = plt.figure(figsize=(4.5, 3))

    Path = mpath.Path
    path_data = [
    (Path.MOVETO, (0, 0)),
    (Path.LINETO, (1, 0)),
    (Path.LINETO, (1,1)),
    (Path.LINETO, (0,1)),
    (Path.CLOSEPOLY, (0,0))]
    plt.axis([-1, 1, -1, 1])
    plt.text(0.1,0.5,"not stable,\nbut center-like")
    plt.text(-0.5,-0.5,"not stable,\nnot center-like")
    codes, verts = zip(*path_data)
    path = mpath.Path(verts, codes)
    patch = mpatches.PathPatch(path, facecolor='r', alpha=0.5)
    plt.gca().add_patch(patch)
    plt.xlabel(r"$k_n$", size=14, family='cmb10')
    plt.ylabel(r"$k_p$", size=14, family='cmb10')
    fig.tight_layout()
    return fig

save = "../../../tex/homework-NonLinear/hw2/figs/si%s.pdf"

def main():
    phase(20,10,0.5).savefig(save%"1phase")
    phase(20,-10,0.5,tf=0.4).savefig(save%"2phase")
    # stability().savefig(save%"stab")
    plt.show()

if __name__ == "__main__":
    main()
