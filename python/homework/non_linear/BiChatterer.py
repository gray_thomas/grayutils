import ly_utils as ly
import numpy as np
from functools import partial


class BiChatterer(object):
    """
    This wraps a system object to allow chattering.

    Potentially wraps another BiChatterer wrapping a system!
    All systems bichatter linearly, that is to say, when switching between
    two alternate calls is super fast, the relative proportion of the calls
    behaves like a linear combination (weighted sum).
    S here acts like sigma. It represents "surface"
    pos == None means chatter about S()==0.
    pos == True means S() is positive.
    pos == False means S() is negative.
    """

    def __init__(self, base_system, S, dSdX, arg, eps_time=1e-9, name="", sigma_dot_bias=lambda: 0):

        self.S = S
        self.dSdX = dSdX
        self.arg = arg
        self.eps_time = eps_time
        self.base_system = base_system
        self.name = name
        self.sigma_dot_bias = sigma_dot_bias
        self.setup_guards()
        self.last_kwargs = {}
        if not hasattr(base_system, 'events'):
            raise TypeError("base_system %s needs events" %
                            base_system.__class__.__name__)
        self._events = []
        self.reset()

    def reset(self):
        if self.S() == 0.0:
            if self.dSdt(tristate=True) > 0:
                self.enter_state(tristate=True, skip=True)
                # skip forward
            elif self.dSdt(tristate=False) < 0:
                self.enter_state(tristate=False, skip=True)
                # skip forward
            else:
                self.enter_state(tristate=None)
        else:
            self.enter_state(tristate=self.S() > 0.0)

    def Xd_pos(self):
        kwargs = self.last_kwargs.copy()
        kwargs.update({self.arg: True})
        return self.base_system(**kwargs)

    def Xd_neg(self):
        kwargs = self.last_kwargs.copy()
        kwargs.update({self.arg: False})
        return self.base_system(**kwargs)

    def __call__(self, **kwargs):
        self.last_kwargs.update(kwargs.copy())
        if self.tristate == True:
            return self.Xd_pos()
        if self.tristate == False:
            return self.Xd_neg()
        if self.tristate == None:
            Xd_pos = np.array(self.Xd_pos())
            Xd_neg = np.array(self.Xd_neg())
            dSdX = np.array(self.dSdX())
            den = dSdX.dot(Xd_neg - Xd_pos)
            if den == 0.0:
                return tuple(Xd_pos)
            alpha = dSdX.dot(Xd_neg) / den
            beta = -dSdX.dot(Xd_pos) / den
            return tuple(alpha * Xd_pos + beta * Xd_neg)

    def intersect_dt(self):
        Xd = np.array(self())
        dSdX = np.array(self.dSdX())
        S = self.S()
        den = dSdX.dot(Xd)+self.sigma_dot_bias()
        if den == 0.0:
            return 0.0
        dt = -S / den
        if dt < 0.0:
            dt = 0.0
        if dt > 1e2:
            dt = 1e2
        return dt

    def skip(self, dt, dx):
        t, x = self.state
        self.state = t + dt, tuple([s + sd * dt for s, sd in zip(x, dx)])

    @property
    def state(self):
        return self.base_system.state

    @state.setter
    def state(self, value):
        self.base_system.state = value

    @property
    def events(self):
        # Memoized events necessary to avoid reseting them?
        if self.state_events + self.base_system.events != self._events:
            self._events = self.state_events + self.base_system.events
        return self._events

    def enter_state(self, tristate='init', skip=False):
        self.tristate = tristate
        if tristate == None:
            self.state_events = [self.escape_pos, self.escape_neg]
        elif tristate == True:
            self.state_events = [self.neg]
        elif tristate == False:
            self.state_events = [self.pos]
        else:
            self.reset()
        if skip:
            dt = self.intersect_dt() + self.eps_time
            self.skip(dt, self())
            if self.tristate == False:
                assert (self.S() < 0)
            if self.tristate == True:
                assert (self.S() > 0)

    def dSdt(self, tristate=True):
        dSdX = np.array(self.dSdX())
        if tristate == True:
            dXdt = np.array(self.Xd_pos())
        elif tristate == False:
            dXdt = np.array(self.Xd_neg())
        else:
            return 0.0
        return dSdX.dot(dXdt)+self.sigma_dot_bias()

    def newton_root(self):
        # two steps of newton's root finding method
        for i in range(0, 2):
            self.skip(self.intersect_dt(), self())

    def setup_guards(self):

        #

        def pos():
            assert (self.dSdt(tristate=False) > 0)
            self.newton_root()
            assert (self.dSdt(tristate=False) > 0)
            if self.dSdt(tristate=True) <= 0.0:
                self.enter_state(tristate=None)
                raise ly.StateJump()
            self.enter_state(tristate=True)
            raise ly.Jump()

        self.pos = ly.Guard2(
            call=lambda: ly.EventGEQ0(self.S()), jump=pos,
            name="%s.pos" % self.name)

        #

        def neg():
            prev = self.dSdt(tristate=True)
            prevs = self.state
            if not self.dSdt(tristate=True) < 0:
                print self.state
                print self.S()
                print self.dSdt(tristate=True)
                print self.dSdt(tristate=False)
            # print self.dSdt(tristate=True)
            assert (self.dSdt(tristate=True) < 0)
            self.newton_root()
            if not (self.dSdt(tristate=True) < 0):
                print "was", prev
                print "now", self.dSdt(tristate=True)
                print "prev-state", prevs
                print "now-state", self.state
            assert (self.dSdt(tristate=True) < 0)
            if self.dSdt(tristate=False) >= 0.0:
                self.enter_state(tristate=None)
                raise ly.StateJump()
            self.enter_state(tristate=False)
            raise ly.Jump()

        self.neg = ly.Guard2(
            call=lambda: ly.EventLEQ0(self.S()), jump=neg,
            name="%s.neg" % self.name)

        #

        def escape_neg():
            assert (self.dSdt(tristate=False) < 0)
            self.enter_state(tristate=False, skip=True)
            raise ly.StateJump()

        self.escape_neg = ly.Guard2(
            call=lambda: ly.EventLEQ0(
                self.dSdt(tristate=False)),
            jump=escape_neg, far_side=True,
            name="%s.escape_neg" % self.name)

        #

        def escape_pos():
            assert (self.dSdt(tristate=True) > 0)
            self.enter_state(tristate=True, skip=True)
            raise ly.StateJump()

        self.escape_pos = ly.Guard2(
            call=lambda: ly.EventGEQ0(
                self.dSdt(tristate=True)), jump=escape_pos, far_side=True,
            name="%s.escape_pos" % self.name)


@ly.named_state_system("IntegratorState", ["x", "xd"])
class SlidingModeIntegrator(object):

    """Simple Integrator"""

    def __init__(self, x0=0, xd0=1e-7, eta=1, lambda_=0.1):
        self.state = 0.0, (x0, xd0)
        self.eta = eta
        self.events = []
        self.lambda_ = lambda_
        self.chatterer = BiChatterer(
            self, self.sigma, self.del_sigma, "u", name="SMC")

    def sigma(self):
        return self.lambda_ * self.x + self.xd

    def del_sigma(self):
        return np.array([self.lambda_, 1.0])

    def __call__(self, u=None):
        if u == None:
            raise ValueError("must specify u")
        u_val = -self.eta if u else self.eta
        return self.xd, u_val

    def sim(self, tf=100):
        sys = self.chatterer
        stepper = ly.runge_kutta4(sys, np.linspace(0, tf, 10001))
        try:
            for jump, time, state, guard in ly.hybrid_integrate(stepper):
                if guard:
                    print guard
                yield self
        except StopIteration as e:
            print e


@ly.named_state_system("IntegratorState", ["x", "xd"])
class SlidingModePendulum(object):

    """Simple Pendulum"""

    def __init__(self, x0=0, xd0=1e-7, eta=1, lambda_=0.1, alpha=0.3):
        self.state = 0.0, (x0, xd0)
        self.eta = eta
        self.alpha = alpha
        self.events = []
        self.lambda_ = lambda_
        self.chatterer = BiChatterer(
            self, self.sigma, self.del_sigma, "u", name="SMC", eps_time=1e-8)

    def sigma(self):
        return self.lambda_ * self.x + self.xd

    def del_sigma(self):
        return np.array([self.lambda_, 1.0])

    def __call__(self, u=None):
        if u == None:
            raise ValueError("must specify u")
        u_val = -self.eta if u else self.eta
        return self.xd, u_val - self.alpha * np.sin(self.x)

    def sim(self, tf=100, n=1001):
        sys = self.chatterer
        stepper = ly.runge_kutta4(sys, np.linspace(0, tf, n))
        try:
            for jump, time, state, guard in ly.hybrid_integrate(stepper):
                if guard:
                    print guard
                yield self
        except StopIteration as e:
            print e


@ly.named_state_system("IntegratorState", ["x", "xd"])
class FrictionalSlidingModeIntegrator(object):

    """Simple Integrator"""

    def __init__(self, x0=0, xd0=1e-7, eta=1, fric=0.1, lambda_=0.1):
        self.state = 0.0, (x0, xd0)
        self.events = []
        self.lambda_ = lambda_
        self.eta = eta
        self.fric = fric
        self.friction = BiChatterer(
            self, self.get_xd, lambda: (0, 1), 'f_pos', name="friction")
        self.chatterer = BiChatterer(
            self.friction, self.sigma, lambda: (lambda_, 1), 'u_pos', name="control")

    def get_xd(self):
        return self.xd

    def sigma(self):
        return self.lambda_ * self.x + self.xd

    def __call__(self, f_pos=None, u_pos=None):
        if f_pos == None:
            raise ValueError("f must be specified")
        if u_pos == None:
            raise ValueError("u must be specified")
        u = -self.eta if u_pos else self.eta
        f = -self.fric if f_pos else self.fric
        return self.xd, u + f

    def sim(self, tf=100):
        sys = self.chatterer
        stepper = ly.runge_kutta4(sys, np.linspace(0, tf, 101))
        try:
            for jump, time, state, guard in ly.hybrid_integrate(stepper):
                if guard:
                    print guard
                yield self
        except StopIteration as e:
            print e


def main():
    # sys = SlidingModeIntegrator(x0=2, xd0=1)
    # sys = FrictionalSlidingModeIntegrator(x0=2, xd0=1)
    sys = SlidingModePendulum(x0=0.0, xd0=-0.0, alpha=2.0)
    print sys.chatterer.tristate
    for p in sys.sim(tf=10, n=101):
        print
        print p.t, p.x, p.xd
        print[(e.name, e()) for e in p.chatterer.events], p.chatterer.S()
        print
        # print[(e.name, e()) for e in p.friction.events], p.friction.S()
    import matplotlib.pyplot as plt
    from plot_setup import *
    fig = plt.figure(figsize=(6.5, 4))
    for x0 in np.linspace(-6, 6, 21):
        for xd0 in [-0.2, 0, 0.2]:
            sys = SlidingModePendulum(x0=x0, xd0=xd0, alpha=2.0)
            dat = np.array([[p.t, p.x, p.xd] for p in sys.sim(tf=10)])
            plt.plot(dat[:, 1], dat[:, 2])
    plt.show()


if __name__ == "__main__":
    main()
