from lyapunov import *

class switch(object):
    def __init__(self, value):
        self.value = value
        self.fall = False

    def __iter__(self):
        """Return the match method once, then stop"""
        yield self.match
        raise StopIteration
    
    def match(self, *args):
        """Indicate whether or not to enter a case suite"""
        if self.fall or not args:
            return True
        elif self.value in args: # changed for v1.5, see below
            self.fall = True
            return True
        else:
            return False


def hybridsystemfunctor(system):
    """
    Decorator that adds defaulted time and state arguments to a system class
    or wraps a system function in a system class. 
    This should make it compatible with both Lyapunov and SciPy.

    Like any decorator, hybridsystemfunctor may also be used as a function that 
    takes and returns a class for function. If the given class does not have 
    `__call__` defined to take no arguments, it will raise a 
    NotImplementedError.

    For lyapunov systems with state abstracting hybrid elements, 
    hybridsystemfunctor ensures that the reset() method is used to calculate
    these abstracted states when called as a scipy system. 
    """
    if inspect.isfunction(system):
        class system_class(object):
            def __init__(self, *parameters):
                self.parameters = parameters
            state = state_property(xname="x", tname="t")
            def __call__(self, t=None, x=None):
                if t is not None and x is not None:
                    self.state = t, x
                return system(t, x, *self.parameters)
        return system_class
    else:
        argspec = inspect.getargspec(system.__call__)
        if len(argspec.args) != 1:
            raise NotImplementedError("System __call__ has arity > 0")
        original_call = system.__call__
        def new_call(self, t=None, x=None):
            if t is not None and x is not None:
                self.state = t, x
                self.reset() # resets hybrid states
            return original_call(self)
        system.__call__ = new_call
        return system