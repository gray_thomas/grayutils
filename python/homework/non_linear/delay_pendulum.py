import ly_utils as ly
from collections import namedtuple
import numpy as np
from math import *
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import axes3d
from plot_setup import *


@ly.named_state_system("PendulumState", ["x", "xd", "u"])
class LPSMCPendulum(object):

    """Benito's Pendulum, sliding mode control with low pass."""

    def __init__(self, x0=0, xd0=1e-7, eta=2.0, ku=0.5, lambda_=0.1):
        self.state = 0.0, (x0, xd0, 0.0)
        self.eta = eta
        self.ku = ku
        self.lambda_ = lambda_
        self.l = 2.0
        self.g = 10.0
        self.m = 5.0
        try:
            if self.sigma() > 0:
                self.pos.jump(init=True)
            else:
                self.neg.jump(init=True)
        except ly.Jump:
            pass

        # self.events=[self.capture, self.]
    def sigma(self):
        return self.lambda_ * self.x + self.xd

    def xdd_physics(self):
        return -self.g / self.l * sin(self.x)

    @ly.Guard
    def pos(self):
        return ly.EventThrowIf[self.sigma() > 0]

    @pos.jumper
    def pos(self, init=False):
        self.events = [self.neg]
        self.u_des = - self.eta
        raise ly.Jump()

    @ly.Guard
    def neg(self):
        return ly.EventThrowIf[self.sigma() < 0]

    @neg.jumper
    def neg(self, init=False):
        self.events = [self.pos]
        self.u_des = self.eta
        raise ly.Jump()

    def __call__(self):
        return (self.xd,
                -self.g / self.l * sin(self.x) + self.u,
                self.ku * (self.u_des - self.u))

    def sim(self, tf=100):
        stepper = ly.runge_kutta4(self, np.linspace(0, tf, 10001))
        l, m, g = (self.l, self.m, self.g)
        try:
            for jump, time, state, guard in ly.hybrid_integrate(stepper):
                yield self
        except StopIteration as e:
            print e

save_name = "../../../tex/homework-NonLinear/hw2/figs/LPSMCPendulum_%s.pdf"


def sim(pendulum, tf= 100):
    stepper = ly.runge_kutta4(pendulum, np.linspace(0, tf, 10001))
    times, mag, xs, xds = [], [], [], []
    l, m, g = (pendulum.l, pendulum.m, pendulum.g)
    try:
        for jump, time, state, guard in ly.hybrid_integrate(stepper):
            times.append(time)
            mag.append(sqrt(state[0] ** 2 + state[1] ** 2))
            xs.append(pendulum.x)
            xds.append(pendulum.xd)
    except StopIteration as e:
        print e
    return xs, xds


def sim2(pendulum):
    stepper = ly.runge_kutta4(pendulum, np.linspace(0, 100, 10001))
    times, mag, xs, xds = [], [], [], []
    l, m, g = (pendulum.l, pendulum.m, pendulum.g)
    try:
        for jump, time, state, guard in ly.hybrid_integrate(stepper):
            yield pendulum

    except StopIteration as e:
        print e


def plot1(eta, ku, lambda_, n1, n2):
    fig = plt.figure(figsize=(6.5, 3))
    plt.hold(True)

    cap = lambda x: x if x <= 1 else 2 - x
    new_cycle = [custom_cm(0.9 * cap(x))
                 for x in np.linspace(0.0, 2.0, 11)][0:-1]
    plt.gca().set_color_cycle(new_cycle)
    mpl.rcParams['lines.linewidth'] = 1.9
    mpl.rcParams['lines.solid_capstyle'] = 'round'
    for xd0 in np.linspace(-2, 2, 2):
        for x0 in np.linspace(-4, 4, 20):
            p = LPSMCPendulum(x0=x0, xd0=xd0, eta=eta, ku=ku, lambda_=0.1)
            x, xd, times = sim(p)
            color = next(plt.gca()._get_lines.color_cycle)
            plt.plot(x, xd, '-', color=color)
            # plt.plot(x0, xd0, 'o', color = color)
            # plt.plot(x[-1], xd[-1], 'o', color = color)

    # plt.plot([0,10],[0,-1])
    plt.xlabel(r"$x$", size=12)
    plt.ylabel(r"$\dot{x}$", size=12)
    plt.title(r"Pendulum---[$\eta$, $k_u$, $\lambda$]=[%.2f, %.2f, %.2f] Control" %
              (eta, ku, lambda_), family='cmb10', size=14)
    fig.tight_layout()
    return fig

def phase(eta=2.0, ku=0.5, lambda_=0.1, tf=10, special=None):
    fig = plt.figure(figsize=(4.5, 3))
    plt.hold(True)

    n1 = 2
    n2 = 20
    cap = lambda x: x if x <= 1 else 2 - x
    new_cycle = [custom_cm(0.9 * cap(x))
                 for x in np.linspace(0.0, 2.0, 5)][0:-1]
    plt.gca().set_color_cycle(new_cycle)
    mpl.rcParams['lines.linewidth'] = 1.9
    mpl.rcParams['lines.solid_capstyle'] = 'round'
    for xd0 in np.linspace(-2, 2, 2):
        for x0 in np.linspace(-4, 4, 5):
            p = LPSMCPendulum(x0=x0, xd0=xd0, eta=eta, ku=ku, lambda_=lambda_)
            x, xd = sim(p, tf=tf)
            color = next(plt.gca()._get_lines.color_cycle)
            plt.plot(x, xd, '-', color=color)

    if special:
        for x0, xd0 in special:
            p = LPSMCPendulum(x0=x0, xd0=xd0, eta=eta, ku=ku, lambda_=lambda_)
            x, xd = sim(p, tf=tf)
            plt.plot(x, xd, 'r', lw=3)
    plt.xlabel(r"$x$", size=12)
    plt.ylabel(r"$\dot{x}$", size=12)
    plt.title(r"Pendulum---[$\eta$, $k_u$, $\lambda$]=[%.2f, %.2f, %.2f] Control" %
              (eta, ku, lambda_), family='cmb10', size=14)
    fig.tight_layout()
    return fig


def u_eq(eta=2.0, ku=0.5, lambda_=0.1, tf=10, x0=-4, xd0=-2):
    fig = plt.figure(figsize=(8.5, 5))

    pen = LPSMCPendulum(x0=x0, xd0=xd0, eta=eta, ku=ku, lambda_=lambda_)
    res = np.array([[p.t, p.xd + p.lambda_ * p.x, p.xdd_physics(),
                     p.u, -p.xd * p.lambda_ - p.xdd_physics()] for p in pen.sim(tf=tf)])

    ax3 = fig.add_subplot(313)
    ax3.plot(res[:,0],res[:,4],'b')
    ax3.plot(res[:,0],res[:,3],'r')
    ax3.set_ylabel(r"$u(t)$")
    ax3.set_xlabel(r"$t$")
    ax1 = fig.add_subplot(311, sharex=ax3)
    ax1.plot(res[:, 0], res[:, 1], 'r')
    ax1.set_ylabel(r"$\sigma(t)$")
    plt.title(r"Pendulum---[$\eta$, $k_u$, $\lambda$]=[%.2f, %.2f, %.2f] Control" %
              (eta, ku, lambda_), family='cmb10', size=14)
    ax2 = fig.add_subplot(312, sharex=ax3)
    ax2.plot(res[:, 0], res[:, 2], 'r')
    ax2.set_ylabel(r"$-\alpha\sin(x(t))$")

    fig.tight_layout()
    return fig


def main():
    # plot1(20, 5, 0.1, 2, 4).savefig(save_name % "1")
    # u_eq(eta=2.0, ku=0.5, lambda_=0.1, x0=-4, xd0=-2).savefig(save_name % "2eq")
    # phase(eta=2.0, ku=0.5, lambda_=0.1, special=[(-4, -2)]).savefig(save_name % "2phase")

    # u_eq(eta=2.0, ku=5.0, lambda_=0.1, x0=-4, xd0=-2).savefig(save_name % "3eq")
    # phase(eta=2.0, ku=5.0, lambda_=0.1, special=[(-4, -2)]).savefig(save_name % "3phase")

    # u_eq(eta=5.0, ku=5.0, lambda_=0.1, x0=-4, xd0=-2).savefig(save_name % "4eq")
    # phase(eta=5.0, ku=5.0, lambda_=0.1, special=[(-4, -2)]).savefig(save_name % "4phase")

    # u_eq(eta=3.0, ku=3.0, lambda_=0.1, x0=-4, xd0=-2).savefig(save_name % "5eq")
    # phase(eta=3.0, ku=3.0, lambda_=0.1, special=[(-4, -2)]).savefig(save_name % "5phase")

    # u_eq(eta=3.0, ku=40.0, lambda_=0.1, x0=-4, xd0=-2).savefig(save_name % "6eq")
    # phase(eta=3.0, ku=40.0, lambda_=0.1, special=[(-4, -2)]).savefig(save_name % "6phase")

    # u_eq(eta=6.0, ku=40.0, lambda_=0.1, x0=-4, xd0=-2).savefig(save_name % "7eq")
    # phase(eta=6.0, ku=40.0, lambda_=0.1, special=[(-4, -2)]).savefig(save_name % "7phase")
    # u_eq(eta=6.0, ku=4.0, lambda_=0.1, x0=-4, xd0=-2).savefig(save_name % "8eq")
    # phase(eta=6.0, ku=4.0, lambda_=0.1, special=[(-4, -2)]).savefig(save_name % "8phase")
    u_eq(eta=6.0, ku=0.4, lambda_=0.1, tf=100, x0=-4, xd0=-2).savefig(save_name % "9eq")
    phase(eta=6.0, ku=0.4, lambda_=0.1, tf =100, special=[(-4, -2)]).savefig(save_name % "9phase")

    plt.show()

if __name__ == "__main__":
    main()
