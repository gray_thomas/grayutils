import ly_utils as ly
import matplotlib.pyplot as plt
from plot_setup import *
from BiChatterer import BiChatterer
from functools import partial
from math import sqrt
import random as rand

from harddrive_constants import *
# alpha_b = 0
# alpha_k = 0


@ly.named_state_system("PFState", ["y", "yd", "ydd", "psi", "psid", "psidd"])
class ELO(object):

    def __init__(self, sys, k=4e3):
        self.k = k
        self.sys = sys
        o_a, o_f, i_a, i_f = sys.measurement()
        self.state = 0, (o_a + o_f, 0, 0, o_a - o_f, 0, 0)

    def yddd_prediction(self):
        state = de_convert_state(self.state[1])
        dyn = get_dynamics(state, (0, 0), self.sys.voltages(), sign_w_a=0)
        return tuple(convert_jac(state).dot(np.array(dyn)))[2]

    def psiddd_prediction(self):
        state = de_convert_state(self.state[1])
        dyn = get_dynamics(state, (0, 0), self.sys.voltages(), sign_w_a=0)
        return tuple(convert_jac(state).dot(np.array(dyn)))[5]

    @property
    def yddd(self):
        o_a, o_f, i_a, i_f = self.sys.measurement()
        return (
            self.yddd_prediction()
            - 1  * (self.y - (o_a + o_f))
            - 2 / self.k * self.yd
            - 2 / self.k**2 * self.ydd) * self.k**3

    @property
    def psiddd(self):
        o_a, o_f, i_a, i_f = self.sys.measurement()
        return (
            self.psiddd_prediction()
            - 1 * (self.psi - (o_a - o_f))
            - 2 / self.k * self.psid
            - 2 / self.k**2 * self.psidd) * self.k**3

    @property
    def est(self):
        return self.state[1]

    @property
    def orig(self):
        return de_convert_state(self.state[1])

    def __call__(self):
        return self.yd, self.ydd, self.yddd, self.psid, self.psidd, self.psiddd


@ly.named_state_system("PFState", ["x", "xd", "xdd"])
class PF(object):

    def __init__(self, func, k=10000):
        self.k = k
        self.state = 0, (func(0), 0, 0)
        self.func = func

    def __call__(self):
        xddd = self.k**3 * (self.func(self.t) - self.x - 2 *
                            (self.xd / self.k) - 2 * (self.xdd / self.k**2))
        return self.xd, self.xdd, xddd


@ly.named_state_system("DiffHDState", ["o_a", "o_f"])
class DiffHD(object):

    def __init__(self, hd, la, lf):
        self.state = 0, (0, 0)
        self.la = la
        self.lf = lf
        self.hd = hd

    @property
    def est(self):
        o_a, o_f, i_a, i_f = self.hd.measurement()
        return i_a, o_a, self.la * (o_a - self.o_a), i_f, o_f, self.lf * (o_f - self.o_f)

    def __call__(self):
        o_a, o_f, i_a, i_f = self.hd.measurement()
        return self.la * (o_a - self.o_a), self.lf * (o_f - self.o_f)


@ly.named_state_system("HDObsState", ["i_a", "o_a", "w_a", "i_f", "o_f", "w_f"])
class HDObs(object):

    def __init__(self, hd):
        self.hd = hd
        self.state = 0, (0, 0, 0, 0, 0, 0)

    def __call__(self):
        o_a, o_f, i_a, i_f = hd.measurement
        Xd_est = np.array(self.e_call())
        C = np.array()
        return tuple(Xd_est)

    def e_call(self, sign_w_a=0):
        i_a, o_a, w_a, i_f, o_f, w_f = self.state[1]
        v_a, v_f = self.hd.v_a, self.hd.v_f
        id_f = self.v_f / Lf - Rf / Lf * i_f
        id_a = self.v_a / La - Ra / La * i_a
        wd_a = (
            1 / Jastar * (
                Ka * i_a
                # - beta * {True: 1, False: -1, 0: 0}[sign_w_a]
                - (Kf * self.i_f + d_f)
            )
            + Jf / Jastar * (
                Ka / Ja * i_a
                + B * (1 + alpha_b * abs(w_f)) * w_f
                + K * (1 + alpha_k * o_f**2) * o_f
            )
        )
        wd_f = (
            1 / Jf * (Kf * i_f + d_f)
            - Ka * i_a / Ja
            - B * (1 + alpha_b * abs(w_f)) * w_f
            - K * (1 + alpha_k * o_f**2) * o_f)
        return (id_a, w_a, wd_a, id_f, w_f, wd_f)


@ly.named_state_system("HardDriveState", ["i_a", "o_a", "w_a", "i_f", "o_f", "w_f"])
class HardDrive(object):
    """A hard drive control mechanism with an actuator and a flexure."""

    def __init__(self, F, i_a0=0, o_a0=0, w_a0=0, i_f0=0, o_f0=0, w_f0=0, goal=lambda x: 0):
        self.F = np.array(F)
        self.goal = PF(goal, k=1e5)
        self.state = 0, (i_a0, o_a0, w_a0, i_f0, o_f0, w_f0)
        self.events = []
        self.estimator = ELO(self)
        self.fric = BiChatterer(
            self, lambda: self.w_a, lambda: (0, 0, 1, 0, 0, 0),
            "sign_w_a", name="friction", eps_time=1e-12)
        self.par = ly.ParallelSystems([self.fric, self.estimator, self.goal])

    @property
    def system(self):
        return self.par

    def reset(self, t, state):
        self.state = t, state
        self.fric.reset()

    @property
    def d_a(self):
        return 0.0

    @property
    def d_f(self):
        return 0.0

    def measurement(self):
        return (self.o_a, self.o_f, self.i_a, self.i_f)

    def get_output(self):
        t = convert_state(self.state[1])

    def voltages(self):
        return self.v_a, self.v_f

    @property
    def v_a(self):
        return -self.F[0, :].reshape(-1).dot(np.array(self.state[1]))

    @property
    def v_f(self):
        return -self.F[1, :].reshape(-1).dot(np.array(self.state[1]))

    def __call__(self, sign_w_a=0):
        i_a, o_a, w_a, i_f, o_f, w_f = self.state[1]
        print self.state[1]
        v_a, v_f = self.v_a, self.v_f
        d_a, d_f = self.d_a, self.d_f
        id_f = self.v_f / Lf - Rf / Lf * i_f
        id_a = self.v_a / La - Ra / La * i_a
        wd_a = (
            1 / Jastar * (
                Ka * i_a
                - beta * {True: 1, False: -1, 0: 0}[sign_w_a]
                + d_a
                - (Kf * self.i_f + d_f)
            )
            + Jf / Jastar * (
                Ka / Ja * i_a
                + B * (1 + alpha_b * abs(w_f)) * w_f
                + K * (1 + alpha_k * o_f**2) * o_f
            )
        )
        wd_f = (
            1 / Jf * (Kf * i_f + d_f)
            - Ka * i_a / Ja
            - B * (1 + alpha_b * abs(w_f)) * w_f
            - K * (1 + alpha_k * o_f**2) * o_f)
        return (id_a, w_a, wd_a, id_f, w_f, wd_f)

    def sim(self, tf=100, n=1001):
        sys = self.system
        stepper = ly.dormand_prince(sys, np.linspace(self.t, tf, n))
        # stepper = ly.euler(sys, np.linspace(self.t, tf, n))
        stepper.absolute_tolerance = 1e-4
        stepper.relative_tolerance = 1e-4
        stepper.time_tolerance = 1e-16
        for jump, time, state, guard in ly.hybrid_integrate(stepper):
            ly.check_NaN(self)
            yield self


def approximate_matrix(F, eps):
    h = HardDrive(F)
    z = np.zeros(6)
    h.reset(0, tuple(z))
    eh = np.array(h(sign_w_a=0))
    m = np.zeros((6, 6))
    for i in range(0, 6):
        ei = np.zeros(6)
        ei[i] = eps
        si = tuple(z + ei)
        h.reset(0, si)
        m[:, i] = np.array(h(sign_w_a=0)) / eps
    for r in m:
        for c in r:
            print c
    print("[\n\t" + ";\n\t".join([", ".join(["%+.4e"] * 6)] * 6) +
          "\n]") % tuple((c for r in m for c in r))
    return m


def plot_general(F, tf=1e-6, n=1001, scale=1e-3, goal=0):
    fig = plt.figure(figsize=(6.5, 11))

    rand.seed(25)
    st = tuple([scale * (1 - 2. * rand.random()) for x in range(0, 6)])

    m = approximate_matrix(F, scale * 1e4)
    print st
    hd = HardDrive(F, w_a0=st[0] + 2e-7, o_a0=st[1] - goal, i_a0=st[2],
                   w_f0=st[3], o_f0=st[4], i_f0=st[5])
    d, d1, d2 = [], [], []
    s0 = np.array(hd.state[1]).reshape((6, 1))
    t0 = hd.t
    for h in hd.sim(tf=tf, n=n):
        print h.t, sum(s**2 for s in h.state[1])
        y, yd, ydd, s, sd, sdd = convert_state(h.state[1])
        d.append([h.t, h.v_a, h.v_f, y + goal, yd, ydd, s, sd, sdd])
        y, yd, ydd, s, sd, sdd = convert_state(h.estimator.est)
        d2.append([y + goal, yd, ydd, s, sd, sdd])
        s0 = s0 + (h.t - t0) * m.dot(s0)
        t0 = h.t
        y, yd, ydd, s, sd, sdd = convert_state(h.state[1])
        d1.append(s0)
    labels = [
        r"$V_A$",
        r"$V_F$",
        r"$y$",
        r"$\dot{y}$",
        r"$\ddot{y}$",
        r"$\phi_1$",
        r"$\phi_2$",
        r"$\phi_3$"
    ]
    dat = np.array(d)
    dat1 = np.array(d1)
    dat2 = np.array(d2)
    n = 8
    axs = [fig.add_subplot(n, 1, 1)]
    for i in range(1, n):
        axs.append(fig.add_subplot(n, 1, i + 1, sharex=axs[0]))
    for i in range(0, n):
        axs[i].plot(dat[:, 0], dat[:, i + 1])
        axs[i].set_ylabel(labels[i], size=14)
    for i in range(2, n):
        axs[i].hold(True)
        axs[i].plot(dat[:, 0], dat2[:, i - 2])
    axs[-1].set_xlabel(r"$t$", size=14)

    fig.tight_layout()
    return fig

name_string = "../../../tex/homework-NonLinear/hw3/figs/E_HD%s.pdf"


def main():

    F = np.array(
        # [[+7.145797438234e-06,+7.146333863246e-01,+7.145854540441e-03,+9.579116650313e-06,+2.325924320754e-03,+9.646176172696e-09],
        # [+1.915823330063e-05,+2.427548404543e+00,+2.340797637026e-02,+2.273854880738e+03,-1.477501639648e+03,+9.495770995744e-01]] # opt-- prioritizes y
        [[+4.081715068115e-06, +4.081799675928e-01, +4.081725922349e-03, +4.127549905881e-07, +4.930763484872e-09, +4.491002614055e-10],
         [+8.255099811762e-07, +9.916344646073e+00, +3.470987190916e-03, +1.445825521038e+01, -6.272201325066e-01, +4.892731197955e-05]]
    )
    # plot_fake_goal_finding(F, tf=2.0e-2, n=10001, scale=1e-7, goal=3e-0).savefig(name_string % "_C8")
    plot_general(F, tf=2.0e-3, n=10001, scale=1e-5,
                 goal=3e-2).savefig(name_string % "_4")

    plt.show()

####################################################

####################################################


####################################################


####################################################

def plot_fake_goal_finding(F, tf=1e-6, n=1001, scale=1e-3, goal=0):
    fig = plt.figure(figsize=(6.5, 11))

    rand.seed(25)
    st = tuple([scale * (1 - 2. * rand.random()) for x in range(0, 6)])

    m = approximate_matrix(F, scale * 1e4)
    print st
    hd = HardDrive(F, w_a0=st[0] + 2e-7, o_a0=st[1] - goal, i_a0=st[2],
                   w_f0=st[3], o_f0=st[4], i_f0=st[5])
    d, d1, d2 = [], [], []
    s0 = np.array(hd.state[1]).reshape((6, 1))
    t0 = hd.t
    for h in hd.sim(tf=tf, n=n):
        print h.t, sum(s**2 for s in h.state[1])
        y, yd, ydd, s, sd, sdd = convert_state(h.state[1])
        d.append([h.t, h.v_a, h.v_f, y + goal, yd, ydd, s, sd, sdd])
        y, yd, ydd, s, sd, sdd = convert_state(h.estimator.est)
        d2.append([y + goal, yd, ydd, s, sd, sdd])
        s0 = s0 + (h.t - t0) * m.dot(s0)
        t0 = h.t
        y, yd, ydd, s, sd, sdd = convert_state(h.state[1])
        d1.append(s0)
    labels = [
        r"$V_A$",
        r"$V_F$",
        r"$y$",
        r"$\dot{y}$",
        r"$\ddot{y}$",
        r"$\phi_1$",
        r"$\phi_2$",
        r"$\phi_3$"
    ]
    dat = np.array(d)
    dat1 = np.array(d1)
    dat2 = np.array(d2)
    n = 8
    axs = [fig.add_subplot(n, 1, 1)]
    for i in range(1, n):
        axs.append(fig.add_subplot(n, 1, i + 1, sharex=axs[0]))
    for i in range(0, n):
        axs[i].plot(dat[:, 0], dat[:, i + 1])
        axs[i].set_ylabel(labels[i], size=14)
    for i in range(2, n):
        axs[i].hold(True)
        axs[i].plot(dat[:, 0], dat2[:, i - 2])
    axs[-1].set_xlabel(r"$t$", size=14)

    fig.tight_layout()
    return fig


def plotC3(F, tf=1e-6, scale=1e-3):
    fig = plt.figure(figsize=(6.5, 11))

    rand.seed(25)
    st = tuple([scale * (1 - 2. * rand.random()) for x in range(0, 6)])

    m = approximate_matrix(F, scale * 1e4)
    print st
    hd = HardDrive(F, w_a0=st[0] + 2e-7, o_a0=st[1], i_a0=st[2],
                   w_f0=st[3], o_f0=st[4], i_f0=st[5])
    d, d1 = [], []
    s0 = np.array(hd.state[1]).reshape((6, 1))
    tf, n = tf, 1001
    t0 = hd.t
    for h in hd.sim(tf=tf, n=n):
        print h.t, sum(s**2 for s in h.state[1])
        y, yd, ydd, s, sd, sdd = convert_state(h.state[1])
        d.append([h.t, h.v_a, h.v_f, h.i_a, h.o_a, h.w_a, h.i_f, h.o_f, h.w_f])
        s0 = s0 + (h.t - t0) * m.dot(s0)
        t0 = h.t
        d1.append(s0)
    labels = [
        r"$V_A$",
        r"$V_F$",
        r"$i_A$",
        r"$\theta_A$",
        r"$\omega_A$",
        r"$i_F$",
        r"$\theta_F$",
        r"$\omega_F$"
    ]
    dat = np.array(d)
    dat2 = np.array(d1)
    n = 8
    axs = [fig.add_subplot(n, 1, 1)]
    for i in range(1, n):
        axs.append(fig.add_subplot(n, 1, i + 1, sharex=axs[0]))
    for i in range(0, n):
        axs[i].plot(dat[:, 0], dat[:, i + 1])
        axs[i].set_ylabel(labels[i], size=14)
    # for i in range(2, n):
    #     axs[i].hold(True)
    #     axs[i].plot(dat[:, 0], dat2[:, i - 2])
    axs[-1].set_xlabel(r"$t$", size=14)

    fig.tight_layout()
    return fig


def plotC():
    fig = plt.figure(figsize=(5.5, 7))
    # plt.hold(True)

    st = tuple([1 - 2. * rand.random() for x in range(0, 6)])
    hd = HardDrive(w_a0=st[0], w_f0=st[1], o_f0=st[2], i_f0=0.)
    d = []
    for h in hd.sim(tf=1e-2, n=1001):
        d.append([h.t, h.v_f, h.i_f, h.w_f, h.o_f])
    dat = np.array(d)
    ax1 = fig.add_subplot(4, 1, 1)
    ax2 = fig.add_subplot(4, 1, 2, sharex=ax1)
    ax3 = fig.add_subplot(4, 1, 3, sharex=ax1)
    ax4 = fig.add_subplot(4, 1, 4, sharex=ax1)
    # plt.plot(dat[:, 2], dat[:, 1])
    ax4.set_xlabel(r"$t$", size=14)
    ax1.set_ylabel(r"$\theta_F$", size=14)
    ax1.plot(dat[:, 0], dat[:, 4])
    ax2.set_ylabel(r"$\omega_F$", size=14)
    ax2.plot(dat[:, 0], dat[:, 3])
    ax3.set_ylabel(r"$i_F$", size=14)
    ax3.plot(dat[:, 0], dat[:, 2])
    ax4.set_ylabel(r"$v_F$", size=14)
    ax4.plot(dat[:, 0], dat[:, 1])
    # plt.title(r"Flexure Subsystem, Uncontrolled Behavior",
    #           family='cmb10', size=14)

    fig.tight_layout()
    return fig


def plotC2():
    fig = plt.figure(figsize=(5.5, 7))
    # plt.hold(True)

    st = tuple([1e-6 * (1 - 2. * rand.random()) for x in range(0, 6)])
    hd = HardDrive(w_a0=0.0, o_a0=4.0, i_a0=0.0,
                   w_f0=st[1], o_f0=st[2], i_f0=st[3])
    d = []
    for h in hd.sim(tf=1e-2, n=1001):
        d.append([h.t, h.v_f, h.i_f, h.o_f, h.o_a])
    dat = np.array(d)
    ax1 = fig.add_subplot(4, 1, 1)
    ax2 = fig.add_subplot(4, 1, 2, sharex=ax1)
    ax3 = fig.add_subplot(4, 1, 3, sharex=ax1)
    ax4 = fig.add_subplot(4, 1, 4, sharex=ax1)
    # plt.plot(dat[:, 2], dat[:, 1])
    ax4.set_xlabel(r"$t$", size=14)
    ax1.set_ylabel(r"$\theta_A$", size=14)
    ax1.plot(dat[:, 0], dat[:, 4])
    ax2.set_ylabel(r"$\theta_F$", size=14)
    ax2.plot(dat[:, 0], dat[:, 3])
    ax3.set_ylabel(r"$i_F$", size=14)
    ax3.plot(dat[:, 0], dat[:, 2])
    ax4.set_ylabel(r"$v_F$", size=14)
    ax4.plot(dat[:, 0], dat[:, 1])
    # plt.title(r"Flexure Subsystem, Uncontrolled Behavior",
    #           family='cmb10', size=14)

    fig.tight_layout()
    return fig


def plot1(x1=0.03):
    fig = plt.figure(figsize=(5.5, 3))

    plt.hold(True)
    B = CONSTANT_B
    K = CONSTANT_K

    for w_f0 in [0.0]:
        for o_f0 in np.linspace(-x1, x1, 31):
            hd = HardDrive(w_a0=1e-6, w_f0=w_f0, o_f0=o_f0, i_f0=0.)
            # print hd.fric.events
            d = []
            for h in hd.sim(tf=1e-3, n=1001):
                # print [(e.name, e(), h.fric.dSdt(h.fric.tristate)) for e in
                # h.fric.events]
                d.append([h.t, h.w_f, h.o_f])
            dat = np.array(d)
            # plt.plot(dat[:,0], dat[:,1],'+-',ms=7)
            plt.plot(dat[:, 2], dat[:, 1])
    plt.xlabel(r"$\theta_F$", size=14)
    plt.ylabel(r"$\omega_F$", size=14)
    plt.title(r"Flexure Subsystem, Uncontrolled Behavior",
              family='cmb10', size=14)

    fig.tight_layout()
    return fig


def plot2(x1=0.03):
    fig = plt.figure(figsize=(5.5, 3))
    plt.hold(True)
    for w_f0 in [0.0]:
        for o_f0 in np.linspace(-x1, x1, 31):
            print o_f0
            hd = HardDrive(w_a0=0.0, w_f0=w_f0, o_f0=o_f0, i_f0=0.)
            dat = np.array([[h.t, h.w_f, h.o_f]
                            for h in hd.sim(tf=1e-3, n=1001)])
            # plt.plot(dat[:,0], dat[:,1],'+-',ms=7)
            plt.plot(dat[:, 0], dat[:, 2])
    plt.ylabel(r"$\theta_F$", size=14)
    plt.xlabel(r"$t$", size=14)
    plt.title(r"Flexure Subsystem, Uncontrolled Behavior",
              family='cmb10', size=14)

    fig.tight_layout()
    return fig


def plot3(x1=0.03):
    fig = plt.figure(figsize=(5.5, 3))
    plt.hold(True)
    for o_a0 in [0.0]:
        for w_a0 in np.linspace(-x1, x1, 31):
            hd = HardDrive(w_a0=w_a0, o_a0=o_a0)
            dat = np.array([[h.t, h.w_a, h.o_a]
                            for h in hd.sim(tf=1e-1, n=101)])
            # plt.plot(dat[:,0], dat[:,1],'+-',ms=7)
            plt.plot(dat[:, 0], dat[:, 2])
    plt.ylabel(r"$\theta_A$", size=14)
    plt.xlabel(r"$t$", size=14)
    plt.title(r"Arm Subsystem, Uncontrolled Behavior", family='cmb10', size=14)

    fig.tight_layout()
    return fig


def plot4(x1=0.03):
    fig = plt.figure(figsize=(5.5, 3))
    plt.hold(True)
    for o_a0 in [0.0]:
        for w_a0 in np.linspace(-x1, x1, 31):
            hd = HardDrive(w_a0=w_a0, o_a0=o_a0)
            dat = np.array([[h.t, h.w_a, h.o_a]
                            for h in hd.sim(tf=1e-1, n=101)])
            # plt.plot(dat[:,0], dat[:,1],'+-',ms=7)
            plt.plot(dat[:, 2], dat[:, 1])
    plt.ylabel(r"$\omega_A$", size=14)
    plt.xlabel(r"$\theta_A$", size=14)
    plt.title(r"Arm Subsystem, Uncontrolled Behavior", family='cmb10', size=14)

    fig.tight_layout()
    return fig


def old_plots():
    plot3(0.6).savefig(name_string % "NCarmt")
    plot4(0.6).savefig(name_string % "NCarm")
    plot1(0.03).savefig(name_string % "NCflexure")
    plot1(0.003).savefig(name_string % "NCflexure2")
    plot2(0.03).savefig(name_string % "NCflexuret")
    plot2(0.003).savefig(name_string % "NCflexure2t")

if __name__ == '__main__':
    main()
