import ly_utils as ly
import matplotlib.pyplot as plt
from plot_setup import *
from BiChatterer import BiChatterer
from functools import partial
from math import sqrt
import random as rand
from math import cos, pi, sin

from harddrive_constants import *
"""FBL version!"""
from HD_filters import *


@ly.named_state_system("HDObsState", ["i_a", "o_a", "w_a", "i_f", "o_f", "w_f"])
class HDObs(object):

    def __init__(self, hd):
        self.hd = hd
        self.state = 0, (0, 0, 0, 0, 0, 0)

    def __call__(self):
        o_a, o_f, i_a, i_f = hd.measurement
        Xd_est = np.array(self.e_call())
        C = np.array()
        return tuple(Xd_est)

    def e_call(self, sign_w_a=0):
        i_a, o_a, w_a, i_f, o_f, w_f = self.state[1]
        v_a, v_f = self.hd.v_a, self.hd.v_f
        id_f = self.v_f / Lf - Rf / Lf * i_f
        id_a = self.v_a / La - Ra / La * i_a
        wd_a = (
            1 / Jastar * (
                Ka * i_a
                # - beta * {True: 1, False: -1, 0: 0}[sign_w_a]
                - (Kf * self.i_f + d_f)
            )
            + Jf / Jastar * (
                Ka / Ja * i_a
                + B * (1 + alpha_b * abs(w_f)) * w_f
                + K * (1 + alpha_k * o_f**2) * o_f
            )
        )
        wd_f = (
            1 / Jf * (Kf * i_f + d_f)
            - Ka * i_a / Ja
            - B * (1 + alpha_b * abs(w_f)) * w_f
            - K * (1 + alpha_k * o_f**2) * o_f)
        return (id_a, w_a, wd_a, id_f, w_f, wd_f)

sign = {True: 1, False: -1, 0: 0}
dirac = {True: 0, False: 0, 0: 1.0}


@ly.named_state_system("HardDriveState", ["y", "yd", "ydd", "psi", "psid", "psidd"])
class HardDrive(object):
    """A hard drive control mechanism with an actuator and a flexure."""

    def __init__(self, k_y=5e3, k_psi=2e2, k_est=5e4,  i_a0=0, o_a0=0, w_a0=0, i_f0=0, o_f0=0, w_f0=0, goal=lambda x: 0):
        self.k_y = k_y
        self.k_psi = k_psi
        self.goal = BW4(goal, k=6e3)
        self.state = 0, convert_state((i_a0, o_a0, w_a0, i_f0, o_f0, w_f0))
        self.events = []

        self.sanity_y = BW4(lambda x: self.y, k=k_est)
        self.sanity_psi = BW4(lambda x: self.psi, k=k_est) # 1e5 works well here
        self.par = ly.ParallelSystems(
            [self, self.goal, self.sanity_y, self.sanity_psi]
            # [self, self.estimator, self.LPydd, self.LPpsidd, self.sanity_y, self.sanity_psi]
        )

    @property
    def system(self):
        return self.par

    def reset(self, t, state):
        self.state = t, state
        self.fric.reset()

    @property
    def d_a(self):
        return 0.0

    @property
    def d_f(self):
        return 0.0

    def measurement(self):
        i_a, o_a, w_a, i_f, o_f, w_f = de_convert_state(self.state[1])
        return (o_a, o_f, i_a, i_f)

    def yddd_des(self):
        # y, yd, ydd, psi, psid, psidd = convert_state(self.estimator.est)
        # y, yd, ydd, psi, psid, psidd = self.state[1]
        y, yd, ydd, yddd = self.sanity_y.state[1]
        psi, psid, psidd, psiddd = self.sanity_psi.state[1]
        x, xd, xdd, xddd = self.goal.state[1]
        return (
            xddd
            - 2 * self.k_y * (ydd - xdd)
            - 2 * self.k_y**2 * (yd - xd)
            - self.k_y**3 * (y - x)
        )

    def psiddd_des(self):
        # y, yd, ydd, psi, psid, psidd = convert_state(self.estimator.est)
        # y, yd, ydd, psi, psid, psidd = self.state[1]
        y, yd, ydd, yddd = self.sanity_y.state[1]
        psi, psid, psidd, psiddd = self.sanity_psi.state[1]

        psiddd_des = (
            - 2 * self.k_psi * psidd
            - 2 * self.k_psi**2 * psid
            - self.k_psi**3 * psi)
        # return 0.1
        return psiddd_des

    def yddd_psiddd(self):
        return self.yddd_des(), self.psiddd_des()

    def __call__(self, sign_w_a=0):
        x, xd, xdd, xddd = self.goal.state[1]
        # x, xd, xdd, xddd = 0, 0, 0, 0
        y, yd, ydd, psi, psid, psidd = self.state[1]
        "y", "yd", "ydd", "psi", "psid", "psidd"
        return (yd, ydd, self.yddd_des(), psid, psidd, self.psiddd_des())

    def sim(self, tf=100, n=1001):
        sys = self.system
        stepper = ly.dormand_prince(sys, np.linspace(self.t, tf, n))
        # stepper = ly.euler(sys, np.linspace(self.t, tf, n))
        stepper.absolute_tolerance = 1e-4
        stepper.relative_tolerance = 1e-4
        stepper.time_tolerance = 1e-16
        for jump, time, state, guard in ly.hybrid_integrate(stepper):
            ly.check_NaN(self)
            yield self


def approximate_matrix(eps):
    h = HardDrive()
    z = np.zeros(6)
    h.reset(0, tuple(z))
    eh = np.array(h(sign_w_a=0))
    m = np.zeros((6, 6))
    for i in range(0, 6):
        ei = np.zeros(6)
        ei[i] = eps
        si = tuple(z + ei)
        h.reset(0, si)
        m[:, i] = np.array(h(sign_w_a=0)) / eps
    for r in m:
        for c in r:
            print c
    print("[\n\t" + ";\n\t".join([", ".join(["%+.4e"] * 6)] * 6) +
          "\n]") % tuple((c for r in m for c in r))
    return m


def plot_general(tf=1e-6, n=1001, scale=1e-3):
    fig = plt.figure(figsize=(6.5, 13))

    rand.seed(25)
    st = tuple([scale * (1 - 2. * rand.random()) for x in range(0, 6)])
    goal = lambda t: (-1.0 - 0.0 * (t - 0.02) if t >
                      0.02 else 1.0) if t > 0.01 else 0.0

    print st
    hd = HardDrive(w_a0=0.0, o_a0=-1.0, i_a0=0.0,
                   w_f0=0.0, o_f0=0.0, i_f0=0.0, goal=goal)
    simdat, estdat, goaldat, desdat, sanity_dat = [], [], [], [], []
    for h in hd.sim(tf=tf, n=n):
        print h.t, sum(s**2 for s in h.state[1])
        y, yd, ydd, psi, psid, psidd = h.state[1]
        yddd, psiddd = h.yddd_psiddd()
        v_a, v_f = 0, 0
        simdat.append([h.t, v_a, v_f, y, yd, ydd,
                       yddd, psi, psid, psidd, psiddd])
        # estimator and empirical tripple dots
        # y, yd, ydd, psi, psid, psidd = convert_state(h.estimator.est)
        # yddd, psiddd = h.LPydd.xd, h.LPpsidd.xd
        # estdat.append([y, yd, ydd, yddd, psi, psid, psidd, psiddd])
        x = h.goal.x
        x, xd, xdd, xddd = h.goal.state[1]
        goaldat.append([x, xd, xdd, xddd])
        desdat.append([h.yddd_des(), h.psiddd_des()])

        y, yd, ydd, yddd = h.sanity_y.full
        psi, psid, psidd, psiddd = h.sanity_psi.full
        sanity_dat.append([y, yd, ydd, yddd, psi, psid, psidd, psiddd])

    labels = [
        r"$V_A$",
        r"$V_F$",
        r"$y$",
        r"$\dot{y}$",
        r"$\ddot{y}$",
        r"$\dddot{y}$",
        r"$\psi$",
        r"$\dot\psi$",
        r"$\ddot\psi$",
        r"$\dddot{\psi}$"
    ]
    simdat = np.array(simdat)
    estdat = np.array(estdat)
    goaldat = np.array(goaldat)
    desdat = np.array(desdat)
    sanity_dat = np.array(sanity_dat)
    t = simdat[:, 0]
    y = simdat[:, 2]
    n = 10
    axs = [fig.add_subplot(n, 1, 1)]
    for i in range(1, n):
        axs.append(fig.add_subplot(n, 1, i + 1, sharex=axs[0]))
    for i in range(0, n):
        axs[i].plot(t, simdat[:, i + 1], zorder=10,
                    color=new_cycle[0])  # black
        axs[i].autoscale(False)
        axs[i].hold(True)
        axs[i].set_ylabel(labels[i], size=16)
    for i in range(0, 8):
        axs[i + 2].plot(t, sanity_dat[:, i], zorder=0,
                        color=new_cycle[1])  # midnight blue
    # for i in range(0, 8):
    # axs[i + 2].plot(t, sanity_dat[:, i], zorder=-12, color=new_cycle[4]) #
    # bright green
    for i in range(0, 3):
        axs[i + 2].plot(t, goaldat[:, i], zorder=1,
                        color=new_cycle[2])  # greenish blue
    axs[2].plot(t, [goal(k) for k in t], zorder=-1,
                color=new_cycle[3])  # bright green
    axs[5].plot(t, desdat[:, 0], color=new_cycle[3])
    axs[9].plot(t, desdat[:, 1], color=new_cycle[3])
    axs[-1].set_xlabel(r"$t$", size=14)

    fig.tight_layout()
    return fig

def compare_est_freq(tf=1e-6, n=1001, scale=1e-3):
    fig=None
    n_figs = 4

    rand.seed(25)
    st = tuple([scale * (1 - 2. * rand.random()) for x in range(0, 6)])
    goal = lambda t: (sin((t-0.03)*100.)if t>0.03 else -1.0 + (2.0/0.02) * (t - 0.02) if t >
                      0.02 else 1.0) if t > 0.01 else 0.0

    print st
    for ind, k_est in enumerate([2.585e4,2.6e4,2.7e4,4e4]):
        hd = HardDrive(w_a0=0.0, o_a0=-1.0, i_a0=0.0,
                       w_f0=0.0, o_f0=0.0, i_f0=0.0, goal=goal, k_est =k_est)
        simdat, estdat, goaldat, desdat, sanity_dat = [], [], [], [], []
        for h in hd.sim(tf=tf, n=n):
            print h.t, sum(s**2 for s in h.state[1])
            y, yd, ydd, psi, psid, psidd = h.state[1]
            yddd, psiddd = h.yddd_psiddd()
            v_a, v_f = 0, 0
            simdat.append([h.t,
             # v_a, v_f, 
             y, yd, ydd,yddd,
              # psi, psid, psidd, psiddd
              ])
            x = h.goal.x
            x, xd, xdd, xddd = h.goal.state[1]
            goaldat.append([x, xd, xdd, xddd])
            desdat.append([h.yddd_des(), h.psiddd_des()])

            y, yd, ydd, yddd = h.sanity_y.full
            psi, psid, psidd, psiddd = h.sanity_psi.full
            sanity_dat.append([
                y, yd, ydd, yddd, 
                # psi, psid, psidd, psiddd
                ])

        labels = [
            # r"$V_A$",
            # r"$V_F$",
            r"$y$",
            r"$\dot{y}$",
            r"$\ddot{y}$",
            r"$\dddot{y}$",
            # r"$\psi$",
            # r"$\dot\psi$",
            # r"$\ddot\psi$",
            # r"$\dddot{\psi}$"
        ]
        simdat = np.array(simdat)
        estdat = np.array(estdat)
        goaldat = np.array(goaldat)
        desdat = np.array(desdat)
        sanity_dat = np.array(sanity_dat)
        t = simdat[:, 0]
        y = simdat[:, 2]
        if fig==None:
            fig = plt.figure(figsize=(6.5, 9))
            axs = [fig.add_subplot(n_figs, 1, 1)]
            for i in range(1, n_figs):
                axs.append(fig.add_subplot(n_figs, 1, i + 1, sharex=axs[0]))
            for i in range(0, n_figs):
                axs[i].plot(t, simdat[:, i + 1], zorder=10,
                            color=new_cycle[0])  # black
                axs[i].autoscale(False)
                axs[i].hold(True)
                axs[i].set_ylabel(labels[i], size=16)
            # for i in range(0, 8):
            #     axs[i + 2].plot(t, sanity_dat[:, i], zorder=0,
            #                     color=new_cycle[1])  # midnight blue
            # for i in range(0, 8):
            # axs[i + 2].plot(t, sanity_dat[:, i], zorder=-12, color=new_cycle[4]) #
            # bright green
            for i in range(0, 3):
                axs[i].plot(t, goaldat[:, i], zorder=1,
                                color=new_cycle[2])  # greenish blue
            axs[0].plot(t, [goal(k) for k in t], zorder=-1,
                        color=new_cycle[4])  # bright green
            # axs[5].plot(t, desdat[:, 0], color=new_cycle[3])
            # axs[9].plot(t, desdat[:, 1], color=new_cycle[3])
            axs[-1].set_xlabel(r"$t$", size=14)
        else:
            for i in range(0, n_figs):
                axs[i].plot(t, simdat[:, i + 1], zorder=11+ind, color = new_cycle[1+ind])

    fig.tight_layout()
    return fig

name_string = "../../../tex/homework-NonLinear/hw3/figs/FBL_no_HD%s.pdf"


def main():
    # plot_general(tf=6.0e-2, n=10001, scale=1e-9).savefig(name_string % "_1")
    compare_est_freq(tf=6.0e-2, n=10001, scale=1e-9).savefig(name_string % "_2")
    plt.show()


if __name__ == '__main__':
    main()
