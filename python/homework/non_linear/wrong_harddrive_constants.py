import numpy as np
import random as rand
ACTUATOR_RESISTANCE = 1.0  # Ohm
ACTUATOR_INDUCTANCE = 0.001  # Henry
ACTUATOR_CONSTANT = 60  # Newton meters per ampere
ALPHA_K = 0.05  # Radians squared
CONSTANT_K = 3.55e8  # Newton meter per radian
NOMINAL_ACTUATOR_INERTIA = 1.0  # Kilogram meters squared
# NOMINAL_FLEXTURE_INERTIA = 2.4e-5  # Kilogram meters squared
NOMINAL_FLEXTURE_INERTIA = 1.1e-7  # Kilogram meters squared
NOMINAL_BETA = 10.0  # Newton meters
FLEXURE_RESISTANCE = 2.0  # Ohm
FLEXURE_INDUCTANCE = 0.005  # Henry
FLEXURE_CONSTANT = 60.0  # Newton meters per ampere
ALPHA_B = 0.01  # Radians per second
CONSTANT_B = 11310.0  # Newton meter second radian
DISTURBANCE_TO_ACTUATOR_INERTIA = 0.1  # Kilogram meter squared
DISTURBANCE_TO_FLEXURE_INERTIA = 0.1e-7  # Kilogram meter squared
DISTURBANCE_TO_ACTUATOR = 0.1  # Newton meter
DISTURBANCE_TO_FLEXURE = 0.1e-3  # Newton meter
DISTURBANCE_TO_BETA = 5.0  # Newton meter

Ja = NOMINAL_ACTUATOR_INERTIA + DISTURBANCE_TO_ACTUATOR_INERTIA
Jf = NOMINAL_FLEXTURE_INERTIA + DISTURBANCE_TO_FLEXURE_INERTIA
Jastar = Ja + Jf + 0.01**2 * 0.005
B = CONSTANT_B
K = CONSTANT_K
alpha_k = ALPHA_K
alpha_b = ALPHA_B
beta = NOMINAL_BETA + DISTURBANCE_TO_BETA
Ra = ACTUATOR_RESISTANCE
La = ACTUATOR_INDUCTANCE
Rf = FLEXURE_RESISTANCE
Lf = FLEXURE_INDUCTANCE
Ka = ACTUATOR_RESISTANCE
Kf = FLEXURE_CONSTANT

def convert_state(regular_state):
    i_a, o_a, w_a, i_f, o_f, w_f = regular_state
    y = o_a + o_f
    yd = w_a + w_f
    s = o_a - o_f
    sd = w_a - w_f
    ydd = (
        (1 / Jastar - 1 / Ja) * Ka * i_a
        - 1 / Jastar * beta * np.sign(w_a)
        + 1 / Jf * Kf * i_f
        - B * (1 + alpha_b * abs(w_f)) * w_f
        - K * (1 + alpha_k * o_f * o_f) * o_f
    )
    sdd = (
        (1 / Jastar + 1 / Ja) * Ka * i_a
        - 1 / Jastar * beta * np.sign(w_a)
        - 1 / Jf * Kf * i_f
        + B * (1 + alpha_b * abs(w_f)) * w_f
        + K * (1 + alpha_k * o_f * o_f) * o_f
    )
    standard_state = y, yd, ydd, s, sd, sdd
    return standard_state


def de_convert_state(standard_state):
    y, yd, ydd, s, sd, sdd = standard_state
    o_a = 0.5 * (y + s)
    o_f = 0.5 * (y - s)
    w_a = 0.5 * (yd + sd)
    w_f = 0.5 * (yd - sd)
    i_a = Jastar / (2 * Ka) * (
        ydd + sdd
        + 2 / Jastar * (beta * np.sign(w_a))
    )
    i_f = -Jf / (2 * Kf) * (
        -ydd + sdd
        - 2 / Ja * Ka * i_a
        - 2 * B * (1 + alpha_b * abs(w_f)) * w_f
        - 2 * K * (1 + alpha_k * o_f * o_f) * o_f
    )
    regular_state = i_a, o_a, w_a, i_f, o_f, w_f
    return regular_state

sign = {True: 1, False: -1, 0: 0}
dirac = {True: 0, False: 0, 0: 1.0}


def get_wd_a(state, sign_w_a=0):
    i_a, o_a, w_a, i_f, o_f, w_f = state
    return (
        1 / Jastar * (
            Ka * i_a
            - beta * {True: 1, False: -1, 0: 0}[sign_w_a]
            - (Kf * i_f)
        )
        + Jf / Jastar * (
            Ka / Ja * i_a
            + B * (1 + alpha_b * abs(w_f)) * w_f
            + K * (1 + alpha_k * o_f**2) * o_f
        )
    )


def get_wd_f(state):
    i_a, o_a, w_a, i_f, o_f, w_f = state
    return (
        1 / Jf * Kf * i_f
        - Ka * i_a / Ja
        - B * (1 + alpha_b * abs(w_f)) * w_f
        - K * (1 + alpha_k * o_f**2) * o_f
    )


def get_yddd(state, voltages):
    i_a, o_a, w_a, i_f, o_f, w_f = state
    v_a, v_f = voltages
    wd_f = get_wd_f(state)
    return (
        (1 / Jastar - 1 / Ja) * Ka / La * (v_a - Ra * i_a)
        # impulse term
        + 1 / Jf / Lf * Kf * (v_f - Rf * i_f)
        - B * (1 + 2 * alpha_b * abs(w_f)) * wd_f
        - K * (1 + 3 * alpha_k * o_f**2) * w_f
        # disturbance terms
    )


def get_psiddd(state, voltages):
    i_a, o_a, w_a, i_f, o_f, w_f = state
    v_a, v_f = voltages
    wd_f = get_wd_f(state)
    return (
        (1 / Jastar + 1 / Ja) * Ka / La * (v_a - Ra * i_a)
        # impulse term
        - 1 / Jf / Lf * Kf * (v_f - Rf * i_f)
        + B * (1 + 2 * alpha_b * abs(w_f)) * wd_f
        + K * (1 + 3 * alpha_k * o_f**2) * w_f
        # disturbance terms
    )


def get_voltages(u_y, u_psi):
    T = np.array([
        [(1 / Jastar - 1 / Ja) * Ka / La, 1 / Jf / Lf * Kf],
        [(1 / Jastar + 1 / Ja) * Ka / La, -1 / Jf / Lf * Kf]
    ])
    u = np.array([[u_y], [u_psi]])
    v = np.linalg.solve(T, u)
    v_a = v[0, 0]
    v_f = v[1, 0]
    return v_a, v_f


def get_fundamental_inputs(v_a, v_f):
    T = np.array([
        [(1 / Jastar - 1 / Ja) * Ka / La, 1 / Jf / Lf * Kf],
        [(1 / Jastar + 1 / Ja) * Ka / La, -1 / Jf / Lf * Kf]
    ])
    v = np.array([[v_a], [v_f]])
    u = T.dot(v)
    u_y = u[0, 0]
    u_psi = u[1, 0]
    return u_y, u_psi


def get_u_y(state, yddd_des):
    i_a, o_a, w_a, i_f, o_f, w_f = state
    wd_f = get_wd_f(state)
    return (
        yddd_des
        # + 1/Jastar*beta*dirac_w_a*a_a
        + (1 / Jastar - 1 / Ja) * Ka / La * Ra * i_a
        + 1 / Jf / Lf * Kf * Rf * i_f
        + B * (1 + 2 * alpha_b * abs(w_f)) * wd_f
        + K * (1 + 3 * alpha_k * o_f**2) * w_f
    )


def get_u_psi(state, psiddd_des):
    i_a, o_a, w_a, i_f, o_f, w_f = state
    wd_f = get_wd_f(state)
    return (
        psiddd_des
        # + 1/Jastar*beta*dirac_w_a*a_a
        + (1 / Jastar + 1 / Ja) * Ka / La * Ra * i_a
        - 1 / Jf / Lf * Kf * Rf * i_f
        - B * (1 + 2 * alpha_b * abs(w_f)) * wd_f
        - K * (1 + 3 * alpha_k * o_f**2) * w_f
    )


def get_dynamics(state, disturbances, voltages, sign_w_a=0):
    i_a, o_a, w_a, i_f, o_f, w_f = state
    v_a, v_f = voltages
    d_a, d_f = disturbances
    id_f = v_f / Lf - Rf / Lf * i_f
    id_a = v_a / La - Ra / La * i_a
    wd_a = (
        get_wd_a(state, sign_w_a=sign_w_a)
        + d_a / Jastar - d_f / Jastar
    )
    wd_f = get_wd_f(state) + d_f / Jf
    # return (id_a, w_a, 0.0, id_f, w_f, 0.0)
    return (id_a, w_a, wd_a, id_f, w_f, wd_f)


def random_state(scale):
    return tuple([scale * (1 - 2. * rand.random()) for x in range(0, 6)])


def del_ydd(state):
    i_a, o_a, w_a, i_f, o_f, w_f = state
    return [(1 / Jastar - 1 / Ja) * Ka, 0, 0, Kf / Jf, -K * (1 + 3 * alpha_k * o_f**2), -B * (1 + 2 * alpha_b * abs(w_f))]


def convert_jac(state):
    i_a, o_a, w_a, i_f, o_f, w_f = state
    return np.array([
        [0, 1, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 1],
        [(1 / Jastar - 1 / Ja) * Ka, 0, 0, Kf / Jf, -K *
         (1 + 3 * alpha_k * o_f**2), -B * (1 + 2 * alpha_b * abs(w_f))],
        [0, 1, 0, 0, -1, 0],
        [0, 0, 1, 0, 0, -1],
        [(1 / Jastar + 1 / Ja) * Ka, 0, 0, -Kf / Jf, +K *
         (1 + 3 * alpha_k * o_f**2), +B * (1 + 2 * alpha_b * abs(w_f))]
    ])


def de_convert_jac(state):
    return np.linalg.inv(convert_jac(state))

if __name__ == '__main__':
    convert_jac((0, 0, 0, 0, 0, 0))
    de_convert_jac((0, 0, 0, 0, 0, 0))


def test_plant_inversion():
    rand.seed(25)
    for i in range(0, 1000):
        state = random_state(1e0)
        yddd_des_o = (1. - 2. * rand.random()) * 1e10
        psiddd_des_o = (1. - 2. * rand.random()) * 1e10
        u_y = get_u_y(state, yddd_des_o)
        u_psi = get_u_psi(state, psiddd_des_o)
        v_a, v_f = get_voltages(u_y, u_psi)
        yddd = get_yddd(state, (v_a, v_f))
        psiddd = get_psiddd(state, (v_a, v_f))

        assert abs(yddd_des_o - yddd) < 1e5
        assert abs(psiddd_des_o - psiddd) < 1e5

        s1 = convert_state(state)
        dyn = get_dynamics(state, (0, 0), (v_a, v_f), sign_w_a=(state[2] > 0))
        yphid = convert_jac(state).dot(np.array(dyn))
        print "\n".join([", ".join(["%+.6e"] * 2)] * 2) % (yphid[2], yphid[5], yddd_des_o, psiddd_des_o)
        assert abs(yphid[2] - yddd_des_o) < 1e3
        assert abs(yphid[5] - psiddd_des_o) < 1e3


def main():
    test_plant_inversion()

if __name__ == '__main__':
    main()
