import ly_utils as ly
from collections import namedtuple
import numpy as np
from math import *
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from plot_setup import *


@ly.named_state_system("PendulumState", ["x", "xd"])
class SlidingModePendulum(object):

    """Benito's Pendulum"""

    def __init__(self, x0=0, xd0=1e-7, eta=1, lambda_=0.1):
        self.state = 0.0, (x0, xd0)
        self.eta = eta
        self.lambda_ = lambda_
        self.l = 2.0
        self.g = 10.0
        self.m = 5.0
        try:
            if self.sigma() > 0:
                self.pos.jump(init=True)
            else:
                self.neg.jump(init=True)
        except ly.Jump:
            pass

        # self.events=[self.capture, self.]
    def sigma(self):
        return self.lambda_ * self.x + self.xd

    def del_sigma(self):
        return np.array([self.lambda_, 1.0])

    def sigma_dot(self):
        return sum([x * d for x, d in zip(self.del_sigma(), [self.x, self.xd])])


    # def sigma(self):
    #     return  self.x + self.xd*abs(self.xd)/(2*self.eta)

    # def del_sigma(self):
    #     return np.array([1, abs(self.xd)/self.eta])

    # def sigma_dot(self):
    #     return sum([x * d for x, d in zip(self.del_sigma(), [self.x, self.xd])])

    # def xdd_physics(self):
    #     return -self.g / self.l * sin(self.x)

    def xdd_physics(self): # simplified
        return -self.g / self.l * sin(self.x)

    # def xdd_neg(self):
    #     return self.xdd_physics() + self.eta - self.xd*self.lambda_

    # def xdd_pos(self):
    #     return self.xdd_physics() - self.eta - self.xd*self.lambda_

    def xdd_neg(self):
        return self.xdd_physics() + self.eta

    def xdd_pos(self):
        return self.xdd_physics() - self.eta

    def xdd_follow(self):
        sig_x, sig_xd = self.del_sigma()
        return -self.xd * sig_x / sig_xd

    def stability_estimate(self, alpha=0.1):
        kdelt = 0.5 * (-self.kp + self.kn)
        k0 = 0.5 * (-self.kp - self.kn)
        if -kdelt > abs(1 + alpha * k0) + abs(alpha * self.g / self.l):
            return True
        return False

    @property
    def u(self):
        return self.xdd()-self.xdd_physics()

    @ly.Guard
    def pos(self):
        return ly.EventThrowIf[self.sigma() > 0 and self.sigma_dot() > 0]

    def intersect_dt(self):
        del_sigma = self.del_sigma().reshape((1, 2))
        sigma = self.sigma()
        x_dot = np.array([[self.xd], [self.xdd()]])
        den = del_sigma.dot(x_dot)[0, 0]
        if abs(den)<1e-7:
            return 0.0
        return -sigma / den

    def skip(self, dt):
        t, (x, xd) = self.state
        xdd = self.xdd()
        t += dt
        x += xd * dt
        xd += xdd * dt
        self.state = t, (x, xd)

    @pos.jumper
    def pos(self, init=False):
        if not init:
            if self.xdd_pos() <= self.xdd_follow():
                self.events = [self.escape_pos, self.escape_neg]
                dt = self.intersect_dt()
                self.skip(dt)
                self.xdd = self.xdd_follow
                raise ly.StateJump()
            # self.check_capture()
        # self.k = self.kp
        self.events = [self.neg]
        self.xdd = self.xdd_pos
        raise ly.Jump()

    @ly.Guard
    def neg(self):
        return ly.EventThrowIf[self.sigma() < 0 and self.sigma_dot() < 0]

    @neg.jumper
    def neg(self, init=False):
        if not init:
            if self.xdd_neg() >= self.xdd_follow():
                self.events = [self.escape_pos, self.escape_neg]
                dt = self.intersect_dt()
                self.skip(dt)
                self.xdd = self.xdd_follow
                raise ly.StateJump()

        self.events = [self.pos]
        self.xdd = self.xdd_neg
        raise ly.Jump()

    @ly.Guard
    def escape_neg(self):
        return ly.EventThrowIf[self.xdd_neg() < self.xdd_follow()]

    @ly.Guard
    def escape_pos(self):
        return ly.EventThrowIf[self.xdd_pos() > self.xdd_follow()]

    @escape_neg.jumper
    def escape_neg(self):
        self.events = [self.pos]
        self.xdd = self.xdd_neg
        dt = self.intersect_dt() + 1e-7
        self.skip(dt)
        raise ly.Jump()

    @escape_pos.jumper
    def escape_pos(self):
        self.events = [self.neg]
        self.xdd = self.xdd_pos
        dt = self.intersect_dt() + 1e-7
        self.skip(dt)
        raise ly.Jump()

    def __call__(self):
        return (self.xd, self.xdd())

    def sim(self, tf=100):
        stepper = ly.runge_kutta4(self, np.linspace(0, tf, 10001))
        l, m, g = (self.l, self.m, self.g)
        try:
            for jump, time, state, guard in ly.hybrid_integrate(stepper):
                yield self
        except StopIteration as e:
            print e


def sim(pendulum, tf=100):
    stepper = ly.runge_kutta4(pendulum, np.linspace(0, tf, 10001))
    times, mag, xs, xds = [], [], [], []
    l, m, g = (pendulum.l, pendulum.m, pendulum.g)
    try:
        for jump, time, state, guard in ly.hybrid_integrate(stepper):
            # print time, state[0] / pi
            # if guard:
            # print "%+0.2f"%state[0], "%+0.12f"%pendulum.sigma() ,guard.name
            times.append(time)
            mag.append(sqrt(state[0] ** 2 + state[1] ** 2))
            xs.append(pendulum.x)
            xds.append(pendulum.xd)
    except StopIteration as e:
        print e
    return xs, xds




def phase(eta, lambda_, tf=10, special=None):
    fig = plt.figure(figsize=(4.5, 3))
    plt.hold(True)

    n1 = 2
    n2 = 20
    cap = lambda x: x if x <= 1 else 2 - x
    new_cycle = [custom_cm(0.9 * cap(x))
                 for x in np.linspace(0.0, 2.0, 11)][0:-1]
    plt.gca().set_color_cycle(new_cycle)
    mpl.rcParams['lines.linewidth'] = 1.9
    mpl.rcParams['lines.solid_capstyle'] = 'round'
    for xd0 in np.linspace(-2, 2, 2):
        for x0 in np.linspace(-4, 4, 20):
            p = SlidingModePendulum(x0=x0, xd0=xd0, eta=eta, lambda_=lambda_)
            x, xd = sim(p, tf=tf)
            color = next(plt.gca()._get_lines.color_cycle)
            plt.plot(x, xd, '-', color=color)

    if special:
        for x0, xd0 in special:
            p = SlidingModePendulum(x0=x0, xd0=xd0, eta=eta, lambda_=lambda_)
            x, xd = sim(p, tf=tf)
            plt.plot(x, xd, 'r', lw=3)
    plt.xlabel(r"$x$", size=12)
    plt.ylabel(r"$\dot{x}$", size=12)
    plt.title(r"Pendulum---[$\eta$, $\lambda$]=[%.2f, %.2f] Control" %
              (eta, lambda_), family='cmb10', size=14)
    fig.tight_layout()
    return fig

def alt_phase(eta, lambda_, tf=10):
    fig = plt.figure(figsize=(4.5, 3))
    plt.hold(True)

    n1 = 2
    n2 = 20
    cap = lambda x: x if x <= 1 else 2 - x
    new_cycle = [custom_cm(0.9 * cap(x))
                 for x in np.linspace(0.0, 2.0, 11)][0:-1]
    plt.gca().set_color_cycle(new_cycle)
    mpl.rcParams['lines.linewidth'] = 1.9
    mpl.rcParams['lines.solid_capstyle'] = 'round'
    for xd0 in np.linspace(-2, 2, 2):
        for x0 in np.linspace(-4, 4, 20):
            p = SlidingModePendulum(x0=x0, xd0=xd0, eta=eta, lambda_=lambda_)
            x, xd = sim(p, tf=tf)
            x=np.array(x)
            xd = np.array(xd)
            sigma = xd + lambda_*x
            xi = xd + (1/lambda_)* x
            color = next(plt.gca()._get_lines.color_cycle)
            plt.plot(sigma, xi, '-', color=color)

    plt.xlabel(r"$\sigma$", size=12)
    plt.ylabel(r"$\xi$", size=12)
    plt.title(r"Pendulum---[$\eta$, $\lambda$]=[%.2f, %.2f] Control" %
              (eta, lambda_), family='cmb10', size=14)
    fig.tight_layout()
    return fig

def u_eq(eta, lambda_, x0=-4, xd0=-2, tf=10):
    fig = plt.figure(figsize=(8.5, 5))

    pen = SlidingModePendulum(x0=x0, xd0=xd0, eta=eta, lambda_=lambda_)
    res=np.array([[p.t, p.xd + p.lambda_*p.x, p.xdd_physics(),  p.u, -p.xd*p.lambda_-p.xdd_physics()] for p in pen.sim(tf=tf)])
    
    ax3 = fig.add_subplot(313)
    ax3.plot(res[:,0],res[:,4],'b')
    ax3.plot(res[:,0],res[:,3],'r')
    ax3.set_ylabel(r"$u(t)$") 
    ax3.set_xlabel(r"$t$")
    ax1 = fig.add_subplot(311, sharex=ax3)
    ax1.plot(res[:,0],res[:,1],'r')
    ax1.set_ylabel(r"$\sigma(t)$")   
    plt.title(r"Pendulum---[$\eta$, $\lambda$]=[%.2f, %.2f] Control" %
              (eta, lambda_), family='cmb10', size=14)
    ax2 = fig.add_subplot(312, sharex=ax3)
    ax2.plot(res[:,0],res[:,2],'r')
    ax2.set_ylabel(r"$-\alpha\sin(x(t))$") 

    
    fig.tight_layout()
    return fig

def stability():
    fig = plt.figure(figsize=(4.5, 3))

    Path = mpath.Path
    path_data = [
        (Path.MOVETO, (0, 0)),
        (Path.LINETO, (1, 0)),
        (Path.LINETO, (1, 1)),
        (Path.LINETO, (0, 1)),
        (Path.CLOSEPOLY, (0, 0))]
    plt.axis([-1, 1, -1, 1])
    plt.text(0.1, 0.5, "not stable,\nbut center-like")
    plt.text(-0.5, -0.5, "not stable,\nnot center-like")
    codes, verts = zip(*path_data)
    path = mpath.Path(verts, codes)
    patch = mpatches.PathPatch(path, facecolor='r', alpha=0.5)
    plt.gca().add_patch(patch)
    plt.xlabel(r"$k_n$", size=14, family='cmb10')
    plt.ylabel(r"$k_p$", size=14, family='cmb10')
    fig.tight_layout()
    return fig

save = "../../../tex/homework-NonLinear/hw2/figs/pend%s.pdf"


def main():
    # phase(0.5, 0.5,tf=100).savefig(save % "1phase") #
    # phase(0.15, 0.5,tf=1000).savefig(save % "4phase")
    # phase(0.5, 0.1,tf=10).savefig(save % "5phase")
    # phase(8, 0.5,tf=10).savefig(save % "6phase")
    # alt_phase(0.5, 0.5,tf=100).savefig(save % "1alt")
    # alt_phase(0.15, 0.5,tf=1000).savefig(save % "4alt")
    # alt_phase(0.5, 0.1,tf=10).savefig(save % "5alt")
    # alt_phase(8, 0.5,tf=10).savefig(save % "6alt")
    # phase(20,-10,0.5,tf=0.4).savefig(save%"2phase")
    # u_eq(0.5,0.5, tf=100).savefig(save%"1ueq")
    # phase(2.5, 0.5,special=[(-4,-2)],tf=10).savefig(save % "2phase")
    u_eq(2.5,0.5, tf=10).savefig(save%"2ueq")
    # phase(5, 0.5,special=[(-4,-2)],tf=10).savefig(save % "3phase")
    u_eq(5,0.5, tf=10).savefig(save%"3ueq")
    # phase(.5, 0.5,special=[(-4,-2)],tf=30).savefig(save % "1phase")
    u_eq(.5,0.5, tf=30).savefig(save%"1ueq")
    # stability().savefig(save%"stab")
    plt.show()

if __name__ == "__main__":
    main()
