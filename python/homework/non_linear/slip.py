import lyapunov as ly
import numpy as np 
import matplotlib.pyplot as plt
from math import pow, sqrt, pi
import ly_utils as lu




@lu.hybridsystemfunctor
class Violin(object):
	"""
	A violin system based on a nonlinear hybrid system model.
	"""
	def __init__(self, x0=(0.0003,-0.05), mu_s=1.5, mu_d=1.2, N=3.0/1.2, vc=0.015, 
		m=0.02, vb=0.012, k=10000.0, max_events=7):
		"""
		The violin model's parameters mu_d and mu_s represent dynamic 
		and static friction coefficients respectively. N represents the
		normal force of the bow against the string. vb represents the 
		bow velocity, and m the mass of the lumped mass model string. 
		vc represents the critical velocity at which the dynamic 
		friction coefficient is attained. k is the spring constant of
		the string tension. num_events simply counts the number of 
		events which have occured, and is compared to max_events to
		terminate the simulation before the end is reached in certain
		circumstances. bowstate and freestate represent the states. 
		these states are used only for comparisons, and their value
		is irrelevant. dstate is the discrete state of the system. 
		events is a list of event functions as defined by lyapunov.
		"""
		self.state = 0.0, x0
		self.mu_d=mu_d
		self.mu_s=mu_s
		self.N=N
		self.vc=vc #m/s
		self.m=m #kg
		self.vb=vb #m/s
		self.k=k # N/m
		self.max_events=max_events
		self.bowstate="on_bow"
		self.freestate="free"
		self.events = [self.bow_catch_event,self.friction_break_event]
		self.reset()

	state=ly.state_property(xname="x")

	def reset(self):
		"""
		Prepares the system for integration by finding the dstate
		based on the state. Resets the number of events. 
		Part of hybrid systems add-on convention.
		"""
		self.num_events=0
		u=self.x[1]-self.vb
		if u>0:
			self.dstate = self.freestate
			self.free_direction = 1.0
		elif u<0:
			self.dstate = self.freestate
			self.free_direction = -1.0
		else:
			self.dstate = self.bowstate

	def mu(self,relative_velocity):
		"""
		Nonlinear friction coefficient, changes parabolically with
		the relative velocity of bow and string.
		"""
		u=abs(relative_velocity)
		if u>1e20:
			u=1e20
		return self.mu_d+(self.mu_s-self.mu_d)/pow(self.vc,2)*pow(u-self.vc,2)

	def friction_break_event(self):
		""" 
		Event: spring force exceeds static friction while on bow. 
		"""
		for case in lu.switch(self.dstate):
			if case(self.bowstate):
				return self.mu_s*self.N-abs(self.restoring_force())
			if case(self.freestate):
				return 0.0
		
	def bow_catch_event(self):
		""" 
		Event: string matches bow velocity while free. 
		"""
		for case in lu.switch(self.dstate):
			if case(self.bowstate):
				return 0.0
			if case(self.freestate):
				return self.x[1]-self.vb

	def handle_events(self,stepper,events):
		""" 
		Handles the discrete state change of the system, based
		on which event was triggered. Returns true to 
		terminate the integration when self.max_events have
		occured.
		Part of hybrid systems add-on convention.
		"""
		event=events[0] # assume 1 event
		if len(events)>1:
			print "Bonus event ignored!", events[1]
		for case in lu.switch(event):
			if case(self.friction_break_event):	
				self.dstate=self.freestate
				self.free_direction=np.sign(self.restoring_force())
				stepper.step_across((self.x[0],self.vb
					+1e-8*self.free_direction))
				break
			if case(self.bow_catch_event):
				if self.mu_s*self.N-abs(self.restoring_force())>=0.0:
					self.dstate=self.bowstate
					stepper.step_across((self.x[0],self.vb))
				else:
					self.dstate=self.freestate
					self.free_direction=np.sign(self.restoring_force())
					stepper.step_across((self.x[0],self.vb
						+1e-8*self.free_direction))
				break
			if case():
				print "confusion!"
		self.num_events+=1
		if self.num_events>=self.max_events:
			return True # terminate loop
		return False # continue

	def restoring_force(self):
		"""
		Simply the force of a linear spring: violin string tension
		"""
		return -self.x[0]*self.k

	def friction_force(self):
		"""
		Force of the bow on the string, based on nonlinear friction.
		Note that while this friction force would always oppose
		relative motion of bow and string, the signum of relative
		velocity is constant in any particular regime of the motion
		and so technically this function only handles either the
		case when the bow is faster or slower than the string, and 
		only by considering the discrete transitions can the whole
		range be explored. 
		"""
		u=self.x[1]-self.vb
		f=-self.mu(u)*self.N*self.free_direction
		return f

	def equilibrium_point(self):
		"""
		Returns the equilibrium x, the point at which zero velocity 
		and zero acceleration are allowed. Note, that for most
		model paramters of interest, this is an unstable focus.
		"""
		return (1.0/self.k*self.N*(self.mu_d+(self.mu_s-self.mu_d)/
			(self.vc*self.vc)*pow(abs(self.vb)-self.vc,2)))

	def free_accel(self):
		"""
		Newton's equations of motion for the violin string lumped
		mass model.
		"""
		acc=(self.restoring_force()+self.friction_force())/self.m
		return (self.x[1],acc)

	def __call__(self):
		"""
		State time derivative is returned based on the discrete
		state of the system. The dynamics in the bow state are 
		trivial, since this case is friction dominated.
		"""
		for case in lu.switch(self.dstate):
			if case(self.bowstate):
				return (self.x[1],0.0)
			if case(self.freestate):
				return self.free_accel()
			if case():
				raise Exception("State Halp: %s!"%self.dstate)




def main():
	# system=Violin(N=0.8,m=0.1,k=0.1*pow(440*(2.0*pi),2),max_events=20) # 440 A
	# system=Violin(N=0.1,m=0.02,vb=0.015,vc=0.07,k=0.02*pow(440*(2.0*pi),2),max_events=20) # 440 A
	system=Violin(mu_s=1.5, mu_d=1.0, N=0.400, vc=1.0, 
		m=1.0, vb=1.0, k=1.0, max_events=7)
	system.x=-1.5,0.0
	system.reset()
	simulate=True
	switch_times=[]
	if simulate:
		logger=ly.Recorder(system)
		stepper=ly.dormand_prince(system, np.linspace(0.0,8.4,20000))
		print stepper.time_tolerance
		stepper.time_tolerance=1e-6
		# print stepper.relative_tolerance
		# print stepper.absolute_tolerance
		i=0
		j=0
		for t, events in stepper:
			j+=1
			# if j%100==0:
			# 	print system.events[0]()
			# print system.state
			assert system.state.t==t
			logger.log(events)
			if events:
				if system.handle_events(stepper,events):
					break
				switch_times.append(t)
				plt.plot([system.x[0]],[system.x[1]],'k.')
				# plt.plot([t,t],[0,0.02],'k')
			try:
				ly.check_NaN(system)
			except ArithmeticError:
				break
		xs=np.array(logger.x)
		# deltas=[x-y for (x,y) in zip(switch_times[2:],switch_times[0:-2])]
		# print deltas
		# plt.plot(system.equilibrium_point(),0,'r+')
		# period=deltas[-1]
		# print period
		# freq=1.0/period
		# print "frequency is %f"%freq
		# print logger.events
		# plt.plot([])
		plt.plot(xs[:,0],xs[:,1] )

	friction_plot=False
	if friction_plot:
		plt.figure()
		us=np.linspace(-0.02,0.02,1000)
		fs=[]
		system.mu_d=1.2
		system.mu_s=1.6
		system.vb=0.012 # 1.2 cm/s
		system.vc=0.01 # 1 cm/s
		for u in us:
			# print system.friction_force()
			fs.append(system.mu(u))
		plt.plot(us,fs)
	plt.show()

if __name__=="__main__":
	main()