import ly_utils as ly
import matplotlib.pyplot as plt
from plot_setup import *
from BiChatterer import BiChatterer
from functools import partial
from math import sqrt
import random as rand

import harddrive_constants as true_hd
import wrong_harddrive_constants as model_hd
# from harddrive_constants import *
"""FBL version!"""

from HD_filters import *


def get_noise_functions(tf, fq, scale=1.0):
    noise_a = noise(tf, fq, scale * true_hd.DISTURBANCE_TO_ACTUATOR)
    noise_f = noise(tf, fq, scale * true_hd.DISTURBANCE_TO_FLEXURE)
    return noise_a, noise_f


@ly.named_state_system("HDObsState", ["i_a", "o_a", "w_a", "i_f", "o_f", "w_f"])
class HDObs(object):

    def __init__(self, hd):
        self.hd = hd
        self.state = 0, (0, 0, 0, 0, 0, 0)

    def __call__(self):
        o_a, o_f, i_a, i_f = hd.measurement
        Xd_est = np.array(self.e_call())
        C = np.array()
        return tuple(Xd_est)

    def e_call(self, sign_w_a=0):
        i_a, o_a, w_a, i_f, o_f, w_f = self.state[1]
        v_a, v_f = self.hd.v_a, self.hd.v_f
        id_f = self.v_f / Lf - Rf / Lf * i_f
        id_a = self.v_a / La - Ra / La * i_a
        wd_a = (
            1 / Jastar * (
                Ka * i_a
                # - beta * {True: 1, False: -1, 0: 0}[sign_w_a]
                - (Kf * self.i_f + d_f)
            )
            + Jf / Jastar * (
                Ka / Ja * i_a
                + B * (1 + alpha_b * abs(w_f)) * w_f
                + K * (1 + alpha_k * o_f**2) * o_f
            )
        )
        wd_f = (
            1 / Jf * (Kf * i_f + d_f)
            - Ka * i_a / Ja
            - B * (1 + alpha_b * abs(w_f)) * w_f
            - K * (1 + alpha_k * o_f**2) * o_f)
        return (id_a, w_a, wd_a, id_f, w_f, wd_f)


@ly.named_state_system("HardDriveState", ["i_a", "o_a", "w_a", "i_f", "o_f", "w_f"])
class HardDrive(object):
    """A hard drive control mechanism with an actuator and a flexure."""

    def __init__(self, k_y=5e3, k_psi=1e3, i_a0=0, o_a0=0,
                 w_a0=0, i_f0=0, o_f0=0, w_f0=0, goal=lambda x: 0,
                 noise_a=lambda t: 0, noise_f=lambda t: 0):
        self.state = 0, (i_a0, o_a0, w_a0, i_f0, o_f0, w_f0)
        self.noise_a = BWN(lambda: noise_a(self.get_t()),
                           n=2, k=(1.0 * pi * noise_a.freq_hz))
        self.noise_f = BWN(lambda: noise_f(self.get_t()), n=2, k=(1.0 * pi * noise_f.freq_hz)
                           )
        self.k_y = k_y
        self.k_psi = k_psi
        self.goal = BW4(lambda: goal(self.t), k=6e3)
        self.events = []
        self.fric = BiChatterer(
            self, lambda: self.w_a, lambda: (0, 0, 1, 0, 0, 0),
            "sign_w_a", name="friction", eps_time=1e-12)
        self.sanity_y = BW4(lambda: self.o_a + self.o_f, k=4e4)
        self.sanity_psi = BW4(lambda: self.o_a - self.o_f, k=4e4)
        self.par = ly.ParallelSystems(
            [self.fric,  self.goal, self.sanity_y, self.sanity_psi, self.noise_a, self.noise_f])

    def get_t(self):
        return self.t

    @property
    def system(self):
        return self.par

    def reset(self, t, state):
        self.state = t, state
        self.fric.reset()

    def measurement(self):
        return (self.o_a, self.o_f, self.i_a, self.i_f)

    def yddd_des(self):
        y, yd, ydd, yddd = self.sanity_y.state[1]
        x, xd, xdd, xddd = self.goal.state[1]
        return (
            xddd
            - 2 * self.k_y * (ydd - xdd)
            - 2 * self.k_y**2 * (yd - xd)
            - self.k_y**3 * (y - x))

    def psiddd_des(self):
        psi, psid, psidd, psiddd = self.sanity_psi.state[1]
        return (
            - 2 * self.k_psi * psidd
            - 2 * self.k_psi**2 * psid
            - self.k_psi**3 * psi)

    def yddd_psiddd(self):
        return (true_hd.get_yddd(self.state[1], self.voltages()),
                true_hd.get_psiddd(self.state[1], self.voltages()))

    @property
    def est_normal_state(self):
        y, yd, ydd, yddd = self.sanity_y.state[1]
        psi, psid, psidd, psiddd = self.sanity_psi.state[1]
        return (y, yd, ydd, psi, psid, psidd)

    def voltages(self):
        est_state = model_hd.de_convert_state(self.est_normal_state)
        i_a, o_a, w_a, i_f, o_f, w_f = est_state
        yddd_des = self.yddd_des()
        psiddd_des = self.psiddd_des()
        wd_f = model_hd.get_wd_f(est_state)
        u_y = model_hd.get_u_y(est_state, yddd_des)
        u_psi = model_hd.get_u_psi(est_state, psiddd_des)
        v_a, v_f = model_hd.get_voltages(u_y, u_psi)

        # assert yddd_des == get_yddd(est_state, (v_a, v_f))
        # assert psiddd_des == get_psiddd(est_state, (v_a, v_f))

        return v_a, v_f

    def __call__(self, sign_w_a=0):
        try:
            d_a, d_f = self.noise_a.x, self.noise_f.x
            # d_a, d_f = 0, 0
            return true_hd.get_dynamics(self.state[1], (d_a, d_f), self.voltages(), sign_w_a=sign_w_a)
        except OverflowError:
            raise ly.StopIntegration()

    def sim(self, tf=100, n=1001):
        sys = self.system
        stepper = ly.dormand_prince(sys, np.linspace(self.t, tf, n))
        # stepper = ly.euler(sys, np.linspace(self.t, tf, n))
        stepper.absolute_tolerance = 1e-4
        stepper.relative_tolerance = 1e-4
        stepper.time_tolerance = 1e-16
        for jump, time, state, guard in ly.hybrid_integrate(stepper):
            ly.check_NaN(self)
            yield self


def approximate_matrix(eps):
    h = HardDrive()
    z = np.zeros(6)
    h.reset(0, tuple(z))
    eh = np.array(h(sign_w_a=0))
    m = np.zeros((6, 6))
    for i in range(0, 6):
        ei = np.zeros(6)
        ei[i] = eps
        si = tuple(z + ei)
        h.reset(0, si)
        m[:, i] = np.array(h(sign_w_a=0)) / eps
    for r in m:
        for c in r:
            print c
    print("[\n\t" + ";\n\t".join([", ".join(["%+.4e"] * 6)] * 6) +
          "\n]") % tuple((c for r in m for c in r))
    return m


def plot_general(tf=1e-6, n=1001, scale=1e-3):

    noise_a, noise_f = get_noise_functions(tf, 1e-1 * n / tf, scale=1e4)
    fig = plt.figure(figsize=(6.5, 13))

    rand.seed(25)
    st = true_hd.random_state(scale)
    goal = lambda t: (sin((t - 0.03) * 100.)if t > 0.03 else -1.0 + (2.0 / 0.02) * (t - 0.02) if t >
                      0.02 else 1.0) if t > 0.01 else 0.0

    print st
    hd = HardDrive(w_a0=st[0], o_a0=st[1] - 1., i_a0=st[2],
                   w_f0=st[3], o_f0=st[4], i_f0=st[5], goal=goal, noise_a=noise_a, noise_f=noise_f)
    simdat, estdat, goaldat, desdat, sanity_dat, raw_state_dat = [], [], [], [], [], []
    noise = []
    for h in hd.sim(tf=tf, n=n):
        print h.t, sum(s**2 for s in h.state[1])
        y, yd, ydd, psi, psid, psidd = true_hd.convert_state(h.state[1])
        raw_state_dat.append(h.state[1])
        yddd, psiddd = h.yddd_psiddd()
        v_a, v_f = h.voltages()
        simdat.append([h.t, v_a, v_f, y, yd, ydd,
                       yddd, psi, psid, psidd, psiddd])
        # estimator and empirical tripple dots
        # y, yd, ydd, psi, psid, psidd = convert_state(h.estimator.est)
        # yddd, psiddd = h.LPydd.xd, h.LPpsidd.xd
        # estdat.append([y, yd, ydd, yddd, psi, psid, psidd, psiddd])
        x, xd, xdd, xddd = h.goal.state[1]
        goaldat.append([x, xd, xdd, xddd])
        desdat.append([h.yddd_des(), h.psiddd_des()])
        noise.append(h.noise_f.x)

        y, yd, ydd, yddd = h.sanity_y.state[1]
        psi, psid, psidd, psiddd = h.sanity_psi.state[1]
        sanity_dat.append([y, yd, ydd, yddd, psi, psid, psidd, psiddd])

    labels = [
        r"$V_A$",
        r"$V_F$",
        r"$y$",
        r"$\dot{y}$",
        r"$\ddot{y}$",
        r"$\dddot{y}$",
        r"$\psi$",
        r"$\dot\psi$",
        r"$\ddot\psi$",
        r"$\dddot{\psi}$"
    ]
    simdat = np.array(simdat)
    estdat = np.array(estdat)
    goaldat = np.array(goaldat)
    desdat = np.array(desdat)
    sanity_dat = np.array(sanity_dat)
    raw_state_dat = np.array(raw_state_dat)
    t = simdat[:, 0]
    y = simdat[:, 2]
    n = 10
    axs = [fig.add_subplot(n, 1, 1)]
    for i in range(1, n):
        axs.append(fig.add_subplot(n, 1, i + 1, sharex=axs[0]))
    for i in range(0, n):
        axs[i].plot(t, simdat[:, i + 1], zorder=10,
                    color=new_cycle[0])  # black
        axs[i].hold(True)
        axs[i].set_ylabel(labels[i], size=16)
    for i in range(0, 8):
        axs[i + 2].plot(t, sanity_dat[:, i], zorder=2,
                        color=new_cycle[1])  # midnight blue
    # for i in range(0, 8):
    # axs[i + 2].plot(t, sanity_dat[:, i], zorder=-12, color=new_cycle[4]) #
    # # bright green
    # axs[2].plot(t, raw_state_dat[:,1], zorder=20, color=custom_cm(0.95))
    # axs[6].plot(t, raw_state_dat[:,1], zorder=20, color=custom_cm(0.95))
    # axs[2].plot(t, raw_state_dat[:,4], zorder=20, color=custom_cm(0.85))
    # axs[6].plot(t, raw_state_dat[:,4], zorder=20, color=custom_cm(0.85))
    for i in range(0, 3):
        axs[i + 2].plot(t, goaldat[:, i], zorder=1,
                        color=new_cycle[2])  # greenish blue
    axs[2].plot(t, [goal(k) for k in t], zorder=-1,
                color=new_cycle[3])  # bright green
    axs[5].plot(t, desdat[:, 0], color=new_cycle[3])
    axs[9].plot(t, desdat[:, 1], color=new_cycle[3])
    axs[-1].set_xlabel(r"$t$", size=14)

    fig.tight_layout()
    return fig

name_string = "../../../tex/homework-NonLinear/hw3/figs/FBL_HD_NOISE%s.pdf"


def main():
    plot_general(tf=6.0e-2, n=10001, scale=1e-3).savefig(name_string % "_1")
    plt.show()


if __name__ == '__main__':
    main()
