import ly_utils as ly
from collections import namedtuple
import numpy as np
from math import *
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import axes3d
from plot_setup import *

@ly.named_state_system("IntegratorState", ["x", "xd"])
class HybridVSBIntegrator(object):

    """Benito's Integrator"""

    def __init__(self, x0=0, xd0=1e-7, kp=2.0, kn=0.5):
        self.state = 0.0, (x0, xd0)
        self.kp = kp
        self.kn = kn
        self.k = kp
        self.l = 2.0
        self.g = 10.0
        self.m = 5.0
        self.j = 0
        try:
            if x0*xd0>0:
                self.pos.jump(init=True)
            else:
                self.neg.jump(init=True)
        except ly.StopIntegration:
            print "we must stop as soon as we are integrated"
        except ly.Jump:
            pass

        # self.events=[self.capture, self.]


    @ly.Guard
    def stop_me(self):
        return ly.EventThrowIf[True]

    @stop_me.jumper
    def stop_me(self):
        raise ly.StopIntegration()

    @ly.Guard
    def pos(self):
        return ly.EventThrowIf[self.xd*self.x > 0]

    @pos.jumper
    def pos(self, init=False):
        if not init:
            self.check_capture()
        self.events=[self.neg]
        self.k=self.kp
        raise ly.Jump()

    @ly.Guard
    def neg(self):
        return ly.EventThrowIf[self.xd*self.x < 0]

    @neg.jumper
    def neg(self, init=False):
        if not init:
            self.check_capture()
        self.events=[self.pos]
        self.k=self.kn
        raise ly.Jump()

    def check_capture(self):
        xdd_pos = - self.kp * self.x
        xdd_neg = - self.kn * self.x
        if self.x*self.xd>0:
            if xdd_pos<0 and xdd_neg >0:
                raise ly.StopIntegration()
        else:
            if xdd_pos>0 and xdd_neg <0:
                raise ly.StopIntegration()

    def check_enter_pos(self, x):
        xdd_pos =  - self.kp * self.x
        xdd_neg =  - self.kn * self.x
        if x>0:
            if xdd_pos<0 and xdd_neg >0:
                return True
        else:
            if xdd_pos>0 and xdd_neg <0:
                return True
        return False

    @ly.Guard
    def capture(self):
        return ly.EventThrowIf[False]

    def __call__(self):
        # if self.stopme or self.j>10:
        #     raise ly.StopIntegration()
        alpha = self.g / self.l
        return (self.xd, -self.k * self.x)


def sim(pendulum):
    stepper = ly.runge_kutta4(pendulum, np.linspace(0, 100, 10001))
    times, mag, xs, xds = [], [], [], []
    l, m, g = (pendulum.l, pendulum.m, pendulum.g)
    try:
        for jump, time, state, guard in ly.hybrid_integrate(stepper):
            # print time, state[0] / pi
            times.append(time)
            mag.append(sqrt(state[0] ** 2 + state[1] ** 2))
            xs.append(pendulum.x)
            xds.append(pendulum.xd)
    except StopIteration as e:
        print e
    return xs, xds
save_string = "../../../tex/homework-NonLinear/hw2/figs/ns%s.pdf"
def plot13():
    fig = plt.figure(figsize=(6.5, 3))
    plt.hold(True)
    p = HybridVSBIntegrator(x0=0.0, kp=0, kn=0)

    n1=2
    n2 =  20
    cap = lambda x: x if x <= 1 else 2 - x
    new_cycle=[custom_cm(0.9*cap(x)) for x in np.linspace(0.0,2.0,11)][0:-1]
    plt.gca().set_color_cycle(new_cycle)
    mpl.rcParams['lines.linewidth'] = 1.9
    mpl.rcParams['lines.solid_capstyle']= 'round'
    for xd0 in np.linspace(-2, 2, 2):
        for x0 in np.linspace(-4, 4, 20):
            p=HybridVSBIntegrator(x0=x0, xd0=xd0, kp=10, kn=-1)
            x, xd = sim(p)
            color = next(plt.gca()._get_lines.color_cycle)
            plt.plot(x, xd, '-', color = color)
            # plt.plot(x0, xd0, 'o', color = color)
            # plt.plot(x[-1], xd[-1], 'o', color = color)
    plt.xlabel(r"$x$", size=12)
    plt.ylabel(r"$\dot{x}$", size=12)
    plt.title(r"Integrator---[kp,kn]=[%.2f, %.2f] Control" %
              (p.kp, p.kn), family='cmb10', size=14)
    fig.tight_layout()
    return fig

def plot14():
    fig = plt.figure(figsize=(6.5, 3))
    plt.hold(True)
    p = HybridVSBIntegrator(x0=0.0, kp=0, kn=0)

    n1=2
    n2 =  20
    cap = lambda x: x if x <= 1 else 2 - x
    new_cycle=[custom_cm(0.9*cap(x)) for x in np.linspace(0.0,2.0,11)][0:-1]
    plt.gca().set_color_cycle(new_cycle)
    mpl.rcParams['lines.linewidth'] = 1.9
    mpl.rcParams['lines.solid_capstyle']= 'round'
    for xd0 in np.linspace(-2, 2, 2):
        for x0 in np.linspace(-4, 4, 20):
            p=HybridVSBIntegrator(x0=x0, xd0=xd0, kp=10, kn=-1)
            x, xd = sim(p)
            color = next(plt.gca()._get_lines.color_cycle)
            plt.plot(x, xd, '-', color = color)
            # plt.plot(x0, xd0, 'o', color = color)
            # plt.plot(x[-1], xd[-1], 'o', color = color)
    plt.xlabel(r"$x$", size=12)
    plt.ylabel(r"$\dot{x}$", size=12)
    plt.title(r"Integrator---[kp,kn]=[%.2f, %.2f] Control" %
              (p.kp, p.kn), family='cmb10', size=14)
    fig.tight_layout()
    return fig
def gen_V(pendulum, alpha=0.1, lw=1):
    stepper = ly.runge_kutta4(pendulum, np.linspace(0, 100, 10001))
    times, os, ods, mag, V = [], [], [], [], []
    for jump, time, state, guard in ly.hybrid_integrate(stepper):
        # print time, state[0] / pi
        times.append(time)
        os.append(state[0])
        ods.append(state[1])
        mag.append(sqrt(state[0] ** 2 + state[1] ** 2))
        V.append(0.5 * pow(state[0], 2) + 0.5 * alpha * pow(state[1], 2))
    plt.plot(mag, V, lw=lw)
def plot_V(kp, kn, alpha=0.1):
    fig = plt.figure(figsize=(4.5, 3))
    plt.hold(True)
    for x0 in np.linspace(-2, 2, 20):
        gen_V(HybridVSBIntegrator(x0=x0, kp=kp, kn=kn), alpha=alpha)
    plt.gca().axis([0, 2, 0, 2])
    plt.gca().autoscale(False)
    overlay_limits(alpha=alpha, xmax=30)
    plt.xlabel(r"$\|\mathbf{x}\|$", size=12)
    plt.ylabel(r"$\frac{1}{2} x^2+\frac{1}{2}\alpha \dot{x}^2$",
               family='cmr10', size=12)
    plt.title(r"Integrator---[kp,kn]=[%.2f, %.2f] Control" %
              (kp, kn), family='cmb10', size=14)
    fig.tight_layout()
    return fig

def overlay_limits(alpha=0.1, xmax=10, lw=4):
    x = np.linspace(0, xmax, 1000)
    # V = x^2 + alpha xd^2
    # minV = min(X^2, alpha X^2)
    # maxV = max(X^2, alpha X^2)
    if alpha < 1:
        maxV = 0.5 * x ** 2
        minV = alpha * maxV
    else:
        minV = 0.5 * x ** 2
        maxV = alpha * minV
    plt.plot(x, minV, 'r', lw=lw)
    plt.plot(x, maxV, 'b', lw=lw)

def test(kp,kn):

    fig = plt.figure(figsize=(4.5, 3))
    plt.hold(True)

    n1=2
    n2 =  20
    cap = lambda x: x if x <= 1 else 2 - x
    new_cycle=[custom_cm(0.9*cap(x)) for x in np.linspace(0.0,2.0,11)][0:-1]
    plt.gca().set_color_cycle(new_cycle)
    mpl.rcParams['lines.linewidth'] = 1.9
    mpl.rcParams['lines.solid_capstyle']= 'round'
    for xd0 in np.linspace(-2, 2, 2):
        for x0 in np.linspace(-6, 6, 5):
            x, xd = sim(HybridVSBIntegrator(x0=x0, xd0=xd0, kp=kp, kn=kn))
            color = next(plt.gca()._get_lines.color_cycle)
            plt.plot(x, xd, '-', color = color)
            # plt.plot(x0, xd0, 'o', color = color)
            # plt.plot(x[-1], xd[-1], 'o', color = color)
    plt.xlabel(r"$x$", size=12)
    plt.ylabel(r"$\dot{x}$", size=12)
    plt.title(r"Integrator---[kp,kn]=[%.2f, %.2f] Control" %
              (kp, kn), family='cmb10', size=14)
    fig.tight_layout()
    return fig

def main():
    test(2,0.8).savefig(save_string%"test1")
    plot_V(2,0.8, alpha=2/(2.8)).savefig(save_string%"test1V")


    plt.show()

if __name__ == "__main__":
    main()
