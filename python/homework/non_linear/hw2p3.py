import ly_utils as ly
from collections import namedtuple
import numpy as np
from math import *
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import axes3d
from plot_setup import *

@ly.named_state_system("PendulumState", ["x", "xd"])
class HybridVSBPendulum(object):

    """Benito's Pendulum"""

    def __init__(self, x0=0, xd0=1e-7, kp=2.0, kn=0.5):
        self.state = 0.0, (x0, xd0)
        self.kp = kp
        self.kn = kn
        self.k = kp
        self.l = 2.0
        self.g = 10.0
        self.m = 5.0
        self.j = 0
        try:
            if x0*xd0>0:
                self.pos.jump(init=True)
            else:
                self.neg.jump(init=True)
        except ly.StopIntegration:
            print "we must stop as soon as we are integrated"
        except ly.Jump:
            pass

        # self.events=[self.capture, self.]

    def stability_estimate(self, alpha=0.1):
        kdelt = 0.5*(-self.kp+self.kn)
        k0 = 0.5*(-self.kp-self.kn)
        if -kdelt > abs(1+alpha*k0)+abs(alpha*self.g/self.l):
            return True
        return False

    @ly.Guard
    def stop_me(self):
        return ly.EventThrowIf[True]

    @stop_me.jumper
    def stop_me(self):
        raise ly.StopIntegration()

    @ly.Guard
    def pos(self):
        return ly.EventThrowIf[self.xd*self.x > 0]

    @pos.jumper
    def pos(self, init=False):
        if not init:
            self.check_capture()
        self.events=[self.neg]
        self.k=self.kp
        raise ly.Jump()

    @ly.Guard
    def neg(self):
        return ly.EventThrowIf[self.xd*self.x < 0]

    @neg.jumper
    def neg(self, init=False):
        if not init:
            self.check_capture()
        self.events=[self.pos]
        self.k=self.kn
        raise ly.Jump()

    def check_capture(self):
        xdd_pos = -self.g / self.l * sin(self.x) - self.kp * self.x
        xdd_neg = -self.g / self.l * sin(self.x) - self.kn * self.x
        if self.x*self.xd>0:
            if xdd_pos<0 and xdd_neg >0:
                raise ly.StopIntegration()
        else:
            if xdd_pos>0 and xdd_neg <0:
                raise ly.StopIntegration()

    def check_enter_pos(self, x):
        xdd_pos = -alpha * sin(self.x) - self.kp * self.x
        xdd_neg = -alpha * sin(self.x) - self.kn * self.x
        if x>0:
            if xdd_pos<0 and xdd_neg >0:
                return True
        else:
            if xdd_pos>0 and xdd_neg <0:
                return True
        return False

    @ly.Guard
    def capture(self):
        return ly.EventThrowIf[False]

    def __call__(self):
        # if self.stopme or self.j>10:
        #     raise ly.StopIntegration()
        alpha = self.g / self.l
        return (self.xd, - alpha * sin(self.x) -self.k * self.x)


def sim(pendulum):
    stepper = ly.runge_kutta4(pendulum, np.linspace(0, 100, 10001))
    times, mag, xs, xds = [], [], [], []
    l, m, g = (pendulum.l, pendulum.m, pendulum.g)
    try:
        for jump, time, state, guard in ly.hybrid_integrate(stepper):
            # print time, state[0] / pi
            times.append(time)
            mag.append(sqrt(state[0] ** 2 + state[1] ** 2))
            xs.append(pendulum.x)
            xds.append(pendulum.xd)
    except StopIteration as e:
        print e
    return xs, xds

def plot13():
    fig = plt.figure(figsize=(6.5, 3))
    plt.hold(True)
    p = HybridVSBPendulum(x0=0.0, kp=0, kn=0)

    n1=2
    n2 =  20
    cap = lambda x: x if x <= 1 else 2 - x
    new_cycle=[custom_cm(0.9*cap(x)) for x in np.linspace(0.0,2.0,11)][0:-1]
    plt.gca().set_color_cycle(new_cycle)
    mpl.rcParams['lines.linewidth'] = 1.9
    mpl.rcParams['lines.solid_capstyle']= 'round'
    for xd0 in np.linspace(-2, 2, 2):
        for x0 in np.linspace(-4, 4, 20):
            p=HybridVSBPendulum(x0=x0, xd0=xd0, kp=10, kn=-1)
            x, xd = sim(p)
            color = next(plt.gca()._get_lines.color_cycle)
            plt.plot(x, xd, '-', color = color)
            # plt.plot(x0, xd0, 'o', color = color)
            # plt.plot(x[-1], xd[-1], 'o', color = color)
    plt.xlabel(r"$x$", size=12)
    plt.ylabel(r"$\dot{x}$", size=12)
    plt.title(r"Pendulum---[kp,kn]=[%.2f, %.2f] Control" %
              (p.kp, p.kn), family='cmb10', size=14)
    fig.tight_layout()
    fig.savefig("../../../tex/homework-NonLinear/hw2/figs/plot13.pdf")

def plot14():
    fig = plt.figure(figsize=(6.5, 3))
    plt.hold(True)
    p = HybridVSBPendulum(x0=0.0, kp=0, kn=0)

    n1=2
    n2 =  20
    cap = lambda x: x if x <= 1 else 2 - x
    new_cycle=[custom_cm(0.9*cap(x)) for x in np.linspace(0.0,2.0,11)][0:-1]
    plt.gca().set_color_cycle(new_cycle)
    mpl.rcParams['lines.linewidth'] = 1.9
    mpl.rcParams['lines.solid_capstyle']= 'round'
    for xd0 in np.linspace(-2, 2, 2):
        for x0 in np.linspace(-4, 4, 20):
            p=HybridVSBPendulum(x0=x0, xd0=xd0, kp=10, kn=-1)
            x, xd = sim(p)
            color = next(plt.gca()._get_lines.color_cycle)
            plt.plot(x, xd, '-', color = color)
            # plt.plot(x0, xd0, 'o', color = color)
            # plt.plot(x[-1], xd[-1], 'o', color = color)
    plt.xlabel(r"$x$", size=12)
    plt.ylabel(r"$\dot{x}$", size=12)
    plt.title(r"Pendulum---[kp,kn]=[%.2f, %.2f] Control" %
              (p.kp, p.kn), family='cmb10', size=14)
    fig.tight_layout()
    fig.savefig("../../../tex/homework-NonLinear/hw2/figs/plot14.pdf")

def plot15():
    fig = plt.figure(figsize = (6.5, 6.5))
    plt.hold(True)
    p = HybridVSBPendulum(x0=0.0, kp=0, kn=0)
    k0s = np.linspace(-20,20,101)
    chatter = -np.abs(k0s)-abs(p.g/p.l)
    alpha=0.05
    lyap = -np.abs(1+alpha*k0s)-abs(alpha*p.g/p.l)
    new_upper_bound = 1/alpha - p.g/p.l+k0s
    new_upper_bound2 = -1/alpha -0.20*p.g/p.l-k0s
    new_chatter_low = p.g/p.l - k0s
    new_chatter_high = -p.g/p.l*(-0.20)+k0s


    plt.arrow(0,0,-9,-9, head_width=0.6, head_length=1.0, fc='k', ec='k')
    plt.arrow(0,0,-9,+9, head_width=0.6, head_length=1.0, fc='k', ec='k')
    plt.text(-11,-11,r"$k_p$", rotation=45, size=15)
    plt.text(-11,+11,r"$k_n$", rotation=-45, size=15)
    plt.plot(k0s, chatter, 'red')
    plt.plot(k0s, lyap, 'green')

    plt.plot(k0s, new_chatter_low, 'r--')
    plt.plot(k0s, new_chatter_high, 'g--')

    plt.plot(k0s, new_upper_bound,'blue')
    plt.plot(k0s, new_upper_bound2, 'cyan')

    plt.axis('equal')
    plt.axis([-20,20,-25,12])
    plt.autoscale(False)
    plt.fill_between(k0s, chatter, lyap, color='#e5e5e3')


    experimental = [
        (5, -5, True, True),
        (-5, -5, True, False),
        (0, -10, True, None),
        (-15, -10, False, False),
        (-15, -1.5, False, False),
        (-15, -0.5, False, False),
        (-15, -1.5, False, False),
        (-15, 10, False, True),
        (-10, -1.5, False, False),
        (-5, 5, False, True),
        (-10, -10, True, False),
        (0, -5, True, None),
        (-5, -0.01, False, False),
        (5, -0.01, None, True),
        (-5, -4, False, False),
        (-10, -9, False, False),
        (-15, -14, False, False),
        (-10, 1, False, True),
        (-0.5, -0.25, True, False)
    ]
    for x, y, chat, stab in experimental:
        if chat:
            plt.plot(x,y,'rx',ms=20, lw=4, markeredgewidth=2)
        if False==chat:
            plt.plot(x,y,'ro',ms=20, lw=4, markeredgewidth=2,markeredgecolor='red', markerfacecolor='None')
        if stab:
            plt.plot(x,y,'gx',ms=16, lw=4, markeredgewidth=2)
        if False==stab:
            plt.plot(x,y,'go',ms=16, lw=4, markeredgewidth=2,markeredgecolor='green', markerfacecolor='None')

    plt.xlabel(r"$k_0$", size=15)
    plt.ylabel(r"$k_\delta$", size=15)
    plt.tight_layout()
    fig.savefig("../../../tex/homework-NonLinear/hw2/figs/plot15.pdf")
def test(k0,kd,name):
    kp=-(k0+kd)
    kn=-(k0-kd)

    fig = plt.figure(figsize=(6.5, 3))
    plt.hold(True)

    n1=2
    n2 =  20
    cap = lambda x: x if x <= 1 else 2 - x
    new_cycle=[custom_cm(0.9*cap(x)) for x in np.linspace(0.0,2.0,11)][0:-1]
    plt.gca().set_color_cycle(new_cycle)
    mpl.rcParams['lines.linewidth'] = 1.9
    mpl.rcParams['lines.solid_capstyle']= 'round'
    for xd0 in np.linspace(-2, 2, 2):
        for x0 in np.linspace(-6, 6, 5):
            x, xd = sim(HybridVSBPendulum(x0=x0, xd0=xd0, kp=kp, kn=kn))
            color = next(plt.gca()._get_lines.color_cycle)
            plt.plot(x, xd, '-', color = color)
            # plt.plot(x0, xd0, 'o', color = color)
            # plt.plot(x[-1], xd[-1], 'o', color = color)
    plt.xlabel(r"$x$", size=12)
    plt.ylabel(r"$\dot{x}$", size=12)
    plt.title(r"Pendulum---[kp,kn]=[%.2f, %.2f] Control" %
              (kp, kn), family='cmb10', size=14)
    fig.tight_layout()
    fig.savefig("../../../tex/homework-NonLinear/hw2/figs/test3_%s.pdf"%name)

def main():

    # print HybridVSBPendulum(x0=1, kp=2, kn=0.5).stability_estimate(alpha=0.2)
    plot15() # (red x if chatter else o, green x if unstable else o)
    # test(5,-5,"stable") #bad in all possible ways (red x, green x)
    # test(-5,-5, "stable") # actually chatter (red x, green o)
    # test(0,-10,"stable") # definite chatter (red x)
    # test(-15,-10,'stable') # actually stable (red o, green o)
    # test(-15,-1.5, 'stable2') # stable, near boundry (red o , green o)
    # test(-15,-0.5, "unstable") # actually looks stable, hard to tell (red o, green o)
    # test(-15,-1.5, "unstable") # actually stable (red o , green o)
    # test(-15,10,'maybe') #definite unstable (red o, green x)
    # test(-10,-1.5,"stable") # (red o, green o)
    # test(-5, 5, "unstable") # (red o, green x)
    # test(-10, -10, "unstable") # (red x, green o)
    # test(0, -5, "unstable") # (red x, green )
    test(-0.5, -0.25, "beautiful") # (red o, green o)
    # test(5,5)
    # print HybridVSBPendulum(x0=1, kp=10, kn=-1000).stability_estimate()
    # plot_Lyap()

    plt.show()

if __name__ == "__main__":
    main()
