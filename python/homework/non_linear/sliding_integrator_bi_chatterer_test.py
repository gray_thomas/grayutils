import ly_utils as ly
from collections import namedtuple
import numpy as np
from math import *
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from plot_setup import *
from BiChatterer import BiChatterer


@ly.named_state_system("IntegratorState", ["x", "xd"])
class SlidingModeIntegrator(object):

    """Simple Integrator"""

    def __init__(self, x0=0, xd0=1e-7, eta=1, lambda_=0.1):
        self.state = 0.0, (x0, xd0)
        self.eta = eta
        self.lambda_ = lambda_
        self.events = []
        self.chatterer = BiChatterer(
            self, self.sigma, self.del_sigma, self.xdd_pos, self.xdd_neg)

    def sigma(self):
        return self.lambda_ * self.x + self.xd

    def del_sigma(self):
        return np.array([self.lambda_, 1.0])

    def xdd_neg(self):
        return self.xd, self.eta

    def xdd_pos(self):
        return self.xd, - self.eta

    def sim(self, tf=100):
        sys = self.chatterer
        stepper = ly.runge_kutta4(sys, np.linspace(0, tf, 101))
        try:
            for jump, time, state, guard in ly.hybrid_integrate(stepper):
                yield self
        except StopIteration as e:
            print e


def sim(pendulum, tf=100):
    times, mag, xs, xds = [], [], [], []
    for p in pendulum.sim(tf=tf):
        times.append(p.t)
        mag.append(sqrt(p.x ** 2 + p.xd ** 2))
        xs.append(p.x)
        xds.append(p.xd)
    return xs, xds


def phase(eta, lambda_, tf=10):
    fig = plt.figure(figsize=(4.5, 3))
    plt.hold(True)

    n1 = 2
    n2 = 20
    cap = lambda x: x if x <= 1 else 2 - x
    new_cycle = [custom_cm(0.9 * cap(x))
                 for x in np.linspace(0.0, 2.0, 11)][0:-1]
    plt.gca().set_color_cycle(new_cycle)
    mpl.rcParams['lines.linewidth'] = 1.9
    mpl.rcParams['lines.solid_capstyle'] = 'round'
    for xd0 in np.linspace(-2, 2, 2):
        for x0 in np.linspace(-4, 4, 20):
            p = SlidingModeIntegrator(x0=x0, xd0=xd0, eta=eta, lambda_=lambda_)
            x, xd = sim(p, tf=tf)
            color = next(plt.gca()._get_lines.color_cycle)
            plt.plot(x, xd, '-', color=color)

    plt.xlabel(r"$x$", size=12)
    plt.ylabel(r"$\dot{x}$", size=12)
    plt.title(r"Integrator---[$\eta$, $\lambda$]=[%.2f, %.2f] Control" %
              (eta, lambda_), family='cmb10', size=14)
    fig.tight_layout()
    return fig


def alt_phase(eta, lambda_, tf=10):
    fig = plt.figure(figsize=(4.5, 3))
    plt.hold(True)

    n1 = 2
    n2 = 20
    cap = lambda x: x if x <= 1 else 2 - x
    new_cycle = [custom_cm(0.9 * cap(x))
                 for x in np.linspace(0.0, 2.0, 11)][0:-1]
    plt.gca().set_color_cycle(new_cycle)
    mpl.rcParams['lines.linewidth'] = 1.9
    mpl.rcParams['lines.solid_capstyle'] = 'round'
    for xd0 in np.linspace(-2, 2, 2):
        for x0 in np.linspace(-4, 4, 20):
            p = SlidingModeIntegrator(x0=x0, xd0=xd0, eta=eta, lambda_=lambda_)
            x, xd = sim(p, tf=tf)
            x = np.array(x)
            xd = np.array(xd)
            sigma = xd + lambda_ * x
            xi = xd + (1 / lambda_) * x
            color = next(plt.gca()._get_lines.color_cycle)
            plt.plot(sigma, xi, '-', color=color)

    plt.xlabel(r"$\sigma$", size=12)
    plt.ylabel(r"$\xi$", size=12)
    plt.title(r"Integrator---[$\eta$, $\lambda$]=[%.2f, %.2f] Control" %
              (eta, lambda_), family='cmb10', size=14)
    fig.tight_layout()
    return fig


def stability():
    fig = plt.figure(figsize=(4.5, 3))

    Path = mpath.Path
    path_data = [
        (Path.MOVETO, (0, 0)),
        (Path.LINETO, (1, 0)),
        (Path.LINETO, (1, 1)),
        (Path.LINETO, (0, 1)),
        (Path.CLOSEPOLY, (0, 0))]
    plt.axis([-1, 1, -1, 1])
    plt.text(0.1, 0.5, "not stable,\nbut center-like")
    plt.text(-0.5, -0.5, "not stable,\nnot center-like")
    codes, verts = zip(*path_data)
    path = mpath.Path(verts, codes)
    patch = mpatches.PathPatch(path, facecolor='r', alpha=0.5)
    plt.gca().add_patch(patch)
    plt.xlabel(r"$k_n$", size=14, family='cmb10')
    plt.ylabel(r"$k_p$", size=14, family='cmb10')
    fig.tight_layout()
    return fig

save = "../../../tex/homework-NonLinear/hw2/figs/ns_sm%s.pdf"


def main():
    # phase(0.5, 0.5,tf=100).savefig(save % "1yphase") #
    # phase(0.15, 0.5,tf=1000).savefig(save % "4phase")
    # phase(0.5, 0.1,tf=10).savefig(save % "5phase")
    # phase(8, 0.5,tf=10).savefig(save % "6phase")
    # alt_phase(0.5, 0.5,tf=100).savefig(save % "1alt")
    # alt_phase(0.15, 0.5,tf=1000).savefig(save % "4alt")
    # alt_phase(0.5, 0.1,tf=10).savefig(save % "5alt")
    phase(2, 0.5, tf=10.0).savefig(save % "test")
    # phase(20,-10,0.5,tf=0.4).savefig(save%"2phase")
    # stability().savefig(save%"stab")
    plt.show()

if __name__ == "__main__":
    main()
