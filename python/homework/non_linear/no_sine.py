import ly_utils as ly
from collections import namedtuple
import numpy as np
from math import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from plot_setup import *


@ly.named_state_system("IntegratorState", ["o", "op"])
class Integrator(object):

    def __init__(self, o0=0, op0=1, k=0):
        self.state = 0.0, (o0, op0)
        self.k = k

    def u(self):
        return -self.k * self.o

    def __call__(self):
        control = self.u()
        return (self.op, - sin(self.o) + control)


@ly.named_state_system("IntegratorState", ["x", "xd"])
class BIntegrator(object):

    """Benito's Integrator"""

    def __init__(self, x0=0, k=0, kd=0):
        self.state = 0.0, (x0, 0.0)
        self.k = k
        self.kd = kd
        self.l = 2.0
        self.g = 10.0
        self.m = 5.0

    def u(self):
        return -self.k * self.x - self.kd * self.xd

    def eigenvalues(self, state):
        self.state = 0.0, state
        update = self()
        for u in update:
            if abs(u) > 1e-7:
                raise NotImplementedError("Not an equilibrium")
        A = np.array([[0.0, 1.0], [-self.k - self.g / self.l, 0.0]])
        eig, vec = np.linalg.eig(A)
        return eig

    def __call__(self):
        return (self.xd, self.u())


@ly.named_state_system("IntegratorState", ["x", "xd"])
class VSBIntegrator(object):

    """Benito's Integrator"""

    def __init__(self, x0=0, kp=2.0, kn=0.5):
        self.state = 0.0, (x0, 0.0)
        self.kp = kp
        self.kn = kn
        self.l = 2.0
        self.g = 10.0
        self.m = 5.0

    def u(self):
        det = self.x * self.xd
        if det > 0:
            return -self.kp * self.x
        return -self.kn * self.x

    def eigenvalues(self, state):
        self.state = 0.0, state
        update = self()
        for u in update:
            if abs(u) > 1e-7:
                raise NotImplementedError("Not an equilibrium")
        A = np.array([[0.0, 1.0], [-self.k - self.g / self.l, 0.0]])
        eig, vec = np.linalg.eig(A)
        return eig

    def __call__(self):
        return (self.xd, self.u())


@ly.named_state_system("IntegratorState", ["x", "xd"])
class hybridVSBIntegrator(object):

    """Benito's Integrator"""

    def __init__(self, x0=0, kp=2.0, kn=0.5):
        self.state = 0.0, (x0, 0.0)
        self.kp = kp
        self.kn = kn
        self.k = kp
        self.l = 2.0
        self.g = 10.0
        self.m = 5.0
        # self.events=[self.capture, self.]

    def stability_estimate(self, alpha=0.1):
        kdelt = 0.5 * (-self.kp + self.kn)
        k0 = 0.5 * (-self.kp - self.kn)
        if -kdelt > abs(1 + alpha * k0) + abs(alpha * self.g / self.l):
            return True
        return False

    def pos(self):
        return ly.EventThrowIf[self.xd * self.x > 0]

    def neg(self):
        return ly.EventThrowIf[self.xd * self.x < 0]

    def check_capture(self, x):
        xdd_pos = -alpha * sin(self.x) - self.kp * self.x
        xdd_neg = -alpha * sin(self.x) - self.kn * self.x
        if x > 0:
            if xdd_pos < 0 and xdd_neg > 0:
                return True
        else:
            if xdd_pos > 0 and xdd_neg < 0:
                return True
        return False

    def check_enter_pos(self, x):
        xdd_pos = -alpha * sin(self.x) - self.kp * self.x
        xdd_neg = -alpha * sin(self.x) - self.kn * self.x
        if x > 0:
            if xdd_pos < 0 and xdd_neg > 0:
                return True
        else:
            if xdd_pos > 0 and xdd_neg < 0:
                return True
        return False

    @ly.Guard
    def capture(self):
        return ly.EventThrowIf[False]

    def eigenvalues(self, state):
        self.state = 0.0, state
        update = self()
        for u in update:
            if abs(u) > 1e-7:
                raise NotImplementedError("Not an equilibrium")
        A = np.array([[0.0, 1.0], [-self.k - self.g / self.l, 0.0]])
        eig, vec = np.linalg.eig(A)
        return eig

    def __call__(self):
        alpha = self.g / self.l
        return (self.xd, - alpha * sin(self.x) - self.k * self.x)


def gen_phase(pendulum):
    stepper = ly.runge_kutta4(pendulum, np.linspace(0, 100, 10001))
    times, os, ods = [], [], []
    for jump, time, state, guard in ly.hybrid_integrate(stepper):
        # print time, state[0] / pi
        times.append(time)
        os.append(state[0])
        ods.append(state[1])
    plt.plot(os, ods)


def gen_V(pendulum, alpha=0.1, lw=1):
    stepper = ly.runge_kutta4(pendulum, np.linspace(0, 100, 10001))
    times, os, ods, mag, V = [], [], [], [], []
    for jump, time, state, guard in ly.hybrid_integrate(stepper):
        # print time, state[0] / pi
        times.append(time)
        os.append(state[0])
        ods.append(state[1])
        mag.append(sqrt(state[0] ** 2 + state[1] ** 2))
        V.append(0.5 * pow(state[0], 2) + 0.5 * alpha * pow(state[1], 2))
    plt.plot(mag, V, lw=lw)


def gen_new_V(pendulum, kp=0.1, lw=1):
    stepper = ly.runge_kutta4(pendulum, np.linspace(0, 100, 10001))
    times, mag, V = [], [], []
    l, m, g = (pendulum.l, pendulum.m, pendulum.g)
    for jump, time, state, guard in ly.hybrid_integrate(stepper):
        # print time, state[0] / pi
        times.append(time)
        mag.append(sqrt(state[0] ** 2 + state[1] ** 2))
        x, xd = pendulum.x, pendulum.xd
        # m*l*g*(cos(x)-1.0)+
        V.append(m * l * g * (1 - cos(x)) + 0.5 * m *
                 pow(l * xd, 2) + 0.5 * kp * m * pow(l * x, 2))
    plt.plot(mag, V, lw=lw)


def sim(pendulum):
    stepper = ly.runge_kutta4(pendulum, np.linspace(0, 100, 10001))
    times, mag, xs, xds = [], [], [], []
    l, m, g = (pendulum.l, pendulum.m, pendulum.g)
    for jump, time, state, guard in ly.hybrid_integrate(stepper):
        # print time, state[0] / pi
        times.append(time)
        mag.append(sqrt(state[0] ** 2 + state[1] ** 2))
        xs.append(pendulum.x)
        xds.append(pendulum.xd)
    return xs, xds


def plot_new_V_3D(kp, kd, name, ext="pdf"):
    s = 1.3
    fig = plt.figure(figsize=(s * 4.5, s * 3))
    ax1 = fig.add_subplot(111, projection='3d')
    dpen = BIntegrator()
    m, l, g = dpen.m, dpen.l, dpen.g

    @np.vectorize
    def V(x, xd):
        return m * l * g * (1 - cos(x)) + 0.5 * m * pow(l * xd, 2) + \
            0.5 * kp * m * pow(l * x, 2)

    X, Y = np.meshgrid(np.linspace(-12, 12, 60), np.linspace(-6, 6, 30))
    Z = V(X, Y)

    plt.hold(True)
    for x0 in [-20]:
        x, xd = sim(BIntegrator(x0=x0, k=kp, kd=kd))
        ax1.plot(x, xd, V(x, xd), color='k', linewidth=3)
        t = np.linspace(0.0, 1.0)
        ax1.plot(x, xd, V(x, xd), color='w', linewidth=1)
    ax1.plot_wireframe(X, Y, Z, rstride=1, cstride=1, alpha=0.2)
    ax1.set_xlabel(r"x", size=12)
    ax1.set_ylabel(r"xd", size=12)
    ax1.set_zlabel("V'(x, xd)", size=12)
    plt.title(r"Integrator---K=[%.2f, %.2f] Control" %
              (kp, kd), family='cmb10', size=14)
    fig.tight_layout()
    fig.savefig("../../../tex/homework-NonLinear/hw2/figs/%s.%s" % (name, ext))
    # fig2 = plot_PD_phase(kp, kd)
    # fig2.savefig("../../../tex/homework-NonLinear/hw2/figs/new_V_phase.pdf")


def plot_B_V_3D(kp, kd, name, ext="pdf"):
    s = 1.3
    fig = plt.figure(figsize=(s * 4.5, s * 3))
    ax1 = fig.add_subplot(111, projection='3d')
    dpen = BIntegrator()
    m, l, g = dpen.m, dpen.l, dpen.g

    @np.vectorize
    def V(x, xd):
        return 0.1 * pow(xd, 2) + pow(x, 2)

    X, Y = np.meshgrid(np.linspace(-12, 12, 60), np.linspace(-6, 6, 30))
    Z = V(X, Y)

    plt.hold(True)
    for x0 in [-20]:
        x, xd = sim(BIntegrator(x0=x0, k=kp, kd=kd))
        ax1.plot(x, xd, V(x, xd), color='k', linewidth=3)
        t = np.linspace(0.0, 1.0)
        ax1.plot(x, xd, V(x, xd), color='w', linewidth=1)
    ax1.plot_wireframe(X, Y, Z, rstride=1, cstride=1, alpha=0.2)
    ax1.set_xlabel(r"x", size=12)
    ax1.set_ylabel(r"xd", size=12)
    ax1.set_zlabel("V'(x, xd)", size=12)
    plt.title(r"Integrator---K=[%.2f, %.2f] Control" %
              (kp, kd), family='cmb10', size=14)
    fig.tight_layout()
    fig.savefig("../../../tex/homework-NonLinear/hw2/figs/%s.%s" % (name, ext))
    # fig2 = plot_PD_phase(kp, kd)
    # fig2.savefig("../../../tex/homework-NonLinear/hw2/figs/new_V_phase.pdf")


def plot1():
    fig = plt.figure(figsize=(4.5, 3))
    # fig.set_size_inches((2,3))
    plt.hold(True)
    for x0 in np.linspace(-10, 10, 20):
        gen_phase(BIntegrator(x0=x0, k=0))
    plt.xlabel("Angle", family='cmr10', size=12)
    plt.ylabel("Angular Velocity", family='cmr10', size=12)
    plt.title(r"Integrator---No Control", family='cmb10', size=14)
    fig.tight_layout()
    return fig


def plot2():
    fig = plt.figure(figsize=(4.5, 3))
    # fig.set_size_inches((2,3))
    plt.hold(True)
    for x0 in np.linspace(-20, 20, 40):
        gen_phase(BIntegrator(x0=x0, k=0.5))
    plt.xlabel("Angle", family='cmr10', size=12)
    plt.ylabel("Angular Velocity", family='cmr10', size=12)
    plt.title(r"Integrator---K=0.5 Control", family='cmb10', size=14)
    fig.tight_layout()
    return fig


def plot2_and_haif():
    fig = plt.figure(figsize=(4.5, 3))
    # fig.set_size_inches((2,3))
    plt.hold(True)
    for x0 in np.linspace(-20, 20, 40):
        gen_phase(BIntegrator(x0=x0, k=0.25))
    plt.xlabel("Angle", family='cmr10', size=12)
    plt.ylabel("Angular Velocity", family='cmr10', size=12)
    plt.title(r"Integrator---K=0.25 Control", family='cmb10', size=14)
    fig.tight_layout()
    return fig


def plot3():
    fig = plt.figure(figsize=(4.5, 3))
    # fig.set_size_inches((2,3))
    plt.hold(True)
    for x0 in np.linspace(-10, 10, 20):
        gen_phase(BIntegrator(x0=x0, k=2))
    plt.xlabel("Angle", family='cmr10', size=12)
    plt.ylabel("Angular Velocity", family='cmr10', size=12)
    plt.title(r"Integrator---K=2 Control", family='cmb10', size=14)
    fig.tight_layout()
    return fig


def plot4():
    fig = plt.figure(figsize=(4.5, 3))
    # fig.set_size_inches((2,3))
    plt.hold(True)
    for x0 in np.linspace(-10, 10, 20):
        gen_V(BIntegrator(x0=x0, k=1.0), alpha=0.10)
    plt.gca().autoscale(False)
    overlay_limits(alpha=0.1, xmax=30)
    plt.xlabel(r"$\|\mathbf{x}\|$", size=12)
    plt.ylabel(r"$\frac{1}{2} x^2+\frac{1}{2}\alpha \dot{x}^2$",
               family='cmr10', size=12)
    plt.title(r"Integrator---K=1 Control", family='cmb10', size=14)
    fig.tight_layout()
    return fig


def plot4prime():
    fig = plt.figure(figsize=(4.5, 3))
    # fig.set_size_inches((2,3))
    plt.hold(True)
    for x0 in np.linspace(-10, 10, 20):
        gen_phase(BIntegrator(x0=x0, k=1.0))
    plt.xlabel("Angle", family='cmr10', size=12)
    plt.ylabel("Angular Velocity", family='cmr10', size=12)
    plt.title(r"Integrator---K=1 Control", family='cmb10', size=14)
    fig.tight_layout()
    return fig


def plot5(k):
    fig = plt.figure(figsize=(4.5, 3))
    # fig.set_size_inches((2,3))
    plt.hold(True)
    for x0 in np.linspace(-10, 10, 20):
        gen_V(BIntegrator(x0=x0, k=k), alpha=0.10)
    plt.gca().autoscale(False)
    overlay_limits(alpha=0.1, xmax=30)
    plt.xlabel(r"$\|\mathbf{x}\|$", size=12)
    plt.ylabel(r"$\frac{1}{2} x^2+\frac{1}{2}\alpha \dot{x}^2$",
               family='cmr10', size=12)
    plt.title(r"Integrator---K=%.2f Control" % k, family='cmb10', size=14)
    fig.tight_layout()
    return fig


def plot5prime(k):
    fig = plt.figure(figsize=(4.5, 3))
    # fig.set_size_inches((2,3))
    plt.hold(True)
    for x0 in np.linspace(-10, 10, 20):
        gen_phase(BIntegrator(x0=x0, k=k))
    plt.xlabel("Angle", family='cmr10', size=12)
    plt.ylabel("Angular Velocity", family='cmr10', size=12)
    plt.title(r"Integrator---K=%.2f Control" % k, family='cmb10', size=14)
    fig.tight_layout()
    return fig


def plot_PD_phase(k, kd):
    fig = plt.figure(figsize=(4.5, 3))
    # fig.set_size_inches((2,3))
    plt.hold(True)
    for x0 in np.linspace(-10, 10, 20):
        gen_phase(BIntegrator(x0=x0, k=k, kd=kd))
    plt.xlabel("Angle", family='cmr10', size=12)
    plt.ylabel("Angular Velocity", family='cmr10', size=12)
    plt.title(r"Integrator---K=[%.2f, %.2f] Control" %
              (k, kd), family='cmb10', size=14)
    fig.tight_layout()
    return fig


def plot10():
    fig = plt.figure(figsize=(4.5, 3))
    k, kd = (10.0, 1.0)
    plt.hold(True)
    for x0 in np.linspace(-10, 10, 20):
        gen_V(BIntegrator(x0=x0, k=k, kd=kd), alpha=0.10)
    gen_V(BIntegrator(x0=12, k=k, kd=kd), alpha=0.10, lw=4)
    plt.gca().autoscale(False)
    overlay_limits(alpha=0.1, xmax=30)
    plt.xlabel(r"$\|\mathbf{x}\|$", size=12)
    plt.ylabel(r"$\frac{1}{2} x^2+\frac{1}{2}\alpha \dot{x}^2$",
               family='cmr10', size=12)
    plt.title(r"Integrator---K=[%.2f, %.2f] Control" %
              (k, kd), family='cmb10', size=14)
    fig.tight_layout()
    return fig


def plot_V(k, kd, alpha=0.1):
    fig = plt.figure(figsize=(4.5, 3))
    plt.hold(True)
    for x0 in np.linspace(-2, 2, 20):
        gen_V(BIntegrator(x0=x0, k=k, kd=kd), alpha=alpha)
    # gen_V(BIntegrator(x0=12, k=k, kd=kd), alpha=alpha, lw=4)
    plt.gca().axis([0, 2, 0, 2])
    plt.gca().autoscale(False)
    overlay_limits(alpha=alpha, xmax=30)
    plt.xlabel(r"$\|\mathbf{x}\|$", size=12)
    plt.ylabel(r"$\frac{1}{2} x^2+\frac{1}{2}\alpha \dot{x}^2$",
               family='cmr10', size=12)
    plt.title(r"Integrator---K=[%.2f, %.2f] Control" %
              (k, kd), family='cmb10', size=14)
    fig.tight_layout()
    return fig


def plot_new_V(kp, kd):
    fig = plt.figure(figsize=(4.5, 3))
    # fig.set_size_inches((2,3))
    plt.hold(True)
    for x0 in np.linspace(-10, 10, 20):
        gen_new_V(BIntegrator(x0=x0, k=kp, kd=kd), kp=kp)
    plt.xlabel(r"$\|\mathbf{x}\|$", size=12)
    plt.ylabel(r"$V^{\,\prime}(x, \dot{x})$", size=12)
    plt.title(r"Integrator---K=[%.2f, %.2f] Control" %
              (kp, kd), family='cmb10', size=14)
    fig.tight_layout()
    fig.savefig("../../../tex/homework-NonLinear/hw2/figs/new_V.pdf")
    fig2 = plot_PD_phase(kp, kd)
    return fig


def plot12():
    fig = plt.figure(figsize=(4.5, 3))
    plt.hold(True)
    p = VSBIntegrator(x0=0.0, kp=0, kn=0)
    for x0 in np.linspace(-10, 10, 20):
        p = VSBIntegrator(x0=x0, kp=10, kn=-1)
        x, xd = sim(p)
        plt.plot(x, xd)
    plt.xlabel(r"$x$", size=12)
    plt.ylabel(r"$\dot{x}$", size=12)
    plt.title(r"Integrator---[kp,kn]=[%.2f, %.2f] Control" %
              (p.kp, p.kn), family='cmb10', size=14)
    fig.tight_layout()
    return fig





def plot_Lyap(alpha=0.1, name=""):
    fig = plt.figure(figsize=(4.5, 3))
    plt.hold(True)
    overlay_limits(alpha=alpha)

    plt.xlabel(r"$\|\mathbf{x}\|$", size=12)
    plt.ylabel(r"$V(x, \dot{x})$", size=12)
    fig.tight_layout()
    return fig

save_string = "../../../tex/homework-NonLinear/hw2/figs/no_sine%s.pdf"


def main():
    # plot1().savefig(save_string%"1")
    # plot_PD_phase(1,0.0).savefig(save_string%"Pcontrol")
    # plot_V(1,0).savefig(save_string%"Pfunc")
    # plot_V(.2,0,alpha=5.0).savefig(save_string%"Pfunc2")
    # plot_V(5,0,alpha=0.2).savefig(save_string%"Pfunc3")
    # plot_PD_phase(1,1).savefig(save_string%"PD")
    plot_V(1,1,alpha=0.1).savefig(save_string%"PDv")

    plt.show()

if __name__ == "__main__":
    main()
