import matplotlib.pyplot as plt 
import numpy as np 
import phase_portrait as pplt
from math import sqrt, pow, pi
import violin
from stiction_PD import StictionPD

######################## hello plot ############################
if False: 
	def d_constant_vel(X):
		return np.array([X[1],1.0])
	hello_fig=plt.figure()
	IC_range=[np.array([0.0,0.0]), np.array([1.0,0.0])]
	for x in [0.1,0.2,0.3]:
		for xd in [-0.2,0.0,0.2,0.3]:
			IC_range.append(np.array([x,xd]))
	times=np.linspace(0,1.0,1000)
	title="Hello Plot"
	Output_states=pplt.phase_portrait(d_constant_vel, IC_range, times, title)
	arrows=Output_states
	arrows.extend(IC_range)
	arrows.extend(pplt.even_polar_range(0.0,0.0,1.0,15,50))
	# print arrows
	pplt.overlay_arrows(d_constant_vel, arrows)

##################### irrational oscillator ######################
# if True:
if False:
	def generate_irrational_oscillator_func(k,m,a,h,L):
		def derivs(X,t):
			x1=X[0]+a
			x2=X[0]-a
			l1=sqrt(pow(X[0]+a,2)+h*h)
			l2=sqrt(pow(X[0]-a,2)+h*h)
			f1=k*(L-l1)
			f2=k*(L-l2)
			fx1=f1*x1/l1 # = k*(L-l1)*x/l1= k*x1 *(L/l1-1)
			fx2=f2*x2/l2
			fx=fx1+fx2
			accel=fx/m
			return np.array([X[1],accel])
		return derivs

	fig=plt.figure()
	times=np.linspace(0,1.0,1000)
	ICs=pplt.scaled_polar_range(0.0,0.0,1.5,15,4,10.0)
	plt.plot([x[0] for x in ICs],[x[1] for x in ICs],'b.')
	derivs=generate_irrational_oscillator_func(50.0,1.5,1.0,0.1,2.0)
	pplt.phase_portrait(derivs, ICs, times, "Irrational Oscillator")
	fig.savefig("../../../tex/homework-NonLinear/hw1/irrOscPhase.png")

################## violin ################
if False:
# if True: 

	times=np.linspace(0,0.5,5000)

	fig=plt.figure()
	ICs=pplt.scaled_polar_range(0.00034,0.0,0.0001,5,9,1.7e2)
	plt.plot([x[0] for x in ICs],[x[1] for x in ICs],'b.')
	sys=violin.Violin()
	pplt.ly_phase_portrait(sys, ICs, times, "Violin Oscillator")
	QICs=pplt.xy_range(0.00034,0.0,0.0001,1.7e-2,20,20)
	pplt.scipy_overlay_arrows(sys, QICs)
	plt.xlabel("$x$ $m$",fontsize=16)
	plt.ylabel("$\dot x$ $\\frac{m}{s}$",fontsize=16)
	plt.title("Violin Oscillator",fontsize=18, family="serif")
	fig.tight_layout()
	fig.savefig("../../../tex/homework-NonLinear/hw1/violinA.pdf")

################## violins ################
# if False:
if False: 
	systems={
	# "A":violin.Violin(),
	# "B":violin.Violin(N=10),
	# "C":violin.Violin(mu_d=0.3),
	# "D":violin.Violin(N=0.3,m=0.02,vb=0.05,vc=0.4,
	# 	k=0.02*pow(880*(2.0*pi),2),max_events=20),
	"1":violin.Violin(x0=(0.0,0.02),    mu_s=1.0, N=1.000, vc=0.5, 
		mu_d=1.0, m=1.0, vb=1.0, k=1.0),
	"2":violin.Violin(x0=(0.0,0.02),    mu_s=1.5, N=0.250, vc=1.5, 
		mu_d=1.0, m=1.0, vb=1.0, k=1.0),
	"3":violin.Violin(x0=(0.0,0.02),    mu_s=1.0, N=1.000, vc=1.0, 
		mu_d=1.0, m=1.0, vb=1.0, k=1.0),
	"4":violin.Violin(x0=(0.0,0.02),    mu_s=0.5, N=1.000, vc=1.5, 
		mu_d=1.0, m=1.0, vb=1.0, k=1.0),
	"5":violin.Violin(x0=(0.0,0.02),    mu_s=1.5, N=1.000, vc=1.5, 
		mu_d=1.0, m=1.0, vb=1.0, k=1.0),
	"6":violin.Violin(x0=(0.0,0.02),    mu_s=2.0, N=1.000, vc=1.5, 
		mu_d=1.0, m=1.0, vb=1.0, k=1.0),
	"7":violin.Violin(x0=(0.0,0.02),    mu_s=1.5, N=10.000, vc=1.5, 
		mu_d=1.0, m=1.0, vb=1.0, k=1.0),
	"8":violin.Violin(x0=(0.0,0.02),    mu_s=1.5, N=4.000, vc=1.5, 
		mu_d=1.0, m=1.0, vb=1.0, k=1.0),
	"9":violin.Violin(x0=(0.0,0.02),    mu_s=1.5, N=1.000, vc=10.0, 
		mu_d=1.0, m=1.0, vb=1.0, k=1.0),
	}

	times=np.linspace(0,8.5,1000)

	for key in systems.keys():
		fig=plt.figure()
		x_center=systems[key].equilibrium_point()
		y_center=0.0
		y_width=1.7
		x_width=1.7
		ICs=pplt.xy_polar_range(x_center,y_center,x_width,y_width,6,3)
		plt.plot([x[0] for x in ICs],[x[1] for x in ICs],'b.')
		sys=systems[key]
		pplt.ly_phase_portrait(sys, ICs, times, "Violin Oscillator %s"%key)
		QICs=pplt.xy_range(x_center,y_center,x_width,y_width,20,20)
		pplt.scipy_overlay_arrows(sys, QICs)
		plt.xlabel("$x$ $m$",fontsize=16)
		plt.ylabel("$\dot x$ $\\frac{m}{s}$",fontsize=16)
		plt.title("Violin Oscillator %s"%key,fontsize=18, family="serif")
		fig.tight_layout()
		fig.savefig("../../../tex/homework-NonLinear/hw1/violin%s.pdf"%key)



################## dimensionless irrational oscillator ################
if False: 
	a_params=[0.25,0.45,0.50,0.00,0.25,0.50]
	b_params=[0.25,0.45,0.50,0.00,0.00,0.00]
	def generate_dimensionless_oscillator_func(a,b):
		def derivs(X,t):
			x=X[0]
			accel=(-(x+a)*(1.0-1.0/sqrt(pow(x+a,2)+pow(b,2)))
				-(x-a)*(1.0-1.0/sqrt(pow(x-a,2)+pow(b,2))))
			return np.array([X[1],accel])
		return derivs
	
	times=np.linspace(0,10.0,1000)
	for i in range(0,len(a_params)):
		fig=plt.figure()
		ICs=pplt.scaled_polar_range(0.0,00.1,2.0,15,4,1.0)
		plt.plot([x[0] for x in ICs],[x[1] for x in ICs],'b.')
		derivs=generate_dimensionless_oscillator_func(a_params[i],b_params[i])
		pplt.phase_portrait(derivs, ICs, times, "Irrational Oscillator")
		fig.savefig("../../../tex/homework-NonLinear/hw1/dimlPhase%d.png"%i)
############### Magnetic Suspention ####################################
if False:
	m=2.5
	b=1.5
	g=9.8
	mus=[0.5,2.0]
	lambdas=[0.1,1.0]
	ks = [0.5,2.0]
	rs = [0.0,0.5]
	times=np.linspace(0,1.25,1000)
	i=0
	def gen_plot(_mu,_lambda,_k, _r):
		global i 
		fig=plt.figure()
		def derivs(X,t=0):
			y=X[0]
			yd = X[1]
			ydd = -g - b/m*yd + (_lambda * _mu * pow(_k*y-_k*_r,2) )/ (2.0*m*pow(1+_mu*y,2))
			return np.array([yd,ydd])

		scale=4e1
		y_bonus=2.5
		try:
			Q = 2 * m*g*_mu/(_lambda*_k*_k)
			a_ = Q-1
			b_ = 2*(Q/_mu+_r)
			c_ = Q/_mu/_mu-_r*_r
			focus_y=-b_/(2.0*a_)
			if b_*b_-4*a_*c_<0.0:
				raise ArithmeticError()
			focus_y1=(-b_+sqrt(b_*b_-4*a_*c_))/(2.0*a_)
			focus_y2=(-b_-sqrt(b_*b_-4*a_*c_))/(2.0*a_)
			y_width=abs(sqrt(b_*b_-4*a_*c_)/(2.0*a_))*y_bonus
		except ZeroDivisionError as e:
			focus_y=0
			y_width=0.001*scale
			focus_y1=0
			focus_y2=0
		except ArithmeticError as e:
			focus_y=-b_/(2.0*a_)
			focus_y1=focus_y
			focus_y2=focus_y
			y_width=0.001*scale
		
		yd_width=15*y_width
		yd_text=yd_width*2.5
		yd_text_pos=yd_width*2.25
		print focus_y
		# def derivs(X,t=0):
		# 	y = X[0]
		# 	yd = X[1]
		# 	ydd = - 10.0*y 
		# 	return np.array([yd,ydd])
		# focus_y=0.0
		plt.plot(focus_y1,0,'b.',ms=4)
		plt.plot(focus_y2,0,'b.',ms=4)
		ICs= pplt.xy_range(focus_y, 0.0, y_width, yd_width, n_x=5, n_y=5)
		QICs=pplt.xy_range(focus_y, 0.0, y_width, yd_width,    20,    20)
		pplt.phase_portrait(derivs, ICs, times, "$\mu=%0.1f$, $\lambda=%0.1f$, $k=%0.1f$, $r=%0.1f$"%(_mu, _lambda, _k, _r))
		pplt.overlay_arrows(derivs, QICs, minshaft=2.0)
		# plt.text(focus_y,yd_text_pos,
		# 	"$\mu=%0.1f$, $\lambda=%0.1f$, $k=%0.1f$, $r=%0.1f$"%(_mu, _lambda, _k, _r),
		# 	fontsize=12,color='b')
		plt.axis([focus_y-y_width,focus_y+y_width,0-yd_text,0+yd_text])
		fig.savefig("../../../tex/homework-NonLinear/hw1/mag_sus_%d.png"%i)
		i+=1

	gen_plot(0.0,0.99,0.99,0.99)
	for _r in rs:
		for _mu in mus:
			for _lambda in lambdas:
				for _k in ks:
					gen_plot(_mu,_lambda,_k,_r)


############################ Stiction with PD control ###################
if True: 
	systems={
	# "1":StictionPD(f_d=0.1, f_s=0.2, kp=1, kd=0.1),
	# "2":StictionPD(f_d=0.1, f_s=0.5, kp=1, kd=0.1),
	# "3":StictionPD(f_d=0.0, f_s=0.2, kp=1, kd=0.9),
	# "4":StictionPD(f_d=0.2, f_s=0.2, kp=1, kd=0.0),
	# "5":StictionPD(f_d=0.0, f_s=0.5, kp=1, kd=0.0),
	# "6":StictionPD(f_d=0.2, f_s=0.3, kp=1, kd=-0.3),
	# "7":StictionPD(f_d=-0.1, f_s=0.3, kp=1, kd=0.3),
	"8":StictionPD(f_d=-0.05, f_s=0.4, kp=1, kd=-0.05),
	}

	times=np.linspace(0,10.5,1000)

	for key in systems.keys():
		fig=plt.figure()
		x_center=0.0
		y_center=0.1
		y_width=1.0
		x_width=1.0
		ICs=pplt.xy_polar_range(x_center,y_center,x_width,y_width,12,3)
		plt.plot([x[0] for x in ICs],[x[1] for x in ICs],'b.')
		sys=systems[key]
		pplt.ly_phase_portrait(sys, ICs, times, "PD S %s"%key)
		QICs=pplt.xy_range(x_center,y_center,x_width,y_width,20,20)
		pplt.scipy_overlay_arrows(sys, QICs)
		plt.xlabel("$x$ $m$",fontsize=16)
		plt.ylabel("$\dot x$ $\\frac{m}{s}$",fontsize=16)
		plt.title("PD Stiction System %s"%key,fontsize=18, family="serif")
		fig.tight_layout()
		fig.savefig("../../../tex/homework-NonLinear/hw1/stiction%s.pdf"%key)




plt.show()

