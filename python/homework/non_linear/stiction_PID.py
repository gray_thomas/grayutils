import lyapunov as ly
import numpy as np 
import matplotlib.pyplot as plt
from math import pow, sqrt, pi, copysign
import ly_utils as lu

def sign(x):
	return copysign(1.0,x)

@lu.hybridsystemfunctor
class StictionPD(object):
	def __init__(self, x0=(0.0,1.0), m=1.0, kp=10.0, kd=0.1, f_s=2.0, f_d=1.0, max_events=7):
		self.m=m
		self.kp=kp
		self.kd=kd
		self.f_s=f_s
		self.f_d=f_d
		self.state=0.0,x0

		self.max_events=max_events
		self.stuckstate="stuck"
		self.freestate="free"
		self.stuckevents=[self.friction_break_event]
		self.freeevents=[self.stiction_lock_event]
		self.reset()

	state=ly.state_property(xname="x")

	def stiction_lock_event(self):
		return self.x[1]

	def friction_break_event(self):
		return abs(self.spring_force())-self.f_s

	def spring_force(self):
		return -self.kp*self.x[0]

	def free_derivs(self):
		return (self.x[1], -self.kp*self.x[0]-self.kd*self.x[1]-self.f_d*sign(self.x[1]))

	def __call__(self):
		for case in lu.switch(self.dstate):
			if case(self.freestate):
				return self.free_derivs()
			if case(self.stuckstate):
				return (0.0,0.0)
		raise RuntimeException("hey")

	def reset(self):
		self.num_events=0
		if self.x[1]==0.0:
			self.dstate=self.stuckstate
			self.events=self.stuckevents
		else:
			self.dstate=self.freestate
			self.events=self.freeevents

	def handle_events(self,stepper,events):
		event=events[0] # assume 1 event
		if len(events)>1:
			print "Bonus event ignored!", events[1]
		for case in lu.switch(event):
			if case(self.friction_break_event):
				stepper.step_across((self.x[0],epsilon))
				self.dstate=self.freestate
				self.events=self.freeevents
				print "hey friction_break_event"
				break
			if case(self.stiction_lock_event):
				if self.friction_break_event()<0:
					stepper.step_across((self.x[0],0.0))
					self.dstate=self.stuckstate
					self.events=self.stuckevents
				else:
					epsilon=copysign(1e-7,self.spring_force())
					stepper.step_across((self.x[0],epsilon))
					self.dstate=self.freestate
					self.events=self.freeevents
				break
			if case():
				print "confusion!"
		self.num_events+=1
		if self.num_events>=self.max_events:
			return True # terminate loop
		return False # continue

	def equilibrium_point(self):
		return 0






def main():
	system=StictionPD(kp=1000, kd=10, f_s=10)
	system.x=0.0,1.9
	system.reset()
	# switch_times=[]
	logger=ly.Recorder(system)
	stepper=ly.dormand_prince(system, np.linspace(0.0,2.0,2000))
	print stepper.time_tolerance
	# stepper.time_tolerance=1e-6
	i=0
	j=0
	for t, events in stepper:
		j+=1
		assert system.state.t==t
		logger.log(events)
		# print system.state
		if events:
			if system.handle_events(stepper,events):
				break
			# switch_times.append(t)
			plt.plot([system.x[0]],[system.x[1]],'k.')
			# plt.plot([t,t],[0,0.02],'k')
		try:
			ly.check_NaN(system)
		except ArithmeticError:
			break
	xs=np.array(logger.x)
	plt.plot(xs[:,0],xs[:,1] )

	plt.show()

if __name__=="__main__":
	main()