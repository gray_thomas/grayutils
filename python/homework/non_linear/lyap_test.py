import lyapunov as ly
import numpy as np 
import matplotlib.pyplot as plt

class system_A(object):
	def __init__(self):
		self.state = 0.0, (1.0,1.0)
		self.acceleration = lambda : -9.81

		self.events = [lambda: self.x[0]]
	state=ly.state_property(xname="x")

	def __call__(self):
		# print self.x
		return (self.x[1],self.acceleration())
system=system_A()
logger=ly.Recorder(system)
stepper=ly.runge_kutta4(system, np.linspace(0.0,10.0,1000))

i=0
for t, events in stepper:
	# print system.state
	# assert system.state.t==t
	logger.log()
	if events:
		stepper.step_across((system.x[0],system.x[1]*-0.95))
		print "hi"
		i+=1
		if i>10:
			break

	print system.x
plt.plot(logger.t, np.array(logger.x))
plt.show()

