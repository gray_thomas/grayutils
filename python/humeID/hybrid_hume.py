import lyapunov as ly
import numpy as np
import matplotlib.pyplot as plt
from math import pow, sqrt, pi
import ly_utils as lu
from collections import namedtuple, deque
import time
from gu.math import *
from allen_variance import Hume_IMU_Gyro_Noise_Model, Hume_IMU_Accelerometer_Noise_Model
from leds import new_const
import hume_frames_and_poses as hf
# Frame Class

# class HybridHumeRetroactiveEKF(object):
# 	"""
# 	12+1 state extended kalman filter based on a hybrid robot
# 	model. States model a rigid body transform which takes
# 	coordinate vectors expressed in body frame and expresses them
# 	in the world frame. Key points:
# 		--- Quaternion covariance is measured as a 3 vector
# 		--- Quaternion derivative is measured as a 3 vector
# 		--- Update requires a sensors structure as an input
# 		>>> Either that, or the object uses a sensor provider
# 		--- Filter accounts for delayed sensors by applying
# 				retroactive updates.
# 	"""
# 	def __init__(self, max_delay=15):
# 		self.vec = np.zeros((3,1))
# 		self.quat = Quaternion(1.0,0.0,0.0,0.0)
# 		self.vel = np.zeros((3,1))
# 		self.omega = np.zeros((3,1))
# 		self.co_variance = 1.0e7*np.eye(12)
# 		self.sensor_history=deque(maxlen=max_delay)

# 	def update(self, sensors):
# 		""" updates the system, altering the discrete time state """
# 	def predict(self):
# 		""" predicts sensor measurements """


class ConstantQuantity(object):

    """ A spoof for the future time varying reaction force and inertia objects """

    def __init__(self, val, deriv):
        self.constant_value = val
        self.zero_deriv = deriv

    def get_value(self, time):
        return self.constant_value

    def get_value_dot(self, time):
        return self.zero_deriv


@lu.hybridsystemfunctor
@lu.namedstatesystem("HumeState",
                     ['x', 'y', 'z', 'vx', 'vy', 'vz', 'qw', 'qx', 'qy', 'qz', 'wx', 'wy', 'wz'], xname="X")
class HybridHume(object):

    """
    A hybrid system representing our hume robot in single and dual
    leg-ground contact, when viewed as a floating 3D Articulated Body.
    This is a strange model, perhaps, since important details are omitted.
    The idea is that the controller can provide the estimator with an estimate
    of the articulated body inertia of the robot expressed in base frame,
    and that it can also provide an estimate of the foot location and 
    reaction force in that frame. We will also ask it for in-frame time
    derivatives of the articulated body inertia and foot position. We will
    not need derivatives or time varying transforms for the leds and imu
    frames, because we know those to be fixed to the base link (torso). 
    Using a rigid body inertia also avoids explicit calculation of the center
    of mass. We can observe both the case where the rigid body inertia is
    properly estimated, and when it is estimated with a bias. If our estimator
    can estimate that bias, and it can estimate it quickly, then we can reduce
    error due to the model's inaccuracy, and perhaps even use the signal to 
    adjust the model parameters. To simulate such a model we will need some way
    to obtain a rigid body inertia which approximates that of the real robot.
    However, that can be done in a different file. In this file we will simply
    define how the rigid body inertia changes with time, and analytically
    differentiate that expression, which will likely be a sum of cosines. 
    This model also requires a time varying expression for the net reaction 
    wrench. One interesting aspect of this model is that there is the potential
    for the wrench, the second derivative of foot position, and the inertia of
    the robot to violate the constraint that the foot is stationary in the 
    world frame. It could also be made to violate the constraint that the net
    wrench evaluate to a force at the foot. To handle this issue, we will 
    specify fewer things, and automatically generate others. We will fix all
    aspects of the inertia and 3d reaction force before hand. Since we know
    the footstep location, we can convert the 3d reaction into a wrench. 
    Since we can use this wrench and the inertia to find the rate of change 
    of the body reference frame, we can find the leg length. Thus the critical 
    time varying inputs are the rigid body inertia of the system, and the 
    ground reaction force. Step one is to use a constant value.
    """

    def __init__(self, inertia_provider=None, force_provider=None,
                 x0=(0.0, 0.0, 0.0,   0.0, 0.0, 0.0,  1.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0), max_events=1):
        self.state = 0.0, x0
        self.normalize()
        self.W, self.B, self.I, self.C = hf.world, hf.body, hf.imu, hf.camera

        self.IMU_T = hf.imu_pose
        self.gyro_noise_models = [
            Hume_IMU_Gyro_Noise_Model(init_filt=0.02),
            Hume_IMU_Gyro_Noise_Model(init_drift=0.0),
            Hume_IMU_Gyro_Noise_Model()]
        self.acc_noise_models = [
            Hume_IMU_Accelerometer_Noise_Model(),
            Hume_IMU_Accelerometer_Noise_Model(),
            Hume_IMU_Accelerometer_Noise_Model()]
        self.max_events = max_events
        self.events = []
        self.inertia_provider = inertia_provider
        self.force_provider = force_provider
        self.foot_pos = np.array([[0.0, 0.0, 0.0]]).T
        self.reset()
        # 'x','y','z','vx','vy','vz','qw','qx','qy','qz','wx','wy','wz'

    @property
    def p(self):
        return np.array([[self.x, self.y, self.z]]).T

    @property
    def pd(self):
        return self.v.eval(self.p)

    @property
    def q(self):
        return Quaternion(self.qw, self.qx, self.qy, self.qz)

    @property
    def w(self):
        return np.array([[self.wx, self.wy, self.wz]]).T

    def normalize(self):
        # print "previous q norm=", self.q.norm
        qnew = self.q.normalize()
        self.state = self.state[0], (
            self.x, self.y, self.z,
            self.vx, self.vy, self.vz,
            qnew.w, qnew.x, qnew.y, qnew.z,
            self.wx, self.wy, self.wz
        )
        # print "new q norm=", self.q.norm

    @property
    def T(self):
        return Transform(self.W, self.B, vec=self.p, quat=self.q)

    @property
    def v(self):
        return ThinSpatialVector.new_motion(self.W, np.array([[self.vx, self.vy, self.vz, self.wx, self.wy, self.wz]]).T)

    @property
    def a(self):
        u = self()
        return ThinSpatialVector.new_motion(self.W, np.array([[u[3], u[4], u[5], u[10], u[11], u[12]]]).T)

    def reset(self):
        """
        Prepares the system for integration by finding the dstate
        based on the state. Resets the number of events. 
        Part of hybrid systems add-on convention.
        """
        pass

    def handle_events(self, stepper, events):
        """ 
        Handles the discrete state change of the system, based
        on which event was triggered. Returns true to 
        terminate the integration when self.max_events have
        occured.
        Part of hybrid systems add-on convention.
        """
        event = events[0]  # assume 1 event
        if len(events) > 1:
            print "Bonus event ignored!", events[1]
        for case in lu.switch(event):
            if case():
                print "confusion!"
        self.num_events += 1
        if self.num_events >= self.max_events:
            return True  # terminate loop
        return False  # continue

    def measureLEDs(self):
        """ noisy measurement of led positions """
        trues = self.T.forward_transform_fat_point_matrix(new_const)
        rotated_trues = hf.camera_pose.reverse_transform_fat_point_matrix(
            trues)
        noise = np.random.randn(3, 7) * 0.005
        scaled_output = 1000.0 * (rotated_trues + noise)
        return scaled_output

    def measureGyros(self):
        """ noisy measurement of angular velocity """
        omega = self.w  # world
        omega = self.T.reverseDCM.dot(omega)  # body
        omega = self.IMU_T.reverseDCM.dot(omega)  # imu frame
        omega_hat = np.zeros((3, 1))
        for i in range(0, 3):
            omega_hat[i, 0] = self.gyro_noise_models[i](omega[i, 0])
        return omega_hat

    def measureAccelerometers(self):
        """ noisy measurement of acceleration """
        v = self.v
        a = self.a
        imu_pose = self.T * self.IMU_T
        p = imu_pose.vec

        rdd = evaluate_acceleration(a, v, p)

        rdd += np.array([[0.0, 0.0, 9.8]]).T
        imu_rdd = imu_pose.reverseDCM.dot(rdd)

        accel_hat = np.zeros((3, 1))
        for i in range(0, 3):
            accel_hat[i, 0] = (
                1.0 / 9.8) * self.acc_noise_models[i](imu_rdd[i, 0])
        return accel_hat

    def measureIMU(self):
        return np.bmat([[self.measureGyros()], [self.measureAccelerometers()]])

    def call_with_debugging(self):
        """
        State time derivative is returned based on the discrete
        state of the system. 
        To calculate the derivative, first calculate the net reaction wrench
        this is easiest in world frame. Then, set the derivative of momentum 
        equal to the net reaction wrench. This should include the following terms:

        f = d/dt L = d/dt[I v] = d/dt I v + I d/dt v

        0= ...
                - ^W f ...
                + d/dt [^W I _W] * ^W v ...
                + ^W I _W * ^W a 

        so we can write

        ^W a = ...
                [^W I _W].inv * ^W f ... 
                - [^W I _W].inv * d/dt [^W I _W] * ^W v

        This differs from the equations in body frame, 
        as the world frame has no rotational velocity.

        # We have an opportunity to check our math between world and body frame
        """
        # define mathematical symbols
        W, B = self.W, self.B

        X = self.T.m_trans
        X_inverse = self.T.m_inv
        X_dual = self.T.f_trans
        X_dual_inverse = self.T.f_inv

        v = self.v

        # calculate foot wrench in world frame
        force_at_foot = self.force_provider.get_value(self.t)
        force_in_world = FrameVector(W, force_at_foot)
        foot = FramePoint(W, self.foot_pos)
        foot_wrench = force_from_point_force(foot, force_in_world)

        # calculate inertia
        inertia_in_body_frame = ThinSpatialMapping.new_inertia(
            B, self.inertia_provider.get_value(self.t))
        inertia = X_dual * inertia_in_body_frame * X_inverse

        # compute rigid body acceleration field
        gravity = ThinSpatialVector.new_motion(
            W, np.array([[0.0, 0.0, -9.8, 0.0, 0.0, 0.0]]).T)

        force = foot_wrench - v.f_cross() * inertia * v

        a = (~inertia) * force + gravity

        # compute derivative of the transform's vector component
        r = self.T.vec
        rd = v.eval(r)

        # compute derivative of the transform's quaternion component
        qd = self.q.stable_deriv(v.rx, v.ry, v.rz, 1.0)

        # save some debugging information
        debug_datum = namedtuple("DebuggingDatum",
                                 [
                                     "accel",
                                     "reaction_wrench"
                                 ])(
            a,
            foot_wrench
        )

        return (rd[0, 0], rd[1, 0], rd[2, 0],  a.x, a.y, a.z,  qd.w, qd.x, qd.y, qd.z,  a.rx, a.ry, a.rz), debug_datum

    def __call__(self):
        update, debug_datum = self.call_with_debugging()
        return update


def main():
    start_time = time.time()
    m = 20.0  # kg
    g = 9.8
    Fbb_rigid_body_inertia = ConstantQuantity(
        m * np.eye(6),
        np.zeros((6, 6)))
    Fw_reaction_force = ConstantQuantity(
        np.array([[0.0, 0.0, m * g]]).T,
        np.array([[0.0, 0.0, 0.0]]).T)
    system = HybridHume(Fbb_rigid_body_inertia, Fw_reaction_force,
                        x0=(-0.3, 0.0, 0.9, 0.4, 0.0, 0.0, 0.90, 0.0, -0.22, 0.0,   0.0, 0.0, 0.0), max_events=20)

    system.foot_pos = np.array([[0.0, 0.0, 0.0]]).T

    logger = ly.Recorder(system)
    # step thrice
    stepper = ly.dormand_prince(system, np.arange(0.0, 0.2, 0.001))
    imu_dat = []
    led_dat = []
    acc_dat = []
    data_ts = []
    for t, events in stepper:
        assert system.t == t
        try:
            ly.check_NaN(system)
        except ArithmeticError:
            break
        logger.log(events)
        if events:
            if system.handle_events(stepper, events):
                break
        else:
            """ Most common case: no event, sample point. """
            imu_dat.append(system.measureGyros())
            acc_dat.append(system.measureAccelerometers())
            led_dat.append(system.measureLEDs())
            data_ts.append(t)
            pass

    print dir(logger)
    print len(logger.x)
    print logger.x[0]
    fig = plt.figure()
    # ax1=fig.add_subplot(331)
    # ax2=fig.add_subplot(332)
    # ax1.plot(logger.t,[(x[8]) for x in logger.x])
    fig.add_subplot(331).plot(logger.t, [x[0] for x in logger.x])
    fig.add_subplot(332).plot(logger.t, [x[1] for x in logger.x])
    fig.add_subplot(333).plot(logger.t, [x[2] for x in logger.x])
    fig.add_subplot(334).plot(data_ts, [w[0, 0] for w in imu_dat])
    fig.add_subplot(335).plot(data_ts, [w[1, 0] for w in imu_dat])
    fig.add_subplot(336).plot(data_ts, [w[2, 0] for w in imu_dat])
    fig.add_subplot(337).plot(data_ts, [a[0, 0] for a in acc_dat])
    fig.add_subplot(338).plot(data_ts, [a[1, 0] for a in acc_dat])
    fig.add_subplot(339).plot(data_ts, [a[2, 0] for a in acc_dat])
    fig.tight_layout()
    end_time = time.time()
    print "Elapsed computation time:", end_time - start_time
    plt.show()

if __name__ == "__main__":
    main()
