from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation
from leds import new_const
from hume_frames_and_poses import *
from hume_sim import HumeSim
from hybrid_hume import HybridHume
import attitudeEstimator.attitude_estimation_cpp as ae_cpp
from gu.math.transforms import Transform
import random as rand 
from allen_variance import StraightNoiseErrorModel
from animation_3d_graphics import *
from animation_live_3d_graphics import *

plt.rcParams['animation.ffmpeg_path']= '/usr/bin/ffmpeg'

live=False
sim=True
class HumeAnimation:
    def __init__(self):
        self.generate_figure()        
        self.generate_plot_objects()
        self.sim=HumeSim()

    def generate_figure(self):
        s=0.0125
        self._figure = plt.figure(figsize=(s*1920,s*1080))
        self._ax = self._figure.add_subplot(
            111, autoscale_on=False, projection='3d', xlim=(-1, 1), ylim=(-1, 1), zlim=(-0,2))
        self._ax.grid()

    def generate_plot_objects(self):
        self.graphic_objects=[]
        if live:
            self.receiver=ae_cpp.wrapped_matrix_receiver()
            self.imu=ae_cpp.imu_receiver()
            self.kfhandler=LiveKFandLEDhandler(self.receiver,self.imu)
            self.graphic_objects.append(LiveGraphicLEDs(self.kfhandler))
            self.graphic_objects.append(FrameDisplayKf(self.kfhandler))
            self.graphic_objects.append(IMUPlotter(self.kfhandler))
            self.receiver.start()
            self.imu.start()
        if sim:
            self.graphic_objects.append(TimeDisplay())
            self.graphic_objects.append(SimLEDdiff())
            self.graphic_objects.append(FrameDisplay())
            # self.graphic_objects.append(KfFrameDisplay())
            self.graphic_objects.append(FrameMotionDisplay())
            self.graphic_objects.append(FullStateDisplay())
            self.graphic_objects.append(KFCovarianceDisplay())
            # self.graphic_objects.append(KfMotionDisplay())
            self.graphic_objects.append(SimIMUPlotter())

        
        self._ax.set_xlabel("x")
        self._ax.set_ylabel('y')
        self._ax.set_zlabel('z')

    def get_figure(self):
        return self._figure

    def simulate(self,**kwargs):
        if sim:
            self.sim.simulate(**kwargs)
        # CALL TO SIMULATION PROGRAM
        # PREPROCESSING FOR ANIMATION

        
    def init_all(self):
        """ Iterate through init callbacks """
        return tuple(
            [ret for rets in [ob.init(self._ax) for ob in self.graphic_objects] for ret in rets]
            )

    def anim_all(self,i):
        """ Iterate through anim callbacks """
        return tuple(
            [ret for rets in [ob.anim(self.sim,i) for ob in self.graphic_objects] for ret in rets]
            )

    def begin_animation(self,skip=25):
        self._ani = animation.FuncAnimation(
            self._figure, self.anim_all, np.arange(1, 100 if not sim else len(self.sim.led_dat),skip), # realtime
            # self._figure, self.anim_all, np.arange(1, 100 if not sim else len(self.sim.led_dat),1), # 25 x slowmo
            interval=25, blit=False, init_func=self.init_all)
        # Writer = animation.writers['ffmpeg']
        # writer = Writer(fps=30, metadata=dict(artist='Me'), bitrate=8000)
        # self._ani.save('double_pendulum.mp4', writer=writer)
        

def main():
    sim=HumeAnimation()

    sim.simulate(tf=2.2, dt=0.005)
    print "done simulating"
    sim.begin_animation(skip=5)
    plt.show()

if __name__=="__main__":
    main()