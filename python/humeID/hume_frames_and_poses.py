from gu.math import *

world=Frame('world')
camera=Frame('camera')
body=Frame('body')
imu=Frame('imu')
# From HumePoseKalmanFilter.cpp
# imuPose.setOrientation(0, -1, 0, -1, 0, 0, 0, 0, -1);
# imuPose.setPosition(.020, .050, 0.100);
imu_pose = Transform(body,imu,vec=np.array([[0.0,2.75,16.0]]).T*0.0254,
            quat=Quaternion(0.0,sqrt(2)/2,-sqrt(2)/2,0.0))
# convert from camera frame to our more convenient world frame. Also adjusts units.
led_cameraT=np.array([[0,0,-0.001],[-0.001,0,0],[0,0.001,0]])
camera_pose = Transform(world, camera, vec=np.array([[0.0,0.0,0.0]]).T, quat=Quaternion(1.0,1.0,-1.0,-1.0))