'''
Created on Jan 15, 2014

@author: Gray Thomas
'''
import numpy as np
import matplotlib.pyplot as plt
import math
from networkUtils.dyNetUtils.dyNetYamlReader import *
from humeID.HumeDataFolder import HumeDataFolder
from humeID.HumeDataFolderV2 import HumeDataFolderV2
from humeID import GRAY_UTILS
# from humeID import MEKA
from yaml import load, dump
import io

    
def standardFileNamesDict(numbersList):
    dict = {}
    for i in numbersList:
        dict[i] = "save_file_" + str(i)
    return dict

class HumeDataFetcher(object):

    def __init__(self):
        self.baseDir = GRAY_UTILS+"/../Hume_Basic_Data/"
        self.experiments = {1:("20140115_one_step_IROS/", standardFileNamesDict([2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13])),
                          2:("20140117_RSS_onestep_regression/", standardFileNamesDict([1, 2, 3, 4, 5, 6, 7])),
                          "hume_experiment_data":("../hume_experiment_data", {"master":""})}
#         self.baseFile="/home/hcrl/wk/whole_body_controller_hume/data_analysis/20140115_one_step_IROS/"
        self.folders = {2:"save_file_2", 3:"save_file_3", 4:"save_file_4", 5:"save_file_5", 6:"save_file_6",
                      7:"save_file_7", 8:"save_file_8", 9:"save_file_9", 10:"save_file_10", 11:"save_file_11",
                      12:"save_file_12", 13:"save_file_13"}
        self.dataFiles = {}
    def get_most_recent_data(self):
        return HumeDataFolderV2(GRAY_UTILS+"/../hume_experiment_data")
    def getDataSet(self, number):
        return self.getDataSetExp(1, number)
    def getDataSetExp(self, experiment, number):
        (main, subs) = self.experiments[experiment]
        return HumeDataFolder(self.baseDir + main + subs[number])
    



def plotConvergenceTest():
    dat = DyNetPlotterYamlData(GRAY_UTILS + "/DyNetUtils/src/test/data/" + 
                         "testConvergence_Noise_30_A_0.85_B_0.25_C_-2_D_-0.1_DataSize_120_seed_1342.yaml")
    plt.figure()
    plt.hold(True)
    plt.plot(dat.epochData[5].measurements_predicted, '#360022', linewidth=2)
    plt.plot(dat.epochData[4].measurements_predicted, '#451d00', linewidth=2)
    plt.plot(dat.epochData[3].measurements_predicted, '#430005', linewidth=2)
    plt.plot(dat.epochData[2].measurements_predicted, '#b40071', linewidth=2)
    plt.plot(dat.epochData[1].measurements_predicted, '#e76200', linewidth=2)
    plt.plot(dat.epochData[0].measurements_predicted, '#e10010', linewidth=2)
    plt.plot(dat.measurements, 'k.', linewidth=2)
    
def fuzzyDelay(vector, t0, tf, delay):
    low = math.floor(delay)
    high = math.ceil(delay)
    if (low == high):
        return vector[t0 + low:tf + low]
    lowScaling = abs(delay - high)
    highScaling = abs(delay - low)
    lowVector = vector[t0 + low:tf + low]
    highVector = vector[t0 + high:tf + high]
    result = highScaling * highVector + lowScaling * lowVector
    return result
    
def forceVertXPredictor(data, t0, tf, force0, d1, d2, s1, s2, bl, z1, z2):
    delayQEI = fuzzyDelay(data[:, 1] - z1, t0, tf, d1) * s1
    delayJ = fuzzyDelay(data[:, 2] - z2, t0, tf, d2) * s2
    blVec = [(bl if (v > 0) else -bl) for v in data[t0:tf, 0]]
    return delayJ - delayQEI + force0 + blVec

def saveTrainingSet(fileName, npArrayInputs, npArrayOutputs, folder=GRAY_UTILS + "/Demos/data/",):
    numInputs = npArrayInputs.shape[1]
    numOutputs = npArrayOutputs.shape[1]
    T = np.concatenate((npArrayInputs, npArrayOutputs), axis=1)
    np.savetxt(folder + fileName, T, header="%d %d" % (numInputs, numOutputs), comments="")
# from linearUtils.convolution import setupConvolutionFilters



def estimateSensors(data, axs, to, tf, nConv, convolutionKernel, convolvedWeighting): 
    cv = lambda(x):(np.convolve(x, convolutionKernel, mode='same')[nConv:-nConv]).reshape((-1,1))   

    R = np.concatenate((np.ones((tf - to, 1)),
                    cv(data[(-nConv + to - 1):(tf - 1 + nConv), 1]),
                    cv(data[(to - nConv):(tf + nConv), 1]),
                    cv(data[(to - nConv):(tf + nConv), 2]),
                    cv(np.multiply(data[(to - nConv):(tf + nConv), 2], data[(to - nConv):(tf + nConv), 1])),
                    cv(np.multiply(data[(to - nConv):(tf + nConv), 2], data[(to - nConv):(tf + nConv), 2]))
                      ), axis=1)
    # two models, one for joint angles less than 5000, and the other for angles > 5000
    # Note that 5000 is the bottom end of the range.
    Rnormal=np.array(R)
    Rextreme=np.zeros(R.shape)
    for (j,i) in enumerate(range(to,tf)):
        if (data[i,2]<5000):
            zeros=Rextreme[j,:]
            Rextreme[j,:]=Rnormal[j,:]
            Rnormal[j,:]=zeros
    R = np.concatenate((Rnormal,Rextreme),axis=1)
        
    
#     convolvedWeighting=np.multiply(convolvedWeighting,extremeJointAngles.reshape((-1,)))# don't do this.
    koConvR = np.array(R)
    for col in range(0, R.shape[1]):
        koConvR[:, col] = np.multiply(R[:, col], convolvedWeighting)
    weightedData = np.multiply(data[to:tf, [3]], convolvedWeighting.reshape((-1, 1)))
    weightedData = np.convolve(weightedData.reshape(-1,), convolutionKernel, mode='same').reshape(-1, 1)
    sol = np.linalg.solve(R.T.dot(koConvR), R.T.dot(weightedData))
    expectation = R.dot(sol)
    
    
    maxSpringEncoder = max(data[to:tf, 3]) * 1.0
    minSpringEncoder = min(data[to:tf, 3]) * 1.0

    
    convolvedData3 = np.convolve(data[(to - nConv):(tf + nConv), 3], convolutionKernel, mode='same')[nConv:-nConv]
    plt.setp(axs[3, 0].plot(convolvedData3, "k-"), linewidth=1.0)
    plt.setp(axs[3, 0].plot(R.dot(sol), "m"), linewidth=1.0)
    plt.setp(axs[3, 0].plot(R.dot(sol), "c"), linewidth=1.0)
    plt.setp(axs[3, 0].plot(data[to:tf, 3], "b-"), linewidth=2.0)
    axs[3, 0].set_ylabel("Spring\nEncoder\nInteger\nValue", rotation="horizontal")
    rawError = (R.dot(sol) - data[to:tf, [3]])
    smoothError = R.dot(sol) - convolvedData3.reshape((-1, 1))
    rawError = np.multiply(rawError, convolvedWeighting.reshape((-1, 1)))
    smoothError = np.multiply(smoothError, convolvedWeighting.reshape((-1, 1)))
    errorVariance = rawError.T.dot(rawError) * 1.0 / (rawError.shape[0] - 1.0)
    
    print "spring normal: const: %f, m z^-1: %f, m: %f+%f*j, j: %f+%f*j" % (
                sol[0],sol[1],sol[2],sol[4],sol[3],sol[5])
    print "spring extreme: const: %f, m z^-1: %f, m: %f+%f*j, j: %f+%f*j" % (
                sol[6],sol[7],sol[8],sol[9],sol[10],sol[11])
    print errorVariance
    plt.setp(axs[3, 1].plot(rawError * 100 / (maxSpringEncoder - minSpringEncoder), "b"), linewidth=2.0)
    plt.setp(axs[3, 1].plot(smoothError * 100 / (maxSpringEncoder - minSpringEncoder), "r"), linewidth=2.0)
    axs[3, 1].yaxis.set_major_formatter(plt.FormatStrFormatter('%.1f%%'))
    return sol


    






def generateLinearPlantModel(alphas, betas, gammas, zeros):
    A=np.zeros((4,4))
    B=np.zeros((4,1))
    C=np.zeros((3,4))
    D=np.zeros((3,1))
    dt=1 #due to choice of units
    A[0,1]=1.0; #motor position integration
    A[2,3]=1.0; #joint position integration
    temp1=alphas[1]+alphas[4]*zeros[1]+alphas[2]
    temp2=alphas[3]+2*alphas[4]*zeros[0]+2*alphas[5]*zeros[1]
    A[1,0]=betas[2]*(temp1)
    A[1,1]=betas[4]-betas[2]*alphas[1]*dt
    A[1,2]=betas[2]*(temp2)
    A[1,3]=0
    B[1,0]=betas[1]
    A[3,0]=gammas[1]*(temp1)
    A[3,1]=gammas[1]*(-dt*alphas[1])
    A[3,2]=gammas[2]+gammas[1]*(temp2)
    A[3,3]=gammas[3]
    C[0,0]=1.0
    C[1,2]=1.0
    C[2,0]=temp1
    C[2,1]=-dt*alphas[1]
    C[2,2]=temp2
    return A,B,C,D


        
