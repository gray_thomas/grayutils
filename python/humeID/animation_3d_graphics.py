from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation
from leds import new_const
from hume_frames_and_poses import *
from hume_sim import HumeSim
from hybrid_hume import HybridHume
from hume_kf import HumeState, HumeStateDelta
import attitudeEstimator.attitude_estimation_cpp as ae_cpp
from gu.math.transforms import Transform
import random as rand 
from allen_variance import StraightNoiseErrorModel
from math import sqrt

class GraphicLEDs(object):
    def __init__(self,zoffset=0.0):
        self.dots=None
        self.zoffset=zoffset

    def anim(self,sim,i):
        leds=sim.led_dat[i]
        self.dots.set_data(leds[0,:],leds[1,:])
        self.dots.set_3d_properties(leds[2,:]+np.ones((leds.shape[1],))*self.zoffset)
        return [self.dots]

    def init(self,ax):
        if self.dots==None:
            self.dots, = ax.plot([], [], 'mo', zs=[])
        self.dots.set_data(new_const[0,:],new_const[1,:])
        self.dots.set_3d_properties(new_const[2,:]+200.0*np.ones((7,)))
        return [self.dots]

class baseGraphicVector(object):
    def __init__(self):
        self.line=None
    def init_line(self,ax,c1):
        if self.line==None:
            self.line,=ax.plot([],[],c1,zs=[])
        return [self.line]
    def anim_vec(self,p0,pv):
        pf=p0+pv
        self.line.set_data([p0[0,0],pf[0,0]],[p0[1,0],pf[1,0]])
        self.line.set_3d_properties([p0[2,0],pf[2,0]])
        return [self.line]
    def anim_line(self,p0,pf):
        self.line.set_data([p0[0,0],pf[0,0]],[p0[1,0],pf[1,0]])
        self.line.set_3d_properties([p0[2,0],pf[2,0]])
        return [self.line]


class BaseGraphicLEDDiff(object):
    def __init__(self):
        self.dots1=None

    def init_lines(self,ax,c1,c2):
        if self.dots1==None:
            self.dots1, = ax.plot([], [], c1, zs=[])
            self.dots2, = ax.plot([], [], c2, zs=[])
            self.lines=[]
            for i in range(0,7):
                self.lines.append([
                    ax.plot([],[],'r',zs=[])[0],
                    ax.plot([],[],'g',zs=[])[0],
                    ax.plot([],[],'b',zs=[])[0]
                    ])# 21 tiny error lines.

        self.dots1.set_data(new_const[0,:],new_const[1,:])
        self.dots1.set_3d_properties(new_const[2,:]+200.0*np.ones((7,)))
        self.dots2.set_data(new_const[0,:],new_const[1,:])
        self.dots2.set_3d_properties(new_const[2,:]+200.0*np.ones((7,)))
        return [self.dots1,self.dots2]+[line for lines in self.lines for line in lines]

    def anim_diff(self,leds1,leds2):
        self.dots1.set_data(leds1[0,:],leds1[1,:])
        self.dots1.set_3d_properties(leds1[2,:])
        self.dots2.set_data(leds2[0,:],leds2[1,:])
        self.dots2.set_3d_properties(leds2[2,:])
        for i in range(0,7):
            p0=leds1[:,i]
            pF=leds2[:,i]
            self.lines[i][0].set_data([p0[0],pF[0]],[p0[1],p0[1]])
            self.lines[i][0].set_3d_properties([p0[2],p0[2]])
            self.lines[i][1].set_data([pF[0],pF[0]],[p0[1],pF[1]])
            self.lines[i][1].set_3d_properties([p0[2],p0[2]])
            self.lines[i][2].set_data([pF[0],pF[0]],[pF[1],pF[1]])
            self.lines[i][2].set_3d_properties([p0[2],pF[2]])
        return [self.dots1,self.dots2]+[line for lines in self.lines for line in lines]

class SimLEDdiff(BaseGraphicLEDDiff):
    def __init__(self):
        super(SimLEDdiff,self).__init__()
        print self.dots1
        
    def init(self,ax):
        return self.init_lines(ax,'mo','cx') 

    def anim(self,sim,i):
        leds1=0.001*camera_pose.forward_transform_fat_point_matrix(sim.led_dat[i])
        leds2=sim.predicted_leds[i]
        return self.anim_diff(leds1,leds2)


class TimeDisplay(object):
    def __init__(self, format_string=None):
        if format_string==None:
            format_string='time = %.3fs'
        self.format_string=format_string
        self.time_text=None
        
    def anim(self,sim,i):
        self.time_text.set_text(self.format_string%(sim.data_ts[i]))

        return [self.time_text]
    def init(self,ax):
        if self.time_text==None:
            self.time_text = ax.text(0.9, 0.5,2.5, '')
        return [self.time_text]

class BaseFrameDisplay(object):
    def __init__(self,scale=0.2, colors=('r','g','b'), thk=3):
        self.lines=None
        self.scale=scale
        self.colors=colors
        self.thk=thk

    def init(self,ax):
        if self.lines==None:
            self.lines=[ax.plot([0.0,0.1],[0.0,0.0],self.colors[0]
                , linewidth=self.thk, zs=[200.0,200.0])[0],
                        ax.plot([0.0,0.0],[0.0,0.1],self.colors[1]
                , linewidth=self.thk, zs=[200.0,200.0])[0],
                        ax.plot([0.0,0.0],[0.0,0.0],self.colors[2]
                , linewidth=self.thk, zs=[200.0,200.1])[0]]
        return self.lines

    def anim_frame(self,T):
        pts=T.forward_transform_fat_point_matrix(
            np.concatenate([np.zeros((3,1)),np.eye(3,3)*self.scale],axis=1))
        for i in range(0,3):
            self.lines[i].set_data(pts[0,[0,i+1]],pts[1,[0,i+1]])
            self.lines[i].set_3d_properties(pts[2,[0,i+1]])
        return self.lines

class BaseFrameMotionDisplay(object):
    def __init__(self, scale=0.2, dt=0.1, color='k', thk=1):
        self.lines=None
        self.scale=scale
        self.dt=dt
        self.thk=thk
        self.col=color
    def init(self,ax):
        if self.lines==None:
            self.lines=[ax.plot([0.0,0.1],[0.0,0.0], 
                self.col, linewidth=self.thk, zs=[200.0,200.0])[0],
                        ax.plot([0.0,0.0],[0.0,0.1], 
                self.col, linewidth=self.thk, zs=[200.0,200.0])[0],
                        ax.plot([0.0,0.0],[0.0,0.0], 
                self.col, linewidth=self.thk, zs=[200.0,200.1])[0],
                        ax.plot([0.0,0.0],[0.0,0.0], 
                self.col, linewidth=self.thk, zs=[200.0,200.1])[0]]
        return self.lines
    def anim_motion(self, transform, motion):
        pts=transform.forward_transform_fat_point_matrix(
            np.concatenate([np.zeros((3,1)),np.eye(3,3)*self.scale],axis=1))
        pts2=transform.move(motion*self.dt).forward_transform_fat_point_matrix(
            np.concatenate([np.zeros((3,1)),np.eye(3,3)*self.scale],axis=1))
        for i in range(0,4):
            self.lines[i].set_data([pts[0,i],pts2[0,i]],[pts[1,i],pts2[1,i]])
            self.lines[i].set_3d_properties([pts[2,i],pts2[2,i]])
        return self.lines

class FrameMotionDisplay(BaseFrameMotionDisplay):
    def __init__(self):
        super(FrameMotionDisplay,self).__init__()
        self.hume=HybridHume()
    def anim(self,sim,i):
        self.hume.state=sim.logger.t[i], sim.logger.x[i]
        self.hume.normalize()
        return self.anim_motion(self.hume.T, self.hume.v)

class BaseHumeStateDisplay(object):
    def __init__(self):
        self.lines=None
        self.kwargs={'linewidth':0.5,'ms':4.4,'alpha':0.4}
        self.color="--.k"
        self.dt=0.2
        self.scale=0.3
    def init(self,ax):
        if self.lines==None:
            self.lines=[ax.plot([0.0,0.1],[0.0,0.0], 
                self.color, zs=[200.0,200.0], **self.kwargs)[0],
                        ax.plot([0.0,0.0],[0.0,0.1], 
                self.color, zs=[200.0,200.0], **self.kwargs)[0]]
        return self.lines
    def anim_hume_state(self, hume_state):
        pts=hume_state.T.forward_transform_fat_point_matrix(
            np.concatenate([np.zeros((3,1)),np.eye(3,3)*self.scale],axis=1))
        pts2=hume_state.T.move(hume_state.v*self.dt).forward_transform_fat_point_matrix(
            np.concatenate([np.zeros((3,1)),np.eye(3,3)*self.scale],axis=1))
        line1pts=np.concatenate([pts2[:,[1]],pts[:,[1]],pts[:,[0]],pts[:,[2]],pts2[:,[2]]],axis=1)
        self.lines[0].set_data(line1pts[0,:],line1pts[1,:])
        self.lines[0].set_3d_properties(line1pts[2,:])
        line2pts=np.concatenate([pts2[:,[0]],pts[:,[0]],pts[:,[3]],pts2[:,[3]]],axis=1)
        self.lines[1].set_data(line2pts[0,:],line2pts[1,:])
        self.lines[1].set_3d_properties(line2pts[2,:])
        return self.lines

class GeometryTesterForSpacialTransformLogarithm(object):
    def __init__(self):
        self.lines=None
        self.kwargs={'linewidth':0.5,'ms':4.4,'alpha':0.4}
        self.color="--.k"
        self.dt=0.2
        self.scale=0.3
        self.frame_display_1=BaseFrameDisplay()
        self.frame_display_2=BaseFrameDisplay()
    def init(self,ax):
        if self.lines==None:
            self.lines=[
                ax.plot([],[], self.color, zs=[], **self.kwargs)[0],
                ax.plot([],[], self.color, zs=[], **self.kwargs)[0],
                ax.plot([],[], self.color, zs=[], **self.kwargs)[0],
                ax.plot([],[], self.color, zs=[], **self.kwargs)[0],
                ]
        return (self.lines+
            self.frame_display_1.init(ax)+
            self.frame_display_2.init(ax))
    def animate_transform_log(self, T1, T2):
        T1*(~T2)
        return (self.lines+
            self.frame_display_1.anim_frame(T1)+
            self.frame_display_2.anim_frame(T2))

    def anim(self,i):
        w=Frame('world')
        b=Frame('body')
        T1=Transform(w,b)
        T2=Transform(w,b)
        return self.animate_transform_log(T1,T2)


class FrameDisplay(BaseFrameDisplay):
    def __init__(self):
        super(FrameDisplay, self).__init__()
        self.hume=HybridHume()
        
    def anim(self,sim,i):
        self.hume.state = sim.logger.t[i], sim.logger.x[i]
        self.hume.normalize()
        return self.anim_frame(self.hume.T)

class FullStateDisplay(BaseHumeStateDisplay):
    def __init__(self):
        super(FullStateDisplay,self).__init__()


    def anim(self,sim,i):
        state=HumeState(*sim.logger.x[i])
        assert abs(state.q.norm-1.0)<1e-7
        return self.anim_hume_state(state)

class KFCovarianceDisplay(object):
    def __init__(self):
        self.state_displays=[BaseHumeStateDisplay() for i in range(0,24)]
    def init(self, ax):
        return [ret for rets in [disp.init(ax) for disp in self.state_displays] for ret in rets ]
    def anim(self, sim , i):
        # state=sim.filter_state[i]
        state=HumeState(*sim.logger.x[i])
        cov=sim.filter_cov[i]
        w , v = np.linalg.eigh( cov )
        rets=[]
        scale_factor=2
        for i in range(0,12):
            delta1=HumeStateDelta.from_vec( v[:,[i]]*scale_factor*sqrt(w[i]))
            rets.extend(self.state_displays[i*2+0].anim_hume_state(state.move(delta1)))
            delta2=HumeStateDelta.from_vec(-v[:,[i]]*scale_factor*sqrt(w[i]))
            rets.extend(self.state_displays[i*2+1].anim_hume_state(state.move(delta2)))
        return rets

class KfFrameDisplay(BaseFrameDisplay):
    def __init__(self):
        super(FrameDisplay, self).__init__()
        self.hume=HybridHume()
        
    def anim(self,sim,i):
        self.hume.state= sim.logger.t[i], sim.logger.x[i]
        self.hume.normalize()
        return self.anim_frame(self.hume.T)

class BaseIMUPlotter(object):
    def __init__(self):
        self.ang=baseGraphicVector()
        self.lin=baseGraphicVector()
        self.g=baseGraphicVector()
        self.tot=baseGraphicVector()

    def init(self,ax):
        return (self.ang.init_line(ax,'r')
            +self.lin.init_line(ax,'b')
            +self.g.init_line(ax,'g')
            +self.tot.init_line(ax,'c'))

    def anim_base(self,ang_vel,lin_acc,imu_pose):
        dcm=imu_pose.forwardDCM
        p0=np.array(imu_pose.vec).reshape((-1,1))
        pa=p0+dcm.dot(ang_vel)
        pl=p0+dcm.dot(lin_acc)
        pg=pl-np.array([[0.0,0.0,1.0]]).T
        return [(self.ang.anim_line(p0,pa) +
                self.lin.anim_line(p0,pl) +
                self.g.anim_line(pl,pg) +
                self.tot.anim_line(p0,pg))]

class SimIMUPlotter(BaseIMUPlotter):
    def __init__(self):
        super(SimIMUPlotter, self).__init__()
        self.hume=HybridHume()
    def anim(self,sim,i):
        self.hume.state = sim.logger.t[i], sim.logger.x[i]
        self.hume.normalize()
        imu_pose=self.hume.T * self.hume.IMU_T
        lin_acc=sim.acc_dat[i]
        ang_vel=sim.imu_dat[i]
        return self.anim_base(ang_vel,lin_acc,imu_pose)

