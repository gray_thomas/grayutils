'''
Created on Jan 16, 2014

@author: hcrl
'''
from humeID.humePlotter import HumeRagdoll, HumePlotter
from humeID.dataFetcher import HumeDataFetcher
from math import atan2
from math import sin,cos,pow
import numpy as np
from numpy import dot
import matplotlib.pyplot as plt


class ComRegressorCalculator(object):

    def setupTimes(self):
        self.time=self.data.getTime()
        i0 = 0
        phases=self.data.getPhase()
        for i in range (0,len(phases)):
            if (phases[i]==2):
                i0=i 
                break
        i1 = len(phases)-1
        for i in range (i0,len(phases)):
            if (phases[i]!=2):
                i1=i 
                break
        self.t=range(i0,i1)
        print self.t
        print len(self.t)
        self.dt=(self.time[i1]-self.time[i0])/len(self.t)
        self.tEx=range(i0-1,i1+1)
    
    def numericallyDifferentiateBodyKinematics(self,bodyPositionExtended,tiltExtended):
        self.bodyPositionVel=np.zeros((len(self.t),2))
        self.bodyPositionAccel=np.zeros((len(self.t),2))
        self.tiltVel=np.zeros((len(self.t),1))
        self.tiltAccel=np.zeros((len(self.t),1))
        for i in range (0,len(self.t)):
            self.bodyPositionVel[i,:]=(bodyPositionExtended[i+2,:]-bodyPositionExtended[i,:])/(2*self.dt)
            self.tiltVel[i,0]=(tiltExtended[i+2,0]-tiltExtended[i,:])/(2*self.dt)
            self.bodyPositionAccel[i,:]=(bodyPositionExtended[i+2,:]-2*bodyPositionExtended[i+1,:]+bodyPositionExtended[i,:])/(self.dt*self.dt)
            self.tiltAccel[i,0]=(tiltExtended[i+2,0]-2*tiltExtended[i+1,0]+tiltExtended[i,0])/(self.dt*self.dt)
        self.tilt=tiltExtended[1:-1,:]
        print len(bodyPositionExtended)
        self.bodyPosition=bodyPositionExtended[1:-1,:]
        print len(self.bodyPosition)
        
    def setupBodyFrameKinematics(self):
        positions=self.data.getMeasuredJoints()
        bodyPositionExtended=np.zeros((len(self.tEx),2))
        tiltExtended=np.zeros((len(self.tEx),1))
        for (iEx,tEx) in enumerate(self.tEx):
            self.ragdoll.solveSingleContactKinematics(positions[tEx,1], positions[tEx,2], 
                                                      positions[tEx,4], positions[tEx,5], positions[tEx,9])
            bodyPositionExtended[iEx,:]=self.ragdoll.hipPoint.T
            torsoVector=self.ragdoll.torsoPoint-self.ragdoll.hipPoint
            tiltExtended[iEx,0]=atan2(torsoVector[1],torsoVector[0])
        
        self.numericallyDifferentiateBodyKinematics(bodyPositionExtended,tiltExtended)
        
    def generateRotationMatriciesAndDerivatives(self):
        self.Rx=np.zeros((len(self.t),2))
        self.dRx=np.zeros((len(self.t),2))
        self.ddRx=np.zeros((len(self.t),2))
        self.Rz=np.zeros((len(self.t),2))
        self.dRz=np.zeros((len(self.t),2))
        self.ddRz=np.zeros((len(self.t),2))
        for i in range (0,len(self.t)):
            th=self.tilt[i]
            dth=self.tiltVel[i]
            ddth=self.tiltAccel[i]
            self.Rx[i,:]=[cos(th),-sin(th)]
            self.Rz[i,:]=[sin(th),cos(th)]
            self.dRx[i,:]=[-sin(th)*dth,-cos(th)*dth]
            self.dRz[i,:]=[cos(th)*dth,-sin(th)*dth]
            self.ddRx[i,:]=[-sin(th)*ddth+-cos(th)*dth*dth, -cos(th)*ddth+sin(th)*dth*dth]
            self.ddRz[i,:]=[cos(th)*ddth-sin(th)*dth*dth, -sin(th)*ddth+-cos(th)*dth*dth]
            
# return ddTheta*np.array([[-sin(theta),-cos(theta)],[cos(theta),-sin(theta)]]) - pow(dTheta,2) * self.getR(theta)

    def __init__(self,data):
        self.ragdoll=HumeRagdoll()
        self.data=data
        self.setupTimes()
        self.setupBodyFrameKinematics()
        self.generateRotationMatriciesAndDerivatives()
        
    def generateLSProblem(self,phi0,sigmaXdd,sigmaZ,sigmaPhi,ts=[]):
        K=phi0[0,0]
        Xc=phi0[1,0]
        Yc=phi0[2,0]
#         Z0=phi0[3,0]
        if (len(ts)==0):
            ts=range(0,len(self.t))
        denominator=np.diag(np.array([pow(sigma,-2) for sigma in sigmaPhi]))
        numerator=np.zeros((4,1))
        Cb=np.array([[Xc],[Yc]])
        predictionXdd=np.zeros((len(ts),1))
        predictionZ=np.zeros((len(ts),1))
        error=0
        for (i,t) in enumerate(ts):
            Bx=self.bodyPosition[t,0]
            Bz=self.bodyPosition[t,1]
            ddBx=self.bodyPositionAccel[t,0]
            Rx=self.Rx[t,:]
            Rz=self.Rz[t,:]
            ddRx=self.ddRx[t,:]
            subRegressor=np.zeros((2,4))

            subRegressor[0,0]=Bx+dot(Rx,Cb)
            subRegressor[0,1]=K*Rx[0]-ddRx[0]
            subRegressor[0,2]=K*Rx[1]-ddRx[1]
            subRegressor[0,3]=0
            subRegressor[1,0]=0
            subRegressor[1,1]=-Rz[0]
            subRegressor[1,2]=-Rz[1]
            subRegressor[1,3]=1
            subOutput=np.zeros((2,1))
            subOutput[0,0]=ddBx
            subOutput[1,0]=Bz
            scalingMatrix=np.zeros((2,2))
            scalingMatrix[0,0]=pow(sigmaXdd,-2)
            scalingMatrix[1,1]=pow(sigmaZ,-2)
            scaledSubRegressorTranspose=dot(subRegressor.transpose(),scalingMatrix)
            denominator+=dot(scaledSubRegressorTranspose,subRegressor)
            prediction=dot(subRegressor,phi0)
            errors=subOutput-prediction
            numerator+=dot(scaledSubRegressorTranspose,errors)
            error+=dot(dot(errors.transpose(),scalingMatrix),errors)
            predictionXdd[i]=prediction[0]
            predictionZ[i]=prediction[1]
            
        return (numerator,denominator,error,predictionXdd,predictionZ)

if __name__ == '__main__':
    experimentNum=5
    dataSet=HumeDataFetcher().getDataSet(experimentNum)
    regressor=ComRegressorCalculator(dataSet)
    plotter = HumePlotter(dataSet)
#     phi0=np.array([[-58.11569789],[-4.65661338],[-0.22734314],[-3.94211381]])
#     sigmaPhi=np.array([1.1,1.1,1.1,1.1]).transpose()
    phi0=np.array([[10*9.8/0.78*0.90],[0.1],[-0.04],[0.818]])
    sigmaPhi=np.array([0.02,0.01,0.01,0.1]).transpose()
    sigmaXdd=0.5 # m/s/s
    sigmaZ=5.0 # m
#     ts=range(10,30)
    for i in range(0,1):
        (n,d,e,predictionXdd,predictionZ)= regressor.generateLSProblem(phi0,sigmaXdd,sigmaZ,sigmaPhi)
        print d
        deltas=np.linalg.solve(d, n)
        print "previous error equals: "+str(e)
        print "phi0 is :"
        print phi0
#         phi0[0]+=deltas[0]
#         phi0[1]+=deltas[1]
#         phi0[2]+=deltas[2]
#         phi0[3]+=deltas[3]
    plt.subplot(3,1,1)
    plt.title("Experiment "+str(experimentNum))
    timeVector=dataSet.getTime()
    timesToPlot=np.array([timeVector[t] for t in regressor.t])
    rts=np.array(regressor.t)
    print rts.shape
    print regressor.bodyPositionAccel.shape
    plt.plot(timesToPlot,regressor.bodyPositionAccel[:,0],'b')
    plt.plot(timesToPlot,predictionXdd,'r')
    plt.legend([r"numerical $\ddot{X}_{body}$",r"predicted $\ddot{X}_{body}$"])
    plt.subplot(3,1,2)
    plt.plot(timesToPlot,regressor.bodyPosition[:,1],'b')
    plt.plot(timesToPlot,predictionZ,'r')
    plt.legend([r"numerical $Z_{body}$",r"predicted $Z_{body}$"])
    plt.subplot(3,1,3)
    tau=dataSet.getMeasuredTorque()
    plt.plot(timesToPlot,tau[rts[0]:rts[-1]+1,1],'r')
    plt.plot(timesToPlot,tau[rts[0]:rts[-1]+1,2],'g')
    plt.plot(timesToPlot,tau[rts[0]:rts[-1]+1,4],'b')
    plt.plot(timesToPlot,tau[rts[0]:rts[-1]+1,5],'m')
    plt.legend([r"$\tau_1$",r"$\tau_2$",r"$\tau_4$",r"$\tau_5$"])
    plotter.plotRagDollsWithCOM(rts, phi0[1], phi0[2], regressor.tilt, phi0[3])
    plt.show()
            
        
        