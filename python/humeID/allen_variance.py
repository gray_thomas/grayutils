import numpy as np
import random as r
import matplotlib.pyplot as plt 
from math import *

class DriftingBiasErrorModel(object):
	""" This model's bias randomly walks """
	def __init__(self,sigma=1.0,init=0.0, T=1.0):
		self.value=init  #[rad/s]
		self.sigma=sigma  #[rad/s^2]
	def next(self):
		return self()
	def __call__(self):
		if self.value!=0.0:
			if log(abs(self.value))>12.0*log(2)+log(abs(self.sigma)):
				raise RuntimeError("silly scaling")
		self.value+=r.gauss(0.0,self.sigma)
		return self.value

class StraightNoiseErrorModel(object):
	""" This is straight up noise """
	def __init__(self,sigma=1.0):
		self.sigma=sigma
	def next(self):
		return self()
	def __call__(self):
		return r.gauss(0.0,self.sigma) #[rad/s]

class DiscreteLowPass(object):
	""" this is a low pass filter, representing internal filtering """
	def __init__(self, pole, init=0.0):
		self.pole=pole
		self.k=(1.0-self.pole)
		self.value=init
	def __call__(self, inp):
		# (value - B value pole)=k inp
		# value = k inp + B value pole
		# k = 1-pole
		self.value += self.k*(inp - self.value)
		return self.value


def compute_allan_variance(series,avg_n):
	n=len(series)
	bins=int(n/avg_n-1)
	# print bins-1
	# print [range(avg_n*i,avg_n*i+avg_n) for i in range(0,bins)]
	avg_series=[sum(sub)*(1.0/avg_n) for sub in [series[avg_n*i:avg_n*i+avg_n] for i in range(0,bins)]]
	avar=1.0/(2.0*(bins-1.0))*sum([pow(avg_series[i+1]-avg_series[i],2) for i in range (0,bins-1)])
	return avar

def make_allan_variance_plot(d,low_=20, high_=5000, dec_=30):
	adev=[]
	bin_sizes=int_logarithmic_range(low_,high_,dec_)
	for bin_size in bin_sizes:
		adev.append(compute_allan_variance(d,bin_size))
	plt.loglog(bin_sizes,adev)
def make_allan_deviation_plot(d,low_=20, high_=5000, dec_=30):
	adev=[]
	bin_sizes=int_logarithmic_range(low_,high_,dec_)
	for bin_size in bin_sizes:
		adev.append(sqrt(compute_allan_variance(d,bin_size)))
	plt.loglog(bin_sizes,adev)
from math import *
def int_logarithmic_range(mmin,mmax,perdecade):
	lns=np.linspace(log10(mmin),log10(mmax),perdecade*(log10(mmax)-log10(mmin)))
	ints=[int(ceil(pow(10,ln))) for ln in lns]
	return ints

def loglogline(points,plot=False):
	xs, ys=zip(*[(log(p[0]),log(p[1])) for p in points])
	x=np.array(xs).reshape((-1,1))
	y=np.array(ys).reshape((-1,1))
	X=np.concatenate([np.ones((len(xs),1)),x],axis=1)
	# xs*phi=ys
	phi,arg2,arg3,arg4=np.linalg.lstsq(X,y)
	if plot:
		plt.loglog([exp(x) for x in xs],[exp(out) for out in X.dot(phi)])
		# plt.loglog([exp(x) for x in xs],[exp(y) for y in ys])
	return phi

def logloglineslope1(points,plot=False):
	slope=1.0
	# xs*phi=ys
	xs, ys=zip(*[(log(p[0]),log(p[1])) for p in points])
	x=np.array(xs).reshape((-1,1))
	y=np.array(ys).reshape((-1,1))
	u=np.ones((len(xs),1))
	X=np.concatenate([u,x],axis=1)
	# xs*phi_est=ys-xs*phi_known
	phi,arg2,arg3,arg4=np.linalg.lstsq(u,y-x*slope)
	phi=np.array([[phi[0,0]],[slope]])
	if plot:
		plt.loglog([exp(x) for x in xs],[exp(out) for out in (X.dot(phi))])
		# plt.loglog([exp(x) for x in xs],[exp(y) for y in ys])
	return phi

def logloglineslopeneg1(points,plot=False):
	slope=-1.0
	# xs*phi=ys
	xs, ys=zip(*[(log(p[0]),log(p[1])) for p in points])
	x=np.array(xs).reshape((-1,1))
	y=np.array(ys).reshape((-1,1))
	u=np.ones((len(xs),1))
	X=np.concatenate([u,x],axis=1)
	# xs*phi_est=ys-xs*phi_known
	phi,arg2,arg3,arg4=np.linalg.lstsq(u,y-x*slope)
	phi=np.array([[phi[0,0]],[slope]])
	if plot:
		plt.loglog([exp(x) for x in xs],[exp(out) for out in (X.dot(phi))])
		# plt.loglog([exp(x) for x in xs],[exp(y) for y in ys])
	return phi

def plot_prediction(bin_sizes,c1,c2):
	expected=[exp(c1)/b+b*exp(c2) for b in bin_sizes]
	print "var=%.4f/b+b*%.4f"%(exp(c1),exp(c2))
	plt.loglog(bin_sizes,expected)
	return expected
# variance at low bin size (sigma_noise=1, sigma_bias=0.1):
# 	ln(var)=-0-ln(bin_size)
# so 
# 	var=bin_size^-1
# at high bin size:
# 	ln(var)=-5ish + ln(bin_size)
# so 
#	var=e^-5 * bin_size
# perhaps we can combine the two:
#	var=1/b+b*e^-5
# experimentally (sigma_noise = 1.00000, sigma_drift = 0.10000):
# 	var=(1.043 \pm 0.007)/b+b(0.0033 \pm 0.0001)
# experimentally (sigma_noise = 1.00000, sigma_drift = 0.20000):
# 	var=(1.00ish)/b+b(0.0134 \pm 0.0007)
# experimentally (sigma_noise = 1.00000, sigma_drift = 0.40000):
# 	var=(1.00ish)/b+b(0.055 \pm 0.015)
# hypothesis:
#	var=(sigma_noise^2)/b + b (sigma_drift^2 / 3)
# this hypothesis seems highly likely, accuratly predicting the
# variance in 9 randomly generated cases within the sampling variance 
# of this variance estimate which is inherent in the emperical plot generation process.
# the nature of the constant 3 is not entirely certain, however, as it is difficult
# to estimate the value with precision.
# From IEEE 952-1997:
# 	sigma_squared_omega(nT0)= 
#								1/2 R^2 (nT0)^2  # nope
#								+ 1/3 K^2 (nT0)^1 # yup
#								+ 2/pi ln(2) B^2 # nope
#								+ N^2/nT0         # yup
#								+ 3 Q^2/(nT0)^2  # nope
# where 
	# K is rate random walk,     # exists
	# Q is angle quantization,   # not important?
	# R is ramp,                 # no evidence
	# B is bias instability,     # I can't find a state space model which predicts this.
	# and N is angle random walk # exists

# on to the question on modeling our sensor.
# 	bias_dev = min(sqrt(var))=min(sqrt((sigma_noise^2)/b + b (sigma_drift^2 / 3))
# this occurs at 
#	arg_b_min (sigma_noise^2)/b + b (sigma_drift^2 / 3)
# which, as an arg min is invariant to constants such that 
# 	A (1/(k*b)+b*k)=(sigma_noise^2)/b + b (sigma_drift^2 / 3)
#   A*k=sigma_drift^2/3
#	A/k=sigma_noise^2
#	A^2=sigma_drift^2/3*sigma_noise^2
#	A=sd*sn / sqrt(3)
#	k=sd^2 / (3 * sd*sn / sqrt(3))
#	k = sd / (sn * sqrt(3))
# minimum value occurs when 
#	(1/(k*b)+b*k) = 2
# because, differentiating
#	d/dx [x+x^-1]= 1 + -1*x^-2=0 @ x=1 dd/dxdx=2x^-3 is convex for positive x --> global minimum
# so 
#	b = 1/k = sn * sqrt(3) / sd
# thus
	# b [tics]
	# sn^2 [(rad/sec)^2 / tic_freq]
	# sd^2 [(rad/sec)^2 / tic]
	# bias_dev^2 [(rad/sec)^2]
#	bias_dev = sqrt( sn^2 / b + b * sd^2 / 3 ) = 
# 			 = sqrt( sn^2 * sd * sqrt(3) / sn + ( sn / (sqrt(3) * sd) ) * sd^2 / 3 )
#			 = sqrt( sn * sd / sqrt(3) + sn * sd / sqrt(3) )
#			 = sqrt(2 / sqrt(3) * sn * sd)
#			 = sqrt(2) * 1/sqrt(sqrt(3)) * sqrt(sn) * sqrt(sd)
# so
#	sn = arw*sqrt(pi)   [V/T sqrt(1/T)]  - red angle noise
#	sd = bias_dev^2 * sqrt(3) / sn / 2    [V/T^2 sqrt(1/T)] - red angular rate noise

# KVH_IMU_1750
# Angle Random Walk = 0.012 deg/sqrt(hr) = 0.7 deg/hour/sqrt(Hz)
# Bias Instability = 0.1 deg/hour (max), 0.05 deg/hour (typ)
class Hume_IMU_Gyro_Noise_Model(object):
	def __init__(self,init_filt=0.0,init_drift=0.0,T=0.001):
		self.T=T # [s] =  1 ms (generally)
		self.f=1.0/self.T # [Hz] = 1 KHz
		self.filt_Fb = 440 # [Hz]
		self.filt_pole = exp(-self.filt_Fb*self.T)
		self.reported_noise_density=0.03 # deg/sec/sqrt(Hz)
		self.arw = self.reported_noise_density*pi/180.0 # rad/sec/sqrt(Hz)
		self.var_filt_n = pow(self.arw,2) # (rad/sec)^2/Hz
		self.sn_squared = self.var_filt_n * pi * self.f # (rad/sec)^2/Hz [pi motivated by L2 norm of low pass]
		self.sn = sqrt(self.sn_squared) # rad/sec (per sample)
		self.reported_bias_instability = 18.0 # deg/hour
		self.bi = self.reported_bias_instability * pi/180.0 /60.0/60.0 # rad/sec
		self.bias_variance = pow(self.bi,2) # (rad/sec)^2
		# bv=2 / sqrt(3) * sn * sd
		self.sd = self.bias_variance * sqrt(3) / 2.0 / self.sn # rad/sec / tic
		print "imu drift rate", self.sd
		print "imu noise level", self.arw*sqrt(self.filt_Fb)
		print "tics until 1 std bias move", pow(self.arw*sqrt(self.filt_Fb)/self.sd,2)
		self.filt=DiscreteLowPass(self.filt_pole,init=init_filt)
		self.drift=DriftingBiasErrorModel(sigma=self.sd,init=init_drift)
		self.noise=StraightNoiseErrorModel(sigma=self.sn)
	def __call__(self, true_value):
		return self.filt(self.noise()+self.drift()+true_value)
# LORD_3DM-GX3-25-OEM
# Noise Density = 0.03 deg/sec/sqrt(Hz)
# arw = 0.00523598 # rad/sec/sqrt(Hz)
# var_n = 2.741556 e-7 # (rad/sec)^2 /Hz
# var_n * f_LP = sigma_noise^2 /pi f_LP 
# sn^2=8.612854633416614e-7 # (rad/sec)^2 /Hz
# sn = 0.000928054666 # rad/sec /sqrt(Hz)
# Bias Instability = 18 deg/hour
# bias variance= (18 * pi/180 *60*60)^2 (rad/s)^2
# bi= 0.005 deg/sec = 8.7e-5 rad/sec
# bi=bias_dev=sqrt(2) / sqrt(sqrt(pi)) * sqrt(sn) * sqrt(sd)
# bi*sqrt(sqrt(pi))/sqrt(2)=sqrt(sn)*sqrt(sd)=8.2e-5 rad/sec
# sn*sd = bi^2*sqrt(pi)/sqrt(2) = 6.7e-9 (rad/sec)^2
# sd = 7.27220521772925 e-6 rad/sec
class Hume_IMU_Accelerometer_Noise_Model(object):
	def __init__(self,init_filt=0.0,init_drift=0.0,T=0.001):
		self.T=T # [s] =  1 ms (generally)
		self.f=1.0/self.T # [Hz] = 1 KHz
		self.filt_Fb = 225 # [Hz]
		self.filt_pole = exp(-self.filt_Fb*self.T)
		self.reported_noise_density=80.0e-6 # g/sqrt(Hz)
		self.arw = self.reported_noise_density*9.8 # m/s/s/sqrt(Hz)
		self.var_filt_n = pow(self.arw,2) # (m/s/s)^2/Hz
		self.sn_squared = self.var_filt_n * pi * self.f # (m/s/s)^2 [pi motivated by L2 norm of low pass]
		self.sn = sqrt(self.sn_squared) # m/s/s (per sample)
		self.reported_bias_instability = 0.04e-3 # g
		self.bi = self.reported_bias_instability *9.8 # m/s/s
		self.bias_variance = pow(self.bi,2) # (m/s/s)^2
		# bv=2 / sqrt(3) * sn * sd
		self.sd = self.bias_variance * sqrt(3) / 2.0 / self.sn # m/s/s / tic
		print "imu acc drift rate", self.sd
		print "imu acc noise level", self.arw*sqrt(self.filt_Fb)
		print "[acc] tics until 1 std bias move", pow(self.arw*sqrt(self.filt_Fb)/self.sd,2)
		self.filt=DiscreteLowPass(self.filt_pole,init=init_filt)
		self.drift=DriftingBiasErrorModel(sigma=self.sd,init=init_drift)
		self.noise=StraightNoiseErrorModel(sigma=self.sn)
	def __call__(self, true_value):
		return self.filt(self.noise()+self.drift()+true_value)


def plot_hyp1(bin_sizes,sigma_noise,sigma_drift):
	expected=[pow(sigma_noise,2)/b+b*pow(sigma_drift,2)/pi for b in bin_sizes]
	print "var=%.4f/b+b*%.4f"%(pow(sigma_noise,2),pow(sigma_drift,2)/3.0)
	plt.loglog(bin_sizes,expected)
	return expected
def plot_hyp_dev_deg_hours(bin_sizes,sigma_noise,sigma_drift):
	var=[pow(sigma_noise,2)/b+b*pow(sigma_drift,2)/pi for b in bin_sizes]
	print "var=%.4f/b+b*%.4f"%(pow(sigma_noise,2),pow(sigma_drift,2)/3.0)
	dev=[sqrt(v)*180.0/pi*60*60 for v in var]
	plt.loglog(bin_sizes,dev)
	return dev

if __name__=="__main__":
	noise_model=Hume_IMU_Gyro_Noise_Model()
	def gen_series(n=1000,sigma_drift=0.005,sigma_noise=1.0,model=noise_model):
		# filt=DiscreteLowPass(0.5)
		# drift=DriftingBiasErrorModel(sigma=sigma_drift,T=0.001)
		# # drift=StableDriftingBiasErrorModel(sigma=sigma_drift, k=0.001)
		# drift=RootBiasDriftModel(sigma=sigma_drift, k=1.0)
		# noise=StraightNoiseErrorModel(sigma=sigma_noise)
		time_series=[]	
		for i in range (0,n):
			time_series.append(model(0.0))
		return time_series
	for j in range(0,0):
		time_series=gen_series()
		plt.plot(time_series)
	for j in range(0,00):
		d=gen_series(n=100000)
		make_allan_variance_plot(d)
	for j in range(0,00):
		d=gen_series(n=100000,sigma_drift=0.01)
		make_allan_variance_plot(d)
	for j in range(0,00):
		d=gen_series(n=100000,sigma_noise=2)
		make_allan_variance_plot(d)
	### Fitting lines to allan variance plot subparts
	for j in range(0,0):
		d=gen_series(n=100000,sigma_drift=0.1)
		make_allan_variance_plot(d,low_=10, high_=5000, dec_=30)
	for j in range(0,0):
		d=gen_series(n=40000,sigma_drift=0.4)
		avar=[]
		bin_sizes=int_logarithmic_range(100,500,30)
		for bin_size in bin_sizes:
			avar.append(compute_allan_variance(d,bin_size))
		phi_slow = logloglineslope1(zip(bin_sizes,avar),plot=True)
		avar=[]
		bin_sizes=int_logarithmic_range(1,6,30)
		for bin_size in bin_sizes:
			avar.append(compute_allan_variance(d,bin_size))
		phi_fast= logloglineslopeneg1(zip(bin_sizes,avar),plot=True)
		print "slow", phi_slow.T, "fast", phi_fast.T
		bin_sizes=int_logarithmic_range(1,5000,30)
		plot_prediction(bin_sizes,phi_fast[0,0],phi_slow[0,0])
		make_allan_variance_plot(d,low_=1, high_=5000, dec_=30)
	for k in range(0,0):
		for j in range(0,0):
			sigma_drift=[0.4,0.2,0.1][j]
			sigma_noise=[4.0,8.0,16.0][k]
			bin_sizes=int_logarithmic_range(1,5000,30)
			d=gen_series(n=40000,sigma_noise=sigma_noise,sigma_drift=sigma_drift)
			plot_hyp1(bin_sizes,sigma_noise,sigma_drift)
			make_allan_variance_plot(d,low_=1, high_=5000, dec_=30)
	plt.figure()
	plt.title("Our IMU")
	sigma_drift=7.27220521772925e-6
	sigma_noise=0.000928054666
	bin_sizes=int_logarithmic_range(1,500000,30)
	print "sigma_noise=%0.6f or %0.6f"%(sigma_noise*sqrt(1000),noise_model.sn)
	print "sigma_drift=%0.6f or %0.6f"%(sigma_drift*sqrt(1000)*0.001,noise_model.sd)
	plt.plot(gen_series(n=6000,model=Hume_IMU_Gyro_Noise_Model(T=3600.0)))
	plt.figure()
	# d=gen_series(n=5000000*2,sigma_noise=sigma_noise,sigma_drift=sigma_drift)
	# plot_hyp1(bin_sizes,sigma_noise*sqrt(1000),sigma_drift*sqrt(1000)*0.001)
	plot_hyp_dev_deg_hours(bin_sizes,noise_model.sn,noise_model.sd)
	plt.grid(True)
	plt.xlabel("bin size (ms)")
	plt.ylabel("allan deviation deg/hour")
	# make_allan_variance_plot(d,low_=1, high_=500000, dec_=30)

	print sqrt(0.1)
	a=[203.69,0.1035]
	b=[4655.4,0.344]
	y=[0.1035,0.344]
	x=[203.69,4655.4]
	print loglogline([a,b])
	a=[20.9751,0.439186]
	b=[242.759,0.135788]
	c=[68.4309,0.244953]
	print loglogline([a,b,c])
	a=[21.2505,0.217443]
	b=[29.9664,0.184433]
	c=[40.9213,0.159287]
	d=[55.7017,0.138679]
	e=[89.3168,0.108767]
	print loglogline([a,b,c,d,e])
	a=[512.963,0.13935]
	b=[929.664,.182001]
	c=[1630.54,0.236924]
	d=[2927.56,0.324056]
	e=[4831.45,0.423241]
	print loglogline([a,b,c,d,e])

	plt.show()

	# print time_series
	# make_allan_variance_plot(time_series)