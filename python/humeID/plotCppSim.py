'''
Created on Apr 13, 2014

@author: hcrl
'''
import numpy as np
import matplotlib.pyplot as plt
import math
from uta.humeID import GRAY_UTILS
from uta.humeID import MEKA
from mpl_toolkits.mplot3d import Axes3D
from yaml import load, dump
import io


# inputTorque = sea.torquePerUnitInput * input / sea.motor.mass;


def plotCppSimFromSimFolder(fileName):
    data = np.loadtxt(MEKA + "/m3hume_monolith/cppsim/" + fileName);
    timeData = data[:, 0];
    motorCurrentDacData = data[:, 1];
    motorEncoder = data[:, 2];
    motorEncoderVelocity = data[:, 3];
    jointEncoder = data[:, 4];
    jointEncoderVelocity = data[:, 5];
    seaSpringEncoder = data[:, 6];
    accelJoint = data[:, 7];
    filtJoint = data[:, 8];
    filtJointVel = data[:, 9];
    filtJointAcc = data[:, 10];
    bias = data[:, 11];
    gain = data[:, 12];
    estGain = data[:, 13];
    estFric = data[:, 14];
    estBias = data[:, 15];
    lpDAC = data[:, 16];
#     estimatedFriction=data[:,7];
#     estimatedBias=data[:,8];
#     estimatedGain=data[:,9];
#     simulationEncoderNoise=data[:,10];
#     simulationFrictionNoise=data[:,11];
    sc = lambda (dat): 1.0 / (max(max(dat), -min(dat)))
    scales = [sc(data[:, i]) for i in range(0, 17)]

    plt.figure()
    plt.hold(True)
    plt.plot(timeData, data[:, 1]*scales[1]); # dac
    plt.plot(timeData, data[:, 16]*scales[1]); # lp dac
    plt.plot(timeData, data[:, 4]*scales[4]);
    plt.plot(timeData, data[:, 5]*scales[5]);
    plt.plot(timeData, data[:, 8]*scales[4]);
    plt.plot(timeData, data[:, 9]*scales[5]);
    plt.plot(timeData, data[:, 7]*scales[7]);
    plt.plot(timeData, data[:, 10]*scales[7]);
    plt.xlabel("time in seconds")
    plt.ylabel("scaled by maximum value of actual signal")
    plt.title("Filtered Signals used by gain estimator")
    plt.legend(["current","filtered current","position","filtered position", "velocity", "filtered velocity", "acceleration", "filtred acceleration"])
    plt.figure()
    plt.subplot(4, 1, 1)
    plt.plot(timeData, filtJointVel/ max(filtJointVel));
    plt.plot(timeData, filtJointAcc / max(filtJointAcc));
    plt.plot(timeData, lpDAC/max(lpDAC));
    plt.legend(["filtered joint velocity",'filtered joint acceleartion','filtered DAC'])
    
    plt.subplot(4, 1, 2)
    plt.plot(timeData, data[:, 1:7].dot(np.diagflat([scales[1:7]])));
    plt.legend(["input","motor","motVel","out","outvel","spring"])
    plt.subplot(4, 1, 3)
    plt.plot(timeData,bias)
    plt.plot(timeData,gain)
    plt.plot(timeData,estBias)
    plt.plot(timeData,estGain)
    plt.plot(timeData,estFric)
    plt.legend(["bias",'gain','estBias','estGain','estFric'])
    plt.subplot(4, 1, 4)
    plt.plot(timeData, filtJointVel);
    plt.plot(timeData, filtJointAcc);
    plt.plot(timeData, lpDAC);
    plt.legend(["filtered joint velocity",'filtered joint acceleartion','filtered DAC'])
    
    plt.figure()
    plt.plot(timeData, data[:, 1:7])
    plt.legend(['input','motor enc','motor enc vel','joint enc','joint enc vel','spring enc'])
    


if __name__ == '__main__':
    plotCppSimFromSimFolder('demo1.txt');
    plt.show()
