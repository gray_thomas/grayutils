import numpy as np

class HumeDataFolder(object):
    def __init__(self, folderLocation):
        self.folderLocation = folderLocation + "/";
    def loadRawMatrix(self, fileName):
        data = np.loadtxt(self.folderLocation + fileName)
        return data
    def getTime(self):
        return self.loadRawMatrix("time.txt")
    def getTorqueInput(self):
        return self.loadRawMatrix("torque_input.txt")
    def getPhase(self):
        return self.loadRawMatrix("phase.txt")
    def getMeasuredTorque(self):
        return self.loadRawMatrix("actual_torque.txt")
    def getMeasuredVelocity(self):
        return self.loadRawMatrix("full_state_vel.txt")
    def getMeasuredJoints(self):
        return self.loadRawMatrix("full_state_pos.txt")
    def getFootDesired(self):
        return self.loadRawMatrix("foot_des.txt")
    def getFootPosition(self):
        return self.loadRawMatrix("foot_pos.txt")
    def getFootVelocity(self):
        return self.loadRawMatrix("foot_vel.txt")
    def getCOMPosition(self):
        return self.loadRawMatrix("com_pos.txt")
    def getCOMVelocity(self):
        return self.loadRawMatrix("com_vel.txt")
    def getCOMDesired(self):
        return self.loadRawMatrix("com_des.txt")