from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation
from leds import new_const
from hume_frames_and_poses import *
from hume_sim import HumeSim
from hybrid_hume import HybridHume
import attitudeEstimator.attitude_estimation_cpp as ae_cpp
from gu.math.transforms import Transform
import random as rand 
from allen_variance import StraightNoiseErrorModel
from animation_3d_graphics import *
from gu.math.frames import Frame
from gu.math.thin_spatial_algebra import *
from gu.math.Quaternion import Quaternion
from math import sqrt

class LiveKFandLEDhandler(object):
    def __init__(self,receiver,imu):
        receiver.add_python_listener(self)
        imu.add_python_listener(self.imuUpdate)
        self.kf=ae_cpp.wrapped_kalman_filter(led_cameraT,new_const,25.0,0.4);
        self.data=new_const
        self.imu_ang_vel=np.array([[1.0,1.0,1.0]]).T
        self.imu_lin_acc=np.array([[0.0,0.0,-1.0]]).T
        self.nm=StraightNoiseErrorModel(sigma=0.1)
        
    def imuUpdate(self, lin_acc, ang_vel):
        self.imu_lin_acc=np.array(lin_acc).reshape((-1,1))
        self.imu_ang_vel=np.array(ang_vel).reshape((-1,1))

    def __call__(self, leds, selector):
        self.data=led_cameraT.dot(np.array(leds))
        self.kf.measure_imu([self.nm(),self.nm(),self.nm(),self.nm()+0.1,self.nm()+0.1,self.nm()+0.1])
        self.kf.measure_leds(leds,[bool(x) for x in selector[0,:]])

class LiveGraphicLEDs(BaseGraphicLEDDiff):
    def __init__(self,kfhandler):
        super(LiveGraphicLEDs,self).__init__()
        self.data=new_const+200.0*np.ones((3,7))
        self.kfhandler=kfhandler;

    def anim(self,sim,i):
        leds1=self.kfhandler.data
        leds2=self.kfhandler.kf.estimate_leds()
        return self.anim_diff(leds1,leds2)

    def init(self,ax):
        return self.init_lines(ax,'ro','kx')

class FrameDisplayKf(object):
    def __init__(self,kfhandler):
        self.lines=None
        self.kfhandler=kfhandler
    def init(self,ax):
        if self.lines==None:
            self.lines=[ax.plot([0.0,0.1],[0.0,0.0],'r',zs=[200.0,200.0],linewidth=4)[0],
                        ax.plot([0.0,0.0],[0.0,0.1],'g',zs=[200.0,200.0],linewidth=4)[0],
                        ax.plot([0.0,0.0],[0.0,0.0],'b',zs=[200.0,200.1],linewidth=4)[0]]
            self.hume=HybridHume()
        return self.lines
    def anim(self,sim,i):
        tran = Transform(None,None)
        dcm, vec = self.kfhandler.kf.estimate_transform()
        tran.forwardDCM=dcm
        tran.vec=vec
        pts=tran.forward_transform_fat_point_matrix(
            np.concatenate([np.zeros((3,1)),np.eye(3,3)*0.2],axis=1))
        # print pts
        for i in range(0,3):
            self.lines[i].set_data(pts[0,[0,i+1]],pts[1,[0,i+1]])
            self.lines[i].set_3d_properties(pts[2,[0,i+1]])
        return self.lines




class IMUPlotter():
    def __init__(self, kfhandler):
        self.ang=baseGraphicVector()
        self.lin=baseGraphicVector()
        self.g=baseGraphicVector()
        self.tot=baseGraphicVector()
        self.kfhandler=kfhandler
    def init(self,ax):
        return (self.ang.init_line(ax,'r')
            +self.lin.init_line(ax,'b')
            +self.g.init_line(ax,'g')
            +self.tot.init_line(ax,'c'))
    def anim(self,sim,i):
        tran = Transform(world,body)
        dcm, vec = self.kfhandler.kf.estimate_transform()
        tran.forwardDCM=dcm
        tran.vec=vec
        ttot= tran*imu_pose
        dcm=ttot.forwardDCM
        vec=ttot.vec
        p0=vec.reshape((-1,1))
        # print p0, self.kfhandler.imu_ang_vel
        pa=p0+dcm.dot(self.kfhandler.imu_ang_vel)
        pl=p0+dcm.dot(self.kfhandler.imu_lin_acc)
        pg=pl-np.array([[0.0,0.0,1.0]]).T
        return (self.ang.anim_line(p0,pa) +
                self.lin.anim_line(p0,pl) +
                self.g.anim_line(pl,pg) +
                self.tot.anim_line(p0,pg))