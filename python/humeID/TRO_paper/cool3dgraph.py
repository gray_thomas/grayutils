
"""
A module to generate a graph for the TRO paper.

This module will calculate the A' matrix, and graph it's largest
magnitude of an eigenvalue as a function of t' and kappa_p
"""

from math import sinh, cosh, tanh, sqrt
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as axes3d
from matplotlib import cm
from matplotlib.colors import LinearSegmentedColormap
import numpy as np


def coth(var1):
    """Hyperbolic cotangent function."""
    return 1.0 / tanh(var1)


def get_a_prime(frequency, step_time, proportional_gain, time_to_reversal):
    """return the A' matrix."""
    rel_t = step_time * frequency
    rel_prime = time_to_reversal * frequency
    a_prime = np.array([[
        1 + proportional_gain * (coth(rel_t) - 1),
        (1.0 / frequency) * (1 + sinh(rel_t) - cosh(rel_t)) * coth(rel_prime)
    ], [
        proportional_gain * frequency * sinh(rel_t),
        cosh(rel_t) - sinh(rel_t) * tanh(rel_prime)]])
    return a_prime


def largest_eig_mag(a_prime):
    """returns the magnitude of the largest eigenvalue of a_prime."""
    eigs, vecs = np.linalg.eig(a_prime)
    max_eig_mag = max([abs(eig) for eig in eigs])
    return max_eig_mag


def main():
    """Make the plot."""
    frequency = sqrt(9.8 / 1.1)
    step_time = 0.5
    a_prime = get_a_prime(frequency, step_time, 0.4, 0.2)

    def gmag(pgain, tprime):
        a_prime = get_a_prime(frequency, step_time, pgain, tprime)
        return largest_eig_mag(a_prime)

    fig_1 = plt.figure(figsize=(16,9),facecolor='w')
    ax = fig_1.gca(projection='3d')
    min_p=-0.4
    max_p=0.05
    min_t=0.2
    max_t=0.7
    pgains = np.arange(min_p, max_p, 0.01)
    tprimes = np.arange(min_t, max_t, 0.005)
    max_eigs = np.zeros((len(pgains), len(tprimes)))
    sat_max = np.zeros((len(pgains), len(tprimes)))
    #for i, item in enumerate(L):
    for i, pgain in enumerate(pgains):
        for j, tprime in enumerate(tprimes):
            max_eigs[i, j]=gmag(pgain, tprime)
            sat_max[i, j]= max_eigs[i, j]
            if sat_max[i, j] > 1.0:
                sat_max[i, j] = 1.0

    pgains, tprimes=np.meshgrid(pgains, tprimes)
    # R = np.sqrt(pgains**2 + tprimes**2)
    # max_eigs = np.sin(R)
    max_eigs = max_eigs.T
    cdict = {'red':   [(0.0,  1.0, 1.0),
                       (0.8,  1.0, 1.0),
                       (0.99, 0.0, 0.0),
                       (1.0,  1.0, 1.0)],

             'green': [(0.0,  1.0, 1.0),
                       (0.8,  1.0, 1.0),
                       (0.99, 0.0, 0.0),
                       (1.0,  0.0, 0.0)],

             'blue':  [(0.0,  1.0, 1.0),
                       (0.8,  1.0, 1.0),
                       (0.99, 1.0, 1.0),
                       (1.0,  0.0, 0.0)]}
    my_colors = LinearSegmentedColormap('control',cdict)
    surf = ax.plot_surface(pgains, tprimes, max_eigs, rstride=1, cstride=1, alpha=0.3,
        linewidth=0, antialiased=False, facecolors=my_colors(sat_max.T),vmin=0.0, vmax=1.0)
    levels = [0.80,0.90,0.95,0.98,0.99,1.0,1.25]
    cset=ax.contour(pgains, tprimes, max_eigs, levels, zdir='z', offset=0.7, colors='k')
    ax.clabel(cset,levels, zdir='z', offset=0.7, inline=1,fmt = '%1.2f',fontsize=14)
    # cset=ax.contourf(pgains, tprimes, max_eigs, zdir='x', offset=min_p, cmap=cm.coolwarm)
    # cset=ax.contourf(pgains, tprimes, max_eigs, zdir='y', offset=max_t, cmap=cm.coolwarm)
    m = cm.ScalarMappable(cmap=my_colors)
    m.set_array([0,1])
    plt.colorbar(m)
    ax.set_xlabel('pgains')
    ax.set_xlim(min_p, max_p)
    ax.set_ylabel('tprimes')
    ax.set_ylim(min_t, max_t)
    ax.set_zlabel('max_eigs')
    ax.set_zlim(0.7, 1.0)
    # fig_1.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()

if __name__ == "__main__":
    main()
