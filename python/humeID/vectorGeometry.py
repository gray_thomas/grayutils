import numpy as np 
from math import pow, sqrt, pi
from collections import namedtuple
from gu.math import Quaternion, quat_exponent, cross
from gu.math.frames import *
from gu.math.thin_spatial_algebra import *

if __name__=="__main__":
    a = Quaternion(1.0,2.0,3.0,4.0)
    print a, a.norm
    print a.normalize()
    print a.normalize().DCM()




class FramePoint(object):
    def __init__(self, base_frame, vec):
        self.base_frame=base_frame
        assert vec.shape==(3,1)
        self.vec = vec

    def add_vector(self,other):
        Frame.check(self,other)
        return FramePoint(self.base_frame, self.vec+other.vec)
        
    def __add__(self, other):
        if isinstance(other, FramePoint):
            raise NonsenseException("Points are never added. Perhaps you meant to add a point and a vector?")
        if isinstance(other, FrameVector):
            return self.add_vector(other)
        raise NotImplemented("Class un-addable to FramePoint objects: %s "% type(other))

    # def __sub__(self,other):


    def __repr__(self):
        return "[%s](%.5f,%.5f,%.5f)"%(self.base_frame.name,self.vec[0,0],self.vec[1,0],self.vec[2,0])

class FrameVector(object):
    def __init__(self, base_frame, vec):
        self.base_frame=base_frame
        assert vec.shape==(3,1)
        self.vec=vec
    def add_vector(self,other):
        Frame.check(self,other)
        return FrameVector(self.base_frame, self.vec+other.vec)
    def add_point(self,other):
        Frame.check(self,other)
        return FramePoint(self.base_frame, self.vec+other.vec)
    def cross(self, other):
        Frame.check(self,other)
        return FrameVector(self.base_frame, cross(self.vec).dot(other.vec) )
    def __add__(self,other):
        if isinstance(other, FramePoint):
            return self.add_point(other)
        if isinstance(other, FrameVector):
            return self.add_vector(other)
        raise NotImplemented("Class un-addable to FrameVector objects: %s "% type(other))
    def __repr__(self):
        return "[%s]<%.5f,%.5f,%.5f>"%(self.base_frame.name,self.vec[0,0],self.vec[1,0],self.vec[2,0])


class SpatialForceVector(object):
    def __init__(self, base_frame, v, w):
        self.base_frame=base_frame
        assert v.shape==(3,1)
        assert w.shape==(3,1)
        self.v=v
        self.w=w
    @classmethod
    def from_point_force(cls, point, force):
        Frame.check(point,force)
        return cls(point.base_frame, force.vec, cross(point.vec).dot(force.vec))
    @property 
    def vec(self):
        return np.array([[self.v[0],self.v[1],self.v[2],self.w[0],self.w[1],self.w[2]]]).T
    def __repr__(self):
        return "[%s][*[%.2f,%.2f,%.2f -- %.2f,%.2f,%.2f]*]" %(self.base_frame, self.v[0], self.v[1], self.v[2], self.w[0], self.w[1], self.w[2])
    @classmethod
    def from_vec(cls, base_frame, vec):
        assert vec.shape==(6,1)
        return cls(base_frame, vec[0:3,:], vec[3:6,:])

class SpatialMotionVector(object):
    def __init__(self, base_frame, v, w):
        self.base_frame=base_frame
        self.v=v
        self.w=w
    @property 
    def vec(self):
        return np.array([[self.v[0],self.v[1],self.v[2],self.w[0],self.w[1],self.w[2]]]).T
    def __repr__(self):
        return "[%s][[%.2f,%.2f,%.2f -- %.2f,%.2f,%.2f]]" %(self.base_frame, self.v[0], self.v[1], self.v[2], self.w[0], self.w[1], self.w[2])

class ArticulatedBodyInertia(object):
    def __init__(self, base_frame, mat):
        self.base_frame=base_frame
        assert mat.shape==(6,6)
        self.mat=mat
    def __mul__(self,other):
        if not isinstance(other, SpatialMotionVector):
            raise NotImplemented("only valid for motion vectors")
        Frame.check(self,other)
        return SpatialForceVector(self.base_frame, self.mat.dot(other.vec))
    def __repr__(self):
        return "ArticulatedInertia [%s]:\n"%self.base_frame+str(self.mat)+"\nEnd ArticulatedInertia"

class InverseArticulatedInertia(object):
    def __init__(self, base_frame, mat):
        self.base_frame=base_frame
        assert mat.shape==(6,6)
        self.mat=mat
    def __mul__(self,other):
        if not isinstance(other, SpatialForceVector):
            raise NotImplemented("only valid for motion vectors")
        Frame.check(self,other)
        return SpatialMotionVector(self.base_frame, self.mat.dot(other.vec))
    def __repr__(self):
        return "InverseInertia [%s]:\n"%self.base_frame+str(self.mat)+"\nEnd InverseInertia"

def inverse_of_inertia(inertia):
    return InverseArticulatedInertia(inertia.base_frame, np.linalg.inv(inertia.mat))
def inverse_of_inverse_inertia(inv_inertia):
    return ArticulatedBodyInertia(inv_inertia.base_frame, np.linalg.inv(inv_inertia.mat))
setattr(ArticulatedBodyInertia,"inverse",inverse_of_inertia)
setattr(InverseArticulatedInertia,"inverse",inverse_of_inverse_inertia)


if __name__=="__main__":
    print "spatial vector test"
    W=Frame("world")
    B=Frame("body")
    p=FramePoint(W,np.array([[1.0,0.0,0.0]]).T)
    f=FrameVector(W,np.array([[0.0,1.0,0.0]]).T)
    sf=SpatialForceVector.from_point_force(p,f)
    Ia=ArticulatedBodyInertia(W,10.0*np.eye(6,6))
    print sf
    print Ia
    print Ia.inverse()
    print Ia.inverse().inverse()

class Transform(object):
    def __init__(self, base_frame, frame, 
        vec=np.array([0.0,0.0,0.0]), quat=Quaternion(1.0,0.0,0.0,0.0)):
        self.base_frame=base_frame
        self.frame=frame
        self.vec=np.array(vec)
        self.quat=quat.versor
        self.DCM=self.quat.DCM()
    @property 
    def forwardDCM(self):
        return self.DCM
    @forwardDCM.setter
    def forwardDCM(self,newDCM):
        dxx = newDCM[0,0]
        dxy = newDCM[0,1]
        dxz = newDCM[0,2]
        dyx = newDCM[1,0]
        dyy = newDCM[1,1]
        dyz = newDCM[1,2]
        dzx = newDCM[2,0]
        dzy = newDCM[2,1]
        dzz = newDCM[2,2]
        directionTest2=np.array([
            [(dxx + dyy + dzz), (dyz - dzy), (dzx - dxz), (dxy - dyx)],
            [(dyz - dzy), (dxx - dyy - dzz), (dyx + dxy), (dzx + dxz)],
            [(dzx - dxz), (dyx + dxy), (dyy - dxx - dzz), (dzy + dyz)],
            [(dxy - dyx), (dzx + dxz), (dzy + dyz), (dzz - dxx - dyy)]
            ])
        directionTest2 *= (1.0 / 3.0)
        eigs, eigvecs = np.linalg.eigh(directionTest2)
        l1=[(eigs[i],eigvecs[:,i]) for i in range(0,4)]
        l1.sort(key=lambda (x): -x[0])
        best=l1[0]
        q=best[1]
        ret = Quaternion(q[0],-q[1],-q[2],-q[3])
        self.quat=ret
        self.DCM=self.quat.DCM()

    @property
    def reverseDCM(self):
        return self.DCM.T
    @reverseDCM.setter
    def reverseDCM(self, value):
        self.forwardDCM=value.T

    @property
    def forwardQuat(self):
        return self.quat
    @forwardQuat.setter
    def forwardQuat(self, value):
        self.quat = value.versor
        self.DCM=self.quat.DCM()

    @property
    def reverseQuat(self):
        return self.quat.conjugate
    @reverseQuat.setter
    def reverseQuat(self, value):
        self.forwardQuat = value.conjugate
    
    

    def transform_vector(self,vec):
        Frame.check(self,vec)
        return FrameVector(self.base_frame, self.DCM.dot(vec.vec))

    def transform_point(self,point):
        Frame.check(self,point)
        return FramePoint(self.base_frame, self.vec+self.DCM.dot(point.vec))

    def __mul__(self, other):
        if isinstance(other, FramePoint):
            return transform_point(other)
        if isinstance(other, FrameVector):
            return transform_vector(other)
        if isinstance(other, Transform):
            return transform_transform(other)
        raise NotImplemented("Class untransformable %s "% type(other))

    def __repr__(self):
        return "[%s]{%s}:<<%.2f,%.2f,%.2f> + <%.2f,%.2f,%.2f>i + <%.2f,%.2f,%.2f>j + <%.2f,%.2f,%.2f>k>"%(
            self.base_frame,self.frame,
            self.vec[0],self.vec[1],self.vec[2],
            self.DCM[0,0],self.DCM[1,0],self.DCM[2,0],
            self.DCM[0,1],self.DCM[1,1],self.DCM[2,1],
            self.DCM[0,2],self.DCM[1,2],self.DCM[2,2])

from math import sin, cos
if __name__=="__main__":
    world=Frame("world")
    body=Frame("body")
    trans=Transform(world,body)
    for theta in np.linspace (-pi,pi,8):
        for phi in np.linspace( -pi,pi,8):
            for psy in np.linspace(-pi,pi,8):
                rz=np.array([
                    [cos(theta),-sin(theta),0.0],
                    [sin(theta),cos(theta),0.0],
                    [0.0,0.0,1.0]])
                ry=np.array([
                    [cos(phi),0,sin(phi)],
                    [0.0,1.0,0.0],
                    [-sin(phi),0,cos(phi)]])
                rx=np.array([
                    [1.0,0.0,0.0],
                    [0.0,cos(psy),-sin(psy)],
                    [0.0,sin(psy),cos(psy)]])
                mat=rz.dot(ry.dot(rx))
                trans.forwardDCM=mat
                diff = trans.forwardDCM-mat
                for r in range(0,3):
                    for c in range(0,3):
                        assert abs(diff[r,c])<1e-7
    print "hey, transform.forwardDCM=mat must work, because the assertions passed"
    theta=pi/2
    trans.forwardQuat=quat_exponent(0.0,0.0,theta)
    rz=np.array([
        [cos(theta),-sin(theta),0.0],
        [sin(theta),cos(theta),0.0],
        [0.0,0.0,1.0]])
    print rz
    print trans.forwardDCM


if __name__=="__main__":
    worldFrame=Frame("world")
    bodyFrame=Frame("body")
    print worldFrame
    w_a=FramePoint(worldFrame, np.array([[0.0,1.0,0.0]]).T)
    w_c=FrameVector(worldFrame, np.array([[1.0,0.0,0.0]]).T)
    b_b=FramePoint(bodyFrame, np.array([[2.3,3.0,0.2]]).T)
    b_d=FrameVector(bodyFrame, np.array([[2.3,3.0,0.2]]).T)
    b_e=FrameVector(bodyFrame, np.array([[2.0,10.0,0.0]]).T)
    print w_a,w_c,b_b
    try:
        w_a+b_b
    except NonsenseException as e:
        print "expected Exception:",e
    try:
        w_a+b_d
    except FrameException as e:
        print "expected Exception:",e
    w_d=w_a+w_c
    print w_d,type(w_d)
    b_f = b_d+b_e
    print b_f,type(b_f)



