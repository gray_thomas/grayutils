import numpy as np 
from gu.math import *
from collections import namedtuple
import hume_frames_and_poses as hf
from leds import new_const
from math import sqrt

HumeStateDeltaTuple=namedtuple("HumeStateDelta",
	['tvx','tvy','tvz','dvx','dvy','dvz','twx','twy','twz','dwx','dwy','dwz'])
class HumeStateDelta(HumeStateDeltaTuple):
	@classmethod
	def from_spatial_vectors(cls,v,a):
		v.base_frame.assert_match(hf.world)
		a.base_frame.assert_match(hf.world)
		return cls(v.x,v.y,v.z,a.x,a.y,a.z, v.rx, v.ry, v.rz, a.rx, a.ry, a.rz)

	@classmethod
	def from_vec(cls,vec):
		assert vec.shape==(12,1)
		return cls(*vec.reshape((-1,)))

	@property
	def tv(self):
	    return ThinSpatialVector.new_motion(hf.world, np.array([[self.tvx,self.tvy,self.tvz,self.twx,self.twy,self.twz]]).T)
	@property
	def dv(self):
	    return ThinSpatialVector.new_motion(hf.world, np.array([[self.dvx,self.dvy,self.dvz,self.dwx,self.dwy,self.dwz]]).T)

	def __init__(self,*args):
		super(HumeStateDelta,self).__init__(*args)

HumeStateTuple=namedtuple("HumeState",
	['x','y','z','vx','vy','vz','qw','qx','qy','qz','wx','wy','wz'])
class HumeState(HumeStateTuple):
	@classmethod
	def from_transform_motion(cls,T,v):
		T.base_frame.assert_match(hf.world)
		T.frame.assert_match(hf.body)
		v.base_frame.assert_match(hf.world)
		return cls(T.x,T.y,T.z,v.x,v.y,v.z,T.qw,T.qx,T.qy,T.qz, v.rx, v.ry, v.rz)
	
	@classmethod
	def from_vec(cls,vec):
		assert vec.shape==(13,1)
		return cls(*vec.reshape((-1,)))

	@classmethod
	def default(cls):
		return cls(0.0,0.0,0.0, 0.0,0.0,0.0, 1.0,0.0,0.0,0.0, 0.0,0.0,0.0)

	def __init__(self,*args):
		super(HumeState,self).__init__(*args)

	@property
	def p(self):
	    return np.array([[self.x,self.y,self.z]]).T
	@property
	def v_raw(self):
	    return np.array([[self.vx,self.vy,self.vz]]).T
	@property
	def q(self):
	    return Quaternion(self.qw,self.qx,self.qy,self.qz)
	@property
	def w(self):
	    return np.array([[self.wx,self.wy,self.wz]]).T

	@property
	def T(self):
		return Transform(hf.world, hf.body, vec=self.p, quat=self.q)
	@property
	def v(self):
	    return ThinSpatialVector.new_motion(hf.world, np.array([[self.vx,self.vy,self.vz,self.wx,self.wy,self.wz]]).T)

	def move(self,delta):
		T_new=self.T.move(delta.tv)
		v_new=self.v+delta.dv
		return HumeState.from_transform_motion(T_new, v_new)

class HumeKF(object):
	def __init__(self):
		self.delta_x=np.zeros((12,1))
		self.delta_covariance=1e1*np.eye(12,12)
		self.X=HumeState.default()
		self.constellation=new_const
		self.i=0.01
		
	def integrateNormalDynamics(self, spatial_acceleration_of_body_hat, dt):
		# incorporate previous delta into x
		# integrate x assuming zero acceleration
		# save the timestep
		# delta is now zero
		raise NotImplemented()

	def eventUpdate(self):
		# handle the impact
		raise NotImplemented()

	def measureIMU(self, imu):
		# accel component will change delta (velocity component)
		# uncertainty will have structure, since angular velocity
		# appears linearly in v
		# Note: covariance will include this structure. If we change
		# reference frames we may be able to improve the numerical
		# accuracy, but this will require a method to remove the 
		# dependence from the covariance matrix, which sounds involved.
		# It's not so bad. Use the linear transform between frames.
		# cov(T v) = T cov(v) T'
		raise NotImplemented()

	def measureLEDs(self, leds, selector):
		num_leds=sum([1 for s in selector if s])
		if num_leds<3:
			raise NotImplemented()
		R = np.zeros((3*num_leds,12))
		i=0
		for j in range(0,num_leds):
			if selector[j]:
				R[3*i:3*i+3,0:3]=np.eye(3,3)
				R[3*i,3:6]=self.constellation[:,j]
				R[3*i+1,6:9]=self.constellation[:,j]
				R[3*i+2,9:12]=self.constellation[:,j]
				i+=1
		information=R.T.dot(R)
		data=R.T.dot(leds.T.reshape((-1,1)))
		regularization_position=np.array([[0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0]]).T
		eigenvalues, eigenvector_matrix = np.linalg.eig(information)
		rep_mat=lambda x:np.array_repr(
			x,suppress_small=True,precision=3,max_line_width=260)

		s2p=sqrt(2)/2.0
		s2n=-s2p
		spin_mode_selector=np.array(np.bmat([[np.eye(3,3),np.zeros((3,9))],
									[np.bmat([[np.zeros((1,3)),np.array([[0.0,s2n,0.0,s2p,0.0,0.0,0.0,0.0,0.0]])],
									[np.zeros((1,3)),np.array([[0.0,0.0,s2p,0.0,0.0,0.0,s2n,0.0,0.0]])],
									[np.zeros((1,3)),np.array([[0.0,0.0,0.0,0.0,0.0,s2n,0.0,s2p,0.0]])]])]])).T
		scale_mode_selector=np.array(np.bmat([[np.eye(3,3),np.zeros((3,9))],
									[np.bmat([[np.zeros((1,3)),np.array([[1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]])],
									[np.zeros((1,3)),np.array([[0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0]])],
									[np.zeros((1,3)),np.array([[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0]])]])]])).T
		skew_mode_selector=np.array(np.bmat([[np.eye(3,3),np.zeros((3,9))],
									[np.bmat([[np.zeros((1,3)),np.array([[0.0,s2p,0.0,s2p,0.0,0.0,0.0,0.0,0.0]])],
									[np.zeros((1,3)),np.array([[0.0,0.0,s2p,0.0,0.0,0.0,s2p,0.0,0.0]])],
									[np.zeros((1,3)),np.array([[0.0,0.0,0.0,0.0,0.0,s2p,0.0,s2p,0.0]])]])]])).T
		print rep_mat(R)
		print rep_mat(R.T.dot(R))
		print rep_mat(data)
		print rep_mat(eigenvalues)
		print rep_mat(eigenvector_matrix)
		eigenvalues, eigenvector_matrix = np.linalg.eig(scale_mode_selector.T.dot(R.T.dot(R.dot(scale_mode_selector))))
		print rep_mat(eigenvalues)
		print rep_mat(eigenvector_matrix)
		eigenvalues, eigenvector_matrix = np.linalg.eig(skew_mode_selector.T.dot(R.T.dot(R.dot(skew_mode_selector))))
		print rep_mat(eigenvalues)
		print rep_mat(eigenvector_matrix)
		eigenvalues, eigenvector_matrix = np.linalg.eig(spin_mode_selector.T.dot(R.T.dot(R.dot(spin_mode_selector))))
		print rep_mat(eigenvalues)
		print rep_mat(eigenvector_matrix)




	def measureFootPoint(self, foot_in_body, foot_velocity_in_body):
		# use the fact that this point's velocity is expected to be low
		raise NotImplemented()

	def getEstimatedState(self):
		""" return a HumeState object """
		return self.X

	def getSigma(self):
		""" retun the state delta covariance matrix """
		return self.delta_covariance

if __name__=="__main__":
	kf=HumeKF()
	print  sqrt(kf.getSigma()[0,0])
	kf.measureLEDs(new_const[:,0:3],[True]*3+[False]*4)
	q1=Quaternion(1.0,0.0,0.0,0.0)
	print q1.DCM()
	w=quat_exponent(0,0,0.1)
	print np.zeros((12,1)).shape
	delta=HumeStateDelta.from_vec(np.zeros((12,1)))
	delta=HumeStateDelta.from_vec(np.array([[1.0,0.0,0.0,  0.0,0.0,0.0, 0.0,0.0,0.0, 0.0,0.0,0.0]]).T)
	base=HumeState.default()
	print base.move(delta)
	print delta.tvx
	print delta.tvy
	print delta.tvz
	print w
