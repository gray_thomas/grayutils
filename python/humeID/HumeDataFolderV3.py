import numpy as np

class HumeDataFolder(object):
    def __init__(self, folderLocation):
        self.folderLocation = folderLocation + "/";
        # self.test_names={
        #     "%d_x_phase_path.txt":"pathx%d",
        #     "%d_x_nx_phase_path.txt":"npathx%d",
        #     "%d_y_phase_path.txt":"pathy%d",
        #     "%d_y_nx_phase_path.txt":"npathy%d"
        # }
        self.names={
            # "grav_1.txt",
            # "grav_2.txt",
            # "Js_1.txt",
            # "Js_2.txt",
            # "U_1.txt",
            # "U_2.txt",
            # "Jt_1.txt",
            # "Jt_2.txt",
            # "Wint_1.txt",
            # "Wint_2.txt",
            # "Ainv_1.txt",
            # "Ainv_2.txt",
            "curr_conf.txt":['q'],
            # "gamma.txt":['gamma'],
            # "landing_loc.txt":[],
            # "phase.txt",
            # "Scenario.txt",
            "Body_Orientation.txt":['dcm'],
            "Body_Position.txt":['x'],
            # "Curr_Left_foot_force.txt",
            # "Left_foot_pos_des.txt",
            # "plan_yaw.txt",
            # "switching_time.txt",
            # "Curr_Right_foot_force.txt",
            # "Left_foot_pos.txt",
            # "rfoot_contact.txt",
            # "torque.txt",
            # "com_pos_pl.txt",
            # "curr_vel.txt",
            # "Left_foot_vel_des.txt",
            # "Right_foot_pos_des.txt",
            # "com_pos.txt",
            # "experiment_video.avi",
            # "Left_foot_vel.txt",
            # "Right_foot_pos.txt",
            # "com_vel.txt",
            # "filter_com_vel.txt",
            # "lfoot_contact.txt",
            # "Right_foot_vel_des.txt",
            "cpu_time.txt":['t'],
            # "foot_placement.txt",
            # "local_stance_foot.txt",
            # "Right_foot_vel.txt"
        }
        # WBC_names=["Js", "Jt", "U", "Ainv", "Wint", "grav"]
        # self.WBC_internal_names={}
        # for ind in [1,2]:
        #     for name in WBC_names:
        #         self.WBC_internal_names[name+"_"+str(ind)]=name+"_"+str(ind)+".txt"

        # for ind in [1,2]:
        #     self.alt_names["Jt_%d"%ind]=["jac%d"%ind,"task_jacobian%d"%ind]
        #     self.alt_names["Js_%d"%ind]=["bar_jac%d"%ind,"constrained_jacobian%d"%ind]
        #     self.alt_names["Wint_%d"%ind]=["internal_matrix_%d"%ind]
        #     self.alt_names["Grav_%d"%ind]=["gravity_vector_%d"%ind]
        #     self.alt_names["Ainv_%d"%ind]=["inverse_mass_inertia_matrix_%d"%ind]

        # self.alt_names["Jt_1"].append(["jac","task_jacobian","jacobian","jac_Xd_qd"])
        # self.alt_names["Js_1"].append(["bar_jac","constrained_jacobian","cjac"])
        #########################
        
        self.lookupVectorFileName={}
        # self['pos6']=self.data[:,3]
        self.memoized_data={}
        for filename in self.names.keys():
            alt_names=self.names[filename]
            for alt_name in alt_names:
                self.lookupVectorFileName[alt_name]=filename
                # setattr(HumeDataFolder, alt_name, self[alt_name])

    def __setitem__(self, item, value):
        self.__dict__[item]=value

    def __getitem__(self, key):
        try:
            return self.loadVector(self.lookupVectorFileName[key])
        except KeyError:
            return self.__dict__[key]

    def loadVector(self, fileName):
        print "loading ", fileName
        try:
            return self.memoized_data[fileName]
        except KeyError:
            data = np.loadtxt(self.folderLocation + fileName)
            self.memoized_data[fileName]=data
            return data
