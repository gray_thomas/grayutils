'''
Created on Jan 15, 2014

@author: hcrl
'''



from math import pi
from math import sqrt
from math import sin
from math import cos
from numpy import linalg as lin

from humeID.dataFetcher import HumeDataFetcher
from humeID.dataFetcher import HumeDataFetcher
import matplotlib.pyplot as plt
import numpy as np
from humeID.HumeRagdoll import HumeRagdoll
import matplotlib.animation as animation

class HumePlotter(object):
    def __init__(self):
        # self.dataSet=dataSet
        self.ragdoll=HumeRagdoll()

    def set_data_old(self, dataSet):
        self.phase = dataSet.getPhase()
        self.q = dataSet.getMeasuredJoints()
        self.foot_des = dataSet.getFootDesired()
        self.foot_pos = dataSet.getFootPosition()
        self.com_pos = dataSet.getCOMPosition()
        self.torque = dataSet.getMeasuredTorque()
        return self

    def set_data_new(self,dataSet):
        self.phase=dataSet.phase
        self.q=dataSet.q
        self.foot_des=dataSet.foot_des
        self.foot_pos=dataSet.foot_pos
        self.com_pos=dataSet.filtered_com_pos
        self.torque=dataSet.torque
        return self

    def plotDualContactRagDolls(self,data_indexes):
        plt.figure()
        phases=self.phase
        data=self.q
        for t in data_indexes:
            if (phases[t]==4):
                self.ragdoll.solveDualContactKinematics(data[t,1], data[t,2], data[t,4], data[t,5],data[t,9])
                self.ragdoll.drawMe()
#         plt.show()
    def plotRagDolls(self,data_indexes):
        plt.figure()
        phases=self.phase
        data=self.q
        for t in data_indexes:
            if (phases[t]==4):
                self.ragdoll.solveDualContactKinematics(data[t,1], data[t,2], data[t,4], data[t,5],data[t,9])
                self.ragdoll.drawMe()
            if (phases[t]==2):
                self.ragdoll.solveSingleContactKinematics(data[t,1], data[t,2], data[t,4], data[t,5],data[t,9])
                self.ragdoll.drawMe()
#         plt.show()
    def plotRagDollsPlus(self,data_indexes):
        plt.figure()
        phases=self.phase
        data=self.q
#         footDesired=self.foot_des
#         footActual=self.foot_pos
#         comPosition=self.com_pos
        ghostFootData=np.zeros((len(data_indexes),2))
        for (i,t) in enumerate(data_indexes):
            if (phases[t]==4):
                self.ragdoll.solveDualContactKinematics(data[t,1], data[t,2], data[t,4], data[t,5],data[t,9])
                self.ragdoll.drawMe()
                self.ragdoll.solveSingleContactKinematics(data[t,1], data[t,2], data[t,4], data[t,5],data[t,9])
                self.ragdoll.drawMeGhost()
                ghostFootData[i,:]=self.ragdoll.leftFootPoint
#                 plt.plot(footDesired[t,0],footDesired[t,1],'ko')
#                 plt.plot(footActual[t,0],footActual[t,1],'rx')
#                 plt.plot(comPosition[t,0],comPosition[t,1],'go')
#                 plt.plot(comPosition[t,0],comPosition[t,1],'k+')
            if (phases[t]==7 ):
                self.ragdoll.solveSingleContactKinematics(data[t,1], data[t,2], data[t,4], data[t,5],data[t,9])
                self.ragdoll.drawMeGhost()
                ghostFootData[i,:]=self.ragdoll.leftFootPoint
#                 plt.plot(footDesired[t,0],footDesired[t,1],'ko')
#                 plt.plot(footActual[t,0],footActual[t,1],'rx')
#                 plt.plot(comPosition[t,0],comPosition[t,1],'go')
#                 plt.plot(comPosition[t,0],comPosition[t,1],'k+')
        plt.plot(ghostFootData[:,0],ghostFootData[:,1],'c')
#         plt.show()
    def plotRagDollsWithCOM(self,data_indexes,x0,y0,thetas,z0):
        plt.figure()
        phases=self.phase
        data=self.q
        footDesired=self.foot_des
        footActual=self.foot_pos
        comPosition=self.com_pos
        for (i,t) in enumerate( data_indexes):
            self.ragdoll.solveSingleContactKinematics(data[t,1], data[t,2], data[t,4], data[t,5],data[t,9])
            self.ragdoll.drawMe()
            plt.plot(footDesired[t,0],footDesired[t,1],'ko')
            plt.plot(footActual[t,0],footActual[t,1],'rx')
            plt.plot(comPosition[t,0],comPosition[t,1],'go')
            plt.plot(comPosition[t,0],comPosition[t,1],'k+')
            xcom=self.ragdoll.hipPoint[0]+cos(thetas[i])*x0+-sin(thetas[i])*y0
            ycom=self.ragdoll.hipPoint[1]+sin(thetas[i])*x0+cos(thetas[i])*y0
            plt.plot(xcom,ycom,'go')
            plt.plot(xcom,ycom,'k+')
        plt.plot([-1.0,1.0],[z0,z0],'b')
#         plt.show()
    
    def plotRagDollsAssumingDualContact(self,data_indexes):
        plt.figure()
        phases=self.phase
        data=self.q
        for t in data_indexes:
            self.ragdoll.solveDualContactKinematics(data[t,1], data[t,2],  data[t,4], data[t,5],data[t,9])
            self.ragdoll.drawMe()
#         plt.show()

    def generate_figure(self):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, autoscale_on=False, xlim=(-2, 2), ylim=(-0, 1.5), aspect='equal')
        self.ax.grid()
        return self.fig

    def animateRagdolls(self,data_indexes):
        self.generate_figure()
        self.data_indexes=data_indexes
        t=data_indexes[0]
        data=self.q
        self.ragdoll.solveSingleContactKinematics(data[t,1], data[t,2],  data[t,4], data[t,5], data[t,9])
        self.ragdoll.drawMe(self.ax)
        self.anim=animation.FuncAnimation(self.fig, self._animRagdoll_dual_contact, 
            np.arange(1, len(self.data_indexes)), interval=50, blit=True)

    def _animRagdoll_dual_contact(self,i):
        print "hey", i
        try:
            t=self.data_indexes[i]
            data=self.q[t,:]
            print data
            self.ragdoll.solveSingleContactKinematics(data[7], data[8], data[10], data[11], data[5])
            res=self.ragdoll.animateMe()
        except Exception as e:
            print e
        finally:
            print "sup"
        return res

    def plotPosGraphs(self,data_indexes):
        plt.figure()
        data=self.q
        for j in range (0,6):
            plt.subplot(6,1,j+1)
            plt.plot(data_indexes,data[data_indexes,j])
                
#         plt.show()
    def plotOriGraphs(self,data_indexes):
        plt.figure()
        data=self.q
        for j in range (0,3):
            plt.subplot(3,1,j+1)
            plt.plot(data_indexes,data[data_indexes,j+9])
                
#         plt.show()
    def plotDesGraphs(self,data_indexes):
        plt.figure()
        data=self.torque
        for j in range (0,6):
            plt.subplot(6,1,j+1)
            plt.plot(data_indexes,data[data_indexes,j])
                
#         plt.show()
    def plotPhaseGraph(self,data_indexes):
        plt.figure()
        phase=self.phase
        plt.plot(data_indexes,phase[data_indexes])
#         plt.show()
    def plotComparisonPositionGraphs(self,data_indexes):
        plt.figure()
        joints=self.q
        des=self.torque
        for j in range (0,6):
            plt.subplot(6,1,j+1)
            plt.plot(data_indexes,joints[data_indexes,j])
            plt.plot(data_indexes,des[data_indexes,j])
                
#         plt.show()
if __name__ == '__main__':
    fetch=HumeDataFetcher()
    # dat=fetch.getDataSet(2)
    # for i in range (7,8):
    #     HumePlotter().set_data_old(fetch.getDataSet(i)).plotRagDollsPlus(range(0000,1700,2))
    # 
    data=fetch.get_most_recent_data()
    print data.t
    print data.t.shape
    HumePlotter().set_data_new(data).animateRagdolls(range(0000,len(data.t),10))
    print "hey"
    plt.show()

    # plotter=HumePlotter(dat)

#     plotter.plotRagDollsAssumingDualContact(range(500,1500,10))
#     plotter.plotOriGraphs(range(500,1500))
#     plotter.plotComparisonPositionGraphs(range(500,1500))
#     plotter.plotPhaseGraph(range(500,1500))

        