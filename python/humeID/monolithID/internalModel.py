import matplotlib.pyplot as plt
import numpy as np
import math
def estimateInternalModel(data, axs, to, tf, nConv, convolutionKernel, convolvedWeighting, convolvedMotorAccel, convolvedVelocity):
    cv = lambda(x):np.convolve(x, convolutionKernel, mode='same')[nConv:-nConv]
    signum = lambda(xs):np.array([1.0 if x > 0 else -1.0 for x in xs]).reshape((-1,))
    convolvedR = np.concatenate((np.ones((tf - to, 1)),
                      cv(data[(to - nConv):(tf + nConv), 0]).reshape((-1, 1)),
                      cv(data[(to - nConv):(tf + nConv), 3]).reshape((-1, 1)),
#                       signum(convolvedVelocity).reshape(-1,1),
                      cv(signum(data[(to - nConv):(tf + nConv), 1] - data[(to - nConv - 1):(tf + nConv - 1), 1])).reshape((-1, 1)),
                      convolvedVelocity.reshape(-1,1)
                      ), axis=1)
    vThresh = 60.0
    velocityThresholdWeighting = np.array([1.0 if abs(v) > vThresh else 0.0 for v in convolvedVelocity]).reshape((-1, 1))
    weighting = np.multiply(velocityThresholdWeighting, convolvedWeighting.reshape((-1,1)))
    koConvR = np.array(convolvedR)
    for col in range(0, koConvR.shape[1]):
        koConvR[:, [col]] = np.multiply(convolvedR[:, [col]], weighting)
    weightedData = convolvedMotorAccel.reshape((-1, 1))
    weightedData = np.multiply(weightedData, weighting).reshape(-1, 1)
    sol1 = np.linalg.solve(convolvedR.T.dot(koConvR), convolvedR.T.dot(weightedData))
    print "model estimate: constantAccel=%f, currentConstant=%f, springConstant=%f, coulombFriction=%f, damping=%f" % (
                                            sol1[0], sol1[1], sol1[2], sol1[3], sol1[4])
    expectedAccel1=convolvedR.dot(sol1)
    axs[5, 0].plot(expectedAccel1, 'c')
    error=expectedAccel1.reshape((-1,)) - convolvedMotorAccel.reshape((-1,))
    scaledError=error * 100.0 / (max(convolvedMotorAccel) - min(convolvedMotorAccel))
   
    axs[5, 1].plot(scaledError, 'b')
    axs[5, 1].plot(np.multiply(scaledError.reshape((-1,1)),weighting),'r')
    axs[5, 1].yaxis.set_major_formatter(plt.FormatStrFormatter('%.1f%%'))
    plt.figure(2)
    plt.hold(True)
    error=np.multiply(error,convolvedWeighting)
    dt=0.0005 # seconds
    Nsamps=error.shape[0]
    freq=np.linspace(0,1.0/dt,Nsamps)
    plt.loglog(freq,[math.sqrt(z.conjugate()*z)*(2.0/Nsamps) for z in np.fft.fft(error)],'b.')
    plt.loglog([freq[Nsamps/2],freq[Nsamps/2]],[0.0001,1],'k')
    plt.gca().set_xlabel("Frequency, Cps");
    plt.gca().set_ylabel("Model prediction error magnitude, encoder ticks per squared controller timestep");
    
    return sol1