import matplotlib.pyplot as plt
from linearUtils.convolution import setupConvolutionFilters
from humeID.monolithID import estimateSensorsSimple, estimateInternalModel, estimateExternalModel
from humeID.monolithID import identifyEquilibriumEncoderValues
import numpy as np

def plotFancySharedX(data, to=20218, tf=300000 + 20218):
    fig, axs = plt.subplots(8, 2, sharex=True, sharey=False)
    axs[0, 0].set_title(r"HumeData, Right Knee Joint")
    axs[0, 1].set_title(r"Estimator Errors as Percentage of Variance")
    
    plt.setp(axs[0, 0].plot(data[to:tf, 0], "b-"), linewidth=2.0)
    axs[0, 0].set_ylabel("Input\nCurrent\nDAC\nValue", rotation="horizontal")
    plt.setp(axs[1, 0].plot(data[to:tf, 1], "b-"), linewidth=2.0)
    axs[1, 0].set_ylabel("Motor\nEncoder\nInteger\nValue", rotation="horizontal")
    plt.setp(axs[2, 0].plot(data[to:tf, 2], "b-"), linewidth=2.0)
    axs[2, 0].set_ylabel("Joint\nEncoder\nInteger\nValue", rotation="horizontal")

    nConv, convolutionKernel, convolvedWeighting, diffKernel, ddifKernal= setupConvolutionFiltersOld(data, axs, to, tf)
    
    convolvedMotorVelocity = np.convolve(data[to - nConv:tf + nConv, 1], diffKernel, mode='same')[nConv:-nConv]
    convolvedMotorAcceleration = np.convolve(data[to - nConv:tf + nConv, 1], ddifKernal, mode='same')[nConv:-nConv]
    convolvedJointVelocity = np.convolve(data[to - nConv:tf + nConv, 2], diffKernel, mode='same')[nConv:-nConv]
    convolvedJointAcceleration = np.convolve(data[to - nConv:tf + nConv, 2], ddifKernal, mode='same')[nConv:-nConv]
    axs[4, 0].set_ylabel("Motor\nEncoder\nVelocity", rotation="horizontal")
    axs[5, 0].set_ylabel("Motor\nEncoder\nAcceleration", rotation="horizontal")
    axs[6, 0].set_ylabel("Joint\nEncoder\nVelocity", rotation="horizontal")
    axs[7, 0].set_ylabel("Joint\nEncoder\nAcceleration", rotation="horizontal")
    axs[4, 0].plot(convolvedMotorVelocity, "m")
    axs[5, 0].plot(convolvedMotorAcceleration, "m")
    axs[6, 0].plot(convolvedJointVelocity,'m')
    axs[7, 0].plot(convolvedJointAcceleration,'m')
    
    
    alphas=estimateSensorsSimple(data, axs, to, tf, nConv, convolutionKernel, convolvedWeighting)
    
    betas=estimateInternalModel(data, axs, to, tf, nConv, convolutionKernel, 
                                          convolvedWeighting, convolvedMotorAcceleration, convolvedMotorVelocity)
    gammas=estimateExternalModel(data, axs, to, tf, nConv, convolutionKernel, convolvedWeighting, convolvedJointAcceleration, 
                          convolvedJointVelocity)
    zeros=identifyEquilibriumEncoderValues(alphas,betas,gammas)
#     A,B,C,D = generateLinearPlantModel(alphas, betas, gammas, zeros)
#     print A,B,C,D
#     axs[1,0].plot([to,tf],np.array([1,1])*zeros[0])
#     axs[2,0].plot([to,tf],np.array([1,1])*zeros[1])
#     axs[3,0].plot([to,tf],np.array([1,1])*zeros[2])
    for i in range(0, 8):
        axs[i, 0].yaxis.set_label_coords(-0.15, 0.5)
        axs[i, 1].yaxis.set_label_coords(1.15, 0.5)
    axs[5, 0].set_xbound(0, tf - to)
     
    axs[5, 0].xaxis.set_major_formatter(plt.FormatStrFormatter('%d'))
     
    axs[5, 0].set_xlabel(r"Raw Controller Time Steps")
    return data