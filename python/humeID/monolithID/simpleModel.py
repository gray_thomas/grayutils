import matplotlib.pyplot as plt
import numpy as np
def estimateSensorsSimple(data, axs, to, tf, nConv, convolutionKernel, convolvedWeighting):    


#     angulize=lambda(x):2*math.pi*((x%4092)/4092.0)
    clampLin=lambda(x,low,hi):0.0 if x<low else 0.0 if x>hi else (x-low*1.0)/(1.0*hi-low)*2 if x-low<hi-x else (hi*1.0-x)/(hi*1.0-low)*2 
#     x=1
#     low=0
#     hi=2
#     assert clampLin((1,0,2))==1
#     assert clampLin((0,0,2))==0
#     assert clampLin((2,0,2))==0
#     assert clampLin((1.5,0,2))==0.5
#     assert clampLin((0.5,0,2))==0.5
#     assert clampLin((0.75,0,2))==0.75
#     assert clampLin((1.75,0,2))==0.25
#     assert clampLin((1.25,0,2))==0.75
    nLin=5
    vals=np.linspace(4250,8000,nLin+2)
    
    Rlin=np.zeros((tf-to,nLin))
    for i in range(0,nLin):
        Rlin[:,i]=np.array([clampLin((dat,vals[i],vals[i+2]))for dat in data[to:tf,2]])
#     plt.show()
    R = np.concatenate((np.ones((tf - to, 1)),
                    data[to:tf, [1]],
                    data[to:tf, [2]],
                     
                    Rlin
                      ), axis=1)

#     convolvedWeighting=np.multiply(convolvedWeighting,extremeJointAngles.reshape((-1,)))# don't do this.
    koConvR = np.array(R)
    for col in range(0, R.shape[1]):
        koConvR[:, col] = np.multiply(R[:, col], convolvedWeighting)
    weightedData = np.multiply(data[to:tf, [3]], convolvedWeighting.reshape((-1, 1)))
    weightedData = np.convolve(weightedData.reshape(-1,), convolutionKernel, mode='same').reshape(-1, 1)
    sol = np.linalg.solve(R.T.dot(koConvR), R.T.dot(weightedData))
    vals=[0,2,3]
    solDumb=np.linalg.solve(R[:,vals].T.dot(koConvR[:,vals]),R[:,vals].T.dot(weightedData))
    
    
    maxSpringEncoder = max(data[to:tf, 3]) * 1.0
    minSpringEncoder = min(data[to:tf, 3]) * 1.0

    
    convolvedData3 = np.convolve(data[(to - nConv):(tf + nConv), 3], convolutionKernel, mode='same')[nConv:-nConv]
    plt.setp(axs[3, 0].plot(convolvedData3, "k-"), linewidth=1.0)
    plt.setp(axs[3, 0].plot(R.dot(sol), "m"), linewidth=1.0)
    plt.setp(axs[3, 0].plot(R.dot(sol), "c"), linewidth=1.0)
    plt.setp(axs[3, 0].plot(data[to:tf, 3], "b-"), linewidth=2.0)
    axs[3, 0].set_ylabel("Spring\nEncoder\nInteger\nValue", rotation="horizontal")
    rawError = (R.dot(sol) - data[to:tf, [3]])
    smoothError = R.dot(sol) - convolvedData3.reshape((-1, 1))
    rawError = np.multiply(rawError, convolvedWeighting.reshape((-1, 1)))
    smoothError = np.multiply(smoothError, convolvedWeighting.reshape((-1, 1)))
    errorVariance = rawError.T.dot(rawError) * 1.0 / (rawError.shape[0] - 1.0)
    print "spring silly alphas: [%f, %f, %f]" % (
                solDumb[0],solDumb[1],solDumb[2])
    print "spring simple alphas: [%f, %f, %f, %f, %f, %f]" % (
                sol[0],sol[1],sol[2],sol[3],sol[4],sol[5])
    print errorVariance
    plt.setp(axs[3, 1].plot(rawError * 100 / (maxSpringEncoder - minSpringEncoder), "b"), linewidth=2.0)
    plt.setp(axs[3, 1].plot(smoothError * 100 / (maxSpringEncoder - minSpringEncoder), "r"), linewidth=2.0)
    axs[3, 1].yaxis.set_major_formatter(plt.FormatStrFormatter('%.1f%%'))
    plt.figure(3)
    plt.plot(data[to:tf,2],np.multiply(convolvedWeighting.reshape((-1, 1)),R[:,vals].dot(solDumb) - data[to:tf, [3]]),'g.')
#     plt.plot(data[to:tf,2],np.multiply(convolvedWeighting.reshape((-1, 1)),(R[:,0:6].dot(sol[0:6]) - data[to:tf, [3]])),'m.')
    plt.plot(data[to:tf,2],np.multiply(convolvedWeighting.reshape((-1, 1)),(R.dot(sol) - data[to:tf, [3]])),'r.')
    plt.plot(data[to:tf:100,2],10.0*Rlin[::100,:],'.')
    plt.title("Nonlinearity in the encoders")
    plt.xlabel("Joint encoder measurement")
    plt.ylabel("Signed spring encoder prediction error.")
    plt.legend(["simple, linear model","model with delay term and basis function non-linearity compensation"])
    
    return sol