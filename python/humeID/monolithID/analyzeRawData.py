from humeID.HumeDataFolder import HumeDataFolder
from humeID import GRAY_UTILS
from humeID.monolithID.plots import plotFancySharedX
import numpy as np
import matplotlib.pyplot as plt
from humeID.dataFetcher import saveTrainingSet
data=np.loadtxt(GRAY_UTILS+"/../humeData/"+"sweepTest5.txt")
to = 23660  # sweepTest5
tf = 530000  # sweepTest5
def main():
    print "hello world"
    #     data=np.loadtxt(MEKA+"/m3hume_monolith/old_logs/secondFrequencySweepTest.txt")
#     data=np.loadtxt(MEKA+"/m3hume_monolith/old_logs/sweepTest3.txt")
#     data=np.loadtxt(MEKA+"/m3hume_monolith/old_logs/sweepTest4.txt")
#     data=np.loadtxt(MEKA+"/m3hume_monolith/old_logs/"+"sweepTest5.txt")

#     fig=plt.figure(3)
#     ax = fig.add_subplot(111, projection='3d')
#     dec=200
#     mavg=np.average(data[to:tf:dec,1])
#     javg=np.average(data[to:tf:dec,2])
# #     spring normal: const: 68161.326650, m z^-1: -0.164309, m: 0.382991+-0.000068*j, j: -24.141210+0.001791*j
# #     spring extreme: const: -139491.629817, m z^-1: 0.039308, m: -0.562092+41.133638*j, j: 0.000103+-0.002688*j
#     ax.scatter(data[to:tf:dec,2],
#                     (68161.326650+
#                     -24.141210*data[to:tf:dec,2]+
#                     0.001791*np.multiply(data[to:tf:dec,2],data[to:tf:dec,2])+
#                     0.382991*data[to:tf:dec,1]+
#                     -0.000068*np.multiply(data[to:tf:dec,1],data[to:tf:dec,2])+
#                     -0.164309*data[(to-1):(tf-1):dec,1]),
#                     data[to:tf:dec,3],c='g')
#     
#     plt.figure(4)
#     plt.plot(data[to:tf,2],data[to:tf,3]-(68161.326650+
#                             -24.141210*data[to:tf,2]+
#                     0.001791*np.multiply(data[to:tf,2],data[to:tf,2])+
#                     0.382991*data[to:tf,1]+
#                     -0.000068*np.multiply(data[to:tf,1],data[to:tf,2])+
#                     -0.164309*data[(to-1):(tf-1),1]))
#     plt.plot(data[to:tf,2],data[to:tf,3]-(-139491.629817+
#                             0.000103*data[to:tf,2]+
#                     -0.002688*np.multiply(data[to:tf,2],data[to:tf,2])+
#                     -0.562092*data[to:tf,1]+
#                     41.133638*np.multiply(data[to:tf,1],data[to:tf,2])+
#                     0.039308*data[(to-1):(tf-1),1]),'g')
#     ax.scatter(data[to:tf:100,1],data[to:tf:100,2],data[to:tf:100,3])
    
    varianceTraining = data[0:to, 1:4]
    avg = np.reshape(np.average(varianceTraining, axis=0), (1, -1))
    var = 1.0 / (varianceTraining.shape[0] - 1.0) * (varianceTraining - avg).T.dot(varianceTraining - avg)
    
    print var
    plotFancySharedX(data, to, tf)
    print "post plotFancySharedX"
    saveTrainingSet("sweepTest5.trn", data[to:tf, 1].reshape((-1, 1)), data[to:tf, 2:4])
    plt.show()
    
if __name__ == '__main__':
    main()