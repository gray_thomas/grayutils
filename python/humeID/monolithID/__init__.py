print "hello humeID.monolithID"
from simpleModel import *
from internalModel import *
from externalModel import *
def identifyEquilibriumEncoderValues(alphas,betas,gammas):
    z3=-betas[0]/betas[2]
    z2=(-gammas[0]-gammas[1]*z3)/gammas[2]
    z1=(alphas[0]-z3+alphas[3]*z2+alphas[5]*z2*z2)/(-alphas[1]-alphas[2]-alphas[4]*z2)
    return [z1,z2,z3]