import numpy as np
from math import sin, cos
from gu.math.Quaternion import *
from gu.math.transforms import *

 # parts of hume:
 # bod-body
 # abd-abductor link
 # tgh-femur, thigh
 # clf-shank, calf


def rotX(ang):
    return np.array([[1,0,  0],[0,cos(ang),-sin(ang)],[0,sin(ang),cos(ang)]])
def rotY(ang):
    return np.array([[cos(ang), 0,sin(ang)], [0,1,0],[-sin(ang),0,cos(ang)]])
def rotZ(ang):
    return np.array([[cos(ang), -sin(ang),0.0],[sin(ang), cos(ang),0.0],[0.0,0.0,1.0]])

world=Frame('world')


class Joint(object):
    def __init__(self, offset):
        self.DCM=np.eye(3)
        self.offset=offset
        self.parent=None
        self.child=None
    def setX(self, value):
        self.transAB.forwardDCM=rotX(value)
    def setY(self, value):
        self.transAB.forwardDCM=rotY(value)
    def setZ(self, value):
        self.transAB.forwardDCM=rotZ(value)
    def connect(self, linkA, linkB):
        linkA.children.append((self,linkB))
        linkB.parent=(self,linkA)
        linkB.link_parent=linkA
        linkB.joint_parent=self
        self.parent=linkA
        self.transAB=Transform(linkA.frame, linkB.frame, vec=self.offset)

class Link(object):
    def __init__(self,inertia_properties,name):
        self.name=name 
        self.frame=Frame(name)
        self.transWL=Transform(world, self.frame)
        self.mass_props=inertia_properties
        self.children=[]
        self.link_parent=None
        self.joint_parent=None
    def recursive_update_child_frames(self):
        if self.link_parent==None:
            pass
        else:
            self.transWL = self.link_parent.transWL * self.joint_parent.transAB
            # print self.transWL
        for c_joint, child in self.children:
            child.recursive_update_child_frames()
    def get_com(self):
        return (self.transWL*FramePoint(self.frame,self.mass_props.com)).vec



class InertiaProps(object):
    def __init__(self):
        pass


bod = InertiaProps()
abd = InertiaProps()
tgh = InertiaProps()
clf = InertiaProps()

bod.com = np.array([[-0.01, 0.0, 0.32]]).T 
abd.com = np.array([[-34.16, 0.0, 42.93]]).T*1e-3
tgh.com = np.array([[-3.96, 0 , -147.77]]).T*1e-3
clf.com = np.array([[0.0, 0.0, -.25]]).T

bod.mass = 10.61
abd.mass = 2.1656
tgh.mass = 2.66
clf.mass = .20

bod.inertia = np.array([
    [(1/12)*bod.mass*(.5**2+.3**2), 0, 0], 
    [0, (1/12)*bod.mass*(.5**2+.14**2), 0], 
    [0, 0, (1/12)*bod.mass*(.3**2+.14**2)]])

abd.inertia = np.array([[3734834, -91070, -302016], [-91070, 13912108, 19618.8], [-302016, 19618.8, 13551856]])*1e-9 # meka's secret numbers

tgh.inertia = np.array([[38216989, -20214, -3000316], [-20214, 39835025, 57353], [-3000316, 57353, 4558825]])*1e-9 # meka's secret numbers

Radius = 1.05*10**(-2)
h = .45
clf.inertia = np.array([
    [(1/12)*clf.mass*h**2+(1/4)*clf.mass*Radius**2, 0, 0], 
    [0, (1/12)*clf.mass*h**2+(1/4)*clf.mass*Radius**2, 0], 
    [0, 0, (1/2)*clf.mass*Radius**2]])

class HumeRagdoll3D(object):
    def __init__(self):
        self.lz6 = 0
        self.hip_separation = 279.4 * 1e-3
        self.lz7 = -0.3535e-1 # drop down for forward hip joint from abduction joint
        self.lx8 = 0.181e-1 # knee is slightly forward at zero position
        self.lz8 = -0.45 # femur length drop
        self.lz9 = -0.4575 # shank length drop

        body=Link(bod,'Body')
        rabd=Link(abd,'Right Abductor Link')
        rtgh=Link(tgh,'Right Thigh')
        rclf=Link(clf,'Right Calf')
        labd=Link(abd,'Left Abductor Link')
        ltgh=Link(tgh, 'Left Thigh')
        lclf=Link(clf, 'Left Calf')

        self.bodies=[body, rabd, rtgh, rclf, labd, ltgh, lclf]
        self.root=body
        
        j0=Joint(np.array([[0.0,-self.hip_separation*0.5,0.0]]).T)
        j0.set=j0.setX
        j0.connect(body,rabd)

        j1=Joint(np.array([[0.0,0.0,self.lz7]]).T)
        j1.set=j1.setY
        j1.connect(rabd,rtgh)

        j2=Joint(np.array([[self.lx8,0.0,self.lz8]]).T)
        j2.set=j2.setY
        j2.connect(rtgh,rclf)


        j3=Joint(np.array([[0.0,self.hip_separation*0.5,0.0]]).T)
        j3.set=j3.setX
        j3.connect(body,labd)
        
        j4=Joint(np.array([[0.0,0.0,self.lz7]]).T)
        j4.set=j4.setY
        j4.connect(labd,ltgh)

        j5=Joint(np.array([[self.lx8,0.0,self.lz8]]).T)
        j5.set=j5.setY
        j5.connect(ltgh,lclf)
        self.joints=[j0,j1,j2,j3,j4,j5]

    def set_joints(self,q):
        for i in range(0,len(self.joints)):
            self.joints[i].set(q[i])

    def get_com(self):
        self.root.recursive_update_child_frames()
        mass_sum=0
        for body in self.bodies:
            mass_sum+=body.mass_props.mass
        
        com=np.array([[0,0,0.0]]).T
        for body in self.bodies:
            com+=body.get_com()*body.mass_props.mass/mass_sum
        return com

if __name__=="__main__":
    from HumeDataFolderV3 import HumeDataFolder
    import matplotlib.pyplot as plt
    hume=HumeRagdoll3D()
    path='/home/gray/wk/Hume_Experiment/20150501_16_15_14'
    dat=HumeDataFolder(path)
    qs = dat['q'][:,6:]
    coms = np.zeros((3,qs.shape[0]))
    for i in range(0,qs.shape[0]):
        hume.set_joints(qs[i,:])
        coms[:,[i]] = hume.get_com()
    np.savetxt(path+'/com.txt',coms.T)
    plt.plot(coms.T)
    plt.show()
    
