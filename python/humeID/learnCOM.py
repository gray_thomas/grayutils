'''
Created on Jan 15, 2014

@author: Gray Thomas
'''
from humeID.dataFetcher import HumeDataFetcher
if __name__ == '__main__':
    dataFetch=HumeDataFetcher()
    dataSet=dataFetch.getDataSet(2)
    jointData=dataSet.getMeasuredJoints()
    assert (-0.0886==jointData[0,0])   
    assert (0.4437==jointData[0,1])
    assert (-1.6227==jointData[0,2])
    assert(0.0249==jointData[0,3])
    assert(0.9369==jointData[0,4])
    assert(-2.1615==jointData[0,5])
    assert(0 ==jointData[0,6])
    assert(0 ==jointData[0,7])
    assert(0 ==jointData[0,8])
    assert(-0.0066==jointData[0,9])
    assert(-0.0004==jointData[0,10])
    assert(-0.0926==jointData[0,11]) 
    print jointData