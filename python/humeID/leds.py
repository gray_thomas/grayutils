import numpy as np 
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import Axes3D
default_constellation=np.array(
	[[-585.411011, -588.638977, -605.916382, -739.386719, -716.418762, -442.641052, -457.334229] ,
	[1349.616577, 1615.051514, 1287.638550, 1470.277588, 1505.585449, 1497.346924, 1474.048584], 
	[2144.938965, 1959.498047, 1893.115479, 1894.559692, 2061.909424, 2037.680054, 1873.852905] ])
default_constellation = 0.001*default_constellation
print np.sum(default_constellation, axis=1)
print 1.0/7.0*np.diag(np.sum(default_constellation, axis=1)).dot(np.ones((3,7)))



# This was hand measured, in inches.
new_const=np.array([
	[-3.0,0.0,18.5],
	[1.75,0.0,20.25],
	[3.0,0.0,9.5],
	[3.0,5.75,16.0],
	[-3.0,5.75,16.0],
	[-3.0,-5.75,16.0],
	[3.0,-5.75,16.0]
	]).T * 0.0254 # to put it in meters



if __name__=="__main__":
	default_constellation= default_constellation-1.0/7.0*np.diag(np.sum(default_constellation, axis=1)).dot(np.ones((3,7)))
	fig=plt.figure()
	ax=fig.add_subplot(111,projection='3d',aspect='equal')

	# X=default_constellation[0,:]
	# Y=default_constellation[1,:]
	# Z=default_constellation[2,:]
	X=new_const[0,:]
	Y=new_const[1,:]
	Z=new_const[2,:]

	max_range = np.array([X.max()-X.min(), Y.max()-Y.min(), Z.max()-0.0]).max() / 2.0
	mean_x = (X.max()+X.min())*0.5
	mean_y = ( Y.max()+Y.min())*0.5
	mean_z = (Z.max()+0.0)*0.5
	ax.plot(X,Y,"ro",zs=Z)
	ax.plot([0.0,0.1],[0.0,0.0],'r',zs=[0.0,0.0])
	ax.plot([0.0,0.0],[0.0,0.1],'g',zs=[0.0,0.0])
	ax.plot([0.0,0.0],[0.0,0.0],'b',zs=[0.0,0.1])
	ax.set_xlim(mean_x - max_range, mean_x + max_range)
	ax.set_ylim(mean_y - max_range, mean_y + max_range)
	ax.set_zlim(mean_z - max_range, mean_z + max_range)
	ax.set_xlabel("x")
	ax.set_ylabel('y')
	ax.set_zlabel('z')

	plt.show()