import numpy as np 
from math import pow, sqrt, pi
from collections import namedtuple
from gu.math import Quaternion, quat_exponent, cross
from gu.math.frames import *
from gu.math.thin_spatial_algebra import *

if __name__=="__main__":
    a = Quaternion(1.0,2.0,3.0,4.0)
    print a, a.norm
    print a.normalize()
    print a.normalize().DCM()

class FramePoint(object):
    def __init__(self, base_frame, vec):
        self.base_frame=base_frame
        assert vec.shape==(3,1)
        self.vec = vec

    def add_vector(self,other):
        Frame.check(self,other)
        return FramePoint(self.base_frame, self.vec+other.vec)
        
    def __add__(self, other):
        if isinstance(other, FramePoint):
            raise NonsenseException("Points are never added. Perhaps you meant to add a point and a vector?")
        if isinstance(other, FrameVector):
            return self.add_vector(other)
        raise NotImplemented("Class un-addable to FramePoint objects: %s "% type(other))

    # def __sub__(self,other):


    def __repr__(self):
        return "[%s](%.5f,%.5f,%.5f)"%(self.base_frame.name,self.vec[0,0],self.vec[1,0],self.vec[2,0])

class FrameVector(object):
    def __init__(self, base_frame, vec):
        self.base_frame=base_frame
        assert vec.shape==(3,1)
        self.vec=vec
    def add_vector(self,other):
        Frame.check(self,other)
        return FrameVector(self.base_frame, self.vec+other.vec)
    def add_point(self,other):
        Frame.check(self,other)
        return FramePoint(self.base_frame, self.vec+other.vec)
    def cross(self, other):
        Frame.check(self,other)
        return FrameVector(self.base_frame, cross(self.vec).dot(other.vec) )
    def __add__(self,other):
        if isinstance(other, FramePoint):
            return self.add_point(other)
        if isinstance(other, FrameVector):
            return self.add_vector(other)
        raise NotImplemented("Class un-addable to FrameVector objects: %s "% type(other))
    def __repr__(self):
        return "[%s]<%.5f,%.5f,%.5f>"%(self.base_frame.name,self.vec[0,0],self.vec[1,0],self.vec[2,0])


class SpatialForceVector(object):
    def __init__(self, base_frame, v, w):
        self.base_frame=base_frame
        assert v.shape==(3,1)
        assert w.shape==(3,1)
        self.v=v
        self.w=w
    @classmethod
    def from_point_force(cls, point, force):
        Frame.check(point,force)
        return cls(point.base_frame, force.vec, cross(point.vec).dot(force.vec))
    @property 
    def vec(self):
        return np.array([[self.v[0],self.v[1],self.v[2],self.w[0],self.w[1],self.w[2]]]).T
    def __repr__(self):
        return "[%s][*[%.2f,%.2f,%.2f -- %.2f,%.2f,%.2f]*]" %(self.base_frame, self.v[0], self.v[1], self.v[2], self.w[0], self.w[1], self.w[2])
    @classmethod
    def from_vec(cls, base_frame, vec):
        assert vec.shape==(6,1)
        return cls(base_frame, vec[0:3,:], vec[3:6,:])

class SpatialMotionVector(object):
    def __init__(self, base_frame, v, w):
        self.base_frame=base_frame
        self.v=v
        self.w=w
    @property 
    def vec(self):
        return np.array([[self.v[0],self.v[1],self.v[2],self.w[0],self.w[1],self.w[2]]]).T
    def __repr__(self):
        return "[%s][[%.2f,%.2f,%.2f -- %.2f,%.2f,%.2f]]" %(self.base_frame, self.v[0], self.v[1], self.v[2], self.w[0], self.w[1], self.w[2])

class ArticulatedBodyInertia(object):
    def __init__(self, base_frame, mat):
        self.base_frame=base_frame
        assert mat.shape==(6,6)
        self.mat=mat
    def __mul__(self,other):
        if not isinstance(other, SpatialMotionVector):
            raise NotImplemented("only valid for motion vectors")
        Frame.check(self,other)
        return SpatialForceVector(self.base_frame, self.mat.dot(other.vec))
    def __repr__(self):
        return "ArticulatedInertia [%s]:\n"%self.base_frame+str(self.mat)+"\nEnd ArticulatedInertia"

class InverseArticulatedInertia(object):
    def __init__(self, base_frame, mat):
        self.base_frame=base_frame
        assert mat.shape==(6,6)
        self.mat=mat
    def __mul__(self,other):
        if not isinstance(other, SpatialForceVector):
            raise NotImplemented("only valid for motion vectors")
        Frame.check(self,other)
        return SpatialMotionVector(self.base_frame, self.mat.dot(other.vec))
    def __repr__(self):
        return "InverseInertia [%s]:\n"%self.base_frame+str(self.mat)+"\nEnd InverseInertia"

def inverse_of_inertia(inertia):
    return InverseArticulatedInertia(inertia.base_frame, np.linalg.inv(inertia.mat))
def inverse_of_inverse_inertia(inv_inertia):
    return ArticulatedBodyInertia(inv_inertia.base_frame, np.linalg.inv(inv_inertia.mat))
setattr(ArticulatedBodyInertia,"inverse",inverse_of_inertia)
setattr(InverseArticulatedInertia,"inverse",inverse_of_inverse_inertia)


if __name__=="__main__":
    print "spatial vector test"
    W=Frame("world")
    B=Frame("body")
    p=FramePoint(W,np.array([[1.0,0.0,0.0]]).T)
    f=FrameVector(W,np.array([[0.0,1.0,0.0]]).T)
    sf=SpatialForceVector.from_point_force(p,f)
    Ia=ArticulatedBodyInertia(W,10.0*np.eye(6,6))
    print sf
    print Ia
    print Ia.inverse()
    print Ia.inverse().inverse()





