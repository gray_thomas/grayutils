import numpy as np
import matplotlib.pyplot as plt 
from math import sin, cos, sqrt, pi
from numpy import linalg as lin
import matplotlib.animation as animation
class HumeRagdoll(object):
    def __init__(self):
        self.femur=0.45
        self.shank=0.43
        self.barPivot=0.25
        self.torso=0.56
        self.bar=0.67
        self.railHeight=1.06
        self.rHip=0.0
        self.rKnee=0.0
        self.lHip=0.0
        self.lKnee=0.0

        self.rightKneeRSF=np.array([self.shank,0.0])
        self.hipRSF=self.rightKneeRSF+np.dot(self.rotMat(-self.rKnee),np.array([self.femur,0.0]))
        self.barPivotRSF=self.hipRSF+np.dot(self.rotMat(-self.rKnee-self.rHip),np.array([self.barPivot,0.0]))
        self.torsoRSF=self.hipRSF+np.dot(self.rotMat(-self.rKnee-self.rHip),np.array([self.torso,0.0]))
        self.leftKneeRSF=self.hipRSF+np.dot(self.rotMat(-self.rKnee-self.rHip+self.lHip+pi),np.array([self.femur,0.0]))
        self.leftFootRSF=self.leftKneeRSF+np.dot(self.rotMat(-self.rKnee-self.rHip+self.lHip+self.lKnee+pi),np.array([self.shank,0.0]))
        self.dualContact=True
    def rotMat(self,angle):
        return np.array([[cos(angle),sin(angle)],[-sin(angle),cos(angle)]])
    def solveLawOfCosines(self,a,b,theta):
        return sqrt(pow(a,2)+pow(b,2)-a*b*cos(theta))
    def solveSingleContactKinematics(self,rHip,rKnee,lHip,lKnee,torsoOri):
        self.solveDualContactKinematics(rHip,rKnee,lHip,lKnee,torsoOri)
        self.dualContact=False
        bodyVectorRSF=self.torsoRSF-self.hipRSF
        upVectorRSF=np.dot(self.rotMat(-torsoOri),bodyVectorRSF)
        newYRSF=self.normalize(upVectorRSF)
        newXRSF=np.dot(self.rotMat(-pi/2),newYRSF)
        newBasisRSF=np.array([newXRSF,newYRSF])
        RightShankFrameBasisVectorsExpressedInNewBasis=newBasisRSF.T
        self.setFrame(RightShankFrameBasisVectorsExpressedInNewBasis)
        self.solveBar()
        
    def normalize(self,vector):
        return vector*(1.0/lin.norm(vector)) 
    def solveDualContactKinematics(self,rHip,rKnee,lHip,lKnee,torsoOri):
        self.dualContact=True
        self.solveFramelessKinematics(rHip,rKnee,lHip,lKnee)
        rightFootToLeftFootRotatedNinetyDegreesInRightShankFrame=np.dot(self.rotMat(pi/2),self.leftFootRSF)

        # assume right knee is above the surface of the ground
        newYRSF=rightFootToLeftFootRotatedNinetyDegreesInRightShankFrame*np.dot(rightFootToLeftFootRotatedNinetyDegreesInRightShankFrame,self.rightKneeRSF)
        newXRSF=np.dot(self.rotMat(-pi/2),newYRSF)
        newYRSF=self.normalize(newYRSF)
        newXRSF=self.normalize(newXRSF)
        
        newBasisRSF=np.array([newXRSF,newYRSF])

        RightShankFrameBasisVectorsExpressedInNewBasis=newBasisRSF.T
        self.setFrame(RightShankFrameBasisVectorsExpressedInNewBasis)
        self.solveBar()
        torsoVector=self.torsoPoint-self.hipPoint
        altTorsoVector=np.dot(self.rotMat(-torsoOri),torsoVector)
        self.altTorsoPoint=altTorsoVector+self.hipPoint
        
    
    def solveBar(self):
        try:
            self.railRollerPoint=np.array([self.barPivotPoint[0]+
                                           sqrt(pow(self.bar,2)-pow(self.railHeight-self.barPivotPoint[1],2)),self.railHeight])
        except ValueError:
            # Handle the case where the bar cannot possibly reach the body. Sure sign of error.
            self.railRollerPoint=np.array([self.barPivotPoint[0]+0.1,self.railHeight])
            
    def setFrame(self,RightShankFrameBasisVectorsExpressedInNewBasis):
        self.rightFootPoint=np.array([0.0,0.0])
        self.rightKneePoint=np.dot(RightShankFrameBasisVectorsExpressedInNewBasis,self.rightKneeRSF)
        self.hipPoint=np.dot(RightShankFrameBasisVectorsExpressedInNewBasis,self.hipRSF)
        self.barPivotPoint=np.dot(RightShankFrameBasisVectorsExpressedInNewBasis,self.barPivotRSF)
        self.torsoPoint=np.dot(RightShankFrameBasisVectorsExpressedInNewBasis,self.torsoRSF)
        self.leftKneePoint=np.dot(RightShankFrameBasisVectorsExpressedInNewBasis,self.leftKneeRSF)
        self.leftFootPoint=np.dot(RightShankFrameBasisVectorsExpressedInNewBasis,self.leftFootRSF)
        
    def solveFramelessKinematics(self,rHip,rKnee,lHip,lKnee):
        self.rHip=rHip
        self.lHip=lHip
        self.rKnee=rKnee
        self.lKnee=lKnee
        self.rightKneeRSF=np.array([self.shank,0.0])
        self.hipRSF=self.rightKneeRSF+np.dot(self.rotMat(-self.rKnee),np.array([self.femur,0.0]))
        self.barPivotRSF=self.hipRSF+np.dot(self.rotMat(-self.rKnee-self.rHip),np.array([self.barPivot,0.0]))
        self.torsoRSF=self.hipRSF+np.dot(self.rotMat(-self.rKnee-self.rHip),np.array([self.torso,0.0]))
        self.leftKneeRSF=self.hipRSF+np.dot(self.rotMat(-self.rKnee-self.rHip+self.lHip+pi),np.array([self.femur,0.0]))
        self.leftFootRSF=self.leftKneeRSF+np.dot(self.rotMat(-self.rKnee-self.rHip+self.lHip+self.lKnee+pi),np.array([self.shank,0.0]))
        


    def drawMe(self, ax=None):
        if ax==None:
            ax=plt.gca()
        self.rail_line,=ax.plot([-1.0,1.0],[self.railHeight,self.railHeight],'r')
        self.ground_line,=ax.plot([-1.0,1.0],[0.0,0.0],'g')
        self.line1,=ax.plot([self.rightFootPoint[0],self.rightKneePoint[0],self.hipPoint[0],self.leftKneePoint[0],self.leftFootPoint[0]],
                 [self.rightFootPoint[1],self.rightKneePoint[1],self.hipPoint[1],self.leftKneePoint[1],self.leftFootPoint[1]],'bo-')
        self.line2,=ax.plot([self.hipPoint[0],self.torsoPoint[0],self.barPivotPoint[0],self.railRollerPoint[0]],
                 [self.hipPoint[1],self.torsoPoint[1],self.barPivotPoint[1],self.railRollerPoint[1]],'bo-')
        plt.axis('equal')
    def drawMeBlank(self, ax=None):
        if ax==None:
            ax=plt.gca()
        self.rail_line,=ax.plot([-1.0,1.0],[self.railHeight,self.railHeight],'r')
        self.ground_line,=ax.plot([-1.0,1.0],[0.0,0.0],'g')
        self.line1,=ax.plot([],[],'bo-')
        self.line2,=ax.plot([],[],'bo-')
        plt.axis('equal')

    def animateMe(self):
        self.line1.set_data([self.rightFootPoint[0],self.rightKneePoint[0],self.hipPoint[0],self.leftKneePoint[0],self.leftFootPoint[0]],
                 [self.rightFootPoint[1],self.rightKneePoint[1],self.hipPoint[1],self.leftKneePoint[1],self.leftFootPoint[1]])
        self.line2.set_data([self.hipPoint[0],self.torsoPoint[0],self.barPivotPoint[0],self.railRollerPoint[0]],
                 [self.hipPoint[1],self.torsoPoint[1],self.barPivotPoint[1],self.railRollerPoint[1]])
        return self.line1, self.line2

    def drawMeGhost(self):
        plt.plot([self.rightFootPoint[0],self.rightKneePoint[0],self.hipPoint[0],self.leftKneePoint[0],self.leftFootPoint[0]],
                 [self.rightFootPoint[1],self.rightKneePoint[1],self.hipPoint[1],self.leftKneePoint[1],self.leftFootPoint[1]],'co-')
        plt.plot([self.hipPoint[0],self.torsoPoint[0],self.barPivotPoint[0],self.railRollerPoint[0]],
                 [self.hipPoint[1],self.torsoPoint[1],self.barPivotPoint[1],self.railRollerPoint[1]],'co-')
        plt.axis('equal')