import numpy as np

class HumeDataFolderV2(object):
    def __init__(self, folderLocation):
        self.folderLocation = folderLocation + "/";
        self.vector_names={
            "hume_system_->tot_configuration_": "curr_conf.txt",
            "hume_system_->tot_vel_": "curr_vel.txt",
            "hume_system_->torque_input_": "gamma.txt",
            "hume_system_->curr_torque_": "torque.txt",
            "hume_system_->getCurrentTime()": "time.txt",
            "hume_system_->controller_->RFoot_force_": "Right_foot_force.txt",
            "hume_system_->controller_->LFoot_force_": "Left_foot_force.txt",
            "hume_system_->curr_RFoot_force_": "Curr_Right_foot_force.txt",
            "hume_system_->curr_LFoot_force_": "Curr_Left_foot_force.txt",
            "hume_system_->ori_int_force_": "Ori_internal_force.txt",
            "hume_system_->des_int_force_": "Des_internal_force.txt",
            "hume_system_->controller_->phase_": "phase.txt",
            "hume_system_->RFoot_Contact()": "rfoot_contact.txt",
            "hume_system_->LFoot_Contact()": "lfoot_contact.txt",
            "hume_system_->body_vel_ave_": "filter_body_vel_x.txt",
            "hume_system_->body_pos_ave_": "filter_body_pos_x.txt",
            "FOOT_Task::data_.foot_des":"Foot_des.txt",
            "FOOT_Task::data_.foot_vel_des":"Foot_vel_des.txt",
            "FOOT_Task::data_.foot_pos":"Foot_pos.txt",
            "FOOT_Task::data_.foot_vel":"Foot_vel.txt"
        }
        WBC_names=["Js", "Jt", "U", "Ainv", "Wint", "grav"]
        self.WBC_internal_names={}
        for ind in [1,2]:
            for name in WBC_names:
                self.WBC_internal_names[name+"_"+str(ind)]=name+"_"+str(ind)+".txt"
        self.alt_names={
            "hume_system_->tot_configuration_":["q"],
            "hume_system_->tot_vel_": ["qd"],
            "hume_system_->torque_input_": ["gamma"],
            "hume_system_->curr_torque_": ["torque"],
            "hume_system_->getCurrentTime()": ["t"],
            "hume_system_->controller_->RFoot_force_": ["right_force"],
            "hume_system_->controller_->LFoot_force_": ["left_force"],
            "hume_system_->curr_RFoot_force_": ["current_right_force"],
            "hume_system_->curr_LFoot_force_": ["current_left_force"],
            "hume_system_->ori_int_force_": ["ori_int_force"],
            "hume_system_->des_int_force_": ["des_int_force"],
            "hume_system_->controller_->phase_": ["controller_phase","phase"],
            "hume_system_->RFoot_Contact()": ["rfoot_contact"],
            "hume_system_->LFoot_Contact()": ["lfoot_contact"],
            "hume_system_->body_vel_ave_": ["filter_body_vel_x"],
            "hume_system_->body_pos_ave_": ["filter_body_pos_x","filtered_com_pos"],
            "FOOT_Task::data_.foot_des": ["foot_des"],
            "FOOT_Task::data_.foot_vel_des":["foot_vel_des"],
            "FOOT_Task::data_.foot_pos":["foot_pos"],
            "FOOT_Task::data_.foot_vel":["foot_vel"]
        }
        for ind in [1,2]:
            self.alt_names["Jt_%d"%ind]=["jac%d"%ind,"task_jacobian%d"%ind]
            self.alt_names["Js_%d"%ind]=["bar_jac%d"%ind,"constrained_jacobian%d"%ind]
            self.alt_names["Wint_%d"%ind]=["internal_matrix_%d"%ind]
            self.alt_names["Grav_%d"%ind]=["gravity_vector_%d"%ind]
            self.alt_names["Ainv_%d"%ind]=["inverse_mass_inertia_matrix_%d"%ind]

        self.alt_names["Jt_1"].append(["jac","task_jacobian","jacobian","jac_Xd_qd"])
        self.alt_names["Js_1"].append(["bar_jac","constrained_jacobian","cjac"])
        #########################
        
        self.lookupVectorFileName={}
        # self['pos6']=self.data[:,3]
        self.memoized_data={}
        for name in self.vector_names.keys():
            fileName=self.vector_names[name]
            try:
                print fileName, "is", self.alt_names[name]
                for better in self.alt_names[name]:
                    self.lookupVectorFileName[better]=fileName
                    setattr(HumeDataFolderV2, better, self[better])
            except KeyError as e:
                print name, "has no alternate names"

    def __setitem__(self, item, value):
        self.__dict__[item]=value

    def __getitem__(self, key):
        try:
            return self.loadVector(self.lookupVectorFileName[key])
        except KeyError:
            return self.__dict__[key]

    def loadVector(self, fileName):
        try:
            return self.memoized_data[fileName]
        except KeyError:
            data = np.loadtxt(self.folderLocation + fileName)
            self.memoized_data[fileName]=data
            return data

if __name__=="__main__":
    from humeID import GRAY_UTILS
    dat=HumeDataFolderV2(GRAY_UTILS+"/../hume_experiment_data")
    print dat.q
    print dat.filter_body_pos_x