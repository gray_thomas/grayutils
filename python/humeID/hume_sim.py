import lyapunov as ly
import numpy as np 
import matplotlib.pyplot as plt
from math import pow, sqrt, pi
import ly_utils as lu
from collections import namedtuple, deque
import time
from gu.math import *
from allen_variance import Hume_IMU_Gyro_Noise_Model
from leds import new_const
from hume_frames_and_poses import *
from hybrid_hume import *
from attitudeEstimator.attitude_estimation_cpp import wrapped_kalman_filter
from hume_kf import HumeKF

class HumeSim(object):
	def __init__(self, filter_to_test=HumeKF()):
		self.kf=wrapped_kalman_filter(led_cameraT,new_const,2.5,0.04)
		self.filter=filter_to_test
		self.setup()
	# x0=(-0.3,0.2,0.9, 0.4,0.0,0.0, 0.90,0.3,-0.22,0.3,   0.0,0.0,10.1), max_events=20) #realistic ICs
	def setup(self,x0=(-0.3,0.0,0.0, 0.4,0.0,0.0, 1.0,0.0,00.0,0.0,   0.0,0.0,0.0)):
		m=20.0 #kg
		g=9.8
		Fbb_rigid_body_inertia=ConstantQuantity(
			m*np.eye(6),
			np.zeros((6,6))) # body frame is coincedent with center of mass
		Fw_reaction_force=ConstantQuantity(
			np.array([[0.0,0.0,m*g]]).T, #sustain gravity, at cost of rotation
			# np.array([[0.0,0.0,0.0]]).T, #freefall
			np.array([[0.0,0.0,0.0]]).T)
		self.system=HybridHume(Fbb_rigid_body_inertia,Fw_reaction_force, x0=x0, max_events=20) 
		self.system.foot_pos=np.array([[0.0,0.0,0.0]]).T
		self.logger=ly.Recorder(self.system)
		self.delay=4
		self.predicted_leds=[]

	def loop(self):
		for t, events in self.stepper:
			assert self.system.t==t
			try:
				ly.check_NaN(self.system)
			except ArithmeticError:
				break
			self.logger.log(events)
			if events:
				if self.system.handle_events(self.stepper,events):
					break
			else:
				""" Most common case: no event, sample point. """
				self.handle_discrete(t)

	def handle_discrete(self,t):
		""" update sensors, then feed the sensor data to the filters. Record discrete info """
		self.imu_dat.append(self.system.measureGyros())
		self.led_dat.append(self.system.measureLEDs())
		self.acc_dat.append(self.system.measureAccelerometers())
		self.data_ts.append(t)
		deriv,debugging=self.system.call_with_debugging()
		self.debugging_data.append(debugging)
		self.state_derivative.append(deriv)
		self.update_old_filter()
		self.update_test_filter()
		
	def update_old_filter(self):
		self.predicted_leds.append(self.kf.estimate_leds())
		self.kf.measure_imu([0.0,0.0,0.0,0.0,0.0,0.0])
		if len(self.led_dat)%2==0 and len(self.led_dat)>self.delay:
			self.kf.measure_leds(self.led_dat[-self.delay],	[True for i in range(0,7)] )

	def update_test_filter(self):
		# self.filter.integrateNormalDynamics(self.system.a,self.dt) # todo: corrupt model's prediction
		# self.filter.measureIMU(np.bmat([[self.acc_dat[-1]],[self.imu_dat[-1]]]))
		# if (len(self.led_dat)%2==0):
		# 	self.filter.measure_leds( self.led_dat[-1], [True for i in range(0,7)])
		self.filter_state.append(self.filter.getEstimatedState())
		self.filter_cov.append(self.filter.getSigma())


	def plot_results(self):
		fig = plt.figure(figsize=[20,12])
		
		class counter(object):
			def __init__(self):
				self.n=0
			def __call__(self):
				self.n+=1
				return self.n
		incn=counter()
		rows=3
		cols=4
		fig.add_subplot(rows,cols,incn()).plot(self.logger.t,[x[0] for x in self.logger.x])
		fig.add_subplot(rows,cols,incn()).plot(self.logger.t,[x[1] for x in self.logger.x])
		fig.add_subplot(rows,cols,incn()).plot(self.logger.t,[x[2] for x in self.logger.x])
		incn()
		fig.add_subplot(rows,cols,incn()).plot(self.logger.t,[x[6] for x in self.logger.x])
		fig.add_subplot(rows,cols,incn()).plot(self.logger.t,[x[7] for x in self.logger.x])
		fig.add_subplot(rows,cols,incn()).plot(self.logger.t,[x[8] for x in self.logger.x])
		fig.add_subplot(rows,cols,incn()).plot(self.logger.t,[x[9] for x in self.logger.x])
		# fig.add_subplot(334).plot(self.data_ts,[w[0,0] for w in self.imu_dat])
		# fig.add_subplot(335).plot(self.data_ts,[w[1,0] for w in self.imu_dat])
		# fig.add_subplot(336).plot(self.data_ts,[w[2,0] for w in self.imu_dat])
		# fig.add_subplot(334).plot(self.data_ts,[d.reaction_wrench.x for d in self.debugging_data])
		# fig.add_subplot(335).plot(self.data_ts,[d.reaction_wrench.y for d in self.debugging_data])
		# fig.add_subplot(336).plot(self.data_ts,[d.reaction_wrench.z for d in self.debugging_data])
		# fig.add_subplot(337).plot(self.data_ts,[a[0,0] for a in self.acc_dat])
		# fig.add_subplot(338).plot(self.data_ts,[a[1,0] for a in self.acc_dat])
		# fig.add_subplot(339).plot(self.data_ts,[a[2,0] for a in self.acc_dat])
		# fig.add_subplot(rows,cols,incn()).plot(self.data_ts,[d.gravity_wrench.x for d in self.debugging_data]) # gravity wrench
		# fig.add_subplot(rows,cols,incn()).plot(self.data_ts,[d.gravity_wrench.y for d in self.debugging_data])
		# fig.add_subplot(rows,cols,incn()).plot(self.data_ts,[d.gravity_wrench.z for d in self.debugging_data])
		fig.add_subplot(rows,cols,incn()).plot(self.data_ts,[x[3] for x in self.logger.x]) # gravity wrench
		fig.add_subplot(rows,cols,incn()).plot(self.data_ts,[x[4] for x in self.logger.x])
		fig.add_subplot(rows,cols,incn()).plot(self.data_ts,[x[5] for x in self.logger.x])
		fig.tight_layout()
		return fig

	def simulate(self, tf=0.2, dt=0.001):
		
		start_time=time.time()
		self.dt=dt
		self.stepper=ly.dormand_prince(self.system, np.arange(0.0,tf,dt))
		self.imu_dat=[]
		self.led_dat=[]
		self.acc_dat=[]
		self.data_ts=[]
		self.filter_state=[]
		self.filter_cov=[]
		self.debugging_data=[]
		self.state_derivative=[]
		self.loop()
		end_time=time.time()
		print "Elapsed computation time:", end_time-start_time

def testPhysicsSituation1():
	sim=HumeSim()
	sim.setup()
	sim.system.state=0.0,(1.0,0.0,1.0, 0.0,0.0,0.0, 1.00,0.0,0.0,0.0,   0.1,0.0,0.0)
	sim.simulate()
	sim.plot_results()

if __name__=="__main__":
	testPhysicsSituation1()
	plt.show()