import matplotlib.pyplot as plt
import numpy as np
import random as rand
from math import sin, cos, sqrt, pi


if __name__ == '__main__':
    N=4000
    Q=60
    S=6
    cdfs=[[]]*S
    t = np.linspace(0, 40., N)
    cdf_times = t[[k*floor(N/S) for k in range(0,S)]]
    e = np.array([[1, 0], [0, 1]])
    print dir(rand)
    fig1= plt.figure()
    ax1 = plt.gca()
    fig2=plt.figure()
    ax2 = plt.gca()
    for i in range(0, Q):
        r = sqrt(rand.random())
        o = pi*(rand.random())*2.0
        x,xd=r*cos(o),r*sin(o)
        x,xd=rand.gauss(0,1),rand.gauss(0,1)
        xs = np.zeros((N,))
        xds = np.zeros((N,))
        for j, t_val in enumerate(t):
            r=np.linalg.norm([x,xd])
            D = 1.0
            U = D*(1.0+2.0/r)
            dVdt=D*(-1+2.0/r)*rand.random()
            acc = 2*3.14*rand.gauss(0,1)-x -xd

            x = x + (t[1] - t[0]) * xd
            xd = xd + (t[1] - t[0]) * acc

            xs[j] = x
            xds[j]=xd

        # ax1.plot(xs[0:100], xds[0:100],'k')
        # ax1.plot(xs[-100:], xds[-100:],'m')
        # ax1.plot(xs, xds)
        ax1.plot(xs, xds)
        ax2.plot(t, xs)

    plt.show()
