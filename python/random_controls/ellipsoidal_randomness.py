import matplotlib.pyplot as plt
import numpy as np
import random as rand
from math import sin, cos, sqrt, pi


if __name__ == '__main__':
    N=200000
    t = np.linspace(0, 1000., N)
    e = np.array([[1, 0], [0, 1]])
    print dir(rand)
    fig1= plt.figure()
    ax1 = plt.gca()
    fig2=plt.figure()
    ax2 = plt.gca()
    for i in range(0, 10):
        r = sqrt(rand.random())
        o = pi*(rand.random())*2.0
        x,xd=r*cos(o),r*sin(o)
        if r>1:
            x=x/r
            xd = xd/r
        xs = np.zeros((N,))
        xds = np.zeros((N,))
        for j, t_val in enumerate(t):
            max_xdd = 100.0
            min_xdd = -100.0
            r=np.linalg.norm([x,xd])
            D = 1.0
            U = D*(1.0+2.0/r)
            dVdt=D*(-1+2.0/r)*rand.random()
            # G = 10.0
            # dVdt = (rand.random()-0.5)*10.0/r**2
            # k1 = G*(0.5-0.5*r)
            # k2 = G*(0.0-0.5*r)
            # if xd>0:
            #     max_xdd = min(max_xdd,-x+k1/(xd if abs(xd) >1e-7 else 1e-7* np.sign(xd)))
            #     min_xdd = max(min_xdd,-x+k2/(xd if abs(xd) >1e-7 else 1e-7* np.sign(xd)))
            # if xd<0:
            #     max_xdd = min(max_xdd,-x+k1/(xd if abs(xd) >1e-7 else 1e-7* np.sign(xd)))
            #     min_xdd = max(min_xdd,-x+k2/(xd if abs(xd) >1e-7 else 1e-7* np.sign(xd)))

            # acc = min_xdd + rand.random()*(max_xdd-min_xdd)
            if r>0.99:
                if dVdt>0:
                    dVdt=0.0
            if r<0.001:
                if dVdt<0:
                    dVdt=0.0
            acc = -x+dVdt/(xd if abs(xd) >1e-1 else 1e-1* np.sign(xd))
            max_acc = 10.0
            acc = max_acc if acc>max_acc else -max_acc if acc<-max_acc else acc

            x = x + (t[1] - t[0]) * xd
            xd = xd + (t[1] - t[0]) * acc

            xs[j] = x
            xds[j]=xd

        # ax1.plot(xs[0:100], xds[0:100],'k')
        # ax1.plot(xs[-100:], xds[-100:],'m')
        # ax1.plot(xs, xds)
        ax1.plot(xs[::100], xds[::100], 'ko')
        # ax2.plot(t, xs)

    plt.show()
