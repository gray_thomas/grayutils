import matplotlib.pyplot as plt
import numpy as np
import random as rand
from math import sin, cos, sqrt, pi
from plot_setup import *


if __name__ == '__main__':
    N=30
    rs = np.linspace(0, 1., N)
    qs = np.linspace(1., 1., N)
    dqs = np.zeros((N,))

    fig1= plt.figure()
    ax1 = plt.gca()
    for m in range(0, 2000):
        dqs = np.zeros((N,))
        for i in range(0,N):
            if i==0:
                dqs[i]-=qs[i]*0.4
                dqs[i+1]+=qs[i]*0.4
                continue
            if i==N-1:
                dqs[i]-=qs[i]*0.6
                dqs[i-1]+=qs[i]*0.6
                continue
            dqs[i]-=qs[i]
            dqs[i+1]+=qs[i]*0.4
            dqs[i-1]+=qs[i]*0.6
        assert abs(sum(dqs))<1e-7
        qs += dqs*0.05
        if m%100==1:
            ax1.plot(rs, qs)
        # ax2.plot(t, xs)

    plt.show()
