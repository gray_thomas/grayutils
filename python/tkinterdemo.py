import Tkinter as tk
import Tkconstants as tkc
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

class My_App:

	def __init__(self):
		self._root=tk.Tk()
		self._generate_left_frame(self._root).pack(side=tkc.LEFT)
		self._generate_plot(self._root).pack(side=tkc.LEFT)
		self._generate_right_frame(self._root).pack(side=tkc.LEFT)

	def _generate_right_frame(self,parent_of_left_frame):
		self._right_frame=tk.Frame(master=parent_of_left_frame)
		tk.Label(master=self._right_frame, 
			text="Spinbox Input").pack()
		self._spinbox=tk.Spinbox(master=self._right_frame, 
			text="spinbox", values=range(1,20), 
			command=self._print_spinbox)
		self._spinbox.pack()
		tk.Label(master=self._right_frame, 
			text="Scale Input").pack()
		self._scale=tk.Scale(master=self._right_frame,
			from_=0.0, to=5.0, resolution=0.1, 
			orient=tkc.HORIZONTAL)
		self._scale.pack()
		return self._right_frame

	def _print_spinbox(self):
		print self._spinbox.get()

	def _generate_left_frame(self,parent_of_left_frame):
		left_frame=tk.Frame(master=parent_of_left_frame)
		tk.Button(master=left_frame, text="show",
			command=self._show_right).pack()
		tk.Button(master=left_frame, text="hide", 
			command=self._hide_right).pack()
		tk.Button(master=left_frame, text="display", 
			command=self._display_slider_value).pack()
		return left_frame	

	def _generate_plot(self,parent):
		''' return a frame with an animated plot '''
		plot_frame=tk.Frame(master=parent)

		tk.Label(master=plot_frame, text="Plot").pack()
		self._fig=plt.figure()
		canvas = FigureCanvasTkAgg(self._fig, master=plot_frame)
		
		ax=self._fig.add_subplot(111)
		self._x = np.arange(0, 2*np.pi, 0.01)
		self._animated_line, = ax.plot(self._x,np.sin(self._x))
		ax.axes.set_xlabel("time")
		ax.axes.set_ylabel("position")
		canvas.get_tk_widget().pack()
		
		return plot_frame

	def _animate(self, frame):
		self._animated_line.set_ydata(np.sin(self._x+frame/10.0))


	def _display_slider_value(self):
		print self._scale.get()

	def _hide_right(self):
		self._right_frame.pack_forget()

	def _show_right(self):
		self._right_frame.pack()

	def start(self):
		ani=animation.FuncAnimation(self._fig, self._animate, 
			np.arange(1,200), interval=25, blit=False)
		self._root.mainloop()

if __name__=="__main__":
	app = My_App()
	app.start()