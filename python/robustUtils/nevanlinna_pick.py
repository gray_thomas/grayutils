import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import cvxpy as cvx
# import matplotlib.pyplot as plt
# import numpy as np


def plot_complex_function(f, N=30, r=1000):
	fig = plt.figure()
	ax = fig.gca(projection='3d')
	Re = np.linspace(-1, 1, N)
	Im = np.linspace(-1, 1, N)
	F = np.array([[abs(f(complex(re, im))) for re in Re] for im in Im])
	Re, Im = np.meshgrid(Re, Im)

	thetas = np.linspace(0, 2*np.pi, r)
	line = [abs(f(complex(np.cos(th),np.sin(th)))) for th in thetas]
	ax.plot(np.cos(thetas), np.sin(thetas), zs = line)
	surf = ax.plot_surface(Re, Im, F, rstride=1, cstride=1, cmap=cm.coolwarm,
	        linewidth=0, antialiased=True)
	# ax.set_zlim(-1.01, 1.01)
	ax.set_xlabel(r"$\mathbb{R}$", size=16)
	ax.set_ylabel(r"$j\mathbb{R}$", size=16)

	# ax.zaxis.set_major_locator(LinearLocator(10))
	# ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	# fig.colorbar(surf, shrink=0.5, aspect=5)

def plot_complex_function2(f, N=30, r=1000):
	fig = plt.figure()
	ax = fig.gca(projection='3d')
	Re = np.linspace(-1, 1, N)
	Im = np.linspace(-1, 1, N)
	Fre = np.array([[f(complex(re, im)).real for re in Re] for im in Im])
	Fim = np.array([[f(complex(re, im)).imag for re in Re] for im in Im])
	Re, Im = np.meshgrid(Re, Im)

	thetas = np.linspace(0, 2*np.pi, r)
	linere = [f(complex(np.cos(th),np.sin(th))).real for th in thetas]
	lineim = [f(complex(np.cos(th),np.sin(th))).imag for th in thetas]
	ax.plot(np.cos(thetas), np.sin(thetas), zs = linere, color='k')
	ax.plot(np.cos(thetas), np.sin(thetas), zs = lineim, color ='k')
	surf = ax.plot_surface(Re, Im, Fre, rstride=1, cstride=1, cmap=cm.Blues,
	        linewidth=0, antialiased=True)
	surf = ax.plot_surface(Re, Im, Fim, rstride=1, cstride=1, cmap=cm.Reds,
	        linewidth=0, antialiased=True)
	# ax.set_zlim(-1.01, 1.01)
	ax.set_xlabel(r"$\mathbb{R}$", size=16)
	ax.set_ylabel(r"$j\mathbb{R}$", size=16)

	# ax.zaxis.set_major_locator(LinearLocator(10))
	# ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	# fig.colorbar(surf, shrink=0.5, aspect=5)


def generate_pick_matrix(lambdas,zs):
	assert len(lambdas)==len(zs)
	n=len(lambdas)
	pick_mat = np.zeros((n,n))
	for i in range(0,n):
		for j in range(0,n):
			pick_mat[i,j] = pow((1-zs[j].conjugate()*zs[i])/(1-lambdas[j].conjugate()*lambdas[i]),n)
	return pick_mat
j=complex(0,1)
print dir(j)
if __name__ == '__main__':
	# print generate_pick_matrix([1.0],[0.5+0.1*j])
	X = cvx.Semidef(2)
	a=9.21699
	f = lambda z: 0.207*((a-z)/(a+z))
	print f(1)-1.0/6.
	print f(2)-2./15.
	plot_complex_function2(f)
	plt.show()
