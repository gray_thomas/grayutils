print "good morning, Dr. Bakolas"
import numpy as np
import math

#f=1/2 x' Q x - x' b
Q=np.array([4.0, 1.0, 1.0, 4.0]).reshape((2,2))
b=np.array([1., 0.]).reshape((2,1))
x=np.array([0.,0.]).reshape((2,1))
e=0.0001
f = lambda x: 0.5*x.T.dot(Q).dot(x)-x.T.dot(b)
x_previous=np.array(x)
print "starting with x = ", x.T," transpose"
is_done=False
while (not is_done):
	grad=x.T.dot(Q)-b.T
	print "grad f is", grad
	# calculate the terms of the new quadratic polynomial in alpha, E(alpha_k)
	E_2 = 0.5 * -grad.dot(Q).dot(-grad.T)
	E_1 = 0.5 * (x.T.dot(Q).dot(-grad.T)+-grad.dot(Q).dot(x))+grad.dot(b)
	E_0 = 0.5 * (x.T.dot(Q).dot(x)) - x.T.dot(b)
	Ep_1 = E_2 * 2
	Ep_0 = E_1
	a_min = -Ep_0/Ep_1
	print "polynomial E(a) = %.3f a^2 + %.3f a + %.3f " % (E_2, E_1, E_0)
	print "minimum at E'(a) = %.3f a + %.3f = 0 => a = %.3f" % (Ep_1, Ep_0, a_min)
	x_previous=np.array(x)
	x = x-a_min*grad.T
	print "new x is", x.T,"transpose, with f(x) = %.8f" % f(x)
	if math.sqrt((a_min*a_min * grad.dot(grad.T)))<e:
		is_done=True
print "we finished! x* =",x.T,'transpose, f(x) = %.8f'% f(x)
x_star=np.linalg.solve(Q,b)
print "analytically we should get x* = A^-1 * b = ", x_star.T,'transpose for f(x*)=', f(x_star)