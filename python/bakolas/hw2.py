from scipy.optimize import *
import numpy as np
def p3func(x, sign=1.0):
	return sign*(x[0]**2-2.0*x[0]*x[1]+3.0*x[1]**2-4.0)
def p3func_deriv(x,sign=1.0):
	dfdx0 = sign*(2.0*x[0]-2.0*x[1])
	dfdx1 = sign*(-2.0*x[0]+6.0*x[1])
	return np.array([ dfdx0, dfdx1 ])

p3func_cons = ({'type': 'eq',
	'fun' : lambda x: np.array([x[1]-x[0]-2.0]),
	'jac' : lambda x: np.array([-1.0, 1.0])},
)

p3_res = minimize(p3func, [-2.3,0.0], args=(1.0,), jac=p3func_deriv,
	 method='SLSQP', options={'disp': True}, constraints=p3func_cons)
print p3_res

def p4func(x, sign=1.0):
	return sign*(0.5*x[0]**2+0.125*x[1]**2)
def p4func_deriv(x,sign=1.0):
	dfdx0 = sign*(x[0])
	dfdx1 = sign*(0.25*x[1])
	return np.array([ dfdx0, dfdx1 ])

p4func_cons = ({'type': 'eq',
	'fun' : lambda x: np.array([0.5-x[0]*x[1]]),
	'jac' : lambda x: np.array([-x[1], -x[0]])},
)

p4_res = minimize(p4func, [2.3,0.0], args=(1.0,), jac=p4func_deriv,
	 method='SLSQP', options={'disp': True}, constraints=p4func_cons)
print p4_res

def p6func(x, sign=1.0):
	return sign*(0.5*(x[0]**2+x[1]**2+x[2]**2))
def p6func_deriv(x,sign=1.0):
	dfdx0 = sign*(x[0])
	dfdx1 = sign*(x[1])
	dfdx2 = sign*(x[2])
	return np.array([ dfdx0, dfdx1 , dfdx2 ])

p6func_cons = ({'type': 'ineq',
	'fun' : lambda x: -np.array([x[0]+x[1]+x[2]+6.0]),
	'jac' : lambda x: -np.array([1.0, 1.0,1.0])},
)

p6_res = minimize(p6func, [-2.3,0.0,433.7], args=(1.0,), jac=p6func_deriv,
	 method='SLSQP', options={'disp': True}, constraints=p6func_cons)
print p6_res