function [m,Model,res]=PostulateARMA(ts,P)

%Variable ts is the time-series and P is the level of significance that will be used
%to for F tests associated with the model fitting.
%
%Please note that in the UT patch, I only have the 95% significance level
%tables and hence, whatever level you state as P, you will be running 95%
%confidence interval statistical tests (unless you use the statistical
%toolbox, in which case you will fully utilize the parameter P).
%
%Time-series object can be created by typing Data=iddata(ts); Then, this stucture is compatible with the
%ARMA fiting procedures in Matlab.
%
%Model is the model object representing the ARMA model fitted to the Data time-series object.
%It contains all necessary information about this model.
%Type set(idpoly) to see all the model properties and how you can get them.
%
%m is the mean value of the Data time-series calculated using the simple sample mean.
%
%Created by Dragan Djurdjanovic, September 2002, Ann Arbor, MI.
%
%Last modified Mar. 19th 2013 in Austin, TX

load F_Distribution_Wrkspc %Loading the Fischer tables (for those who do not have the statistics toolbox
ni1(length(ni1))=300;
ni2(length(ni2))=300;
%You do not need the line above if you have the statistical toolbox.





%ts=get(Data,'y');
N=length(ts);
m=mean(ts);
[n1,n2]=size(ts);
ts=ts-m*ones(n1,n2);
Cycle=1;

Data=iddata(ts);

%Initializing
CurrentModel=armax(Data,[2 1]);
n=1;
%CurrentEstimationInfo=CurrentModel.EstimationInfo;
%CurrentRSS=CurrentEstimationInfo.LossFcn*(N-4);
r=resid(CurrentModel,Data);
residuals=r.y;
CurrentRSS=sum(residuals.^2); %residual sum of squares

while Cycle
    n=n+1;
    OldModel=CurrentModel;
    OldRSS=CurrentRSS;
    CurrentModel=armax(Data,[2*n 2*n-1]);
    disp(['testing model ARMA(',num2str(2*n),',',num2str(2*n-1),')'])
    
    %CurrentEstimationInfo=CurrentModel.EstimationInfo;
    r=resid(CurrentModel,Data);
    residuals=r.y;
    CurrentRSS=sum(residuals.^2); %residual sum of squares
    %CurrentRSS=CurrentEstimationInfo.LossFcn*(N-4*n);
    TestRatio=((OldRSS-CurrentRSS)/4)/(CurrentRSS/(N-4*n));
    Control=finv(P,4,N-4*n); %Had to change this!
    %If you have statistical toolbox, uncomment the line above
    %and comment the line below
%     Control=FindFischer(4,N-4*n,f_95,ni1,ni2);
    [TestRatio Control;2*n 2*n-1;2*n-2 2*n-3];%this was just for debugging purposes.
    if TestRatio<Control
        disp(['model ARMA(',num2str(2*n-2),',',num2str(2*n-3),') wins!'])
        Cycle=0;
        PreliminaryModel=OldModel;
        PreliminaryRSS=OldRSS;
        [TestRatio Control;2*n-1 2*n-2]; %this was just for debugging purposes.
    end
end
AR_Order=length(PreliminaryModel.a)-1;
MA_Order=length(PreliminaryModel.c)-1;


%Now check if the odd valued model is good
CurrentModel=armax(Data,[AR_Order-1 AR_Order-2]);
disp(['testing odd model: ARMA(',num2str(AR_Order-1),',',num2str(AR_Order-2),')'])
%CurrentEstimationInfo=CurrentModel.EstimationInfo;
%CurrentRSS=CurrentEstimationInfo.LossFcn*(N-2*AR_Order-4);
r=resid(CurrentModel,Data);
residuals=r.y;
CurrentRSS=sum(residuals.^2); %residual sum of squares

TestRatio=((CurrentRSS-PreliminaryRSS)/2)/(PreliminaryRSS/(N-(2*AR_Order-2)));

Control=finv(P,2,N-(2*AR_Order-2));
%If you have statistical toolbox, uncomment the line above
%and comment the line below
% Control=FindFischer(2,N-(2*AR_Order-2),f_95,ni1,ni2);

if TestRatio<Control
    disp(['odd model ARMA(',num2str(AR_Order-1),',',num2str(AR_Order-2),') wins!'])
    PreliminaryModel=CurrentModel;
    PreliminaryRSS=CurrentRSS;
else
    disp(['even model: ARMA(',num2str(AR_Order),',',num2str(AR_Order-1),') wins!'])
end

%Now, removing the unnecessary MA parameters.
AR_Order=length(PreliminaryModel.a)-1;
MA_Order=length(PreliminaryModel.c)-1;
CurrMA=MA_Order;
CurrentModel=PreliminaryModel;
CurrentRSS=PreliminaryRSS;

if CurrMA>1
    Cycle=1;
else
    Cycle=0;
    Model=PreliminaryModel;
    RSS=PreliminaryRSS;
end

while Cycle
    OldModel=CurrentModel;
    OldRSS=CurrentRSS;
    CurrMA=CurrMA-1;
    CurrentModel=armax(Data,[AR_Order CurrMA]); 
    disp(['testing reduced phi ARMA(',num2str(AR_Order),',',num2str(CurrMA),')'])
    %CurrentEstimationInfo=CurrentModel.EstimationInfo;
    %CurrentRSS=CurrentEstimationInfo.LossFcn*(N-AR_Order-CurrMA-1);
    r=resid(CurrentModel,Data);
    residuals=r.y;
    CurrentRSS=sum(residuals.^2); %residual sum of squares
    
    NumOfParams=AR_Order+CurrMA+1;
    TestRatio=((CurrentRSS-PreliminaryRSS)/1)/(PreliminaryRSS/(N-NumOfParams));
    %%%%%%%%%%%%%%%%%%
%     Control=FindFischer(1,NumOfParams,f_95,ni1,ni2); %This is a patch function I made since we do not have
    %the finv function (no statistics toolbox).
    %If you have statistical toolbox, uncomment the line below
    %and comment the line above
    Control=finv(P,1,NumOfParams);
    if TestRatio>Control
        disp(['final model is ARMA(',num2str(AR_Order),',',num2str(CurrMA+1),') !!'])
        Cycle=0;
        Model=OldModel;
        RSS=OldRSS;
    end
end %Done

r=resid(Model,Data);
res=r.y;