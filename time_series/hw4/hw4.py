import numpy as np
from math import sqrt, atan2,pi
from hw3tools import *
# P=np.array([[-1,0.2,0.25,0.05],[1,-1,1,0]]) # P2
# P=np.array([[-1,1.5,-0.6],[1, -0.5,0.0]]) # P3.2 i
# P=np.array([[-1,2.0,-0.6,0.2],[1,-0.6,-0.5,0.0]]) # P3.2 iii
# P=np.array([[-1.0, 1.0, -1.0],[1.0,-3.0,0.0]]) # P3 1
# P=np.array([[-1.0, +1.1, -0.3],[1.0,-0.6,0.0]]) # P3.7
P=np.array([[-1.0, 0.2, 0.04],[1.0,-0.3,0.0]]) # HW4 P1
P1=np.concatenate([np.zeros((2,1)),P],axis=1)
P0=np.concatenate([P,np.zeros((2,1))],axis=1)
system="0="
for i in range(0,P.shape[1]):
	system+=("+(%.2f)X_{t-%d}"%(P[0,i],i))
for i in range(0,P.shape[1]):
	system+=("+(%.2f)a_{t-%d}"%(P[1,i],i))
print system

res=np.array(P)
for i in range (1,20):
	P1=np.concatenate([np.zeros((2,i)),P],axis=1)
	P0=np.concatenate([res,np.zeros((2,1))],axis=1)
	res=P0+P1*P0[0,i]

for i in range(0,res.shape[1]):
	print "%0.8f X_{t-%d}"%(res[0,i],i)
for i in range(0,res.shape[1]):
	print " %0.8f a_{t-%d}"%(res[1,i],i)
for i in range(0,min(res.shape[1],20)):
	print "G_{%d} =& %0.8f \\\\\\nonumber" % (i+1,res[1,i])

l2s,l1s = quadratic(1,-1,1)
print "hey", l1s,l2s
g1s,g2s = find_gs(l1s,l2s,-3.0)
print "gs=",g1s,g2s

l1=complex(0.5,-0.5*sqrt(3.0))
l2=complex(0.5,0.5*sqrt(3.0))
print l1, l2
A=complex(0.5,-2.5/sqrt(3.0))
B=complex(0.5,2.5/sqrt(3.0))
print "Gs A, B=", A, B

for i in range(0,20):
	print "G_",i
	print A *pow(l1,i) + B * pow(l2,i)
	print g1s *pow(l1s,i) + g2s * pow(l2s,i)
print atan2(l1.imag, -l1.real)
print 2.0*pi/6.0
print l1+l2, l1*l2
print sqrt(pow(A.real,2)+pow(A.imag,2))
print sqrt(pow(B.real,2)+pow(B.imag,2))


for l in range (0,10):
	print "\gamma_{%d}=(%d)\sigma_a^2 \\\\\\nonumber"%(l,autocovar(l1s,l2s,g1s,g2s,l,1.0).real)
for l in range (0,10):
	print "\\rho_{%d}=(%f)\sigma_a^2 \\\\\\nonumber"%(l,autocorr(l1,l2,A,B,l).real)
l1,l2=quadratic(1,-0.2,0.04)
g1,g2=find_gs(l1,l2,-0.3)
print "gs", g1, g2
print "ls", l1,l2
print g1+g2
print g1*l2 + g2*l1
print "start"
print g1*g1/(1-l1*l1)
print g1*g2/(1-l1*l2)
print g2*g2/(1-l2*l2)
# g1=0.5
import math
# g2=0.5
def magnitude(theta):
	zinv=complex(cos(theta),-sin(theta))
	h=g1/(1-l1*zinv)+g2/(1-l2*zinv)
	H=h*h.conjugate()
	return H
def simp_mag(theta):
	zinv=complex(cos(theta),-sin(theta))
	h=(1.0-0.3*zinv)/(1.0-0.2*zinv+0.04*zinv*zinv)
	H=h*h.conjugate()
	return H
import matplotlib.pyplot as plt 

opt=1.9018595
magopt=1.074156236
thetas=np.linspace(-pi,pi,40000)
# thetas=np.linspace(opt-0.0001,opt+0.0001,4000)
mags=[magnitude(theta) for theta in thetas]
# print thetas,mags
# plt.plot(thetas,[math.log10(magnitude(theta)) for theta in thetas])
plt.plot(thetas,[magnitude(theta) for theta in thetas])
# plt.plot(thetas,[simp_mag(theta) for theta in thetas],'r.')
plt.show()


def print_explicit_green(gs,ls,num):
	for i in range(0,10):
		G=sum([g*pow(l,i) for g,l in zip(gs,ls)])
		print "G_{%d} =& %0.8f \\\\\\nonumber" %(i+1,G)
# for l in range (0,10):
# 	print "\\rho_{%d}=(%f) \\\\\\nonumber"%(l,autocorr(l1,l2,g1,g2,l).real)
RSS21=202.3
RSS43=RSS21*197.0/(2.37*4+197)
print RSS43
