import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import Axes3D
import numpy as np  
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from math import *
from cmath import *
from scipy.special import gamma
from matplotlib import gridspec
from hw3tools import *

phi_1=0.2
phi_2=-0.04
theta_1=0.3

def g(zinv):
	return (1.0-theta_1*zinv)/(1.0-phi_1*zinv-phi_2*zinv*zinv)
def mag_squared(z):
	return (z*z.conjugate()).real
def saturate(v):
	m=2
	if v>m:
		return m
	if v<-m:
		return -m
	return v
def cinv(z):
	return complex(1.0,0.0)/z
vec_sat=np.vectorize(saturate)
v_mag=np.vectorize(mag_squared)
cplex=np.vectorize(complex)
vinv=np.vectorize(cinv)
gamma_v=np.vectorize(gamma)
vec_g=np.vectorize(g)

l1,l2=quadratic(1,-phi_1,-phi_2)
g1,g2=find_gs(l1,l2,-theta_1)
print "gs", g1, g2
print "ls", l1,l2

expo=complex(0.5,0.5*sqrt(3))
res=expo*(1.0/(expo-0.3)+1.0/(expo-l2)+1.0/(expo-l1))
print res
print -res.imag*2
print -res.imag*2 * mag_squared(g(expo))

thetas=np.linspace(-pi,pi,10000)
xs=np.cos(thetas)
ys=np.sin(thetas)
zinvs=vinv(cplex(xs,ys))
hs=vec_sat(v_mag(vec_g(zinvs)))

fig=plt.figure()
gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
ax=fig.add_subplot(gs[0],projection='3d',aspect="equal")
ax.plot(xs,ys,'k',zs=hs,label='projection of unit circle')
ax.plot([l1.real,l2.real],[l1.imag,l2.imag],'bx',zs=[0,0],label='poles')
ax.plot([l1.real,l1.real],[l1.imag,l1.imag],'b',zs=[0,2])
ax.plot([l2.real,l2.real],[l2.imag,l2.imag],'b',zs=[0,2])
ax.plot([theta_1],[0],'bo',zs=[0],mfc='none',mec='b',label='zero')
ra=1.1
X = np.arange(-ra, ra, 0.02)
Y = np.arange(-ra, ra, 0.02)
X, Y = np.meshgrid(X, Y)
Zinv = vinv(cplex(X,Y))
ax2=fig.add_subplot(gs[1])
ax2.plot(thetas,hs)
ax2.plot([atan2(l1.imag,l1.real)]*2,[min(hs),max(hs)])
ax2.plot([atan2(l2.imag,l2.real)]*2,[min(hs),max(hs)])
ax2.set_xlabel(r"$\theta$")
ax2.set_ylabel(r"$\|H(z)\|_2^2$")

# gammas=gamma_v(Zinv)

val = v_mag(vec_g(Zinv))
cont=ax.contour(X,Y,vec_sat(val),cmap=cm.coolwarm, stride=0.1,extend3d=False)
wire=ax.plot_wireframe(X,Y,vec_sat(val),cmap=cm.coolwarm,rstride=10, cstride=10)
surf = ax.plot_surface(X, Y, vec_sat(val), rstride=3, cstride=3, cmap=cm.coolwarm,
        linewidth=0, antialiased=False, alpha=0.3)
ax.set_zlim(-0.01, 2.01)
ax.legend(bbox_to_anchor=(0., 1.02, 1., .102),loc=3,
           ncol=3, mode="expand", borderaxespad=0.)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
fig.suptitle("magnitude squared for $G(z)=(1-(%.2f))/z)/(1-(%.2f)/z-(%.2f)/z^2)$"%(theta_1,phi_1,phi_2))

# fig.colorbar(surf, shrink=0.5, aspect=5)
fig.tight_layout()

plt.show()
