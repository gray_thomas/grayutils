import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import Axes3D
import numpy as np  
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from math import *
from cmath import *
from scipy.special import gamma
from hw3tools import *
def g(zinv):
	return (1.0-0.3*zinv)/(1.0-0.2*zinv+0.04*zinv*zinv)
def mag_squared(z):
	return (z*z.conjugate()).real
def saturate(v):
	m=2
	if v>m:
		return m
	if v<-m:
		return -m
	return v
def cinv(z):
	return complex(1.0,0.0)/z
vec_sat=np.vectorize(saturate)
v_mag=np.vectorize(mag_squared)
cplex=np.vectorize(complex)
vinv=np.vectorize(cinv)
gamma_v=np.vectorize(gamma)
vec_g=np.vectorize(g)

l1,l2=quadratic(1,-0.2,0.04)
g1,g2=find_gs(l1,l2,-0.3)
print "gs", g1, g2
print "ls", l1,l2

thetas=np.linspace(-pi,pi,10000)
xs=np.cos(thetas)
ys=np.sin(thetas)
zinvs=vinv(cplex(xs,ys))
hs=vec_sat(v_mag(vec_g(zinvs)))

fig=plt.figure()
ax=fig.add_subplot(111,projection='3d',aspect="equal")
ax.plot(xs,ys,'k',zs=hs,label='projection of unit circle')
ax.plot([l1.real,l2.real],[l1.imag,l2.imag],'bx',zs=[0,0],label='poles')
ax.plot([l1.real,l1.real],[l1.imag,l1.imag],'b',zs=[0,2])
ax.plot([l2.real,l2.real],[l2.imag,l2.imag],'b',zs=[0,2])
ax.plot([0.3],[0],'bo',zs=[0],mfc='none',mec='b',label='zero')
ra=1.1
X = np.arange(-ra, ra, 0.02)
Y = np.arange(-ra, ra, 0.02)
X, Y = np.meshgrid(X, Y)
Zinv = vinv(cplex(X,Y))

# gammas=gamma_v(Zinv)

val = v_mag(vec_g(Zinv))
cont=ax.contour(X,Y,vec_sat(val),cmap=cm.coolwarm, stride=0.1,extend3d=False)
wire=ax.plot_wireframe(X,Y,vec_sat(val),cmap=cm.coolwarm,rstride=10, cstride=10)
surf = ax.plot_surface(X, Y, vec_sat(val), rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
ax.set_zlim(-0.01, 2.01)
ax.legend()
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
fig.suptitle("magnitude squared for G(z)=(1-0.3/z)/(1-0.2/z+0.04/z^2)")

fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()
