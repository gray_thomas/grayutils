import numpy as np
from math import sqrt, atan2,pi, sin, cos

def quadratic(a,b,c):
	det = pow(b,2)-4.0*a*c
	if det>=0:
		den=sqrt(pow(b,2)-4*a*c)/2.0
	else:
		den=complex(0.0,1.0)*sqrt(-pow(b,2)+4.0*a*c)/2.0
	cent=-b*0.5
	print cent
	return cent+den,cent-den

def autocovar(l1,l2,A,B,l,sigma_a):
	term1=A*A*pow(l1,l)/(1.0-pow(l1,2))
	term2=A*B*(pow(l1,l)+pow(l2,l))/(1.0-l1*l2)
	term3=A*B*pow(l2,l)/(1.0-pow(l2,2))
	return pow(sigma_a,2)*(term1+term2+term3)
def autocorr(l1,l2,A,B,l):
	return (autocovar(l1,l2,A,B,l,1)/autocovar(l1,l2,A,B,0,1))
def find_gs(l1,l2,a2):
	# Derivation of this approach:
	# (1-a2/z)/[(1-l1 /z)(1-l2/z) = g1/(1-l1/z)+g2/(1-l2/z)
	# 1-a2/z = g1(1-l2/z)+g2(1-l1/z)
	# 1-a2 = g1(1-l2) + g2 (1-l1)
	# 1+a2 = g1(1+l2) + g2 (1+l1)
	# X = A G
	# G = A^{-1}X
	Xs=np.array([[1.0+a2],[1.0-a2]])
	As=np.array([[1.0-l2 , 1.0-l1],
				 [1.0+l2 , 1.0+l1]])
	Test=np.array([[1.0],
		[1.0-a2*complex(0.0,1.0)]])
	TestA=np.array([[1.0, 1.0],
		[1.0-l2*complex(0.0,1.0), 1.0-l1*complex(0.0,1.0)]])
	Gs=np.linalg.solve(As,Xs)
	return Gs[0,0],Gs[1,0]

def decompose_cosine_expression(A,a,psi,omega):
	g1=0.5*A*complex(cos(psi),sin(psi))
	g2=0.5*A*complex(cos(psi),-sin(psi))
	l1=a*complex(cos(omega),sin(omega))
	l2=a*complex(cos(omega),-sin(omega))
	return g1,g2,l1,l2

def assert_real_eps(z,eps):
	assert abs(z.imag-0.0)<eps

def find_arma32_from_greens(g1,g2,g3,l1,l2,l3):
	assert(abs(g1+g2+g3-complex(1.0,0.0))<1e-7)
	o1=g1*l2+g1*l3+g2*l1+g2*l3+g3*l1+g3*l2
	o2=-(g1*l2*l3+g2*l1*l3+g3*l1*l2)
	p1=l1+l2+l3
	p2=-(l1*l2+l1*l3+l2*l3)
	p3=l1*l2*l3
	assert_real_eps(o1,1e-7)
	assert_real_eps(o2,1e-7)
	assert_real_eps(p1,1e-7)
	assert_real_eps(p2,1e-7)
	assert_real_eps(p3,1e-7)
	return o1.real,o2.real,p1.real,p2.real,p3.real

def find_arma21_from_greens(g1,g2,l1,l2):
	assert(abs(g1+g2-complex(1.0,0.0))<1e-7)
	o1=g1*l2+g2*l1
	p1=l1+l2
	p2=-(l1*l2)
	assert_real_eps(o1,1e-7)
	assert_real_eps(p1,1e-7)
	assert_real_eps(p2,1e-7)

	return o1.real,p1.real,p2.real

if __name__=="__main__":
	#### Problem 3.10 ii)
	print "Problem 3.10 ii)"
	g1,g2,l1,l2=decompose_cosine_expression(1.0,0.8,0.6-0.5*pi,0.8)
	A=1.0/(g1+g2)
	print "A=",A.real
	o1,p1,p2= find_arma21_from_greens(A*g1,A*g2,l1,l2)
	print "ARMA(2,1) Result: $X_t-(%.5f) X_{t-1} - (%.5f) X_{t-2} = a_t - (%.5f) a_{t-1} $"%(p1,p2,o1)
	#### Problem 3.10 iii)
	print "Problem 3.10 iii)"
	g1,g2,l1,l2=decompose_cosine_expression(1.0,0.4,0.6,0.8)
	print g1, g2
	g3=0.4
	l3=0.6
	A = (1.0-g3)/(g1+g2)
	print "A=",A.real
	o1,o2,p1,p2,p3=find_arma32_from_greens(A*g1,A*g2,g3,l1,l2,l3)
	print "ARMA(3,2) Result: $X_t-(%.5f) X_{t-1} - (%.5f) X_{t-2} - (%.5f) X_{t-3} = a_t - (%.5f) a_{t-1} - (%.5f) a_{t-2}$"%(p1,p2,p3,o1,o2)

	#### Problem 3.11
	print 0.4/0.9
	X5=(
		2+
		-2*.4
		+1*.4*0.9
		-1*0.4*0.9*0.9
		+0.5*0.4*pow(0.9,3))
	print X5
	x=[0,0,0,0,0,0]
	a=[0,0.5,-1,1,-2,2]
	for t in range(1,6):
		x[t]=0.9*x[t-1]+a[t]-0.5*a[t-1]
		print "X_{%d}=0.9X_{%d}+a_{%d}-0.5a_{%d}=%0.5f\\\\\\nonumber"%(t,t-1,t,t-1,x[t])

