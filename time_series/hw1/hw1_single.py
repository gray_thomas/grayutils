import numpy as np
import matplotlib.pyplot as plt
from hw1_y import y
from hw1_sales import data, data_labels
from math import sqrt


regressor_1 = np.concatenate([y[0:-1,:]], axis=1)
measured_y=y[1:]
# Q=np.linalg.inves
variance_of_raw_signal = y.T.dot(y)/(np.size(y))
variance_of_parameter_estimates = np.linalg.inv(regressor_1.T.dot(regressor_1)) # assuming unit standard deviation in noise
phi_vector_1 = np.linalg.solve(regressor_1.T.dot(regressor_1),regressor_1.T.dot(y[1:,:]))
predicted_y = regressor_1.dot(phi_vector_1)
residuals_1 = measured_y - predicted_y
first_moment_of_residuals=np.sum(residuals_1)/np.size(residuals_1)
n_degrees_of_freedom=1 # estimating two parameters: bias and autoregression
residual_variance_est=np.sum(residuals_1*residuals_1)/((np.size(residuals_1)-n_degrees_of_freedom))
# variance_phi_1=residual_variance_est*variance_of_parameter_estimates[1,1]
variance_phi_0=residual_variance_est*variance_of_parameter_estimates[0,0]
print "y_t estimate = %.4f * y_{t-1}" % phi_vector_1[0]
print "\phi estimate variance relative to noise variance is %.5f." % variance_of_parameter_estimates[0,0]
print "first modment of residuals should be zero. It is %.7f."%first_moment_of_residuals
print "residual variance is estimated to be %.5f." % residual_variance_est
sum_squared_residuals=residuals_1.T.dot(residuals_1)
print "residual sum squared is %.2f" % sum_squared_residuals
print "variance of raw data is %.5f" % variance_of_raw_signal
print "final estimate of the parameter variance for phi_1 is %.5f." % (variance_phi_0)
# print "std deviations away from zero for \phi_1 is %0.3f." % (phi_vector_1[1]/sqrt(variance_phi_1))
print "std deviations away from zero for \phi_1 is %0.3f." % (phi_vector_1[0]/sqrt(variance_phi_0))


def simulate_1st_order(phi_vector,initial_previous_y,number):
	y_prev=initial_previous_y
	estimates=[y_prev]
	for i in range(0,number):
		y_prev=phi_vector[0]+phi_vector[1]*y_prev
		estimates.append(y_prev)
	return estimates


fig1=plt.figure()
plt.plot(simulate_1st_order(phi_vector_1,1.0,5),linewidth=2)
plt.title("Step response of model")
plt.xlabel("Discrete sample")
plt.ylabel("Model's estimate of y")
fig1.savefig("../../../tex/homework-TimeSeries/hw1/step.png")
fig2=plt.figure()
plt.plot(measured_y,'k',linewidth=2)
plt.plot(predicted_y,'g',linewidth=2)
plt.legend(["Data","Estimates"])
plt.xlabel("Discrete sample")
plt.ylabel("Value")
plt.title("Prediction accuracy")
fig2.savefig("../../../tex/homework-TimeSeries/hw1/prediction.png")

def plot_for_some_n_list(size,ns,figname):
	fig3=plt.figure(figsize=size)
	months=data[:,[0]]*12+data[:,[1]]
	revenue=data[:,[2]]*1e-3
	plt.plot(months,revenue,'k',linewidth=2)
	plt.xlabel("Months AD")
	plt.ylabel("Revenue, G$")
	plt.title("Revenue")
	sum_squared_residuals=[]
	for n in ns:
		Rcs=[np.ones((np.size(months)-n,1)), months[n:,:]]
		for i in range(n-1,-1,-1):
			Rcs.append(revenue[i:-n+i,:])
		Ra=np.concatenate(Rcs,axis=1)
		phi_a, residuals_squared_a, q, sigmas_a = np.linalg.lstsq(Ra,revenue[n:,:])
		sum_squared_residuals.append(residuals_squared_a/(np.size(months)-1))
		print Ra.shape, phi_a.shape, months[n:,:].shape
		revenue_estimate=Ra.dot(phi_a)
		plt.plot(months[n:,:],revenue_estimate)
	legends=["Data"]
	for n,ssr in zip(ns,sum_squared_residuals):
		legends.append("AR(%d), $\\frac{1}{n+1}\Sigma r^2=%0.2e $"%(n,ssr))
	plt.legend(legends,loc='best')
	fig3.savefig(figname)
plot_for_some_n_list((12,10),[0,1,2,6,12,24],"../../../tex/homework-TimeSeries/hw1/other_revenue.png")
plot_for_some_n_list((5,5),[0],"../../../tex/homework-TimeSeries/hw1/simple.png")
# print a
# print y
plt.show()