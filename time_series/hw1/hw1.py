import numpy as np
import matplotlib.pyplot as plt
from hw1_y import y
from hw1_sales import data, data_labels
from math import sqrt
from time_series import *

if False: # set true to regen plots from part 1
	regressor_1 = np.concatenate([np.ones((49,1)),y[0:-1,:]], axis=1)
	measured_y=y[1:]
	# Q=np.linalg.inves
	variance_of_raw_signal = y.T.dot(y)/(np.size(y))
	variance_of_parameter_estimates = np.linalg.inv(
		regressor_1.T.dot(regressor_1)) 
		# assuming unit standard deviation in noise
	phi_vector_1 = np.linalg.solve(
		regressor_1.T.dot(regressor_1),regressor_1.T.dot(y[1:,:]))
	predicted_y = regressor_1.dot(phi_vector_1)
	residuals_1 = measured_y - predicted_y
	first_moment_of_residuals=np.sum(residuals_1)/np.size(residuals_1)
	n_degrees_of_freedom=2 # estimating two parameters: bias and autoregression
	residual_variance_est=np.sum(residuals_1*residuals_1)/(
		(np.size(residuals_1)-n_degrees_of_freedom))
	variance_phi_1=residual_variance_est*variance_of_parameter_estimates[1,1]
	variance_phi_0=residual_variance_est*variance_of_parameter_estimates[0,0]
	print "y_t estimate = %.4f + %.4f * y_{t-1}" % (
		phi_vector_1[0],phi_vector_1[1])
	print ("\phi estimate variance relative to noise variance is %.5f." % 
		variance_of_parameter_estimates[1,1])
	print ("first modment of residuals should be zero. It is %.7f."%
		first_moment_of_residuals)
	print "residual variance is estimated to be %.5f." % residual_variance_est
	sum_squared_residuals=residuals_1.T.dot(residuals_1)
	print "residual sum squared is %.2f" % sum_squared_residuals
	print "variance of raw data is %.5f" % variance_of_raw_signal
	print "final estimate of the parameter variance for phi_1 is %.5f." % (
		variance_phi_1)
	print "std deviations away from zero for \phi_1 is %0.3f." % (
		phi_vector_1[1]/sqrt(variance_phi_1))
	print "std deviations away from zero for \phi_0 is %0.3f." % (
		phi_vector_1[0]/sqrt(variance_phi_0))


	def simulate_1st_order(phi_vector,initial_previous_y,number):
		y_prev=initial_previous_y
		estimates=[y_prev]
		for i in range(0,number):
			y_prev=phi_vector[0]+phi_vector[1]*y_prev
			estimates.append(y_prev)
		return estimates


	fig1=plt.figure()
	plt.plot(simulate_1st_order(phi_vector_1,1.0,5),linewidth=2)
	plt.title("Step response of model")
	plt.xlabel("Discrete sample")
	plt.ylabel("Model's estimate of y")
	fig1.savefig("../../../tex/homework-TimeSeries/hw1/step.png")
	fig2=plt.figure()
	plt.plot(measured_y,'k',linewidth=2)
	plt.plot(predicted_y,'g',linewidth=2)
	plt.legend(["Data","Estimates"])
	plt.xlabel("Discrete sample")
	plt.ylabel("Value")
	plt.title("Prediction accuracy")
	fig2.savefig("../../../tex/homework-TimeSeries/hw1/prediction.png")

#########################3 Refactored version ###############
if False: # set true to regen plots from part 1
	ar1=AutoRegressive(1)
	ar1.regress(y)
	ar1.check(extra_degrees=0)

	
	print "y_t estimate = %.4f * y_{t-1}" % (ar1.params[0])
	print ("\phi estimate variance relative to noise variance is %.5f." % 
		ar1.variance_ratios)
	print "residual variance is estimated to be %.5f." % covariance(ar1.residuals,ar1.residuals)
	print "residual sum squared is %.2f" % ar1.sum_of_squared_residuals
	print "variance of raw data is %.5f" % covariance(y,y)
	print "final estimate of the parameter variance for phi_1 is %.5f." % (
		ar1.param_covariance[0,0])
	print "std deviations away from zero for \phi_1 is %0.3f." % (
		ar1.params[0]/sqrt(ar1.param_covariance[0,0]))

	def simulate_1st_order(initial_previous_y,number):
		y_prev=initial_previous_y
		e_max=initial_previous_y
		e_min=initial_previous_y
		e_mins=[y_prev]
		e_maxs=[y_prev]
		estimates=[y_prev]
		for i in range(0,number):
			y_prev=ar1.params[0]*y_prev
			options=[(ar1.params[0]-ar1.c2std)*e_min,(ar1.params[0]-ar1.c2std)*e_max,
				(ar1.params[0]+ar1.c2std)*e_min,(ar1.params[0]+ar1.c2std)*e_max]
			e_max=max(options)+2*ar1.residual_std_dev
			e_min=min(options)-2*ar1.residual_std_dev
			estimates.append(y_prev)
			e_maxs.append(e_max)
			e_mins.append(e_min)
		plt.plot(estimates,'k',linewidth=2)
		plt.plot(e_maxs,'b',linewidth=2)
		plt.plot(e_mins,'b',linewidth=2)
		return estimates, e_min, e_max


	fig1=plt.figure()
	simulate_1st_order(1.0,10)
	plt.title("Step response of model")
	plt.xlabel("Discrete sample")
	plt.ylabel("Model's estimate of y")
	fig1.savefig("../../../tex/homework-TimeSeries/hw1/step.png")
	fig2=plt.figure()
	plt.plot(ar1.predictable_measurements,'k',linewidth=2)
	plt.plot(ar1.predictions,'g',linewidth=2)
	plt.legend(["Data","Estimates"])
	plt.xlabel("Discrete sample")
	plt.ylabel("Value")
	plt.title("Prediction accuracy")
	fig2.savefig("../../../tex/homework-TimeSeries/hw1/prediction.png")

	auto_correlation_plot(ar1.residuals,20,"AR(1) Residual Autocorrelation",
		"../../../tex/homework-TimeSeries/hw1/p1acorr.png")
	auto_correlation_plot(y,20,"Data Autocorrelation",
		"../../../tex/homework-TimeSeries/hw1/p1dacorr.png")


	# plt.sho

def plot_for_some_n_list(size,ns,figname,residualplot=None):
	fig3=plt.figure(figsize=size)
	months=data[:,[0]]*12+data[:,[1]]
	months_from_start=months-np.ones(months.shape)*months[0]
	revenue=data[:,[2]]*1e-6
	plt.plot(months_from_start,revenue,'k',linewidth=2)
	plt.xlabel("T")
	plt.ylabel("Revenue, T$")
	plt.title("Revenue")
	sum_squared_residuals=[]
	residual_variances=[]
	residuals=[]
	for n in ns:
		Rcs=[np.ones((np.size(months_from_start)-n,1)), months_from_start[n:,:]]
		for i in range(n-1,-1,-1):
			Rcs.append(revenue[i:-n+i,:])
		Ra=np.concatenate(Rcs,axis=1)
		phi_a, _, q, sigmas_a = np.linalg.lstsq(Ra,revenue[n:,:])
		# sum_squared_residuals.append(residuals_squared_a/
			# (np.size(months_from_start)-1))
		revenue_estimate=Ra.dot(phi_a)
		residual=revenue[n:,:]-Ra.dot(phi_a)
		residuals.append(residual)
		sum_squared_residual=residual.T.dot(residual)[0,0]
		sum_squared_residuals.append(sum_squared_residual)
		residual_variance=sum_squared_residual/(np.size(residuals)-n-2) 
		residual_variances.append(residual_variance)
		# the -2 above is for the degrees of freedom 
		# from the linear and bias regressors
		plt.plot(months_from_start[n:,:],revenue_estimate)
	legends=["Data"]
	for i,n,ssr,var in zip(
		range(0,len(ns)),ns,sum_squared_residuals,residual_variances):
		legends.append(
			"AR(%d), $\Sigma r^2=%0.2e, \; \; \\frac{\Sigma r^2}{N-%d}=%0.2e $"%\
			(n,ssr,i+2,var))
	plt.legend(legends,loc='best')
	plt.tight_layout()
	fig3.savefig(figname)
	if residualplot!=None:
		fig5=plt.figure(figsize=size)
		plt.xlabel("T")
		if len(ns)==1:
			plt.title("AR(%d) residuals"%(n))
		plt.ylabel("Revenue, T$")
		for i in range(0,len(ns)):
			n=ns[i]
			plt.plot(months_from_start[n:,:],residuals[i])
		fig5.savefig(residualplot)


def fitting_plot(size,ns,figname):
	months=data[:,[0]]*12+data[:,[1]]
	months_from_start=months-np.ones(months.shape)*months[0]
	revenue=data[:,[2]]*1e-6
	fig4=plt.figure(figsize=size)
	sum_squared_residuals=[]
	residual_variances=[]
	for n in ns:
		Rcs=[np.ones((np.size(months_from_start)-n,1)), months_from_start[n:,:]]
		for i in range(n-1,-1,-1):
			Rcs.append(revenue[i:-n+i,:])
		Ra=np.concatenate(Rcs,axis=1)
		phi_a, residuals_squared_a, q, sigmas_a = np.linalg.lstsq(
			Ra,revenue[n:,:])
		residual=revenue[n:,:]-Ra.dot(phi_a)
		sum_squared_residual=residual.T.dot(residual)[0,0]
		residual_variance=sum_squared_residual/(np.size(residual)-n-2) 
		sum_squared_residuals.append(sum_squared_residual)
		residual_variances.append(residual_variance)
	
	plt.semilogy(ns,residual_variances,'b',linewidth=3)
	plt.semilogy(ns,sum_squared_residuals,'g',linewidth=3)
	plt.title("Performance of autoregressive models")
	plt.xlabel("Model order")
	plt.legend(["variance of residuals","sum squared residuals"])
	plt.tight_layout()
	fig4.savefig(figname)

#### Reimplementation of part 2
# obtain unbiased data
months=data[:,[0]]*12+data[:,[1]]
months_from_start=months-np.ones(months.shape)*months[0]
RawRevenue=data[:,[2]]*1e-6
Rc0=np.concatenate([np.ones((np.size(months_from_start),1)), months_from_start[:,:]],axis=1)
print Rc0.shape
phi0,_,_,_=np.linalg.lstsq(Rc0,RawRevenue[:,:])
print phi0.shape
revenue=RawRevenue[:,:]-Rc0.dot(phi0)
if False:
	figa=plt.figure(figsize=(8,6))
	plt.plot(RawRevenue)
	plt.plot(Rc0.dot(phi0))
	plt.xlabel("T")
	plt.ylabel("Revenue, T$")
	plt.title("Revenue")
	plt.legend(["revenue","determanistic_trend"],loc='best')
	figa.savefig("../../../tex/homework-TimeSeries/hw1/simple.png")
ar2=AutoRegressive(2)
ar2.regress(revenue)
ar2.check(extra_degrees=2)
# auto_correlation_plot(ar2.residuals,20,"AR(2) Residual Autocorrelation",
# 		"../../../tex/homework-TimeSeries/hw1/p2acorr2.png")
ar4=AutoRegressive(4)
ar4.regress(revenue)
ar4.check(extra_degrees=2)
# auto_correlation_plot(ar4.residuals,20,"AR(4) Residual Autocorrelation",
# 		"../../../tex/homework-TimeSeries/hw1/p2acorr4.png")
ar12=AutoRegressive(12)
ar12.regress(revenue)
print "ar12 params", ar12.params
ar12.check(extra_degrees=2)
plt.figure()
plt.plot(ar12.residuals)


def plot_for_some_n_list2(size,ns,figname,residualplot=None):
	fig3=plt.figure(figsize=size)

	plt.plot(months_from_start,revenue,'k',linewidth=2)
	plt.xlabel("T")
	plt.ylabel("Revenue, T$")
	plt.title("Revenue")
	sum_squared_residuals=[]
	residual_variances=[]
	residuals=[]
	for n in ns:
		Rcs=[]
		for i in range(n-1,-1,-1):
			Rcs.append(revenue[i:-n+i,:])
		Ra=np.concatenate(Rcs,axis=1)
		phi_a, _, q, sigmas_a = np.linalg.lstsq(Ra,revenue[n:,:])
		# sum_squared_residuals.append(residuals_squared_a/
			# (np.size(months_from_start)-1))
		revenue_estimate=Ra.dot(phi_a)
		residual=revenue[n:,:]-Ra.dot(phi_a)
		residuals.append(residual)
		sum_squared_residual=residual.T.dot(residual)[0,0]
		sum_squared_residuals.append(sum_squared_residual)
		residual_variance=sum_squared_residual/(np.size(residuals)-n-2) 
		residual_variances.append(residual_variance)
		if True:
			fig5=plt.figure(figsize=(8,6))
			print residual-ar12.residuals
			auto_correlation_plot(residual,20,"Residual autocorrelation")
			plt.show()
		# the -2 above is for the degrees of freedom 
		# from the linear and bias regressors
		plt.plot(months_from_start[n:,:],revenue_estimate)
	legends=["Data"]
	for i,n,ssr,var in zip(
		range(0,len(ns)),ns,sum_squared_residuals,residual_variances):
		legends.append(
			"AR(%d), $\Sigma r^2=%0.2e, \; \; \\frac{\Sigma r^2}{N-%d}=%0.2e $"%\
			(n,ssr,i+2,var))
	plt.legend(legends,loc='best')
	plt.tight_layout()
	fig3.savefig(figname)
	if residualplot!=None:
		fig5=plt.figure(figsize=size)
		plt.xlabel("T")
		if len(ns)==1:
			plt.title("AR(%d) residuals"%(n))
		plt.ylabel("Revenue, T$")
		for i in range(0,len(ns)):
			n=ns[i]
			plt.plot(months_from_start[n:,:],residuals[i])
		fig5.savefig(residualplot)
# plot_for_some_n_list2((8,6),[12],"fig")
# auto_correlation_plot(ar12.residuals,20,"AR(12) Residual Autocorrelation",
# 		"../../../tex/homework-TimeSeries/hw1/p2acorr12.png")

def fitting_plot2(size,ns,figname):
	fig4=plt.figure(figsize=size)
	sum_squared_residuals=[]
	residual_variances=[]
	for n in ns:
		Rcs=[]
		for i in range(n-1,-1,-1):
			Rcs.append(revenue[i:-n+i,:])
		Ra=np.concatenate(Rcs,axis=1)
		phi_a, residuals_squared_a, q, sigmas_a = np.linalg.lstsq(
			Ra,revenue[n:,:])
		residual=revenue[n:,:]-Ra.dot(phi_a)
		sum_squared_residual=residual.T.dot(residual)[0,0]
		residual_variance=sum_squared_residual/(np.size(residual)-n-2) 
		sum_squared_residuals.append(sum_squared_residual)
		residual_variances.append(residual_variance)
	
	plt.semilogy(ns,residual_variances,'b',linewidth=3)
	plt.semilogy(ns,sum_squared_residuals,'g',linewidth=3)
	plt.title("Performance of autoregressive models")
	plt.xlabel("Model order")
	plt.legend(["variance of residuals","sum squared residuals"])
	plt.tight_layout()
	fig4.savefig(figname)

############# Uncomment the command to regenerate figures ############
# plot_for_some_n_list2((12,10),
# 	[1,2,6,12,24],"../../../tex/homework-TimeSeries/hw1/other_revenue.png")
# # plot_for_some_n_list((8,6),[0],
# # 	"../../../tex/homework-TimeSeries/hw1/simple.png")
# plot_for_some_n_list2((8,6),[2],"../../../tex/homework-TimeSeries/hw1/ar2.png",
# 	residualplot="../../../tex/homework-TimeSeries/hw1/arRes2.png")
# plot_for_some_n_list2((8,6),[4],"../../../tex/homework-TimeSeries/hw1/ar4.png",
# 	residualplot="../../../tex/homework-TimeSeries/hw1/arRes4.png")
# fitting_plot2((8,6),range(1,40),
# 	"../../../tex/homework-TimeSeries/hw1/residuals.pdf")
# print a
# print y
plt.show()