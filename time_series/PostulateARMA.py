from scipy.stats import f as fischer_statistic
import statsmodels.api as sm
import numpy as np 

def postulateARMA(ts, P):
	"""
	Variable ts is the time-series and P is the level of significance that will be used for F-tests associated with model fitting. This function returns a tuple (m, model, res), where m is the mean, model is the ARMA(2n, 2n-1) model object, and res is presumably the residual.
	Adapted from PostulateARMA.m by Dragan Djurdjanovic.
	"""
	m = np.mean(ts, axis=0)
	(N, vectorial_n)=ts.shape
	ts = ts - (np.ones((N, vectorial_n))).dot(np.diagflat(m))
	cycle = 1
	single_time_series = (vectorial_n==1)

	if single_time_series:
		CurrentModel=sm.tsa.ARMA(ts, order=(14,13)).fit(transparams=False, trend='nc', solver='nm')
		print dir(CurrentModel)
		print CurrentModel.summary()
		print CurrentModel.summary2()
		# print CurrentModel.df_resid
		print CurrentModel.resid.shape
		print "Rss", CurrentModel.resid.reshape((-1,1)).T.dot(CurrentModel.resid.reshape((-1,1)))
		print 2.41597282851e+11
		print 1.23091233397e+11
		print 6.43849123e+10
	else:
		CurrentModel = []
		for i in range(0,vectorial_n):
			exog=ts[:,[q for q in range(0,vectorial_n) if (q != i)]]
			CurrentModel.append(sm.tsa.ARMA(ts[:,[i]], order=(2,1), exog=exog).fit())
		print CurrentModel[0]


if __name__=="__main__":
	# print np.__version__ 
	# sm.test()
	# exit()
	import hw1_sales
	ts_pre=hw1_sales.data[:,[2]]
	l=ts_pre.shape[0]
	R=np.concatenate([np.ones((l,1)),np.array([range(0,l)]).T],axis=1)
	print R.shape
	print np.linalg.solve.__doc__
	phi=np.linalg.solve((R.T.dot(R)),R.T.dot(ts_pre))
	print phi
	ts=ts_pre - R.dot(phi)
	(m, model, res) = postulateARMA(ts, 0.95)
	# import matplotlib.pyplot as plt 
	# plt.plot(ts)
	# plt.plot(ts_pre)
	# plt.show()
