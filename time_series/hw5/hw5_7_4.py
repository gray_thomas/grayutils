from math import sqrt, exp

a1=3
a2=1.9

mu_1=-1.5+sqrt(7.0/20.0)
mu_2=-1.5-sqrt(7.0/20.0)

gamma_t=lambda (sigma_z, t): sigma_z**2 * (exp(mu_1*t)/mu_1 - exp(mu_2*t)/mu_2)/(2*(mu_1**2-mu_2**2))
coeff_gamma_t_1=1.0/mu_1 /(2*(mu_1**2-mu_2**2))
coeff_gamma_t_2=-1.0/mu_2/(2*(mu_1**2-mu_2**2))
print coeff_gamma_t_1, coeff_gamma_t_2

delta=0.1
l1=exp(delta*mu_1)
l2=exp(delta*mu_2)
print "\lambda_1 = ", l1
print "\lambda_2 = ", l2

# Delta = 1.0
# k=1: 0.0542012158396
# k=2: 0.0241783475497
# k=10: 1.75959867244e-05
# k=20: 1.99672645521e-09
# k=100: 5.48968187633e-41

# Delta = 0.1 
# k=1: 0.0869645768726
# k=2: 0.0849802108024
# k=10: 0.0542012158396
# k=20: 0.0241783475497
# k=100: 1.75959867244e-05

# sigma_z**2 * coeff_gamma_t_1 = sigma_a**2 * Gamma_1
# sigma_z**2/sigma_a**2 = Gamma_1/coeff_gamma_t_1 (a quadratic in theta_1)
# sigma_z**2/sigma_a**2 = Gamma_2/coeff_gamma_t_2
Gamma_1_quadratic_term = 1.0/((l2-l1)**2 * (1.0-l1**2))+1.0/((l1-l2)*(l2-l1)*(1.0-l1*l2))
Gamma_1_linear_term = (-2*l1)/((l2-l1)**2 * (1.0-l1**2))+(-l1-l2)/((l1-l2)*(l2-l1)*(1.0-l1*l2))
Gamma_1_const_term = (l1**2)/((l2-l1)**2 * (1.0-l1**2))+(l1*l2)/((l1-l2)*(l2-l1)*(1.0-l1*l2))

Gamma_2_quadratic_term = 1.0/((l2-l1)**2 * (1.0-l2**2)) + 1.0/ ((l1-l2)*(l2-l1)*(1.0-l1*l2))
Gamma_2_linear_term = (-2*l2)/((l2-l1)**2 * (1.0-l2**2)) + (-l1-l2)/ ((l1-l2)*(l2-l1)*(1.0-l1*l2))
Gamma_2_const_term = (l2**2)/((l2-l1)**2 * (1.0-l2**2)) + (l1*l2)/ ((l1-l2)*(l2-l1)*(1.0-l1*l2))

G1q, G1l,G1c = Gamma_1_quadratic_term, Gamma_1_linear_term, Gamma_1_const_term
G2q,G2l,G2c = Gamma_2_quadratic_term, Gamma_2_linear_term, Gamma_2_const_term



c0=Gamma_1_quadratic_term/coeff_gamma_t_1-Gamma_2_quadratic_term/coeff_gamma_t_2
c1=(Gamma_1_linear_term/coeff_gamma_t_1-Gamma_2_linear_term/coeff_gamma_t_2)/c0
c2=(Gamma_1_const_term/coeff_gamma_t_1-Gamma_2_const_term/coeff_gamma_t_2)/c0

print Gamma_1_quadratic_term/coeff_gamma_t_1, Gamma_1_linear_term/coeff_gamma_t_1, Gamma_1_const_term/coeff_gamma_t_1
print Gamma_2_quadratic_term/coeff_gamma_t_2, Gamma_2_linear_term/coeff_gamma_t_2, Gamma_2_const_term/coeff_gamma_t_2
print c1, c2
th1a=-c1/2+sqrt(c1**2/4-c2)
th1b=-c1/2-sqrt(c1**2/4-c2)
print th1a, th1b
print th1a**2+c1*th1a+c2
print th1b**2+c1*th1b+c2
print "sigma_z^2/sigma_a^2 = ",(G1q*th1a**2+G1l*th1a+G1c)/coeff_gamma_t_1 
print (G2q*th1a**2+G2l*th1a+G2c)/coeff_gamma_t_2
print (G1q*th1b**2+G1l*th1b+G1c)/coeff_gamma_t_1 
print (G2q*th1b**2+G2l*th1b+G2c)/coeff_gamma_t_2 
print "sigma_a^2/sigma_z^2 = ",1.0/((G1q*th1a**2+G1l*th1a+G1c)/coeff_gamma_t_1 )
siga2_over_sigmaz2=1.0/((G1q*th1a**2+G1l*th1a+G1c)/coeff_gamma_t_1)

# this is to check:
g1=lambda (l1, l2, th1): (l1-th1)/(l1-l2)
g2=lambda (l1, l2, th1): (l2-th1)/(l2-l1)
Gamma1=lambda (l1, l2, g1, g2): g1*g1/(1-l1*l1)+g1*g2/(1-l1*l2)
Gamma2=lambda (l1, l2, g1, g2): g2*g2/(1-l2*l2)+g1*g2/(1-l1*l2)
gamma_k = lambda (l1, l2, Gamma1, Gamma2, sigma_a2,k): sigma_a2*(Gamma1*pow(l1,k)+Gamma2*pow(l2,k))


eg1=g1((l1, l2, th1a))
eg2=g2((l1, l2, th1a))
eG1=Gamma1((l1, l2, eg1, eg2))
eG2=Gamma2((l1, l2, eg1, eg2))
gamma_10 = gamma_k((l1, l2, eG1, eG2, siga2_over_sigmaz2, 10))
print gamma_k((l1, l2, eG1, eG2, siga2_over_sigmaz2, 1))
print gamma_t((1.0,delta))
print coeff_gamma_t_1*l1+coeff_gamma_t_2*l2
print (eG1*l1+eG2*l2)*siga2_over_sigmaz2
print gamma_k((l1, l2, eG1, eG2, siga2_over_sigmaz2, 2))
print gamma_k((l1, l2, eG1, eG2, siga2_over_sigmaz2, 10))
print gamma_k((l1, l2, eG1, eG2, siga2_over_sigmaz2, 20))
print gamma_k((l1, l2, eG1, eG2, siga2_over_sigmaz2, 100))


