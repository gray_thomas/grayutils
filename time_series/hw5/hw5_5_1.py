import numpy as np
from math import sqrt, atan2,pi
from hw3tools import *
# P=np.array([[-1,0.2,0.25,0.05],[1,-1,1,0]]) # P2
# P=np.array([[-1,1.5,-0.6],[1, -0.5,0.0]]) # P3.2 i
# P=np.array([[-1,2.0,-0.6,0.2],[1,-0.6,-0.5,0.0]]) # P3.2 iii
# P=np.array([[-1.0, 1.0, -1.0],[1.0,-3.0,0.0]]) # P3 1
P=np.array([[-1.0, 1.0, -0.3],[1.0,0.4,0.0]]) # P5.1
P1=np.concatenate([np.zeros((2,1)),P],axis=1)
P0=np.concatenate([P,np.zeros((2,1))],axis=1)
system="0="
for i in range(0,P.shape[1]):
	system+=("+X")
for i in range(0,P.shape[1]):
	system+=("+a")


number=3
res=np.array(P)
for i in range (1,number+1):
	P1=np.concatenate([np.zeros((2,i)),P],axis=1)
	P0=np.concatenate([res,np.zeros((2,1))],axis=1)
	res=P0+P1*P0[0,i]
string="0="
for i in range(0,res.shape[1]):
	string+= "+%0.5f X_{t+%d}"%(res[0,i],1+number-i)
for i in range(0,res.shape[1]):
	string+= " +%0.5f a_{t+%d}"%(res[1,i],1+number-i)
print string
# for i in range(0,min(res.shape[1],20)):
# 	print "G_{%d} =& %0.8f \\\\\\nonumber" % (i+1,res[1,i])

l2s,l1s = quadratic(1,-1,1)
print "hey", l1s,l2s
g1s,g2s = find_gs(l1s,l2s,-3.0)
print "gs=",g1s,g2s

l1=complex(0.5,-0.5*sqrt(3.0))
l2=complex(0.5,0.5*sqrt(3.0))
print l1, l2
A=complex(0.5,-2.5/sqrt(3.0))
B=complex(0.5,2.5/sqrt(3.0))
print "Gs A, B=", A, B

for i in range(0,5):
	print "G_",i
	print A *pow(l1,i) + B * pow(l2,i)
	print g1s *pow(l1s,i) + g2s * pow(l2s,i)
print atan2(l1.imag, -l1.real)
print 2.0*pi/6.0
print l1+l2, l1*l2
print sqrt(pow(A.real,2)+pow(A.imag,2))
print sqrt(pow(B.real,2)+pow(B.imag,2))


for l in range (0,0):
	print "\gamma_{%d}=(%d)\sigma_a^2 \\\\\\nonumber"%(l,autocovar(l1s,l2s,g1s,g2s,l,1.0).real)
for l in range (0,0):
	print "\\rho_{%d}=(%f)\sigma_a^2 \\\\\\nonumber"%(l,autocorr(l1,l2,A,B,l).real)
l1,l2=quadratic(1,-1.1,0.3)
g1,g2=find_gs(l1,l2,0.6)
print "gs", g1, g2
print "ls", l1,l2
for i in range(0,0):
	print "G_{%d} =& %0.8f \\\\\\nonumber" %(i+1,g1 *pow(l1,i) + g2 * pow(l2,i))
for l in range (0,0):
	print "\\rho_{%d}=(%f) \\\\\\nonumber"%(l,autocorr(l1,l2,g1,g2,l).real)