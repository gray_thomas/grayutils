from __future__ import print_function
import matplotlib.pyplot as plt 
import numpy as np
import statsmodels.api as sm
import pandas as pd
from statsmodels.tsa.arima_process import arma_generate_sample
import pickle
dta=pickle.load(open("hw5_1.pkl","rb"))
dates = sm.tsa.datetools.dates_from_range('1992m1', length=dta.shape[0])
y = pd.TimeSeries(dta, index=dates)
arma_mod = sm.tsa.ARMA(y, order=(4,3))
arma_res = arma_mod.fit(start_params=np.array([0.07,-0.23,0.0,0.0,-0.136,0.0,0.0]),trend='nc', solver='lbfgs')
print(y.tail())

print(arma_res.summary2())
raw_residuals=arma_res.resid.values.reshape((-1,1))
print(raw_residuals.T.dot(raw_residuals))

import matplotlib.pyplot as plt
fig, ax = plt.subplots(figsize=(10,8))
fig = arma_res.plot_predict(start='1992m1', end='2010m5', ax=ax)
legend = ax.legend(loc='upper left')
plt.show()
