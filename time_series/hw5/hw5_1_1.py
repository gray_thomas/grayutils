from __future__ import print_function
import numpy as np
import statsmodels.api as sm
from scipy import stats
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from statsmodels.graphics.api import qqplot
from hw1_sales import data, data_labels


y=data[:,2]
nsample = y.shape[0]
sig = 0.5
x = np.linspace(0, 12*data[-1,0]+data[-1,1]-12*data[0,0]-data[0,1], nsample)
X = np.column_stack((x, np.ones(nsample)))
beta = [0.5, 0.5, -0.02, 5.]



res = sm.OLS(y, X).fit()
print(res.summary())
print('Parameters: ', res.params)
print('Standard errors: ', res.bse)
# print('Predicted values: ', res.predict())

prstd, iv_l, iv_u = wls_prediction_std(res)

fig, ax = plt.subplots(figsize=(8,6))

ax.plot(x, y, 'o', label="data")
ax.plot(x, res.fittedvalues, 'r--.', label="OLS")
ax.plot(x, iv_u, 'r--')
ax.plot(x, iv_l, 'r--')
ax.legend(loc='best');
ax.set_xlabel("months")
ax.set_ylabel("M$")
fig.savefig("../../../tex/homework-TimeSeries/hw5/hw5_1_1.pdf")
import pickle
dta=res.resid
pickle.dump(dta,open("hw5_1.pkl","wb"))

plt.show()