from cmath import sqrt, exp, log

a1=10.0
a2=100.0

mu_1=-a1*0.5+sqrt((a1*0.5)**2-a2)
mu_2=-a1*0.5-sqrt((a1*0.5)**2-a2)
print "mu_1", mu_1
print "mu_2", mu_2

impulse_t=lambda (sigma_z, t): sigma_z * (exp(mu_1*t) - exp(mu_2*t))/(mu_1-mu_2)
coeff_1=1.0/(mu_1-mu_2)
coeff_2=-1.0/(mu_1-mu_2)
print "coeffss", coeff_1, coeff_2

delta=0.1
l1=exp(delta*mu_1)
l2=exp(delta*mu_2)
print "\lambda_1 = ", l1, 'norm', abs(l1)
print "\lambda_2 = ", l2, 'norm', abs(l2)

# coeff_1 * sigma_z = g_1 * sigma_a  # factor of l1**k
# coeff_2 * sigma_z = g_2 * sigma_a  # factor of l2**k

# sigma_z/sigma_a = g1_th * theta_1
a=coeff_1+coeff_2
b=-l2*coeff_1 - l1*coeff_2
print "a", a
print "b", b


print exp(-5.5*1.0)
print exp(-5.5*0.1)
print exp(-5.5*0.01)
print exp(-5.5*0.001)

def first_order(L,RSS,N):
	s2a=19355.9/(369-2)
	a=log(L)
	s2z=a*s2a/(1-exp(-a))
	return a, s2z

print first_order(0.998,19355.9,369)
print first_order(0.98,15.58,160)
print first_order(0.63,1217.31,250)

