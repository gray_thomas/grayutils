function [total_model]=test_parsimonious_model(...
                                            ts, ar_reduction, model, P,varargin)
                                        

%% Set params
if nargin<5
opt=armaxOptions;
opt.SearchOption.Tolerance =1e-5;
opt.SearchOption.MaxIter = 50;
else
    opt=varargin{1};
end
%% Copy and update model
model_init=idpoly(model.a, model.b,model.c,model.d,model.f,model.NoiseVariance, model.Ts);
model_v2=pem(ts,model_init,opt);
e_1=pe(model,ts);
rss_1=e_1' * e_1;
fprintf('original model rss  %f\n',rss_1);
e_1=pe(model_v2,ts);
rss_1=e_1' * e_1;
fprintf('improved model rss  %f\n',rss_1);
%% Obtain pre-filtered series
y=filter(ar_reduction,1,ts);

%% Build a parsimonious initial condition
AR_reduction=length(ar_reduction)-1;
AR_size=length(model.a)-1;
MA_size=length(model.c)-1;
[N,~]=size(ts);


%% Copy, convert to parsimonious form, and update model
[q,r]=deconv(model_v2.a,ar_reduction);
parsimonious_init=idpoly(q, model.b,model.c,model.d,model.f,model.NoiseVariance, model.Ts);
q2i=pe(parsimonious_init,y);
parsimonious_model=pem(y,parsimonious_init,opt);
e2=pe(parsimonious_model,y);
rss_2=e2'*e2;
fprintf('initial  p-mod rss  %f\n',q2i'*q2i);
fprintf('improved p-mod rss  %f\n',rss_2);




Nmod=N-AR_size-MA_size-1;
control_statistic=finv(P,AR_reduction,Nmod);
test_statistic=((rss_2-rss_1)/AR_reduction)/(rss_1/(Nmod));
if test_statistic<control_statistic
    fprintf('simple model seems reasonable! %f < %f\n',test_statistic, control_statistic)
else
    fprintf('simple model sacrifices too much! %f >= %f \n', test_statistic, control_statistic)
end
total_model_init=idpoly(conv(ar_reduction,parsimonious_init.a),...
    [],parsimonious_init.c,parsimonious_init.d,[],...
    parsimonious_init.NoiseVariance,parsimonious_init.Ts);
total_model=idpoly(conv(ar_reduction,parsimonious_model.a),...
    [],parsimonious_model.c,parsimonious_model.d,[],...
    parsimonious_model.NoiseVariance,parsimonious_model.Ts);
e3=pe(total_model,ts);
rsst=e3'*e3;
fprintf('improved t-mod rss  %f\n',rsst);
compare(ts, model, model_v2, total_model,total_model_init)


end