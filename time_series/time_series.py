import numpy as np 
from math import sqrt
import random
import matplotlib.pyplot as plt

class AutoRegressive(object):
	def __init__(self,order):
		self.order=order
	def regress(self,rdata):
		data=rdata.reshape((-1))
		N = np.size(data)-self.order
		reg=np.zeros((np.size(data)-self.order,self.order))
		for i in range(0,self.order):
			reg[:,self.order-i-1]=data[i:-self.order+i]
		rTrInv = np.linalg.inv(	reg.T.dot( reg ) )
		Y = data[ self.order: ]
		phi = rTrInv * reg.T.dot( Y )
		Yhat = reg.dot( phi )
		a = Y-Yhat[:,0]
		print Y.shape
		print Yhat.shape
		Ea2 = np.sum(a*a)
		sigma_2_a = Ea2/(N-self.order)
		self.N = N
		self.regressor = reg
		self.variance_ratios = rTrInv
		self.predictable_measurements = Y
		self.params = phi
		self.predictions = Yhat
		self.residuals = a
		self.sum_of_squared_residuals = Ea2
		self.data=data
	def check(self,extra_degrees=0):
		self.residual_variance = self.sum_of_squared_residuals /(
			self.N - self.order - extra_degrees)
		self.residual_std_dev = sqrt(self.residual_variance)
		self.param_covariance=self.variance_ratios*self.residual_variance
		self.c2std=2*np.sqrt(np.diag(self.param_covariance))
		print "hey", self.c2std

def auto_correlations(data,max,extra_degrees=0):
	h=1/auto_covariance(data,0)
	ret=[]
	for i in range (0,max):
		ret.append(auto_covariance(data,i)*h)
	return ret

def auto_covariance(data,k):
	if k==0:
		return covariance(data,data)
	return np.sum(data[:-k]*data[k:])/(np.size(data)-k)

def correlation(A,B):
	return covariance(sscore(A),sscore(B))

def covariance(A,B):
	assert np.size(A)==np.size(B)
	return np.sum(A*B)/np.size(B)

def sscore(A):
	mu=np.sum(A)/np.size(A)
	Aprime=A-np.ones(A.shape)*mu
	var=np.sum(Aprime*Aprime)/(np.size(A)-1)
	# -1 for the mu estimation
	sigma=sqrt(var)
	ret=Aprime/sigma
	return ret

def auto_correlation_plot(data,maxK,title,loc=None):
	fig=plt.figure()
	corr=auto_correlations(data,maxK)
	plt.plot( corr,'ko--',linewidth=1,markersize=5)
	err95=2/sqrt(np.size(data))
	plt.plot( [0,maxK], [err95,err95], 'b')
	plt.plot( [0,maxK], [-err95,-err95], 'b')
	plt.title(title)
	plt.xlabel("K")
	plt.ylabel("Correlation")
	fig.tight_layout()
	if loc!=None:
		fig.savefig(loc)

def main():
	ar1=AutoRegressive(1)
	# print dir(np.random)
	N=100000
	true_phi=-1.0
	rand_bonus=0.1*np.random.standard_normal((N,1))
	da=np.array([1,-1]*(N/2)).reshape((N,1))+rand_bonus
	for i in range(1,N):
		da[i]=true_phi*da[i-1]+rand_bonus[i]
	ar1.regress(da)
	ar1.check()
	print ar1.params
	print ar1.residual_variance
	# print ar1.residuals
	print ar1.residual_std_dev
	print correlation(ar1.residuals[:-1],ar1.residuals[1:])
	print auto_correlations(ar1.residuals,10)
	print 2/sqrt(N)
	auto_correlation_plot(ar1.residuals,100,"AR(1) residuals")
	# plt.plot( auto_correlations(ar1.residuals,10))
	plt.show()

if __name__ == '__main__':
	main()


