import numpy as np
# P=np.array([[-1,0.2,0.25,0.05],[1,-1,-1,0]]) # P2
# P=np.array([[-1,1.5,-0.6],[1, -0.5,0.0]]) # P3.2 i
P=np.array([[-1,2.0,-0.6,0.2],[1,-0.6,-0.5,0.0]]) # P3.2 iii
P1=np.concatenate([np.zeros((2,1)),P],axis=1)
P0=np.concatenate([P,np.zeros((2,1))],axis=1)

res=np.array(P)
for i in range (1,6):
	P1=np.concatenate([np.zeros((2,i)),P],axis=1)
	P0=np.concatenate([res,np.zeros((2,1))],axis=1)
	res=P0+P1*P0[0,i]

for i in range(0,res.shape[1]):
	print "%0.8f X_{t-%d}"%(res[0,i],i)
for i in range(0,res.shape[1]):
	print " %0.8f a_{t-%d}"%(res[1,i],i)
for i in range(1,min(res.shape[1],6)):
	print "G_%d =& %0.8f \\\\\\nonumber" % (i,res[1,i])