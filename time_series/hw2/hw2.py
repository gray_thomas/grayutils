import scipy.io 
import matplotlib.pyplot as plt 
import time_series as ts
mat = scipy.io.loadmat('hw2p.mat')
X=mat['X'].T[:,0]
print X
ar1=ts.AutoRegressive(1)
ar1.regress(X)
ar1.check(extra_degrees=0)
ts.auto_correlation_plot(X,20,"Raw data P1: Autocorrelation",
	"../../../tex/homework-TimeSeries/hw2/raw_acorr.pdf")
ts.auto_correlation_plot(ar1.residuals,20,
	"P1 AR(1) residuals: Autocorrelation",
	"../../../tex/homework-TimeSeries/hw2/AR1_acorr.pdf")
fig=plt.figure()
print ar1.params
print ar1.c2std
print "phi 1 in range [%.4f, %.4f] with 95 percent probability"%(
	ar1.params[0,0]-ar1.c2std[0],ar1.params[0,0]+ar1.c2std[0])
plt.plot(ar1.predictable_measurements,'.-')
plt.plot(ar1.predictions,'.-')
fig.savefig(
	"../../../tex/homework-TimeSeries/hw2/p1_comp.pdf")
plt.show()