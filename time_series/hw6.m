a=[1, 0.087, 0.140, 0.892]
q=roots(a)
phase(q)
abs(q)
exp(phase(q(2))*1i)
1.1155 / pi/2*12
2*pi*2/12
%%
A0=38.9974
g=1.7992
tau=35.3912
phi1=0.7887
rss1=1.6639
params=4
N=100
variance=rss1/(N-4)

% yt= A0+g*(1-exp(-t/tau))+Xt
% Xt=Xtm*phi1+at
step2varA=(1+phi1^2)*variance

%%
rss2=1.5774
v2=1.5774/(N-7)
phi1=1.6873
theta1=0.9788
step2varB=(1+(phi1-theta1)^2)*v2

step2varB/step2varA
%%
testStatistic=(rss1-rss2)/3  / (v2)
finv(0.80,3,100-7)
finv(0.95,3,100-7)
finv(0.99,3,100-7)

%%
roots([1,-1.6873,0.7499])
roots([1,-0.9788])
poly([0.85,0.85])
(-0.7225 + 0.7499)/0.14 *2

%%
t=1:100
w=0.7
y=10*exp(-0.05*t) .* (0.5 * sin(w*t)+sqrt(1-0.25)*cos(w*t))
q=plot(y)
q.LineWidth=3
plot(10*exp(-0.05*t),'k:','LineWidth',2)
plot(-10*exp(-0.05*t),'k:','LineWidth',2)
hold on
10*sqrt(0.75)

