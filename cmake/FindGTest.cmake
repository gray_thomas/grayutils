# - Try to find gtest lib

#  GTEST_FOUND - system has gtest lib with correct version
#  GTEST_INCLUDE_DIR - the gtest include directory
#  GTEST_SRC_DIR - the gtest source directory, used to compile gtest in each project which uses it.

# # Check for the GrayUtils library home environment variable
if(DEFINED ENV{GRAY_UTILS_HOME})
  message("gray utils home is defined $ENV{GRAY_UTILS_HOME}")
  set(CMAKE_MODULE_PATH "$ENV{GRAY_UTILS_HOME}/cmake" ${CMAKE_MODULE_PATH})
  set(GRAY_UTILS_CMAKE_FOLDER "$ENV{GRAY_UTILS_HOME}/cmake")
else()
  message(FATAL_ERROR "gray utils home doesn't exist. Please specify its location.")
endif()

#   set(gtest_FIND_VERSION "${gtest_FIND_VERSION_MAJOR}.${gtest_FIND_VERSION_MINOR}.${gtest_FIND_VERSION_PATCH}")
# endif(NOT gtest_FIND_VERSION)
set(GTEST_FIND_VERSION "1.7.0") #for now


if (GTEST_INCLUDE_DIR && GTEST_SRC_DIR)
  set(GTEST_FOUND ${GTEST_VERSION_OK})
else
  find_path(GTEST_SRC_DIR NAMES gtest-death-test.cc
      PATHS
      ${GRAY_UTILS_ROOT}/..
      PATH_SUFFIXES src
    )
  find_path(GTEST_INCLUDE_DIR NAMES gtest-death-test.h
      PATHS
      ${GRAY_UTILS_ROOT}/..
      PATH_SUFFIXES include
    )

  if(GTEST_INCLUDE_DIR)

  endif(GTEST_INCLUDE_DIR)

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(gtest DEFAULT_MSG EIGEN3_INCLUDE_DIR EIGEN3_VERSION_OK)

  mark_as_advanced(GTEST_INCLUDE_DIR)
  mark_as_advanced(GTEST_SRC_DIR)
endif