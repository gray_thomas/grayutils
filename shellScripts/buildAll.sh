#!/bin/sh
cd $GRAY_UTILS_HOME
# if [ ! -d $GRAY_UTILS_HOME/UTAThreads/build]; then
# 	mkdir $GRAY_UTILS_HOME/UTAThreads/build
# fi 
# cd UTAThreads/build
# cmake ../cmake
# sudo make install


instLib ()
{
	red='\e[0;31m'
	purple='\e[0;35m' 
	lightPurple= '\e[1;35m'
	echo -e "${purple}==Installing Library: $1, option is $2 ${NC}"
	NC='\e[0m' # No Color
	if [ "$2" = "cyg" ]; then
		echo -e "${purple}$0 $1 $2 $3 clearly Cygwin version!${NC}"
		if [ "$3" = "clean" ]; then
			echo -e "${purple}you said clean${NC}"
			rm -r "$GRAY_UTILS_HOME/$1/build"
		fi
		if [ ! -d "$GRAY_UTILS_HOME/$1/build" ]; then
			mkdir "$GRAY_UTILS_HOME/$1/build"
		fi 
		cd $GRAY_UTILS_HOME/$1/build
		cmake ../cmake
		if [ "$3" = "eclipse" ]; then
			echo -e "${purple}you said eclipse${NC}"
			if [ ! -d "$GRAY_UTILS_HOME/$1/ide" ]; then
				mkdir "$GRAY_UTILS_HOME/$1/ide"
			fi 
			cd $GRAY_UTILS_HOME/$1/ide
			cmake -G"Eclipse CDT4 - Unix Makefiles" ../cmake
			cd $GRAY_UTILS_HOME/$1/build
		fi
		if [ "$3" = "eclipse-clean" ]; then
			echo -e "${purple}you said eclipse-clean${NC}"
			if [ -d "$GRAY_UTILS_HOME/$1/ide" ]; then
				rm -r "$GRAY_UTILS_HOME/$1/ide"
				mkdir "$GRAY_UTILS_HOME/$1/ide"
			else
				mkdir "$GRAY_UTILS_HOME/$1/ide"
			fi 
			cd $GRAY_UTILS_HOME/$1/ide
			cmake -G"Eclipse CDT4 - Unix Makefiles" -D_ECLIPSE_VERSION=4.3 ../cmake
			cd $GRAY_UTILS_HOME/$1/build
		fi
				if [ "$3" = "eclipse-fancy" ]; then
			echo -e "${purple}you said eclipse-fancy${NC}"
			if [ -d "$GRAY_UTILS_HOME/$1/ide" ]; then
				rm -r "$GRAY_UTILS_HOME/$1/ide"
				mkdir "$GRAY_UTILS_HOME/$1/ide"
			else
				mkdir "$GRAY_UTILS_HOME/$1/ide"
			fi 
			cd $GRAY_UTILS_HOME/$1/ide
			cmake -G"Eclipse CDT4 - Unix Makefiles" -D_ECLIPSE_VERSION=4.3 ../cmake
			echo -e "${purple}fixing generated links with sed regex.${NC}"
			sed -i 's_<location>/home_<location>C:/cygwin64/home_g' .project && echo -e "${purple}links changed.${NC}"
			sed -i 's_<pathentry kind="src" path="\[Source directory\]"/>_<pathentry kind="src" path="[Source directory]"/>\n<pathentry kind="src" path="[Targets]"/>_' .cproject && echo -e "${purple}Targets made into src directory.${NC}"
			sed -i 's_/usr/bin/cc_cc_' .cproject && echo -e "${purple}CC reference changed to be on the path.${NC}"
			sed -i 's_-E -P -v -dD _-E -P -v -dD -std=gnu++11 _' .cproject && echo -e "${purple}CC discoverty changed to use gnu++11 .${NC}"
			echo -e "${purple}importing into eclipse.${NC}"
			eclipseraw -nosplash\
			 -data C:/cygwin64/home/gray/wk/ \
			 -application org.eclipse.cdt.managedbuilder.core.headlessbuild \
			 -import file:///C:/cygwin64`pwd` && echo -e "${purple}imported.${NC}"
			cd $GRAY_UTILS_HOME/$1/build
		fi
		echo -e "${purple}No sense using sudo in cygwin!${NC}"
		make install
		cd $GRAY_UTILS_HOME
	else
		echo "Not Cygwin! $0 $1 $2 cats"
		if [ "$2" = "clean" ]; then
			echo "you said clean"
			sudo rm -r "$GRAY_UTILS_HOME/$1/build"
		fi
		if [ ! -d "$GRAY_UTILS_HOME/$1/build" ]; then
			mkdir "$GRAY_UTILS_HOME/$1/build"
		fi 
		cd $GRAY_UTILS_HOME/$1/build
		cmake ../cmake
		if [ "$2" = "eclipse" ]; then
			echo "you said eclipse"
			if [ ! -d "$GRAY_UTILS_HOME/$1/ide" ]; then
				mkdir "$GRAY_UTILS_HOME/$1/ide"
			fi 
			cd $GRAY_UTILS_HOME/$1/ide
			cmake -G"Eclipse CDT4 - Unix Makefiles" ../cmake
			cd $GRAY_UTILS_HOME/$1/build
		fi
		if [ "$2" = "eclipse-clean" ]; then
			echo "you said eclipse-clean"
			if [ -d "$GRAY_UTILS_HOME/$1/ide" ]; then
				sudo rm -r "$GRAY_UTILS_HOME/$1/ide"
				mkdir "$GRAY_UTILS_HOME/$1/ide"
			else
				mkdir "$GRAY_UTILS_HOME/$1/ide"
			fi 
			cd $GRAY_UTILS_HOME/$1/ide
			cmake -G"Eclipse CDT4 - Unix Makefiles" ../cmake
			cd $GRAY_UTILS_HOME/$1/build
		fi
		if [ "$2" = "cxx-clean" ]; then
			echo "you said cxx-clean"
			if [ -d "$GRAY_UTILS_HOME/$1/build" ]; then
				sudo rm -r "$GRAY_UTILS_HOME/$1/build"
				mkdir "$GRAY_UTILS_HOME/$1/build"
			else
				mkdir "$GRAY_UTILS_HOME/$1/build"
			fi 
			cd $GRAY_UTILS_HOME/$1/build
			echo -e "${purple} You said use CXX: $3 and RPATH: $4 ${NC}"
			CXX="$3" CMAKE_INSTALL_RPATH="$4" cmake ../cmake -DCMAKE_INSTALL_RPATH="$4"
			cd $GRAY_UTILS_HOME/$1/build
		fi
		sudo make install
		cd $GRAY_UTILS_HOME
	fi
}
instLib ThirdPartyLibs/gtest-1.7.0 $1 $2 $3 $4
instLib GUThreads $1 $2 $3 $4
instLib GUTiming $1 $2 $3 $4
instLib VectorGeometry $1 $2 $3 $4
instLib RigidBodyDynamics $1 $2 $3 $4
instLib LinearControls $1 $2 $3 $4
instLib PhaseSpaceUDP/receive $1 $2 $3 $4
instLib JoystickDriver/send $1 $2 $3 $4
instLib JoystickDriver/receive $1 $2 $3 $4
instLib AttitudeEstimator $1 $2 $3 $4

cd $GRAY_UTILS_HOME
ls
