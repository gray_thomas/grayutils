#include "AggressiveTimingDaemon.hpp"
#include "Daemon.hpp"
namespace gu{
namespace timing{


void AggressiveTimingDaemon::loop()
{
	if (std::chrono::high_resolution_clock::now() > goalPoint)
	{
		agressivelyTimedLoop();
		goalPoint += roundedPeriod;
	}
}
signed int AggressiveTimingDaemon::getMicroError()
{
	auto error = std::chrono::high_resolution_clock::now() - goalPoint;
	return std::chrono::duration_cast<std::chrono::duration<signed int, std::micro>>(error).count();
}
AggressiveTimingDaemon::AggressiveTimingDaemon(double periodSeconds)
{
	roundedPeriod = std::chrono::milliseconds((int) (periodSeconds * 1000));
	goalPoint = std::chrono::high_resolution_clock::now();
}
}
}
