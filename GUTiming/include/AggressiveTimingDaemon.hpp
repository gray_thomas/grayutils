#ifndef AGGRESSIVE_TIMING_DAEMON__HPP
#define AGGRESSIVE_TIMING_DAEMON__HPP
#include <chrono>
#include <Daemon.hpp>
namespace gu
{
namespace timing
{

class AggressiveTimingDaemon: public gu::threads::Daemon
{
	std::chrono::milliseconds roundedPeriod;
	std::chrono::time_point<std::chrono::high_resolution_clock> goalPoint;
protected:
	virtual void agressivelyTimedLoop()=0;
	void loop();
	signed int getMicroError();
public:
	AggressiveTimingDaemon(double periodSeconds);
	virtual ~AggressiveTimingDaemon()
	{
	}
	;
};
}
}
#endif
