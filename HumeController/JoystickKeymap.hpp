/*
 * JoystickKeymap.hpp
 *
 *    Created on: Apr 23, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#ifndef JOYSTICKKEYMAP_HPP_
#define JOYSTICKKEYMAP_HPP_

#define T0L 0
#define T0LN "[JR]"
#define T1L 1
#define T1LN "[JP]"
#define T0M 2
#define T0MN "[TL]"
#define UNUSEDJOYSTICKYAW 3
#define UNUSEDJOYSTICKYAWN "[JY]"
#define T1M 4
#define T1MN "[TR]"
#define ENABLE 0
#define ENABLEN "[Trig]"
#define LEG_SWITCH 1
#define LEG_SWITCHN "[2]"
#define START_T0 2
#define START_T0N "[3]"
#define REVERT_T0 3
#define REVERT_T0N "[4]"
#define SAVE_T0 4
#define SAVE_T0N "[5]"
#define DUMP_INITIALIZERS 5
#define DUMP_INITIALIZERSN "[6]"
#define UB7 6
#define NB7 "[7]"
#define ATTACH_T0 7
#define ATTACH_T0N "[8]"
#define UB9 8
#define NB9 "[9]"
#define INVERT_T0 9
#define INVERT_T0N "[10]"
#define UB11 10
#define NB11 "[11]"
#define SCROLL_UP 11
#define SCROLL_UPN "[+]"
#define SCROLL_DOWN 12
#define SCROLL_DOWNN "[-]"
#define UNUSED_POWER 13
#define UNUSED_POWERN "[@]"
#define DECREASE_JOINT 14
#define DECREASE_JOINTN "[<]"
#define INCREASE_JOINT 16
#define INCREASE_JOINTN "[>]"
#define HIGHER_COMPONENT 15
#define HIGHER_COMPONENTN "[^]"
#define LOWER_COMPONENT 17
#define LOWER_COMPONENTN "[v]"



#endif /* JOYSTICKKEYMAP_HPP_ */
