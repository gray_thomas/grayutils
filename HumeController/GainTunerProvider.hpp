/*
 * GainTunerProvider.hpp
 *
 *    Created on: Apr 23, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#ifndef GAINTUNERPROVIDER_HPP_
#define GAINTUNERPROVIDER_HPP_

#include "TuneableDouble.hpp"

namespace uta
{
namespace hume
{
namespace ui
{
class GainTunerProvider
{
public:
	virtual ~GainTunerProvider()
	{
	}
	virtual void attachGainTuner(int tunerNumber, TuneableDouble* tuneable)=0;
	virtual bool getClick(int i)=0;
	virtual bool getRelease(int i)=0;
	virtual bool getButton(int i)=0;
	virtual double getAxis(int axis)=0;
};
}
}
}

#endif /* GAINTUNERPROVIDER_HPP_ */
