/*
 * HumeLogger.cpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "HumeLogger.hpp"
#include "HumePlottingFunctions.hpp"
namespace uta
{
namespace hume
{
void HumeLogger::doLogging(M3HumeEmbeddedStatus* statuses[2][3], message& message, HumeIMU& inertialSensor)
{
	if (isSetup)
	{
		ostream& outputStream = useCout ? cout : outputFileStream;
		bool parity[2][3];
		unsigned char tics[2][3][2];
		for (int i = 0; i < 2; i++)
			for (int j = 0; j < 3; j++)
			{
				tics[i][j][0] = statuses[i][j]->reports[0].controlTicIndex;
				tics[i][j][1] = statuses[i][j]->reports[1].controlTicIndex;

				if (tics[i][j][0] == 99 && tics[i][j][1] == 0)
					parity[i][j] = 0;
				else if (tics[i][j][1] == 99 && tics[i][j][0] == 0)
					parity[i][j] = 1;
				else if (tics[i][j][1] > tics[i][j][0])
					parity[i][j] = 0;
				else
					parity[i][j] = 1;
			}
		for (int fastTic = 0; fastTic < 2; fastTic++)
		{
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < 3; j++)
				{
					outputStream << statuses[i][j]->reports[parity[i][j] ? (1 + fastTic) % 2 : fastTic];
				}
			outputStream << message;
			outputStream << inertialSensor;
			outputStream << " " << count << "\n";
			outputStream.flush();
		}
		count++;
	}
	// each line = 1 inferred 500us tick: [30 for DSPs] [29 for phaseSpace] [18 for IMU] [1 for Plotter] \n
	// report0[0][0]-report0[1][2], message, inertialSensor, control count \n
	// report= [tic, current, motor, joint, spring] //5*6 = 30 numbers

	// message = messageIndex, point0-point6 // 1+4*7 = 29 numbers
	// point = [valid x y z]
	// inertialSensor = orientation[0][0]-[2][2], accelerometer[0-2], angularVelocity [0-2], magnetometer[0-2] // 9 + 9= 18 numbers
	// total of 70 numbers, 140 number per ms, averaging 8 chars that makes 800+360=1160 bytes per ms, or 1,160,000 bytes per second
	// Meka computer has a a disk write bandwidth of 1.0 GB/s, so this 1.16MB/s burden should be fine. I hope.
}
void HumeLogger::setupLogging(string filename)
{
	isSetup = true;
	useCout = false;
	outputFileStream.open(filename.c_str());
}
void HumeLogger::setupConsoleLogging()
{
	isSetup = true;
	useCout = true;
}
}
}

