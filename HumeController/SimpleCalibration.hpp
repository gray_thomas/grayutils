/*
 * SimpleCalibration.hpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SIMPLECALIBRATION_HPP_
#define SIMPLECALIBRATION_HPP_
#include <Eigen/Dense>
using namespace Eigen;
namespace uta
{
namespace hume
{
short sh(double value);
struct MekaParams
{
	double spring_const;
	double zero_joint_theta;
	double zero_spring_theta;
	double hub_r;
	double scale;
	double dac_max;
	double joint_theta_max;
	double joint_theta_min;
	MekaParams(){}
	MekaParams(double spring_const, double zero_joint_theta, double zero_spring_theta, double hub_r, double scale,
				double dac_max, double joint_theta_max, double joint_theta_min) :
				spring_const(spring_const),
				zero_joint_theta(zero_joint_theta),
				zero_spring_theta(zero_spring_theta),
				hub_r(hub_r),
				scale(scale),
				dac_max(dac_max),
				joint_theta_max(joint_theta_max),
				joint_theta_min(joint_theta_min)
	{
	}
};
struct JointParams
{
	double radAtEncoderMin;
	double radAtEncoderMax;
	double encoderMin;
	double encoderMax;
	double springZero;
	double torquePerSpringEncoder;
	JointParams(){}
	JointParams(double radAtEncoderMin, double radAtEncoderMax, double encoderMin, double encoderMax,
				double springZero, double torquePerSpringEncoder) :
				radAtEncoderMin(radAtEncoderMin),
				radAtEncoderMax(radAtEncoderMax),
				encoderMin(encoderMin),
				encoderMax(encoderMax),
				springZero(springZero),
				torquePerSpringEncoder(torquePerSpringEncoder)
	{
	}
	JointParams(MekaParams& meka);
};
class SimpleCalibration
{
	JointParams joints[2][3];
	MekaParams meka[2][3];
	void setupMekaParams();
public:
	SimpleCalibration();
	void setJointEncoderParameters(double radiansAtEncoderMin[2][3], double radiansAtEncoderMax[2][3],
									short encoderMin[2][3], short encoderMax[2][3]);
	void setDefaultParameters();
	void getEncodersFromJointAngles(const double jointAngles[2][3], short jointEncoders[2][3]);
	Matrix<double, 2,3> getSpringEncodersFromTorques(Matrix<double, 2,3>& torques);
	void getFractionsFromJointAngles(const double jointAngles[2][3], double jointFraction[2][3]);
};
}
}

#endif /* SIMPLECALIBRATION_HPP_ */
