/*
 * HumeJointTask.cpp
 *
 *    Created on: Apr 23, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "HumeJointTask.hpp"
namespace uta
{
namespace hume
{
namespace ui
{
HumeJointTask::HumeJointTask() :
			TuneableModule(3),
			osc(0.001),
			rawFeedForward(0),
			oscFreq(0),
			oscMagnitude(0)
{
	tuneables[0].name = "feedForward";
	rawFeedForward = &(tuneables[0].value);
	tuneables[1].name = "oscFreq";
	oscFreq = &(tuneables[1].value);
	tuneables[2].name = "oscMagnitude";
	oscMagnitude = &(tuneables[2].value);
}
double HumeJointTask::getPositionDesired()
{
	double rawFeedForward = *(this->rawFeedForward);
	double oscFreq = *(this->oscFreq);
	double oscMagnitude = *(this->oscMagnitude);
	osc.frequency=oscFreq;
	double sineWave = uta::hume::oscMappedSine(osc.getNext(),-oscMagnitude,oscMagnitude);
	return rawFeedForward+sineWave;
}
void HumeJointTask::printState(bool isSelected)
{
	cout << (isSelected ? "=TaskLevel=" : " TaskLevel ");
	TuneableModule::printState(isSelected);
	cout << "\n";
}
}
}
}
