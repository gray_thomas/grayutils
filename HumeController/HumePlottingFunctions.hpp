/*
 * HumePlottingFunctions.hpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef HUMEPLOTTINGFUNCTIONS_HPP_
#define HUMEPLOTTINGFUNCTIONS_HPP_

#include <iostream>
#include "m3hume_pdo_v0_def.h"
#include "HumeIMU.hpp"
#include "comm_udp.h"

using namespace std;
namespace uta
{
namespace hume
{
ostream & operator<<(ostream& out, M3HumeEmbeddedStatus& status);
ostream & operator<<(ostream& out, M3HumeEmbeddedCommand& command);
ostream & operator<<(ostream& out, HumeIMU& inertialSensor);
ostream & operator<<(ostream& out, message& message);
ostream & operator<<(ostream& out, NewM3HumeEmbeddedControlReport& report);
}
}

#endif /* HUMEPLOTTINGFUNCTIONS_HPP_ */
