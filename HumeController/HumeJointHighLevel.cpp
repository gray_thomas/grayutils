/*
 * HumeJointHighLevel.cpp
 *
 *    Created on: Apr 23, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "HumeJointHighLevel.hpp"
namespace uta
{
namespace hume
{
namespace ui
{
HumeJointHighLevel::HumeJointHighLevel() :
			TuneableModule(2),
			proportionalGain(0),
			derivativeGain(0)
{
	tuneables[0].name = "propGain";
	proportionalGain = &(tuneables[0].value);
	tuneables[1].name = "derivGain";
	derivativeGain = &(tuneables[1].value);
}
double HumeJointHighLevel::doControl(double desiredPositionRad, double positionRad, double velocityRadPerSec)
{
	return 0;
}
void HumeJointHighLevel::printState(bool isSelected)
{
	cout << (isSelected ? "=HighLevel=" : " HighLevel ");
	TuneableModule::printState(isSelected);
	cout << "\n";
}
}
}
}
