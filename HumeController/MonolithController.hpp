/*
 * MonolithController.hpp
 *
 *    Created on: Apr 16, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef MONOLITHCONTROLLER_HPP_
#define MONOLITHCONTROLLER_HPP_
#include "m3hume_pdo_v0_def.h"
#include "comm_udp.h"
#include "HumeIMU.hpp"
#include <iostream>
#include <string>
using namespace std;
namespace uta
{
namespace hume
{
namespace monolithInterface
{
class MonolithController
{
public:
	virtual ~MonolithController(){}
	virtual void setupLogging(string filename)=0;
	virtual void doControl(M3HumeEmbeddedStatus* statuses[2][3], message& message, HumeIMU& inertialSensor,
							M3HumeEmbeddedCommand* commandOutputs[2][3])=0;
};
extern MonolithController* instantiateDefaultController(void);
}
}
}

#endif /* MONOLITHCONTROLLER_HPP_ */
