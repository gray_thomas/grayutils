/*
 * TuneableModule.hpp
 *
 *    Created on: Apr 23, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#ifndef TUNEABLEMODULE_HPP_
#define TUNEABLEMODULE_HPP_
#include "TuneableDouble.hpp"
#include <iomanip>
#include <iostream>
#include "DoubleTuner.hpp"
#include "GainTunerProvider.hpp"
#include "vector"

using namespace std;

namespace uta
{
namespace hume
{
namespace ui
{
class TuneableModule
{
protected:
	std::vector<TuneableDouble> tuneables;
	int sel;
public:
	TuneableModule(int num);
	virtual ~TuneableModule()
	{
	}
	void tune(GainTunerProvider* tunerProvider);
	void select(GainTunerProvider* tunerProvider);
	void dumpInitializer();
	virtual void printState(bool isSelected);
	void initialize(double);
	void initialize(double, double);
	void initialize(double, double, double);
	void initialize(double, double, double, double);
	void initialize(double, double, double, double, double);
	void initialize(double, double, double, double, double, double);
	void initialize(double, double, double, double, double, double, double);
	void initialize(double, double, double, double, double, double, double, double);
	void initialize(double, double, double, double, double, double, double, double, double);
	void initialize(double, double, double, double, double, double, double, double, double, double);
	void initialize(double, double, double, double, double, double, double, double, double, double, double);
};
}
}
}

#endif /* TUNEABLEMODULE_HPP_ */
