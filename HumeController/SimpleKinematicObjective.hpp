/*
 * SimpleKinematicObjective.hpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SIMPLEKINEMATICOBJECTIVE_HPP_
#define SIMPLEKINEMATICOBJECTIVE_HPP_

namespace uta
{
namespace hume
{
struct SimpleKinematicObjective
{
	double leftFootDistanceAheadOfBody;
	double rightFootDistanceAheadOfBody;
	double leftFootDistanceLeftOfBody;
	double rightFootDistanceLeftOfBody;
	double hipHeight;
	double forwardLeanAngleRadians;
	double bodyRollAboutTheForwardDirection;
};
}
}

#endif /* SIMPLEKINEMATICOBJECTIVE_HPP_ */
