/*
 * TuneableModule.cpp
 *
 *    Created on: Apr 23, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "HumeJoystickController.hpp"
namespace uta
{
namespace hume
{
namespace ui
{
TuneableDouble getDefaultDouble()
{
	TuneableDouble defaultDouble;
	defaultDouble.name = "UnnamedTuneable";
	defaultDouble.value = 1.0;
	defaultDouble.saved = 1.0;
	return defaultDouble;
}
TuneableModule::TuneableModule(int i) :
			tuneables(i, getDefaultDouble()),
			sel(0)
{

}
void TuneableModule::tune(GainTunerProvider* tunerProvider)
{
	if (tunerProvider->getClick(SCROLL_UP))
		sel = (sel + 1) % tuneables.size();
	if (tunerProvider->getClick(SCROLL_DOWN))
		sel = (sel - 1 + tuneables.size()) % tuneables.size();
	if (tunerProvider->getClick(SCROLL_UP) || tunerProvider->getClick(SCROLL_DOWN))
		tunerProvider->attachGainTuner(0, &(tuneables[sel]));
}
void TuneableModule::select(GainTunerProvider* tunerProvider)
{
	tunerProvider->attachGainTuner(0, &(tuneables[sel]));
}
void TuneableModule::dumpInitializer()
{
	for (unsigned i = 0; i < tuneables.size(); i++)
		cout << tuneables[i].value << ((((i + 1) % tuneables.size()) == 0) ? "" : ", ");
}

void TuneableModule::printState(bool isSelected)
{
	cout << " ";
	for (unsigned i = 0; i < tuneables.size(); i++)
	{
		bool decorate = (i == (unsigned) sel) && isSelected;
		cout << (decorate ? "=" : " ") << setw(12) << tuneables[i].name << (decorate ? "=" : " ") << " = ";
		cout << setw(12) << tuneables[i].value;
		bool isLast = ((i + 1) % tuneables.size()) == 0;
		bool isThird = ((i + 1) % 3) == 0;
		cout << (isLast ? "" : (isThird ? "\n            " : " | "));
	}
}

void TuneableModule::initialize(double d1)
{
	tuneables[0].value = d1;
	tuneables[0].saved = d1;
}
void TuneableModule::initialize(double d1, double d2)
{
	initialize(d1);
	tuneables[1].value = d2;
	tuneables[1].saved = d2;
}
void TuneableModule::initialize(double d1, double d2, double d3)
{
	initialize(d1, d2);
	tuneables[2].value = d3;
	tuneables[2].saved = d3;
}
void TuneableModule::initialize(double d1, double d2, double d3, double d4)
{
	initialize(d1, d2, d3);
	tuneables[3].value = d4;
	tuneables[3].saved = d4;
}
void TuneableModule::initialize(double d1, double d2, double d3, double d4, double d5)
{
	initialize(d1, d2, d3, d4);
	tuneables[4].value = d5;
	tuneables[4].saved = d5;
}
void TuneableModule::initialize(double d1, double d2, double d3, double d4, double d5, double d6)
{
	initialize(d1, d2, d3, d4, d5);
	tuneables[5].value = d6;
	tuneables[5].saved = d6;
}
void TuneableModule::initialize(double d1, double d2, double d3, double d4, double d5, double d6, double d7)
{
	initialize(d1, d2, d3, d4, d5, d6);
	tuneables[6].value = d7;
	tuneables[6].saved = d7;
}
void TuneableModule::initialize(double d1, double d2, double d3, double d4, double d5, double d6, double d7,
								double d8)
{
	initialize(d1, d2, d3, d4, d5, d6, d7);
	tuneables[7].value = d8;
	tuneables[7].saved = d8;
}
void TuneableModule::initialize(double d1, double d2, double d3, double d4, double d5, double d6, double d7,
								double d8, double d9)
{
	initialize(d1, d2, d3, d4, d5, d6, d7, d8);
	tuneables[8].value = d9;
	tuneables[8].saved = d9;
}
void TuneableModule::initialize(double d1, double d2, double d3, double d4, double d5, double d6, double d7,
								double d8, double d9, double d10)
{
	initialize(d1, d2, d3, d4, d5, d6, d7, d8, d9);
	tuneables[9].value = d10;
	tuneables[9].saved = d10;
}
void TuneableModule::initialize(double d1, double d2, double d3, double d4, double d5, double d6, double d7,
								double d8, double d9, double d10, double d11)
{
	initialize(d1, d2, d3, d4, d5, d6, d7, d8, d9, d10);
	tuneables[10].value = d11;
	tuneables[10].saved = d11;
}


}
}
}
