/*
 * FrictionBiasGainEstimator.cpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "FrictionBiasGainEstimator.hpp"

using namespace Eigen;

namespace uta
{
namespace hume
{
FrictionBiasGainEstimator::FrictionBiasGainEstimator()
{
	e.setZero(3);
	B.setIdentity(3, 3);
	alpha = 1.0;
	epsilon = 1e-6;
}
void FrictionBiasGainEstimator::setup(double alpha, double epsilon)
{
	this->alpha = alpha;
	this->epsilon = epsilon;
	e.setZero();
	B.setIdentity() * 1e12;
}
double signum(double x)
{
	if (x > 0)
		return 1.0;
	if (x < 0)
		return -1.0;
	return 0.0;
}
double robustSignum(double x, double epsion)
{
	if (x > epsion)
		return 1.0;
	if (x < -epsion)
		return -1.0;
	return 0.0;
}
VectorXd FrictionBiasGainEstimator::update(double Xdd, double Xd, double currentDAC)
{
	if (abs(Xd) < epsilon)
		return B * e;
	MatrixXd R;
	R.setZero(1, 3);
	R << currentDAC, signum(Xd), 1.0;
	MatrixXd numerator = B * R.transpose() * R * B;
	MatrixXd oneByOne = R * B * R.transpose();
	double denomenator = oneByOne(0, 0) + alpha;
	B = 1.0 / alpha * (B - numerator / denomenator);
	e = alpha * e + R.transpose() * Xdd;
	return B * e;
}
}
}
