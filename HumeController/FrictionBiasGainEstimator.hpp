/*
 * FrictionBiasGainEstimator.hpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef FRICTIONBIASGAINESTIMATOR_HPP_
#define FRICTIONBIASGAINESTIMATOR_HPP_
#include <Eigen/Dense>
using namespace Eigen;
namespace uta
{
namespace hume
{
class FrictionBiasGainEstimator
{
	MatrixXd B;
	double alpha;
	VectorXd e;
	double epsilon;
public:
	FrictionBiasGainEstimator();
	void setup(double alpha, double epsilon);
	VectorXd update(double Xdd, double Xd, double currentDAC);
	MatrixXd debugB(){return B;}
	VectorXd debuge(){return e;}
};
double signum(double x);
double robustSignum(double x,double epsion);
}
}

#endif /* FRICTIONBIASGAINESTIMATOR_HPP_ */
