/*
 * HumePlottingFunctions.cpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include "HumePlottingFunctions.hpp"
#include <iomanip>
namespace uta
{
namespace hume
{
ostream & operator<<(ostream& out, NewM3HumeEmbeddedControlReport& report)
{

	out << (int) report.controlTicIndex << " ";
	out << report.rawOutCommandedMotorDAC << " ";
	out << report.raw1MotorEncoder << " ";
	out << report.raw2JointEncoder << " ";
	out << report.raw3SpringEncoder << " ";

//	out.flush(); // turns out, this really helps reduce omissions in the output.
	return out;
}

ostream & operator<<(ostream& out, message& message)
{
//	Int32BitField* bits = ((Int32BitField*) message.validBits);
	out << setprecision(8);
	out << message.index << " ";
	unsigned check = 0x80000000;
	bool valids[32];
	for (int i = 0; i < 32; i++)
	{
		valids[i] = (message.validBits & check) > 0;
		check >>= 1;
	}

	for (int i = 0; i < 7; i++)
		out << (int) valids[i] << " " << message.x[i] << " " << message.y[i] << " " << message.z[i] << " ";
	return out;
}

ostream & operator<<(ostream& out, HumeIMU& inertialSensor)
{
	out << setprecision(8);
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			out << inertialSensor.orientation[i][j] << " ";
	for (int i = 0; i < 3; i++)
		out << inertialSensor.accelerometer[i] << " ";
	for (int i = 0; i < 3; i++)
		out << inertialSensor.angularVelocity[i] << " ";
	for (int i = 0; i < 3; i++)
		out << inertialSensor.magnetometer[i] << " ";

	return out;
}

ostream& tab(ostream& out, int num)
{
	for (int i = 0; i < num; i++)
		out << "\t";
	return out;
}
ostream& printReport(ostream& out, NewM3HumeEmbeddedControlReport& report, int tablevel)
{
	out << "M3HumeEmbeddedStatus:\n";
	tab(out, tablevel) << "controlStates: [";
	for (int i = 0; i < MAX_CONTROL_STATES; i++)
		out << report.controlStates[i] << ((i + 1 < MAX_CONTROL_STATES) ? ", " : "]\n");
	tab(out, tablevel) << "raw1MotorEncoder: " << report.raw1MotorEncoder << "\n";
	tab(out, tablevel) << "raw2JointEncoder: " << report.raw2JointEncoder << "\n";
	tab(out, tablevel) << "raw3SpringEncoder: " << report.raw3SpringEncoder << "\n";
	tab(out, tablevel) << "raw4CurrentADC: " << report.raw4CurrentADC << "\n";

	tab(out, tablevel) << "rawOutCommandedMotorDAC: " << report.rawOutCommandedMotorDAC << "\n";
	tab(out, tablevel) << "debugSaturatedFeedbackOutput: " << report.debugSaturatedFeedbackOutput << "\n";
	tab(out, tablevel) << "debuginput2: " << report.debuginput2 << "\n";
	tab(out, tablevel) << "debugUnsaturatedMotorDAC: " << report.debugUnsaturatedMotorDAC << "\n";
	tab(out, tablevel) << "debugFeedbackOutput: " << report.debugFeedbackOutput << "\n";
	tab(out, tablevel) << "ctrl_state: StatusFlags:[ commsTimeout: " << report.ctrl_state.commsTimeout << ", enable:"
			<< report.ctrl_state.enable << ", foot:" << report.ctrl_state.foot << ", latency:"
			<< report.ctrl_state.latency << ", other:" << report.ctrl_state.other << "]\n";
	tab(out, tablevel) << "controlTicIndex: " << report.controlTicIndex << "\n";

	return out;
}
ostream& operator<<(ostream& out, M3HumeEmbeddedStatus& status)
{
	out << "M3HumeEmbeddedStatus:\n";
	printReport(tab(out, 1) << "reports[0]:", status.reports[0], 2);
	printReport(tab(out, 1) << "reports[1]:", status.reports[1], 2);
	tab(out, 1) << "padding:[" << hex << status.padding[0] << ", " << hex << status.padding[1] << hex
			<< status.padding[2] << "]\n";
	tab(out, 1) << "secondToLastChar:[" << hex << status.secondToLastChar << "]\n";
	tab(out, 1) << "lastChar:[" << hex << status.lastChar << "]\n";
	return out;
}
ostream& operator<<(ostream& out, M3HumeEmbeddedCommand& command)
{
	out << "M3HumeEmbeddedCommand\n";
	tab(out, 1) << "feedForward:[" << dec << command.feedForward << "]\n";
	tab(out, 1) << "watchdog:[" << command.watchdog << "]\n";
	tab(out, 1) << "dacLimit:[" << command.dacLimit << "]\n";
	tab(out, 1) << "feedbackLimit:[" << command.feedbackLimit << "]\n";
	tab(out, 1) << "expected1MotorEncoder:[" << command.expected1MotorEncoder << "]" << " SignalBoost:["
			<< (int) command.bitShift1MotorEncoderSignalBoost << "]\n";
	tab(out, 1) << "expected2JointEncoder:[" << command.expected2JointEncoder << "]" << " SignalBoost:["
			<< (int) command.bitShift2JointEncoderSignalBoost << "]\n";
	tab(out, 1) << "expected3SpringEncoder:[" << command.expected3SpringEncoder << "]" << " SignalBoost:["
			<< (int) command.bitShift3SpringEncoderSignalBoost << "]\n";
	tab(out, 1) << "expected4CurrentADC:[" << command.expected4CurrentADC << "]" << " SignalBoost:["
			<< (int) command.bitShift4CurrentADCSignalBoost << "]\n";
	tab(out, 1) << "controlMatrix:[\n";
	for (int i = 0; i < CTRL_MATRIX_ROWS; i++)
	{
		tab(out, 2) << "[";
		for (int j = 0; j < CTRL_MATRIX_COLS; j++)
			out << hex << command.controlMatrix[i][j] << ((j + 1 < CTRL_MATRIX_COLS) ? ", " : "]");
		out << dec << ((i + 1 < CTRL_MATRIX_ROWS) ? ",\n" : "]\n");
	}
	tab(out, 1) << "bitShiftDACOutputBoost:[" << (int) command.bitShiftDACOutputBoost << "]\n";
	tab(out, 1) << "control_flags:[ enable:" << (int) command.control_flags.enable << ", extraStates:"
			<< (int) command.control_flags.extraStates << ", latency:" << (int) command.control_flags.latency
			<< ", other:" << (int) command.control_flags.other << "]\n";
	return out;
}
}
}
