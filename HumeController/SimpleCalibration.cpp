/*
 * SimpleCalibration.cpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "SimpleCalibration.hpp"
#include <iostream>
#include <math.h>
#define PI 3.14159265359
#define PI2OVERVERTX (2.0*3.14159265359/16383.0)
using namespace std;

namespace uta
{
namespace hume
{
short sh(double value)
{
	// 0x0000 =  0.000 // 0x4000 = 0.500 // 0xFFFF = -0.00003
	// 0x7FFF =  0.997 // 0x2000 = 0.250 // 0xC000 = -0.50000
	// 0x8000 = -1.000 // 0x1000 = 0.125 // 0xFFDF = -0.001
	if (value > 0.99996948242)
		value = 0.99996948242;
	if (value < -1.0)
		value = -1.0;
	return (short) (value * 32768.0);
}
void SimpleCalibration::setJointEncoderParameters(double radiansAtEncoderMin[2][3], double radiansAtEncoderMax[2][3],
													short encoderMin[2][3], short encoderMax[2][3])
{
	setDefaultParameters();
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 3; j++)
		{
			if (encoderMax[i][j] <= encoderMin[i][j])
			{
				cerr << "encoder max " << encoderMax[i][j] << " is lower than encoder min " << encoderMin[i][j]
						<< "!" << endl;
				this->joints[i][j].radAtEncoderMax = radiansAtEncoderMin[i][j];
				this->joints[i][j].radAtEncoderMin = radiansAtEncoderMax[i][j];
				this->joints[i][j].encoderMax = encoderMin[i][j];
				this->joints[i][j].encoderMin = encoderMax[i][j];
			}
			else
			{
				this->joints[i][j].radAtEncoderMax = radiansAtEncoderMax[i][j];
				this->joints[i][j].radAtEncoderMin = radiansAtEncoderMin[i][j];
				this->joints[i][j].encoderMax = encoderMax[i][j];
				this->joints[i][j].encoderMin = encoderMin[i][j];
			}
		}
}

SimpleCalibration::SimpleCalibration()
{
	setDefaultParameters();
}
JointParams::JointParams(MekaParams& meka)
{
	this->encoderMax = meka.joint_theta_max;
	this->encoderMin = meka.joint_theta_min;
	this->radAtEncoderMax = (meka.joint_theta_max + meka.zero_joint_theta) * meka.scale * PI2OVERVERTX;
	this->radAtEncoderMin = (meka.joint_theta_min + meka.zero_joint_theta) * meka.scale * PI2OVERVERTX;
	this->springZero = meka.zero_spring_theta;
	this->torquePerSpringEncoder = meka.hub_r * meka.spring_const * PI2OVERVERTX;
}
void SimpleCalibration::setupMekaParams()
{
	MekaParams m00(-5160, -9700, -7848, 0.075, -1, 800, 10325, 9300);
	MekaParams m01(5160, -5677, -9145, 0.032, 1, 1000, 8727, 3560);
	MekaParams m02(-5160, -10258, -8814, 0.032, 1, 2040, 10215, 3990);// Hub r is .03 in the yaml, but all the others are 0.032
	MekaParams m10(5160, -4680, -7610, 0.075, -1, 1000, 5250, 4120);
	MekaParams m11(-5160, -8330, -7753, 0.032, -1, 1000, 9650, 4680);
	MekaParams m12(5160, -4789, -7962, 0.032, -1, 2040, 11000, 4800);

	this->meka[0][0] = m00;
	this->meka[0][1] = m01;
	this->meka[0][2] = m02;
	this->meka[1][0] = m10;
	this->meka[1][1] = m11;
	this->meka[1][2] = m12;
}

void SimpleCalibration::setDefaultParameters()
{
	double degToRad = PI / 180.0;
	setupMekaParams();

	JointParams j00(degToRad * -13.0, degToRad * 13.0, 9166, 10426, meka[0][0].zero_spring_theta,
			meka[0][0].hub_r * meka[0][0].spring_const * PI2OVERVERTX);
	JointParams j10(degToRad * -13.0, degToRad * 13.0, 4051, 5359, meka[1][0].zero_spring_theta,
			meka[1][0].hub_r * meka[1][0].spring_const * PI2OVERVERTX);
	JointParams j01(degToRad * -30.0, degToRad * 80.0, 4036, 9165, meka[0][1].zero_spring_theta,
			meka[0][1].hub_r * meka[0][1].spring_const * PI2OVERVERTX);
	JointParams j11(degToRad * 80.0, degToRad * -30.0, 4880, 9936, meka[1][1].zero_spring_theta,
			meka[1][1].hub_r * meka[1][1].spring_const * PI2OVERVERTX);
	JointParams j02(degToRad * -145.0, degToRad * 0.0, 3880, 10270, meka[0][2].zero_spring_theta,
			meka[0][2].hub_r * meka[0][2].spring_const * PI2OVERVERTX);
	JointParams j12(degToRad * 0.0, degToRad * -145.0, 4743, 11140, meka[1][2].zero_spring_theta,
			meka[1][2].hub_r * meka[1][2].spring_const * PI2OVERVERTX);
	this->joints[0][0] = j00;
	this->joints[0][1] = j01;
	this->joints[0][2] = j02;
	this->joints[1][0] = j10;
	this->joints[1][1] = j11;
	this->joints[1][2] = j12;

//	radiansAtEncoderMin[0][2] = degToRad * -145.0;
//	radiansAtEncoderMax[0][2] = degToRad * 0.0;
//	encoderMin[0][2] = 3880;
//	encoderMax[0][2] = 10270;
//	radiansAtEncoderMin[1][2] = degToRad * 0.0;
//	radiansAtEncoderMax[1][2] = degToRad * -145.0;
//	encoderMin[1][2] = 4743;
//	encoderMax[1][2] = 11140;

//	radiansAtEncoderMin[0][1] = degToRad * -30.0;
//	radiansAtEncoderMax[0][1] = degToRad * 80.0;
//	encoderMin[0][1] = 4036;
//	encoderMax[0][1] = 9165;
//	radiansAtEncoderMin[1][1] = degToRad * 80.0;
//	radiansAtEncoderMax[1][1] = degToRad * -30.0;
//	encoderMin[1][1] = 4880;
//	encoderMax[1][1] = 9936;

//	encoderMin[0][0] = 9166;
//	encoderMax[0][0] = 10426;
//	encoderMin[1][0] = 4051;
//	encoderMax[1][0] = 5359;
//	radiansAtEncoderMin[0][0] = degToRad * -13.0;
//	radiansAtEncoderMax[0][0] = degToRad * 13.0;
//	radiansAtEncoderMin[1][0] = degToRad * -13.0;
//	radiansAtEncoderMax[1][0] = degToRad * 13.0;

}
Matrix<double, 2, 3> SimpleCalibration::getSpringEncodersFromTorques(Matrix<double, 2, 3>& torques)
{
	Matrix<double,2, 3> ret;
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 3; j++)
			ret(i, j) = (joints[i][j].springZero + torques(i, j) / joints[i][j].torquePerSpringEncoder);
	return ret;
}

void SimpleCalibration::getFractionsFromJointAngles(const double jointAngles[2][3], double jointFraction[2][3])
{
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 3; j++)
		{
			double radianRange = (joints[i][j].radAtEncoderMax - joints[i][j].radAtEncoderMin);
			jointFraction[i][j] = (jointAngles[i][j] - joints[i][j].radAtEncoderMin) / radianRange;
		}
}
void SimpleCalibration::getEncodersFromJointAngles(const double jointAngles[2][3], short jointEncoders[2][3])
{
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 3; j++)
		{
			if (std::isnan(jointAngles[i][j]) || std::isinf(jointAngles[i][j]) != 0)
			{
				jointEncoders[i][j] = (joints[i][j].encoderMax / 2 - joints[i][j].encoderMin / 2); // use halfway point in case of error
			}
			else
			{

				double radianRange = (joints[i][j].radAtEncoderMax - joints[i][j].radAtEncoderMin);
				double encoderRange = joints[i][j].encoderMax - joints[i][j].encoderMin;
				jointEncoders[i][j] = joints[i][j].encoderMin
						+ encoderRange * (jointAngles[i][j] - joints[i][j].radAtEncoderMin) / radianRange;
				if (jointEncoders[i][j] > joints[i][j].encoderMax)
					jointEncoders[i][j] = joints[i][j].encoderMax;
				if (jointEncoders[i][j] < joints[i][j].encoderMin)
					jointEncoders[i][j] = joints[i][j].encoderMin;
			}
		}
}
}
}
