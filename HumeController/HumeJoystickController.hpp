/*
 * HumeJoystickController.hpp
 *
 *    Created on: Apr 21, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#ifndef HUMEJOYSTICKCONTROLLER_HPP_
#define HUMEJOYSTICKCONTROLLER_HPP_

#include "JoystickReceiver.hpp"
#include "TuneableDouble.hpp"
#include "HumeLTIFilters.hpp"
#include "LTIStateSpaceCompensator.hpp"
#include "Oscillator.hpp"
#include <m3hume_pdo_v0_def.h>
#include "comm_udp.h"
#include "HumeIMU.hpp"
#include <iomanip>
#include <iostream>
#include "JoystickKeymap.hpp"
#include "DoubleTuner.hpp"
#include "GainTunerProvider.hpp"
#include "TuneableModule.hpp"
#include "HumeJointTask.hpp"
#include "HumeJointHighLevel.hpp"
#include "HumeJointLowLevel.hpp"

using namespace uta::ui;
namespace uta
{
namespace hume
{
namespace ui
{

enum TuneSelection
{
	HIGH, LOW, TASK
};

class HumeJoystickGainTuner: public uta::ui::JoystickReader, GainTunerProvider
{
	HumeJointHighLevel highLevel[2][3];
	HumeJointLowLevel lowLevel[2][3];
	HumeJointTask tasks[2][3];
	TuneableDouble exampleDouble;
	int count;
	int leg;
	int joint;
	TuneSelection tuneSelect;
	DoubleTuner tuners[2];
protected:
	void handleUpdate();
	void dumpInitializers();
public:
	HumeJoystickGainTuner();
	void usurpLowLevelJointControl(M3HumeEmbeddedStatus* statuses[2][3], message& message, HumeIMU& inertialSensor,
									M3HumeEmbeddedCommand* commands[2][3]);
	void printState();
	void attachGainTuner(int tunerNumber, TuneableDouble* tuneable);
	bool getClick(int i);
	bool getRelease(int i);
	bool getButton(int i);
	double getAxis(int axis);
};

}
}
}

#endif /* HUMEJOYSTICKCONTROLLER_HPP_ */
