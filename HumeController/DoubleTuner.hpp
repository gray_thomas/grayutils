/*
 * DoubleTuner.hpp
 *
 *    Created on: Apr 23, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#ifndef DOUBLETUNER_HPP_
#define DOUBLETUNER_HPP_
#include "TuneableDouble.hpp"
using namespace std;
namespace uta
{
namespace hume
{
namespace ui
{
class DoubleTuner
{
	double magAtStart;
	double linAtStart;
	double valueAtStart;
	bool tuneing;
	TuneableDouble* tuneable;
public:
	void setTuneable(TuneableDouble* tuneable);
	void startTuneing(double mag, double lin);
	void endTuneing();
	void update(double mag, double lin);
	void invert(void);
	void save(void);
	void revert(void);
	double getValue();
	double getSaved();
	string getName();
};
}
}
}

#endif /* DOUBLETUNER_HPP_ */
