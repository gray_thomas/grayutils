/*
 * HumeController.hpp
 *
 *    Created on: Apr 7, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef HUMECONTROLLER_HPP_
#define HUMECONTROLLER_HPP_
#include "m3hume_pdo_v0_def.h"
#include "comm_udp.h"
#include "iostream"
#include "fstream"
#include "Eigen/Dense"
#include "MonolithController.hpp"
#include "HumeJoystickController.hpp"

#include "HumeIMU.hpp"
#include "HumeLogger.hpp"
#include "Oscillator.hpp"
#include "SimpleKinematicObjective.hpp"
#include "SimpleKinematics.hpp"
#include "SimpleCalibration.hpp"
#include "LTIStateSpaceCompensator.hpp"
#include "FrictionBiasGainEstimator.hpp"
#include "HumePlottingFunctions.hpp"
#include <Daemon.hpp>
using namespace uta::threads;
#include <JoystickReceiver.hpp>

using namespace std;
using namespace Eigen;
using namespace uta::ui;
using namespace uta::hume::ui;
namespace uta
{

namespace hume
{

class HumeController: public monolithInterface::MonolithController
{
	Oscillator frequencySweepOscillator;
	Oscillator preSineOscillator;
	Oscillator amplitudeOscillator;
	SimpleKinematics kinematics;
	SimpleCalibration calibration;
	SimpleKinematicObjective objective;
	LTIStateSpaceCompensator rightKneeLeadCompensator;
	HumeJoystickGainTuner joystick;
	HumeLogger logger;
	double jointAngles[2][3];
	short jointEncoders[2][3];
	void doDirectJoystickControl(M3HumeEmbeddedCommand* commands[2][3]);
public:
	HumeController();
	virtual ~HumeController()
	{
	}
	void setupLogging(string filename);
	void doControl(M3HumeEmbeddedStatus* statuses[2][3], message& message, HumeIMU& inertialSensor,
					M3HumeEmbeddedCommand* commandOutputs[2][3]);
};

}
}

#endif /* HUMECONTROLLER_HPP_ */
