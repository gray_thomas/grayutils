/*
 * HighLevelControllerInterface.hpp
 *
 *    Created on: Apr 20, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#ifndef HIGHLEVELCONTROLLERINTERFACE_HPP_
#define HIGHLEVELCONTROLLERINTERFACE_HPP_


namespace uta
{
namespace hume
{
namespace highLevel
{
enum JointMode {Torque, Position};
union JointUnion
{
	double torqueDesiredNewtonMeters;
	double positionDesiredRadians;
};
struct JointOutput
{
	JointMode mode;
	JointUnion desired;
};
struct ControllerInputs
{
	double pelvisLocationMeters[3];
	double pelvisYawPitchRollRadians[3];
	bool leftFootContact;
	bool rightFootContact;
	double jointAnglesRadians[2][3];
	double jointAngularVelocitiesRadiansPerSecond[2][3];
	double jointAngularAccelrationsRadiansPerSecond[2][3];
	double jointTorquesNewtonMeters[2][3];
};
struct ControllerOutputs
{
	JointOutput jointOutputs[2][3];
};

class HighLevelController
{
public:
	virtual ~HighLevelController(){}
	virtual void doHighLevelControl(ControllerInputs& inputs, ControllerOutputs& outputs);
};
}
}
}


#endif /* HIGHLEVELCONTROLLERINTERFACE_HPP_ */
