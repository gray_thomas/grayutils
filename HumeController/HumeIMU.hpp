/*
 * HumeIMU.hpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef HUMEIMU_HPP_
#define HUMEIMU_HPP_
namespace uta
{
namespace hume
{

struct HumeIMU
{
	double orientation[3][3];
	double magnetometer[3];
	double accelerometer[3];
	double angularVelocity[3];
};
}
}
#endif /* HUMEIMU_HPP_ */
