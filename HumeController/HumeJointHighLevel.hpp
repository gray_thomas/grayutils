/*
 * HumeJointHighLevel.hpp
 *
 *    Created on: Apr 23, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#ifndef HUMEJOINTHIGHLEVEL_HPP_
#define HUMEJOINTHIGHLEVEL_HPP_

#include "TuneableModule.hpp"

namespace uta
{
namespace hume
{
namespace ui
{

class HumeJointHighLevel: public TuneableModule
{
public:
	double* proportionalGain;
	double* derivativeGain;
	HumeJointHighLevel();
	double doControl(double desiredPositionRad, double positionRad, double velocityRadPerSec);
	void printState(bool isSelected);
};

}
}
}
#endif /* HUMEJOINTHIGHLEVEL_HPP_ */
