/*
 * HumeLogger.hpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef HUMELOGGER_HPP_
#define HUMELOGGER_HPP_
#include "m3hume_pdo_v0_def.h"
#include "comm_udp.h"
#include "HumeIMU.hpp"
#include "fstream"

using namespace std;
namespace uta
{
namespace hume
{

class HumeLogger
{
	bool isSetup;
	bool useCout;
	long count;
	string fileName;
	ofstream outputFileStream;
public:
	HumeLogger() :
				isSetup(false),
				useCout(true),
				count(0),
				fileName()
	{
	}
	~HumeLogger()
	{
		outputFileStream.close();
	}
	void setupLogging(string filename);
	void setupConsoleLogging();
	void doLogging(M3HumeEmbeddedStatus* statuses[2][3], message& message, HumeIMU& inertialSensor);
};
}
}

#endif /* HUMELOGGER_HPP_ */
