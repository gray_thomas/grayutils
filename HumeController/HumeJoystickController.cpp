/*
 * HumeJoystickController.cpp
 *
 *    Created on: Apr 21, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "HumeJoystickController.hpp"

using namespace std;
namespace uta
{
namespace hume
{
namespace ui
{
double mapShort(short in)
{
	return (double) in / 32767.0;
}
TuneSelection nextTuneSelect(TuneSelection input)
{
	switch (input)
	{
	case LOW:
		return HIGH;
	case HIGH:
		return TASK;
	case TASK:
		return LOW;
	default:
		return LOW;
	}
}
TuneSelection previousTuneSelect(TuneSelection input)
{
	switch (input)
	{
	case LOW:
		return TASK;
	case HIGH:
		return LOW;
	case TASK:
		return HIGH;
	default:
		return LOW;
	}
}

HumeJoystickGainTuner::HumeJoystickGainTuner() :
			count(0),
			leg(0),
			joint(2),
			tuneSelect(TASK),
			tuners()
{
	exampleDouble.name = "example";
	exampleDouble.value = 1.0;
	exampleDouble.saved = 0.5;

	//Initializers
	highLevel[0][0].initialize(1e-05, 1e-05);
	lowLevel[0][0].initialize(1e-05, 0.107055, 0.000592912, 20.7168, 33.9811, 49.2439, -1, 1, 1);
	tasks[0][0].initialize(-21632.1, 0.522309, 15515.3);
	highLevel[0][1].initialize(1e-05, 1e-05);
	lowLevel[0][1].initialize(1182.1, 0.472442, 0.000540643, 0.55581, 57.4792, 31.9446, -1, 1, 13293.6);
	tasks[0][1].initialize(-3598.82, 0.236015, 15027.2);
	highLevel[0][2].initialize(1e-05, 1e-05);
	lowLevel[0][2].initialize(0.0613498, 0.0183682, 0.000361539, 1.09688, 1630.37, 7.23795, -50.8149, 8.51363e-05, 0.573224);
	tasks[0][2].initialize(-76113.2, 0.164105, 50283.6);
	highLevel[1][0].initialize(1e-05, 1e-05);
	lowLevel[1][0].initialize(1e-05, 0.107025, 0.000692067, 1, 1, 1, -1, 1, 1);
	tasks[1][0].initialize(4331.49, 0.52231, 15045.4);
	highLevel[1][1].initialize(1e-05, 1e-05);
	lowLevel[1][1].initialize(1e-05, 0.336226, 0.000523976, 1, 1, 33.9873, -1, 1, 1);
	tasks[1][1].initialize(-894.656, 0.229598, 14988.2);
	highLevel[1][2].initialize(1e-05, 1e-05);
	lowLevel[1][2].initialize(7.34046e-06, -0.0320436, -0.000104918, 0.281381, 2.10083, 25.7253, -1, 0.0343464, 1);
	tasks[1][2].initialize(-79352.6, 0.161222, 48724.6);

}
void HumeJoystickGainTuner::dumpInitializers()
{
	cout << "\n//Initializers\n";
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cout << "highLevel[" << i << "][" << j << "].initialize(";
			highLevel[i][j].dumpInitializer();
			cout << ");\n";
			cout << "lowLevel[" << i << "][" << j << "].initialize(";
			lowLevel[i][j].dumpInitializer();
			cout << ");\n";
			cout << "tasks[" << i << "][" << j << "].initialize(";
			tasks[i][j].dumpInitializer();
			cout << ");\n";
		}
	}
	cout << endl;
}
void HumeJoystickGainTuner::handleUpdate()
{
	cout << "received state ";
	for (int i = 0; i < 14; i++)
		cout << (mostRecentMessage.buttons[i] ? "x" : "o");
	for (int i = 0; i < 7; i++)
		cout << " " << ((double) mostRecentMessage.axes[i]) / 32767.0;
	cout << endl;

	latchButtons();
	// State Transitions
	if (GetButtonClick(DECREASE_JOINT))
		joint = (joint + 2) % 3;
	if (GetButtonClick(INCREASE_JOINT))
		joint = (joint + 1) % 3;
	if (GetButtonClick(HIGHER_COMPONENT))
		tuneSelect = nextTuneSelect(tuneSelect);
	if (GetButtonClick(LOWER_COMPONENT))
		tuneSelect = previousTuneSelect(tuneSelect);
	if (GetButtonClick(LEG_SWITCH))
		leg = (leg + 1) % 2;
	if (GetButtonClick(LEG_SWITCH) || GetButtonClick(LOWER_COMPONENT) || GetButtonClick(HIGHER_COMPONENT)
			|| GetButtonClick(INCREASE_JOINT) || GetButtonClick(DECREASE_JOINT))
	{
		if (tuneSelect == LOW)
			lowLevel[leg][joint].select(this);
		if (tuneSelect == HIGH)
			highLevel[leg][joint].select(this);
		if (tuneSelect == TASK)
			tasks[leg][joint].select(this);
	}

	if (getRelease(DUMP_INITIALIZERS))
	{
		dumpInitializers();
	}

	if (tuneSelect == LOW)
		lowLevel[leg][joint].tune(this);
	if (tuneSelect == HIGH)
		highLevel[leg][joint].tune(this);
	if (tuneSelect == TASK)
		tasks[leg][joint].tune(this);
	if (this->getClick(ATTACH_T0))
		this->attachGainTuner(0, &this->exampleDouble);
	if (this->getClick(START_T0))
		this->tuners[0].startTuneing(this->getAxis(T0M), this->getAxis(T0L));
	if (this->getRelease(START_T0))
		this->tuners[0].endTuneing();
	if (this->getClick(SAVE_T0))
		this->tuners[0].save();
	if (this->getClick(REVERT_T0))
		this->tuners[0].revert();
	if (this->getClick(INVERT_T0))
		this->tuners[0].invert();
	this->tuners[0].update(this->getAxis(T0M), this->getAxis(T0L));

	printState();
}
void HumeJoystickGainTuner::usurpLowLevelJointControl(M3HumeEmbeddedStatus* statuses[2][3], message& message,
														HumeIMU& inertialSensor,
														M3HumeEmbeddedCommand* commands[2][3])
{

	double task = tasks[leg][joint].getPositionDesired();
//	double high = highLevel[leg][joint].doControl(task);
	commands[leg][joint]->feedForward = lowLevel[leg][joint].doControl(task,
			statuses[leg][joint]->reports[0].raw3SpringEncoder, statuses[leg][joint]->reports[0].raw2JointEncoder,
			statuses[leg][joint]->reports[0].raw1MotorEncoder);

	if (!(mostRecentMessage.buttons[ENABLE]))
	{
		for (int i = 0; i < 2; i++)
			for (int j = 0; j < 3; j++)
				commands[i][j]->feedForward = 0;
	}
}
void HumeJoystickGainTuner::printState()
{
	cout << "\n\nHumeJoystickController is " << (mostRecentMessage.buttons[0] ? "ENABLED" : "DISABLED") << "\n";
	cout << "joint[" << leg << "," << joint << "]\n";
	cout << "([Trig] enables, [2] toggles leg, [<][>]: changes joint)\n";
	cout << "TunerA: " << tuners[0].getName();
	cout << " = " << tuners[0].getValue();
	cout << ", saved = " << tuners[0].getSaved() << "\n";
	cout << " (" << T0LN << " linear, " << T0MN << " exponential, " <<
	START_T0N << " starts, " << INVERT_T0N << " inverts, " << SAVE_T0N << " saves, " << REVERT_T0N << " reverts)\n";
	tasks[leg][joint].printState(tuneSelect == TASK);
	highLevel[leg][joint].printState(tuneSelect == HIGH);
	lowLevel[leg][joint].printState(tuneSelect == LOW);
//	cout << "=" << name << "= active = " << value << " ([6] saves) saved = " << saved << " ([7] reverts)";
	cout << "([3] tweaks) ([TL] exponential) ([TR] linear)";
	cout << endl;
}
void HumeJoystickGainTuner::attachGainTuner(int tunerNumber, TuneableDouble* tuneable)
{
	tuners[tunerNumber].setTuneable(tuneable);
}
bool HumeJoystickGainTuner::getClick(int i)
{
	return latches[i].isClicked();
}
bool HumeJoystickGainTuner::getRelease(int i)
{
	return latches[i].isReleased();
}
bool HumeJoystickGainTuner::getButton(int i)
{
	return mostRecentMessage.buttons[i];
}
double HumeJoystickGainTuner::getAxis(int axis)
{
	return mapShort(mostRecentMessage.axes[axis]);
}

//DONE: implement a double tuning to test my code.
//DONE: implement one of the buttons causing a big data dump to the console for gain recovery
//DONE: implement navigation among the various gains
//TODO: implement non-linear damping estimate
//TODO: implement motor zero initialization
//TODO: implement motor based control
//TODO: implement spring based control to compare
//TODO: make sure the button mapping shows up in the printouts.
//TODO: test the hold and tweak functionality.
//TODO: learn the LED constellation
//TODO: write something which estimates the LED constellation
//TODO: look at the IMU data
//TODO: find the delay between the IMU and the PhaseSpace system
//TODO: write a kalman filter which incorporates both sensors but does not understand the dynamics.

}
}
}
