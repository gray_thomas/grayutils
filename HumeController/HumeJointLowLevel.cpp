/*
 * HumeJointLowLevel.cpp
 *
 *    Created on: Apr 23, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "HumeJointLowLevel.hpp"
#include "Eigen/Dense"
using namespace Eigen;
namespace uta
{
namespace hume
{
namespace ui
{

HumeJointLowLevel::HumeJointLowLevel() :
			TuneableModule(9),
			MencZero(0),
			propGain(0),
			lowSpeedD(0),
			stiction(0),
			minSpeed(0),
			friction(0),
			speedThresh(0),
			highSpeedD(0),
			errorTol(0),
			rawOut(0),
			error(0)
{
	tuneables[0].name = "MencZero";
	MencZero = &(tuneables[0].value);
	tuneables[1].name = "propGain";
	propGain = &(tuneables[1].value);
	tuneables[2].name = "dervGain";
	lowSpeedD = &(tuneables[2].value);
	tuneables[3].name = "stiction";
	stiction = &(tuneables[3].value);
	tuneables[4].name = "minSpeed";
	minSpeed = &(tuneables[4].value);
	tuneables[5].name = "friction";
	friction = &(tuneables[5].value);
	tuneables[6].name = "speedThresh";
	speedThresh = &(tuneables[6].value);
	tuneables[7].name = "highSpeedD";
	highSpeedD = &(tuneables[7].value);
	tuneables[8].name = "errorTol";
	errorTol = &(tuneables[8].value);
	motorDiff = setup3rdOrderButterworthDifferentiator(200, 0.001);
	jointDiff = setup3rdOrderButterworthDifferentiator(200, 0.001);
	springDiff = setup3rdOrderButterworthDifferentiator(200, 0.001);

}
double signum(double x)
{
	if (x > 0)
		return 1.0;
	if (x < 0)
		return -1.0;
	return 0.0;
}
double HumeJointLowLevel::doControl(double desiredFromHighLevel, double springEncoder, double jointEncoder,
									double motorEncoder)
{
	VectorXd mDiffInputs(1), mDiffOutputs(3);
	VectorXd jDiffInputs(1), jDiffOutputs(3);
	VectorXd sDiffInputs(1), sDiffOutputs(3);
	mDiffInputs << motorEncoder;
	mDiffOutputs = motorDiff.update(mDiffInputs);
	jDiffInputs << jointEncoder;
	jDiffOutputs = jointDiff.update(jDiffInputs);
	sDiffInputs << springEncoder;
	sDiffOutputs = springDiff.update(sDiffInputs);
	double MencZero = *(this->MencZero);
	double propGain = *(this->propGain);
	double lowSpeedD = *(this->lowSpeedD);
	double stiction = *(this->stiction);
	double minSpeed = *(this->minSpeed);
	double friction = *(this->friction);
	double speedThresh = *(this->speedThresh);
	double highSpeedD = *(this->highSpeedD);
	double errorTol = *(this->errorTol);
	double motor = mDiffOutputs[0];
	double motorDot = mDiffOutputs[1];
	double motorDDot = mDiffOutputs[2];
	error = desiredFromHighLevel - motorEncoder + MencZero;

	rawOut = propGain * (error);
	rawOut += -lowSpeedD * motorDot;
	rawOut += friction * signum(error);
	if ((speedThresh > 0) && (abs(motorDot) > speedThresh))
	{
		double speedInExcessOfThreshold = abs(motorDot) - speedThresh;
		double additionalDamping = highSpeedD - lowSpeedD;
		rawOut += signum(motorDot) * speedInExcessOfThreshold * additionalDamping;
	}
	if ((abs(motorDot) < minSpeed) && (minSpeed > 0))
		rawOut += signum(error) * stiction;
	if ((abs(error) < errorTol) && (errorTol > 0))
	{
		rawOut *= (1.0 - 0.5 * cos(M_PI * error / errorTol));
	}
	return rawOut;
}
void HumeJointLowLevel::printState(bool isSelected)
{
	cout << (isSelected ? "=LowLevel==" : " LowLevel  ") << " error = " << error << "\n           ";
	TuneableModule::printState(isSelected);
	cout << "\n Current = " << rawOut << "\n";
}
}
}
}
