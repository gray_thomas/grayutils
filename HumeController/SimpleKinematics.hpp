/*
 * SimpleKinematics.hpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SIMPLEKINEMATICS_HPP_
#define SIMPLEKINEMATICS_HPP_

#include <Eigen/Dense>
#include "SimpleKinematicObjective.hpp"

using namespace Eigen;
namespace uta
{
namespace hume
{
class SimpleKinematics
{
	double shankLengthMM, kneeForwardMM, femurLengthMM, hipAxisOffsetMM, halfHipSeparationMM,
			ConnectingBarHeightAboveHipMM, ConnectingBarLengthMM, ConnectingBarEffectiveRailHeightMM;
public:
	SimpleKinematics();
	void setParameters(double shankLengthMM, double kneeForwardMM, double femurLengthMM, double hipAxisOffsetMM,
						double halfHipSeparationMM, double ConnectingBarHeightAboveHipMM,
						double ConnectingBarLengthMM, double ConnectingBarEffectiveRailHeightMM);
	void setDefaultParameters();
	void getJointAnglesFromPlanarCOMRelativeToStanceAndTwoTiltPlanes(SimpleKinematicObjective objective,
																		double jointAngles[2][3]);
	void solveForLegAngles(Vector3d footPointInLegFrame, Vector3d& angles);
};
void setupRollMatrix(Matrix3d& rollMatrix, double angle);
void setupPitchMatrix(Matrix3d& rollMatrix, double angle);
}
}


#endif /* SIMPLEKINEMATICS_HPP_ */
