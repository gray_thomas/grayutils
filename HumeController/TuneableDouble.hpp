/*
 * TuneableDouble.hpp
 *
 *    Created on: Apr 23, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#ifndef TUNEABLEDOUBLE_HPP_
#define TUNEABLEDOUBLE_HPP_
#include <iostream>
using namespace std;
namespace uta
{
namespace hume
{
namespace ui
{
struct TuneableDouble
{
public:
	double value;
	double saved;
	string name;
};
}
}
}




#endif /* TUNEABLEDOUBLE_HPP_ */
