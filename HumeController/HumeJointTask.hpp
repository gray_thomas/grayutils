/*
 * HumeJointTask.hpp
 *
 *    Created on: Apr 23, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#ifndef HUMEJOINTTASK_HPP_
#define HUMEJOINTTASK_HPP_

#include "TuneableModule.hpp"
#include "Oscillator.hpp"

namespace uta
{
namespace hume
{
namespace ui
{

class HumeJointTask: public TuneableModule
{
	Oscillator osc;
public:
	double* rawFeedForward;
	double* oscFreq;
	double* oscMagnitude;
	HumeJointTask();
	double getPositionDesired();
	void printState(bool isSelected);
};
}
}
}

#endif /* HUMEJOINTTASK_HPP_ */
