/*
 * SimpleKinematics.cpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "SimpleKinematics.hpp"

namespace uta
{
namespace hume
{
void SimpleKinematics::setParameters(double shankLengthMM, double kneeForwardMM, double femurLengthMM,
										double hipAxisOffsetMM, double halfHipSeparationMM,
										double ConnectingBarHeightAboveHipMM, double ConnectingBarLengthMM,
										double ConnectingBarEffectiveRailHeightMM)
{
	this->shankLengthMM = shankLengthMM;
	this->kneeForwardMM = kneeForwardMM;
	this->femurLengthMM = femurLengthMM;
	this->hipAxisOffsetMM = hipAxisOffsetMM;
	this->halfHipSeparationMM = halfHipSeparationMM;
	this->ConnectingBarHeightAboveHipMM = ConnectingBarHeightAboveHipMM;
	this->ConnectingBarLengthMM = ConnectingBarLengthMM;
	this->ConnectingBarEffectiveRailHeightMM = ConnectingBarEffectiveRailHeightMM;
}
SimpleKinematics::SimpleKinematics()
{
	setDefaultParameters();
}

void SimpleKinematics::setDefaultParameters()
{
	this->shankLengthMM = 410.00;
	this->kneeForwardMM = 18.10;
	this->femurLengthMM = 450.00;
	this->hipAxisOffsetMM = 35.35;
	this->halfHipSeparationMM = 279.40;
	this->ConnectingBarHeightAboveHipMM = 228.0;
	this->ConnectingBarLengthMM = 670.0;
	this->ConnectingBarEffectiveRailHeightMM = 1068.0;
}

void setupRollMatrix(Matrix3d& rollMatrix, double angle)
{
	rollMatrix << 1.0, 0.0, 0.0, //
	0.0, cos(angle), -sin(angle), //
	0.0, sin(angle), cos(angle);
}

void setupPitchMatrix(Matrix3d& rollMatrix, double angle)
{
	rollMatrix << cos(angle), 0.0, sin(angle), //
	0.0, 1.0, 0.0, //
	-sin(angle), 0.0, cos(angle);
}

void SimpleKinematics::solveForLegAngles(Vector3d footPointInLegFrame, Vector3d& angles)
{
	double P4x, P4y, P4z;
	P4x = footPointInLegFrame[0];
	P4y = footPointInLegFrame[1];
	P4z = footPointInLegFrame[2];

	double X4, Z4, a0, a1, a2;
	X4 = P4x;
	Z4 = sqrt(pow(P4z, 2) + pow(P4y, 2));
	a0 = asin(P4y / Z4);

	double F = this->femurLengthMM, K = this->kneeForwardMM, Sh = this->shankLengthMM, Delta = this->hipAxisOffsetMM;
	double femurC2 = pow(F, 2) + pow(K, 2);
	a2 = atan(K / F) - acos((pow(X4, 2) + pow(Z4 - Delta, 2) - femurC2 - pow(Sh, 2)) / (2 * Sh * sqrt(femurC2)));

	a1 = atan2(X4, Z4 - Delta) - atan2(K + Sh * sin(a2), F + Sh * cos(a2));

	angles[0] = a0;
	angles[1] = a1;
	angles[2] = a2;

//	cout << "pow(P4z, 2) = " << pow(P4z, 2) << endl;
//	cout << "pow((P4y - H), 2) = " << pow(P4y, 2) << endl;
//	cout << "Z4 = " << Z4 << endl;
//	cout << "a0 = " << a0 << endl;
//	cout << "a2 = " << a2 << endl;
//	cout << "femurC2 = " << femurC2 << endl;
//	cout << "atan(K/F) = " << atan(K / F) << endl;
//	cout << "(pow(X4,2)+pow(Z4-Delta,2)-femurC2-pow(Sh,2)) = "
//			<< (pow(X4, 2) + pow(Z4 - Delta, 2) - femurC2 - pow(Sh, 2)) << endl;
//	cout << "a1 = " << a1 << endl;
//	cout << "atan2(X4, Z4 - Delta) = " << atan2(X4, Z4 - Delta) << endl;
//	cout << "atan2(K + Sh * sin(a2), F + Sh * cos(a12)) = " << atan2(K + Sh * sin(a2), F + Sh * cos(a2)) << endl;
//	cout << "K + Sh * sin(a2) = " << K + Sh * sin(a2) << endl;
//	cout << "F + Sh * cos(a2) = " << F + Sh * cos(a2) << endl;

}

void SimpleKinematics::getJointAnglesFromPlanarCOMRelativeToStanceAndTwoTiltPlanes(
		SimpleKinematicObjective objective, double jointAngles[2][3])
{
	Vector3d humeFrameOrigin;
	Matrix3d roll;
	Matrix3d pitch;
	Matrix3d humeFrameRotation;

	humeFrameOrigin << 0.0, 0.0, objective.hipHeight;
	setupRollMatrix(roll, objective.bodyRollAboutTheForwardDirection);
	setupPitchMatrix(pitch, objective.forwardLeanAngleRadians);
	humeFrameRotation = pitch * roll;

	Vector3d leftFootPoint;
	Vector3d leftFootPointInHume;
	leftFootPoint << objective.leftFootDistanceAheadOfBody, objective.leftFootDistanceLeftOfBody, 0.0;
	leftFootPointInHume = humeFrameRotation.inverse() * (leftFootPoint - humeFrameOrigin);
//	cout << "left foot in hume frame: \n" << leftFootPointInHume << "\n" << endl;

	Vector3d rightFootPoint;
	Vector3d rightFootPointInHume;
	rightFootPoint << objective.rightFootDistanceAheadOfBody, objective.rightFootDistanceLeftOfBody, 0.0;
	rightFootPointInHume = humeFrameRotation.inverse() * (rightFootPoint - humeFrameOrigin);
//	cout << "right foot in hume frame: \n" << rightFootPointInHume << "\n" << endl;

	Vector3d leftJointAnglesVector;
	Vector3d rightJointAnglesVector;

	Vector3d leftLegFrameOriginInHume;
	Vector3d rightLegFrameOriginInHume;
	leftLegFrameOriginInHume << 0.0, halfHipSeparationMM, 0.0;
	rightLegFrameOriginInHume << 0.0, -halfHipSeparationMM, 0.0;
	double P4x, P4y, P4z;
	P4x = leftFootPointInHume[0];
	P4y = leftFootPointInHume[1] - this->halfHipSeparationMM;
	P4z = leftFootPointInHume[2];

	solveForLegAngles(leftFootPointInHume - leftLegFrameOriginInHume, leftJointAnglesVector);
	solveForLegAngles(rightFootPointInHume - rightLegFrameOriginInHume, rightJointAnglesVector);

	for (int i = 0; i < 3; i++)
	{
		jointAngles[0][i] = rightJointAnglesVector[i];
		jointAngles[1][i] = leftJointAnglesVector[i];
	}
}
}
}

