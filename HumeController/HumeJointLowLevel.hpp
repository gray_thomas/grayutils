/*
 * HumeJointLowLevel.hpp
 *
 *    Created on: Apr 23, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#ifndef HUMEJOINTLOWLEVEL_HPP_
#define HUMEJOINTLOWLEVEL_HPP_


#include "TuneableModule.hpp"
#include "LTIStateSpaceCompensator.hpp"
#include "HumeLTIFilters.hpp"



namespace uta
{
namespace hume
{
namespace ui
{

class HumeJointLowLevel: public TuneableModule
{
public:
	double* MencZero;
	double* propGain;
	double* lowSpeedD;
	double* stiction;
	double* minSpeed;
	double* friction;
	double* speedThresh;
	double* highSpeedD;
	double* errorTol;
	double rawOut;
	double error;
	LTIStateSpaceCompensator springDiff;
	LTIStateSpaceCompensator jointDiff;
	LTIStateSpaceCompensator motorDiff;
	HumeJointLowLevel();
	double doControl(double desiredFromHighLevel, double springEncoder, double jointEncoder, double motorEncoder);
	void printState(bool isSelected);
};
}
}
}


#endif /* HUMEJOINTLOWLEVEL_HPP_ */
