/*
 * HumeController.cpp
 *
 *    Created on: Apr 7, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "HumeController.hpp"
#include <iomanip>
#include <string>
#include <fstream>
#include <cmath>
#include <Eigen/Dense>
#include <iostream>
#include "HumeLTIFilters.hpp"

using namespace Eigen;
using namespace std;
namespace uta
{
namespace hume
{

monolithInterface::MonolithController* monolithInterface::instantiateDefaultController()
{
	return new HumeController;
}

HumeController::HumeController() :
			frequencySweepOscillator(0.001),
			preSineOscillator(0.001),
			amplitudeOscillator(0.001),
			kinematics(),
			calibration(),
			objective(),
			logger(),
			jointAngles(),
			jointEncoders()
{
	objective.leftFootDistanceAheadOfBody = 000;
	objective.rightFootDistanceAheadOfBody = 000;
	objective.leftFootDistanceLeftOfBody = 200;
	objective.rightFootDistanceLeftOfBody = -200;
	objective.hipHeight = 740;
	objective.forwardLeanAngleRadians = -0.3;
	objective.bodyRollAboutTheForwardDirection = 0.00;
	rightKneeLeadCompensator = setupLead(1.0 / 8.69e2, 10, 1, 0.001);
	joystick.start();
}

void HumeController::setupLogging(string filename)
{
	logger.setupLogging(filename);
}
double mapShort(short in)
{
	return (double) in / 32767.0;
}
void setupDefaultLowLevelFeedback(M3HumeEmbeddedCommand* commands[2][3])
{
	short lowLevelControllersMatrix[2][3][CTRL_MATRIX_ROWS][CTRL_MATRIX_COLS] =
	{
	{

	{
	{ sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0) },
	{ sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0) },
	{ sh(0.0), sh(0.0), sh(0.0), sh(-0.0), sh(0.0), sh(0.0) } }, // stable position feedback is negative
			{
			{ sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0) },
			{ sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0) },
			{ sh(0.0), sh(0.0), sh(0.0), sh(-0.0), sh(0.0), sh(0.0) } }, // stable position feedback is negative
			{
			{ sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0) },
			{ sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0) },
			{ sh(0.0), sh(0.0), sh(0.0), sh(-0.0), sh(0.0), sh(0.0) } }, }, // stable position feedback is negative
			{
			{
			{ sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0) },
			{ sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0) },
			{ sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0) } }, // stable position feedback is positive
					{
					{ sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0) },
					{ sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0) },
					{ sh(0.0), sh(0.0), sh(0.0), sh(0), sh(0.0), sh(0.0) } }, // stable position feedback is positive
					{
					{ sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0) },
					{ sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0), sh(0.0) },
					{ sh(0.0), sh(0.0), sh(0.0), sh(-0.0), sh(0.0), sh(0.0) } } } }; // stable position feedback is negative

	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			commands[i][j]->bitShiftDACOutputBoost = 0;
			commands[i][j]->bitShift1MotorEncoderSignalBoost = 0;
			commands[i][j]->bitShift2JointEncoderSignalBoost = 0;
			commands[i][j]->bitShift3SpringEncoderSignalBoost = 0;

			for (unsigned k = 0; k < CTRL_MATRIX_ROWS; k++)
				for (unsigned l = 0; l < CTRL_MATRIX_COLS; l++)
					commands[i][j]->controlMatrix[k][l] = lowLevelControllersMatrix[i][j][k][l];
		}
	}
}

void HumeController::doControl(M3HumeEmbeddedStatus* statuses[2][3], message& message, HumeIMU& inertialSensor,
								M3HumeEmbeddedCommand* commands[2][3])
{
	logger.doLogging(statuses, message, inertialSensor);

	kinematics.getJointAnglesFromPlanarCOMRelativeToStanceAndTwoTiltPlanes(objective, jointAngles);
	calibration.getEncodersFromJointAngles(jointAngles, jointEncoders);

	setupDefaultLowLevelFeedback(commands);

	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			commands[i][j]->feedForward = 0.0;
			commands[i][j]->bitShiftDACOutputBoost = 0;
			commands[i][j]->expected1MotorEncoder = 0;
			commands[i][j]->expected2JointEncoder = jointEncoders[i][j];
			commands[i][j]->expected3SpringEncoder = 8000;
		}
	}
//	doDirectJoystickControl(commands);
	joystick.usurpLowLevelJointControl(statuses, message, inertialSensor, commands);

	for (int i = 0; i < 2; i++)
	{
		commands[i][0]->dacLimit = 250;
		commands[i][1]->dacLimit = 250;
		commands[i][2]->dacLimit = 135;
		for (int j = 0; j < 3; j++)
		{
			commands[i][j]->watchdog++;
			commands[i][j]->watchdog %= 100;

			commands[i][j]->feedbackLimit = 0;
			commands[i][j]->control_flags.enable = 1;
		}
	}
}
void HumeController::doDirectJoystickControl(M3HumeEmbeddedCommand* commands[2][3])
{
	int leg = 0;
	int joint = 2;
	if (joystick.getJoystick().buttons[1])
		joint = 1;
	if (joystick.getJoystick().buttons[3])
		joint = 2;
	if (joystick.getJoystick().buttons[1] && joystick.getJoystick().buttons[3])
		joint = 0;
	if (joystick.getJoystick().buttons[2] || joystick.getJoystick().buttons[4])
		leg = 1;
	if (joystick.getJoystick().buttons[2])
		joint = 1;
	if (joystick.getJoystick().buttons[4])
		joint = 2;
	if (joystick.getJoystick().buttons[2] && joystick.getJoystick().buttons[4])
		joint = 0;
	if ((joystick.getJoystick().buttons[2] || joystick.getJoystick().buttons[4] || joystick.getJoystick().buttons[1]
			|| joystick.getJoystick().buttons[3]))
	{
		double proportional = mapShort(joystick.getJoystick().axes[2]);
		double derivative = mapShort(joystick.getJoystick().axes[4]);
		commands[leg][joint]->feedForward = mapShort(joystick.getJoystick().axes[1])
				* commands[leg][joint]->dacLimit; //rightKneeLeadCompensator.update(leadCompensatorInputs)[0];
		if (!joystick.getJoystick().buttons[0])
			commands[leg][joint]->feedForward = 0;
	}
}

}
}

