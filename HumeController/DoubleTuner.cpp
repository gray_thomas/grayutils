/*
 * DoubleTuner.cpp
 *
 *    Created on: Apr 23, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include "DoubleTuner.hpp"
#include "math.h"
namespace uta
{
namespace hume
{
namespace ui
{
void DoubleTuner::setTuneable(TuneableDouble* tuneable)
{
	if (NULL != tuneable)
	{
		//handle surprise escape: revert to saved value
		if (tuneing)
			revert();
	}
	this->tuneable = tuneable;
}
void DoubleTuner::endTuneing(void)
{
	this->tuneing = false;
}
void DoubleTuner::startTuneing(double mag, double lin)
{
	if (NULL == tuneable)
		return;
	tuneing = true;
	magAtStart = mag;
	linAtStart = lin;
	valueAtStart = tuneable->value;
}
void DoubleTuner::update(double mag, double lin)
{
	if (tuneing)
		tuneable->value = valueAtStart * (1 + (lin - linAtStart) * 0.1) * exp(3 * (mag - magAtStart));
}
void DoubleTuner::invert(void)
{
	valueAtStart *= -1;
}
void DoubleTuner::save(void)
{
	if (NULL == tuneable)
		return;
	tuneable->saved = tuneable->value;
}
void DoubleTuner::revert(void)
{
	if (NULL == tuneable)
		return;
	tuneing = false;
	tuneable->value = tuneable->saved;
}
double DoubleTuner::getValue()
{
	if (NULL == tuneable)
		return 0;
	return tuneable->value;
}
double DoubleTuner::getSaved()
{
	if (NULL == tuneable)
		return 0;
	return tuneable->saved;
}
string DoubleTuner::getName()
{
	if (NULL == tuneable)
		return string("Unassigned");
	return tuneable->name;
}
}
}
}
