// #include <ros/ros.h>
#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <sdf/parser.hh>
#include <stdio.h>
#include <unistd.h>

namespace gazebo
{
  class ValkyrieAnklePlugin: public ModelPlugin
  {
  public:
    ValkyrieAnklePlugin()
    {
    }

    ~ValkyrieAnklePlugin()
    {
    }

    template<typename T>
    T sdf_get_value(sdf::ElementPtr sdf, const std::string &key, const T &def)
    {
      if(sdf->HasElement(key))
      {
        std::string value = sdf->GetElement(key)->Get<std::string>();
        return boost::lexical_cast<T>(value);
      }
      else
        return def;
    }

    void Load(physics::ModelPtr _parent, sdf::ElementPtr sdfElement)
    {
      std::cerr << "Loading ValkyrieAnklePlugin" << std::endl;
      this->model = _parent;
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&ValkyrieAnklePlugin::OnUpdate, this, _1));

      gazebo::physics::Joint_V joints = this->model->GetJoints();
      gazebo::physics::Link_V links = this->model->GetLinks();

      for(unsigned int i = 0; i < joints.size(); i++)
      {
        joints[i]->GetName();
        joints[i]->GetLowStop(0).Radian();
        joints[i]->GetHighStop(0).Radian();
      }

      // next_loop_time = boost::get_system_time();
      std::cerr << "Done loading ValkyireAnklePlugin" << std::endl;
    }


    long last_iters;
    long iters;

    // Called by the world update start event
    void OnUpdate(const common::UpdateInfo & /*_info*/)
    {
      double seconds_per_step = this->model->GetWorld()->GetPhysicsEngine()->GetUpdatePeriod();
      double alternate_time = this->model->GetWorld()->GetSimTime().Double();
      // std::cerr << "Update spam! from ValkyireAnklePlugin" << std::endl;
      last_iters = iters;
      iters = this->model->GetWorld()->GetIterations();

      gazebo::physics::Joint_V joints = this->model->GetJoints();

      // read directly from the links
      for(unsigned int i = 0; i < joints.size(); i++)
      {
        joints[i]->GetName();
        gazebo::physics::JointWrench gazebo_wrench = joints[i]->GetForceTorque(0);
        math::Vector3 axis = joints[i]->GetLocalAxis(0);
        double raw_position = joints[i]->GetAngle(0).Radian();
        double raw_velocity = joints[i]->GetVelocity(0);
        double raw_torque = -(axis.x * gazebo_wrench.body2Torque.x + 
          axis.y * gazebo_wrench.body2Torque.y + 
          axis.z * gazebo_wrench.body2Torque.z);

      }
      // read more from the root link
      gazebo::physics::Link_V links = this->model->GetLinks();
      gazebo::math::Pose root_link_pose = links.at(0)->GetWorldPose();
      gazebo::math::Vector3 root_link_linear_vel = links.at(0)->GetWorldLinearVel();
      gazebo::math::Vector3 root_link_angular_vel = links.at(0)->GetWorldAngularVel();
      gazebo::math::Vector3 root_link_linear_accel = links.at(0)->GetWorldLinearAccel();
      gazebo::math::Vector3 root_link_angular_accel = links.at(0)->GetWorldAngularAccel();

      //apply command
      for(unsigned int i = 0; i < joints.size(); i++)
      {
          joints[i]->SetForce(0, 0.0);
      }
    }

  private:
    physics::ModelPtr model;
    event::ConnectionPtr updateConnection;
  };

// Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN (ValkyrieAnklePlugin)

} // namespace gazebo