#include <string>
#include <fstream>
#include <vector>
namespace gu
{
extern std::string active_folder; // global_internal_state: eg DyNetUtils/test1
std::string getDataFolderPath(std::string folderName);
std::string data_path(std::string folderName, std::string fileName);
std::string data_local(std::string fileName);
int prepareDataFolder(std::string folderName);
int set_active_folder(std::string folderName);
inline std::string get_active_folder(){return active_folder;}
std::vector<std::string>& split(const std::string& s, char delim, std::vector<std::string>& elems);
std::vector<std::string> split(const std::string& s, char delim);
}