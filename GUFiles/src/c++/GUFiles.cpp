#include "GUFiles.hpp"
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <sys/types.h>
namespace gu
{
std::string active_folder("");
std::string getDataFolderPath(std::string folderName)
{
	return std::string(GRAY_UTILS_DATA)+std::string("/")+folderName;
}
std::string data_path(std::string folderName, std::string fileName)
{
	return std::string(GRAY_UTILS_DATA)+std::string("/")+folderName+std::string("/")+fileName;
}
std::vector<std::string>& split(const std::string& s, char delim, std::vector<std::string>& elems) 
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) 
    {
        elems.push_back(item);
    }
    return elems;
}
std::vector<std::string> split(const std::string& s, char delim) 
{
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}
int prepareDataFolder(std::string folderName)
{
	if (system(NULL))
	{
		std::vector<std::string> elems=gu::split(folderName,'/');
		std::string folder=elems[0];
		std::string mystring("mkdir "+gu::getDataFolderPath(folder));		
		std::cout<<"executing shell command: >>>"<<mystring<<std::endl;	
		system(mystring.c_str());
		for (int i=1;i<elems.size();i++)
		{
			folder+="/"+elems[i];
			std::string mystring("mkdir -p "+gu::getDataFolderPath(folder));		
			system(mystring.c_str());
			std::cout<<"executing shell command: >>>"<<mystring<<std::endl;	
		}
		return 0;
	} 
	else
	{
		std::cerr<<"system call failed"<<std::endl;
		return 1;
	}
}
int set_active_folder(std::string folderName)
{
	active_folder=folderName;
	return prepareDataFolder(active_folder);
}
std::string data_local(std::string fileName)
{
	return data_path(active_folder,fileName);
}
}