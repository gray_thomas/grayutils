#include <iostream>
#include <fstream>
#include "GUFiles.hpp"
void test_manual()
{
	gu::prepareDataFolder("test_folder");
	std::ofstream outfile;
	outfile.open(gu::data_path("test_folder","test.txt"));
	outfile<<"hello world"<<std::endl;
	outfile.close();
	std::ifstream infile;
	infile.open(gu::data_path("test_folder","test.txt"));
	std::string line;
	// infile.getline(line);
	std::getline (infile,line);
	std::cout << line << std::endl;
	infile.close();
}
void test_active()
{
	gu::set_active_folder("test2/test_a/active_folder_test");
	std::ofstream outfile;
	outfile.open(gu::data_local("hello.txt"));
	outfile<<"Hello world! Active folder: "<<gu::get_active_folder()<<std::endl;
	outfile.close();
	std::ifstream infile;
	infile.open(gu::data_local("hello.txt"));
	std::string line;
	// infile.getline(line);
	std::getline (infile,line);
	std::cout << line << std::endl;
	infile.close();
}
int main()
{	
	test_manual();
	test_active();
	
	// std::cout <<gu::getDataFolderPath("hats")<<std::endl;
}