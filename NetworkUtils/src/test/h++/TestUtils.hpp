/*
 * TestUtils.hpp
 *
 *    Created on: Nov 23, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef TESTUTILS_HPP_
#define TESTUTILS_HPP_

#include <exception>
#include <stdexcept>
#include <string>
#include <Eigen/Dense>

namespace uta
{
namespace testUtils
{

class AssertionFailedException: public std::exception
{
	std::string myString;
public:
	AssertionFailedException(std::string inputString);
	~AssertionFailedException() throw (){}
	virtual const char * what() const throw();
};

void assertEquals(std::string testName, double value, double expected, double tolerance);
void assertNotEqual(std::string testName, double value, double forbiddenValue, double tolerance);
void assertEqualsQuietly(double value, double expected, double tolerance);
void assertEqualMatrices(std::string matrixName,
                         Eigen::MatrixXd value, Eigen::MatrixXd expected, double tolerance);
void assertEqualMatricesVerbose(std::string matrixName,
                         Eigen::MatrixXd value, Eigen::MatrixXd expected, double tolerance);
}
}

#endif /* TESTUTILS_HPP_ */
