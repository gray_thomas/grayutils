/*
 * testNetworkUtils.cpp
 *
 *    Created on: Feb 15, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include <iostream>
#include <string>
#include <TestUtils.hpp>
// #include <DyNetPlotter.hpp>
using namespace uta::testUtils;

int main(int argc, const char* argv[])
{
	std::cout << "testNetworkUtils is a trivial test" << std::endl;
	assertEquals("assertion test",1.0,1.0,1e-8);
	// uta::networkUtils::dyNetUtils::populateEmitter();
	std::cout << "testNetworkUtils has passed." << std::endl;
	return 0;
}


