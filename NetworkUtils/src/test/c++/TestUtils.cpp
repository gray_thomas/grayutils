/*
 * TestUtils.cpp
 *
 *    Created on: Nov 23, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "TestUtils.hpp"

#include <memory>
#include <sstream>
#include <iostream>
#include <string>
#include <cmath>
#include <Eigen/Dense>

using namespace std;
namespace uta
{
namespace testUtils
{

AssertionFailedException::AssertionFailedException(string inputString)
{
	myString = inputString;
}
const char * AssertionFailedException::what() const throw ()
{
	cerr << myString << endl;
	return "AssertionFailedException";
}

void assertEquals(string testName, double value, double expected, double tolerance)
{
	cout << "assertion: " << testName << " : " << value << " aught to equal the following: " << expected << endl;
	if (std::isnan(value))
		throw AssertionFailedException("actual value is NaN!");

	if (abs(value - expected) > tolerance)
	{
		stringstream ss;
		ss << testName << " failed since " << value << " fails to equal " << expected;
		cout << ss.str() << endl;
		throw AssertionFailedException(ss.str());
	}
	cout << "\tsuccess: " << testName << " is as expected" << endl;
}
void assertNotEqual(string testName, double value, double forbiddenValue, double tolerance)
{
	cout << "assertion: " << testName << " : " << value << " should completely avoid the following: "
			<< forbiddenValue << endl;
	if (std::isnan(value))
		throw AssertionFailedException("actual value is NaN!");

	if (abs(value - forbiddenValue) < tolerance)
	{
		stringstream ss;
		ss << testName << " failed since " << value << " fails to avoid " << forbiddenValue;
		cout << ss.str() << endl;
		throw AssertionFailedException(ss.str());
	}
	cout << "\tsuccess: " << testName << " is as forbidden" << endl;
}
void assertEqualsQuietly(double value, double expected, double tolerance)
{
	if (std::isnan(value))
		throw AssertionFailedException("actual value is NaN!");

	if (abs(value - expected) > tolerance)
	{
		throw AssertionFailedException("shh");
	}
}
void assertEqualMatrices(string matrixName, Eigen::MatrixXd value, Eigen::MatrixXd expected, double tolerance)
{
	for (unsigned i = 0; i < expected.rows(); i++)
		for (unsigned j = 0; j < expected.cols(); j++)
			try
			{
				assertEqualsQuietly((double) value(i, j), (double) expected(i, j), tolerance);
			} catch (AssertionFailedException& e)
			{
				stringstream ss;
				ss << matrixName << " not as expected! Matrix[" << i << "," << j << "] = " << value(i, j) << " != "
						<< expected(i, j) << " within tolerance " << tolerance;
				cout << ss.str() << endl;
				throw AssertionFailedException(ss.str());
			}
}
void assertEqualMatricesVerbose(string matrixName, Eigen::MatrixXd value, Eigen::MatrixXd expected, double tolerance)
{
	for (unsigned i = 0; i < expected.rows(); i++)
		for (unsigned j = 0; j < expected.cols(); j++)
			try
			{
				assertEqualsQuietly((double) value(i, j), (double) expected(i, j), tolerance);
			} catch (AssertionFailedException& e)
			{
				stringstream ss;
				ss << matrixName << " not as expected! Matrix[" << i << "," << j << "] = " << value(i, j) << " != "
						<< expected(i, j) << " within tolerance " << tolerance;
				cout << ss.str() << endl;
				throw AssertionFailedException(ss.str());
			}
	cout << "\tsuccess: " << matrixName << " is as expected!" << endl;
}

}
}
//#include <DyNetPlotter.hpp>
//#include <iostream>
//#include <yaml-cpp/yaml.h>
//using namespace YAML;
//using namespace std;
//void subFunc()
//{
//	Emitter out;
//	out << BeginSeq;
//	out << "fuzzy";
//	out << "bunnies";
//	out << EndSeq;
//	cout << out.c_str() << endl;
//}
//
//void uta::networkUtils::dyNetUtils::populateEmitter()
//{
//	cout << "this is in test utils.cpp with .a archive of yaml-cpp" << endl;
//	subFunc();
//	cout << "emitter safely deleted" << endl;
//}
//void uta::networkUtils::dyNetUtils::altRenderYAML(void)
//{
//	cout << "altRenderYAML" << endl;
//	//TODO: determine why YAML::Emitter destruction causes crash.
//	Emitter emitter;
//	emitter << BeginDoc;
//	emitter << BeginSeq;
//	cout << "hey" << endl;
//	emitter << "eggs";
//	cout << "hey" << endl;
//	emitter << "bread";
//	emitter << "milk";
//
//	emitter << EndSeq;
//	emitter << EndDoc;
////	string value=emitter.c_str();
//	std::string output = emitter.c_str();
//	std::string lastError = emitter.GetLastError();
//	string value = lastError;
//	cout << output << value << endl;
//}

