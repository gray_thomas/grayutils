/*
 * ProcessableGraphNode.hpp
 *
 *    Created on: Nov 9, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef PROCESSABLEGRAPHNODE_HPP_
#define PROCESSABLEGRAPHNODE_HPP_
#include <vector>
namespace uta
{
namespace networkUtils
{
void testProcessableGraphNode(void);
class ProcessableGraphNode
{
public:
	ProcessableGraphNode(void* context, void (*execute)(void*), void* (*getOutput)(void*, unsigned),
							void (*registerInput)(void*, void*,unsigned));

	void recursiveSetup(void* setupInput);
	void recursiveClear(char clearNumber);
	void recursiveExecute(void);
	void addOutput(ProcessableGraphNode* newOutput);
	int getNumberOfOutputs(void)
	{
		return outputs.size();
	}
	int getNumberOfInputs(void)
	{
		return numberOfInputs;
	}
	int getNumberOfInputsRegistered(void)
	{
		return inputsRegistered;
	}
protected:
	int inputsRegistered;
	int numberOfInputs;
	char lastClearNumber;
	std::vector<ProcessableGraphNode*> outputs;
	void* context;
	void (*executeFunctionPointer)(void*);
	void* (*ptrToGetOutput)(void*, unsigned);
	void (*ptrToAddInput)(void*, void*, unsigned);

};
}
}

#endif /* PROCESSABLEGRAPHNODE_HPP_ */
