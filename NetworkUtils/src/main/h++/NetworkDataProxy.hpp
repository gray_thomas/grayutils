/*
 * NetworkDataProxy.hpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef NETWORKDATAPROXY_HPP_
#define NETWORKDATAPROXY_HPP_
#include <vector>

using namespace std;
namespace uta
{
namespace networkUtils
{
class AbstractMultiInputMultiOutputNetworkObject;
class NetworkState;
class NetworkDataProxy
{
	AbstractMultiInputMultiOutputNetworkObject* object;
	bool doConnectForward;
	bool doConnectReverse;
public:
	NetworkDataProxy(AbstractMultiInputMultiOutputNetworkObject* object);
	NetworkState& getNetworkState();
	virtual ~NetworkDataProxy(void);
	AbstractMultiInputMultiOutputNetworkObject* getObject();
	virtual unsigned getNumberOfIndexes(void)=0;
	virtual int getIndex(unsigned i)=0;
	bool connectForward(void);
	bool connectReverse(void);
	void setConnectForward(bool connect);
	void setConnectReverse(bool connect);
};
class MultiIndexNetworkDataProxy: public NetworkDataProxy
{
	vector<int> indexes;
public:
	MultiIndexNetworkDataProxy(AbstractMultiInputMultiOutputNetworkObject* object);
	unsigned getNumberOfIndexes(void);
	void addIndex(int index);
	int getIndex(unsigned i);
};
class SingleIndexNetworkDataProxy: public NetworkDataProxy
{
	int index;
public:
	SingleIndexNetworkDataProxy(AbstractMultiInputMultiOutputNetworkObject* object, int index) ;
	unsigned getNumberOfIndexes(void);
	int getIndex(unsigned i);
	int getIndex(void);
};
class NoIndexNetworkDataProxy: public NetworkDataProxy
{
public:
	NoIndexNetworkDataProxy(AbstractMultiInputMultiOutputNetworkObject* object);
	unsigned getNumberOfIndexes(void);
	int getIndex(unsigned i);
};

}
}

#endif /* NETWORKDATAPROXY_HPP_ */
