/*
 * ScalarElement.hpp
 *
 *    Created on: Nov 8, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SCALARELEMENT_HPP_
#define SCALARELEMENT_HPP_
#include <vector>
#include "ProcessableGraphNode.hpp"
#include "NetworkStateVector.hpp"
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "NetworkDataProxy.hpp"
#include <cmath>
using namespace std;

namespace uta
{
namespace networkUtils
{

class ScalarElement: public AbstractMultiInputMultiOutputNetworkObject
{
public:
	ScalarElement(NetworkState& dataStore);
	NetworkState& getDataStore();
	SingleIndexNetworkDataProxy* getScalarResult();
protected:
	int index;
	SingleIndexNetworkDataProxy scalarOutput;
	NetworkState& dataStore;
};


}
}
#endif /* SCALARELEMENT_HPP_ */
