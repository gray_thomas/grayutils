/*
 * NetworkStateVector.hpp
 *
 *    Created on: Nov 8, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef NETWORKSTATEVECTOR_HPP_
#define NETWORKSTATEVECTOR_HPP_

#include <iostream>
#include <vector>
#include <list>
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"

using namespace std;

namespace uta
{
namespace networkUtils
{
class AbstractMultiInputMultiOutputNetworkObject;
class NetworkState
{
public:
	NetworkState()
	{
	}
	virtual ~NetworkState()
	{
	}
	virtual int allocateTrainableIndex(void)=0;
	virtual int allocateNonTrainableIndex(void)=0;
	virtual int allocateConstantIndex(void)=0;
	virtual void setValue(int index, double value)=0;
	virtual double getValue(int index)=0;
	virtual void setPartial(int index, double partial)=0;
	virtual double getPartial(int index)=0;
	virtual void addToValue(int index, double value)=0;
	virtual void addToPartial(int index, double value)=0;
	virtual void registerDestructable(AbstractMultiInputMultiOutputNetworkObject*)=0;
};
using namespace std;
class NetworkStateVector: public NetworkState
{
private:
	vector<bool> wipeThisPartial;
	vector<double> values;
	vector<double> partials;
	vector<unsigned> trainableIndexes;
	vector<unsigned> tempIndexes;
	vector<AbstractMultiInputMultiOutputNetworkObject*> objectsToDestroy;
public:
	NetworkStateVector() :
				NetworkState(),
				wipeThisPartial(),
				values(),
				partials(),
				trainableIndexes(),
				tempIndexes(),
				objectsToDestroy()
	{
	}
	virtual ~NetworkStateVector();
	virtual int allocateTrainableIndex(void);
	virtual int allocateNonTrainableIndex(void);
	virtual int allocateConstantIndex(void);
	virtual void setValue(int index, double value);
	virtual double getValue(int index);
	virtual void setPartial(int index, double partial);
	virtual double getPartial(int index);
	virtual void addToValue(int index, double value);
	virtual void addToPartial(int index, double partial);
	virtual void registerDestructable(AbstractMultiInputMultiOutputNetworkObject*);

	void packPartialsWithRespectToTrainableData(vector<double>& partialsWithRespectToTrainableData);
	void packTrainableData(vector<double>& trainableData);
	vector<double> getPartialsWithRespectToTrainableData(void);
	vector<double> getTrainableData(void);
	void setTrainableData(vector<double>);
	unsigned getNumberOfTrainableValues(void){return trainableIndexes.size();}

	const vector<unsigned>& viewTrainableIndexes(void){return trainableIndexes;}

	void print(void);
	void setPartialAsInput(int index);
	void resetGradients(void);
	void resetNetwork(void);

};

}
}

#endif /* NETWORKSTATEVECTOR_HPP_ */
