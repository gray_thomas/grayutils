/*
 * AbstractMultiInputMultiOutputNetworkObject.hpp
 *
 *    Created on: Nov 18, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef ABSTRACTMULTIINPUTMULTIOUTPUTNETWORKOBJECT_HPP_
#define ABSTRACTMULTIINPUTMULTIOUTPUTNETWORKOBJECT_HPP_
#include <vector>
#include "NetworkStateVector.hpp"
#include "NetworkDataProxy.hpp"
using namespace std;
namespace uta
{
namespace networkUtils
{
class NetworkState;
class AbstractMultiInputMultiOutputNetworkObject;
class AbstractMultiInputMultiOutputNetworkObject
{
public:
	AbstractMultiInputMultiOutputNetworkObject(NetworkState& dataStore);
	virtual ~AbstractMultiInputMultiOutputNetworkObject();
	virtual void forwardCalculate(void)=0;
	virtual void reverseCalculate(void)=0;
	void recursiveClear(char clearNumber);
	void forwardRecursiveExecute(void);
	void reverseRecursiveExecute(void);

	virtual void addInputForwardOnly(NetworkDataProxy* input);
	virtual void addInputReverseOnly(NetworkDataProxy* input);
	virtual void addInput(NetworkDataProxy* input);

	void print(void);
	NetworkState& getDataStore(void){return dataStore;}
protected:
	vector<int> forwardInputIndexes;
	NetworkState& dataStore;

private:
	static void connectForwardExecutionGraph(AbstractMultiInputMultiOutputNetworkObject* input,
												AbstractMultiInputMultiOutputNetworkObject* output);
	static void connectReverseExecutionGraph(AbstractMultiInputMultiOutputNetworkObject* input,
												AbstractMultiInputMultiOutputNetworkObject* output);
	void addInputIndexVector(vector<int> inputIndexes);
	void addInputIndex(int inputIndex);
	void addInputIndexes(NetworkDataProxy* networkProxy);
	unsigned forwardNumberOfInputs;
	unsigned forwardInputsRegistered;
	unsigned reverseInputsRegistered;
	unsigned reverseNumberOfInputs;
	char lastClearNumber;
	vector<AbstractMultiInputMultiOutputNetworkObject*> forwardOutputs;
	vector<AbstractMultiInputMultiOutputNetworkObject*> reverseOutputs;
};
}
}

#endif /* ABSTRACTMULTIINPUTMULTIOUTPUTNETWORKOBJECT_HPP_ */
