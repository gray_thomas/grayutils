/*
 * ScalarConstant.hpp
 *
 *    Created on: Dec 11, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SCALARCONSTANT_HPP_
#define SCALARCONSTANT_HPP_


#include "ScalarElement.hpp"
#include "NetworkStateVector.hpp"
#include "NetworkDataProxy.hpp"
using namespace std;

namespace uta
{
namespace networkUtils
{

class ScalarConstant: public ScalarElement
{
private:
	double value;
public:
	ScalarConstant(NetworkState& dataStore, NoIndexNetworkDataProxy* inputDataProxy, double value);
	~ScalarConstant();

	virtual void forwardCalculate(void);
	virtual void reverseCalculate(void);
};
}
}




#endif /* SCALARCONSTANT_HPP_ */
