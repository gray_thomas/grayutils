/*
 * NetworkLayout.hpp
 *
 *    Created on: Nov 8, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef NETWORK_HPP
#define NETWORK_HPP

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "NetworkDataProxy.hpp"
#include "Trainable.hpp"
#include <stdexcept>
#include <Eigen/Dense>
using namespace std;
using namespace Eigen;
namespace uta
{
namespace networkUtils
{

class Network: public AbstractMultiInputMultiOutputNetworkObject, virtual public Trainable
{
public:
	Network(NetworkStateVector& stateVector, unsigned inputs);
	virtual ~Network()
	{
	}
	virtual void forwardCalculate(void);
	virtual void reverseCalculate(void);
	virtual void addInput(NetworkDataProxy* input);

	void clear(void);
	void executeForwardNetwork(void);
	void executeGradientNetwork(void);

//	void setInput(unsigned index, double value);
//	void setPartialRelativeToOutput(unsigned indexOfOutput, double partial);
//	double getOutput(unsigned indexOfOutput);
//	double getPartialRelativeToInput(unsigned indexOfInput);

	virtual vector<double> getWeights(void);
	virtual vector<double> getWeightPartials(void);
	virtual vector<double> getInputPartials(void);
	virtual vector<double> getOutputs(void);

	virtual double getWeight(unsigned index);
	virtual double getWeightPartial(unsigned index);
	virtual double getInputPartial(unsigned index);
	virtual double getOutput(unsigned index);

	virtual void packWeights(vector<double>& weights);
	virtual void packWeightPartials(vector<double>& weightPartials);
	virtual void packInputPartials(vector<double>& inputPartials);
	virtual void packOutputs(vector<double>& output);

	virtual void setWeights(vector<double>& weights);
	virtual void setInputs(vector<double>& input);
	virtual void setOutputPartials(vector<double>& outputPartials);

	virtual void setWeight(unsigned index, double weight);
	virtual void setInput(unsigned index, double input);
	virtual void setOutputPartial(unsigned index, double outputPartial);

	virtual unsigned inputSize(void);
	virtual unsigned outputSize(void);
	virtual unsigned weightSize(void);

	virtual void calculateOutputs(void);
	virtual void calculatePartials(void);
	virtual void setAsOutput(NetworkDataProxy* output)
	{
		this->addInput(output);
	}
	NoIndexNetworkDataProxy* getGeneratorNetworkInputProxy(void);
	virtual SingleIndexNetworkDataProxy* getNetworkInput(unsigned indexOfInput);
	virtual MultiIndexNetworkDataProxy* getNetworkInput(vector<unsigned> indexesOfInputs);
protected:
	NetworkStateVector& stateVector;
	SingleIndexNetworkDataProxy& constant(double value);
	virtual SingleIndexNetworkDataProxy& trainable(double value);
private:
	char lastClear;
	vector<int> networkInputIndexes;
	vector<int> networkOutputIndexes;

};
struct SpliningStdDev
{
	SpliningStdDev(double,double);
	SpliningStdDev(double);
	SpliningStdDev();
	double initial, final;
	double get(double alpha);
};

class DyNetWithNames: public Network
{
	vector<string> inputNames;
	vector<string> outputNames;
	vector<string> parameterNames;
	vector<string> stateNames;
	vector<SpliningStdDev> measurementError;
	vector<SpliningStdDev> parameterGlobal;
	vector<SpliningStdDev> parameterDamping;
	vector<SpliningStdDev> stateUpdate;
	vector<SpliningStdDev> stateDamping;

	unsigned states;
	unsigned parameters;
	unsigned inputs;
	unsigned rawOutputs;
	unsigned trueOutputs;
protected:
	virtual SingleIndexNetworkDataProxy& trainable(double value);
	SingleIndexNetworkDataProxy& trainable(double initialValue, double dampingStd, string name);
	SingleIndexNetworkDataProxy& trainable(double initialValue, SpliningStdDev dampingStd, string name);
	SingleIndexNetworkDataProxy* getNetworkInput(unsigned indexOfInput, string inputName);
	SingleIndexNetworkDataProxy* getNetworkState(unsigned indexOfState, double stateDampingStd, string stateName);
	SingleIndexNetworkDataProxy* getNetworkState(unsigned indexOfState, SpliningStdDev stateDampingStd, string stateName);
	void setAsStateUpdate(NetworkDataProxy* output, double stateUpdateErrorStd, string stateName);
	void setAsStateUpdate(NetworkDataProxy* output, SpliningStdDev stateUpdateErrorStd, string stateName);
	void setAsMeasurement(NetworkDataProxy* output, double measurementErrorStd, string outputName);
	void setAsMeasurement(NetworkDataProxy* output, SpliningStdDev measurementErrorStd, string outputName);
public:
	DyNetWithNames(NetworkStateVector& stateVector, unsigned states, unsigned inputs);
	virtual SingleIndexNetworkDataProxy* getNetworkInput(unsigned indexOfInput);
	virtual void setAsOutput(NetworkDataProxy* output);
	VectorXd getStdDevParamsDamping();
	VectorXd getStdDevMeasurementError();
	VectorXd getStdDevUpdateError();
	VectorXd getStdDevStateDamping();
	VectorXd getStdDevParamsDamping(double alpha);
	VectorXd getStdDevMeasurementError(double alpha);
	VectorXd getStdDevUpdateError(double alpha);
	VectorXd getStdDevStateDamping(double alpha);
	const vector<string> & viewParameterNames() const
	{
		return parameterNames;
	}
	const vector<string> & viewOutputNames() const
	{
		return outputNames;
	}
	const vector<string> & viewInputNames() const
	{
		return inputNames;
	}
	const vector<string> & viewStateNames() const
	{
		return stateNames;
	}

};

}
}

#endif /* NETWORK_HPP_ */
