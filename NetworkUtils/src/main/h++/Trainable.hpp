/*
 * Trainable.hpp
 *
 *    Created on: Nov 25, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef TRAINABLE_HPP_
#define TRAINABLE_HPP_
#include <vector>
using namespace std;
namespace uta
{
namespace networkUtils
{
class Trainable
{
public:
	virtual ~Trainable()
	{
	}
	virtual double getWeight(unsigned index)=0;
	virtual double getWeightPartial(unsigned index)=0;
	virtual double getInputPartial(unsigned index)=0;
	virtual double getOutput(unsigned index)=0;

	virtual void setWeight(unsigned index, double weight)=0;
	virtual void setInput(unsigned index, double input)=0;
	virtual void setOutputPartial(unsigned index, double outputPartial)=0;

	virtual vector<double> getWeights(void)=0;
	virtual vector<double> getWeightPartials(void)=0;
	virtual vector<double> getInputPartials(void)=0;
	virtual vector<double> getOutputs(void)=0;

	virtual unsigned inputSize(void)=0;
	virtual unsigned outputSize(void)=0;
	virtual unsigned weightSize(void)=0;

	virtual void calculateOutputs(void)=0;
	virtual void calculatePartials(void)=0;

	virtual void packWeights(vector<double>& weights)=0;
	virtual void packWeightPartials(vector<double>& weightPartials)=0;
	virtual void packInputPartials(vector<double>& inputPartials)=0;
	virtual void packOutputs(vector<double>& output)=0;

	virtual void setWeights(vector<double>& weights)=0;
	virtual void setInputs(vector<double>& inputs)=0;
	virtual void setOutputPartials(vector<double>& outputPartials)=0;

};

}
}

#endif /* TRAINABLE_HPP_ */
