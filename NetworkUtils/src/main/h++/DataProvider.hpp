/*
 * DataProvider.hpp
 *
 *  Created on: Jan 9, 2014
 *      Author: Gray
 */

#ifndef DATAPROVIDER_HPP_
#define DATAPROVIDER_HPP_
#include <vector>
using namespace std;
namespace uta
{
namespace networkUtils
{
class DataProvider
{
public:
	virtual ~DataProvider()
	{
	}

	virtual unsigned viewSize(void)=0;
	virtual unsigned viewInputSize(void)=0;
	virtual unsigned viewOutputSize(void)=0;
	virtual void packInputs(unsigned index, vector<double> &ins)=0;
	virtual void packOutputs(unsigned index, vector<double> &outs)=0;
	virtual double getOutput(unsigned dataIndex, unsigned outIndex)=0;
	virtual double getInput(unsigned dataIndex, unsigned inputIndex)=0;
};
}
}

#endif /* DATAPROVIDER_HPP_ */
