/*
 * DataSet.hpp
 *
 *    Created on: Nov 23, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef DATASET_HPP_
#define DATASET_HPP_

#include "DataProvider.hpp"
#include <vector>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;
namespace uta {
namespace networkUtils {
class DataSet : public DataProvider {
private:
	vector<vector<double> > inputs;
	vector<vector<double> > outputs;
	unsigned size;
	unsigned numberOfInputs;
	unsigned numberOfOutputs;
public:
	DataSet(string fileName);
	DataSet(unsigned inputs, unsigned outputs);
	DataSet();
	void addFile(string fileName);
	void addLine(string& line);
	void addLine(double ins[], double outs[]);
	void pushToFile(string fileName);
	unsigned viewSize(void) {
		return size;
	}
	unsigned viewInputSize(void) {
		return numberOfInputs;
	}
	unsigned viewOutputSize(void) {
		return numberOfOutputs;
	}
	void packInputs(unsigned index, vector<double> &ins) {
		for (unsigned i = 0; i < numberOfInputs; i++)
			ins[i] = inputs[i][index];
	}
	void packOutputs(unsigned index, vector<double> &outs) {
		for (unsigned i = 0; i < numberOfOutputs; i++)
			outs[i] = outputs[i][index];
	}
	double getOutput(unsigned dataIndex, unsigned outIndex) {
		return outputs[outIndex][dataIndex];
	}
	double getInput(unsigned dataIndex, unsigned inputIndex) {
		return inputs[inputIndex][dataIndex];
	}
	string print(void) {
		stringstream ss;
		ss << "DataSet" << endl;
		for (unsigned i = 0; i < numberOfInputs; i++) {
			ss << "\tinput[" << i << "] = [ ";
			for (unsigned k = 0; k < size; k++) {
				ss << inputs[i][k];
				if (k < size - 1)
					ss << ", ";
			}
			ss << "]" << endl;
		}
		for (unsigned o = 0; o < numberOfOutputs; o++) {
			ss << "\toutput[" << o << "] = [ ";
			for (unsigned k = 0; k < size; k++) {
				ss << outputs[o][k];
				if (k < size - 1)
					ss << ", ";
			}
			ss << "]" << endl;
		}
		return ss.str();
	}
};
}
}

#endif /* DATASET_HPP_ */
