/*
 * TrainableScalarElement.hpp
 *
 *    Created on: Nov 19, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef TRAINABLESCALARELEMENT_HPP_
#define TRAINABLESCALARELEMENT_HPP_

#include "ScalarElement.hpp"
#include "NetworkStateVector.hpp"
#include "NetworkDataProxy.hpp"
using namespace std;

namespace uta
{
namespace networkUtils
{

class TrainableScalarElement: public ScalarElement
{
private:
	int trainableIndex;
public:
	TrainableScalarElement(NetworkState& dataStore, NoIndexNetworkDataProxy* inputDataProxy, double initialValue);
	~TrainableScalarElement();

	virtual void forwardCalculate(void);
	virtual void reverseCalculate(void);
};
}
}


#endif /* TRAINABLESCALARELEMENT_HPP_ */
