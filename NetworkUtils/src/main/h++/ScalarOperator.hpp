/*
 * ScalarOperator.hpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SCALAROPERATOR_HPP_
#define SCALAROPERATOR_HPP_

#include <vector>
#include "ProcessableGraphNode.hpp"
#include "NetworkStateVector.hpp"
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "NetworkDataProxy.hpp"
#include <cmath>
using namespace std;

namespace uta
{
namespace networkUtils
{

class ScalarOperator: public AbstractMultiInputMultiOutputNetworkObject
{
public:
	ScalarOperator(NetworkState& dataStore, SingleIndexNetworkDataProxy* input);
	SingleIndexNetworkDataProxy* getScalarResult();
	void addInput(NetworkDataProxy* input);
protected:
	int index;
	SingleIndexNetworkDataProxy scalarOutput;
	NetworkState& dataStore;
};

}
}


#endif /* SCALAROPERATOR_HPP_ */
