/*
 * PerformanceTimer.hpp
 *
 *    Created on: Oct 6, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef PERFORMANCETIMER_HPP_
#define PERFORMANCETIMER_HPP_


#include <sys/time.h>

struct timezone;

using namespace std;
namespace uta{
namespace timingUtils{

class PerformanceTimer
{
public:
	double getTime(void) const
	{
		return time;
	}
	void startTimer(void)
	{
		gettimeofday(&start, NULL);
	}
	void endTimer(void)
	{
		gettimeofday(&end, NULL);
		secondDifference = end.tv_sec - start.tv_sec;
		microSecondDifference = end.tv_usec - start.tv_usec;
		time = secondDifference + microSecondDifference * (1e-6);
	}
	double getCurrentTime(void)
	{
		endTimer();
		return getTime();
	}
private:
	struct timeval start, end;
	long secondDifference, microSecondDifference;
	double time;
};
}
}

#endif /* PERFORMANCETIMER_HPP_ */
