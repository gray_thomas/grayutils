/*
 * TrainableScalarElement.cpp
 *
 *    Created on: Nov 19, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "../h++/TrainableScalarElement.hpp"
#include <vector>
#include "../h++/AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "../h++/NetworkStateVector.hpp"
#include "../h++/ScalarElement.hpp"

using namespace std;
namespace uta
{
namespace networkUtils
{

TrainableScalarElement::TrainableScalarElement(NetworkState& dataStore, NoIndexNetworkDataProxy* inputDataProxy,
												double initialValue) :
			ScalarElement(dataStore),
			trainableIndex(dataStore.allocateTrainableIndex())
{
	dataStore.setValue(trainableIndex, initialValue);
	AbstractMultiInputMultiOutputNetworkObject::addInput(inputDataProxy);
}
TrainableScalarElement::~TrainableScalarElement()
{
}
void TrainableScalarElement::forwardCalculate(void)
{
	dataStore.setValue(index, dataStore.getValue(trainableIndex));
}
void TrainableScalarElement::reverseCalculate(void)
{
	dataStore.setPartial(trainableIndex, dataStore.getPartial(index));
}

}
}

