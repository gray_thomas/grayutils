/*
 * DataSet.cpp
 *
 *    Created on: Nov 23, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "DataSet.hpp"

#include <exception>
#include <stdexcept>
#include <fstream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include <math.h>
#include <GUFiles.hpp>

using namespace std;
namespace uta
{
namespace networkUtils
{

DataSet::DataSet() :
		inputs(), outputs(), size(0), numberOfInputs(0), numberOfOutputs(0)
{
}
DataSet::DataSet(unsigned inputs, unsigned outputs) :
		inputs(), outputs(), size(0), numberOfInputs(inputs), numberOfOutputs(outputs)
{
	for (unsigned i = 0; i < numberOfInputs; i++)
	{
		this->inputs.push_back(vector<double>());
	}
	for (unsigned o = 0; o < numberOfOutputs; o++)
	{
		this->outputs.push_back(vector<double>());
	}
}
DataSet::DataSet(string fileName) :
		DataSet()
{
	addFile(fileName);
}

void DataSet::addFile(string fileName)
{
	ifstream file;

	file.open(gu::data_local(fileName).c_str());
	cout << "opening file : " << gu::data_local(fileName) << " to produce a DataSet Object" << endl;
	if (file.is_open())
	{
		cout << "file opened sucessfully" << endl;
		string line;
		getline(file, line);
		istringstream splitLine(line);
		unsigned inputSize, outputSize;
		string hashsign;
		splitLine >> hashsign;
		splitLine >> inputSize;
		splitLine >> outputSize;
		if (numberOfInputs != 0 && numberOfInputs != inputSize)
			throw exception();
		if (numberOfOutputs != 0 && numberOfOutputs != outputSize)
			throw exception();
		numberOfInputs = inputSize;
		numberOfOutputs = outputSize;
		for (unsigned i = 0; i < numberOfInputs; i++)
		{
			this->inputs.push_back(vector<double>());
		}
		for (unsigned o = 0; o < numberOfOutputs; o++)
		{
			this->outputs.push_back(vector<double>());
		}
		while (getline(file, line))
		{
			addLine(line);
		}
		file.close();
	}
	else
	{
		cerr << "file failed to open!" << endl;
		throw runtime_error("file failed to open");
	}
}
void DataSet::addLine(string& line)
{
	istringstream splitLine(line);
	double temp;
	for (unsigned i = 0; i < numberOfInputs; i++)
	{
		splitLine >> temp;
		inputs[i].push_back(temp);
	}
	for (unsigned o = 0; o < numberOfOutputs; o++)
	{
		splitLine >> temp;
		outputs[o].push_back(temp);
	}
	size++;
}
void DataSet::addLine(double ins[], double outs[])
{
	for (unsigned i = 0; i < numberOfInputs; i++)
	{
		inputs[i].push_back(ins[i]);
	}
	for (unsigned o = 0; o < numberOfOutputs; o++)
	{
		outputs[o].push_back(outs[o]);
	}
	size++;
}
ostream& pushCatchNaN(ostream& out, double doubleToPush)
{
	if (isfinite(doubleToPush))
		out << doubleToPush;
	else
		out << "nan";
	return out;
}
void DataSet::pushToFile(string fileName)
{
	ofstream file;
	cout << "pushing to file: " << gu::data_local(fileName) << endl;
	file.open(gu::data_local(fileName).c_str());
	file << "# " << numberOfInputs << " " << numberOfOutputs << endl;

	for (unsigned i = 0; i < size; i++)
	{
		for (unsigned q = 0; q < numberOfInputs; q++)
			pushCatchNaN(file,inputs[q][i])<<" ";
		for (unsigned p = 0; p < numberOfOutputs; p++)
		{
			pushCatchNaN(file,outputs[p][i]);
			if (p + 1 < numberOfOutputs)
				file << " ";
		}
		if (i + 1 < size)
			file << endl;
	}
	file.close();

}
}
}
