/*
 * ScalarConstant.cpp
 *
 *    Created on: Dec 11, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */


#include "../h++/ScalarConstant.hpp"
#include <vector>
#include "../h++/AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "../h++/NetworkStateVector.hpp"
#include "../h++/ScalarElement.hpp"

using namespace std;
namespace uta
{
namespace networkUtils
{

ScalarConstant::ScalarConstant(NetworkState& dataStore, NoIndexNetworkDataProxy* inputDataProxy,
												double initialValue) :
			ScalarElement(dataStore),
			value(initialValue)
{
	AbstractMultiInputMultiOutputNetworkObject::addInput(inputDataProxy);
}
ScalarConstant::~ScalarConstant()
{
}
void ScalarConstant::forwardCalculate(void)
{
	dataStore.setValue(index, value);
}
void ScalarConstant::reverseCalculate(void)
{
	// do nothing with my partial: dataStore.getPartial(index);
}

}
}

