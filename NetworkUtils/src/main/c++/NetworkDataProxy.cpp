/*
 * NetworkDataProxy.cpp
 *
 *    Created on: Dec 11, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "NetworkDataProxy.hpp"
#include <vector>
#include "NetworkStateVector.hpp"
using namespace std;
namespace uta
{
namespace networkUtils
{

NetworkDataProxy::NetworkDataProxy(AbstractMultiInputMultiOutputNetworkObject* object) :
			object(object),
			doConnectForward(true),
			doConnectReverse(true)
{
}
NetworkState& NetworkDataProxy::getNetworkState()
{
	return object->getDataStore();
}
NetworkDataProxy::~NetworkDataProxy(void)
{
}
AbstractMultiInputMultiOutputNetworkObject* NetworkDataProxy::getObject()
{
	return object;
}

bool NetworkDataProxy::connectForward(void)
{
	return this->doConnectForward;
}
bool NetworkDataProxy::connectReverse(void)
{
	return this->doConnectReverse;
}
void NetworkDataProxy::setConnectForward(bool connect)
{
	this->doConnectForward = connect;
}
void NetworkDataProxy::setConnectReverse(bool connect)
{
	this->doConnectReverse = connect;
}

MultiIndexNetworkDataProxy::MultiIndexNetworkDataProxy(AbstractMultiInputMultiOutputNetworkObject* object) :
			NetworkDataProxy(object),
			indexes()
{
}
unsigned MultiIndexNetworkDataProxy::getNumberOfIndexes(void)
{
	return indexes.size();
}
void MultiIndexNetworkDataProxy::addIndex(int index)
{
	indexes.push_back(index);
}
int MultiIndexNetworkDataProxy::getIndex(unsigned i)
{
	return indexes[i];
}

SingleIndexNetworkDataProxy::SingleIndexNetworkDataProxy(AbstractMultiInputMultiOutputNetworkObject* object,
															int index) :
			NetworkDataProxy(object),
			index(index)
{
}
unsigned SingleIndexNetworkDataProxy::getNumberOfIndexes(void)
{
	return 1;
}
int SingleIndexNetworkDataProxy::getIndex(unsigned i)
{
	return index;
}
int SingleIndexNetworkDataProxy::getIndex(void)
{
	return index;
}

NoIndexNetworkDataProxy::NoIndexNetworkDataProxy(AbstractMultiInputMultiOutputNetworkObject* object) :
			NetworkDataProxy(object)
{
}
unsigned NoIndexNetworkDataProxy::getNumberOfIndexes(void)
{
	return 0;
}
int NoIndexNetworkDataProxy::getIndex(unsigned i)
{
	return -1;
}

}
}
