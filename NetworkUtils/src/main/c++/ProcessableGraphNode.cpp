/*
 * ProcessableGraphNode.cpp
 *
 *    Created on: Nov 9, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "ProcessableGraphNode.hpp"
#include "iostream"
using namespace std;
namespace uta
{
namespace networkUtils
{
ProcessableGraphNode::ProcessableGraphNode(void* context, void (*execute)(void*), void* (*getOutput)(void*, unsigned),
               							void (*registerInput)(void*, void*,unsigned)) :
			inputsRegistered(0),
			numberOfInputs(0),
			lastClearNumber(0),
			outputs(),
			context(context),
			executeFunctionPointer(execute),
			ptrToGetOutput(getOutput),
			ptrToAddInput(registerInput)
{

}
void ProcessableGraphNode::recursiveClear(char clearNumber)
{
	if (clearNumber != lastClearNumber)
	{
		inputsRegistered = 0;
		lastClearNumber = clearNumber;
		for (unsigned i = 0; i < outputs.size(); i++)
		{
			outputs[i]->recursiveClear(clearNumber);
		}
	}
}
void ProcessableGraphNode::recursiveExecute(void)
{
	if (inputsRegistered == numberOfInputs)
	{
		executeFunctionPointer(context);
		for (unsigned i = 0; i < outputs.size(); i++)
		{
			outputs[i]->inputsRegistered++;
			outputs[i]->recursiveExecute();
		}
	}
}
void ProcessableGraphNode::recursiveSetup(void* ptrToInputObject)
{
	ptrToAddInput(context, ptrToInputObject, inputsRegistered);
	if (inputsRegistered == numberOfInputs)
	{
		for (unsigned i = 0; i < outputs.size(); i++)
		{
			void* ptrToOutputObject = ptrToGetOutput(context, i);
			outputs[i]->inputsRegistered++;
			outputs[i]->recursiveSetup(ptrToOutputObject);
		}
	}
}

void ProcessableGraphNode::addOutput(ProcessableGraphNode* newOutput)
{
	outputs.push_back(newOutput);
	newOutput->numberOfInputs++;
}

static void exampleExecutable(void* input)
{
	int numToPrint = *(int*) input;
	cout << "number " << numToPrint << " reporting in" << endl;
}
static void* getOutput(void* input, unsigned numberOfOutput)
{
	int numToPrint = *(int*) input;
	cout << "number " << numToPrint << " reporting in" << " as input number " << numberOfOutput
			<< endl;
	return input;
}
static void addInput(void* input, void* input2, unsigned numberOfInput)
{
	int myNum = *(int*) input;
	int otherNum = *(int*) input2;
	cout << "number " << myNum << " registering input from " << otherNum << " as input number " << numberOfInput
			<< endl;
}
void testProcessableGraphNode(void)
{
	cout << "hello testProcessableGraphNode" << endl;
	cout << endl;
	ProcessableGraphNode node1(new int(1), exampleExecutable, getOutput, addInput);
	ProcessableGraphNode node2(new int(2), exampleExecutable, getOutput, addInput);
	ProcessableGraphNode node3(new int(3), exampleExecutable, getOutput, addInput);
	ProcessableGraphNode node4(new int(4), exampleExecutable, getOutput, addInput);
	node1.addOutput(&node2);
	node3.addOutput(&node2);
	node2.addOutput(&node4);

	node1.recursiveClear(22);
	node3.recursiveClear(22);
	cout << "node1.recursiveExecute():" << endl;
	node1.recursiveExecute();
	cout << endl << "node3.recursiveExecute():" << endl;
	node3.recursiveExecute();

	cout << endl << "clearing nodes" << endl;

	node1.recursiveClear(23);
	node3.recursiveClear(23);
	cout << endl << "node3.recursiveExecute():" << endl;
	node3.recursiveExecute();
	cout << endl << "node1.recursiveExecute():" << endl;
	node1.recursiveExecute();

	cout << endl << "clearing nodes" << endl;

	node1.recursiveClear(23);
//	node3.recursiveClear(23);
	cout << endl << "node3.recursiveExecute():" << endl;
	node3.recursiveExecute();
	cout << endl << "node1.recursiveExecute():" << endl;
	node1.recursiveExecute();

	node1.recursiveClear(24);
	node3.recursiveClear(24);
	cout << endl << "node3.recursiveSetup():" << endl;
	node3.recursiveSetup(new int(28));
	cout << endl << "node1.recursiveExecute():" << endl;
	node1.recursiveSetup(new int(1337));
	// Node 4 should receive a message: 1337
	// execution should terminate early on the first recursive setup since not all inputs were accounted for.
	// I need to get hooked up with a real testing library.
}
}
}

