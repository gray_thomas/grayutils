/*
 * ScalarElement.cpp
 *
 *    Created on: Nov 19, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "../h++/ScalarElement.hpp"
#include <vector>
#include "../h++/AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "../h++/NetworkStateVector.hpp"
#include "NetworkDataProxy.hpp"
#include <iostream>
using namespace std;
namespace uta
{
namespace networkUtils
{
ScalarElement::ScalarElement(NetworkState& dataStore) :
			AbstractMultiInputMultiOutputNetworkObject(dataStore),
			scalarOutput(this, dataStore.allocateNonTrainableIndex()),
			dataStore(dataStore)
{
	index = scalarOutput.getIndex();
}
NetworkState& ScalarElement::getDataStore()
{
	return dataStore;
}
SingleIndexNetworkDataProxy* ScalarElement::getScalarResult()
{
	return &scalarOutput;
}


}
}

