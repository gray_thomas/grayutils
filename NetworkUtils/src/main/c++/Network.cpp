/*
 * Network.cpp
 *
 *    Created on: Nov 19, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include "Network.hpp"
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "ScalarConstant.hpp"
#include "TrainableScalarElement.hpp"
#include <stdexcept>
using namespace std;
namespace uta
{
namespace networkUtils
{

Network::Network(NetworkStateVector& stateVector, unsigned inputs) :
		AbstractMultiInputMultiOutputNetworkObject(stateVector), stateVector(stateVector), lastClear(0),
				networkInputIndexes(), networkOutputIndexes()
{
	for (unsigned i = 0; i < inputs; i++)
	{
		networkInputIndexes.push_back(stateVector.allocateNonTrainableIndex());
	}
}

void Network::forwardCalculate(void)
{

}
void Network::reverseCalculate(void)
{

}
void Network::addInput(NetworkDataProxy* input)
{
	this->addInputReverseOnly(input);
	for (unsigned i = 0; i < input->getNumberOfIndexes(); i++)
	{
		networkOutputIndexes.push_back(input->getIndex(i));
		stateVector.setPartialAsInput(input->getIndex(i));
	}
}

void Network::clear(void)
{
	stateVector.resetNetwork();
	lastClear = (lastClear + 1) % 65;
	this->recursiveClear(lastClear);
}
void Network::executeForwardNetwork(void)
{
	lastClear = (lastClear + 1) % 65;
	recursiveClear(lastClear);
	forwardRecursiveExecute();
}
void Network::executeGradientNetwork(void)
{
	stateVector.resetGradients();
	lastClear = (lastClear + 1) % 65;
	recursiveClear(lastClear);
	reverseRecursiveExecute();
}

NoIndexNetworkDataProxy* Network::getGeneratorNetworkInputProxy(void)
{
	NoIndexNetworkDataProxy* networkInput = new NoIndexNetworkDataProxy(this);
	networkInput->setConnectReverse(false);
	return networkInput;
}

SingleIndexNetworkDataProxy* Network::getNetworkInput(unsigned indexOfInput)
{
	if (indexOfInput >= this->inputSize())
		throw new runtime_error("input does not exist, cannot create data proxy");
	SingleIndexNetworkDataProxy* networkInput = new SingleIndexNetworkDataProxy(this, indexOfInput);
	networkInput->setConnectReverse(false);
	return networkInput;
}
MultiIndexNetworkDataProxy* Network::getNetworkInput(vector<unsigned> indexOfInput)
{
	MultiIndexNetworkDataProxy* networkInput = new MultiIndexNetworkDataProxy(this);
	networkInput->setConnectReverse(false);
	for (unsigned i = 0; i < indexOfInput.size(); i++)
		networkInput->addIndex(networkInputIndexes[indexOfInput[i]]);
	return networkInput;
}

vector<double> Network::getWeights(void)
{
	return stateVector.getTrainableData();
}
vector<double> Network::getWeightPartials(void)
{
	return stateVector.getPartialsWithRespectToTrainableData();
}
vector<double> Network::getInputPartials(void)
{
	vector<double> inputPartials;
	packInputPartials(inputPartials);
	return inputPartials;
}
vector<double> Network::getOutputs(void)
{
	vector<double> outputs;
	packOutputs(outputs);
	return outputs;
}

double Network::getWeight(unsigned index)
{
	return stateVector.getValue(stateVector.viewTrainableIndexes()[index]);
}
double Network::getWeightPartial(unsigned index)
{
	return stateVector.getPartial(stateVector.viewTrainableIndexes()[index]);
}
double Network::getInputPartial(unsigned index)
{
	return stateVector.getPartial(networkInputIndexes[index]);
}
double Network::getOutput(unsigned index)
{
	if (index >= networkOutputIndexes.size())
		throw runtime_error("getOutput Index Out of bounds");
	return stateVector.getValue(networkOutputIndexes[index]);
}

void Network::packWeights(vector<double>& weights)
{
	stateVector.packTrainableData(weights);
}
void Network::packWeightPartials(vector<double>& weightPartials)
{
	stateVector.packPartialsWithRespectToTrainableData(weightPartials);
}
void Network::packInputPartials(vector<double>& inputPartials)
{
	if (inputPartials.size() != networkInputIndexes.size())
		inputPartials.resize(networkInputIndexes.size());
	for (unsigned i = 0; i < networkInputIndexes.size(); i++)
		inputPartials[i] = (stateVector.getPartial(networkInputIndexes[i]));
}
void Network::packOutputs(vector<double>& outputs)
{
	if (outputs.size() != networkOutputIndexes.size())
		outputs.resize(networkOutputIndexes.size());
	for (unsigned i = 0; i < networkOutputIndexes.size(); i++)
		outputs[i] = (getOutput(i));
}

void Network::setWeights(vector<double>& weights)
{
	stateVector.setTrainableData(weights);
}
void Network::setInputs(vector<double>& inputValues)
{
	for (unsigned i = 0; i < this->networkInputIndexes.size(); i++)
		stateVector.setValue(networkInputIndexes[i], inputValues[i]);
}
void Network::setOutputPartials(vector<double>& outputPartials)
{
	for (unsigned o = 0; o < this->networkOutputIndexes.size(); o++)
		stateVector.setPartial(networkOutputIndexes[o], outputPartials[o]);
}

void Network::setWeight(unsigned index, double weight)
{
	stateVector.setValue(stateVector.viewTrainableIndexes()[index], weight);
}
void Network::setInput(unsigned index, double inputValue)
{
	stateVector.setValue(networkInputIndexes[index], inputValue);
}
void Network::setOutputPartial(unsigned index, double outputPartial)
{
	stateVector.setPartial(networkOutputIndexes[index], outputPartial);
}

unsigned Network::inputSize(void)
{
	return this->networkInputIndexes.size();
}
unsigned Network::outputSize(void)
{
	return this->networkOutputIndexes.size();
}
unsigned Network::weightSize(void)
{
	return stateVector.getNumberOfTrainableValues();
}

void Network::calculateOutputs(void)
{
	executeForwardNetwork();
}
void Network::calculatePartials(void)
{
	executeGradientNetwork();
}

SingleIndexNetworkDataProxy& Network::constant(double value)
{
	return *(new ScalarConstant(this->dataStore, getGeneratorNetworkInputProxy(), value))->getScalarResult();
}
SingleIndexNetworkDataProxy& Network::trainable(double value)
{
	return *(new TrainableScalarElement(this->dataStore, getGeneratorNetworkInputProxy(), value))->getScalarResult();
}
DyNetWithNames::DyNetWithNames(NetworkStateVector& stateVector, unsigned states, unsigned inputs) :
		Network(stateVector, inputs + states), inputNames(), outputNames(), parameterNames(), stateNames(),
				measurementError(), parameterGlobal(), parameterDamping(), stateUpdate(states), stateDamping(states),
				states(states), parameters(0), inputs(inputs), rawOutputs(0), trueOutputs(0)
{
	for (unsigned i = 0; i < inputs; i++)
		inputNames.push_back(string(""));
	for (unsigned i = 0; i < states; i++)
		stateNames.push_back(string(""));
}
SingleIndexNetworkDataProxy& DyNetWithNames::trainable(double value)
{
	stringstream ss;
	ss << "parameter " << parameters;
	return trainable(value, 1.0, ss.str());
}
SingleIndexNetworkDataProxy& DyNetWithNames::trainable(double initialValue, double dampingStd, string name)
{
	return trainable(initialValue, SpliningStdDev(dampingStd),name);
}
SingleIndexNetworkDataProxy& DyNetWithNames::trainable(double initialValue, SpliningStdDev dampingStd, string name)
{
	this->parameterDamping.push_back(dampingStd);
	this->parameterGlobal.push_back(SpliningStdDev(1e99));
	this->parameterNames.push_back(name);
	parameters++;
	return Network::trainable(initialValue);
}
SingleIndexNetworkDataProxy* DyNetWithNames::getNetworkInput(unsigned indexOfInput)
{
	if (indexOfInput < states)
	{
		stringstream ss;
		ss << "state " << indexOfInput;
		return getNetworkState(indexOfInput, 1.0, ss.str());
	}
	else
	{
		int index = indexOfInput - states;
		stringstream ss;
		ss << "input " << index;
		return getNetworkInput(index, ss.str());
	}
}
SingleIndexNetworkDataProxy* DyNetWithNames::getNetworkInput(unsigned indexOfInput, string inputName)
{
	if (indexOfInput >= inputs)
		throw runtime_error("DyNetWithNames::getNetworkInput: Error: input out of range.");
	this->inputNames[indexOfInput].assign(inputName);
	return Network::getNetworkInput(indexOfInput + states);
}
SingleIndexNetworkDataProxy* DyNetWithNames::getNetworkState(unsigned indexOfState, double stateDampingStd,
		string stateName)
{
	return getNetworkState(indexOfState,SpliningStdDev(stateDampingStd),stateName);
}
SingleIndexNetworkDataProxy* DyNetWithNames::getNetworkState(unsigned indexOfState, SpliningStdDev stateDampingStd,
		string stateName)
{
	this->stateDamping[indexOfState] = stateDampingStd;
	this->stateNames[indexOfState].assign(stateName);
	return Network::getNetworkInput(indexOfState);
}
void DyNetWithNames::setAsStateUpdate(NetworkDataProxy* output, double stateUpdateErrorStd, string stateName)
{
	setAsStateUpdate(output,SpliningStdDev(stateUpdateErrorStd),stateName);
}
void DyNetWithNames::setAsStateUpdate(NetworkDataProxy* output, SpliningStdDev stateUpdateErrorStd, string stateName)
{
	if (rawOutputs >= states)
		throw runtime_error("all state updates already set");
	if (0 != stateName.compare(stateNames[rawOutputs]))
	{
		cerr << "Error: output state '" << stateName << "' miscompared to '" << stateNames[rawOutputs] << "'" << endl;
		throw runtime_error("state comparison is mismatched in network constructor.");
	}
	this->stateUpdate[rawOutputs] = stateUpdateErrorStd;
	Network::setAsOutput(output);
	rawOutputs++;
} // check for string equality!
void DyNetWithNames::setAsMeasurement(NetworkDataProxy* output, double measurementErrorStd, string outputName)
{
	setAsMeasurement(output,SpliningStdDev(measurementErrorStd),outputName);
}
void DyNetWithNames::setAsMeasurement(NetworkDataProxy* output, SpliningStdDev measurementErrorStd, string outputName)
{
	if (rawOutputs < states)
	{
		cerr << "Error: output number" << rawOutputs << " should be " << stateNames[rawOutputs] << endl;
		throw runtime_error("output defined before all states were compared in network constructor.");
	}
	this->measurementError.push_back(measurementErrorStd);
	this->outputNames.push_back(outputName);
	Network::setAsOutput(output);
	rawOutputs++;
	trueOutputs = rawOutputs - states;
}
void DyNetWithNames::setAsOutput(NetworkDataProxy* output)
{
	if (rawOutputs < states)
	{
		stringstream ss;
		ss << "state " << rawOutputs;
		setAsStateUpdate(output, 1.0, ss.str());
	}
	else
	{
		stringstream ss;
		ss << "measurement " << trueOutputs;
		setAsMeasurement(output, 1.0, ss.str());
	}
}
VectorXd DyNetWithNames::getStdDevParamsDamping()
{
	VectorXd ret(this->parameters);
	for (unsigned i = 0; i < parameters; i++)
		ret(i) = this->parameterDamping[i].initial;
	return ret;
}
VectorXd DyNetWithNames::getStdDevMeasurementError()
{
	VectorXd ret(this->trueOutputs);
	for (unsigned i = 0; i < trueOutputs; i++)
		ret(i) = this->measurementError[i].initial;
	return ret;
}
VectorXd DyNetWithNames::getStdDevUpdateError()
{
	VectorXd ret(this->states);
	for (unsigned i = 0; i < states; i++)
		ret(i) = this->stateUpdate[i].initial;
	return ret;
}
VectorXd DyNetWithNames::getStdDevStateDamping()
{
	VectorXd ret(this->states);
	for (unsigned i = 0; i < states; i++)
		ret(i) = this->stateDamping[i].initial;
	return ret;
}
VectorXd DyNetWithNames::getStdDevParamsDamping(double alpha)
{
	VectorXd ret(this->parameters);
	for (unsigned i = 0; i < parameters; i++)
		ret(i) = this->parameterDamping[i].get(alpha);
	return ret;
}
VectorXd DyNetWithNames::getStdDevMeasurementError(double alpha)
{
	VectorXd ret(this->trueOutputs);
	for (unsigned i = 0; i < trueOutputs; i++)
		ret(i) = this->measurementError[i].get(alpha);
	return ret;
}
VectorXd DyNetWithNames::getStdDevUpdateError(double alpha)
{
	VectorXd ret(this->states);
	for (unsigned i = 0; i < states; i++)
		ret(i) = this->stateUpdate[i].get(alpha);
	return ret;
}
VectorXd DyNetWithNames::getStdDevStateDamping(double alpha)
{
	VectorXd ret(this->states);
	for (unsigned i = 0; i < states; i++)
		ret(i) = this->stateDamping[i].get(alpha);
	return ret;
}
SpliningStdDev::SpliningStdDev(double init, double fin) :
		initial(init), final(fin)
{
}
SpliningStdDev::SpliningStdDev(double initAndFin) :
		initial(initAndFin), final(initAndFin)
{
}
SpliningStdDev::SpliningStdDev() :
		initial(1), final(1)
{
}
double SpliningStdDev::get(double alpha)
{
	if (alpha>1)
		alpha=1.0;
	if (alpha<0)
		alpha=0.0;

	double current = initial * pow(final / initial, alpha);
	if (current>1e6)
		current=1e6;
	if (current<1e-6)
		current=1e-6;
	// saturation avoids high condition matricies
	return current;
}

}
}

