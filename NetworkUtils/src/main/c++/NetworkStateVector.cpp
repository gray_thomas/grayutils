/*
 * NetworkStateVector.cpp
 *
 *    Created on: Nov 8, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "NetworkStateVector.hpp"
#include "vector"

namespace uta
{
namespace networkUtils
{
using namespace std;

int NetworkStateVector::allocateTrainableIndex(void)
{
	int index = values.size();
	values.push_back(0.0);
	partials.push_back(0.0);
	wipeThisPartial.push_back(true);
	trainableIndexes.push_back(index);
	return index;
}

int NetworkStateVector::allocateNonTrainableIndex(void)
{
	int index = values.size();
	values.push_back(0.0);
	partials.push_back(0.0);
	wipeThisPartial.push_back(true);
	tempIndexes.push_back(index);
	return index;
}

int NetworkStateVector::allocateConstantIndex(void)
{
	int index = values.size();
	values.push_back(0.0);
	partials.push_back(0.0);
	wipeThisPartial.push_back(true);
	return index;
}

void NetworkStateVector::setValue(int index, double value)
{
	values[index] = value;
}

double NetworkStateVector::getValue(int index)
{
	return values[index];
}

void NetworkStateVector::setPartial(int index, double partial)
{
	partials[index] = partial;
}

double NetworkStateVector::getPartial(int index)
{
	return partials[index];
}

void NetworkStateVector::addToPartial(int index, double partial)
{
	partials[index] += partial;
}
void NetworkStateVector::addToValue(int index, double value)
{
	values[index] += value;
}

vector<double> NetworkStateVector::getPartialsWithRespectToTrainableData(void)
{
	vector<double> partialsToReturn;
	packPartialsWithRespectToTrainableData(partialsToReturn);
	return partialsToReturn;
}

void NetworkStateVector::packPartialsWithRespectToTrainableData(vector<double>& partialsWithRespectToTrainableData)
{
	if (partialsWithRespectToTrainableData.size()!=trainableIndexes.size())
		partialsWithRespectToTrainableData.resize(trainableIndexes.size());
	for (unsigned i = 0; i < trainableIndexes.size(); i++)
	{
		partialsWithRespectToTrainableData[i]=(partials[trainableIndexes[i]]);
	}
}

vector<double> NetworkStateVector::getTrainableData(void)
{
	vector<double> valuesToReturn;
	packTrainableData(valuesToReturn);
	return valuesToReturn;
}

void NetworkStateVector::packTrainableData(vector<double>& trainableData)
{
	if (trainableData.size()!=trainableIndexes.size())
		trainableData.resize(trainableIndexes.size());
	for (unsigned i = 0; i < trainableIndexes.size(); i++)
	{
		trainableData[i]=(values[trainableIndexes[i]]);
	}
}

void NetworkStateVector::resetGradients(void)
{
	for (unsigned i = 0; i < partials.size(); i++)
		if (wipeThisPartial[i])
			partials[i] = 0.0;
}
void NetworkStateVector::resetNetwork(void)
{
	for (unsigned i = 0; i < tempIndexes.size(); i++)
		values[tempIndexes[i]] = 0.0;
	for (unsigned i = 0; i < partials.size(); i++)
		if (wipeThisPartial[i])
			partials[i] = 0.0;
}

NetworkStateVector::~NetworkStateVector()
{
	for (unsigned i = 0; i < objectsToDestroy.size(); i++)
		delete objectsToDestroy[i];
}

void NetworkStateVector::registerDestructable(AbstractMultiInputMultiOutputNetworkObject*networkObject)
{
	objectsToDestroy.push_back(networkObject);
}

void NetworkStateVector::setTrainableData(vector<double> allocator)
{
	for (unsigned i = 0; i < allocator.size(); i++)
		values[trainableIndexes[i]] = allocator[i];
}
void NetworkStateVector::setPartialAsInput(int index)
{
	wipeThisPartial[index]=false;
}

void NetworkStateVector::print(void)
{
	if (values.size() == 0)
	{
		cout<<"network state vecotr is empty"<<endl;
		return;
	}
	cout << "network state vector is as follows:\n\tvalues [";
	for (unsigned i = 0; i < values.size() - 1; i++)
	{
		cout << values[i] << ", ";
	}
	cout << values[values.size() - 1] << "]" << endl;
	cout << "\tpartials [";
	for (unsigned i = 0; i < partials.size() - 1; i++)
	{
		cout << partials[i] << ", ";
	}
	cout << partials[partials.size() - 1] << "]" << endl;
}

}
}

