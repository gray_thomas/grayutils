/*
 * ScalarOperator.cpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */


#include "ScalarOperator.hpp"
#include <vector>
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "NetworkStateVector.hpp"
#include "NetworkDataProxy.hpp"
#include <iostream>
using namespace std;
#define UNINITIALIZED_INDEX -1

namespace uta
{
namespace networkUtils
{
ScalarOperator::ScalarOperator(NetworkState& dataStore, SingleIndexNetworkDataProxy* input) :
			AbstractMultiInputMultiOutputNetworkObject(dataStore),
			index(UNINITIALIZED_INDEX),
			scalarOutput(this,dataStore.allocateNonTrainableIndex()),
			dataStore(dataStore)
{
	addInput(input);
	index=scalarOutput.getIndex();
}
SingleIndexNetworkDataProxy* ScalarOperator::getScalarResult()
{
	return &scalarOutput;
}
void ScalarOperator::addInput(NetworkDataProxy* input)
{
	if (index==UNINITIALIZED_INDEX)
		AbstractMultiInputMultiOutputNetworkObject::addInput(input);
	else
		cerr<<"ignoring multiple inputs to a scalar operator."<<endl;
}

}
}


