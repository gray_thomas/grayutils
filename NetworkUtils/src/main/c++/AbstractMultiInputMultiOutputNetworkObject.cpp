/*
 * AbstractMultiInputMultiOutputNetworkObject.cpp
 *
 *    Created on: Nov 18, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include <vector>
using namespace std;
namespace uta
{
namespace networkUtils
{
AbstractMultiInputMultiOutputNetworkObject::AbstractMultiInputMultiOutputNetworkObject(NetworkState& dataStore) :
			forwardNumberOfInputs(0),
			forwardInputsRegistered(0),
			reverseInputsRegistered(0),
			reverseNumberOfInputs(0),
			lastClearNumber(0),
			forwardOutputs(),
			reverseOutputs(),
			dataStore(dataStore)
{
	dataStore.registerDestructable(this);
}
AbstractMultiInputMultiOutputNetworkObject::~AbstractMultiInputMultiOutputNetworkObject()
{
}
void AbstractMultiInputMultiOutputNetworkObject::recursiveClear(char clearNumber)
{
	if (clearNumber != lastClearNumber)
	{
		forwardInputsRegistered = 0;
		reverseInputsRegistered = 0;
		lastClearNumber = clearNumber;
		for (unsigned i = 0; i < forwardOutputs.size(); i++)
		{
			forwardOutputs[i]->recursiveClear(clearNumber);
		}
	}
}
void AbstractMultiInputMultiOutputNetworkObject::forwardRecursiveExecute(void)
{
	if (forwardInputsRegistered == forwardNumberOfInputs)
	{
		forwardCalculate();
		for (unsigned i = 0; i < forwardOutputs.size(); i++)
		{
			forwardOutputs[i]->forwardInputsRegistered++;
			forwardOutputs[i]->forwardRecursiveExecute();
		}
	}
}
void AbstractMultiInputMultiOutputNetworkObject::reverseRecursiveExecute(void)
{
	if (reverseInputsRegistered == reverseNumberOfInputs)
	{
		reverseCalculate();
		for (unsigned i = 0; i < reverseOutputs.size(); i++)
		{
			reverseOutputs[i]->reverseInputsRegistered++;
			reverseOutputs[i]->reverseRecursiveExecute();
		}
	}
}

void AbstractMultiInputMultiOutputNetworkObject::connectForwardExecutionGraph(
		AbstractMultiInputMultiOutputNetworkObject* input, AbstractMultiInputMultiOutputNetworkObject* output)
{
	output->forwardNumberOfInputs++;
	input->forwardOutputs.push_back(output);
}
void AbstractMultiInputMultiOutputNetworkObject::connectReverseExecutionGraph(
		AbstractMultiInputMultiOutputNetworkObject* input, AbstractMultiInputMultiOutputNetworkObject* output)
{
	input->reverseNumberOfInputs++;
	output->reverseOutputs.push_back(input);
}

void AbstractMultiInputMultiOutputNetworkObject::addInputForwardOnly(NetworkDataProxy* input)
{
	connectForwardExecutionGraph(input->getObject(), this);
	addInputIndexes(input);
}
void AbstractMultiInputMultiOutputNetworkObject::addInputReverseOnly(NetworkDataProxy* input)
{
	connectReverseExecutionGraph(input->getObject(), this);
}
void AbstractMultiInputMultiOutputNetworkObject::addInput(NetworkDataProxy* input)
{
	if (input->connectForward())
		this->addInputForwardOnly(input);
	if (input->connectReverse())
		this->addInputReverseOnly(input);
}
void AbstractMultiInputMultiOutputNetworkObject::addInputIndexes(NetworkDataProxy* networkProxy)
{
	for (unsigned i = 0; i < networkProxy->getNumberOfIndexes(); i++)
		forwardInputIndexes.push_back(networkProxy->getIndex(i));
}

void AbstractMultiInputMultiOutputNetworkObject::addInputIndexVector(vector<int> inputIndexes)
{
	for (unsigned i = 0; i < inputIndexes.size(); i++)
		forwardInputIndexes.push_back(inputIndexes[i]);
}
void AbstractMultiInputMultiOutputNetworkObject::addInputIndex(int inputIndex)
{
	forwardInputIndexes.push_back(inputIndex);
}

ostream& operator<<(ostream& x, vector<int> indexes)
{
	if (indexes.size() == 0)
	{
		return x << "[]";
	}
	x << "[ ";
	for (unsigned i = 0; i < indexes.size() - 1; i++)
		x << indexes[i] << ", ";
	x << indexes[indexes.size() - 1] << "]";
	return x;
}
ostream& operator<<(ostream& x, vector<AbstractMultiInputMultiOutputNetworkObject*> objects)
{
	if (objects.size() == 0)
	{
		return x << "[]";
	}
	x << "[ ";
	for (unsigned i = 0; i < objects.size() - 1; i++)
		x << (long) objects[i] << ", ";
	x << (long) objects[objects.size() - 1] << "]";
	return x;
}

void AbstractMultiInputMultiOutputNetworkObject::print(void)
{
	cout << "AbstractMultiInputMultiOutputNetworkObject:" << endl;
	cout << "\t" << "with forwardNumberOfInputs " << forwardNumberOfInputs << endl;
	cout << "\t" << "with reverseNumberOfInputs " << reverseNumberOfInputs << endl;
	cout << "\t" << "with lastClearNumber " << (int) lastClearNumber << endl;
	cout << "\t" << "with forwardInputsRegistered " << forwardInputsRegistered << endl;
	cout << "\t" << "with forwardInputIndexes.size() " << forwardInputIndexes.size() << endl;
	cout << "\t" << "with forwardInputIndexes " << forwardInputIndexes << endl;
	cout << "\t" << "with this " << (long) this << endl;
	cout << "\t" << "with forwardOutputs " << forwardOutputs << endl;
	cout << "\t" << "with reverseOutputs " << reverseOutputs << endl;
}

}
}
