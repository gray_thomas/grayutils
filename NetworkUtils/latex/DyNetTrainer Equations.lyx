#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass elsarticle
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine natbib_authoryear
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 0.3in
\topmargin 0.4in
\rightmargin 0.3in
\bottommargin 0.3in
\headheight 0.2in
\headsep 0.2in
\footskip 0.2in
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Comparison of Kalman Filter and DyNetTrainer Algorithms
\end_layout

\begin_layout Author
Gray Thomas
\end_layout

\begin_layout Section
Notation
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbf{x}_{k}=\mathbf{F}_{k}\mathbf{x}_{k-1}+\mathbf{B}_{k}\mathbf{u}_{k-1}+\mathbf{w}_{k}|\;\;\mathbf{w}_{k}\sim N_{(0,\mathbf{Q}_{k})}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mathbf{z}_{k}=\mathbf{H}_{k}\mathbf{x}_{k}+\mathbf{v}_{k}|\;\;\mathbf{v}_{k}\sim N_{(0,\mathbf{R}_{k})}
\]

\end_inset


\end_layout

\begin_layout Section
UnDamped DyNetTrainer Algorithm
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
Rx=z\;\;\rightarrow\;\;\left(\begin{array}{ccc}
\mathbf{H}_{0}\\
\mathbf{F}_{1} & -I\\
 & \mathbf{H}_{1}\\
 & \mathbf{F}_{2} & -I\\
 &  & \mathbf{H}_{2}
\end{array}\right)\left(\begin{array}{c}
\mathbf{x}_{0}\\
\mathbf{x}_{1}\\
\mathbf{x}_{2}
\end{array}\right)=\left(\begin{array}{c}
\mathbf{z}_{0}\\
\mathbf{B}_{1}\mathbf{u}_{0}\\
\mathbf{z}_{1}\\
\mathbf{B}_{2}\mathbf{u}_{1}\\
\mathbf{z}_{2}
\end{array}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
R^{T}\Sigma R=\left(\begin{array}{ccccc}
\mathbf{H}_{0}^{\intercal} & \mathbf{F}_{1}^{\intercal}\\
 & -\mathbf{I} & \mathbf{H}_{1}^{\intercal} & \mathbf{F}_{2}^{\intercal}\\
 &  &  & -\mathbf{I} & \mathbf{H}_{2}^{\intercal}
\end{array}\right)\left(\begin{array}{ccccc}
\mathbf{R}_{0}^{-1}\\
 & \mathbf{Q}_{1}^{-1}\\
 &  & \mathbf{R}_{1}^{-1}\\
 &  &  & \mathbf{Q}_{2}^{-1}\\
 &  &  &  & \mathbf{R}_{2}^{-1}
\end{array}\right)\left(\begin{array}{ccc}
\mathbf{H}_{0}\\
\mathbf{F}_{1} & -\mathbf{I}\\
 & \mathbf{H}_{1}\\
 & \mathbf{F}_{2} & -\mathbf{I}\\
 &  & \mathbf{H}_{2}
\end{array}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
=\left(\begin{array}{ccc}
\mathbf{H}_{0}^{\intercal}\mathbf{R}_{0}^{-1}\mathbf{H}_{0}+\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\mathbf{F}_{1} & -\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\\
-\mathbf{Q}_{1}^{-1}\mathbf{F}_{1} & \mathbf{Q}_{1}^{-1}+\mathbf{H}_{1}^{\intercal}\mathbf{R}_{1}^{-1}\mathbf{H}_{1}+\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\mathbf{F}_{2} & -\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\\
 & -\mathbf{Q}_{2}^{-1}\mathbf{F}_{2} & \mathbf{Q}_{2}^{-1}+\mathbf{H}_{2}^{\intercal}\mathbf{R}_{2}^{-1}\mathbf{H}_{2}
\end{array}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
R^{\intercal}\Sigma z=\left(\begin{array}{ccccc}
\mathbf{H}_{0}^{\intercal} & \mathbf{F}_{1}^{\intercal}\\
 & -\mathbf{I} & \mathbf{H}_{1}^{\intercal} & \mathbf{F}_{2}^{\intercal}\\
 &  &  & -\mathbf{I} & \mathbf{H}_{2}^{\intercal}
\end{array}\right)\left(\begin{array}{ccccc}
\mathbf{R}_{0}^{-1}\\
 & \mathbf{Q}_{1}^{-1}\\
 &  & \mathbf{R}_{1}^{-1}\\
 &  &  & \mathbf{Q}_{2}^{-1}\\
 &  &  &  & \mathbf{R}_{2}^{-1}
\end{array}\right)\left(\begin{array}{c}
\mathbf{z}_{0}\\
\mathbf{B}_{1}\mathbf{u}_{0}\\
\mathbf{z}_{1}\\
\mathbf{B}_{2}\mathbf{u}_{1}\\
\mathbf{z}_{2}
\end{array}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
=\left(\begin{array}{c}
\mathbf{H}_{0}^{\intercal}\mathbf{R}_{0}^{-1}\mathbf{z}_{0}+\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\mathbf{B}_{1}\mathbf{u}_{0}\\
-\mathbf{Q}_{1}^{-1}\mathbf{B}_{1}\mathbf{u}_{0}+\mathbf{H}_{1}^{\intercal}\mathbf{R}_{1}^{-1}\mathbf{z}_{1}+\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\mathbf{B}_{2}\mathbf{u}_{1}\\
-\mathbf{Q}_{2}^{-1}\mathbf{B}_{2}\mathbf{u}_{1}+\mathbf{H}_{2}^{\intercal}\mathbf{R}_{2}^{-1}\mathbf{z}_{2}
\end{array}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
x=(R^{\intercal}\Sigma R)^{-1}R^{\intercal}\Sigma z
\]

\end_inset


\end_layout

\begin_layout Subsection
Solution via Gaussian Elimination
\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\begin{array}{ccc}
\mathbf{H}_{0}^{\intercal}\mathbf{R}_{0}^{-1}\mathbf{H}_{0}+\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\mathbf{F}_{1} & -\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\\
-\mathbf{Q}_{1}^{-1}\mathbf{F}_{1} & \mathbf{Q}_{1}^{-1}+\mathbf{H}_{1}^{\intercal}\mathbf{R}_{1}^{-1}\mathbf{H}_{1}+\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\mathbf{F}_{2} & -\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\\
 & -\mathbf{Q}_{2}^{-1}\mathbf{F}_{2} & \mathbf{Q}_{2}^{-1}+\mathbf{H}_{2}^{\intercal}\mathbf{R}_{2}^{-1}\mathbf{H}_{2}
\end{array}\left|\begin{array}{c}
\mathbf{H}_{0}^{\intercal}\mathbf{R}_{0}^{-1}\mathbf{z}_{0}+\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\mathbf{B}_{1}\mathbf{u}_{0}\\
-\mathbf{Q}_{1}^{-1}\mathbf{B}_{1}\mathbf{u}_{0}+\mathbf{H}_{1}^{\intercal}\mathbf{R}_{1}^{-1}\mathbf{z}_{1}+\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\mathbf{B}_{2}\mathbf{u}_{1}\\
-\mathbf{Q}_{2}^{-1}\mathbf{B}_{2}\mathbf{u}_{1}+\mathbf{H}_{2}^{\intercal}\mathbf{R}_{2}^{-1}\mathbf{z}_{2}
\end{array}\right.\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Lambda_{0}\equiv(\mathbf{H}_{0}^{\intercal}\mathbf{R}_{0}^{-1}\mathbf{H}_{0}+\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\mathbf{F}_{1})^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\gamma_{0}\equiv\mathbf{H}_{0}^{\intercal}\mathbf{R}_{0}^{-1}\mathbf{z}_{0}+\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\mathbf{B}_{1}\mathbf{u}_{0}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\begin{array}{ccc}
\mathbf{I} & -\Lambda_{0}\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\\
-\mathbf{Q}_{1}^{-1}\mathbf{F}_{1} & \mathbf{Q}_{1}^{-1}+\mathbf{H}_{1}^{\intercal}\mathbf{R}_{1}^{-1}\mathbf{H}_{1}+\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\mathbf{F}_{2} & -\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\\
 & -\mathbf{Q}_{2}^{-1}\mathbf{F}_{2} & \mathbf{Q}_{2}^{-1}+\mathbf{H}_{2}^{\intercal}\mathbf{R}_{2}^{-1}\mathbf{H}_{2}
\end{array}\left|\begin{array}{c}
\Lambda_{0}\gamma_{0}\\
-\mathbf{Q}_{1}^{-1}\mathbf{B}_{1}\mathbf{u}_{0}+\mathbf{H}_{1}^{\intercal}\mathbf{R}_{1}^{-1}\mathbf{z}_{1}+\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\mathbf{B}_{2}\mathbf{u}_{1}\\
-\mathbf{Q}_{2}^{-1}\mathbf{B}_{2}\mathbf{u}_{1}+\mathbf{H}_{2}^{\intercal}\mathbf{R}_{2}^{-1}\mathbf{z}_{2}
\end{array}\right.\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\begin{array}{ccc}
\mathbf{I} & -\Lambda_{0}\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\\
0 & \mathbf{Q}_{1}^{-1}+\mathbf{H}_{1}^{\intercal}\mathbf{R}_{1}^{-1}\mathbf{H}_{1}+\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\mathbf{F}_{2}-\mathbf{Q}_{1}^{-1}\mathbf{F}_{1}\Lambda_{0}\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1} & -\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\\
 & -\mathbf{Q}_{2}^{-1}\mathbf{F}_{2} & \mathbf{Q}_{2}^{-1}+\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}\mathbf{H}_{k}
\end{array}\left|\begin{array}{c}
\Lambda_{0}\gamma_{0}\\
-\mathbf{Q}_{1}^{-1}\mathbf{B}_{1}\mathbf{u}_{0}+\mathbf{H}_{1}^{\intercal}\mathbf{R}_{1}^{-1}\mathbf{z}_{1}+\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\mathbf{B}_{2}\mathbf{u}_{1}+\mathbf{Q}_{1}^{-1}\mathbf{F}_{1}\Lambda_{0}\gamma_{0}\\
-\mathbf{Q}_{2}^{-1}\mathbf{B}_{2}\mathbf{u}_{1}+\mathbf{H}_{2}^{\intercal}\mathbf{R}_{2}^{-1}\mathbf{z}_{2}
\end{array}\right.\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Lambda_{1}\equiv(\mathbf{Q}_{1}^{-1}+\mathbf{H}_{1}^{\intercal}\mathbf{R}_{1}^{-1}\mathbf{H}_{1}+\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\mathbf{F}_{2}-\mathbf{Q}_{1}^{-1}\mathbf{F}_{1}\Lambda_{0}\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1})^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\gamma_{1}\equiv-\mathbf{Q}_{1}^{-1}\mathbf{B}_{1}\mathbf{u}_{0}+\mathbf{H}_{1}^{\intercal}\mathbf{R}_{1}^{-1}\mathbf{z}_{1}+\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\mathbf{B}_{2}\mathbf{u}_{1}+\mathbf{Q}_{1}^{-1}\mathbf{F}_{1}\Lambda_{0}\gamma_{0}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\begin{array}{ccc}
\mathbf{I} & -\Lambda_{0}\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\\
0 & \mathbf{I} & -\Lambda_{1}\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\\
 & -\mathbf{Q}_{2}^{-1}\mathbf{F}_{2} & \mathbf{Q}_{2}^{-1}+\mathbf{H}_{2}^{\intercal}\mathbf{R}_{2}^{-1}\mathbf{H}_{2}
\end{array}\left|\begin{array}{c}
\Lambda_{0}\gamma_{0}\\
\Lambda_{1}\gamma_{1}\\
-\mathbf{Q}_{2}^{-1}\mathbf{B}_{2}\mathbf{u}_{1}+\mathbf{H}_{2}^{\intercal}\mathbf{R}_{2}^{-1}\mathbf{z}_{2}
\end{array}\right.\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\begin{array}{ccc}
\mathbf{I} & -\Lambda_{0}\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\\
0 & \mathbf{I} & -\Lambda_{1}\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\\
 & 0 & \mathbf{Q}_{2}^{-1}+\mathbf{H}_{2}^{\intercal}\mathbf{R}_{2}^{-1}\mathbf{H}_{2}-\mathbf{Q}_{2}^{-1}\mathbf{F}_{2}\Lambda_{1}\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}
\end{array}\left|\begin{array}{c}
\Lambda_{0}\gamma_{0}\\
\Lambda_{1}\gamma_{1}\\
-\mathbf{Q}_{2}^{-1}\mathbf{B}_{2}\mathbf{u}_{1}+\mathbf{H}_{2}^{\intercal}\mathbf{R}_{2}^{-1}\mathbf{z}_{2}+\mathbf{Q}_{2}^{-1}\mathbf{F}_{2}\Lambda_{1}\gamma_{1}
\end{array}\right.\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Lambda_{2}\equiv(\mathbf{Q}_{2}^{-1}+\mathbf{H}_{2}^{\intercal}\mathbf{R}_{2}^{-1}\mathbf{H}_{2}-\mathbf{Q}_{2}^{-1}\mathbf{F}_{2}\Lambda_{1}\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1})^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\gamma_{2}\equiv-\mathbf{Q}_{2}^{-1}\mathbf{B}_{2}\mathbf{u}_{1}+\mathbf{H}_{2}^{\intercal}\mathbf{R}_{2}^{-1}\mathbf{z}_{2}+\mathbf{Q}_{2}^{-1}\mathbf{F}_{2}\Lambda_{1}\gamma_{1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\begin{array}{ccc}
\mathbf{I} & -\Lambda_{0}\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\\
0 & \mathbf{I} & -\Lambda_{1}\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\\
 & 0 & \mathbf{I}
\end{array}\left|\begin{array}{c}
\Lambda_{0}\gamma_{0}\\
\Lambda_{1}\gamma_{1}\\
\Lambda_{2}\gamma_{2}
\end{array}\right.\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{x}_{2}\equiv\Lambda_{2}\gamma_{2}$
\end_inset


\end_layout

\begin_layout Subsection
Solution to Intermediate States, via More Gaussian Elimination
\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\begin{array}{ccc}
\mathbf{I} & -\Lambda_{0}\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\\
0 & \mathbf{I} & 0\\
 & 0 & \mathbf{I}
\end{array}\left|\begin{array}{c}
\Lambda_{0}\gamma_{0}\\
\Lambda_{1}\gamma_{1}+\Lambda_{1}\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\mathbf{x}_{2}\\
\mathbf{x}_{2}
\end{array}\right.\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{x}_{1}\equiv\Lambda_{1}\gamma_{1}+\Lambda_{1}\mathbf{F}_{2}^{\intercal}\mathbf{Q}_{2}^{-1}\mathbf{x}_{2}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\begin{array}{ccc}
\mathbf{I} & 0\\
0 & \mathbf{I} & 0\\
 & 0 & \mathbf{I}
\end{array}\left|\begin{array}{c}
\Lambda_{0}\gamma_{0}+\Lambda_{0}\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\mathbf{x}_{1}\\
\mathbf{x}_{1}\\
\mathbf{x}_{2}
\end{array}\right.\right)$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{x}_{0}\equiv\Lambda_{0}\gamma_{0}+\Lambda_{0}\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\mathbf{x}_{1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\left(\begin{array}{ccc}
\mathbf{I} & 0\\
0 & \mathbf{I} & 0\\
 & 0 & \mathbf{I}
\end{array}\left|\begin{array}{c}
\mathbf{x}_{0}\\
\mathbf{x}_{1}\\
\mathbf{x}_{2}
\end{array}\right.\right)$
\end_inset


\end_layout

\begin_layout Subsection
Condensed Version
\end_layout

\begin_layout Standard
\begin_inset Formula $\Lambda_{0}\equiv(\mathbf{H}_{0}^{\intercal}\mathbf{R}_{0}^{-1}\mathbf{H}_{0}+\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\mathbf{F}_{1})^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\gamma_{0}\equiv\mathbf{H}_{0}^{\intercal}\mathbf{R}_{0}^{-1}\mathbf{z}_{0}+\mathbf{F}_{1}^{\intercal}\mathbf{Q}_{1}^{-1}\mathbf{B}_{1}\mathbf{u}_{0}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Lambda_{k}\equiv(\mathbf{Q}_{k}^{-1}+\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}\mathbf{H}_{k}+\mathbf{F}_{k+1}^{\intercal}\mathbf{Q}_{k+1}^{-1}\mathbf{F}_{k+1}-\mathbf{Q}_{k}^{-1}\mathbf{F}_{k}\Lambda_{k-1}\mathbf{F}_{k}^{\intercal}\mathbf{Q}_{k}^{-1})^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\gamma_{k}\equiv-\mathbf{Q}_{k}^{-1}\mathbf{B}_{k}\mathbf{u}_{k-1}+\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}\mathbf{z}_{k}+\mathbf{F}_{k+1}^{\intercal}\mathbf{Q}_{k+1}^{-1}\mathbf{B}_{k+1}\mathbf{u}_{k}+\mathbf{Q}_{k}^{-1}\mathbf{F}_{k}\Lambda_{k-1}\gamma_{k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Lambda_{f}\equiv(\mathbf{Q}_{f}^{-1}+\mathbf{H}_{f}^{\intercal}\mathbf{R}_{f}^{-1}\mathbf{H}_{f}-\mathbf{Q}_{f}^{-1}\mathbf{F}_{f}\Lambda_{f-1}\mathbf{F}_{f}^{\intercal}\mathbf{Q}_{f}^{-1})^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\gamma_{f}\equiv-\mathbf{Q}_{f}^{-1}\mathbf{B}_{f}\mathbf{u}_{f-1}+\mathbf{H}_{f}^{\intercal}\mathbf{R}_{f}^{-1}\mathbf{z}_{f}+\mathbf{Q}_{f}^{-1}\mathbf{F}_{f}\Lambda_{f-1}\gamma_{f-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{x}_{f}\equiv\Lambda_{f}\gamma_{f}$
\end_inset


\end_layout

\begin_layout Subsection
Woodbury Matrix Inversion Lemma
\end_layout

\begin_layout Standard
\begin_inset Formula $\left[\begin{array}{cc}
A & U\\
V & -C^{-1}
\end{array}\right]\left[\begin{array}{c}
X\\
Y
\end{array}\right]=\left[\begin{array}{c}
I\\
0
\end{array}\right]$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $(A+UCV)^{-1}=X=A^{-1}(I-U(C^{-1}+VA^{-1}U)^{-1}VA^{-1})$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $(A+UCV)^{-1}=A^{-1}-A^{-1}U(C^{-1}+VA^{-1}U)^{-1}VA^{-1}$
\end_inset


\end_layout

\begin_layout Subsection
Two Step Kalman
\end_layout

\begin_layout Standard
\begin_inset Formula $\hat{x}_{k|k}=\mathbf{F}_{k}\hat{\mathbf{x}}_{k-1|k-1}+\mathbf{B}_{k}\mathbf{u}_{k-1}+\mathbf{P}_{k|k-1}\mathbf{H}_{k}^{\intercal}\mathbf{S}_{k}^{-1}(\mathbf{z}_{k}-\mathbf{H}_{k}(\mathbf{F}_{k}\hat{\mathbf{x}}_{k-1|k-1}+\mathbf{B}_{k}\mathbf{u}_{k-1}))$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{P}_{k|k}=(\mathbf{I}-(\mathbf{F}_{k}\mathbf{P}_{k-1|k-1}\mathbf{F}_{k}^{\intercal}+\mathbf{Q}_{k})\mathbf{H}_{k}^{\intercal}\mathbf{H}_{k}(\mathbf{F}_{k}\mathbf{P}_{k-1|k-1}\mathbf{F}_{k}^{\intercal}+\mathbf{Q}_{k})\mathbf{H}_{k}^{\intercal}+\mathbf{R}_{k})^{-1}\mathbf{H}_{k}(\mathbf{F}_{k}\mathbf{P}_{k-1|k-1}\mathbf{F}_{k}^{\intercal}+\mathbf{Q}_{k})$
\end_inset


\end_layout

\begin_layout Subsection
Kalman Steps Represented as Various Potentially Recognizable Operators
\end_layout

\begin_layout Standard
\begin_inset Formula $\hat{\mathbf{x}}_{k|k-1}=\mathbf{F}_{k}\hat{\mathbf{x}}_{k-1|k-1}+\mathbf{B}_{k}\mathbf{u}_{k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{U}_{k}:x\rightarrow\mathbf{F}_{k}x+\mathbf{B}_{k}\mathbf{u}_{k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{P}_{k|k-1}=\mathbf{F}_{k}\mathbf{P}_{k-1|k-1}\mathbf{F}_{k}^{\intercal}+\mathbf{Q}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{P}_{k}:K\rightarrow\mathbf{F}_{k}K\mathbf{F}_{k}^{\intercal}+\mathbf{Q}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\tilde{\mathbf{y}}_{k}=\mathbf{z}_{k}-\mathbf{H}_{k}\hat{\mathbf{x}}_{k|k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{Y}_{k}:x\rightarrow\mathbf{z}_{k}-\mathbf{H}_{k}x$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{S}_{k}=\mathbf{H}_{k}\mathbf{P}_{k|k-1}\mathbf{H}_{k}^{\intercal}+\mathbf{R}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{S}_{k}:K\rightarrow\mathbf{H}_{k}K\mathbf{H}_{k}^{\intercal}+\mathbf{R}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{K}_{k}=\mathbf{P}_{k|k-1}\mathbf{H}_{k}^{\intercal}\mathbf{S}_{k}^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{K}_{k}:K\rightarrow K\mathbf{H}_{k}^{\intercal}(\mathcal{S}_{k}\circ K)^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\hat{x}_{k|k}=\hat{x}_{k|k-1}+\mathbf{K}_{k}\tilde{\mathbf{y}}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{P}_{k|k}=(\mathbf{I}-\mathbf{K}_{k}\mathbf{H}_{k})\mathbf{P}_{k|k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{D}_{k}:K\rightarrow\mathbf{I}-K\mathbf{H}_{k}$
\end_inset


\end_layout

\begin_layout Standard
maybe I can add a new operator to simplify that last equation
\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{Z}_{k}:K\rightarrow K-K\mathbf{H}_{k}^{\intercal}(\mathcal{S}_{k}^{-1}\circ K)\mathbf{H}_{k}K=K(K^{-1}-\mathbf{H}_{k}^{\intercal}(\mathcal{S}_{k}^{-1}\circ K)\mathbf{H}_{k})K\;|\; K^{\intercal}=K>0$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{A}_{k}:(x,K)\rightarrow x+K\mathbf{H}_{k}^{\intercal}(\mathcal{S}_{k}^{-1}\circ K)\mathcal{Y}_{k}\circ\mathcal{U}_{k}\circ x$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\hat{x}_{k|k}=\mathcal{A}_{k}\circ(\mathcal{U}_{k}\circ\hat{\mathbf{x}}_{k-1|k-1},\mathcal{P}_{k}\circ\mathbf{P}_{k-1|k-1})$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{P}_{k|k}=\mathcal{Z}_{k}\circ\mathcal{P}_{k}\circ\mathbf{P}_{k-1|k-1}$
\end_inset


\end_layout

\begin_layout Subsection
Inverses for the Kalman Operator Set
\end_layout

\begin_layout Standard
\begin_inset Formula $(A+UCV)^{-1}=A^{-1}-A^{-1}U(C^{-1}+VA^{-1}U)^{-1}VA^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{S}_{k}:K\rightarrow\mathbf{H}_{k}K\mathbf{H}_{k}^{\intercal}+\mathbf{R}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{S}_{k}^{-1}:K\rightarrow(\mathbf{H}_{k}K\mathbf{H}_{k}^{\intercal}+\mathbf{R}_{k})^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $(\mathbf{R}_{k}+\mathbf{H}_{k}K\mathbf{H}_{k}^{\intercal})^{-1}=\mathbf{R}_{k}^{-1}-\mathbf{R}_{k}^{-1}\mathbf{H}_{k}(K^{-1}+\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}\mathbf{H}_{k})^{-1}\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{S}_{k}^{-1}:K\rightarrow\mathbf{R}_{k}^{-1}-\mathbf{R}_{k}^{-1}\mathbf{H}_{k}(K^{-1}+\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}\mathbf{H}_{k})^{-1}\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{P}_{k}:K\rightarrow\mathbf{F}_{k}K\mathbf{F}_{k}^{\intercal}+\mathbf{Q}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{P}_{k}^{-1}:K\rightarrow(\mathbf{F}_{k}K\mathbf{F}_{k}^{\intercal}+\mathbf{Q}_{k})^{-1}=\mathbf{Q}_{k}^{-1}-\mathbf{Q}_{k}^{-1}\mathbf{F}_{k}(K^{-1}+\mathbf{F}_{k}^{\intercal}\mathbf{Q}_{k}^{-1}\mathbf{F}_{k})^{-1}\mathbf{F}_{k}^{\intercal}\mathbf{Q}_{k}^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{D}_{k}^{-1}:K\rightarrow(\mathbf{I}-K\mathbf{H}_{k})^{-1}=\mathbf{I}-(K^{-1}+\mathbf{H}_{k})^{-1}\mathbf{H}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{Z}_{k}:K\rightarrow K-K\mathbf{H}_{k}^{\intercal}(\mathbf{H}_{k}K\mathbf{H}_{k}^{\intercal}+\mathbf{R}_{k})^{-1}\mathbf{H}_{k}K=K(K^{-1}-\mathbf{H}_{k}^{\intercal}(\mathbf{H}_{k}K\mathbf{H}_{k}^{\intercal}+\mathbf{R}_{k})^{-1}\mathbf{H}_{k})K\;|\; K^{\intercal}=K>0$
\end_inset


\end_layout

\begin_layout Standard
can be simplified using the woodbury inversion lemma
\end_layout

\begin_layout Standard
\begin_inset Formula $(K^{-1}+\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}\mathbf{H}_{k})^{-1}=K-K\mathbf{H}_{k}^{\intercal}(\mathbf{R}_{k}+\mathbf{H}_{k}K\mathbf{H}_{k}^{\intercal})^{-1}\mathbf{H}_{k}K$
\end_inset


\end_layout

\begin_layout Standard
to
\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{Z}_{k}:K\rightarrow(K^{-1}+\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}\mathbf{H}_{k})^{-1}$
\end_inset


\end_layout

\begin_layout Standard
thus the inverse is simple
\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{Z}_{k}^{-1}:K\rightarrow K^{-1}+\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}\mathbf{H}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{A}_{k}:(x,K)\rightarrow x+K\mathbf{H}_{k}^{\intercal}(\mathcal{S}_{k}^{-1}\circ K)\mathcal{Y}_{k}\circ\mathcal{U}_{k}\circ x$
\end_inset


\end_layout

\begin_layout Subsection
Rexpression using Kalman Operator Set
\end_layout

\begin_layout Standard
\begin_inset Formula $\hat{\mathbf{x}}_{k|k-1}=\mathcal{U}_{k}\circ\hat{\mathbf{x}}_{k-1|k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{P}_{k|k-1}=\mathcal{P}_{k}\circ\mathbf{P}_{k-1|k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\tilde{\mathbf{y}}_{k}=\mathcal{Y}_{k}\circ\mathcal{U}_{k}\circ\hat{\mathbf{x}}_{k-1|k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{S}_{k}=\mathcal{S}_{k}\circ\mathcal{P}_{k}\circ\mathbf{P}_{k-1|k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{K}_{k}=(\mathcal{P}_{k}\circ\mathbf{P}_{k-1|k-1})\mathbf{H}_{k}^{\intercal}(\mathcal{S}_{k}^{-1}\circ\mathcal{P}_{k}\circ\mathbf{P}_{k-1|k-1})$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\hat{x}_{k|k}=\mathcal{A}_{k}\circ(\mathcal{U}_{k}\circ\hat{\mathbf{x}}_{k-1|k-1},\mathcal{P}_{k}\circ\mathbf{P}_{k-1|k-1})$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{P}_{k|k}=\mathcal{Z}_{k}\circ\mathcal{P}_{k}\circ\mathbf{P}_{k-1|k-1}$
\end_inset


\end_layout

\begin_layout Standard
maybe I can add a new operator to simplify that last equation
\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{Z}_{k}:K\rightarrow K-K\mathbf{H}_{k}^{\intercal}(\mathcal{S}_{k}^{-1}\circ K)\mathbf{H}_{k}K=K(K^{-1}-\mathbf{H}_{k}^{\intercal}(\mathcal{S}_{k}^{-1}\circ K)\mathbf{H}_{k})K\;|\; K^{\intercal}=K>0$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{A}_{k}:(x,K)\rightarrow x+K\mathbf{H}_{k}^{\intercal}(\mathcal{S}_{k}^{-1}\circ K)\mathcal{Y}_{k}\circ\mathcal{U}_{k}\circ x$
\end_inset


\end_layout

\begin_layout Standard
that was the kalman filter expressed in the operator set
\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{U}_{k}:x\rightarrow\mathbf{F}_{k}x+\mathbf{B}_{k}\mathbf{u}_{k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{P}_{k}:K\rightarrow\mathbf{F}_{k}K\mathbf{F}_{k}^{\intercal}+\mathbf{Q}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{Y}_{k}:x\rightarrow\mathbf{z}_{k}-\mathbf{H}_{k}x$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{S}_{k}:K\rightarrow\mathbf{H}_{k}K\mathbf{H}_{k}^{\intercal}+\mathbf{R}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{K}_{k}:K\rightarrow K\mathbf{H}_{k}^{\intercal}(\mathcal{S}_{k}\circ K)^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{D}_{k}:K\rightarrow\mathbf{I}-K\mathbf{H}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Lambda_{k}\equiv(\mathbf{Q}_{k}^{-1}+\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}\mathbf{H}_{k}+\mathbf{F}_{k+1}^{\intercal}\mathbf{Q}_{k+1}^{-1}\mathbf{F}_{k+1}-\mathbf{Q}_{k}^{-1}\mathbf{F}_{k}\Lambda_{k-1}\mathbf{F}_{k}^{\intercal}\mathbf{Q}_{k}^{-1})^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathcal{P}_{k}^{-1}:K\rightarrow(\mathbf{F}_{k}K\mathbf{F}_{k}^{\intercal}+\mathbf{Q}_{k})^{-1}=\mathbf{Q}_{k}^{-1}-\mathbf{Q}_{k}^{-1}\mathbf{F}_{k}(K^{-1}+\mathbf{F}_{k}^{\intercal}\mathbf{Q}_{k}^{-1}\mathbf{F}_{k})^{-1}\mathbf{F}_{k}^{\intercal}\mathbf{Q}_{k}^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{Q}_{k}^{-1}-\mathbf{Q}_{k}^{-1}\mathbf{F}_{k}\Lambda_{k-1}\mathbf{F}_{k}^{\intercal}\mathbf{Q}_{k}^{-1}=\mathcal{P}_{k}^{-1}\circ(\mathbf{Q}_{k-1}^{-1}+\mathbf{H}_{k-1}^{\intercal}\mathbf{R}_{k-1}^{-1}\mathbf{H}_{k-1}-\mathbf{Q}_{k-1}^{-1}\mathbf{F}_{k-1}\Lambda_{k-2}\mathbf{F}_{k-1}^{\intercal}\mathbf{Q}_{k-1}^{-1})$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{Q}_{k}^{-1}-\mathbf{Q}_{k}^{-1}\mathbf{F}_{k}\Lambda_{k-1}\mathbf{F}_{k}^{\intercal}\mathbf{Q}_{k}^{-1}=\mathcal{P}_{k}^{-1}\circ(\Lambda_{k-1}^{-1}-\mathbf{F}_{k}^{\intercal}\mathbf{Q}_{k}^{-1}\mathbf{F}_{k})$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Lambda_{k}\equiv(\mathcal{P}_{k}^{-1}\circ(\Lambda_{k-1}^{-1}-\mathbf{F}_{k}^{\intercal}\mathbf{Q}_{k}^{-1}\mathbf{F}_{k})+\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}\mathbf{H}_{k}+\mathbf{F}_{k+1}^{\intercal}\mathbf{Q}_{k+1}^{-1}\mathbf{F}_{k+1})^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Gamma_{k}\equiv\Lambda_{k}^{-1}-\mathbf{F}_{k+1}^{\intercal}\mathbf{Q}_{k+1}^{-1}\mathbf{F}_{k+1}|\Gamma_{f}=\Lambda_{f}^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Gamma_{k}=\mathcal{P}_{k}^{-1}\circ(\Gamma_{k-1})+\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}\mathbf{H}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\Gamma_{k}=\mathcal{Z}_{k}^{-1}\circ(\mathcal{P}_{k}^{-1}\circ(\Gamma_{k-1}))^{-1}$
\end_inset


\end_layout

\begin_layout Standard
thus we can relate the two matrix quantities:
\end_layout

\begin_layout Standard
\begin_inset Formula $\Gamma_{k}=\mathbf{P}_{k|k}^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{P}_{k|k-1}=\mathcal{P}_{k}\circ\mathbf{P}_{k-1|k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\gamma_{k}\equiv-\mathbf{Q}_{k}^{-1}\mathbf{B}_{k}\mathbf{u}_{k-1}+\mathbf{H}_{k}^{\intercal}\mathbf{R}_{k}^{-1}\mathbf{z}_{k}+\mathbf{F}_{k+1}^{\intercal}\mathbf{Q}_{k+1}^{-1}\mathbf{B}_{k+1}\mathbf{u}_{k}+\mathbf{Q}_{k}^{-1}\mathbf{F}_{k}\Lambda_{k-1}\gamma_{k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{x}_{f}=\Lambda_{f}\gamma_{f}=\Gamma_{f}^{-1}\gamma_{f}$
\end_inset


\end_layout

\begin_layout Section
Kalman Filter
\end_layout

\begin_layout Standard
\begin_inset Formula $\hat{\mathbf{x}}_{k|k-1}=\mathbf{F}_{k}\hat{\mathbf{x}}_{k-1|k-1}+\mathbf{B}_{k}\mathbf{u}_{k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{P}_{k|k-1}=\mathbf{F}_{k}\mathbf{P}_{k-1|k-1}\mathbf{F}_{k}^{\intercal}+\mathbf{Q}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\tilde{\mathbf{y}}_{k}=\mathbf{z}_{k}-\mathbf{H}_{k}\hat{\mathbf{x}}_{k|k-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{S}_{k}=\mathbf{H}_{k}\mathbf{P}_{k|k-1}\mathbf{H}_{k}^{\intercal}+\mathbf{R}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{K}_{k}=\mathbf{P}_{k|k-1}\mathbf{H}_{k}^{\intercal}\mathbf{S}_{k}^{-1}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\hat{x}_{k|k}=\hat{x}_{k|k-1}+\mathbf{K}_{k}\tilde{\mathbf{y}}_{k}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\mathbf{P}_{k|k}=(\mathbf{I}-\mathbf{K}_{k}\mathbf{H}_{k})\mathbf{P}_{k|k-1}$
\end_inset


\end_layout

\end_body
\end_document
