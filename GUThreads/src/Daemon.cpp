/*
 * Daemon.cpp
 *
 *    Created on: Apr 7, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include "Daemon.hpp"
namespace gu
{
namespace threads
{

Daemon::Daemon(void) :
			daemonThread(),
			firstLoopFlag(false),
			isRunning(false)
{

}
Daemon::~Daemon()
{
	pthread_cancel(daemonThread);
	pthread_join(daemonThread, NULL);
}
void Daemon::terminate()
{
	isRunning = false;
}
bool Daemon::isFirstLoop()
{
	return firstLoopFlag;
}
void Daemon::run()
{
	isRunning = true;
	firstLoopFlag = true;
	while (isRunning)
	{
		loop();
		firstLoopFlag = false;
		pthread_testcancel();
	}
}
void *runDaemon(void * arg)
{
	printf("Starting daemon Thread\n");
	((Daemon*) arg)->run();
	return NULL;
}
void sigint(int signo)
{
	(void) signo;
}
void Daemon::start()
{
	sigset_t sigset, oldset;
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGINT);
	pthread_sigmask(SIG_BLOCK, &sigset, &oldset);
	pthread_create(&daemonThread, NULL, runDaemon, this);
	struct sigaction s;
	s.sa_handler = sigint;
	sigemptyset(&s.sa_mask);
	s.sa_flags = 0;
	sigaction(SIGINT, &s, NULL);
	pthread_sigmask(SIG_SETMASK, &oldset, NULL);
}

}
}
