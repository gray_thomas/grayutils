/*
 * RTAIDaemon.hpp
 *
 *    Created on: Jan 6, 2016
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */


#ifndef RTAI_DAEMON_HPP_
#define RTAI_DAEMON_HPP_




#include <pthread.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/mman.h>

#include <rtai_sem.h>

#define DEFAULT_CPUMAP 0xF

class RTAIDaemon
{
    RT_TASK* task;
    int number;
    bool firstLoopFlag;
    int period_ns;
protected:
    static int num_daemons;
    static volatile int end;
    static volatile RTIME start;
    static std::vector<RTAIDaemon*> daemons;
    static SEM *barrier;
    static int tick_time_ns;

    virtual void loop(void) = 0;
    inline bool isFirstLoop() {return firstLoopFlag;}
public:
    RTAIDaemon(int priority, int period_ns);
    void run(void);
    virtual ~RTAIDaemon(void);
    static void start_all_daemons(int tick_time_ns);
    static void end_all_daemons(int dummy) {end = 1;}
};


void *runRTAIDaemon(void * arg);


#endif /* DAEMON_HPP_ */


// in .cpp
#include "rtai_daemon.hpp"
#include <pthread.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/mman.h>

#include <rtai_sem.h>

#include <cstdio>

int RTAIDaemon::num_daemons = 0;
int RTAIDaemon::end = 0;
RTIME RTAIDaemon::start = 0;
std::vector<RTAIDaemon*> RTAIDaemon::daemons;
SEM * RTAIDaemon::barrier = NULL;
int RTAIDaemon::tick_time_ns = 1000000000;

RTAIDaemon::RTAIDaemon(int priority, int period_ns) :
    RT(),
    number(num_daemons),
    firstLoopFlag(false),
    period_ns(period_ns)
{
    char buffer[6];
    snprintf(buffer, sizeof(buffer), "DMN%02d", number);
    if (!(task = rt_thread_init(nam2num((const char *) buffer)),
            priority, 0, SCHED_FIFO, DEFAULT_CPUMAP))
    {
        printf("Can't init task %s!\n", (const char *)buffer);
        exit(1);
    }
    num_daemons++;
    daemons.push_back(self);
}
RTAIDaemon::~RTAIDaemon()
{
    rt_thread_delete(task);
    rt_thread_join(rt_thread);
    num_daemons--;
}
void RTAIDaemon::run()
{
    firstLoopFlag = true;
    mlockall(MCL_CURRENT | MCL_FUTURE);
    rt_make_hard_real_time();
    rt_sem_wait_barrier();
    period = nano2count(tick_time_ns*(period_ns / tick_time_ns));
    expected = start + 3 * (number + 1) * nano2count(tick_time_ns);
    rt_task_make_periodic(task, expected, period);
    while (!end)
    {
        jit = abs(count2nano(rt_get_time() - expected));
        expected += period;

        loop();

        firstLoopFlag = false;
        // rt_busy_sleep(remaining allocation);
        rt_task_wait_period();
    }
    rt_sem_wait_barrier(barrier);
    rt_make_soft_real_time();
}
void *runRTAIDaemon(void * arg)
{
    printf("Starting daemon Thread\n");
    ((RTAIDaemon*) arg)->run();
    return NULL;
}
void RTAIDaemon::start_all_daemons(int tick_time_ns)
{
    RT_TASK * main_task;
    RTAIDaemon::tick_time_ns = tick_time_ns;

    signal(SIGHUP, end_all_daemons);
    signal(SIGINT, end_all_daemons);
    signal(SIGKILL, end_all_daemons);
    signal(SIGTERM, end_all_daemons);
    signal(SIGALRM, end_all_daemons);

    if (!(main_task = rt_thread_init(nam2num("MNTSK"), 0, 0, SCHED_FIFO, 0xF)))
    {
        printf("Can't start main task!\n");
        exit(1);
    }

    start_rt_timer(0);
    barrier = rt_sem_init(nam2num("PREMSM"), 1 + num_daemons);
    for (std::vector<RTAIDaemon*>::iterator it = daemons.begin(); it != daemons.end(); ++it)
    {
        *it->rt_thread = rt_thread_create(runRTAIDaemon, *it, 0);
    }

    start = rt_get_time() + nano2count(200000000);
    rt_sem_wait_barrier(barrier);
    // block on signal
    pause();

    end = 1;
    rt_sem_wait_barrier(barrier);
    stop_rt_timer();
    rt_sem_delete(barrier);
    rt_thread_delete(main_task);
    // other threads deleted via destructor
}