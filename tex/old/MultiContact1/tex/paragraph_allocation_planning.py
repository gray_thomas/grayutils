figpars = 0
pars = 1  # title: A Phase Space Method for Planning Multi-Contact Dynamic Locomotion in a User Interface
# Phase Space Planning for
# Dynamic Multi-Contact Locomotion
pars += 1  # sections heads 
pars += 1  # abstract

# goal: multi-contact dynamic locomotion planning for robots with feet and
# hands.

# Contributions:
# 1) extending PIPM to multi-contact
# 2) applying optimal time planning to this new model
# 3) a mixed integer convex problem
# 4) chattering explained
# 5) sample code?

# Notes:
# The framework permits lexicographic optimization of stance choice.

# Intro
intro = [
    # motivate with PIPM, overview contributions
    1,
]
pars += sum(intro)
# % Luis: add the following:
#  % Tim Bretl
#  % Kris Hauser
#  % Abderrahane Kheddar 
#  % Bouyarmane
# Literature
lit = [
    1,  # pendulum locomotion works
    1,  # General multi-contact planning approaches work too, but are too slow for locomotion
    1,  # Traditional phase-space Manipulator is fast
    1,  # speed has lead to a Recent revival in humanoid robotics
    1,  # our approach combines pendulum locomotion and minimum time (contributions)
    # abstract model separating center of mass and reaction force dynamics.
    # problem is augmented with contact decision problem
    # optimal chattering case is explained and handled
    # solution is implemented with an easy-to-exactly-integrate hybrid
    # automata.
]
pars += sum(lit)


figpars += 2  # robot mapping
# The Multi-contact model # it is based on KoolenEA2013ICHR, sentis
section1 = [
    1,  # justify point mass
    1,  # Introduction to the model, variables
    1,  # Choice of stances
    1,  # Parameters of a stance including kinematic limits
    1,  # Inequality constraints
    1,  # Moment Equality & CF
    1,  # Convexity, quadratic relationship with speed
    2,  # math (interspersed)
    1,  # resulting optimization problems
]
pars += sum(section1)


# be sure to mention in the explaination:
# credit BobrowDubowskyGibson1985IJRR for the basic idea
# credit ShinMckay1986TAC for search ahead
# credit PfeifferJohanni1987JRA for sources and sinks
# credit SlotineYang1989TRA for critical point classification
# credit ShillerLu1992JDSMC for singular arcs, the maximum speed portion
# credit LippBoyd2014IJC for noting the problem is convex in acc and speedsquared

figpars += 2  # multi-step max algorithm figure
figpars += 2  # max state machine diagram
figpars += 2  # multi-step min algorithm figure
figpars += 2  # min state machine diagram
# Algorithm
section2 = [
    1,  # introduce it, based on previous work
    1,  # explain high level principles: stance selection, integration
    1,  # max speed hybrid automata
    1,  # explain max speed multi-step algorithm figures.
    1,  # min speed hybrid automata
    1,  # explain min speed multi-step algorithm figures.
]
pars += sum(section2)  # multi-step


# figpars += 2  # movie fig
figpars += 4  # 2 column results fig
# Optimal Chattering and other behavior
section3 = [
    # 1,  # implementation & physics verification
    1,  # explain movie fig
    1,  # explain chattering fig and our approach to it
    1,  # using chattering to identify where to combine stances.
    1,  # explain how the user can exploit the step choice properties
]
pars += sum(section3)  # multi-step


# Conclusion
conclusion = [
    1,  # In 3D / ballistic / point foot
    # 1,  # complex contacts
    # 1,  # other easy extensions
    1]  # summarize claims
pars += sum(conclusion)

# Media: movie which shows minimum plan following, maximum plan following,
# chatter, UI? state machine transitions?


pars += 3  # bib

pars += figpars
print "Paper is about %.1f pages long\n" % (pars/8.0)

print "\tAll Figures: %.1f pages (%.1f%%)" % (figpars/8.0, (figpars * 100.0) / pars)
textpars = sum(conclusion + section3 + section2 + section1 + intro + lit)
print "\tTotal Text: %.1f pages (%.1f%%)\n" % (textpars/8.0, (textpars * 100.0) / pars)
print "\tIntroduction: %d par (%.1f%%)" % (sum(intro), (sum(intro) * 100.0) / pars)
print "\tLiterature Review: %d par (%.1f%%)" % (sum(lit), (sum(lit) * 100.0) / pars)
print "\tSection 1: %d par (%.1f%%)" % (sum(section1), (sum(section1) * 100.0) / pars)
print "\tSection 2: %d par (%.1f%%)" % (sum(section2), (sum(section2) * 100.0) / pars)
print "\tSection 3: %d par (%.1f%%)" % (sum(section3), (sum(section3) * 100.0) / pars)
print "\tConclusion: %d par (%.1f%%)" % (sum(conclusion), (sum(conclusion) * 100.0) / pars)
# print "\n\n"
