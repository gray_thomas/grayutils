\documentclass[]{article}
\usepackage[letterpaper, portrait, margin=1.2in]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{float}
\usepackage{placeins}
\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=python,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true
  tabsize=3
}

\newcommand{\mathdom}{\mathbf{dom} \,}
\newcommand{\reals}{\mathbf{R}}
\newcommand{\bbm}{\begin{bmatrix}}
\newcommand{\ebm}{\end{bmatrix}}
\newcommand{\mathif}{\;\mathrm{if}\;}
\newcommand{\mathelse}{\;\mathrm{else}\;}
\newcommand{\mathor}{\;\mathrm{or}\;}
\newcommand{\mathwhere}{\;\mathrm{where}\;}
\newcommand{\del}{\nabla}
%opening
\title{}
\author{}

\begin{document}
\title{Convex Optimization for Model Identification\\Theoretical Application to the NASA Valkyrie Robot's Ankle}

\author{Gray Thomas}
\maketitle
\begin{figure}[!h]
\centering
\includegraphics[height=5in]{Valkyrie_black.png}
\caption{Valkyrie}
\end{figure}
\FloatBarrier
\begin{abstract}
This project pursues the problem of model identification from a convex optimization approach. Beginning with the standard least squares solution technique it branches out to alternate norms and explores the differences between their solutions. It demonstrates the effect of filtering the prediction error 
\end{abstract}
\FloatBarrier
\begin{figure}[!h]
\centering
\includegraphics[height=4in]{rig.png}
\includegraphics[height=4in]{upright2.png}
\caption{Valkyrie robot ankle testing apparatus: locked, and upright configurations}
\end{figure}
\FloatBarrier
\section{Mechanical Intuition}
Valkyrie's ankles, like her other joints, are designed for safe human interaction and have stiff series elastic elements connecting the motor cages to the frame of the shank. The spherical joint ended rods connecting the ankle to the ball screw support sliders have significant backlash which can be felt manually. There is also the strong possibility that the slider deforms significantly, causing increased friction inside the motor assembly. Sensing is highly redundant, with a Renishaw linear encoder measuring spring deflection, a single axis force sensor inside the connecting transmission rod, and a six axis force torque sensor in series with the ankle. 
\par 
What we can infer from this mechanical system from a controls and identification perspective is that the error will likely be different in the cases where the system is transitioning from compression to tension on a rod and when the compression/tension state is constant. We can also expect, given the high amount of mechanical flexing that occurs when the system is exerting high forces that the high frequency dynamics are completely unreliable. Nominally the system has four states per degree of freedom, two associated with rigid body motion, and two associated with the internal torque dynamics of the series elastic and the low level force controller.
\section{Model Structure}
To model the system as a linear, eighth order setup with uncoupled torque dynamics we make a simplifying assumption about the rigid body dynamics, which are not, technically, linear. They are close enough to design a stabilizing controller if not an optimal trajectory generator, especially with an $\infty$-norm bounded model error.
I consider the model to operate on two four dimensional state vectors, one for each degree of freedom:
\begin{align}
x_i=\begin{bmatrix} y_i\\\dot{y}_i \end{bmatrix}=\begin{bmatrix}q_i\\\tau_i \\\dot{q_i} \\ \dot{\tau_i} \end{bmatrix} 
&& 
x=\begin{bmatrix}y_1\\y_2\\\dot{y}_1\\\dot{y}_2 \end{bmatrix} 
&&
\hat{y}_i=\begin{bmatrix} \hat{q}_i \\ \hat{\tau}_i \end{bmatrix}
&& 
\hat{y}= \begin{bmatrix} \hat{y}_i \\ \hat{y}_2 \end{bmatrix}
&&
u_i=\left[\begin{array}{@{}c@{}} \tau_{d,i} \\\hline 
n_{1,i} \\ 
n_{2,i} \\
\hline a_{ext,1} \end{array}\right]
&& 
u= \begin{bmatrix} u_i \\ u_2 \end{bmatrix}
\end{align}
This models the inputs to the system as the desired torque $\tau_{d,i}$, a noise associated with friction in the joint $n_{1,i}$, internal friction noise for the series elastic actuator $n_{2,i}$, and an external acceleration $a_{ext,i}$. The outputs of each joint are the measured angle $\hat{q}_i$ and torque $\hat{\tau}_i$ which are nearly perfect measurements of the state corrupted primarily by quantization error and asynchronous update effects.

We start with the model for an independent single degree of freedom system:
\begin{equation}
G_i(s)\sim \left[
\begin{array}{@{}cccc|c|cc|c@{}}
0 & 0 & 1 & 0 & 0 & 0 & 0 & 0\\
0 & 0 & 0 & 1 & 0 & 0 & 0 & 0\\
g_{xx} & 1/m & 0 & 0 & 0 & f_1/m & 0 & 1\\
-g_{xx} r &  -r/m -\alpha & 0 & -\beta & \alpha &- rf_1/m & f_2 & -r \\
\hline
1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
0 & 1 & 0 & 0 & 0 & 0 & 0 & 0\\
\end{array}
\right]
\end{equation}
Now, this is admittedly not a very simple model. The term $g_{xx}$ here represents the Hessian of the gravitational potential field, in units of acceleration. The torque system is fundamentally related to the position system by the drivetrain, since the torque is the spring deflection, and is thus proportional to the difference between the hidden motor angle state and the output angle. Thus it makes sense that acceleration in the output system will be reflected in acceleration of torque by a constant of relation $r$.
\subsection{Discrete time model}
In order to identify this model from sampled data we will need to discretize it, as methods for identifying differential state space models from data are underdeveloped due to the non-linearity of the problem. Since our output represents our state fairly accurately, and since the model includes each of the directly visible states as a pure integrator, a third order matrix autoregressive moving average model with a single moving average sample period should describe the model. 
\begin{equation}
\tilde{y}_k + A_1\hat{y}_{k-1} + A_2\hat{y}_{k-2}=B_2 u_{k-1}
\end{equation}
More specifically, we can anticipate that the majority effect with $A_1$ and $A_2$ is to maintain the integrated states while reflecting the derivative states using a finite difference.
\begin{eqnarray}
x_{k-1} \approx 
	\begin{bmatrix} 
		\hat{y}_{k-1} \\ 
		\frac{\hat{y}_{k-1}-\hat{y}_{k-2}}{\Delta_T}
	\end{bmatrix}
\\
x_{k} \approx 
	\begin{bmatrix} 
		\tilde{y}_{k} \\ 
		\frac{\tilde{y}_{k}-\hat{y}_{k-1}}{\Delta_T}
	\end{bmatrix} 
\approxeq 
	x_{k-1} + \Delta _T 
		\begin{bmatrix}
			0 & 0 & 1 & 0 \\
			0 & 0 & 0 & 1 \\
			g_{xx} & 1/m & 0 & 0 \\
			-g_{xx}r & -r/m -\alpha & 0 & -\beta
		\end{bmatrix} x_{k-1} 
	+ \Delta _T \begin{bmatrix}
	0 \\ 0 \\ 0\\ \alpha
	\end{bmatrix} \tau_{d}
\\
	\frac{\tilde{y}_{k}-\hat{y}_{k-1}}{\Delta_T} \approxeq
		\begin{bmatrix}
			g_{xx} & 1/m \\
			-g_{xx}r & -r/m -\alpha 
		\end{bmatrix}
	\hat{y}_{k-1}
	+
		\begin{bmatrix}
			1 & 0 \\
			0 & 1-\beta
		\end{bmatrix}
	\frac{\hat{y}_{k-1}-\hat{y}_{k-2}}{\Delta_T}
	+ \begin{bmatrix}
		 0\\ \alpha
		\end{bmatrix} \tau_{d}
\\
	\tilde{y}_{k}-2\hat{y}_{k-1}+\hat{y}_{k-2} \approxeq
		\begin{bmatrix}
			g_{xx}\Delta_T & \Delta_T/m \\
			-g_{xx}r \Delta_T & -r \Delta_T/m -\alpha\Delta_T -\beta
		\end{bmatrix}
	\hat{y}_{k-1}
	-
		\begin{bmatrix}
			0 & 0 \\
			0 & -\beta
		\end{bmatrix}
	\hat{y}_{k-2}
	+ \begin{bmatrix}
			 0\\ \alpha
			\end{bmatrix} \tau_{d}
\\
	\tilde{y}_{k}-2\hat{y}_{k-1}+\hat{y}_{k-2} \approxeq
		\begin{bmatrix}
			\theta_1 & \theta_2 \\
			\theta_3 & \theta_4
		\end{bmatrix}
	\hat{y}_{k-1}
	+
		\begin{bmatrix}
			\theta_5 & 0 \\
			0 & \theta_6
		\end{bmatrix}
	\hat{y}_{k-2}
	+ 	\begin{bmatrix}
	 		0\\ \theta_7
		\end{bmatrix} \tau_{d} \label{regressor_form}
\end{eqnarray}
This model, ultimately in a linearly separable format between the parameters and the model inputs and autoregressive factors, loses some physical insight in the process of converting from continuous to discrete time. Also note that $\theta_5$ was added to represent the potential for joint level damping, which perhaps should have been included in the continuous model anyway. From this point we have the fundamentals of our convex optimization problem:
\begin{equation}
\begin{aligned}
&\underset{\theta}{\text{minimize}} &&\sum_k \| \hat{y}_k - \tilde{y}_k(\theta)\|_q
\end{aligned}
\end{equation}
Potentially with some sensibility constraints, though when you constrain your model to adhere to your demands -- that it be stable, or that a particular constant be positive or negative -- you lose indicators by which you could otherwise test the validity of your final model and your identification experiment. If you have to constrain yourself to stable models in order to identify one then it is quite likely that your experiment wasn't well designed to identify the stability properties of your model.
\begin{equation}
\begin{aligned}
&\underset{\theta}{\text{minimize}} &&\sum_k \| \hat{y}_k - \tilde{y}_k(\theta)\|_q\\
&\text{subject to}&& c_i^\intercal \theta >0, \; i=1,\ldots,m
\end{aligned}
\end{equation}
From here we can introduce slack variables to bring the one-norm problem into a form amenable to cvxpy or cvxgen for larger problems.
\begin{equation}
\begin{aligned}
&\underset{\theta}{\text{minimize}} &&\sum_k j_k\\
&\text{subject to}&& c_i^\intercal \theta >0, \; i=1,\ldots,m\\
&&& j_k\geq \hat{y}_k - \tilde{y}_k(\theta), \; k=1,\ldots,n\\
&&& -j_k\leq \hat{y}_k - \tilde{y}_k(\theta), \; k=1,\ldots,n
\end{aligned}
\end{equation}
We can also introduce a single slack variable to get the infinity norm problem, but my instincts tell me this will be very sensitive to outliers in the data.
\begin{equation}
\begin{aligned}
&\underset{\theta}{\text{minimize}} && j\\
&\text{subject to}&& c_i^\intercal \theta >0, \; i=1,\ldots,m\\
&&& j\geq \hat{y}_k - \tilde{y}_k(\theta), \; k=1,\ldots,n\\
&&& -j\leq \hat{y}_k - \tilde{y}_k(\theta), \; k=1,\ldots,n
\end{aligned}
\end{equation}
With the problem in this form it's basically already solved!


\section{Beginnings of a Software Library}
In order to solve this type of optimization problem I started work on a software library in python which facilitates MIMO identification problems. However, it is always important to begin testing any software library on the simplest possible problems, so my first tests are on noiseless, SISO problems. This is useful to distinguish the effects of incorrect modeling assumptions and noise in the physical world from the mistakes which are all too easy to make when writing complex programs. This test code, as can be seen in Appendix A, generates random sample and hold inputs to a second order SISO system constructed in discrete time state space.
\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{easy_ident.pdf}
\caption{Identification experiment on simulated, noiseless, SISO data -- a necessary first step towards more practical model identification. There are a surprising number of hours to be spent finding all of the subtle off-by-one errors in any implementation of an identification framework.}
\end{figure}

\section{Long Term Goals Based on Convex Optimization}
Model identification is ultimately a necessary first step for any robust control process, however the full problem cannot be viewed in isolation as it is shown here. The real open problem seems to be a formulaic way to arrive at the model best suited for use in robust control algorithms--which is an extremely complex problem, since the robust control algorithms are themselves barely tractable by numerical methods. 

\appendix
\section{Abstract ARX Class: Designed for MIMO identification}
\begin{lstlisting}
import numpy as np 
import cvxpy as cvx
class Abstract_ARX:
	def __init__(self, num_params, num_inputs, num_outputs):
		self.num_params=num_params
		self.num_inputs=num_inputs
		self.num_outputs=num_outputs
		self.params=np.zeros((self.num_params,1))
	def gen_regressor_block(self,y,u):
		""" generates regressor for numpy arrays u and y with 
		shapes [udim,time_elements] and [ydim, time_elements]
		phi has dimension [ydim, self.num_params] and 
		y_estimate = model.gen_phi(u,y).dot(parameters)
		model specific.
		this method basically describes the model. This class is
		just making it easier to think a little more generally
		about the various models."""
		pass
	def gen_estimate(self,y,u):
		""" this just returns the portion of ytilde that is not
		based on paramters. For example, in a second order system
		the terms necessary to express that the model estimates the
		second derivative in terms of first order expansions. """
		pass
	def get_parameter_size(self):
		""" return the self.number of parameters in the model """
		return self.num_params
	def get_input_depth(self):
		""" return the self.number of inputs the model uses """
		return self.num_inputs
	def get_output_depth(self):
		""" return the self.number of outputs the model uses """
		return self.num_outputs
	def filter(self, output_history,inputs):
		""" output_history ~ [ydim, self.times] 
			inputs ~ [udim, self.times-1]
			returns: outputs ~ [ydim, self.times] 
			starts simulating at t=max(self.num_inputs,self.num_outputs)"""
		ys=np.zeros(output_history.shape)
		us=inputs
		t_start=max(self.num_inputs,self.num_outputs)+1
		ys[:,0:t_start]=output_history[:,0:t_start]
		for k in range(t_start,ys.shape[1]):
			ys_block=ys[:,(k-1):(k-1-self.num_outputs):-1]
			us_block=us[:,(k-1):(k-1-self.num_inputs):-1]
			ys[:,k]=self.gen_regressor_block(
				ys_block,us_block).dot(self.params)+self.gen_estimate(ys_block,us_block)
		return ys

	def setup_regressor(self,ys,us):
		""" ys [ysize,self.times] us [usize, self.times-1]"""
		print "hey", ys.shape, us.shape
		self.num=ys.shape[1]
		self.output_size=ys.shape[0]
		self.input_size=us.shape[0]
		self.times=max(self.num_inputs,self.num_outputs)+1
		""" regressor blocks for each vector of outputs. """
		self.regressor=np.zeros(((self.num-self.times)*self.output_size,self.num_params))
		self.ideal_output=np.zeros(((self.num-self.times)*self.output_size,1))
		""" block equation: y - get_est = get_regressor_block * params """
		for i in range (0,self.num-self.times):
			k=self.times+i
			block_start=self.output_size*i 
			block_end=self.output_size*(i+1)
			output_history=ys[:,(k-1):(k-1-self.num_outputs):-1]
			input_history=us[:,(k-1):(k-1-self.num_inputs):-1]
			self.regressor[block_start:block_end,:]=self.gen_regressor_block(
					output_history, input_history)
			self.ideal_output[block_start:block_end,0]=ys[:,k]-self.gen_estimate(
					output_history, input_history)
		return self.regressor , self.ideal_output
	def simple_lstsq(self,ys,us):
		reg, out = self.setup_regressor(ys,us)
		self.params, self.id_residuals, self.id_rank, self.id_singular_values=np.linalg.lstsq(reg,out)
	def filt_lstsq(self,ys,us, conv):
		""" as simple lstsq, but conv [nconv*2+1,] is used to convolve the regressor and ideal output  
			from a theoretical standpoint, conv is the (bidirectional) impulse response of the inverse
			of the expected noise transfer function. """
		reg, out = self.setup_regressor(ys,us)
		nConv=(conv.shape[0]-1)/2
		OnConv = self.output_size*nConv
		new_num =reg.shape[0]+(-conv.shape[0]+1)*self.output_size
		conv.reshape((1,-1)) # transpose, make it a row vector
		f_reg = np.zeros((new_num,self.num_params))
		f_io = np.zeros((new_num,1))
		print reg.shape
		print conv.shape,reg[0:0+self.output_size*conv.shape[1]:self.output_size,:].shape
		for r in range (0,new_num):
			f_reg[r,:]=conv.dot(reg[r:r+self.output_size*conv.shape[1]:self.output_size,:])

		self.params, self.id_residuals, self.id_rank, self.id_singular_values=np.linalg.lstsq(reg,out)
	def simple_1_norm(self,ys,us):
		# TODO
		pass

\end{lstlisting}
\section{Basic Tests on Ideal Data}
\begin{lstlisting}
import matplotlib.pyplot as plt 
import numpy as np 
from linearUtils.discrete_simulation import *
from linearUtils.discrete_plots import *
from linearUtils.convolution import *
import random
import scipy
import control as c
from sysID.arx import Abstract_ARX

num=4000
starting_points=[1000]
tf=100.0
step_response_tf=20.0
dt = tf/num
step_response_points=int(step_response_tf/dt)
A_t=np.array([[0.0,1.0],[-10.0,-1.0]])
intA_t = np.array([[1.0, dt],[-dt, 1.0-0.2*dt]])
B_t=np.array([[0.0],[10.0]])
intB_t = np.array([[0.0],[dt]])
C_t=np.array([[0.1,0.0]])

ref_sys=c.StateSpace(A_t,B_t,C_t,np.zeros((1,1)),0.0)
dref_sys=c.StateSpace(intA_t,intB_t,C_t,np.zeros((1,1)),0.0)
# given y-, y--, u-, u--: yd-- must be knowable, and thus y
# y- -> x- = 10.0*y-
# y-- -> x-- = 10.0*y--
# x- = x-- + dt*xd-- -> xd-- = (x- - x--)/dt
# -> xd- = -dt*x-- + (1-0.2*dt)*xd-- + dt*u-- 
# x = x- + dt*xd- -> y = 0.1*(x-+dt*xd-)
# y = 0.1*(10.0*y- + dt*(-dt*x-- + (1-0.2*dt)*xd-- + dt*u--))
# y = y- -dt*dt*y-- + 0.1*dt*(1-0.2*dt)*xd-- + 0.1*dt*dt*u--
# y = y- -dt*dt*y-- + 0.1*dt*(1-0.2*dt)*(x- - x--)/dt + 0.1*dt*dt*u--
# y = y- -dt*dt*y-- + (1-0.2*dt)*(y- - y--) + 0.1*dt*dt*u--
# y = (2.0-0.2*dt)y- + (-dt*dt*+-1+0.2*dt)y-- + (0.1*dt*dt)*u--

K_t=np.array([[0.0,0.0]])
x0=np.array([[0.0],[1.0]])
def get_x_ref(t):
	return np.array([[0.0],[0.0]])

input_steps_per_second=1.0

input_set=[np.array([[random.random()]]) for i in range(0,int(tf*input_steps_per_second))]
def get_u_ref(t):
	return input_set[int(t*input_steps_per_second)]
ts,x,u,xref,uref,dx,du=simulate_state_feedback_with_reference(
	A_t,B_t,K_t,x0,get_x_ref,get_u_ref,tf=tf,n=num)

def get_u_ref_step(t):
	return np.array([[1.0]])

x0_step=np.array([[0.0],[0.0]])
ts_step, xs_step, us_step, xref_step, uref_step, dx_step, du_step = simulate_state_feedback_with_reference(
	A_t,B_t,K_t,x0_step,get_x_ref,get_u_ref_step,tf=step_response_tf,n=step_response_points)
y_step=C_t.dot(xs_step)
step_padding=step_response_points
y_step_padded=np.concatenate([np.zeros((1,step_padding)),y_step],axis=1)
u_step_padded=np.concatenate([np.zeros((1,step_padding)),us_step],axis=1)
print "geoffrey", u_step_padded.shape, y_step_padded.shape
t_step_padded=np.concatenate([np.zeros((step_padding,)),ts_step])


fig,axs=np_solution_plot(ts,x,u,ncols=3,minrows=2)
axs[1][2]=fig.add_subplot(236,sharex=axs[0][0])
axs[0][2]=fig.add_subplot(233)

output_overlay_axis=axs[1][2]
step_response_axis=axs[0][2]
y=C_t.dot(x)
output_overlay_axis.plot(ts,y[0,:])

step_response_axis.plot(ts_step,y_step[0,:])
step_response_axis.set_xlabel("$t$")
step_response_axis.set_title("step response")
step_response_axis.set_ylabel("$y$")


fir_order=500
### model 1
plot_model1=True #FIR no low pass filter
plot_model2=True #IIR: [y-, y--, y---, u-, u--, u---], works!
plot_model3=False
plot_model4=False #FIR with low pass prefilter
plot_model5=False #IIR: [y-, y--, u--], works with minimal set. Perfect model fit, filtering does nothing.

ys=C_t.dot(x[:,:])
us=u[:,:]

if plot_model1:
	# this model has worked when the system is stable:
	# y= b1 u-, b2 u--, b3 u-- ... 
	class Model1(Abstract_ARX):
		def __init__(self):
			Abstract_ARX.__init__(self,num_params=fir_order, num_outputs=0, num_inputs=fir_order)
		def gen_regressor_block(self,y,u):
			return np.array(u[:,:])
		def gen_estimate(self,y,u):
			return np.array([[0.0]])
	model1=Model1()
	
	
	order=fir_order
	times=fir_order
	phi=np.zeros((num-times,order))
	b=np.zeros((num-times,1))
	for i in range(times,num):
		for z in range(0,times):
			phi[i-times,z]=u[0,i-1-z]
		b[i-times,0]=C_t.dot(x[:,i])
	phi = phi[1:,:]
	b = b[1:,:]
	nConv=100
	sigma_in_time=1.0
	model1.setup_regressor(ys,us)
	sum_diff = 0
	sum_phi =0
	sum_b_diff=0
	for r in range (0,phi.shape[0]):
		for c in range (0,phi.shape[1]):
			sum_diff += abs(phi[r,c]-model1.regressor[r,c])
			sum_phi += abs(phi[r,c])
		sum_b_diff+=abs(b[r,0]-model1.ideal_output[r,0])
	print sum_diff, sum_phi, sum_b_diff

	model,residuals,rank,singular_values=np.linalg.lstsq(phi,b)
	model1.simple_lstsq(ys,us)
	model1_step_ys=model1.filter(y_step_padded,u_step_padded)
	print y_step_padded.shape, t_step_padded.shape,  model1_step_ys.shape
	step_response_axis.plot(t_step_padded,model1_step_ys[0,:])


	for init in starting_points:
		sim_y=np.zeros((num,1))
		for i in range(0,init):
			sim_y[i,0]=C_t.dot(x[:,i])
		sim_u=u.T
		sim_y1=model1.filter(ys[:,init:],us[:,init:])
		output_overlay_axis.plot(ts[init:],sim_y1[0,:])
		for i in range(init,num):
			phi_i=np.zeros((1,order))
			for z in range(0,times):
				phi_i[0,z]=sim_u[i-1-z,0]
			sim_y[i,0]=phi_i.dot(model)
		# output_overlay_axis.plot(ts[init:-1],sim_y[init:-1,0])
		# step_response_axis.plot(ts[0:model.shape[0]],model[:,0])
		# step_response_axis.plot(ts[0:model.shape[0]],model1.params[:,0])


if plot_model2:
	# this model works:
	# y^ = a1 y- + a2 y-- + a3 y--- + b1 u- + b2 u-- + b3 u---
	# order=6

	# nConv=100
	# sigma_in_time=1.0
	# dt=ts[1]-ts[0]
	# conv, diffKernel, ddifKernal = setupConvolutionFilters(
	# 	dt, sigma_in_time, nConv=nConv)

	# times=3
	class Model2(Abstract_ARX):
		def __init__(self):
			Abstract_ARX.__init__(self,num_params=6, num_outputs=3, num_inputs=3)
		def gen_regressor_block(self,y,u):
			return np.concatenate([-y[:,:],u[:,:]],axis=1)
		def gen_estimate(self,y,u):
			return np.array([[0.0]])
	model2=Model2()
	model2.simple_lstsq(ys,us)
	model2_step_ys=model2.filter(y_step_padded,u_step_padded)
	step_response_axis.set_autoscaley_on(False)
	step_response_axis.plot(t_step_padded,model2_step_ys[0,:])

	for init in starting_points:
		sim_y2=model2.filter(ys[:,init:],us[:,init:])
		output_overlay_axis.set_autoscaley_on(False)
		output_overlay_axis.plot(ts[init:],sim_y2[0,:])
\end{lstlisting}
\end{document}
