Reviewer 6 of ICRA 2016 submission 2423

Comments to the author
======================

This paper formulates phase space locomotion planning
techniques to handle multi-contact dynamic locomotion with
a path-following multi-contact point mass model. This
planning process can choose the liftoff and landing times
of each limb. Simulation results demonstrate the
application of this planning process to dynamic
multi-contact gap-jumping and stair-climbing in 2D.

Weakness:
This paper gives an offline planning method, which provides
the CoM trajectory and contact timings.
The application of this idea for online control is
questionable.

Minor questions:
What’s T and F in the binary function valid(S,\xi)?? Page
3, bottom left.

Major questions:
1)	How to design the path to follow and the list of
stances in the beginning? Is it possible that no feasible
stance can be find in the list of stances for a given path?
If this happens, what to do? If the optimization fails to
find feasible solution, how do you modify the path or
modify the list of stances?
2)	 As mentioned in the paper, the limb allocation and
any acknowledgement of limb dynamic limitations are left
out of the problem, this will prevent the propose method
from real application.	In practice, the limb allocation
and kinematic constraints have to be considered during
planning, otherwise, the planning result can’t be applied
to control. 

Recommendations:
1) Discussion about how to apply this to real applications
is expected. 
2) 3D results and experiment results are expected. 


Comments on the Video Attachment
================================

The video is too short. It should show step-by-step the
development of the method.  

--------------------------------------------------------------

Reviewer 7 of ICRA 2016 submission 2423

Comments to the author
======================

The authors develop a phase-space planning algorithm for
robots in a multi-contact world that is aware of dynamic
constraints and allows for optimization based on different
parametrizations. The algorithm is demonstrated on a simple
example in a Python based simulator. While this paper
appears to be striving towards an important and relevant
result, simulation of simple examples and lack of direct
comparison to prior art limit the papers usefulness.

The paper does a nice job of citing previous work and prior
art, but would benefit from a comparison of the presented
algorithm to the performance of the cited works.

The paper does a nice job describing the progression of the
algorithm through a planning problem, but suffers from the
immediate jump into the details. A couple paragraphs
dedicated to the high level approach or flow diagram will
provide a gentler introduction to the reader.

The example simulated in Python is significantly simpler,
than the example used to explain the operation of state
machine in Fig 7. A simulation of a more complete example
where each node in the state machine is included would be
very beneficial to the reader. In addition, a simulation of
an example where the planner fails would be beneficial to
understand the limitations of this algorithm.

Figure 1 when printed or viewed in grayscale is very hard
to understand. I would recommend changing the figure colors
so there is more contrast in grayscale to help with
readability.

The variables used throughout the paper are not described
well or buried in the text. It would be very beneficial to
have one paragraph dedicated to the
variable/convention/parametrization descriptions.

The language and sentence structure throughout a few
sections, especially the introduction/prior art, and
behaviour of the model may severely reduce readability for
non-native english speakers. The reviewer is a native
English speaker and had difficulty at times. Shorter and
simpler sentences may help significantly.

Comments on the Video Attachment
================================

The video is too short. The animation should be slowed
down, captions added, parts annotated to help with
understanding.