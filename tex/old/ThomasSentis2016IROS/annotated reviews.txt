TODO March 1st, order of increasing cost:
	[Simpler sentences]
	[Variables paragraph]
	[High-level overview-kinematics]
	[High-level overview-realization]
	[Compare performance]
	[Failure example]
	[Full featured test case]
	[Robustness w/ LP] - My idea
	[Use 3D simulations]
TODO March 7th:
	[Add video captions]
	[Slow down video]


Reviewer 6 is a pale reflection of reviewer 7, but he brings up two good questions.
	1) How to design the path to follow and the list of
stances in the beginning? Is it possible that no feasible
stance can be find in the list of stances for a given path?
If this happens, what to do? If the optimization fails to
find feasible solution, how do you modify the path or
modify the list of stances? [High-level overview-kinematics]
	2) As mentioned in the paper, the limb allocation and
any acknowledgement of limb dynamic limitations are left
out of the problem, this will prevent the propose method
from real application.	In practice, the limb allocation
and kinematic constraints have to be considered during
planning, otherwise, the planning result can’t be applied
to control. [High-level overview-realization]


Reviewer 7 of ICRA 2016 submission 2423

Comments to the author
======================

The authors develop a
	phase-space planning algorithm for robots in a multi-contact world
that is:
	aware of dynamic constraints
	allows for optimization based on different parametrizations.
The algorithm is demonstrated on a simple example in a Python based simulator.
While this paper appears to be striving towards an important and relevant result,
simulation of simple examples and [Use accurate simulations]
lack of direct comparison to prior art limit the papers usefulness. [Compare performance]

The paper does a nice job of citing previous work and prior
art, but would benefit from a comparison of the presented
algorithm to the performance of the cited works. [Compare performance]

The paper does a nice job describing the progression of the
algorithm through a planning problem, but suffers from the
immediate jump into the details. A couple paragraphs
dedicated to the high level approach or flow diagram will
provide a gentler introduction to the reader. [Explain high-level]

The example simulated in Python is significantly simpler,
than the example used to explain the operation of state
machine in Fig 7. A simulation of a more complete example
where each node in the state machine is included would be
very beneficial to the reader. In addition, a simulation of
an example where the planner fails would be beneficial to
understand the limitations of this algorithm. [Failure example] [Full featured test case]

Figure 1 when printed or viewed in grayscale is very hard
to understand. I would recommend changing the figure colors
so there is more contrast in grayscale to help with
readability. [Alter figure 1]

The variables used throughout the paper are not described
well or buried in the text. It would be very beneficial to
have one paragraph dedicated to the
variable/convention/parametrization descriptions. [Variables paragraph]

The language and sentence structure throughout a few
sections, especially the introduction/prior art, and
behaviour of the model may severely reduce readability for
non-native english speakers. The reviewer is a native
English speaker and had difficulty at times. Shorter and
simpler sentences may help significantly. [Simpler sentences]

Comments on the Video Attachment
================================

The video is too short. The animation should be slowed
down, captions added, parts annotated to help with
understanding. [Add video captions] [Slow down video]