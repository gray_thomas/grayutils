\documentclass[journal]{IEEEtran}

\usepackage[pdftex]{graphicx}
\usepackage[cmex10]{amsmath}
\usepackage{amssymb}
\usepackage{array}
\usepackage[caption=false,font=footnotesize]{subfig}
%% \usepackage{fixltx2e}
%% \usepackage{stfloats}
\usepackage{url}
\usepackage{epstopdf}
\usepackage{multirow}
\usepackage{cite}
\usepackage{soul}
\usepackage{tensor}
\usepackage{booktabs}
\usepackage{layouts}
%% \usepackage{subfigure}

\usepackage{color}
\newcommand{\hilight}[1]{\colorbox{yellow}{#1}}

% user definitions
\def\act{_{\rm act}}
\def\ndofs{{n_{\rm dofs}}}
\def\task{_{\rm task}}

\newcommand\vitae[3]{
 \begin{IEEEbiography}[{%%\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]
   {#2}}]{#1}#3
 \end{IEEEbiography}
 }

\hyphenation{Transactions on Robotics}

\begin{document}
\title{State Estimation for a Point Foot Biped Robot Using the Articulated Body Inertia of the Base Joint}
\date{\today}
\author{Gray~Thomas,
   \and Donghyun~Kim, and
   \and Luis~Sentis
\thanks{All authors are with the Department of Mechanical Engineering, The University of Texas at Austin, Austin TX 78712-0292}
\thanks{G. Thomas, D. Kim, and L. Sentis are with the Human Centered Robotics Laboratory}% <-this % stops a space
}


\markboth{IEEE Transactions on Robotics,~Vol.~XX, No.~YY, \today}%
{Shell \MakeLowercase{\textit{et al.}}: State Estimation}

\maketitle

% As a general rule, do not put math, special symbols or citations
% in the abstract or keywords.
% Less than 200 characters
\begin{abstract}
This is new. Our sensors are proprioceptive joint angle sensors, joint torque sensors (series elastic spring deflection), feature tracking (through a ceiling mounted camera system tracking unique marker LEDs on the robot body---this sensor can be replaced by robot mounted depth cameras tracking unique optical features of the landscape), and an inertial measurement unit.
\end{abstract}

%Linewidth=\printinunitsof{in}\prntlen{\linewidth}\\
%Textwidth=\printinunitsof{in}\prntlen{\textwidth}\\
\section{Introduction}
Much like humans use their eyes to determine their orientation rather than their feet, we have used an overhead motion capture system, which identifies the robot position and orientation, with some non-trivial delay. The nontrivial delay is dealt with explicitly in our estimator. We consider the external sensor looking inwards at the robot to be a proxy for the more realistic case of a robot mounted camera or LIDAR identifying and tracking features in the landscape. In fact, these two processes are mathematically identical, and the orientation estimation accuracy of a robot identifying features is actually more reliable since their geometric moments are significantly larger, being on the order of terrain scale rather than being restricted by the body size of the robot.

Our estimator improves overs several estimators used for the VRC by solving the more difficult problem of state identification when the robot is in an under-constrained kinematic state, i.e. balanced on a point foot. This is comparable to the case where a DRC plate foot atlas robot is balanced on a cylindrical ladder rung, or on a pointy rock or cinder block edge. In these cases the robot's orientation cannot be estimated from the ankle joint measurement. Our robot has no ankles.

Our estimator also avoids the common pitfall of assuming that the feet are stationary when they are in contact with the ground. This may be the case for slow moving robots on well known terrain, but experimentally we have detected several foot slipping events, and our state estimator is designed to report these hybrid events.

\subsection{Pitfall of IMU acceleration reference}
Our estimator also avoids the pitfall of assuming that the inertial measurement unit's built in Kalman filtering algorithm can correctly measure the global orientation of the robot. This is a terrible assumption. The IMU, by obvious physical limitation, cannot measure gravity relative to its reference frame. It measures acceleration. It measures acceleration relative to an inertial reference frame which in being sucked into the center of the planet. What the IMU measures in free-fall is zero acceleration, and when the IMU is on a robot which is walking dynamically it measures the forward and backward acceleration which is an inherent part of the linear inverted pendulum model for robot walking. This means that by trusting the IMU to provide orientation data you are looking at a reference frame which is rocking back and forth as you walk. Our algorithm is aware that an IMU alone cannot identify orientation.


\subsection{Why Hume has series elastic actuators}
The primary advantage of series elastic actuators is that the decoupling between feet and body allows the body to be isolated from ground height uncertainty even when the robot moves blindly over it. This makes series elastic legged robots ideal for mounting cameras, lasers, etc. while walking in human environments or scrambling over difficult terrain.

\subsection{Contact Uncertainty}
Kalman filtering accomplishes little over a simple Leuenburger observer if its ability to represent changing state covariance is not exploited. In this paper we represent contact events as more uncertain than the continuous single support dynamics, and thus we more accurately estimate our robot's state immediately after the contact events. This allows us to use parameters which would cause the steady state Kalman gain to represent low pass filtering time constants---the half-life of belief---far beyond the period of a single step.


\section{Prior Work}
Given the frantic pace humanoid robotics during the Darpa Robotics Challenge, there has been a sudden resurgence of interest in the problem of state estimation for dynamically walking robots.

One team which has published several variants of their state estimation approach hails from Carnegie Mellon. This team has a strange philosophy on the subject, mis-believing that their inertial measurement unit can give them an unbiased estimate of their orientation. In fact, this misconception is only possible because the robot, on average, is not accelerating laterally. If it were, then their IMU's internal Kalman filter would give them an estimate of "up" that was tiled towards the direction of that acceleration.

\cite{Stephens2011ICRA} explores the use of a simple linear inverted pendulum model as the underlying model for a steady state Kalman filter. This papers makes some interesting points, including that a model with both an external force and a center of mass offset is not observable with position, center of pressure, and total COM force measurements.
\cite{XinjilefuAtkeson2012IROS}
\cite{XinjilefuEA2014ICRA}
\cite{XinjilefuFengAtkeson2014IROS}

\subsection{Non-Bipedal Legged robots community}
\subsection{DRC robots with plate feet}
The CMU team has produced much work on the subject, highlighting the separability of dynamics, \cite{XinjilefuAtkeson2012IROS}

\subsection{Rabbit, Atrias, and other point foot bipeds}
\begin{figure*}
\centering
\includegraphics[width=\textwidth]{figures/thoma1}
\end{figure*}

\section{Pose Estimation}
\label{sec:sensing}


$k= t-15$

$\hat y^k_i \in \mathbb{R}^{3}$  

$\tilde y^k_i \in \mathbb{R}^{3}$ 

$\tilde y^k_i = x^k + A^k z^k_i$

$T^k=\{x^k\in \mathbb{R}^{3},A^k\in \mathbb{R}^{3\times3}\}$ 

$z_i \in \mathbb{R}^{3}$

\begin{gather} \label{eqn:y_def}
	\theta^k=
		\begin{pmatrix} 
			x^k\\
			\mathrm{vec}(A^k)
		\end{pmatrix} 
,\qquad
	\tilde {\mathbf{y}}^k = 
		\begin{pmatrix} 
			\tilde y^k_1\\
			\vdots \\
			\tilde y^k_7
		\end{pmatrix} =
	R \theta^k,
\\[3mm]\label{eqn:R_def}
	R = 
		\begin{pmatrix}
			I_{3\times3} & e_0 z_1^\intercal  & e_1 z_1^\intercal & e_2 z_1^\intercal\\
			I_{3\times3} & e_0 z_2^\intercal & e_1 z_2^\intercal & e_2 z_2^\intercal\\
			\vdots &\vdots&\vdots&\vdots \\
			I_{3\times3} & e_0 z_7^\intercal & e_1 z_7^\intercal & e_2 z_7^\intercal \\
		\end{pmatrix}, 
\\[3mm]\label{eqn:theta_simp}
	\theta_{simp}^k \triangleq (R^\intercal R)^{-1}R^\intercal \hat{\mathbf{y}}^k, 
\end{gather}

\begin{equation}\label{eqn:weighting}
	W=
		\begin{pmatrix}
			I_{3n\times3n} & 0 & 0\\
			0 & \lambda_1 I_{3\times3}&0\\
			0 & 0 & \lambda_2 I_{9\times9}
		\end{pmatrix} ,
\end{equation}

\begin{equation}
K_{o} \in \mathbb{R}^{n\times 7}= 
	\begin{pmatrix}
		e_0^\intercal \text{ if LED 1 was found}\\
		e_1^\intercal \text{ if LED 2 was found}\\
		\vdots\\
		e_6^\intercal \text{ if LED 7 was found}
	\end{pmatrix}.
\end{equation}
 
\begin{equation}\label{eqn:R_r}
	R_r \triangleq
		\begin{pmatrix}
			(K_{o} \otimes I_{3\times3}) R \\ I_{12\times12}
		\end{pmatrix}.
\end{equation}

\begin{equation} \label{eqn:reg_lstsq}
	\theta_r^k \triangleq (R_r^\intercal W R_r)^{-1}
	R_r^\intercal W
		\begin{pmatrix}
			(K_{o} \otimes I_{3\times3}) \hat{\mathbf{y}}^k \\
			\tilde \theta({k|k-p})
		\end{pmatrix},
\end{equation}

\begin{equation} \label{eqn:imu_int}
	\tilde \theta({b|a})=\theta_r^{a}+\sum_{t=a}^b
		\begin{pmatrix}
			0_3 \\
			 \mathrm{vec}(\hat{\omega}_{\mathrm{IMU}}^t\times) 
		\end{pmatrix} \Delta t,
\end{equation}

\section{New Pose Estimation Problem}

\subsection{Frames}
\newcommand*{\World}{\protect{\scriptscriptstyle \mathcal{W}}}
%\newcommand*{\World}{W}
\newcommand*{\COM}{\protect{\scriptscriptstyle\mathcal{C}}}
\newcommand*{\Body}{\protect{\scriptscriptstyle\mathcal{B}}}
%\newcommand*{\Body}{B}
\newcommand*{\IMU}{\protect{\scriptscriptstyle\mathcal{I}}}
\newcommand*{\LED}{\protect{\scriptscriptstyle\mathcal{L}}}
\newcommand*{\Camera}{\protect{\scriptscriptstyle\mathcal{M}}}

\newcommand*{\Fww}[1]{{\protect{\tensor*[^{\World\kern -1.2pt}]{#1}{_{\kern -1.2pt \World}}}}}
\newcommand*{\Fw}[1]{{\protect{\tensor*[^{\World \kern -1.2pt }]{#1}{}}}}
\newcommand*{\Fb}[1]{{\protect{\tensor*[^{\Body \kern -1.2pt}]{#1}{}}}}
\newcommand*{\Fbb}[1]{\protect{\tensor*[^{\Body \kern -1.2pt}]{#1}{_{\kern -1.2pt \Body}}}}
\newcommand*{\Fwb}[1]{\protect{\tensor*[^{\World\kern -1.2pt}]{#1}{_{\kern -1.2pt\Body}}}}
\newcommand*{\Fbw}[1]{\protect{\tensor*[^{\Body \kern -1.2pt}]{#1}{_{\kern -1.2pt \World}}}}
\newcommand*{\cirdot}{\mathring}
%\newcommand*{\Tfa}{\boldsymbol T}
\newcommand*{\Tfa}{\boldsymbol X}
\newcommand*{\Tff}{\protect{\boldsymbol X^*}}
\newcommand*{\In}{\boldsymbol I}


\newcommand*{\wXmb}{\Fwb{\Tfa}}
\newcommand*{\wXfb}{\Fwb{\Tff}}
\newcommand*{\bXmw}{\Fbw{\Tfa}}
\newcommand*{\bXfw}{\Fbw{\Tff}}
\newcommand*{\bIb}{\Fbb{\In}}
\newcommand*{\bIdb}{\Fbb{\In}}

\begin{tabular}{c|p{6cm}}
Symbol & Description\\\hline
$\World$ & Elevator Frame---equivalently the frame of the room---accelerating upwards relative to the inertial frame to represent gravity\\
$\Body$ & Arbitrary Body Frame fixed to the Torso\\
$\IMU$ & Frame of the Inertial Measurement Unit\\
$\LED$ & Frame of the LED constellation's Centroid\\
$\Camera$ & Frame of the Camera Measurements\\

\end{tabular}
\subsection{A Note on Accelerometers}
Several other researchers struggle with the misconception that an inertial measurement unit is capable of providing an absolute, unbiased orientation measurement for the torso of a legged robot. IMUs are not magic, and they suffer from the same limitations as the ones used in satellites and airplanes. When an IMU is in freefall, it detects no acceleration. How can it produce an unbiased estimate of attitude while it is in freefall? When an IMU is in orbit around a planet, it has a net acceleration relative to that planet, yet it still detects no acceleration. It is, essentially, freely floating in the inertial reference frame, a reference frame which is sucked towards the center of massive bodies in proportion to their mass over the square of distance. 

When an IMU is in a centrifuge in space, does it know which way is down? No, it can only detect the acceleration due to non-gravitational circular motion, an acceleration towards the center of its rotation axis. Standard IMU software, which assumes the IMU is not meaningfully accelerating relative to gravity, will claim this direction as vertical in such a scenario.

Another important question about IMUs is the power of their rotational rate sensing. When an IMU is attached to a planet, can it detect the angular velocity of that planet? Can you determine the planet's orbit from that measurement? In truth, the gyroscope does feel the rotation of the planet, but it is such a slow rotation that the effect is below the sensor's measurement threshold in most cases. For the VectorNav IMU system the standard deviation of angular rate measurements is ten times as large as the rotational rate of the planet.

\subsection{Differentiation}
To meaningfully write our equations of motion, we must describe all measurements as they relate to the state variables we are using to define our system, specifically the six DOF pose, velocity, and angular velocity of the robot's base with respect to the elevator frame---the world as we know it. This process is complicated by several other frames. The inertial measurement sensor measures the acceleration of its own sensor frame relative to the true inertial frame, which is accelerating relative to the elevator frame. To represent this measurement we will apply the general rule for relative accelerations of reference frames. 
A more complex reference frame problem arises when we compute the differential equation which updates our chosen state variables. Since the robot is moving at all times, the center of mass is not at a constant position in body frame. In fact, perhaps it would be better to avoid thinking about the center of mass at all.

Our model expects certain measurements from the control system and the robot. We ask inputs of the control system to simplify the estimator, and to avoid redundantly calculating the linearized robot dynamics. We expect the controller and its robot dynamics model to provide us with an estimate of the articulated body inertia of the robot, in the base frame. We also expect it to give us an estimate of the rate of change of that inertia. As reaction wrench is the derivative of momentum. We must take this time derivative of inertia into account. 
\begin{gather}
\Fw{\dot m} = \Fw{\mathring m} \\
\Fb{\dot m} = \Fb{\cirdot m} + \Fbb{v} \times \Fb{m} \\
f=\frac{d}{dt}Iv\\
= \dot I v + I \dot v\\
= \sum_{i=1}^{6} (\dot g_i g_i + g_i \dot g_i) v + I a\\
= \sum_{i=1}^{6} \left((\cirdot g_i + v \times^* g_i)g_i+g_i(\cirdot g_i + v \times^* g_i)  \right)v+ I a\\
\cirdot I \triangleq \sum_{i=1}^{6} (\cirdot g_i g_i + g_i \cirdot g_i)\\
f=\cirdot I v + [v \times^*] I v - I [v \times] v + I a\\
f=\cirdot I v + [v \times^*] I v + I a
\end{gather}
This equation is true for coordinate vectors represented in body frame, as follows:
\begin{gather}
\Fb{f}=\Fbb{\cirdot I} \Fb{v} + [\Fb{v} \times^*] \Fbb{I} \Fb{v} + \Fbb{I} \Fb{a}
\end{gather}
\begin{equation}
\Fb{v}=\Fbw{T} \Fw{v} 
\end{equation}
\begin{gather}
\Fw{f}=\Fwb{T} (\Fbb{\cirdot I} \Fbw{T} \Fw{v} + [\Fbw{T} \Fw{v} \times^*] \Fbb{I} \Fbw{T} \Fw{v} \\
 + \Fbb{I} \Fbw{T} \Fw{a})
\end{gather}
\begin{align}
 \Fw{f}=&\Fwb{T} \Fbb{\cirdot I} \Fbw{T} \Fw{v} + \\ &\Fwb{T} [\Fbw{T} \Fw{v} \times^*] \Fbb{I} \Fbw{T} \Fw{v} 
  + \\ &\Fwb{T} \Fbb{I} \Fbw{T} \Fw{a})
\end{align}
We use the following relationships to talk about transforms on inertia.
\begin{align}
{\In}&:\mathcal{A}\mapsto\mathcal{F}\\
\Fwb{\Tfa}&:\Fb{\mathcal{A}}\mapsto\Fw{\mathcal{A}}\\
\Fbw{\Tfa}&:\Fw{\mathcal{A}}\mapsto\Fb{\mathcal{A}}\\
\tensor*[^\World]{\Tfa}{^*_\Body}&:\Fb{\mathcal{F}}\mapsto\Fw{\mathcal{F}}
\end{align}
eventually we arrive at the equations we use to simulate the robot:
\begin{align}
\Fw{a} &=[\wXmb \Fbb{\In} \Fbw{\Tfa} ]^{-1} \Fw{f}\\
&-[\Fwb{\Tfa} \Fbb{\In} \Fbw{\Tfa} ]^{-1} \Fwb{\Tfa} \Fbb{\cirdot {\In}} \Fbw{\Tfa} \Fw{v} \\ 
&-[\Fwb{\Tfa} \Fbb{\In} \Fbw{\Tfa} ]^{-1}\Fwb{\Tfa} [\Fbw{\Tfa} \Fw{v} \times^*] \Fbb{\In} \Fbw{\Tfa} \Fw{v}
\end{align}
We can now modify it to represent the uncertainty in the estimates of inertia and inertial derivative given to the estimator by the controller process:
\begin{gather}
\Fb{\hat f}+\Fb{\tilde f}=\Fbb{\hat{\cirdot \In}} \Fb{v}+\Fbb{\tilde{\cirdot I}} \Fb{v} +\\
 [\Fb{v} \times^*] [\Fbb{\hat I}+\Fbb{\tilde I}] \Fb{v} + \Fbb{\hat I} \Fb{a}+ \Fbb{\tilde I} \Fb{a}\\
\end{gather}
Where the terms $\Fb{\tilde f}$, $\Fbb{\tilde I}$, and $\Fbb{\tilde {\cirdot I}}$ are the foot wrench measurement error, the articulated body inertia error, and the articulated body inertia derivative error, all of which are assumed to come from multivariate Gaussian distributions.

For simplicities sake, we treat the update of each sensor separately. This also helps us internalize the mechanism by which the state estimator deals with the significant delay on the feature tracking sensor. Rather than estimating, and thus remembering, only the most recent state, our estimator stores all state estimates, state covariance estimates, non-feature tracking measurements, and linearized dynamics matrices for several update steps. This historic window must be as long as the longest sensor delay. When this most delayed measurement arrives, the estimator recomputes the current state estimate, beginning with the oldest estimate estimate and updating with sensors in chronological order, recomputing the state updates in between each set of simultaneous measurements.
\appendices

\section{Whole Body Operational Space Control} 
\label{sec:appendice}

\newcommand{\njoints}{{n_{\mathrm{joints}}}}
$q_1,\dots,q_\njoints$

$q_b\in\mathbb{R}^6$ 

$q\in \mathbb{R}^6\oplus\mathbb{R}^{\njoints}=\mathbb{R}^{\ndofs}$,
 
$U \in \mathbb{R}^{\big(n_{\rm dofs}-6\big)\times n_{\rm dofs}}$ 

\begin{equation}
q\act = U \; q,
\end{equation}

$q\act \in \mathbb{R}^{\njoints}$

\begin{equation}
A\ddot q + b + g = U^T \tau_{\rm control}
\label{eq:unconstrained_dynamics}
\end{equation} 

$ \{ z\in\mathbb{R}^{\ndofs}:z^TA^{-1}U^T=0\} $.

$J_s \in \mathbb{R}^{3\times n_{\rm dofs}}$

$J_s \ddot{q} + \dot J_s \dot q = \ddot x_{\rm foot(or\;feet)} = 0$

\begin{equation}\label{eq:lambda-dyn}
A\ddot q + b + g + J_s^T \lambda = U^T \tau_{\rm control},
\end{equation}

\begin{equation}
\begin{pmatrix}
A & J_s^T \\ J_s & 0
\end{pmatrix}
\begin{pmatrix}
\ddot{q} \\ \lambda
\end{pmatrix}
=
\begin{pmatrix}
U^T \tau_{\rm control} -b-g \\ -\dot J_s \dot q
\end{pmatrix}.
\end{equation}

\begin{equation} \label{eq:constraintforces}
\begin{pmatrix}
A & J_s^T \\ 0 & I
\end{pmatrix}
\begin{pmatrix}
\ddot{q} \\ \lambda
\end{pmatrix}
=
\begin{pmatrix}
U^T \tau_{\rm control} -b -g \\[2mm] 
\overline J_s^{\,T} \left[U^T \tau_{\rm control} -b -g\right] +\Lambda_s \dot J_s \dot q\\[1mm]
\end{pmatrix},
\end{equation}

$\Lambda_s \triangleq [J_sA^{-1}J_s^T]^{-1}$

$\overline J_s \triangleq A^{-1} J_s^T \Lambda_s$.

 $N_s^T\triangleq I-J_s^T\Lambda_s J_s A^{-1}$

\begin{equation}\label{eq:constrained-dyn}
A \, \ddot q + N_s^T \left( b + g \right)+J_s^T\Lambda_s \dot J_s \dot q= \left(U N_s \right)^T \tau_{\rm control}.
\end{equation}


\begin{equation}
\overline{X} \triangleq A^{-1}X^T[X A^{-1} X^T]^{-1}
\end{equation} 

\begin{equation}
N_s = I - \overline J_s J_s
\end{equation}

$J_s N_s = 0$, $N_s A^{-1} J_s^T = A^{-1} N_s^T J_s^T = 0$

$N_s N_s = N_s$.  

\begin{equation}
\dot p\task = J\task^* \; \dot q\act,
\end{equation}

$J\task^* \triangleq J\task \overline{UN}_s \in \mathbb{R}^{n\task\times n\act}$ 

$J\task \in \mathbb{R}^{n\task\times \ndofs}$

\begin{equation}\label{eq:task-control}
\tau_{\rm control} = J\task^{*\,T} F\task,
\end{equation}

\begin{equation}\label{eq:task-space-dynamics}
F\task = \Lambda\task^* u\task + \mu\task^* + p\task^*,
\end{equation}

\begin{equation}
\ddot x\task = u\task.
\end{equation}

\begin{equation}
\big( U \, N_s \big)^T \; \tau_{\rm control} = 0,
\end{equation}

\begin{equation}
L^* \triangleq \Big(I-U\, N_s \, \overline{U\,N_s}).
\end{equation}

\begin{equation}\label{eq:int-control}
\tau_{\rm control} = L^{*\,T} \tau_{\rm int},
\end{equation}

\begin{equation}
\tau_{\rm control} = J\task^{*\,T} F\task + L^{*\,T} \tau_{\rm int}. 
\end{equation}

\begin{equation}\label{eq:wbosc:mapping_internal_reaction}
F_{\rm int} = W_{\rm int} \; F_r,
\end{equation}

\begin{equation}\label{eq:wbosc:internal_forces}
F_{\rm int} \, = \, \Big(\overline{J}_{i|l}^{\,*}\Big)^T \, \Gamma_{\rm int} + F_{{\rm int},\{t\}} - \mu_i - p_i,
\end{equation}
$\overline{J}_{i|l}^{\,*} \triangleq \left( L^* U \overline J_s W_{\rm int}^T \right)$

$F_{{\rm int},\{t\}} \triangleq W_{\rm int} \overline J_s^T J\task^{*\,T} F\task$

\begin{equation}\label{fig:int-for}
\Gamma_{\rm int} \, = \; J^{\,*\,T}_{i|l} \Big(F_{\rm int, ref} -  F_{{\rm int},\{t\}} + \, \mu_i + \, p_i\Big),
\end{equation}

%% \section*{Acknowledgment}
\bibliographystyle{../bib/IEEEtran}
\bibliography{../bib/IEEEabrv,../bib/biped_state_estimation.bib}

\end{document}



