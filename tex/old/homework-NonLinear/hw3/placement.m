%% Base
ACTUATOR_RESISTANCE = 1.0  ;% Ohm
ACTUATOR_INDUCTANCE = 0.001  ;% Henry
ACTUATOR_CONSTANT = 60  ;% Newton meters per ampere
ALPHA_K = 0.05  ;% Radians squared
CONSTANT_K = 3.55e8  ;% Newton meter per radian
NOMINAL_ACTUATOR_INERTIA = 1.0  ;% Kilogram meters squared
NOMINAL_FLEXTURE_INERTIA = 1.1e-7  ;% Kilogram meters squared
NOMINAL_BETA = 10.0  ;% Newton meters
FLEXURE_RESISTANCE = 2.0  ;% Ohm
FLEXURE_INDUCTANCE = 0.005  ;% Henry
FLEXURE_CONSTANT = 60.0  ;% Newton meters per ampere
ALPHA_B = 0.01  ;% Radians per second
CONSTANT_B = 11310.0  ;% Newton meter second radian
DISTURBANCE_TO_ACTUATOR_INERTIA = 0.1  ;% Kilogram meter squared
DISTURBANCE_TO_FLEXURE_INERTIA = 0.1e-7  ;% Kilogram meter squared
DISTURBANCE_TO_ACTUATOR = 0.1  ;% Newton meter
DISTURBANCE_TO_FLEXURE = 0.1e-3  ;% Newton meter
DISTURBANCE_TO_BETA = 5.0  ;% Newton meter

Jastar = NOMINAL_ACTUATOR_INERTIA + NOMINAL_FLEXTURE_INERTIA + 0.01^2*0.005;
B= CONSTANT_B;
K = CONSTANT_K;
alpha_k = ALPHA_K;
alpha_b = ALPHA_B;
beta = NOMINAL_BETA;
Ja = NOMINAL_ACTUATOR_INERTIA;
Jf = NOMINAL_FLEXTURE_INERTIA;
Ra = ACTUATOR_RESISTANCE;
La = ACTUATOR_INDUCTANCE;
Rf = FLEXURE_RESISTANCE;
Lf = FLEXURE_INDUCTANCE;
Ka = ACTUATOR_RESISTANCE;
Kf = FLEXURE_CONSTANT;
A = zeros(6,6);
A(1,1)=-Ra/La;
A(4,4)=-Rf/Lf;
A(2,3)=1;
A(5,6)=1;
% w_a dot: {i_a, o_a, w_a, i_f, o_f, w_f}
A(3,1)= Ka/Jastar*(1+Jf/Ja);% wa_a--i_A
A(3,2)= 0;% w_a--o_a
A(3,3)=-100.0; % Damping approximation!
% A(3,3)=-1.0; % Damping approximation!
A(3,4)=-Kf/Jastar;
A(3,5)=Jf/Jastar*K;
A(3,6)=Jf/Jastar*B;
% w_f dot: {i_a, o_a, w_a, i_f, o_f, w_f}
A(6,1)=-Ka/Ja;
A(6,4)= Kf/Jf;
A(6,5)=-K;
A(6,6)=-B;

A_nodamp = A;
A_nodamp(3,3)=0; % Damping approx.
A_bigB = A;
A_bigB(6,6)= A_bigB(6,6) * 100;
A_bigK = A;
A_bigK(6,5)= A_bigK(6,5) * 10;
Bm= zeros(6,2);
Bm(1,1)=1/La;
Bm(4,2)=1/Lf;
% 
% F =[+7.457222727315e-06,+7.457505980007e-01,+7.457255113938e-03,+4.031521174986e-07,+4.876316942160e-09,+8.204056333378e-10;
% +8.063042349973e-07,+9.769266116522e+00,+3.405424623091e-03,+1.454198575630e+01,-6.322543324888e-01,+4.943387826946e-05]


y=[0;1;0;0;1;0]
[F,S,e]=lqr(A,Bm,10*y*y',1e-1*[10,0;0,1]);



DE = eig(A-Bm*F)
ndampE = eig(A_nodamp-Bm*F)
ndampE2 = eig(A_nodamp-0.99999*Bm*F)
BE = eig(A_bigB-Bm*F)
kE = eig(A_bigK-Bm*F)
% Bm



fprintf('[%+.12e,%+.12e,%+.12e,%+.12e,%+.12e,%+.12e;\n%+.12e,%+.12e,%+.12e,%+.12e,%+.12e,%+.12e]\n\n',F')
fprintf('[[%+.12e,%+.12e,%+.12e,%+.12e,%+.12e,%+.12e],\n[%+.12e,%+.12e,%+.12e,%+.12e,%+.12e,%+.12e]]\n\n',F')
fprintf('%+.4e & %+.4e & %+.4e & %+.4e & %+.4e & %+.4e \\\\ \n%+.4e & %+.4e & %+.4e & %+.4e & %+.4e & %+.4e \\\\ \n\n',F')
fprintf('%+.4e & %+.4e \\\\ \n%+.4e & %+.4e \\\\ \n%+.4e & %+.4e \\\\ \n%+.4e & %+.4e \\\\ \n%+.4e & %+.4e \\\\ \n%+.4e & %+.4e \\\\ \n%+.4e & %+.4e \\\\ \n\n',F)


C = [
    0,1,0,0,0,0;
    0,0,0,0,1,0
    1,0,0,0,0,0;
    0,0,0,1,0,0;
    ]
[F,S,e]=lqr(A',C',eye(6),1e-5*eye(4));
F
fprintf('[[%+.12e,%+.12e,%+.12e,%+.12e,%+.12e,%+.12e],\n[%+.12e,%+.12e,%+.12e,%+.12e,%+.12e,%+.12e],\n[%+.12e,%+.12e,%+.12e,%+.12e,%+.12e,%+.12e],\n[%+.12e,%+.12e,%+.12e,%+.12e,%+.12e,%+.12e]]\n\n',F')

%% Simple
As = zeros(3,3); % simple
As(1,1)=-Rf/Lf;
As(2,3)=1;
As(3,1)= Kf/Jf;
As(3,2)=-K;
As(3,3)=-B;
Bs = zeros(3,1);
Bs(1,1)=1/Lf;


As, Bs

%%
y=[0;1;0;0;1;0]
C=[1,0,0,0,0,0;0,1,0,0,0,0;0,0,0,1,0,0;0,0,0,0,1,0]
[L,S,e]=lqr((A-Bm*F)',C',eye(6),1e-10*eye(4));
e
L

%% 
eig([
	-1.3074e+03, -2.7125e-04, -9.6136e+07, -5.9784e+01, -5.9622e+05, -2.1552e-01;
	+0.0000e+00, +0.0000e+00, +1.0000e+00, -6.0000e-11, +0.0000e+00, +0.0000e+00;
	+1.0000e+00, +0.0000e+00, +0.0000e+00, -6.0000e+01, +3.9050e+01, +1.2441e-03;
	+2.0280e+07, +1.0317e+04, -1.3192e+07, -6.8548e+03, -3.8914e+02, +4.4400e-01;
	+0.0000e+00, +0.0000e+00, +0.0000e+00, +5.4545e-04, +0.0000e+00, +1.0000e+00;
	-1.0000e+00, +0.0000e+00, +0.0000e+00, +5.4545e+08, -3.5500e+08, -1.1310e+04
])

Ap = [
	-1.0000e+03, +0.0000e+00, +0.0000e+00, +0.0000e+00, +0.0000e+00, +0.0000e+00;
	+0.0000e+00, +0.0000e+00, +1.0000e+00, -6.0000e-11, +0.0000e+00, +0.0000e+00;
	+1.0000e+00, +0.0000e+00, +0.0000e+00, -6.0000e+01, +3.9050e+01, +1.2441e-03;
	+0.0000e+00, +0.0000e+00, +0.0000e+00, -4.0000e+02, +0.0000e+00, +0.0000e+00;
	+0.0000e+00, +0.0000e+00, +0.0000e+00, +5.4545e-04, +0.0000e+00, +1.0000e+00;
	-1.0000e+00, +0.0000e+00, +0.0000e+00, +5.4545e+08, -3.5500e+08, -1.1310e+04
]
eig(Ap)