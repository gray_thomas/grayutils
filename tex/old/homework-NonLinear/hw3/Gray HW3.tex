\documentclass[journal]{IEEEtran}
%\usepackage{cite}

\usepackage{placeins}
\usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
\graphicspath{{./pdf/}{./jpeg/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpeg,.png}

\usepackage[cmex10]{amsmath}
\usepackage{bm,upgreek}
\usepackage{amsfonts}
\usepackage{wasysym}
%\usepackage{amsthm}
\interdisplaylinepenalty=2500
\newtheorem{theorem}{Theorem}[section]
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\renewcommand{\IEEEQED}{\IEEEQEDopen}

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\sinc}{sinc}
\usepackage{algorithmic}

\usepackage{array}

\usepackage{fixltx2e}

\usepackage{url}

\hyphenation{op-tical net-works semi-conduc-tor}

\begin{document}

\title{Observability, Controllability, Linear Control, Feedback Linearization \& Sliding Mode Control:\\Nonlinear Control Systems HW \#3}



% author names and affiliations
% use a multiple column layout for up to three different
% affiliations
\author{\IEEEauthorblockN{Gray Thomas}
}
\IEEEoverridecommandlockouts

\maketitle


\IEEEpeerreviewmaketitle



\section{}
\begin{itemize}

\item Analysis of the flexure system shows that near the origin it has two stable exponential dynamics, one at 3.14e4 Hz, and one at 1.03e+11 Hz. (Note that this differs from what one might expect looking at constants $K$ and $B$, because the effective constants at the origin are in fact $K/J_F$ and $B/J_F$, with $J_F$ being absolutely miniscule.)
\begin{align}\dot
{\begin{pmatrix}
\theta_F\\\omega_F
\end{pmatrix}} = \begin{pmatrix}
0 & 1 \\ -K/J_F & -B/J_F
\end{pmatrix}\begin{pmatrix}
\theta_F\\\omega_F
\end{pmatrix}+\begin{pmatrix}0\\F\end{pmatrix}\\
F = k_F i_F/J_F - k_A i_A / J_A + d_F/J_F
\end{align}
\item Analysis of the electrical dynamics of the flexure motor also reveals that, first, the back emf has been conveniently neglected in the official ``Word of Benito'' equations, decoupling the speed of the flexure from influencing the current. Second, the dynamics of the electrical system are slow! They have a pole at 400 Hz. This is an audio frequency, as compared to the fundamental frequencies in the flexure system itself, which (supposing the flexure was mildly charged) would produce \textit{microwave} radiation. 
\item Keeping in mind that $t_s\leq 4$ seconds with $e_{ss}\leq 1\%$, I suspect the dynamics of the flexture were not intended to be so wildly, wildly fast. Thus \textbf{I will assume henceforth that $K$ and $B$ were not meant to be divided by the flexure inertia afterall!} 
\begin{figure*}
\begin{align}
\dot {i_F} &=\frac{V_F}{L_F} - \frac{R_F}{L_F}i_F\\
\dot{ i_A} &=\frac{V_A}{L_A} - \frac{R_A}{L_A}i_A\\
\dot \omega_F & = \frac 1 {J_F} \left(k_F i_F+d_F\right)-\frac{k_Ai_A}{J_A} - B (1+\alpha_b |\omega_F|)\omega_F - K (1+\alpha_k \theta_F^2)\theta_F\\
\dot \omega_A & = \frac{1}{J_A^*}\left(k_A i_A - \beta \sign(\omega_A)+d_A - (k_Fi_F+d_F)\right)+\frac{J_F}{J_A^*}\left( \frac{k_Ai_A}{J_A} + B (1+\alpha_b |\omega_F|)\omega_F + K (1+\alpha_k \theta_F^2)\theta_F\right)
\end{align}
\caption{Adjusted equations of motion}
\end{figure*}
This results in oscillatory behavior, and eigenvalues near the origin of $\lambda = -5.66e3\pm1.80e4j$ Hz. This is still faster than the dynamics of the electronics, but it is still far lower than 1e11 Hz, and the condition number for the simulation process will again return to the realm of the tractable. Another possible appraoch would be to solve for equilibrium behavior in the spring-damper system and assume equilibrium was constantly maintained, reducing the number of simulated states. But this appraoch seems to bypass the point of a non-linear controls class, so I opted to adjust the parameters instead. A round of system identification would clearly remove this problem in a practical case.
\item Fig.~\ref{fig:uncontrolled flexure} demonstrates the dynamics of the flexure, given my assumptions.
\item Fig.~\ref{fig:uncontrolled arm} demonstrates the arm dynamics.

\item I will also assume that the mass of the flexure is five grams, and that the distance from the arm to the flexure is a centimeter.

\item To see that the system is controllable, consider the following derivatives of the output
\begin{align}
y &= \theta_A + \theta _F\\
\dot y &= \omega_A + \omega_F\\
\ddot y &= \frac 1 {J^*_A}k_A i_A - \frac 1 {J^*_A}\beta \sign(\omega_A)\\
&+\frac 1{J_F}k_Fi_F-\frac 1{J_F}\frac{J_F}{J_A}k_Ai_A \\&- B (1+\alpha|\omega_F|)\omega_F)\\&-K(1+\alpha_k|\theta_F|^2)\theta_F \\&+\frac 1{J_F}d_F+\frac1{J_A}d_A\\
\dddot y &= 
 \left(\frac 1 {J^*_A}-\frac 1{J_A}\right) \frac{k_A}{L_A}(V_A-R_A i_A) \\&
-\frac 1 {J^*_A}\beta \delta(\omega_A)\dot \omega_A\\&
+\frac 1{J_F L_F} k_F (V_F-R_F i_F)\\&
-B (1+2\alpha|\omega_F|)\dot \omega_F)\\&
-K(1+3\alpha_k\theta_F^2)\omega_F \\&
+\frac 1{J_F}\dot d_F
+\frac 1{J_A}\dot d_A
\end{align}
which demonstrate that $y$ has relative order 3 with respect to the control voltages. Unfortunately, this output has relative order 2 with respect to the disturbances---we will not be able to guarantee disturbance rejection. However meaningful control is still possible because it is highly doubtful that the disturbances cannot be bounded in derivative---that is, the disturbances are physical and therefore of finite energy.
\item If we control the output we will be left with the dynamics of the reduced order manifold (ROM). Let $\bm{\uppsi}=(\psi_1,\psi_2,\psi_3)^T$ be defined
\begin{align}
& \psi_1 && = \theta_A-\theta_F\\
\psi_2 & = \dot {\psi_1} && = \omega_A-\omega_F\\
\psi_3 & = \dot \psi_2 && = 
\frac 1 {J^*_A}k_A i_A 
- \frac 1 {J^*_A}\beta \sign(\omega_A)\\&&&
-\frac 1{J_F}k_Fi_F
+\frac 1{J_F}\frac{J_F}{J_A}k_Ai_A \\&&&
+B (1+\alpha_b|\omega_F|)\omega_F)\\&&&
+K(1+\alpha_k|\theta_F|^2)\theta_F \\&&&
-\frac 1{J_F}d_F
+\frac1{J_A}d_A\\
& \dot \psi_3 &&= 
 \left(\frac 1 {J^*_A}+\frac 1{J_A}\right) \frac{k_A}{L_A}(V_A-R_A i_A) \\&&&
-\frac 1 {J^*_A}\beta \delta(\omega_A)\dot \omega_A\\&&&
-\frac 1{J_F L_F} k_F (V_F-R_F i_F)\\&&&
+B (1+2\alpha_b|\omega_F|)\dot \omega_F\\&&&
+K(1+3\alpha_k\theta_F^2)\omega_F \\&&&
-\frac 1{J_F}\dot d_F
+\frac 1{J_A}\dot d_A
\end{align}
This means the reduced order manifold is also of relative order three with respect to a linear combination of the input voltages. In fact, these linear combinations are linearly independent, and we can control the output and the reduced order manifold independently. Again, however, the relative order with respect to disturbances is only two.
\item To demonstrate observability in the ideal sense it is sufficient to see that both the output $y$ and the reduced order manifold's $\xi$ variable can be constructed from the set of measurements. However the direct measurement of current is significant, since the low relative order of the angle measurements with respect to the disturbances will prevent a differentiation strategy from reconstructing the currents accurately. 


\end{itemize}
\begin{figure}
\centering
\includegraphics[width=\columnwidth]{figs/HDNCflexure}
\includegraphics[width=\columnwidth]{figs/HDNCflexuret}
\includegraphics[width=\columnwidth]{figs/HDNCflexure2}
\includegraphics[width=\columnwidth]{figs/HDNCflexure2t}
\caption{Using my alternate interpretation of the equations, the uncontrolled flexure system is not so stiff, and shows interesting non-linear behavior based on it's non-linear damping and stiffness rules. The first two plots show a wider angle range in which the non-linear effects are more obvious.} \label{fig:uncontrolled flexure}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\columnwidth]{figs/HDNCarm}
\includegraphics[width=\columnwidth]{figs/HDNCarmt}
\caption{The dynamics of the arm subsystem indicate the presence of dry friction, capturing the arm at an arbitrary position.}\label{fig:uncontrolled arm}
\end{figure}



When considering systems with multiple sliding mode-like elements---legitimate SM controllers, friction, potentially SM concepts used to get rid of stiff dynamics---the fundamental unit of a sliding mode controller is brought to the fore. I have dubbed as ``BiChatterer'' any simulation component which comprises \begin{enumerate}\item a sliding surface definition function $\sigma(x)$, \item a jacobian relating the derivative of that function to the derivative of the vector state, \item a ``dynamics-influencing argument'', a boolean argument to the dynamics equation which generalizes the idea of adding $\pm\eta$ somewhere, \item an internal variable called the "tristate" which reflects True when $\sigma(x)>0$, False when $\sigma(x)<0$, and None when $\sigma(x)=0$, \item a hybrid dynamical state machine with the tristate enumerating the discrete modes, comprising four hybrid guards: pos, which maps False to True or None; neg, which maps True to False or None; escape\_pos, mapping None to True; and escape\_neg, mapping None to False, \item a new dynamic equation for each tristate, which in the True or False tristates sets the dynamics-influence arguments according to the value of the tristate, but which in the None tristate combines a weighted average of the two standard options in order to guarantee that the time derivative $\dot \sigma=0.$\end{enumerate}
In my model, therefore, the system without friction or control is defined first, then the friction is a bichatterer of the original system, then the control system is a bichatterer of the friction. Any additional sliding mode controllers are then nested as bichatterers in the same way.

\section{}
Consider the normal form,
\begin{align}
\bm{\upmu}&=\begin{pmatrix} y & \dot y & \ddot y \end{pmatrix}^T\\
\dot{\bm{\upmu}}&=\begin{pmatrix}\dot y\\ \ddot y\\
a(\bm \upmu, \bm \uppsi)+ \bm b(\bm \upmu, \bm \uppsi)^T\bm u \end{pmatrix},
\end{align}
using the reduced order manifold as defined before---
\begin{align}
\dot{\bf \uppsi} &= \begin{pmatrix}
\psi_2\\ \psi_3 \\ w(\bm \upmu, \bm \uppsi)+ \bm g(\bm \upmu, \bm \uppsi)^T\bm u 
\end{pmatrix}.
\end{align}
Note that 
\begin{align}
\theta_A &= \frac 1 2 (y+\psi_1)\\
\theta_F &= \frac 1 2 (y-\psi_1)\\
\omega_A &= \frac 1 2 (\dot y + \psi_2)\\
\omega_F &= \frac 1 2 (\dot y - \psi_2)
\end{align}
which is a necessary first step to finding an expression for current in a reasonable about of algebra:
\begin{align}
\ddot y &= (\frac 1 {J^*_A}-\frac 1{J_A})k_Ai_A - \frac 1 {J^*_A}\beta \sign(\dot y + \psi_2)\\
&- B (1+\alpha|\frac 1 2 (\dot y - \psi_2)|)\frac 1 2 (\dot y - \psi_2))\\
&-K(1+\alpha_k|\frac 1 2 (y-\psi_1)|^2)\frac 1 2 (y-\psi_1) \\
&+\frac 1{J_F}d_F+\frac1{J_A}d_A+\frac 1{J_F}k_Fi_F \\
\psi_3 & = (\frac 1 {J^*_A}+\frac 1{J_A})k_A i_A - \frac 1 {J^*_A}\beta \sign(\dot y + \psi_2)\\&
+B (1+\alpha|\frac 1 2 (\dot y - \psi_2)|)\frac 1 2 (\dot y - \psi_2))\\&
+K(1+\alpha_k| \frac 1 2 (y-\psi_1)|^2) \frac 1 2 (y-\psi_1) \\&
-\frac 1{J_F}d_F +\frac1{J_A}d_A -\frac 1{J_F}k_Fi_F\\
\ddot y + \psi_3 & = \frac 2 {J^*_A}(k_Ai_A-\beta \sign(\dot y + \psi_2))+\frac 2 {J_A} d_A\\
\ddot y - \psi_3 & = -\frac 2 {J_A}k_A i_A +2 \frac{d_F}{J_F} + 2 \frac {k_F}{J_F}i_F\\
&- 2 B (1+\alpha|\frac 1 2 (\dot y - \psi_2)|)\frac 1 2 (\dot y - \psi_2))\\
&-2 K(1+\alpha_k| \frac 1 2 (y-\psi_1)|^2) \frac 1 2 (y-\psi_1)
\end{align}
\begin{align}
i_A & = \frac{J^*_A}{2k_A}(\ddot y + \psi_3 +\frac 2 {J^*_A}(\beta \sign(\dot y + \psi_2))-\frac 2 {J_A} d_A)\\
i_F & =\frac{-J_F}{2 k_F}\Big(-\ddot y + \psi_3 -\frac 2 {J_A}k_A i_A +2 \frac{d_F}{J_F}\\
&- 2 B (1+\alpha|\frac 1 2 (\dot y - \psi_2)|)\frac 1 2 (\dot y - \psi_2))\\
&-2 K(1+\alpha_k| \frac 1 2 (y-\psi_1)|^2) \frac 1 2 (y-\psi_1)\Big)
\end{align}
Which implies that these following two equations:
\begin{align}
\dddot y &= 
 \left(\frac 1 {J^*_A}-\frac 1{J_A}\right) \frac{k_A}{L_A}V_A\\&
- \left(\frac 1 {J^*_A}-\frac 1{J_A}\right) \frac{k_A}{L_A} R_A i_A \\&
-\frac 1 {J^*_A}\beta \delta(\omega_A)\dot \omega_A\\&
+\frac 1{J_F L_F} k_F V_F\\&
-\frac 1{J_F L_F} k_F R_F i_F\\&
-B (1+2\alpha|\omega_F|)\dot \omega_F\\&
-K(1+3\alpha_k\theta_F^2)\omega_F \\&
+\frac 1{J_F}\dot d_F
+\frac 1{J_A}\dot d_A,
\end{align}
and
\begin{align}
\dot \psi_3 &= 
 \left(\frac 1 {J^*_A}+\frac 1{J_A}\right) \frac{k_A}{L_A}V_A\\&
-\left(\frac 1 {J^*_A}+\frac 1{J_A}\right) \frac{k_A}{L_A}R_A i_A \\&
-\frac 1 {J^*_A}\beta \delta(\omega_A)\dot \omega_A\\&
-\frac 1{J_F L_F} k_F V_F\\&
-\frac 1{J_F L_F} k_F R_F i_F\\&
+B (1+2\alpha|\omega_F|)\dot \omega_F\\&
+K(1+3\alpha_k\theta_F^2)\omega_F \\&
-\frac 1{J_F}\dot d_F
+\frac 1{J_A}\dot d_A,
\end{align}
are in terms of quantities which can be computed from the new states, inputs, and disturbances.

Note that there is an input left over to completely control the zero dynamics (or, we have two outputs and a  zero-dimensional zero dynamic which is trivially stable.) Thus we can easily design the zero dynamic to be stable.

\section{}
Differentiating (linearizing) at the origin (and exploiting the fact that different desired positions can be accomplished by moving the arm and re-using the original linearization), additionally, note that since friction exists in the arm system, the equilibrium will naturally occur when the arm is stuck via friction---essentially constraining the system. So we can linearize for this constrained case but doing so comes at cost. Usually a linearization is valid in a local neighborhood of the operating point, but our constrained linearization is only valid for a reduced dimension local neighborhood: the intersection of the original neighborhood and the hyper plane in which $\omega_A$ is zero! In this scenario we accept that the fixed position of the arm is close enough, and only need to design a controller for the flexure. This flexure control is quite simple:
\begin{align}
\frac d {dt}
\begin{bmatrix}
i_F\\\theta_F\\\omega_F
\end{bmatrix} &=
\left(\begin{bmatrix}
-\frac{R_F}{L_F} & 0 & 0 \\
 0 & 0 & 1 \\
\frac{k_F}{J_F} & -K & -B \\
\end{bmatrix} +\begin{bmatrix}
 \frac 1 {L_F} \\
0 \\
0 
\end{bmatrix} F \right) \begin{bmatrix}
i_F\\\theta_F\\\omega_F
\end{bmatrix} \nonumber\\
F &= \begin{bmatrix}
-52 & 32 & -0.0022
\end{bmatrix}
\end{align}
This controller's behavior is shown in Fig.~\ref{fig:C1}
\begin{figure}
\includegraphics[width=\columnwidth]{figs/HD_C1}
\caption{The simple control stabilizes the flexture, but assumes the arm starts at rest and stays at rest due to unbreakable friction. This model is not great---friction is broken easily!}\label{fig:C1}
\end{figure}
However initial conditions with non-zero armature current can easily push the arm off of the hyperplane. In addition, the coupling between the flexure's motion and the armature can easily break the friction when the flexture is under feedback control---thus this controller's neighborhood is extremely small. A linear controller which treats friction as though it was merely heavy damping can solve this problem. Using $-\beta^\prime = -100$ to approximate $\frac{\partial}{\partial \omega_A}[ -\beta \sign(\omega_A)/J_A^*]$ we find the following full state feedback control matrix:
\begin{align}
F^T=\begin{pmatrix}
+2.0341e-01 & +9.2744e+04 \\ 
 +4.6661e+02 & +1.0722e+05 \\ 
-6.9734e+04 & -2.0167e+00 \\ 
-3.0950e-07 & -7.6971e-02 \\ 
-3.1330e-04 & -5.1567e+01 \\ 
+3.2261e+01 & -2.2204e-03 \\ 
\end{pmatrix}
\end{align}
Controllers tuned to the model with no friction and those tuned to a model where damping replaces friction work only very close to the origin (Figs.~\ref{fig:C3}, \ref{fig:C4}, \ref{fig:C5}).
It is difficult to chose the poles so that this controller happens to be stable when any of the approximation terms (for arm friction, flexure spring and damping) change. Another direction is to chose the gain matrix to solve an algebraic riccati equation, a LQR solution. Using the LQR calculator it is easy to design a controller which moves the arm slowly but the flexure quickly. This is shown in Fig.~\ref{fig:C8}. This controller design method has less of a tendancy to cancel poles, so it works a lot better when non-linearity is added, however when the system operates outside of the linear region there is an error which lasts a long time (until the arm gets into place---too long to simulate, since the controlled dynamics are so fast).  
\begin{figure}
\includegraphics[width=\columnwidth]{figs/HD_C3}
\caption{The zero-friction-based linear control stabilizes the system even when its linear model is a little sketchy---a system using the linear model is shown in midnight blue for comparison.}\label{fig:C3}
\end{figure}

\begin{figure}
\includegraphics[width=\columnwidth]{figs/HD_C4}
\caption{When friction is added back into the system the zero-friction-based linear control is not able to handle the fact that the arm angle gets stuck.}\label{fig:C4}
\end{figure}

\begin{figure}
\includegraphics[width=\columnwidth]{figs/HD_C5}
\caption{When the approx-friction controller is used, it fights against the friction (still it is off in the end, when the friction locks the arm in place), but care must be taken to ensure that the controller is stable for both the model with the linear damping approximation and the linear approximation model without any friction, or the controller will be wildly unstable when the system is far from the operating point. The non-linearity of the spring and damper also seems to cause instabiliy---but only for larger initial conditions.}\label{fig:C5}
\end{figure}

\begin{figure*}
\includegraphics[width=\columnwidth]{figs/HD_C8} \includegraphics[width=\columnwidth]{figs/HD_C9}
\begin{align}
F^T=\begin{bmatrix}
+4.0817e-06 & +8.2551e-07 \\ 
+4.0818e-01 & +9.9163e+00 \\ 
+4.0817e-03 & +3.4710e-03 \\ 
+4.1275e-07 & +1.4458e+01 \\ 
+4.9308e-09 & -6.2722e-01 \\ 
+4.4910e-10 & +4.8927e-05 \\
\end{bmatrix}
\end{align}
\caption{Goal position $y=3$ on the left, $y=0.03$ on the right. LQR based linear controller. Linear controllers naturally suffer from steady state error when they operate on non-linear plants. The goal position on the right is closer to the origin, so it is almost perfectly reached.}\label{fig:C8}
\end{figure*}

\FloatBarrier
\section{}
In order to design an ELO which can observe despite the disturbance, we must make sure not to differentiate this disturbance. However, there is a trivial solution given the presentation of the model with no sensor error! The uncorrupted portion of the dynamics of this system are sufficient to construct the unmeasured states, and by construct I mean differentiate the angle states to get angular velocity states. The existing control continues to work with this estimator  (Fig.~\ref{fig:E2}). 

However, this filter is difficult to simulate for longer time periods. This sensativity to integration error makes it unsuitable for real systems. 

Using the companion form, and some idealized third order butterworth filter dynamics, we can construct an ELO (Fig.~\ref{ELO}), shown with a sliding mode controller.

A simple and effective filter, based on the normal form, uses a fourth order butterworth filter to estimate high order derivatives of the output and ROM variables. This is easy to simulate, and performs well because, as specified, there is no white noise in the output signal. Real estimators are constrained by the magnitude of this noise and the amount of noise which can be acceptable in the output. Using the extra level of integration makes the second derivative signal a little cleaner.

One might wonder how these two different methods of cleaning a signal differ: low pass differentiating and using dynamics. Is there a simple adjustment to the butterworth filter which allows it to take advantage of a prediction of the future? I do know that the optimal control framework I use to compute the gains can, in the extreme, completely distrust one component of the update equation. When this is the case, I remember from Applied Intelligence, the shape of the two sided impulse response looks a lot like the shape of a gaussian, and a gaussian here is somewhat like the butterworth filter in that it is simple and bandwidth-cuttoff-like. A gaussian is like a wavelet, in this usage. So maybe you can go one way, making an ELO behave like a butterworth filter. Perhaps there is an elegant way to go the other direction?

\begin{figure}
\includegraphics[width=\columnwidth]{figs/HD8_1}
\caption{ELO performance (with the SMC from the last problem. I came back to this one.) Ultimately I used optimal control on the linear model to derive an appropriate gain matrix, and then I applied that gain matrix's updates to the non-linear model. Ten thousand times the recommended amount of noise has been added, and the parameters used by the estimator and controller are not the ones used in the simulation.}\label{ELO}
\end{figure}

\begin{figure*}
\includegraphics[width=\columnwidth]{figs/E_HD_2} \includegraphics[width=\columnwidth]{figs/E_HD_3}
\caption{Differention based estimation, filter cutoff at 10 KHz. Control uses state estimate in place of state feedback in the right figure---resulting in a drastic reduction in transient magnitude at the third derivative level! Input, sadly, does spike initially.}\label{fig:E2}
\end{figure*}
\FloatBarrier
\section{}
FBL is basically superimposing our desired dynamics in the same way WBC imposes impedance dynamics in cartesian space. Since we have already calculated the third derivative of our output and the conveniently-output-like ROM which can be independantly controlled, we can define our desired dynamics and then immediately calculate a control which enforces them. To enforce a butterworth filter like dynamic with a very fast cutoff after a desired frequency, I have defined the desired dynamics,
\begin{align}
\dddot y^{\mathrm{des}} & = \dddot{x}-2k_y(\ddot y - \ddot x) - 2 k_y^2 (\dot y - \dot x) - k_y^3(y-x) \\
\dddot \psi^{\mathrm{des}} & = -2k_\psi\ddot \psi - 2 k_\psi^2 \dot \psi - k_\psi^3\psi.
\end{align}
If we make the bandwidth of the first butterworth-filter-like-behavior much higher than that of the second, then I hypothesize that the interference of disturbances will upset the $y$ dynamic very little. To simplify the algebra, consider the following linear transform of the input voltages:
\begin{align}
u_y =  \left(\frac 1 {J^*_A}-\frac 1{J_A}\right) \frac{k_A}{L_A}V_A
+\frac 1{J_F L_F} k_F V_F\\
u_\psi =  \left(\frac 1 {J^*_A}+\frac 1{J_A}\right) \frac{k_A}{L_A}V_A
-\frac 1{J_F L_F} k_F V_F.
\end{align}
This linear relationship can easily be inverted as a matrix numerically to recover the input voltages. Finally, the feedback linearizing control can be expressed
\begin{align}
u_y& =\dddot y^{\mathrm{des}} 
+\frac 1 {J^*_A}\beta \delta(\omega_A)\dot \omega_A\\&
+ \left(\frac 1 {J^*_A}-\frac 1{J_A}\right) \frac{k_A}{L_A} R_A i_A 
+\frac 1{J_F L_F} k_F R_F i_F\\&
+B (1+2\alpha|\omega_F|)\dot \omega_F
+K(1+3\alpha_k\theta_F^2)\omega_F \\
 u_\psi & = \dddot \psi ^{\mathrm{des}}
+\left(\frac 1 {J^*_A}+\frac 1{J_A}\right) \frac{k_A}{L_A}R_A i_A \\&
+\frac 1 {J^*_A}\beta \delta(\omega_A)\dot \omega_A 
+\frac 1{J_F L_F} k_F R_F i_F\\&
-B (1+2\alpha_b|\omega_F|)\dot \omega_F 
-K(1+3\alpha_k\theta_F^2)\omega_F.
\end{align}
Certain values of observer poles and desired closed loop dynamics will cause instability, since the controller must use the observed data rather than the state. Through trial and error, I've concluded that the characteristic polynomial is stable when $l / k \gg 5.1 $. This polynomial and the graphical result of approaching this limit is shown in Fig.~\ref{fig:no_hd}.
\begin{figure*}
\centering
\includegraphics[width=1.4\columnwidth]{figs/FBL_no_HD_2}
\begin{equation}
\frac{Y}{X}=\frac{\left( \left(\frac s l\right) ^2+0.61 \left(\frac s l\right)+1 \right)\left( \left(\frac s l\right) ^2+1.61 \left(\frac s l\right)+1 \right) \left( \left(\frac s k\right)^3+2 \left(\frac s k\right)^2 + 2\left(\frac s k\right) +1\right) }{\left(\left(\frac s k\right) ^3 \left( \left(\frac s l\right) ^2+0.61 \left(\frac s l\right)+1 \right)\left( \left(\frac s l\right) ^2+1.61 \left(\frac s l\right)+1 \right) +2 \left(\frac s k\right)^2 + 2\left(\frac s k\right) +1\right)}
\end{equation}
\caption{Desired controller behavior with simulated estimator dynamics. Run in isoloation---a purely theoretical result, not a simulation of the hard-drive dynamics. This shows the effect of chosing an estimator frequency. It is assumed that the estimator is a 4th order butterworth filter/differentiator of the observable output $y$, and that the desired behavior replicates a 3rd order butterworth response. Desired behavior at 5 KHz, goal filtered at 6 KHz (another 4th order butterworth), estimator at 40 KHz (black), 27 KHz (blue), 26 KHz (aquamarine), and 25.85 KHz (neon green) in the four experiments respectively. Input signal designed to make the plot look cool: 0 if t$\in[0,0.01)$, 1 in $t\in[0.01,0.02)$, $-1+100(t-0.02)$ if $t\in[0.02,0.03)$, and $\sin(100(t-0.03))$ if $t\in[0.03,\infty)$. Initial condition begins with $\theta_A=-1$. ROM dynamics were boring and stable, since the ROM is controlled at a lower frequency.}\label{fig:no_hd}
\end{figure*}

\begin{figure}
\centering
\includegraphics[width=\columnwidth]{figs/FBL_HD_pure_1}
\caption{Feedback linearization controller demonstrating accurate output tracking. Notice the behavior of the reduced order manifold: $\psi^{\mathrm{des}}=0$ for all time, but it experiences disturbances when the output suddenly moves. In the idealized simulation this did not happen---it occurs because the friction sticking condition is not explicitly compensated for by the controller. This is because the friction term is implulse-like when it appears in the third derivative of state, \emph{i.e.} matching conditions are not satisfied. Due to a weak control on the ROM, compared to the output, the ROM thus suffers the deviation and then tries to correct it slowly, while the output reaches its goal within 0.003 seconds. For this experiment, the estimator was a simple fourth order butterworth filter on $\psi$, and one on $y$, which estimated the state in normal form by simple differentiation (a ``model-free'' estimator---assuming only a quadruple integrator structure for the plant).}
\end{figure}

\FloatBarrier
\section{}
It is difficult to see the effects of disturbances on such a stiff control as this, so I've scaled the noise magnitude by a factor of tenthousand. Even though the model parameters used by the simulation and the controller are different, it is hard to tell it changed. The controller is very high bandwidth, and the disturbances are not enough to destabilize it. The result of adding noise to the FBL simulation is shown in (Fig.~\ref{FBL_NOISE}).
\section{}
Using the framework of the FBL controller, it is very easy to make a SMC. Already the plant is inverted to achieve a desired $\dddot y$, now we can define a sigma function based on deviation from a nice behavior --- a third order butterworth filter at 5 KHz.
\begin{equation}
\sigma=\frac{\ddot y - \ddot x}{k_y^2}+1.4142\frac{\dot y - \dot x}{k_y}+y-x
\end{equation}
where $x$ is the goal. Naturally, if sigma is driven to zero then the ROM induced is a stable transfer funciton in the tracking error. Nice!

Next we define
\begin{equation}
\dot \sigma^{\mathrm{des}} = \min(\max(-k_y \sigma, -\eta_y),\eta)
\end{equation}
which gives the sigma a single pole stable dynamic with saturation. We could also make this stiffer, but given how close the control operates to the bandwidth of the estimator this would induce a limit cycle. Remember, while the trial and error performed with the fourth order FBL filter does not carry over perfectly, it is a good guess---too much stiffer of a control and we would need to make the estimator faster.

Finally, we can calculate our desired $\dddot y$
\begin{align}
\dot \sigma = \frac{\dddot y - \dddot x}{k_y^2}+1.4142\frac{\ddot y - \ddot x}{k_y}+\dot y-\dot x\\
\dddot y^{\mathrm{des}}  =  \dddot x +k_y^2 \dot \sigma^{\mathrm{des}} -1.4142k_y(\ddot y - \ddot x)-k_y^2(\dot y-\dot x)
\end{align}
So naturally, when we are on the surface the equivalent control is whavever would make $\dot \sigma = 0$, or 
\begin{equation}
\dddot y ^{\mathrm{equiv}} = \dddot x -1.4142k_y(\ddot y - \ddot x)-k_y^2(\dot y-\dot x),
\end{equation}
Which is exactly the value my controller would return when on the surface, due to the linear approximation of signum.

In my experiments (representative experiment shown in Fig.~\ref{SMC_NOISE}), the magnitude of $\eta$ determines how quickly the system recovers from the large initial disturbance. Any value smaller than 100 looks pretty bad---it takes a whole hundredth of a second to recover. 


\begin{figure}
\centering
\includegraphics[width=\columnwidth]{figs/FBL_HD_NOISE_1}
\caption{Feedback linearization controller with a thousand times the prescribed quantity of noise! Standard model uncertainty. Note that the output tracks far more accurately than the ROM, due to differing bandwidths of the desired behavior. Due to the unexpected increase in friction, the arm moves towards the goal much more slowly than before.} \label{FBL_NOISE}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\columnwidth]{figs/HD7_1}
\caption{Sliding mode controller with a noisy, uncertain plant! (Note the subtle difference in overshoot versus the feedback linearization controller. A saturated linear sign approximation was used to avoid chattering.) The ROM is still controlled with the FBL. $\eta_y=100$ }\label{SMC_NOISE}
\end{figure}

\end{document}


