\documentclass[]{article}
\usepackage[letterpaper, portrait, margin=1.25in]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{float}
\usepackage{placeins} %Provides \FloatBarrier
\usepackage{listings}
\usepackage{color}
\usepackage{caption}
\usepackage{subcaption}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=python,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=false
  tabsize=3
}
%opening
\title{Non Linear HW1}
\author{Gray Thomas}

\begin{document}

\maketitle
\FloatBarrier
\section*{Problem 1: Violin}
While this system is interesting, I think it's fair to assume that violin string tensions are such that their unforced resonant dynamics are in tune. Thus the problem of tension selection seems tangent to the subject of non-linear dynamics, but with the information provided it should be possible to approach it. The first and most prominent bending mode of a violin string will have position and angular boundary conditions at the ends and satisfy the partial differential equation
\begin{equation}
\epsilon \ddot Y=-T\frac{\partial^2 Y }{(\partial x)^2}
\end{equation}


\newcommand*{\sign}{{\rm sign}}
\subsection*{Equilibrium points and their stability}
The equilibrium points of this model are defined by zero acceleration, zero velocity solutions to 
\begin{equation}
\ddot x= \frac{-k x}{m} -\frac{\sign(\dot x-v_b)N \mu_{(\dot x)}}{m}=0, \;\;\;\;\;\;\dot x=0.
\end{equation} 
We can specify the actual friction coefficient since $\dot x$ is known:
\begin{equation}
\mu_{(\dot x)}=\mu_d+\frac{(\mu_s-\mu_d)}{\nu^2}(|\dot x -v_b| - \nu)^2
\end{equation}
becomes
\begin{equation}
\mu_{(0)}=\mu_d+\frac{(\mu_s-\mu_d)}{\nu^2}(|v_b| - \nu)^2,
\end{equation}
giving a unique equilibrium position
\begin{equation}
x =k^{-1}N (\mu_d+\frac{(\mu_s-\mu_d)}{\nu^2}(|v_b| - \nu)^2).
\end{equation}
Linearizing about this position, 
\begin{equation}
\ddot \xi= \frac{-k}{m}\xi +\frac{N}{m} \frac{\partial \mu}{\partial \dot x}\Big|_{\dot x = 0}\dot \xi
\end{equation}
where 
\begin{equation}
\frac{\partial \mu}{\partial \dot x}\Big|_{\dot x = 0}=
-2\frac{(\mu_s-\mu_d)}{\nu^2}(v_b  - \nu),
\end{equation}
we can find the characteristic polynomial of the linearized system
\begin{equation}
s^2 +2\frac{N}{m}\frac{(\mu_s-\mu_d)}{\nu^2}(v_b  - \nu) s +\frac{k}{m}=0, 
\end{equation}
and its two solutions, the real part of which determines stability as these can also be interpreted as the eigenvalues to the canonical linear system matrix,
\begin{equation}
s=-\frac{N}{m}\frac{(\mu_s-\mu_d)}{\nu^2}(v_b  - \nu) \pm\sqrt{\left(\frac{N}{m}\frac{ (\mu_s-\mu_d)}{\nu^2}(v_b  - \nu)\right) ^2-\frac{k}{m}}
\end{equation}
which are, interestingly, guaranteed to be unstable if the determinant produces complex roots and $v_b<\nu$. This is the most interesting case, since the unstable focus causes the violin string to resonate from a near standstill. Note that when the force becomes sufficiently high relative to the string stiffness then the determinant will become positive, and the system will be capable of having a stable root as well as an unstable one. The equilibrium point will always have at least one unstable root if $\mu_s>\mu_d$ and $\nu>v_b$.
\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{violin1.pdf}
\caption{$\tilde N=1$ $\tilde \mu=1$ $\tilde z=2$}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{violin2.pdf}
\caption{$\tilde N=1/4$ $\tilde \mu=3/2$ $\tilde z=2/3$}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{violin3.pdf}
\caption{$\tilde N=1$ $\tilde \mu=1$ $\tilde z=1$}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{violin4.pdf}
\caption{$\tilde N=1$ $\tilde \mu=0.5$ $\tilde z=2/3$}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{violin5.pdf}
\caption{$\tilde N=1$ $\tilde \mu=3/2$ $\tilde z=2/3$}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{violin6.pdf}
\caption{$\tilde N=1$ $\tilde \mu=2$ $\tilde z=2/3$}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{violin7.pdf}
\caption{$\tilde N=10$ $\tilde \mu=3/2$ $\tilde z=2/3$}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{violin8.pdf}
\caption{$\tilde N=4$ $\tilde \mu=3/2$ $\tilde z=2/3$}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{violin9.pdf}
\caption{$\tilde N=1$ $\tilde \mu=3/2$ $\tilde z=1/10$}
\end{subfigure}
\caption{Various configurations of the violin oscillator.}
\end{figure}

% %
\subsection*{Phase Diagrams}
We can non-dimensionalize the violin model by removing some unneccessary redundancy in the parameter space. 
\begin{align}
\tau_0 \triangleq \frac{\sqrt{m}}{\sqrt{k}}&&
\tau \triangleq \frac{t}{\tau_0}= \frac{\sqrt{k}}{\sqrt{m}}t\\
\chi \triangleq v_b \tau_0&&
\hat x = \frac{x}{\chi}\\
\dot {\hat x}=\frac{d \hat x}{d\tau}=\frac{\sqrt{m} d x}{\chi \sqrt{k} dt} &&
\ddot {\hat x}=\frac{d^2 x}{(d\tau)^2}=\frac{m }{\chi k }\ddot x\\
\ddot x= \frac{-k x}{m} -\frac{\sign(\dot x-v_b)N \mu_{(\dot x)}}{m} &&
\ddot {\hat x}= \frac{ m }{k\chi } \frac{-k x}{m} -\frac{m }{k\chi } \frac{\sign(\dot x-v_b)N \mu_{(\dot x)}}{m} \\
\dot x = \chi \sqrt{\frac{k}{m}}\dot{\hat{x}}&&\ddot {\hat x}= -\hat x -\frac{N }{k\chi } \sign(\chi \sqrt{\frac{k}{m}}\dot{\hat{x}}-\frac{\chi}{\tau_0}) \mu_{(\dot x)} \\
\tilde N \triangleq \frac{N \mu_d }{k\chi } &&
\ddot {\hat x}= -\hat x -\tilde N \sign(\dot{\hat{x}}-1) \frac{\mu_{(\dot x)} }{\mu_d}
\end{align}
\begin{align}
&&\ddot {\hat x}= -\hat x -\tilde N \sign(\dot{\hat{x}}-1) (1+\frac{(\mu_s-\mu_d)}{\mu_d \nu^2}(|\dot x -v_b| - \nu)^2)\\
&&\ddot {\hat x}= -\hat x -\tilde N \sign(\dot{\hat{x}}-1) (1+(\frac{\mu_s}{\mu_d}-1)(\frac{|\dot x -v_b|}{\nu} - 1)^2)\\
&&\ddot {\hat x}= -\hat x -\tilde N \sign(\dot{\hat{x}}-1) (1+(\frac{\mu_s}{\mu_d}-1)(\frac{\frac{\chi}{\tau_0}|\dot{\hat{x}}-1|}{\nu} - 1)^2)\\
\tilde z \triangleq \frac{v_b}{\nu}=\frac{\chi}{\nu \tau_0}
&&\tilde \mu \triangleq \frac{\mu_s}{\mu_d}\\
&&\ddot {\hat x}= -\hat x -\tilde N \sign(\dot{\hat{x}}-1) (1+(\tilde \mu-1)(\tilde z|\dot{\hat{x}}-1| - 1)^2)
\end{align}
This new differential equation has only three parameters to play with!
\begin{equation}
\ddot {\hat x}= -\hat x -\tilde N \sign(\dot{\hat{x}}-1) (1+(\tilde \mu-1)(\tilde z|\dot{\hat{x}}-1| - 1)^2)
\end{equation}

\subsection*{Stability regions in parameter space}
Non dimensionalizing our stability criterion from earlier,
\begin{eqnarray}
s=-\frac{N}{m}\frac{(\mu_s-\mu_d)}{\nu^2}(v_b  - \nu) \pm\sqrt{\left(\frac{N}{m}\frac{ (\mu_s-\mu_d)}{\nu^2}(v_b  - \nu)\right) ^2-\frac{k}{m}} ,\\
s=-\tau_0^{-1}\tilde N\tilde z(\tilde z -1) (\tilde \mu-1) \pm \tau_0^{-1} \sqrt{
\left(\tilde N\tilde z(\tilde z -1) (\tilde \mu-1)\right) ^2-1}.
\end{eqnarray}
This expression has some interesting properties. Consider $\tilde{z}$, which in two cases causes both solutions to be pure imaginary, $\tilde z = 0$ and $ \tilde z = 1$. If $\tilde N=0$ this is the case again, and if $\tilde \mu=1$ it is again. All of these solutions result in centers, and the stability of the system requires extra knowledge. If the solution is to be stable, then we must have $\tilde z(\tilde z -1) (\tilde \mu-1)>0$, which can be accomplished in multiple ways, either by having $\tilde z \in [0,1]$ and $\tilde \mu <1$ or by the opposite of both conditions (excluding boundaries).

% % % % % % % % % % % % %
\FloatBarrier
\section*{Problem 2: Irrational Oscillator}
There are some mistakes in the algebra of the homework with regards to Eqs. (3) and (4), specifically unbalanced parenthesis and unclear presence of a radical. To clarify,
\begin{align}
f_1=k(L-l_1), && l_1=\sqrt{(x-a)^2+h^2}, && f_{x1}=-f_1 (x-a),\\
 && f_{x}=-k(L-\sqrt{(x-a)^2+h^2}) (x-a) &-& k(L-\sqrt{(x+a)^2+h^2}) (x+a) \\
  && \ddot x=-(x-\alpha)(1-\sqrt{(x-\alpha)^2+\beta^2})  &-& (x+\alpha) (1-\sqrt{(x+\alpha)^2+\beta^2}) 
\end{align}\

\subsection*{Equilibrium points and their stability}
This system is energy conserving---it has no dissipative damping---so we can expect that any equilibrium points will be either true oscillatory centers in the non-linear sense or will be unstable non-oscillating rejectors. At a fundamental level this differential equation ignores velocity, so acceleration depends only on position and the curvature of trajectories drawn in phase space will vary such that the osculating circle radius scales linearly with velocity. The differential equation can be solved for zero acceleration

\begin{eqnarray}
0=-(x-\alpha)(1-\sqrt{(x-\alpha)^2+\beta^2})  -(x+\alpha) (1-\sqrt{(x+\alpha)^2+\beta^2})\\
0=-2x+x\sqrt{(x-\alpha)^2+\beta^2} -\alpha\sqrt{(x-\alpha)^2+\beta^2}+ x\sqrt{(x+\alpha)^2+\beta^2} +\alpha\sqrt{(x+\alpha)^2+\beta^2}\\
2x=x \tilde l_1 -\alpha\tilde l_1+ x\tilde l_2 +\alpha\tilde l_2\\
(\tilde l_1 +\tilde l_2-2)x=\alpha(\tilde l_1-\tilde l_2)\\
x=\alpha\frac{(\tilde l_1-\tilde l_2)}{\tilde l_1 +\tilde l_2-2}\\
x=\alpha\frac{\tilde l_1^2-\tilde l_2^2}{(\tilde l_1 +\tilde l_2)(\tilde l_1 +\tilde l_2-2)}\\
x=\alpha\frac{\tilde l_1^2-\tilde l_2^2}{\tilde l_1^2 +2\tilde l_1 \tilde l_2 +\tilde l_2^2 -2 \tilde l_2 -2 \tilde l_1}
\end{eqnarray}
which has an interpretation in the space of $\xi= x/\alpha$ and $\gamma=\beta/\alpha$
\begin{eqnarray}
\xi=\frac{(
\sqrt{(\xi-1)^2+\gamma^2}-
\sqrt{(\xi+1)^2+\gamma^2})}{
\sqrt{(\xi-1)^2+\gamma^2}+
\sqrt{(\xi+1)^2+\gamma^2}-2}\\
\end{eqnarray}
But I digress---it is not neccessary to solve this algebra problem explicitly. As can be seen from the figures, there are at most 3 solutions, and in special cases 2 or 1. The solutions are always symmetric about x=0, so the odd solutions are trivially zero.
\subsection*{Phase Diagrams}
\begin{figure}[!h]
\centering
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{dimlPhase0}
\caption{$\alpha=0.25\;\;\beta=0.25$}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{dimlPhase1}
\caption{$\alpha=0.45\;\;\beta=0.45$}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{dimlPhase2}
\caption{$\alpha=0.50\;\;\beta=0.50$}
\end{subfigure} \\
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{dimlPhase3}
\caption{$\alpha=0.00\;\;\beta=0.00$}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{dimlPhase4}
\caption{$\alpha=0.25\;\;\beta=0.00$}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{dimlPhase5}
\caption{$\alpha=0.50\;\;\beta=0.0$}
\end{subfigure} 
\caption{\textbf{Phase Space Behavior of the Irrational Oscillator.} $\alpha$ and $\beta$ refer to the parameters of the dimensionless model. Simulation performed with scipy's odeint function for 10 dimensionless time units, 1000 linearly spaced time points plotted per initial condition. }\label{irrosc}
\end{figure}
The irrational oscillator has several interesting modes of operation (Fig.~\ref{irrosc}). 
\subsection*{Stability regions in parameter space}
The device can have, depending on the parameters used, either one, two, or three purely oscillatory centers. The fact that the device will not have stable or unstable centers follows from conservation of energy, given that there is no energy dissipation or input for device, so any point which starts at an energy level different from those of the equilibriums cannot ever reach the equilibrium. As alpha increases the third solution becomes more prominent. As beta increases the solution behaves less like a hybrid system as the signum function softens to arctangent. With high beta the outmost equilibria disappear.
\FloatBarrier
\section*{Problem 3: Magnetic Suspension}
The model for a magnetic suspension system is:
\begin{equation}
m\ddot y + b\dot y = -mg + \frac{\lambda}{2}\frac{\mu (k(y-r))^2}{(1+\mu y)^2}
\end{equation}
for $m=2.5$, $b=1.5$, $\mu \in \{0.0,0.5,2.0\}$, $\lambda \in \{0.1, 1.0\}$, $k\in\{0.0,0.5,2.0\}$, and $r\in\{0.0,0.5\}$
\subsection*{Equilibrium points and their stability}
This equation permits an equilibrium at 
\begin{eqnarray}
0=-mg + \frac{\lambda}{2}\frac{\mu (k(y-r))^2}{(1+\mu y)^2}\\
\frac{2mg}{\lambda \mu k^2} (1+\mu y)^2 =(y-r)^2\\
\frac{2mg\mu}{\lambda k^2} (\frac1\mu+y)^2 =(y-r)^2\\
Q\triangleq\frac{2mg\mu}{\lambda k^2}\\
Q y^2 + 2(Q/\mu) y +Q/\mu^2=y^2-2ry+r^2\\
(Q-1)y^2+(2Q/\mu+2r)y+(Q/\mu^2-r^2)=0\\
y=-\frac{(Q/\mu+r)}{Q-1}\pm \frac{\sqrt{(Q/\mu+r)^2-(Q-1)(Q/\mu^2-r^2)}}{Q-1}
\end{eqnarray}
\subsection*{Phase Diagrams}
\begin{figure}[!h]
\centering
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_0}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_1}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_2}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_3}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_4}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_5}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_6}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_7}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_8}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_9}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_10}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_11}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_12}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_13}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_14}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_15}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_16}\end{subfigure}
\begin{subfigure}[b]{0.225\textwidth}\includegraphics[width=\textwidth]{mag_sus_17}\end{subfigure}
\caption{Magnetic suspenision system phase diagrams. Only one trivial case ($\mu$ or $k$ zero) shown, for brevity.}\label{mag}
\end{figure}


\subsection*{Stability regions in parameter space}
When the magnet is active, when $\mu \neq 0 \land k \neq 0$, the system clearly shows a stable focus and a saddle point. This saddle point represents the boundary at which the magnet looses control of the suspended object and it begins to transistion to the purely gravity dominated region of the state space. Near this saddle point there exit lines along which the object could take an infinite amount of time to reach it. Demonstrating stability and saddle point behavior at the two analytically defined equilibrium points is left as an exercize for the reader.
\FloatBarrier
\section*{Problem 4: My Choice: Mass with stiction under PD control}
Modeling a mass with discrete stiction is very similar to modeling a mass with superimposed linear and sliding mode control. A sliding mode controller with $\sigma=\dot x$ and $\eta=N \mu_s$ fights to keep velocity zero; coulomb friction fights to keep velocity zero. This system exhibits a set-valued equilibrium, as the zero dynamics of the sliding mode are that $\dot x=0$. 

The system can be written 
\begin{equation}
m \ddot x = -k_p x -k_d \dot x - \left\{\begin{array}{lll} -k_p x &&: \dot x = 0 \land \|k_p x\|\leq f_s \\ -f_d {\rm sign}(\dot x) &&: {\rm otherwise}  \end{array}  \right.
\end{equation}
\begin{tabular}{cl}
Symbol & Description \\ \hline
$k_p$& Proportional Gain\\
$k_d$& Derivative Gain\\
$f_s$& Static Friction Breaking Point\\
$f_d$& Dynamic Friction Force\\
$ m$ & Mass \\
$x $ & State
\end{tabular}
\subsection*{Equilibrium points and their stability}
This systems interests me because it allows an infinite number of equilibrium points, between plus and minus $mu_s/k_p$
\subsection*{Phase Diagrams}
All phase diagrams simulate systems with $m=1,\;\;k_p=1$. This sets the natural frequency of the system to unity, so that the phase space will be roughly ciruclar, rather than elliptical with an equal aspect ratio.
\begin{figure}[!h]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\includegraphics[width=\textwidth]{stiction1}
\caption{$f_d=0.1$, $f_s=0.2$, and $k_d=0.1$}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
\includegraphics[width=\textwidth]{stiction2}
\caption{$f_d=0.1$, $f_s=0.5$, and $k_d=0.1$}
\end{subfigure}
\end{figure}
\begin{figure}[!h]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\includegraphics[width=\textwidth]{stiction3}
\caption{$f_d=0.0$, $f_s=0.2$, and $k_d=0.9$}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
\includegraphics[width=\textwidth]{stiction4}
\caption{$f_d=0.2$, $f_s=0.2$, and $k_d=0.0$}
\end{subfigure}
\end{figure}
\begin{figure}[!h]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\includegraphics[width=\textwidth]{stiction5}
\caption{$f_d=0.0$, $f_s=0.5$, and $k_d=0.0$}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
\includegraphics[width=\textwidth]{stiction6}
\caption{$f_d=0.2$, $f_s=0.3$, and $k_d=-0.3$}
\end{subfigure}
\end{figure}
\begin{figure}[!h]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\includegraphics[width=\textwidth]{stiction7}
\caption{$f_d=-0.1$, $f_s=0.3$, and $k_d=0.3$}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
\includegraphics[width=\textwidth]{stiction8}
\caption{$f_d=-0.05$, $f_s=0.4$, and $k_d=-0.05$}
\end{subfigure}
\end{figure}
\subsection*{Stability regions in parameter space}
If both friction and dynamic friction forces are positive, then the stiction capture region is attractive. It is nonsensical to write the model with a static case friction force lower than the dynamic case friction force. If this were the case the dynamic friction would theoretically enforce an identical sliding mode, but the integrator would be forced to deal with a discontinuous function. I skipped this unreasonable case. 

Negative damping terms allow energy to enter the system. Thus our system is more interesting if we relax physical limits on friciton---or perhaps imagine the controller is overcompensating for friction---and consider negative damping feedback as well. With anti-friction alone, the system gains a stable limit cycle. The previously stable stiction region is still a large equilibrium set, but now no longer captures all trajectories which start arbitrarily close to it---PD Stiction System 7. If we keep friction physical but negate the derivative term then the stiction capture region again caputures all trajectories which start arbitrarily close to it---PD Stiction System 6. This region of attraction now extends to the boundary of an unstable limit cycle.
\FloatBarrier
\appendix
\section{Plotting Code}
\lstinputlisting{../../../python/homework/non_linear/hw1.py}
\section{Phase Portrait Function}
\lstinputlisting{../../../python/homework/non_linear/phase_portrait.py}
\section{Violin}
\lstinputlisting{../../../python/homework/non_linear/violin.py}
\section{Stiction System}
\lstinputlisting{../../../python/homework/non_linear/stiction_PD.py}
\section{Lyapunov Utils for Hybrid Systems}
\lstinputlisting{../../../python/ly_utils.py}
\end{document}
