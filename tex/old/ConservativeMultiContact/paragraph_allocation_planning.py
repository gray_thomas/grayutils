figpars = 0

pars = 1

# title: Planning Conservative---Yet Dynamic---Multi-Contact Locomotion

pars += 1  # sections heads 
pars += 1  # abstract
"""
Questions for the 3/1 paper on multi-contact planning:
    - Is testing a convex polytope the best we can do?
    - How many examples should we show
    - How much structure should we try to explain to the audience?
    - Can we ignore speed, and therefore greatly simplify the problem?
    - Is there a good way to include the effects of transition uncertainty?
    - Is a bounded force a good model for a human?
    - Is there a good way to describe the case in which this planning approach is good? Is it "whenever kinematics is non-limiting?"
# goal: multi-contact dynamic locomotion planning for robots with feet and
# hands, inclusion of controller uncertainty.

# Research Question: Is it possible to compute, in real time, a multi-contact
# locomotion trajectory which exploits dynamics, but also guarantees that the
# trajectory can be followed within a pre-defined tolerance?

# Validation: A slice of life example: Valkyrie on mars walking up stairs in a sandstorm.

# Validation of formal model aspects: rigorous proof and or derivation.

# why this retains the essence of the problem.


# abstract structure:
# 2-3 sentences about the current state of the art, identifying the particular problem
# 1-2 sentences about what the paper contributes
# 1-2 sentences about the specific result of the paper and the main idea behind it
# a sentence about how the result is demonstrated or defended.


# Contributions:

# *) Awesome pictures motivating Valkyrie multi-contact on Mars.
# *) At the same time, also motivated by Humanoid robots in the home
# 1) Path-following multi-contact rigid-body model with bounded inaccuracy.
# 2) Maximum conservatism planning concept
# 3) Maximum positional uncertainty
# 4) An explaination of low speed chatter
# 5) A chatter resolver

# Which contributions go in Planning Conservative---Yet Dynamic---Multi-Contact Locomotion?

# 1) centroidal multi-contact model
# 2) applying optimal time planning to this new model
# 3) robustness analysis involving LMIs
# 4) sub-optimal chatter resolvers
# 5) sample code?

Main answerable question (the thesis of the paper):
    
Main findings of the paper:
    An algorithm for choosing between stances via greedy optimization

I don't even like the way it chooses between stances.
It's a hack, designed to solve an efficeintly solvable problem, but to certify that the path it chooses can be followed is intractable because of the kinematic assumptions it makes.
It chooses between stances in a way which could be easily approximated with trivial effort.
Nobody cares about the minimum time problem it solves, really, and certaintly not about solving it to high accuracy.
It's complex, and it is not clear that this complexity is justified.
It may be a framework which will be useful temporarily, but long term it will be replaced as soon as possible.
Just because I had good ideas on implementation can't help the larger problem of bad problem assumptions.
Most of the ideas I really liked were previously published in the prior art.
Maybe if ICRA comes back really positive I'll be convined that the ideas matter, but I feel like I'm wasting valuable time thinking about this. 
I think we need to go back to the base line reason for pursuing this problem, and see if there is another way to frame it.

We need to do better.


Video Abstract:
Image: 660x295 JPG < 45 KB
Video 29.97 FPS, 16:9 aspect, MP4, <100MB. 3-5 seconds per slide, if slides are used.


# Notes:
# The framework permits lexicographic optimization of stance choice.

# Intro
"""
intro = [
    # motivate with PIPM, overview contributions
    1,
]
pars += sum(intro)
# % Luis: add the following:
#  % Tim Bretl
#  % Kris Hauser
#  % Abderrahane Kheddar 
#  % Bouyarmane
# Literature
lit = [
    1,  # pendulum locomotion works
    1,  # General multi-contact planning approaches work too, but are too slow for locomotion
    1,  # Traditional phase-space Manipulator is fast
    1,  # speed has lead to a Recent revival in humanoid robotics
    1,  # our approach combines pendulum locomotion and minimum time (contributions)
    # abstract model separating center of mass and reaction force dynamics.
    # problem is augmented with contact decision problem
    # optimal chattering case is explained and handled
    # solution is implemented with an easy-to-exactly-integrate hybrid
    # automata.
]
pars += sum(lit)


figpars += 2  # robot mapping
# The Multi-contact model # it is based on KoolenEA2013ICHR, sentis
section1 = [
    1,  # justify point mass
    1,  # Introduction to the model, variables
    1,  # Choice of stances
    1,  # Parameters of a stance including kinematic limits
    1,  # Inequality constraints
    1,  # Moment Equality & CF
    1,  # Convexity, quadratic relationship with speed
    2,  # math (interspersed)
    1,  # resulting optimization problems
]
pars += sum(section1)


# be sure to mention in the explaination:
# credit BobrowDubowskyGibson1985IJRR for the basic idea
# credit ShinMckay1986TAC for search ahead
# credit PfeifferJohanni1987JRA for sources and sinks
# credit SlotineYang1989TRA for critical point classification
# credit ShillerLu1992JDSMC for singular arcs, the maximum speed portion
# credit LippBoyd2014IJC for noting the problem is convex in acc and speedsquared

figpars += 2  # multi-step max algorithm figure
figpars += 2  # max state machine diagram
figpars += 2  # multi-step min algorithm figure
figpars += 2  # min state machine diagram
# Algorithm
section2 = [
    1,  # introduce it, based on previous work
    1,  # explain high level principles: stance selection, integration
    1,  # max speed hybrid automata
    1,  # explain max speed multi-step algorithm figures.
    1,  # min speed hybrid automata
    1,  # explain min speed multi-step algorithm figures.
]
pars += sum(section2)  # multi-step


# figpars += 2  # movie fig
figpars += 4  # 2 column results fig
# Optimal Chattering and other behavior
section3 = [
    # 1,  # implementation & physics verification
    1,  # explain movie fig
    1,  # explain chattering fig and our approach to it
    1,  # using chattering to identify where to combine stances.
    1,  # explain how the user can exploit the step choice properties
]
pars += sum(section3)  # multi-step


# Conclusion
conclusion = [
    1,  # In 3D / ballistic / point foot
    # 1,  # complex contacts
    # 1,  # other easy extensions
    1]  # summarize claims
pars += sum(conclusion)

# Media: movie which shows minimum plan following, maximum plan following,
# chatter, UI? state machine transitions?


pars += 3  # bib

pars += figpars
print "Paper is about %.1f pages long\n" % (pars/8.0)

print "\tAll Figures: %.1f pages (%.1f%%)" % (figpars/8.0, (figpars * 100.0) / pars)
textpars = sum(conclusion + section3 + section2 + section1 + intro + lit)
print "\tTotal Text: %.1f pages (%.1f%%)\n" % (textpars/8.0, (textpars * 100.0) / pars)
print "\tIntroduction: %d par (%.1f%%)" % (sum(intro), (sum(intro) * 100.0) / pars)
print "\tLiterature Review: %d par (%.1f%%)" % (sum(lit), (sum(lit) * 100.0) / pars)
print "\tSection 1: %d par (%.1f%%)" % (sum(section1), (sum(section1) * 100.0) / pars)
print "\tSection 2: %d par (%.1f%%)" % (sum(section2), (sum(section2) * 100.0) / pars)
print "\tSection 3: %d par (%.1f%%)" % (sum(section3), (sum(section3) * 100.0) / pars)
print "\tConclusion: %d par (%.1f%%)" % (sum(conclusion), (sum(conclusion) * 100.0) / pars)
# print "\n\n"
