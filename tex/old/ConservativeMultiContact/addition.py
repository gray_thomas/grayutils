figpars = 0
pars = 2  # title: A Phase Space Method for Planning Multi-Contact Dynamic Locomotion in a User Interface
pars += 1  # sections heads 
pars += 1  # abstract

# goal: multi-contact dynamic locomotion planning for robots with feet and
# hands.

# Contributions:
# using the maximum or minimum time with the multi-contact model
# allowing contact usage to be specified by the result
# understanding the implications of this.


# minimum time planning for the multi-contact model: robot is a point mass,
# robot has point-contact appendages with limited force capability, robot
# can't move faster than a maximum speed, which is set conservatively. User
# has pre-defined the robot's center of mass path, and also identified contact
# locations, contact properties like surface tangent, max normal force and
# friction coefficient. contacts are availiable to the robot in which
# combinations. Approximate kinematics by specifying maximum "leg-lengths"
# between contacts and the center of mass, though these are not necessarily
# physically motivated. The user can also directly specify for which parts of
# the path a stance can be applied. Our approach seeks to merge phase space
# dynamic locomotion planning with minimum time phase space planning as
# applied in manipulator trajectory scaling. Simple demonstration explains
# some non- intuitive behavior of this model, and how it improves over the
# original algorithms for the application at hand.

# Intro
intro = [
    # Why multi-contact: motiviation: humanlike behavior, disaster response
    # and exo planet missions.
    1,

    # User interfaces with rich realtime feedback are a viable way to address
    # this issue
    1,
]
pars += sum(intro)

# Literature
lit = [
    1,  # Other multi-contact planning approaches are slow
    1,  # Traditional phase-space Manipulator TTSP
    1,  # Recent revival in humanoid robotics
    1,  # Phase space locomotion planning, sentis sentis sentis.
    1,  # contributions:
    # abstract model separating center of mass and reaction force dynamics.
    # problem is augmented with contact decision problem
    # optimal chattering case is explained and handled
    # solution is implemented with an easy-to-exactly-integrate hybrid
    # automata.
]
pars += sum(lit)


figpars += 2  # robot mapping
# The Multi-contact model # it is based on KoolenEA2013ICHR, sentis
section1 = [
    1,  # Introduction to the model, variables
    1,  # Choice of stances
    1,  # Parameters of a stance
    1,  # Kinematic limits
    1,  # Positive optimization variables
    1,  # Maximum normal force
    1,  # Moment Equality & CF
    1,  # Quadratic relationship with speed
    2,  # math (interspersed)
    1,  # resulting optimization problems
]
pars += sum(section1)



figpars += 2  # multi-step max algorithm figure
figpars += 2  # max state machine diagram
figpars += 2  # multi-step min algorithm figure
figpars += 2  # min state machine diagram
# Algorithm
section2 = [
    1,  # introduce it, based on previous work
    1,  # explain high level principles: stance selection, integration
    1,  # max speed hybrid automata
    1,  # explain max speed multi-step algorithm figures.
    1,  # min speed hybrid automata
    1,  # explain min speed multi-step algorithm figures.
]
pars += sum(section2)  # multi-step


figpars += 2  # movie fig
figpars += 2  # chatering fig
figpars += 2  # phase space
# Optimal Chattering and other behavior
section3 = [
    1,  # implementation & physics verification
    1,  # explain movie fig
    1,  # explain chattering fig and our approach to it
    1,  # using chattering to identify where to combine stances.
    1,  # explain how the user can exploit the step choice properties
]
pars += sum(section3)  # multi-step


# Conclusion
conclusion = [
    1,  # In 3D / ballistic / point foot
    1,  # complex contacts
    1]  # summarize claims
pars += sum(conclusion)


pars += 3  # bib

pars += figpars
print "Paper is about %.1f pages long\n" % (pars/8.0)

print "\tAll Figures: %.1f pages (%.1f%%)" % (figpars/8.0, (figpars * 100.0) / pars)
textpars = sum(conclusion + section3 + section2 + section1 + intro + lit)
print "\tTotal Text: %.1f pages (%.1f%%)\n" % (textpars/8.0, (textpars * 100.0) / pars)
print "\tIntroduction: %d par (%.1f%%)" % (sum(intro), (sum(intro) * 100.0) / pars)
print "\tLiterature Review: %d par (%.1f%%)" % (sum(lit), (sum(lit) * 100.0) / pars)
print "\tSection 1: %d par (%.1f%%)" % (sum(section1), (sum(section1) * 100.0) / pars)
print "\tSection 2: %d par (%.1f%%)" % (sum(section2), (sum(section2) * 100.0) / pars)
print "\tSection 3: %d par (%.1f%%)" % (sum(section3), (sum(section3) * 100.0) / pars)
print "\tConclusion: %d par (%.1f%%)" % (sum(conclusion), (sum(conclusion) * 100.0) / pars)
print "\n\n"
