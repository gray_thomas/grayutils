Structural planning, how many paragraphs per section? What fits in 7 pages?
In one page of all text: 1000 words, 6600 charactors (with spaces). 8 paragraphs of 125 words each. 7 pages = 56 paragraphs. 
Title uses 2 paragraphs of space, abstract one, all the section headings together are probably another, bibliography is 3, each figure is 2 (in column with description), so paragraphs = 49 - 2 * num_figs. Some paragraphs are all math. An algorithm will probably take up 3 paragraphs of space.



Heading and abstract: 3 paragraphs
Introduction: 4 paragraphs
Figure: stills from movie: 4 paragraphs
Figure: multi-contact phase space 2 paragraphs
Prior Art: 7 paragraphs
Maximizing Acceleration: 8 paragraphs (why maximize, the positive, the max force, the moment equality, the centrifugal, quadratic speed relationship, mathematical formulae (2))
Intersecting Trajectories: 12 paragraphs
Max Speed Case: 13 paragraphs
Validation: 3 paragraphs
Conclusion: 3 paragraphs
Bib: 3 paragraphs




Idea---misconception---question---addressing the mistaken reader.


What is the scope of the paper?
In order of increaseing scope, we have:
Level 0:
Setting up the convex optimization problem
Level 1:
Integrating convex optimum forward for one contact, nonlinear ode style
Level 2: 
Integrating convex optimum forward hybrid style with whichever contact is locally better (single step)
Level 3:
Considering a maximum speed, calculating plateau nucleations, fusing trajectories (Many step)
Level 4:
Adding the convex optimization problem's active set selector to the hybrid state (Efficiency boost)
Level 5:
Using a higher level optimization process to generate the center of mass path based on properties of the best path (Real problem)

Extensions: 
3D
complex speed limit
automatically generated speed limit -- allowing ballistic trajectories
complex-contacts
thrusters
wheels
second order cone constraints

How should we scope a 6-8 page paper? Probably Level 3. That's worth mentioning immediately on its own merits. The primary verification? Videos of it executing the plans? 

How about I list the possible videos, figures, etc... this research could generate:
Videos of single steps, maybe even a gallery of them with explainations for each different type of multi-contact maneuver. See the effect of the force limit, see the effect of the maximum speed



Phase Space Planning for Multi-Contact Locomotion

Naming ideas:
Greedy Hybrid Automata Integration: The algorithm is greedy because it doesn't think ahead when choosing the acceleration, it just takes the best one it can currently take. The integration is hybrid because it uses systems which fuse continuous trajectories with discrete state change events.
Speed plateau: 
questions they would ask if they totally did not understand what I just wrote:


Convex Optimization Questions:
	Why do we paramterize contact forces with basis vectors and not second order cone constraints?
	How do the dynamics influence the optimization?
	Why does speed influence the result?
	Why is the speed relationship quadratic?
	Are we sure that the speed--feasibility relationship will have a convex boundary?

Integrating Convex Optimization Forward and Backwards in time questions:
	Wait, so we plan to go back in time from the end?
	What does the intersection point represent, exactly?
	As we go back in time, what happens to the measurement of time?
	What happens if we go too fast?
	What happens if one of the trajectories reaches zero speed again?
	What happens if there is no intersection? In what cases could this happen?
	Is this the same as optimal bang-bang control?
	This takes advantage of Pontryagin's Maximum principle, right?
	How do we integrate? Does it matter?
	Why do you call the start and end states "Dynamic-Nucleation sites"?

Using a maximum speed, calculating AZDI regions at max speed, plateau nucleation sites:
	Why do we want to consider a max speed?
	When there is no max speed, it obviously makes sense to accelerate then decelerate, but what do trajectories look like in this new case?
	How do we identify AZDI regions? Why do we use a hybrid system for this?
	So then it's easy to identify the plaeau regions, once AZDI is decided?
	How do we make sure the problem doens't become infeasible below the max_speed?






Introduction Questions:
	What makes this idea worth reading about?
	What is the problem this solves? What is multi-contact?
	Why should it be called 'Greedy Hybrid Automata Integration'
	What did you consider when you chose this particular problem formulation? How can you justify assuming a center of mass path?
	Why is it important to chose a proble which has an efficient solution? What is wrong with choosing a problem formulation which happens to be NP-hard? Why not choose a problem which is simply general?
	Why do you care so much about computational complexity? Does planning really need to be this fast? Who would possibly need such a fast planning process? What makes you so convinced that this is fast? Why did you implement it in python? Why not C++, or Java?
	Sure it can make it easy for a user to design a path, but what about a high level AI?
	How does this idea work, generally?

Prior Art Questions:
	Has anyone who studies bang-bang control come up with something similar?
	Is this a standard application of optimal control?
	Is this the same as phase space planning---maybe it is an extension or generalization of phase space planning?
	Is this similar at all to capture-point planning?
	Does greedy integration improve over rapidly expanding rapid trees? Do they apply to the same problems?
	How does this relate to zero moment point walking?
	How can you compare your 2D planner to the 3D planner that plans HRP2's rock climbing?

Verification Questions:
	Have you verified that this approach can be implemented in software?
	Have you checked to make sure you didn't screw up the physics somehow?
	Have you verified that this approach can solve problems previously solved with phase space planning?
	# Have you verified that this approach is efficient? How many times is the optimization problem called? How many times are the limits checked?
	# Have you verified that the active set does not change except when you say it does?
	# Have you verified that all continuous dynamics are separated by discrete transitions, and that no sudden jumps persist in the continuous regions?
	What does your simulation prove?
	What are the limitations of simulating in 2D?
	What are the limitations of writing the simulation in python?
	What are the limitations imposed by using only point contacts for the validation?
	# What, remind me, are all the extensions which you are not involving?
	Why is it acceptable to simply report the number of times a function was called, rather than the computation time?
	Have you verified that this process is compuationally easier than running a brute force sequential convex optimization?
	Is this approach easier than a single convex optimization step (which includes speeds at all times, etc...)

Deep Understanding Questions:
	So, in total, what do we need to provide to the algorithm to guarantee a useable path?
	Does the model allow constant switching at infinite frequency? How is this acceptable?
	Does the model permit sudden changes in the contact configuration?
	Physically, extremely high speed will always procude infeasibility. Is this fundamentally important? Does this ensure that we have a convex feasibile speed set?
	What happens if the allowable acceleration fluctuates really fast, and there are lots of short speed--plateau regions?
	Would the problem still be tractable if there were not a convex feasible speed set?
	Can you prove that your algorithm has a runtime bound?
	Can these trajectories be guaranteed to happen?

Extension Questions:
	Position varying speed limits:
		Can we define a speed limit that depends on position?
		Can we gernerate a speed limit based on the limiting feasibility of the optimization?
		What would this more general speed limit do to the definition of A Z D I regions?
	3D Extension:
		How will this work in 3D?
		What happens when the robot enters a flight phase?
		What happens when the robot is in 3D, but only has a single contact? A lot of paths won't work because of that, right?
		What is the relationship between x, y, and z that such paths must satisfy?
	Can the idea support wheeled robots?
	Can this idea support robots with propellors or thrustor jets?
	Can we make the convex optimization at the heart of this a little more robust, to account for path tracking error?
	How could this be used for manipulation?
	What happens if the robot is allowed to rotate about its center of mass?
	Lesbeg qualifiers, such as "almost everywhere differentiable"

Conclusion Questions:
	So what does this mean for society?
	When a robot interacts with a person, is that a multi-contact?
	Could this idea spur further research, or is it a dead-end?
	Could you use this for agriculture?
	Could you use this for manufacturing?
	Could you use this for defense?
	Could you use this in people's homes?
	Could you use this in robot sports?


Central ideas of my Multi-Contact paper:
planning a restricted locomotion problem quickly will not only solve that problem; it will make it faster to solve problems for which it is a subproblem.
Efficient solutions to this multi-contact planning problem---however much they may help researchers dealing with pre-defined center of mass paths---show their real utility as a subroutine; an optimization which iteratively adjusts the center of mass path definition can now quickly dispense with infeasible paths. Further, a user interface can now provide realtime feedback on path feasibility. 


Conservatively chosen constraints define the behavior of the final trajectories; whatever they may be, they will be exhausively exploited. If a feasible solution can be found, it may be beneficial to try again, with even more conservative constraints. In this way the algorithm can be used to indirecly optimize for efficiency, fuel use, or user preference. The maximum speed limit, for instance, can be minimized---subject to feasibility---by simple bisection search. Adjusting the maximum forces and friction properties will indirectly limit the actuator torque. When driven to the very limit of feasibility these final solutions become equally the optimum for a constrained minimum time problem and the optimum for a feasibility constrained constraint-minimization problem, whatever that constraint may be.

Iterative convex optimization is a powerful tool for accurately finding the best solution to a problem of this type, and admits a more general set of optimization criteria. Yet it is not nearly fast enough given similar accuracy. For this type of problem, a convex solver will need to discretize the system, and for each discretized point along the path, it will need to consider an integer variable to represent its choice of the multiple stance options availiable, and enough variables to parameterize the constrained reaction forces at that point. It will then need to add additional variables to represent the velocities at each point, which are subject to a physical acceleration constraint as featured in this paper's algorithm. This leaves the optimizer to solve a much larger convex problem than is necessary to simply maximize speed at every point. When the problem has been solved, the speed will change, and this will change the constraints on the forces. In some cases the new constraints will cause an infeasibile optimization problem, and it is difficult to check if this is the case without attempting the problem. So, while this brute force convex optimization approach offers a more fully featured set of optimization criteria, its complexity is much higher. You could run the algorithm in this paper several times in the time it takes to run a single brute force optimization of equivalent accuracy.

Speaking of accuracy, many alternative algorithms will divide time or space into equally sized chunks. Actual dynamics may be very slow in some regions and change quickly in others. To take advantage of this, many tools for integration of systems use a variable step size. They take short steps in the fast regions they take long steps in the boring regions. Extreme differences exist in the multi-contact problem. in order to produce the most accurate results possible in a given time, the algorithm we propose is designed to work with a hybrid system solver. This type of integration algorithm detects and investigates contact changes. When it first notices that the contact state has changed, it searches iteratively to find exactly when it changed. When it finds that point, it starts a new variable step size solver an infintesimal distance beyond the point. This allows it to take big steps in all cases, and disconnects the problem of grid density and contact sensing. In an algorithm which divides up the state space, the only way to accurately identify the precice instant of a contact is to have lots and lots of points. Explicitly avoiding this improves the accuracy of this method. In many cases the robot's foot contact switching coincides with a sudden change in acceleration. When a grid is employed, the actual transition might occur anywhere inside the two grid points about it. The ammount of speed and time error introduced by this simplification can be large. With a grid-type system











It is even possible to use the algorithm as a subroutine in that next optimization---avoiding tedious gradient descent or iterative convex optimization. 




