figpars = 0
pars = 1  # title: Playspace Control for System Identification

pars += 1  # sections heads 
pars += 1  # abstract

# goal: system identification guaranteed to stay in a region

# Surprising results:
# 1) this region is deeply connected to lyapunov functions
# 2) every play-space region is a union of bounded lyapunov function regions for different lyapunov functions of the controller.
# 3) robustness: if the system has stable, unmodeled, high frequency dynamics---similar to a time delay, or the delay from an observer---then the lyapunov inequality is not actually satisfied.

# Notes:
# The framework permits lexicographic optimization of stance choice.

# Intro
intro = [
    1, # Vision of dynamic multi-contact locomotion. Identification of Gap between kinodynamic and locomotion
    1, # Vision of approach: nested optimization
]
pars += sum(intro)
# % Luis: add the following:
#  % Tim Bretl
#  % Kris Hauser
#  % Abderrahane Kheddar 
#  % Bouyarmane
# Literature
lit = [
    1,  # kinodynamic planning
    1,  # kinematic planning using quasi-static assumptions
    1,  # babrow
    1,  # Dynamic locomotion ideas
    1,  # Phase space planning (emphasize footstep re-planning for stabilizing biped)
    1,  # Difference between our model and previous phase space.
    # abstract model separating center of mass and reaction force dynamics.
    # problem is augmented with contact decision problem
    # optimal chattering case is explained and handled
    # solution is implemented with an easy-to-exactly-integrate hybrid
    # automata.
]
pars += sum(lit)


figpars += 2  # triskelion
# The Multi-contact model # it is based on KoolenEA2013ICHR, sentis
section1 = [
    1,  # justify point mass
    1,  # Introduction to the model, variables
    1,  # Choice of stances
    1,  # Parameters of a stance including kinematic limits
    1,  # Inequality constraints
    1,  # Moment Equality & CF
    1,  # Convexity, quadratic relationship with speed
    1,  # resulting optimization problems
]
pars += sum(section1)


# be sure to mention in the explaination:
# credit BobrowDubowskyGibson1985IJRR for the basic idea
# credit ShinMckay1986TAC for search ahead
# credit PfeifferJohanni1987JRA for sources and sinks
# credit SlotineYang1989TRA for critical point classification
# credit ShillerLu1992JDSMC for singular arcs, the maximum speed portion
# credit LippBoyd2014IJC for noting the problem is convex in acc and speedsquared
figpars += 4  # overview figure
figpars += 4  # 4 simple figures, parts of the process
figpars += 3  # max state machine diagram

# Algorithm
section2 = [
    1.5,  # introduce it, based on previous work
    2,  # explain high level principles: stance selection, integration
    1,  # states
    1.5,  # explaination, fig 1
    1,  # max speed
    1,  # critical point
    1,  # post intersection, and proof completion
    1.5,  # splitting up the automata states
    1,  # potential for unfollowable paths
    1,  # discuss limb allocation
    1,  # guaranteeing chatter avoidance
]
pars += sum(section2)  # multi-step


# figpars += 2  # movie fig
figpars += 3  # 2 column results fig + max speed planning figure
figpars += 2 # more complex sequence figure
# Optimal Chattering and other behavior
section3 = [
    # 1,  # implementation & physics verification
    1.25,  # explain movie fig
    1.25,  # explain chattering fig and our approach to it
    1.25,  # using chattering to identify where to combine stances.
    1.25,  # explain how the user can exploit the step choice properties
]
pars += sum(section3)  # multi-step

section4 = [
    # 1,  # implementation & physics verification
    1,  # explain sim
    1,  # simplification of max speeding being critical
    2,  # Explain gap jumping
    1,  # explain other results
]
pars += sum(section4)  # behavior of the model
# Conclusion
conclusion = [
    1,  # In 3D / ballistic / point foot
    # 1,  # complex contacts
    # 1,  # other easy extensions
    1]  # summarize claims
pars += sum(conclusion)

# Media: movie which shows minimum plan following, maximum plan following,
# chatter, UI? state machine transitions?


pars += 6  # bib

pars += figpars
print "Paper is about %.1f pages long\n" % (pars/8.0)

print "\tAll Figures: %.1f pages (%.1f%%)" % (figpars/8.0, (figpars * 100.0) / pars)
textpars = sum(conclusion + section3 + section2 + section1 + intro + lit)
print "\tTotal Text: %.1f pages (%.1f%%)\n" % (textpars/8.0, (textpars * 100.0) / pars)
print "\tIntroduction: %d par (%.1f%%)" % (sum(intro), (sum(intro) * 100.0) / pars)
print "\tLiterature Review: %d par (%.1f%%)" % (sum(lit), (sum(lit) * 100.0) / pars)
print "\tSection 1: %d par (%.1f%%)" % (sum(section1), (sum(section1) * 100.0) / pars)
print "\tSection 2: %d par (%.1f%%)" % (sum(section2), (sum(section2) * 100.0) / pars)
print "\tSection 3: %d par (%.1f%%)" % (sum(section3), (sum(section3) * 100.0) / pars)
print "\tConclusion: %d par (%.1f%%)" % (sum(conclusion), (sum(conclusion) * 100.0) / pars)
# print "\n\n"
