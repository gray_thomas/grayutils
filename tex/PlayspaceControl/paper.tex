\documentclass[letterpaper, 10pt, conference]{IEEEtran}
%\usepackage{cite}

\usepackage{placeins}
\usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
\graphicspath{{./pdf/}{./jpeg/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpeg,.png}

\usepackage[cmex10]{amsmath}
\usepackage{bm,upgreek}
\usepackage{amsfonts}
\usepackage{wasysym}
%\usepackage{amsthm}
\interdisplaylinepenalty=2500
\newtheorem{theorem}{Theorem}[section]
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\renewcommand{\IEEEQED}{\IEEEQEDopen}

\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\sinc}{sinc}
\usepackage{algorithmic}

\usepackage{array}

\usepackage{fixltx2e}

\usepackage{url}

\hyphenation{op-tical net-works semi-conduc-tor}

\begin{document}

\title{Playspace Control for System Identification}

\author{\IEEEauthorblockN{Gray Thomas}
}
\IEEEoverridecommandlockouts

\maketitle


\IEEEpeerreviewmaketitle

\begin{abstract}
In many practical cases a control engineer is brought to a system that is badly modeled, but which has a controller that stabilizes it. This controller is based on a low order model fo the system, and it approximately captures some of the poles, but omits others. Especially the highest frequency poles. We consider a method for conducting system identification experiments and for testing of potentially unstable controllers, which uses this existing controller and the existing model to guarantee that hard state limitations are enforced. We construct a hybrid system with two states: in the first state the existing controller stabilizes the system. The second state can be anything, but the boundary must the based on a robustified version of an iso-surface of the existing controller's lyapunov function with the existing model. This creates what we call a ``play-space'' in the system's state-space. 
\end{abstract}

\section{Introduction}
In many cases the role of the system identification and control engineer is to take a plant with an existing controller and improve the performance, ultimately aiming to save money. Proper identification of the plant requires open loop identification experiments and, when the plant is unstable, this is not viable. In this paper we suggest a strategy for getting away with open loop experiments: we run our open loop experiment until the system's measured state hits some boundary, called the play-fence, and then we switch from experiment to closed loop control to bring the state back to a point we call the "center of the play space", from which point we begin the experiment again. We derive a relationship between the lyapunov function of the controller---the safety controller---and all possible playspace boundaries. We also consider the important concern of robustness: when implemented on physical systems, the lyapunov function inevitably omits some energy storage elements which are too complex to model. However, if we can bound the energy these modes are capable of storing, given some restricting low-pass filter on the input and an assumption that the state remains in the play-space, we can derive a switching surface which we use to initiate the switch to the safety controller in advance of reaching the play-fence. We implement this strategy and show that we can safely test unwise choices of input, since the combination of any controller and the safety controller switched by the conservative play-fence is guaranteed to remain in the play-space as a hybrid system.


\section{Related Work}


\section{Lyapunov Functions and Perfect Playspace Control}
The lyapunov function of the owning controller serves as a natural limit on the playspace.


\section{Playspace Control with Approximate Modelling}
\subsection{Thought Experiment: exciting only the unmodelled dynamics}
Suppose we have a model of the low order system. For a series elastic actuator, this would be a model where the spring has reached equilibrium, and only the second order dynamics of the output is considered. In this model there are some inputs which will excite the high frequency dynamics, but keep the measured states, the position and velocity of the output link, inside a small region. Suppose we violently add energy to the spring oscillation, yet keep the output in the same place---that is, we draw our input from the zeros of the true transfer function between input and our measured outputs. Once our input switches from following this signal to something else, the natural system dynamics could move energy from the high frequency dynamics to the low. If the motor stops wiggling the spring when the spring is deflected, the output will experience a transient. We want to guarantee limits on the output, so we must detect this before it happens. This means 1) we must measure more states, 2) we must restrict our inputs, 3) we need to know the worst case relationship between input energy and output energy, or 4) we must exploit basic physical properties, like the mechanical work done by the motor torque, and use this to pad our estimate of the lyapunov function value.

Suppose we active our controller. It is our assumption that this controller is stable, and also our assumption that it models only part of the system. The part that is not modelled is uncontrolled, and thus must naturally be stable. The settling time of this unmodeled, yet stable, system is of importance. The more likely case is that this settling time is faster than the slowest pole in our closed loop system model. This means the slowest pole of the unmodelled system is faster, and that once our controller has settled to some desired level, the unmodelled system will also have, and that we can assume that the low frequency model is a good approximation of the true lyapunov function. Both of them should reach the noise floor, near zero. In effect, we know the initial value of the lyapunov function is bounded, when we start applying control effort again.

When we apply control effort, that effort has physical meaning, and it has energy. Our low dimensional model describes part of that energy---it explains that part of the energy is being stored, and it also explains that some of it is being dissipated. If our system is stable without control it will be dissipated constantly. If our system is unstable, there will be another energy source included in the system model, and this will add energy to the system in a measurable way. We wouldn't be able to control the system if there was an unmodeled unstable pole. Our model limits the scope of behaviors we could impose, and not knowing of the unstable pole guarantees we would not accidently have placed it under closed loop control with our controller. The unstable pole is not observable in the nominal model, so it can't be under control.

Thus we assume that we know all positive energy sources, and that they are part of the nominal model. We also assume that we know how long it takes the system to settle for arbitrary inputs. This puts a limit on the real part of the unmodeled poles.

Is this pole real-part limit real? What about poles on the j-omega axis? Really high frequency ones?
Suppose we had such a pole (pair) unmodelled in the system. Suppose it truly had no damping. The impulse response of this system would naturally oscillate forever at high frequency. But what if the magnitude was very small? Well in that case, if we keep applying impulses to the system this will grow randomly, and eventually become observable. In fact, if we merely exite the system normally with some noise at all frequencies, we will eventually build up a detectible signal which will not go away if we turn on our controller. This violates the condition that the controlled system must settle in known time. The main point here is that knowing that the system settles is powerful information, and is also information that we can have. For physical systems, it is information that we almost always have. Physical imaginary axis poles are perpetual motion machines, violating conservation of energy. Of course, in models which include large inflows and outflows of energy other than the control input this is not the case. For example in an airplane model where the craft is being thrust through the air the steady state model ignores the considerable energy of the velocity disparity between airplane and stagnant air. In this situation it is easy for physical systems to have marginal or unstable oscillations powered by the interaction with an energy source. Some small part of the airplane might flap at high frequency without being a perpetual motion machine. The flapping trasduces energy from the forward speed, and generates drag. 

In this case however, the unmodelled disturbance might be a non-linear effect. Perhaps the flapping has a speed dependent limit cycle, but the limit cycle has a fixed magnitude. In this case the energy which can be stored in the flapping motion is bounded. It is possible that such a behavior will never be detected in the output, and this makes it truly negligable. If it cannot be detected in the output, no matter how unstable the flapping behavior's pole is at low amplitutdes, it becomes stable at higher amplitudes. If it becomes stable it behaves as other stable poles. 

Suppose we measure the energy added into a system from the input, and the modeled sources of energy. Our system is split into two parts: modeled and unmodeled. From and energy inflow and outflow perspective, the energy into the unmodeled system is thus known. We can consider the energy currently stored in the modeled part of the system as an energy output of this unmodeled system. What happens to energy stored in a stable system? Naturally, it compounds at most as fast as the highest real part of a pole. Negative real part poles, which we are assured in the stable high frequency unmodeled poles, will naturally cause this energy to decay. If we pump mechanical power into the high frequency system it will reach an equilibrium of mechanical energy, and this energy is knowable! It is based simply on the knowledge of the real part of the poles, which is the worst case decay rate for energy.
\begin{equation}
\dot E \leq -a E + f(u)
\end{equation}

In our previous example, where we excited the high frequencies of the plan using a zero of the transfer function between input and measured output, our model was absorbing no energy. All the energy of our input signal was going into the high frequencies. The high frequencies have bounded energy storage, since we know the plant reaches the noise floor in bounded time in closed loop. So now we can bound the transient if we abruptly stop following a zero.

\section{Let's think about energy}
Let's define a lyapunov function quadratically, as a simple representative of energy. We can also make a non-linear representation later. 
\begin{equation}
V = x^T P x
\end{equation}
and time derivative
\begin{equation}
\dot V = x^T A^T P x + x^T P A x = x^T (A^T P + P A) x = x^T Q x 
\end{equation}
consider that P is symmetric and positive definite. It has real eigenvalues. It is normal. It is diagonalizable with unitary matrices.
\begin{equation}
P= u^* \Sigma u
\end{equation}
where 
\begin{equation}
u^* u = u u^* =I
\end{equation}
The relationship $\dot V/V$ can be written
\begin{equation}
\frac {\dot V}{V} = \frac{x^* Q x}{x^* P x} = \frac{x^* u^* \sqrt{\Sigma} \sqrt{\Sigma}^{-1} u Q u^*\sqrt{\Sigma}^{-1} \sqrt{\Sigma}u x}{x^* u^* \sqrt{\Sigma}I \sqrt{\Sigma} u x}
\end{equation}

Real quick, what if x is an eigenvector of A corresponding to $\lambda$: 
\begin{align}
\frac {\dot V}{V} & = \frac{x^* [A^*P + PA] x}{x^* P x}\\
&= \frac{x^*A^*P x + x^* PA x}{x^* P x}\\
&= \frac{x^*\bar \lambda P x + x^* P \lambda x}{x^* P x}\\
&= (\bar \lambda + \lambda)\frac{ x^* P x}{x^* P x}\\
&= (\bar \lambda + \lambda)\\
&= 2 \Re\{\lambda\} 
\end{align}
Great!
What if x is a sum of two eigenvectors of A $x=e_1 + e_2$ corresponding to $\lambda_1$ and $\lambda_2$ respectively

\begin{align}
\frac {\dot V}{V} & = \frac{x^* [A^*P + PA] x}{x^* P x}\\
& = \frac{x^* A^*P x + x^*PA x}{x^* P x}\\
& = \frac{(e_1^* \bar \lambda_1 + e_2^* \bar \lambda_2)P x + x^*P(\lambda_1 e_1+\lambda_2 e_2)}{x^* P x}\\
\end{align}


Wait!!
A lyapunov function cannot always decrease at a rate proportional to its value, because when a second order lyapunov function is stopped at the limit of the spring deflection the rate of change of the lyapunov function is instantaneously zero!!!! 

Lyapunov functions themselves cannot guarantee that the energy in high order unmodeled dynamics decays proportional to its value, and thus we need some other way to show that the amount of umodeled energy does not accumulate over time! This is important to calculating the bound on the unmodeled energy, which is in turn required to calculate the robustification of the playspace. Perhaps we could introduce some expected value, or some averaging over a single oscillation. A single oscillation guarantees that energy is transfered to velocity and is subject to damping. Perhaps too we could look at the complex decomposition of a damped wave. Perhaps the absolute values of the two rotating complex numbers are still decreasing, drawing a bound over their real sum.

There is a unique lyapunov function for a second order system which has the property $\dot V + \mu V = 0$, and this $\mu$ is unique as well. The $\mu$ must satisfy a relation with the damping coefficient of the poles of the second order system. This lyapunov function represents the decay envelope for the system's response! For every possible system this bound is unique. For every damping coefficient, $\mu$ is the same, but the shape of the quadratic form will change---relative to position squared, velocity squared and position times velocity will have different weights. Using a change of state transformation to forcibly diagonalize the second order system in the complex field, this matrix's structure may be more clearly visible.

Can a bound on the real parts of the poles of the unmodeled dynamics really be guaranteed by looking at simple system results? We consider the case of an under damped resonance with a very small relationship to input. A resonance which is unlikely to be excited highly relative to other frequencies in an impulse response. After receiving an impulse, the system's modes will all resonante, and the oscillation will begin to die off based on their real parts. The output will be dominated by the loudest, most well-excited, poles. Smaller poles may persist for a long time, but with magnitudes which are comparable to noise!

Suppose we design a different test: we feed in all-pass noise. If the system is stable the variance of the output should reach a steady state. However, if the system has any unstable poles or any un-damped poles, these poles will slowly be, on average, excited more and more by the noise untill they become noticable in the output. No matter how tiny, an un-damped pole will begin to grow in magnitude with a brownian motion. Brownian motion is unbounded. Thus we can conclude that if noise input causes bounded output after a really long time, then the system has no perfectly undamped resonances. 

If we use all pass noise, and we have a pole which has a real part higher than our expected magnitude, then it is not clear that this will be a detectible phenomenon. If we fit an arma model to the output, we will see the power spectrum of the system. Will this power spectrum help us bound the real-part pole locations? 


But for every Q there exists an X such that $A^*X+XA+Q=0$, so there exists one such that $A^*X+XA+K^*XK=0$ for any $K$. For every Q, X can be found (assuming the system is stable). Is it true that for every $K$ and $X$ can be found? What if $K$ is limited by the real part of the eigenvalues of the matrix A? If so, then maybe our intuition can persist. Maybe our intutition can persist for the energy function too, the one which obeys conservation of energy, if we can construct a suitable K. Maybe K just has to be complex. Energy does not itself decrease constantly, but maybe it can be bounded by something which does. This can also result in a bound on energy, naturally.

Which is a Raleigh quotient for indetermanent $\xi = \sqrt{\Sigma} u x$

\begin{align}
\dot V / V & = \mathrm{R}[\sqrt{\Sigma}^{-1} u (A^* u^* \Sigma u + u^* \Sigma u A) u^T\sqrt{\Sigma}^{-1} ]\\
& = \mathrm{R}[\sqrt{\Sigma}^{-1} u A^* u^* \sqrt{\Sigma}  + \sqrt{\Sigma} u A u^* \sqrt{\Sigma}^{-1} ]
\end{align}
Where $A^\prime = \sqrt{\Sigma} u A u^* \sqrt{\Sigma}^{-1}$ is just a similarity transform of $A$, which preserves the eigenvalues.
As with any matrix, we can split A into a hermetian and skew hermetian portion
\begin{equation}
A = A_h + A_s = \frac{A+A^*}{2}+ \frac{A-A^*}2
\end{equation}
\begin{align}
\dot V/V & = \mathrm{R}[A^{\prime *} +A^\prime ]\\
 & = \mathrm{R}[A_h^{\prime}-A_s^{\prime} +A_h^\prime +A_s^\prime ]
\end{align}

suppose $v$ is a unit eigenvector of $A$ corresponding to $\lambda$.
\begin{align}
\dot V/V (\xi = \sqrt{\Sigma} u v) & = \xi^* [\sqrt{\Sigma}^{-1} u A^* u^* \sqrt{\Sigma}  + \sqrt{\Sigma} u A u^* \sqrt{\Sigma}^{-1}] \xi\\
&= v^* \bar \lambda u^* \sqrt{\Sigma}\sqrt{\Sigma}u v +  v^* u^* \sqrt{\Sigma} \sqrt{\Sigma} u \lambda v
\end{align}

\begin{figure}
\includegraphics[width=\columnwidth]{figs/playspace_in_subspace}
\caption{Visualization of high dimensional playspace boundary.}
\end{figure}

\bibliographystyle{bib/IEEEtran}
\bibliography{bib/IEEEabrv,bib/plan,bib/hds,bib/wbc}

\end{document}


