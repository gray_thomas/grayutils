/*
 * ScalarReciprocalOperator.hpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SCALARRECIPROCALOPERATOR_HPP_
#define SCALARRECIPROCALOPERATOR_HPP_


#include "ScalarOperator.hpp"
#include "NetworkStateVector.hpp"
using namespace std;

namespace uta
{
namespace networkUtils
{

class ScalarReciprocalOperator: public ScalarOperator
{
public:
	ScalarReciprocalOperator(NetworkState& dataStore,SingleIndexNetworkDataProxy* input);
	~ScalarReciprocalOperator();

	virtual void forwardCalculate(void);
	virtual void reverseCalculate(void);
};
SingleIndexNetworkDataProxy& operator/(SingleIndexNetworkDataProxy& num, SingleIndexNetworkDataProxy&den);
}
}


#endif /* SCALARRECIPROCALOPERATOR_HPP_ */
