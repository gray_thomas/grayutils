/*
 * ScalarNegationOperator.hpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SCALARNEGATIONOPERATOR_HPP_
#define SCALARNEGATIONOPERATOR_HPP_


#include "ScalarOperator.hpp"
#include "NetworkStateVector.hpp"
using namespace std;

namespace uta
{
namespace networkUtils
{

class ScalarNegationOperator: public ScalarOperator
{
public:
	ScalarNegationOperator(NetworkState& dataStore,SingleIndexNetworkDataProxy* input);
	~ScalarNegationOperator();

	virtual void forwardCalculate(void);
	virtual void reverseCalculate(void);
};
SingleIndexNetworkDataProxy& operator-(SingleIndexNetworkDataProxy& object);
SingleIndexNetworkDataProxy& operator-(SingleIndexNetworkDataProxy& object1,SingleIndexNetworkDataProxy& object2);
}
}


#endif /* SCALARNEGATIONOPERATOR_HPP_ */
