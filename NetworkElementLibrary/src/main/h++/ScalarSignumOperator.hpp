/*
 * ScalarSignumOperator.hpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SCALARSIGNUMOPERATOR_HPP_
#define SCALARSIGNUMOPERATOR_HPP_

#include "ScalarOperator.hpp"
#include "NetworkStateVector.hpp"
using namespace std;

namespace uta
{
namespace networkUtils
{

class ScalarSignumOperator: public ScalarOperator
{
	double epsilon;
	double reciprocalEpsilon;
public:
	ScalarSignumOperator(NetworkState& dataStore,SingleIndexNetworkDataProxy* input,double epsilon);
	~ScalarSignumOperator();

	virtual void forwardCalculate(void);
	virtual void reverseCalculate(void);
};
SingleIndexNetworkDataProxy& sign(SingleIndexNetworkDataProxy&);
SingleIndexNetworkDataProxy& sign(SingleIndexNetworkDataProxy&, double);
}
}


#endif /* SCALARSIGNUMOPERATOR_HPP_ */
