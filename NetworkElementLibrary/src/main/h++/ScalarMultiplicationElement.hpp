/*
 * ScalarMultiplicationElement.hpp
 *
 *    Created on: Nov 19, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SCALARMULTIPLICATIONELEMENT_HPP_
#define SCALARMULTIPLICATIONELEMENT_HPP_

#include "ScalarElement.hpp"
#include "NetworkStateVector.hpp"
#include <cstdarg>
using namespace std;

namespace uta
{
namespace networkUtils
{

class ScalarMultiplicationElement: public ScalarElement
{
public:
	ScalarMultiplicationElement(NetworkState& dataStore);
	~ScalarMultiplicationElement();

	virtual void forwardCalculate(void);
	virtual void reverseCalculate(void);
};

SingleIndexNetworkDataProxy& operator*(SingleIndexNetworkDataProxy& one, SingleIndexNetworkDataProxy& two);
}
}
#endif /* SCALARMULTIPLICATIONELEMENT_HPP_ */
