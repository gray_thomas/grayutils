/*
 * ScalarCosineOperator.hpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SCALARCOSINEOPERATOR_HPP_
#define SCALARCOSINEOPERATOR_HPP_

#include "ScalarOperator.hpp"
#include "NetworkStateVector.hpp"
using namespace std;

namespace uta
{
namespace networkUtils
{

class ScalarCosineOperator: public ScalarOperator
{
public:
	ScalarCosineOperator(NetworkState& dataStore, SingleIndexNetworkDataProxy* input);
	~ScalarCosineOperator();

	virtual void forwardCalculate(void);
	virtual void reverseCalculate(void);
};
SingleIndexNetworkDataProxy& cos(SingleIndexNetworkDataProxy&input);
}
}

#endif /* SCALARCOSINEOPERATOR_HPP_ */
