/*
 * ScalarSineOperator.hpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SCALARSINEOPERATOR_HPP_
#define SCALARSINEOPERATOR_HPP_

#include "ScalarOperator.hpp"
#include "NetworkStateVector.hpp"
using namespace std;

namespace uta
{
namespace networkUtils
{

class ScalarSineOperator: public ScalarOperator
{
public:
	ScalarSineOperator(NetworkState& dataStore, SingleIndexNetworkDataProxy* input);
	~ScalarSineOperator();

	virtual void forwardCalculate(void);
	virtual void reverseCalculate(void);
};
SingleIndexNetworkDataProxy& sin(SingleIndexNetworkDataProxy& input);

}
}

#endif /* SCALARSINEOPERATOR_HPP_ */
