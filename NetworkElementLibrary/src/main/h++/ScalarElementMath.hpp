/*
 * ScalarElementMath.hpp
 *
 *  Created on: Mar 25, 2014
 *      Author: Gray
 */

#ifndef SCALARELEMENTMATH_HPP_
#define SCALARELEMENTMATH_HPP_
#include <ScalarAdditionElement.hpp>
#include <ScalarCosineOperator.hpp>
#include <ScalarMultiplicationElement.hpp>
#include <ScalarNegationOperator.hpp>
#include <ScalarReciprocalOperator.hpp>
#include <ScalarSigmoidOperator.hpp>
#include <ScalarSignumOperator.hpp>
#include <ScalarSineOperator.hpp>
namespace uta{
namespace networkUtils{
typedef SingleIndexNetworkDataProxy NetReal;
}
}
#endif /* SCALARELEMENTMATH_HPP_ */
