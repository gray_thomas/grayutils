/*
 * ScalarSigmoidOperator.hpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SCALARSIGMOIDOPERATOR_HPP_
#define SCALARSIGMOIDOPERATOR_HPP_

#include "ScalarOperator.hpp"
#include "NetworkStateVector.hpp"
using namespace std;

namespace uta
{
namespace networkUtils
{

class ScalarSigmoidOperator: public ScalarOperator
{
public:
	ScalarSigmoidOperator(NetworkState& dataStore, SingleIndexNetworkDataProxy* input);
	~ScalarSigmoidOperator();

	virtual void forwardCalculate(void);
	virtual void reverseCalculate(void);
};
}
}

#endif /* SCALARSIGMOIDOPERATOR_HPP_ */
