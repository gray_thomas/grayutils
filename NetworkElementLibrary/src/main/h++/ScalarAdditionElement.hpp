/*
 * ScalarAdditionElement.hpp
 *
 *    Created on: Nov 19, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SCALARADDITIONELEMENT_HPP_
#define SCALARADDITIONELEMENT_HPP_

#include "ScalarElement.hpp"
#include "NetworkStateVector.hpp"
using namespace std;

namespace uta
{
namespace networkUtils
{

class ScalarAdditionElement: public ScalarElement
{
public:
	ScalarAdditionElement(NetworkState& dataStore);
	~ScalarAdditionElement();

	virtual void forwardCalculate(void);
	virtual void reverseCalculate(void);
};

SingleIndexNetworkDataProxy& operator+(SingleIndexNetworkDataProxy& one, SingleIndexNetworkDataProxy& two);

}
}


#endif /* SCALARADDITIONELEMENT_HPP_ */
