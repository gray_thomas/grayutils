/*
 * ScalarNegationOperator.cpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "ScalarNegationOperator.hpp"
#include <ScalarAdditionElement.hpp>
#include <vector>
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "NetworkStateVector.hpp"
#include "ScalarOperator.hpp"
#include <cmath>

using namespace std;
namespace uta
{
namespace networkUtils
{

ScalarNegationOperator::ScalarNegationOperator(NetworkState& dataStore, SingleIndexNetworkDataProxy* input) :
		ScalarOperator(dataStore, input)
{
}
ScalarNegationOperator::~ScalarNegationOperator()
{
}
void ScalarNegationOperator::forwardCalculate(void)
{
	dataStore.setValue(index, -dataStore.getValue(forwardInputIndexes[0]));
}
void ScalarNegationOperator::reverseCalculate(void)
{
	dataStore.addToPartial(forwardInputIndexes[0], -dataStore.getPartial(index));
}
SingleIndexNetworkDataProxy& operator-(SingleIndexNetworkDataProxy& object)
{
	return *((new ScalarNegationOperator(object.getNetworkState(), &object))->getScalarResult());
}

SingleIndexNetworkDataProxy& operator-(SingleIndexNetworkDataProxy& object1, SingleIndexNetworkDataProxy& object2)
{
	SingleIndexNetworkDataProxy* negativeObject2 = (new ScalarNegationOperator(object1.getNetworkState(), &object2))
			->getScalarResult();
	ScalarAdditionElement* sum = new ScalarAdditionElement(object1.getNetworkState());
	sum->addInput(&object1);
	sum->addInput(negativeObject2);
	return *sum->getScalarResult();
}

}
}

