/*
 * ScalarReciprocalOperator.cpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "ScalarReciprocalOperator.hpp"
#include <vector>
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "NetworkStateVector.hpp"
#include "ScalarOperator.hpp"
#include "ScalarMultiplicationElement.hpp"
#include <cmath>

using namespace std;
namespace uta
{
namespace networkUtils
{

ScalarReciprocalOperator::ScalarReciprocalOperator(NetworkState& dataStore, SingleIndexNetworkDataProxy* input) :
			ScalarOperator(dataStore, input)
{
}
ScalarReciprocalOperator::~ScalarReciprocalOperator()
{
}
void ScalarReciprocalOperator::forwardCalculate(void)
{
	double input = dataStore.getValue(forwardInputIndexes[0]);
	dataStore.setValue(index, 1.0 / input);
}
void ScalarReciprocalOperator::reverseCalculate(void)
{
	double partial = dataStore.getPartial(index);
	double value = dataStore.getValue(index);
	double derivative = -value*value;
	dataStore.addToPartial(forwardInputIndexes[0], partial * derivative);
}
SingleIndexNetworkDataProxy& operator/(SingleIndexNetworkDataProxy& num, SingleIndexNetworkDataProxy&den)
{
	return num * *((new ScalarReciprocalOperator(den.getNetworkState(),&den))->getScalarResult());
}

}
}

