/*
 * ScalarCosineOperator.cpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "ScalarCosineOperator.hpp"
#include <vector>
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "NetworkStateVector.hpp"
#include "ScalarOperator.hpp"
#include <cmath>

using namespace std;
namespace uta
{
namespace networkUtils
{

ScalarCosineOperator::ScalarCosineOperator(NetworkState& dataStore, SingleIndexNetworkDataProxy* input) :
			ScalarOperator(dataStore, input)
{
}
ScalarCosineOperator::~ScalarCosineOperator()
{
}
void ScalarCosineOperator::forwardCalculate(void)
{
	double input = dataStore.getValue(forwardInputIndexes[0]);
	dataStore.setValue(index, std::cos(input));
}
void ScalarCosineOperator::reverseCalculate(void)
{
	double partial = dataStore.getPartial(index);
	double input = dataStore.getValue(forwardInputIndexes[0]);
	double derivative = -sin(input);
	dataStore.addToPartial(forwardInputIndexes[0], partial * derivative);
}
SingleIndexNetworkDataProxy& cos(SingleIndexNetworkDataProxy&input)
{
	return *((new ScalarCosineOperator(input.getNetworkState(),&input))->getScalarResult());
}

}
}


