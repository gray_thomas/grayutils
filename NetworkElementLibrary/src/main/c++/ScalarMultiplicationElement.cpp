/*
 * ScalarMultiplicationElement.cpp
 *
 *    Created on: Nov 19, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "ScalarMultiplicationElement.hpp"
#include <vector>
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "NetworkStateVector.hpp"
#include "ScalarElement.hpp"

using namespace std;
namespace uta
{
namespace networkUtils
{

ScalarMultiplicationElement::ScalarMultiplicationElement(NetworkState& dataStore) :
			ScalarElement(dataStore)
{
}
ScalarMultiplicationElement::~ScalarMultiplicationElement()
{
}
void ScalarMultiplicationElement::forwardCalculate(void)
{
	double value = 1;
	for (unsigned i = 0; i < forwardInputIndexes.size(); i++)
	{
		value *= dataStore.getValue(forwardInputIndexes[i]);
	}
	dataStore.setValue(index, value);
}
void ScalarMultiplicationElement::reverseCalculate(void)
{
	for (unsigned i = 0; i < forwardInputIndexes.size(); i++)
	{
		double partial = dataStore.getPartial(index);
		for (unsigned j = 0; j < forwardInputIndexes.size(); j++)
		{
			if (i != j)
				partial *= dataStore.getValue(forwardInputIndexes[j]);
		}
		dataStore.addToPartial(forwardInputIndexes[i], partial);
	}
}

SingleIndexNetworkDataProxy& operator*(SingleIndexNetworkDataProxy& one, SingleIndexNetworkDataProxy& two)
{
	ScalarMultiplicationElement* prod = new ScalarMultiplicationElement(one.getNetworkState());
	prod->addInput(&one);
	prod->addInput(&two);
	return *prod->getScalarResult();
}


}
}

