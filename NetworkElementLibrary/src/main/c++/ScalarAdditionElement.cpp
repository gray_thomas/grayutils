/*
 * ScalarAdditionElement.cpp
 *
 *    Created on: Nov 19, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */


#include "ScalarAdditionElement.hpp"
#include <vector>
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "NetworkStateVector.hpp"
#include "ScalarElement.hpp"

using namespace std;
namespace uta
{
namespace networkUtils
{

ScalarAdditionElement::ScalarAdditionElement(NetworkState& dataStore) :
			ScalarElement(dataStore)
{
}
ScalarAdditionElement::~ScalarAdditionElement()
{
}
void ScalarAdditionElement::forwardCalculate(void)
{
	dataStore.setValue(index, 0.0);
	for (unsigned i = 0; i < forwardInputIndexes.size(); i++)
	{
		dataStore.addToValue(index, dataStore.getValue(forwardInputIndexes[i]));
	}
}
void ScalarAdditionElement::reverseCalculate(void)
{
	for (unsigned i = 0; i < forwardInputIndexes.size(); i++)
	{
		dataStore.addToPartial(forwardInputIndexes[i], dataStore.getPartial(index));
	}
}

SingleIndexNetworkDataProxy& operator+(SingleIndexNetworkDataProxy& one, SingleIndexNetworkDataProxy& two)
{
	ScalarAdditionElement* sum = new ScalarAdditionElement(one.getNetworkState());
	sum->addInput(&one);
	sum->addInput(&two);
	return *sum->getScalarResult();
}


}
}


