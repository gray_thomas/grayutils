/*
 * ScalarSigmoidOperator.cpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "ScalarSigmoidOperator.hpp"
#include <vector>
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "NetworkStateVector.hpp"
#include "ScalarElement.hpp"
#include <cmath>

using namespace std;
namespace uta
{
namespace networkUtils
{

ScalarSigmoidOperator::ScalarSigmoidOperator(NetworkState& dataStore, SingleIndexNetworkDataProxy* input) :
			ScalarOperator(dataStore, input)
{
}
ScalarSigmoidOperator::~ScalarSigmoidOperator()
{
}
void ScalarSigmoidOperator::forwardCalculate(void)
{
	double input = dataStore.getValue(forwardInputIndexes[0]);
	dataStore.setValue(index, tanh(input));
}
void ScalarSigmoidOperator::reverseCalculate(void)
{
	double partial = dataStore.getPartial(index);
	double value = dataStore.getValue(index);
	double derivative = (1 - value) * (1 + value);
	dataStore.addToPartial(forwardInputIndexes[0], partial * derivative);

}

}
}

