/*
 * ScalarSineOperator.cpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */


#include "ScalarSineOperator.hpp"
#include <vector>
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "NetworkStateVector.hpp"
#include "ScalarOperator.hpp"
#include <math.h>

using namespace std;
namespace uta
{
namespace networkUtils
{

ScalarSineOperator::ScalarSineOperator(NetworkState& dataStore, SingleIndexNetworkDataProxy* input) :
			ScalarOperator(dataStore, input)
{
}
ScalarSineOperator::~ScalarSineOperator()
{
}
void ScalarSineOperator::forwardCalculate(void)
{
	double input = dataStore.getValue(forwardInputIndexes[0]);
	dataStore.setValue(index, std::sin(input));
}
void ScalarSineOperator::reverseCalculate(void)
{
	double partial = dataStore.getPartial(index);
	double input = dataStore.getValue(forwardInputIndexes[0]);
	double derivative = std::cos(input);
	dataStore.addToPartial(forwardInputIndexes[0], partial * derivative);
}
SingleIndexNetworkDataProxy& sin(SingleIndexNetworkDataProxy& input)
{
	return *((new ScalarSineOperator(input.getNetworkState(),&input))->getScalarResult());
}

}
}

