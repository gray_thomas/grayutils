/*
 * ScalarSignumOperator.cpp
 *
 *    Created on: Nov 22, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "ScalarSignumOperator.hpp"
#include <vector>
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "NetworkStateVector.hpp"
#include "ScalarOperator.hpp"
#include <cmath>

using namespace std;
namespace uta
{
namespace networkUtils
{

ScalarSignumOperator::ScalarSignumOperator(NetworkState& dataStore, SingleIndexNetworkDataProxy* input,
											double epsilon) :
			ScalarOperator(dataStore,input),
			epsilon(epsilon),
			reciprocalEpsilon(1.0 / epsilon)
{
}
ScalarSignumOperator::~ScalarSignumOperator()
{
}
void ScalarSignumOperator::forwardCalculate(void)
{
	double input = dataStore.getValue(forwardInputIndexes[0]);
	dataStore.setValue(index, tanh(input * reciprocalEpsilon));
}
void ScalarSignumOperator::reverseCalculate(void)
{
	double partial = dataStore.getPartial(index);
	double value = dataStore.getValue(index);
	double derivative = (1.0 - value) * (1.0 + value);
	dataStore.addToPartial(forwardInputIndexes[0], partial * derivative * reciprocalEpsilon);

}
SingleIndexNetworkDataProxy& sign(SingleIndexNetworkDataProxy& other)
{
	return *((new ScalarSignumOperator(other.getNetworkState(),&other,1e-6))->getScalarResult());
}
SingleIndexNetworkDataProxy& sign(SingleIndexNetworkDataProxy& other,double scale)
{
	return *((new ScalarSignumOperator(other.getNetworkState(),&other,scale))->getScalarResult());
}

}
}

