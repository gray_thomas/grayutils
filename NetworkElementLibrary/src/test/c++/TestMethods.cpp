/*
 * TestMethods.cpp
 *
 *    Created on: Nov 19, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "TestMethods.hpp"
#include "TestUtils.hpp"
#include "TestOperators.hpp"
#include <cmath>
#include <exception>
#include <iostream>
#include <memory>
#include <new>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>
#include <random>

#include "NetworkDataProxy.hpp"
#include "ScalarAdditionElement.hpp"
#include "ScalarMultiplicationElement.hpp"
#include "ScalarNegationOperator.hpp"
#include "ScalarReciprocalOperator.hpp"
#include "ScalarSigmoidOperator.hpp"
#include "ScalarSignumOperator.hpp"
#include "ScalarSineOperator.hpp"
#include "TrainableScalarElement.hpp"
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "Network.hpp"
#include "NetworkStateVector.hpp"
#include "ScalarElement.hpp"
#include "ScalarOperator.hpp"
#include "ScalarCosineOperator.hpp"
#include "TestUtils.hpp"

#define PI 3.14159265395

using namespace std;
using namespace uta::networkUtils;
using namespace uta::testUtils;
namespace uta
{
namespace networkUtils
{
namespace tests
{
void additionTest(void)
{
	cout << "-------Running addition test:------------------------" << endl;
	NetworkStateVector state;
	Network* myNet = new Network(state, 1);
	ScalarAdditionElement* adder1 = new ScalarAdditionElement(state);
	ScalarAdditionElement* adder2 = new ScalarAdditionElement(state);
	ScalarAdditionElement* adder3 = new ScalarAdditionElement(state);

	SingleIndexNetworkDataProxy* myNetInput1 = myNet->getNetworkInput(0);
	adder1->addInput(myNetInput1);
	adder3->addInput(myNetInput1);
	adder2->addInput(adder1->getScalarResult());
	adder2->addInput(adder3->getScalarResult());
	myNet->setAsOutput(adder2->getScalarResult());
	// (netI0->add1, netI0->add3)->add2->netO

	myNet->setInput(0, 22.0);
	myNet->executeForwardNetwork();
	double output = myNet->getOutput(0);

	cout << "\t" << "forward execution complete!" << endl;
	state.print();
	cout << "output is " << output << endl;
	if (abs(output - 44.0) > 1e-6)
		throw out_of_range("expect equals");
	cout << "as expected!!" << endl << endl;

	myNet->setOutputPartial(0, 1);
	myNet->executeGradientNetwork();
	double partialWithRespectToInput = myNet->getInputPartial(0);

	cout << "\t" << "gradient execution complete!" << endl;
	state.print();
	cout << "partialWithRespectToInput is " << partialWithRespectToInput << endl;
	if (abs(partialWithRespectToInput - 2.0) > 1e-6)
		throw out_of_range("expect equals");
	cout << "as expected!!" << endl;

	cout << "-------addition test finished without error----------" << endl;
}
void multiplicationTest(void)
{
	cout << "-------Running multiplication test:------------------" << endl;
	NetworkStateVector state;
	Network* myNet = new Network(state, 1);
	ScalarMultiplicationElement* mult1 = new ScalarMultiplicationElement(state);
	ScalarMultiplicationElement* mult2 = new ScalarMultiplicationElement(state);
	ScalarMultiplicationElement* mult3 = new ScalarMultiplicationElement(state);

	SingleIndexNetworkDataProxy* myNetInput1 = myNet->getNetworkInput(0);
	mult1->addInput(myNetInput1);
	mult3->addInput(myNetInput1);
	mult2->addInput(mult1->getScalarResult());
	mult2->addInput(mult3->getScalarResult());
	myNet->setAsOutput(mult2->getScalarResult());

	myNet->setInput(0, 2.0);
	myNet->executeForwardNetwork();
	double output = myNet->getOutput(0);

	cout << "\t" << "forward execution complete!" << endl;
	state.print();
	cout << "output is " << output << endl;
	if (abs(output - 4.0) > 1e-6)
		throw out_of_range("expect equals");
	cout << "as expected!!" << endl << endl;

	myNet->setOutputPartial(0, 1);
	myNet->executeGradientNetwork();
	double partialWithRespectToInput = myNet->getInputPartial(0);

	cout << "\t" << "gradient execution complete!" << endl;
	state.print();
	cout << "partialWithRespectToInput is " << partialWithRespectToInput << endl;
	if (abs(partialWithRespectToInput - 4.0) > 1e-6)
		throw out_of_range("expect equals");
	cout << "as expected!!" << endl;

	myNet->setOutputPartial(0, 1);
	myNet->executeGradientNetwork();
	double partialWithRespectToInput2 = myNet->getInputPartial(0);

	cout << "\t" << "gradient execution two in a row complete!" << endl;
	state.print();
	cout << "partialWithRespectToInput is " << partialWithRespectToInput2 << endl;
	if (abs(partialWithRespectToInput2 - 4.0) > 1e-6)
		throw out_of_range("expected them to match");
	cout << "as expected!!" << endl;

	cout << "-------multiplication test finished without error!---" << endl;
}
ostream& operator<<(ostream& x, vector<double> values)
{
	if (values.size() == 0)
	{
		return x << "[]";
	}
	x << "[ ";
	for (unsigned i = 0; i < values.size() - 1; i++)
		x << values[i] << ", ";
	x << values[values.size() - 1] << "]";
	return x;
}
void trainableScalarTest(void)
{
	cout << "-------Trainable scalar test: -----------------------" << endl;
	NetworkStateVector state;
	Network* myNet = new Network(state, 1);
	ScalarMultiplicationElement* mult1 = new ScalarMultiplicationElement(state);
	ScalarMultiplicationElement* mult2 = new ScalarMultiplicationElement(state);
	SingleIndexNetworkDataProxy* myNetInput1 = myNet->getNetworkInput(0);
	NoIndexNetworkDataProxy* myNetProxyGenerator = myNet->getGeneratorNetworkInputProxy();
	TrainableScalarElement* mult3 = new TrainableScalarElement(state, myNetProxyGenerator, 7.0);

	vector<double> trainable = state.getTrainableData();
	cout << "trainable data is : " << trainable << endl;
	trainable[0] = 5;
	state.setTrainableData(trainable);

	mult1->addInput(myNetInput1);
	mult2->addInput(mult1->getScalarResult());
	mult2->addInput(mult3->getScalarResult());
	myNet->setAsOutput(mult2->getScalarResult());

	myNet->setInput(0, 3.0);
	myNet->executeForwardNetwork();
	double output = myNet->getOutput(0);

	cout << "\t" << "forward execution complete!" << endl;
	state.print();
	cout << "output is " << output << endl;
	if (abs(output - 15.0) > 1e-6)
		throw out_of_range("expect equals");
	cout << "as expected!!" << endl << endl;

	myNet->setOutputPartial(0, 1);
	myNet->executeGradientNetwork();
	double partialWithRespectToInput = myNet->getInputPartial(0);

	cout << "\t" << "gradient execution complete!" << endl;
	state.print();
	cout << "partialWithRespectToInput is " << partialWithRespectToInput << endl;
	if (abs(partialWithRespectToInput - 5.0) > 1e-6)
		throw out_of_range("expect equals");
	cout << "as expected!!" << endl;

	myNet->setOutputPartial(0, 1);
	myNet->executeGradientNetwork();
	double partialWithRespectToInput2 = myNet->getInputPartial(0);
	vector<double> trainablePartials = state.getPartialsWithRespectToTrainableData();
	cout << "Trainable Gradient is " << trainablePartials << endl;
	double partialWithRespectToTrainable = trainablePartials[0];

	cout << "\t" << "gradient execution two in a row complete!" << endl;
	state.print();
	cout << "partialWithRespectToInput is " << partialWithRespectToInput2 << endl;
	cout << "partialWithRespectToTrainable is " << partialWithRespectToTrainable << endl;
	if (abs(partialWithRespectToInput2 - 5.0) > 1e-6)
		throw out_of_range("expected them to match");
	if (abs(partialWithRespectToTrainable - 3.0) > 1e-6)
		throw out_of_range("expected them to match");
	cout << "as expected!!" << endl;

	cout << "-------trainable scalar test finished without error!-" << endl;
}

class NetworkBuiltInConstructorExample: public uta::networkUtils::Network
{
public:
	NetworkBuiltInConstructorExample(NetworkStateVector& state) :
				Network(state, 1)
	{
		ScalarMultiplicationElement* mult1 = new ScalarMultiplicationElement(state);
		ScalarMultiplicationElement* mult2 = new ScalarMultiplicationElement(state);
		SingleIndexNetworkDataProxy* myNetInput1 = getNetworkInput(0);
		NoIndexNetworkDataProxy* myNetProxyGenerator = getGeneratorNetworkInputProxy();
		TrainableScalarElement* mult3 = new TrainableScalarElement(state, myNetProxyGenerator, 7.0);

		vector<double> trainable = state.getTrainableData();
		cout << "trainable data is : " << trainable << endl;
		trainable[0] = 5;
		state.setTrainableData(trainable);
		mult1->addInput(myNetInput1);

		mult2->addInput(mult1->getScalarResult());
		mult2->addInput(mult3->getScalarResult());
		setAsOutput(mult2->getScalarResult());
	}
};

void networkBuiltInConstructorTest(void)
{
	cout << "-------Network Built In Constructor Test:------------" << endl;
	NetworkStateVector state;
	Network* myNet = new NetworkBuiltInConstructorExample(state);

	myNet->setInput(0, 3.0);
	myNet->executeForwardNetwork();
	double output = myNet->getOutput(0);

	cout << "\t" << "forward execution complete!" << endl;
	state.print();
	cout << "output is " << output << endl;
	if (abs(output - 15.0) > 1e-6)
		throw out_of_range("expect equals");
	cout << "as expected!!" << endl << endl;

	myNet->setOutputPartial(0, 1);
	myNet->executeGradientNetwork();
	double partialWithRespectToInput = myNet->getInputPartial(0);

	cout << "\t" << "gradient execution complete!" << endl;
	state.print();
	cout << "partialWithRespectToInput is " << partialWithRespectToInput << endl;
	if (abs(partialWithRespectToInput - 5.0) > 1e-6)
		throw out_of_range("expect equals");
	cout << "as expected!!" << endl;

	myNet->setOutputPartial(0, 1);
	myNet->executeGradientNetwork();
	double partialWithRespectToInput2 = myNet->getInputPartial(0);
	vector<double> trainablePartials = state.getPartialsWithRespectToTrainableData();
	cout << "Trainable Gradient is " << trainablePartials << endl;
	double partialWithRespectToTrainable = trainablePartials[0];

	cout << "\t" << "gradient execution two in a row complete!" << endl;
	state.print();
	cout << "partialWithRespectToInput is " << partialWithRespectToInput2 << endl;
	cout << "partialWithRespectToTrainable is " << partialWithRespectToTrainable << endl;
	if (abs(partialWithRespectToInput2 - 5.0) > 1e-6)
		throw out_of_range("expected them to match");
	if (abs(partialWithRespectToTrainable - 3.0) > 1e-6)
		throw out_of_range("expected them to match");
	cout << "as expected!!" << endl;

	cout << "-------Network Built In Constructor Test is done!----" << endl;
}

class NetworkIOExample: public uta::networkUtils::Network
{
public:
	NetworkIOExample(NetworkStateVector& state) :
				Network(state, 5)
	{
		SingleIndexNetworkDataProxy* ins[5];
		ScalarMultiplicationElement* squares[5];
		TrainableScalarElement* weights[5];
		NoIndexNetworkDataProxy* proxy = getGeneratorNetworkInputProxy();
		for (unsigned i = 0; i < 5; i++)
		{
			ins[i] = getNetworkInput(i);
			weights[i] = new TrainableScalarElement(state, proxy, 0.5);
			squares[i] = new ScalarMultiplicationElement(state);
			squares[i]->addInput(ins[i]);
			squares[i]->addInput(ins[i]);
			squares[i]->addInput(weights[i]->getScalarResult());
			setAsOutput(squares[i]->getScalarResult());
		}
	}
};

#define INPUT(i)  (2.0 * i + 6.0)
#define WEIGHT(i) (i + 1.4)
#define OUTPART(i) (32 - 7.3 * i)
void networkVectorizedInputOutputTest(void)
{
	cout << "-------Network Input Output Test: -------------------" << endl;
	NetworkStateVector state;
	Trainable* network = new NetworkIOExample(state);

	cout << "======Test1: Weights are accurately ordered.=========" << endl;
	vector<double> weights = network->getWeights();
	for (unsigned i = 0; i < 5; i++)
		assertEquals("weights equal", weights[i], 0.5, 1e-6);
	for (unsigned i = 0; i < 5; i++)
		weights[i] = WEIGHT(i);
	network->setWeights(weights);
	weights = network->getWeights();
	for (unsigned i = 0; i < 5; i++)
		assertEquals("weights equal", weights[i], WEIGHT(i), 1e-6);
	cout << "======Fin.===========================================" << endl;
	cout << "======Test2: Partials are accurately ordered.========" << endl;
	cout << "===========:A Checking Outputs ======================" << endl;
	for (unsigned i = 0; i < 5; i++)
		network->setInput(i, INPUT(i));
	network->calculateOutputs();
	vector<double> outputs = network->getOutputs();
	for (unsigned i = 0; i < 5; i++)
		assertEquals("outputs equal", outputs[i], WEIGHT(i) * INPUT(i) * INPUT(i), 1e-6);
	for (unsigned i = 0; i < 5; i++)
		outputs[i] = (network->getOutput(i));
	for (unsigned i = 0; i < 5; i++)
		assertEquals("outputs equal", outputs[i], WEIGHT(i) * INPUT(i) * INPUT(i), 1e-6);
	network->packOutputs(outputs);
	for (unsigned i = 0; i < 5; i++)
		assertEquals("outputs equal", outputs[i], WEIGHT(i) * INPUT(i) * INPUT(i), 1e-6);
	cout << "===========:B Checking Partials======================" << endl;
	vector<double> outputPartials(5);
	for (unsigned i = 0; i < 5; i++)
		outputPartials[i] = OUTPART(i);
	network->setOutputPartials(outputPartials);
	network->calculatePartials();
	vector<double> inputPartials(5);
	network->packInputPartials(inputPartials);
	for (unsigned i = 0; i < 5; i++)
		assertEquals("input partial", inputPartials[i], OUTPART(i) * WEIGHT(i) * 2 * INPUT(i), 1e-6);

	cout << "======Fin.===========================================" << endl;
	cout << "-------Network Input Output Test Complete!-----------" << endl;
}

}
}
}
