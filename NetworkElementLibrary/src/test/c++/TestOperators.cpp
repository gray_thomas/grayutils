/*
 * TestOperators.cpp
 *
 *    Created on: Nov 23, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "TestOperators.hpp"

#include <cmath>
#include <exception>
#include <iostream>
#include <memory>
#include <new>
#include <random>
#include <sstream>
#include <string>

#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "Network.hpp"
#include "NetworkStateVector.hpp"
#include "ScalarElement.hpp"
#include "ScalarOperator.hpp"
#include "TestUtils.hpp"
#include "NetworkDataProxy.hpp"
#include "ScalarCosineOperator.hpp"
#include "ScalarMultiplicationElement.hpp"
#include "ScalarNegationOperator.hpp"
#include "ScalarReciprocalOperator.hpp"
#include "ScalarSigmoidOperator.hpp"
#include "ScalarSignumOperator.hpp"
#include "ScalarSineOperator.hpp"

#define PI 3.14159265395

using namespace std;
using namespace uta::networkUtils;
using namespace uta::testUtils;
namespace uta
{
namespace networkUtils
{
namespace tests
{
class OperatorTestNetwork: public uta::networkUtils::Network
{
public:
	OperatorTestNetwork(NetworkStateVector& state) :
				Network(state, 7)
	{
		SingleIndexNetworkDataProxy* myNetInput1 = getNetworkInput(0);
		ScalarSigmoidOperator* sigmoid = new ScalarSigmoidOperator(state, myNetInput1);
		setAsOutput(sigmoid->getScalarResult());

		SingleIndexNetworkDataProxy* myNetInput2 = getNetworkInput(1);
		ScalarSignumOperator* signum = new ScalarSignumOperator(state, myNetInput2, 1e-2);
		setAsOutput(signum->getScalarResult());

		SingleIndexNetworkDataProxy* myNetInput3 = getNetworkInput(2);
		ScalarMultiplicationElement* square = new ScalarMultiplicationElement(state);
		square->addInput(myNetInput3);
		square->addInput(myNetInput3);
		setAsOutput(square->getScalarResult());

		SingleIndexNetworkDataProxy* myNetInput4 = getNetworkInput(3);
		ScalarReciprocalOperator* reciprocal = new ScalarReciprocalOperator(state, myNetInput4);
		setAsOutput(reciprocal->getScalarResult());

		SingleIndexNetworkDataProxy* myNetInput5 = getNetworkInput(4);
		ScalarNegationOperator* negation = new ScalarNegationOperator(state, myNetInput5);
		setAsOutput(negation->getScalarResult());

		SingleIndexNetworkDataProxy* myNetInput6 = getNetworkInput(5);
		ScalarSineOperator* sine = new ScalarSineOperator(state, myNetInput6);
		setAsOutput(sine->getScalarResult());

		SingleIndexNetworkDataProxy* myNetInput7 = getNetworkInput(6);
		ScalarCosineOperator* cosine = new ScalarCosineOperator(state, myNetInput7);
		setAsOutput(cosine->getScalarResult());

	}
	void testOperator(int index, string name, double input, double expectedOutput, double expectedDerivative)
	{
		setInput(index, input);
		executeForwardNetwork();
		stringstream message;
		message << name << "(" << input << ")";
		assertEquals(message.str(), expectedOutput, getOutput(index), 1e-6);
		setOutputPartial(index, 1.0);
		executeGradientNetwork();
		message << " partialDerivative";
		assertEquals(message.str(), expectedDerivative, getInputPartial(index), 1e-6);
	}

	void testSigmoidNetwork(double input, double expectedOutput, double expectedDerivative)
	{
		testOperator(0, "sigmoid", input, expectedOutput, expectedDerivative);
	}
	void testSignumNetwork(double input, double expectedOutput, double expectedDerivative)
	{
		testOperator(1, "signum", input, expectedOutput, expectedDerivative);
	}
	void testSquareNetwork(double input, double expectedOutput, double expectedDerivative)
	{
		testOperator(2, "square", input, expectedOutput, expectedDerivative);
	}
	void testReciprocalNetwork(double input, double expectedOutput, double expectedDerivative)
	{
		testOperator(3, "reciprocal", input, expectedOutput, expectedDerivative);
	}
	void testNegationNetwork(double input, double expectedOutput, double expectedDerivative)
	{
		testOperator(4, "negation", input, expectedOutput, expectedDerivative);
	}
	void testSineNetwork(double input, double expectedOutput, double expectedDerivative)
	{
		testOperator(5, "sine", input, expectedOutput, expectedDerivative);
	}
	void testCosineNetwork(double input, double expectedOutput, double expectedDerivative)
	{
		testOperator(6, "cosine", input, expectedOutput, expectedDerivative);
	}
};

void operatorTest(void)
{
	cout << "-------Sigmoid Operator Test: -----------------------" << endl;
	NetworkStateVector state;
	OperatorTestNetwork* myNet = new OperatorTestNetwork(state);

	myNet->testSigmoidNetwork(0, 0, 1);
	myNet->testSigmoidNetwork(0 + 1e-6, 0 + 1e-6, 1);
	myNet->testSigmoidNetwork(1, tanh(1), 1 - tanh(1) * tanh(1));
	myNet->testSigmoidNetwork(-1, tanh(-1), 1 - tanh(-1) * tanh(-1));
	myNet->testSigmoidNetwork(10, tanh(10), 1 - tanh(10) * tanh(10));
	myNet->testSigmoidNetwork(-10, tanh(-10), 1 - tanh(-10) * tanh(-10));
	myNet->testSigmoidNetwork(100, 1, 0);
	myNet->testSigmoidNetwork(-100, -1, 0);

	myNet->testSignumNetwork(1, 1, 0);
	myNet->testSignumNetwork(-1, -1, 0);
	myNet->testSignumNetwork(0, 0, 100);
	double eps = 1e-4;
	double scale = 100;
	myNet->testSignumNetwork(eps, scale * eps, scale * (1 - eps * eps * scale * scale));
	myNet->testSignumNetwork(-eps, -scale * eps, scale * (1 - eps * eps * scale * scale));

	myNet->testSquareNetwork(0, 0, 0);
	myNet->testSquareNetwork(1, 1, 2);
	myNet->testSquareNetwork(-1, 1, -2);
	myNet->testSquareNetwork(10, 100, 20);
	myNet->testSquareNetwork(-10, 100, -20);
	myNet->testSquareNetwork(1e4, 1e8, 2e4);
	myNet->testSquareNetwork(-1e4, 1e8, -2e4);
	myNet->testSquareNetwork(1e9, 1e18, 2e9);
	myNet->testSquareNetwork(-1e9, 1e18, -2e9);

	myNet->testReciprocalNetwork(1e1, 1e-1, -1e-2);
	myNet->testReciprocalNetwork(-1e1, -1e-1, -1e-2);
	myNet->testReciprocalNetwork(1e-1, 1e1, -1e2);
	myNet->testReciprocalNetwork(-1e-1, -1e1, -1e2);
	myNet->testReciprocalNetwork(1e4, 1e-4, -1e-8);
	myNet->testReciprocalNetwork(-1e4, -1e-4, -1e-8);
	myNet->testReciprocalNetwork(1e-4, 1e4, -1e8);
	myNet->testReciprocalNetwork(-1e-4, -1e4, -1e8);
	myNet->testReciprocalNetwork(0.25, 4, -16);
	myNet->testReciprocalNetwork(-0.25, -4, -16);
	eps = 1e-5;
	scale = -16;
	myNet->testReciprocalNetwork(0.25 + eps, 4 + scale * eps, -1 * (4 + scale * eps) * (4 + scale * eps));

	myNet->testNegationNetwork(1e1, -1e1, -1);
	myNet->testNegationNetwork(-1e1, 1e1, -1);

	myNet->testSineNetwork(0, 0, 1);
	myNet->testSineNetwork(PI, 0, -1);
	myNet->testSineNetwork(PI / 2, 1, 0);
	myNet->testSineNetwork(-PI / 2, -1, 0);

	myNet->testCosineNetwork(0, 1, 0);
	myNet->testCosineNetwork(PI, -1, 0);
	myNet->testCosineNetwork(PI / 2, 0, -1);
	myNet->testCosineNetwork(-PI / 2, 0, 1);

	cout << "-------Operator test completed successfully! --------" << endl;
}
}
}
}

