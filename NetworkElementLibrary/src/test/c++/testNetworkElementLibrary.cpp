/*
 * TestNetworkElementLibrary.cpp
 *
 *    Created on: Feb 15, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include <iostream>
#include <TestMethods.hpp>
#include <TestOperators.hpp>
int main(int argc, const char* argv[])
{
	std::cout << "TestNetworkElementLibrary start" << std::endl;
	uta::networkUtils::tests::additionTest();
	uta::networkUtils::tests::multiplicationTest();
	uta::networkUtils::tests::trainableScalarTest();
	uta::networkUtils::tests::networkBuiltInConstructorTest();
	uta::networkUtils::tests::networkVectorizedInputOutputTest();
	uta::networkUtils::tests::operatorTest();
	std::cout << "TestNetworkElementLibrary finish" << std::endl;
	return 0;
}
