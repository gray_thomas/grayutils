/*
 * TestOperators.hpp
 *
 *    Created on: Nov 23, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef TESTOPERATORS_HPP_
#define TESTOPERATORS_HPP_

namespace uta
{
namespace networkUtils
{
namespace tests
{

void operatorTest(void);

}
}
}


#endif /* TESTOPERATORS_HPP_ */
