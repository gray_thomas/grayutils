/*
 * TestMethods.hpp
 *
 *    Created on: Nov 19, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef TESTMETHODS_HPP_
#define TESTMETHODS_HPP_

namespace uta
{
namespace networkUtils
{
namespace tests
{
void additionTest(void);
void multiplicationTest(void);
void trainableScalarTest(void);
void networkBuiltInConstructorTest(void);
void networkVectorizedInputOutputTest(void);

void pendulumTest(void);
}
}
}

#endif /* TESTMETHODS_HPP_ */
