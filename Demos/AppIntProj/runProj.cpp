/*
 * runProj.cpp
 *
 *  Created on: May 2, 2014
 *      Author: Gray
 */
#include <iostream>
#include <SEANetwork.hpp>
#include <DyNetTrainer.hpp>
#include <DataSet.hpp>
using namespace std;
using namespace uta::networkUtils;
using namespace uta::networkUtils::dyNetUtils;
namespace uta
{
namespace demos
{
namespace appIntProj
{
void errorRecord(double error, double*trainingDSIns, double* trainingDSOuts, int i, string fileBase, int number,
		DataSet& trainingErrorDataSet, double& previousError)
{
	stringstream trainingName;
	trainingName << fileBase << number << "Ep" << i << "Tr.iodat";
	trainingDSIns[0] = i;
	trainingDSOuts[0] = error;
	trainingErrorDataSet.addLine(trainingDSIns, trainingDSOuts);
	previousError = error;
}
void setTrainingWeights(DyNetWithNames* seaNet, DyNetTrainer& trainer, double paramChangeTolerance, double stateChangeTolerance,
		double predictionErrorTolerance, double consistancyErrorTolerance, double measurementErrorTolerance)
{
	Eigen::VectorXd stdDevParamsDamping = seaNet->getStdDevParamsDamping();
	Eigen::VectorXd stdDevStateDamping = seaNet->getStdDevStateDamping();
	Eigen::VectorXd stdDevUpdateError = seaNet->getStdDevUpdateError();
	Eigen::VectorXd stdDevMeasurementError = seaNet->getStdDevMeasurementError();
	VectorXd predictionSubVec(6);
	predictionSubVec << 0.0, 0.0, stdDevUpdateError(2), 0.0, 0.0, stdDevUpdateError(5);
	VectorXd consistancySubVec(6);
	consistancySubVec << stdDevUpdateError(0), stdDevUpdateError(1), 0.0, stdDevUpdateError(3), stdDevUpdateError(4), 0.0;
	stdDevParamsDamping << seaNet->getStdDevParamsDamping() * paramChangeTolerance;
	stdDevStateDamping << seaNet->getStdDevStateDamping() * stateChangeTolerance; // moves states fast
	stdDevUpdateError << predictionSubVec * predictionErrorTolerance + consistancySubVec * consistancyErrorTolerance; //
	stdDevMeasurementError << seaNet->getStdDevMeasurementError() * measurementErrorTolerance;
	// We know that changing the measurement error equates to changing the filter's time constant.
	// However, it's not clear what happens when we change the consistency relative to the predictionError
	trainer.setParameterUncertainty(stdDevParamsDamping);
	trainer.setStateUpdateNoise(stdDevUpdateError);
	trainer.setStateUncertainty(stdDevStateDamping);
	trainer.setMeasurementNoise(stdDevMeasurementError);
}
void trainV3Net(DataSet& trainingSet, string fileBase, unsigned number, string trainingDataFileName)
{
	NetworkStateVector state;
//	DyNetWithNames* seaNet = new uta::demos::SEA::FullSEA(state, 0.0005);
//	DyNetWithNames* seaNet = new uta::demos::SEA::SEALowPass(state, 0.0005);
	DyNetWithNames* seaNet = new uta::demos::SEA::PositionDeflectionSEA(state, 0.0005);
	DyNetTrainerParameters params;
	params.trustRegionScaleParameter = 1.0e3;
	params.linearitySetPoint = 0.75;
	params.linearityMinimumError = 0.25;
	params.trustRegionPenalty = 0.2;
	params.trustRegionPositiveBump = 1.3;
	DyNetTrainer trainer(params, trainingSet, *seaNet);
	DyNetTrainerParameters validatorParams(params);
	validatorParams.trustRegionScaleParameter = 1e8;
	cout << "network memory size is " << trainer.viewSizes().memorySize << endl;
	cout << "dataset input size is " << trainingSet.viewInputSize() << endl;
	VectorXd IC = VectorXd::Zero(seaNet->viewStateNames().size());
	VectorXd states = initializeStatesZero(trainingSet, IC);
	trainer.setStates(states);
	cout << "network input size is " << seaNet->inputSize() << endl;

	DataSet trainingErrorDataSet(1, 1); // epoch: dyNetTrainingError, shootingtrainingError, shootingvalidiationError
	double trainingDSIns[1];
	double trainingDSOuts[1];
	double previousError = 99e99;
	int i;
	int epochs = 80;
	int filterEpochs = 5;
	double maxGeom = epochs - 5 - filterEpochs;
	double final = 500.0; //TUNING VARIABLE
	double geometricModifier = pow(final, 1.0 / maxGeom);


	for (i = 0; i < filterEpochs; i++)
	{
		cout << "filter Epoch " << i << endl;
		double paramChangeTol=1e-2,  stateChangeTol=1e2,  predicitonErrorTol=1e0,  consistancyErrorTol=1e0,  measurementErrorTol=1e0;
		setTrainingWeights(seaNet, trainer,  paramChangeTol,  stateChangeTol,  predicitonErrorTol,  consistancyErrorTol,  measurementErrorTol);
		cout << trainer.viewMeasurementWeight()<<endl;
		cout << trainer.viewUpdateWeight()<<endl;
		cout << trainer.viewParameterDamping()<<endl;
		cout << trainer.viewStateDamping()<<endl;
		trainer.trainOneEpoch();

		errorRecord(trainer.getEpochError(), trainingDSIns, trainingDSOuts, i, fileBase, number, trainingErrorDataSet,
				previousError);
		trainer.log1Epoch(fileBase, number, i);
	}

	for (i = filterEpochs; i < epochs; i++)
	{
		cout << "standard epoch " << i << endl;
		double paramChangeTol=1e2,  stateChangeTol=1e-2,  predicitonErrorTol=1e0,  consistancyErrorTol=1e0,  measurementErrorTol=pow(geometricModifier, i - filterEpochs);
		if (i > maxGeom)
			measurementErrorTol=final;
		setTrainingWeights(seaNet,  trainer,  paramChangeTol,  stateChangeTol,  predicitonErrorTol,  consistancyErrorTol,  measurementErrorTol);

		trainer.trainOneEpoch();

		errorRecord(trainer.getEpochError(), trainingDSIns, trainingDSOuts, i, fileBase, number, trainingErrorDataSet,
				previousError);
		trainer.log1Epoch(fileBase, number, i);

		cout << "-------final weights with error --" << trainer.getElitePoint().error << endl;
		for (int i = 0; i < seaNet->viewParameterNames().size(); i++)
			cout << seaNet->viewParameterNames()[i] << "= " << seaNet->getWeight(i) << endl;
	}
	cout << "trained for " << i << " epochs" << endl;
	trainingErrorDataSet.pushToFile(trainingDataFileName);
	trainer.log(fileBase, number);

	vector<double> bestWeights(trainer.getElitePoint().weights);
	seaNet->setWeights(bestWeights);
	cout << "-------trainV3Network done  --" << endl;

}
}
}
}
using namespace uta::demos::appIntProj;
int main(int argc, const char* argv[])
{
	DataSet exp3Short("exp3Short.iodat");
	trainV3Net(exp3Short, "exp3PosDef", 2, "exp3PosDef2.trn");//was at Tr20
	// exp3PosDef1 has damping
	// LP1 is the first with the alternate state space representation.
	cout << "Hello AppInt Proj" << endl;
}

