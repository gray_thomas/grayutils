import scipy.integrate as integrate
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from networkUtils.pendulumDemo import *
from networkUtils.dyNetUtils.trainingFileManager import * 
from networkUtils.dyNetUtils.convolutionFilter import *
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import matplotlib.pyplot as plt
import math
from networkUtils.dyNetUtils.dyNetYamlReader import *
from humeID import GRAY_UTILS
from humeID import MEKA
from mpl_toolkits.mplot3d import Axes3D
from yaml import load, dump
import io
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

manager = TrainingFileManager("C:/Users/Gray/wk/grayutils/Demos/AppIntProj");
rawSweep3 = "sweepTest3.txt"
rawSweep4 = "sweepTest4.txt"
rawSweep5 = "sweepTest5.txt"
rawKneeBreak = "the_first_time_I_broke_the_knee.txt"


class HumeDataFolder(object):
    def __init__(self, folderLocation):
        self.folderLocation = folderLocation + "/";
    def loadRawMatrix(self, fileName):
        data = np.loadtxt(self.folderLocation + fileName)
        return data
    def getTime(self):
        return self.loadRawMatrix("time.txt")
    def getTorqueInput(self):
        return self.loadRawMatrix("torque_input.txt")
    def getPhase(self):
        return self.loadRawMatrix("phase.txt")
    def getMeasuredTorque(self):
        return self.loadRawMatrix("actual_torque.txt")
    def getMeasuredVelocity(self):
        return self.loadRawMatrix("full_state_vel.txt")
    def getMeasuredJoints(self):
        return self.loadRawMatrix("full_state_pos.txt")
    def getFootDesired(self):
        return self.loadRawMatrix("foot_des.txt")
    def getFootPosition(self):
        return self.loadRawMatrix("foot_pos.txt")
    def getFootVelocity(self):
        return self.loadRawMatrix("foot_vel.txt")
    def getCOMPosition(self):
        return self.loadRawMatrix("com_pos.txt")
    def getCOMVelocity(self):
        return self.loadRawMatrix("com_vel.txt")
    def getCOMDesired(self):
        return self.loadRawMatrix("com_des.txt")
    
def standardFileNamesDict(numbersList):
    dict = {}
    for i in numbersList:
        dict[i] = "save_file_" + str(i)
    return dict

class HumeDataFetcher(object):

    def __init__(self):
#         self.baseDir = "c:/Users/Gray/wk/grayutils/NetworkUtils/src/main/data/"  # Windows
        self.baseDir = "/home/hcrl/wk/grayutils/NetworkUtils/src/main/data/"  # Linux
        self.experiments = {1:("20140115_one_step_IROS/", standardFileNamesDict([2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13])),
                          2:("20140117_RSS_onestep_regression/", standardFileNamesDict([1, 2, 3, 4, 5, 6, 7]))}
#         self.baseFile="/home/hcrl/wk/whole_body_controller_hume/data_analysis/20140115_one_step_IROS/"
        self.folders = {2:"save_file_2", 3:"save_file_3", 4:"save_file_4", 5:"save_file_5", 6:"save_file_6",
                      7:"save_file_7", 8:"save_file_8", 9:"save_file_9", 10:"save_file_10", 11:"save_file_11",
                      12:"save_file_12", 13:"save_file_13"}
        self.dataFiles = {}
        
    def getDataSet(self, number):
        return self.getDataSetExp(1, number)
    def getDataSetExp(self, experiment, number):
        (main, subs) = self.experiments[experiment]
        return HumeDataFolder(self.baseDir + main + subs[number])
    



def plotConvergenceTest():
    dat = DyNetPlotterYamlData(GRAY_UTILS + "/DyNetUtils/src/test/data/" + 
                         "testConvergence_Noise_30_A_0.85_B_0.25_C_-2_D_-0.1_DataSize_120_seed_1342.yaml")
    plt.figure()
    plt.hold(True)
    plt.plot(dat.epochData[5].measurements_predicted, '#360022', linewidth=2)
    plt.plot(dat.epochData[4].measurements_predicted, '#451d00', linewidth=2)
    plt.plot(dat.epochData[3].measurements_predicted, '#430005', linewidth=2)
    plt.plot(dat.epochData[2].measurements_predicted, '#b40071', linewidth=2)
    plt.plot(dat.epochData[1].measurements_predicted, '#e76200', linewidth=2)
    plt.plot(dat.epochData[0].measurements_predicted, '#e10010', linewidth=2)
    plt.plot(dat.measurements, 'k.', linewidth=2)
    
def fuzzyDelay(vector, t0, tf, delay):
    low = math.floor(delay)
    high = math.ceil(delay)
    if (low == high):
        return vector[t0 + low:tf + low]
    lowScaling = abs(delay - high)
    highScaling = abs(delay - low)
    lowVector = vector[t0 + low:tf + low]
    highVector = vector[t0 + high:tf + high]
    result = highScaling * highVector + lowScaling * lowVector
    return result
    
def forceVertXPredictor(data, t0, tf, force0, d1, d2, s1, s2, bl, z1, z2):
    delayQEI = fuzzyDelay(data[:, 1] - z1, t0, tf, d1) * s1
    delayJ = fuzzyDelay(data[:, 2] - z2, t0, tf, d2) * s2
    blVec = [(bl if (v > 0) else -bl) for v in data[t0:tf, 0]]
    return delayJ - delayQEI + force0 + blVec

def saveTrainingSet(fileName, npArrayInputs, npArrayOutputs, folder=GRAY_UTILS + "/Demos/data/",):
    numInputs = npArrayInputs.shape[1]
    numOutputs = npArrayOutputs.shape[1]
    T = np.concatenate((npArrayInputs, npArrayOutputs), axis=1)
    np.savetxt(folder + fileName, T, header="%d %d" % (numInputs, numOutputs), comments="")
    
def setupConvolutionFilters(data, axs, to, tf):
    nConv = 100
    tmax = nConv * 1.0
    sigma = 0.002 * 2000  # seconds to tics
    kosigma = 0.016 * 2000
    ts = np.linspace(-tmax, tmax, nConv * 2 + 1)
    convolutionKernel = np.array([math.exp(-(t / sigma) * (t / sigma)) for t in ts])
    convolutionKernel = convolutionKernel * (1.0 / (np.sum(convolutionKernel)))
    diffKernel = np.array([-t * math.exp(-(t / sigma) * (t / sigma)) for t in ts])
    diffKernel = -diffKernel * (1.0 / (np.dot(diffKernel, ts)))
    ddifKernal = np.array([(2 * t * t / sigma / sigma - 1) * math.exp(-(t / sigma) * (t / sigma)) for t in ts])
    ddifKernal = ddifKernal * (1.0 / np.dot(ddifKernal, np.array([0.5 * t * t for t in ts])))
    
    knockoutGaussian = np.array([math.exp(-(t / kosigma) * (t / kosigma)) for t in ts])
    knockoutGaussian = knockoutGaussian * (1.0 / (np.sum(knockoutGaussian)))
    knockoutGaussian = 1 - convolutionKernel / convolutionKernel[nConv]
    
    # weighting to account for ommitted data
    countingErrors = np.array([(1 + data[to + i - 1, 4] - tS) % 100 for (i, tS) in enumerate(data[to:tf, 4]) ])
    wrappedCountingErrors = np.array([d - 100 if d > 50 else d for d in countingErrors])
    synchWeighting = np.array([0 if dC != 0 else 1 for dC in wrappedCountingErrors]).reshape((-1, 1))
    paddedWeighting = np.concatenate((np.ones((nConv,)), synchWeighting.reshape((-1,)), np.ones((nConv,))), axis=0)
    indexes = []
    start = np.ones(paddedWeighting.shape)
    for i in range(0, len(paddedWeighting)):
        if paddedWeighting[i] == 0:
            indexes = [indexes, i]
            for i, j in enumerate(range(i - nConv, i + nConv + 1)):
                start[j] = knockoutGaussian[i] * start[j]
    print synchWeighting.shape
    print convolutionKernel.shape
    convolvedWeighting = start[nConv:-nConv]
#     weightedAccel = np.multiply(convolvedMotorAcceleration, convolvedWeighting)
#     simpleVelocity = (data[to + 1:tf + 1, 1] - data[to - 1:tf - 1, 1]) * 0.5
#     plt.setp(axs[4, 0].plot(simpleVelocity, "b-"), linewidth=2.0)
    
#     simpleAccel = (data[to + 10:tf + 10, 1] - 2 * data[to:tf, 1] + data[to - 10:tf - 10, 1]) / (10.0 * 10.0)
#     plt.setp(axs[5, 0].plot(simpleAccel, "b-"), linewidth=2.0)
#     axs[5,0].properties()['yaxis'].properties()['label'].set_rotation("horizontal")
#     axs[4, 1].plot(np.multiply(np.convolve(simpleVelocity - convolvedMotorVelocity, convolutionKernel, mode='same'), convolvedWeighting), "b-")
    
#     axs[5, 0].plot(weightedAccel, "g")
#     axs[5, 1].plot(np.multiply((simpleAccel) - convolvedMotorAcceleration, convolvedWeighting), "b-")
#     axs[5, 1].plot(np.multiply((simpleAccel) - weightedAccel, convolvedWeighting), "g-")
    
    axs[0, 1].plot(wrappedCountingErrors)
    axs[0, 1].plot(convolvedWeighting, 'm')
    axs[0, 1].set_ylabel("Synch Errors", rotation="horizontal")
    return nConv, convolutionKernel, convolvedWeighting, diffKernel, ddifKernal


def estimateSensors(data, axs, to, tf, nConv, convolutionKernel, convolvedWeighting): 
    cv = lambda(x):(np.convolve(x, convolutionKernel, mode='same')[nConv:-nConv]).reshape((-1, 1))   

    R = np.concatenate((np.ones((tf - to, 1)),
                    cv(data[(-nConv + to - 1):(tf - 1 + nConv), 1]),
                    cv(data[(to - nConv):(tf + nConv), 1]),
                    cv(data[(to - nConv):(tf + nConv), 2]),
                    cv(np.multiply(data[(to - nConv):(tf + nConv), 2], data[(to - nConv):(tf + nConv), 1])),
                    cv(np.multiply(data[(to - nConv):(tf + nConv), 2], data[(to - nConv):(tf + nConv), 2]))
                      ), axis=1)
    # two models, one for joint angles less than 5000, and the other for angles > 5000
    # Note that 5000 is the bottom end of the range.
    Rnormal = np.array(R)
    Rextreme = np.zeros(R.shape)
    for (j, i) in enumerate(range(to, tf)):
        if (data[i, 2] < 5000):
            zeros = Rextreme[j, :]
            Rextreme[j, :] = Rnormal[j, :]
            Rnormal[j, :] = zeros
    R = np.concatenate((Rnormal, Rextreme), axis=1)
        
    
#     convolvedWeighting=np.multiply(convolvedWeighting,extremeJointAngles.reshape((-1,)))# don't do this.
    koConvR = np.array(R)
    for col in range(0, R.shape[1]):
        koConvR[:, col] = np.multiply(R[:, col], convolvedWeighting)
    weightedData = np.multiply(data[to:tf, [3]], convolvedWeighting.reshape((-1, 1)))
    weightedData = np.convolve(weightedData.reshape(-1,), convolutionKernel, mode='same').reshape(-1, 1)
    sol = np.linalg.solve(R.T.dot(koConvR), R.T.dot(weightedData))
    expectation = R.dot(sol)
    
    
    maxSpringEncoder = max(data[to:tf, 3]) * 1.0
    minSpringEncoder = min(data[to:tf, 3]) * 1.0

    
    convolvedData3 = np.convolve(data[(to - nConv):(tf + nConv), 3], convolutionKernel, mode='same')[nConv:-nConv]
    plt.setp(axs[3, 0].plot(convolvedData3, "k-"), linewidth=1.0)
    plt.setp(axs[3, 0].plot(R.dot(sol), "m"), linewidth=1.0)
    plt.setp(axs[3, 0].plot(R.dot(sol), "c"), linewidth=1.0)
    plt.setp(axs[3, 0].plot(data[to:tf, 3], "b-"), linewidth=2.0)
    axs[3, 0].set_ylabel("Spring\nEncoder\nInteger\nValue", rotation="horizontal")
    rawError = (R.dot(sol) - data[to:tf, [3]])
    smoothError = R.dot(sol) - convolvedData3.reshape((-1, 1))
    rawError = np.multiply(rawError, convolvedWeighting.reshape((-1, 1)))
    smoothError = np.multiply(smoothError, convolvedWeighting.reshape((-1, 1)))
    errorVariance = rawError.T.dot(rawError) * 1.0 / (rawError.shape[0] - 1.0)
    
    print "spring normal: const: %f, m z^-1: %f, m: %f+%f*j, j: %f+%f*j" % (
                sol[0], sol[1], sol[2], sol[4], sol[3], sol[5])
    print "spring extreme: const: %f, m z^-1: %f, m: %f+%f*j, j: %f+%f*j" % (
                sol[6], sol[7], sol[8], sol[9], sol[10], sol[11])
    print errorVariance
    plt.setp(axs[3, 1].plot(rawError * 100 / (maxSpringEncoder - minSpringEncoder), "b"), linewidth=2.0)
    plt.setp(axs[3, 1].plot(smoothError * 100 / (maxSpringEncoder - minSpringEncoder), "r"), linewidth=2.0)
    axs[3, 1].yaxis.set_major_formatter(plt.FormatStrFormatter('%.1f%%'))
    return sol

def estimateSensorsSimple(data, axs, to, tf, nConv, convolutionKernel, convolvedWeighting):    


#     angulize=lambda(x):2*math.pi*((x%4092)/4092.0)
    clampLin = lambda(x, low, hi):0.0 if x < low else 0.0 if x > hi else (x - low * 1.0) / (1.0 * hi - low) * 2 if x - low < hi - x else (hi * 1.0 - x) / (hi * 1.0 - low) * 2 
#     x=1
#     low=0
#     hi=2
#     assert clampLin((1,0,2))==1
#     assert clampLin((0,0,2))==0
#     assert clampLin((2,0,2))==0
#     assert clampLin((1.5,0,2))==0.5
#     assert clampLin((0.5,0,2))==0.5
#     assert clampLin((0.75,0,2))==0.75
#     assert clampLin((1.75,0,2))==0.25
#     assert clampLin((1.25,0,2))==0.75
    nLin = 5
    vals = np.linspace(4250, 8000, nLin + 2)
    
    Rlin = np.zeros((tf - to, nLin))
    for i in range(0, nLin):
        Rlin[:, i] = np.array([clampLin((dat, vals[i], vals[i + 2]))for dat in data[to:tf, 2]])
#     plt.show()
    R = np.concatenate((np.ones((tf - to, 1)),
                    data[to:tf, [1]],
                    data[to:tf, [2]],
                     
                    Rlin
                      ), axis=1)

#     convolvedWeighting=np.multiply(convolvedWeighting,extremeJointAngles.reshape((-1,)))# don't do this.
    koConvR = np.array(R)
    for col in range(0, R.shape[1]):
        koConvR[:, col] = np.multiply(R[:, col], convolvedWeighting)
    weightedData = np.multiply(data[to:tf, [3]], convolvedWeighting.reshape((-1, 1)))
    weightedData = np.convolve(weightedData.reshape(-1,), convolutionKernel, mode='same').reshape(-1, 1)
    sol = np.linalg.solve(R.T.dot(koConvR), R.T.dot(weightedData))
    vals = [0, 1, 2]
    solDumb = np.linalg.solve(R[:, vals].T.dot(koConvR[:, vals]), R[:, vals].T.dot(weightedData))
    
    
    maxSpringEncoder = max(data[to:tf, 3]) * 1.0
    minSpringEncoder = min(data[to:tf, 3]) * 1.0

    
    convolvedData3 = np.convolve(data[(to - nConv):(tf + nConv), 3], convolutionKernel, mode='same')[nConv:-nConv]
    plt.setp(axs[3, 0].plot(convolvedData3, "k-"), linewidth=1.0)
    plt.setp(axs[3, 0].plot(R.dot(sol), "m"), linewidth=1.0)
    plt.setp(axs[3, 0].plot(R.dot(sol), "c"), linewidth=1.0)
    plt.setp(axs[3, 0].plot(data[to:tf, 3], "b-"), linewidth=2.0)
    axs[3, 0].set_ylabel("Spring\nEncoder\nInteger\nValue", rotation="horizontal")
    rawError = (R.dot(sol) - data[to:tf, [3]])
    smoothError = R.dot(sol) - convolvedData3.reshape((-1, 1))
    rawError = np.multiply(rawError, convolvedWeighting.reshape((-1, 1)))
    smoothError = np.multiply(smoothError, convolvedWeighting.reshape((-1, 1)))
    errorVariance = rawError.T.dot(rawError) * 1.0 / (rawError.shape[0] - 1.0)
    print "spring silly alphas: [%f, %f, %f]" % (
                solDumb[0], solDumb[1], solDumb[2])
    print "spring simple alphas: [%f, %f, %f, %f, %f, %f]" % (
                sol[0], sol[1], sol[2], sol[3], sol[4], sol[5])
    print errorVariance
    plt.setp(axs[3, 1].plot(rawError * 100 / (maxSpringEncoder - minSpringEncoder), "b"), linewidth=2.0)
    plt.setp(axs[3, 1].plot(smoothError * 100 / (maxSpringEncoder - minSpringEncoder), "r"), linewidth=2.0)
    axs[3, 1].yaxis.set_major_formatter(plt.FormatStrFormatter('%.1f%%'))
    fig2 = plt.figure(3, figsize=(6.0, 3), facecolor='w')
    plt.plot(data[to:tf, 2], np.multiply(convolvedWeighting.reshape((-1, 1)), R[:, vals].dot(solDumb) - data[to:tf, [3]]), 'g.')
#     plt.plot(data[to:tf,2],np.multiply(convolvedWeighting.reshape((-1, 1)),(R[:,0:6].dot(sol[0:6]) - data[to:tf, [3]])),'m.')
    plt.plot(data[to:tf, 2], np.multiply(convolvedWeighting.reshape((-1, 1)), (R.dot(sol) - data[to:tf, [3]])), 'r.')
#     plt.plot(data[to:tf:100, 2], 10.0 * Rlin[::100, :], '.')
    plt.title("Nonlinearity in the encoders")
    plt.xlabel("Joint encoder measurement")
    plt.ylabel("Signed spring encoder prediction error.")
    plt.legend(["simple, linear model", "model with delay term and basis function non-linearity compensation"])
    
    return sol, fig2
    
def estimateInternalModel(data, axs, to, tf, nConv, convolutionKernel, convolvedWeighting, convolvedMotorAccel, convolvedVelocity):
    cv = lambda(x):np.convolve(x, convolutionKernel, mode='same')[nConv:-nConv]
    signum = lambda(xs):np.array([1.0 if x > 0 else -1.0 for x in xs]).reshape((-1,))
    convolvedR = np.concatenate((np.ones((tf - to, 1)),
                      cv(data[(to - nConv):(tf + nConv), 0]).reshape((-1, 1)),
                      cv(data[(to - nConv):(tf + nConv), 3]).reshape((-1, 1)),
#                       signum(convolvedVelocity).reshape(-1,1),
                      cv(signum(data[(to - nConv):(tf + nConv), 1] - data[(to - nConv - 1):(tf + nConv - 1), 1])).reshape((-1, 1)),
                      convolvedVelocity.reshape(-1, 1)
                      ), axis=1)
    vThresh = 60.0
    velocityThresholdWeighting = np.array([1.0 if abs(v) > vThresh else 0.0 for v in convolvedVelocity]).reshape((-1, 1))
    weighting = np.multiply(velocityThresholdWeighting, convolvedWeighting.reshape((-1, 1)))
    koConvR = np.array(convolvedR)
    for col in range(0, koConvR.shape[1]):
        koConvR[:, [col]] = np.multiply(convolvedR[:, [col]], weighting)
    weightedData = convolvedMotorAccel.reshape((-1, 1))
    weightedData = np.multiply(weightedData, weighting).reshape(-1, 1)
    sol1 = np.linalg.solve(convolvedR.T.dot(koConvR), convolvedR.T.dot(weightedData))
    print "model estimate: constantAccel=%f, currentConstant=%f, springConstant=%f, coulombFriction=%f, damping=%f" % (
                                            sol1[0], sol1[1], sol1[2], sol1[3], sol1[4])
    expectedAccel1 = convolvedR.dot(sol1)
    axs[5, 0].plot(expectedAccel1, 'c')
    error = expectedAccel1.reshape((-1,)) - convolvedMotorAccel.reshape((-1,))
    scaledError = error * 100.0 / (max(convolvedMotorAccel) - min(convolvedMotorAccel))
   
    axs[5, 1].plot(scaledError, 'b')
    axs[5, 1].plot(np.multiply(scaledError.reshape((-1, 1)), weighting), 'r')
    axs[5, 1].yaxis.set_major_formatter(plt.FormatStrFormatter('%.1f%%'))
    fig3 = plt.figure(2, figsize=(4.0, 6), facecolor='w')
    plt.hold(True)
    error = np.multiply(error, convolvedWeighting)
    dt = 0.0005  # seconds
    Nsamps = error.shape[0]
    freq = np.linspace(0, 1.0 / dt, Nsamps)
    plt.loglog(freq, [math.sqrt(z.conjugate() * z) * (2.0 / Nsamps) for z in np.fft.fft(error)], 'b.')
    plt.loglog([freq[Nsamps / 2], freq[Nsamps / 2]], [0.0001, 1], 'k')
    plt.gca().set_xlabel("Frequency (Cps)");
    plt.gca().set_ylabel(r"Model prediction error (\rm{tic}^2)");
    
    return sol1, fig3

def estimateExternalModel(data, axs, to, tf, nConv, convolutionKernel, convolvedWeighting, convolvedJointAccel, convolvedJointVelocity):
    cv = lambda(x):np.convolve(x, convolutionKernel, mode='same')[nConv:-nConv]
    signum = lambda(xs):np.array([1.0 if x > 0 else -1.0 for x in xs]).reshape((-1,))
    convolvedR = np.concatenate((np.ones((tf - to, 1)),  # dc offset
                      cv(data[(to - nConv):(tf + nConv), 3]).reshape((-1, 1)),  # spring torque
                      cv(data[(to - nConv):(tf + nConv), 2]).reshape((-1, 1)),  # gravity
                      convolvedJointVelocity.reshape((-1, 1)),  # damping
                      signum(convolvedJointVelocity).reshape((-1, 1)),  # friction
                      ), axis=1)
#     vThresh = 60.0
#     velocityThresholdWeighting = np.array([1.0 if abs(v) > vThresh else 0.0 for v in convolvedVelocity]).reshape((-1, 1))
#     weighting = np.multiply(velocityThresholdWeighting, convolvedWeighting.reshape((-1,1)))
    koConvR = np.array(convolvedR)
    for col in range(0, koConvR.shape[1]):
        koConvR[:, col] = np.multiply(convolvedR[:, col], convolvedWeighting)
#     weightedData = convolvedMotorAccel.reshape((-1, 1))
    weightedData = np.multiply(convolvedJointAccel, convolvedWeighting).reshape(-1, 1)
    sol1 = np.linalg.solve(convolvedR.T.dot(koConvR), convolvedR.T.dot(weightedData))
    print "exterior model estimate: constantAccel=%f, springConstant=%f, linearGravity=%f, damping=%f, friction=%f" % (
        sol1[0], sol1[1], sol1[2], sol1[3], sol1[4])
    expectedAccel1 = convolvedR.dot(sol1)
    axs[7, 0].plot(expectedAccel1, 'c')
    error = (expectedAccel1.reshape((-1,)) - convolvedJointAccel.reshape((-1,)))
    scaledError = error * 100.0 / (max(convolvedJointAccel) - min(convolvedJointAccel))
    axs[7, 1].plot(scaledError, 'b')
    axs[7, 1].plot(np.multiply(scaledError.reshape((-1,)), convolvedWeighting), 'r')
    axs[7, 1].yaxis.set_major_formatter(plt.FormatStrFormatter('%.1f%%'))
    plt.figure(2)
    error = np.multiply(error, convolvedWeighting)
    dt = 0.0005  # seconds
    Nsamps = error.shape[0]
    freq = np.linspace(0, 1.0 / dt, Nsamps)
    plt.loglog(freq, [math.sqrt(z.conjugate() * z) * (2.0 / Nsamps) for z in np.fft.fft(error)], 'g.')
    
    return sol1

def identifyEquilibriumEncoderValues(alphas, betas, gammas):
    z3 = -betas[0] / betas[2]
    z2 = (-gammas[0] - gammas[1] * z3) / gammas[2]
    z1 = (alphas[0] - z3 + alphas[3] * z2 + alphas[5] * z2 * z2) / (-alphas[1] - alphas[2] - alphas[4] * z2)
    return [z1, z2, z3]

def generateLinearPlantModel(alphas, betas, gammas, zeros):
    A = np.zeros((4, 4))
    B = np.zeros((4, 1))
    C = np.zeros((3, 4))
    D = np.zeros((3, 1))
    dt = 1  # due to choice of units
    A[0, 1] = 1.0;  # motor position integration
    A[2, 3] = 1.0;  # joint position integration
    temp1 = alphas[1] + alphas[4] * zeros[1] + alphas[2]
    temp2 = alphas[3] + 2 * alphas[4] * zeros[0] + 2 * alphas[5] * zeros[1]
    A[1, 0] = betas[2] * (temp1)
    A[1, 1] = betas[4] - betas[2] * alphas[1] * dt
    A[1, 2] = betas[2] * (temp2)
    A[1, 3] = 0
    B[1, 0] = betas[1]
    A[3, 0] = gammas[1] * (temp1)
    A[3, 1] = gammas[1] * (-dt * alphas[1])
    A[3, 2] = gammas[2] + gammas[1] * (temp2)
    A[3, 3] = gammas[3]
    C[0, 0] = 1.0
    C[1, 2] = 1.0
    C[2, 0] = temp1
    C[2, 1] = -dt * alphas[1]
    C[2, 2] = temp2
    return A, B, C, D
    
def plotSimpleData(data, to, tf, figBaseName):
    fig2 = plt.figure(figsize=(20, 5), facecolor='w')
    ax1 = plt.subplot(2, 2, 1)
    ts = np.array(range(to, tf)) * 0.0005
    plt.plot(ts, data[to:tf, 0], linewidth=2.0)
    plt.ylabel(r"Current")
    plt.subplot(2, 2, 2, sharex=ax1)
    plt.plot(ts, data[to:tf, 1], linewidth=2.0)
    plt.ylabel(r"Motor enc.")
    plt.subplot(2, 2, 3, sharex=ax1)
    plt.plot(ts, data[to:tf, 2], linewidth=2.0)
    plt.ylabel(r"Joint enc.")
    plt.subplot(2, 2, 4, sharex=ax1)
    plt.plot(ts, data[to:tf, 3], linewidth=2.0)
    plt.ylabel(r"Spring enc.")
    plt.xlabel(r"Time (s)")
    manager.saveFig(fig2, figBaseName + "Raw.pdf")
def buildSimpleDataSet(data, to, tf, fileName):
    saveData = np.concatenate((data[to:tf, [0]],
                             data[to:tf, [1]],
                             data[to:tf, [3]],
                             data[to:tf, [2]]), axis=1)
    print saveData
    manager.saveIODat(fileName, saveData, 1, 3)
    
def plotFancySharedX(data, to, tf, figBaseName):
    fig, axs = plt.subplots(8, 2, figsize=(12.0, 16), sharex=True, sharey=False, facecolor='w')
    axs[0, 0].set_title(r"HumeData, Right Knee Joint")
    axs[0, 1].set_title(r"Estimator Errors as Percentage of Variance")
    
    plt.setp(axs[0, 0].plot(data[to:tf, 0], "b-"), linewidth=2.0)
    axs[0, 0].set_ylabel("Input\nCurrent\nDAC\nValue", rotation="horizontal")
    plt.setp(axs[1, 0].plot(data[to:tf, 1], "b-"), linewidth=2.0)
    axs[1, 0].set_ylabel("Motor\nEncoder\nInteger\nValue", rotation="horizontal")
    plt.setp(axs[2, 0].plot(data[to:tf, 2], "b-"), linewidth=2.0)
    axs[2, 0].set_ylabel("Joint\nEncoder\nInteger\nValue", rotation="horizontal")
 
    nConv, convolutionKernel, convolvedWeighting, diffKernel, ddifKernal = setupConvolutionFilters(data, axs, to, tf)
     
    convolvedMotorVelocity = np.convolve(data[to - nConv:tf + nConv, 1], diffKernel, mode='same')[nConv:-nConv]
    convolvedMotorAcceleration = np.convolve(data[to - nConv:tf + nConv, 1], ddifKernal, mode='same')[nConv:-nConv]
    convolvedJointVelocity = np.convolve(data[to - nConv:tf + nConv, 2], diffKernel, mode='same')[nConv:-nConv]
    convolvedJointAcceleration = np.convolve(data[to - nConv:tf + nConv, 2], ddifKernal, mode='same')[nConv:-nConv]
    axs[4, 0].set_ylabel("Motor\nEncoder\nVelocity", rotation="horizontal")
    axs[5, 0].set_ylabel("Motor\nEncoder\nAcceleration", rotation="horizontal")
    axs[6, 0].set_ylabel("Joint\nEncoder\nVelocity", rotation="horizontal")
    axs[7, 0].set_ylabel("Joint\nEncoder\nAcceleration", rotation="horizontal")
    axs[4, 0].plot(convolvedMotorVelocity, "m")
    axs[5, 0].plot(convolvedMotorAcceleration, "m")
    axs[6, 0].plot(convolvedJointVelocity, 'm')
    axs[7, 0].plot(convolvedJointAcceleration, 'm')
     
     
    alphas, fig2 = estimateSensorsSimple(data, axs, to, tf, nConv, convolutionKernel, convolvedWeighting)
#     manager.saveFig(fig2, figBaseName + "Sensors.pdf")
    betas, fig3 = estimateInternalModel(data, axs, to, tf, nConv, convolutionKernel,
                                          convolvedWeighting, convolvedMotorAcceleration, convolvedMotorVelocity)
#     manager.saveFig(fig3, figBaseName + "ErrorFrequencyDomain.pdf")
    gammas = estimateExternalModel(data, axs, to, tf, nConv, convolutionKernel, convolvedWeighting, convolvedJointAcceleration,
                          convolvedJointVelocity)
    zeros = identifyEquilibriumEncoderValues(alphas, betas, gammas)
#     A,B,C,D = generateLinearPlantModel(alphas, betas, gammas, zeros)
#     print A,B,C,D
#     axs[1,0].plot([to,tf],np.array([1,1])*zeros[0])
#     axs[2,0].plot([to,tf],np.array([1,1])*zeros[1])
#     axs[3,0].plot([to,tf],np.array([1,1])*zeros[2])
    for i in range(0, 8):
        axs[i, 0].yaxis.set_label_coords(-0.15, 0.5)
        axs[i, 1].yaxis.set_label_coords(1.15, 0.5)
    axs[5, 0].set_xbound(0, tf - to)
      
    axs[5, 0].xaxis.set_major_formatter(plt.FormatStrFormatter('%d'))
      
    axs[5, 0].set_xlabel(r"Raw Controller Time Steps")
    manager.saveFig(fig, figBaseName + ".pdf")
    return data
# data=np.loadtxt(MEKA+"/../humeData/"+"sweepTest5.txt")


def buildBreakDataSet():
    breakData = manager.loadRawMatrix(rawKneeBreak)
    # 0 0 1443 23425 -30171 2274 -2048  -32768  0 1 0  0  60
    # 0 0 1443 23425 -30171 1263 0      -32768  0 0 0  0  55
    # 0 1 2    3     4      5    6       7      8 9 10 11 12
    #     mot  joint spring crap input    
    
    start = 26000
    impulseStart = 26825   
    impulseEnd = 27280  
    end = 28000                     
    breakData
    springs = np.array([(2 ** 16 + unshiftedSpring) * 0.25 for unshiftedSpring in breakData[start:end, 4]])
    springs = springs.reshape((-1, 1))
    goodData = np.concatenate((breakData[start:end, [6]], breakData[start:end, [2]], springs, breakData[start:end, [3]]), axis=1)
    print springs
    print breakData[:, [2]]
    print springs.shape
    print goodData
    manager.saveIODat("break.iodat", goodData, 1, 3)
    
def subspacePlot():
    data = manager.loadRawMatrix(rawSweep5)
    to = 23660  # sweepTest5
    tf = 530000  # sweepTest5
    fig = plt.figure(3)
    ax = fig.add_subplot(111, projection='3d')
    dec = 200
    mavg = np.average(data[to:tf:dec, 1])
    javg = np.average(data[to:tf:dec, 2])
#     spring normal: const: 68161.326650, m z^-1: -0.164309, m: 0.382991+-0.000068*j, j: -24.141210+0.001791*j
#     spring extreme: const: -139491.629817, m z^-1: 0.039308, m: -0.562092+41.133638*j, j: 0.000103+-0.002688*j
    ax.scatter(data[to:tf:dec, 2],
                    (68161.326650 + 
                    - 24.141210 * data[to:tf:dec, 2] + 
                    0.001791 * np.multiply(data[to:tf:dec, 2], data[to:tf:dec, 2]) + 
                    0.382991 * data[to:tf:dec, 1] + 
                    - 0.000068 * np.multiply(data[to:tf:dec, 1], data[to:tf:dec, 2]) + 
                    - 0.164309 * data[(to - 1):(tf - 1):dec, 1]),
                    data[to:tf:dec, 3], c='g')
     
    plt.figure(4)
    plt.plot(data[to:tf, 2], data[to:tf, 3] - (68161.326650 + 
                            - 24.141210 * data[to:tf, 2] + 
                    0.001791 * np.multiply(data[to:tf, 2], data[to:tf, 2]) + 
                    0.382991 * data[to:tf, 1] + 
                    - 0.000068 * np.multiply(data[to:tf, 1], data[to:tf, 2]) + 
                    - 0.164309 * data[(to - 1):(tf - 1), 1]))
    plt.plot(data[to:tf, 2], data[to:tf, 3] - (-139491.629817 + 
                            0.000103 * data[to:tf, 2] + 
                    - 0.002688 * np.multiply(data[to:tf, 2], data[to:tf, 2]) + 
                    - 0.562092 * data[to:tf, 1] + 
                    41.133638 * np.multiply(data[to:tf, 1], data[to:tf, 2]) + 
                    0.039308 * data[(to - 1):(tf - 1), 1]), 'g')
    ax.scatter(data[to:tf:100, 1], data[to:tf:100, 2], data[to:tf:100, 3])
     
    varianceTraining = data[0:to, 1:4]
    avg = np.reshape(np.average(varianceTraining, axis=0), (1, -1))
    var = 1.0 / (varianceTraining.shape[0] - 1.0) * (varianceTraining - avg).T.dot(varianceTraining - avg)
     
    print var
def plotDyNetRCTrainer(name, thetaLabel):
    dyNetFF1 = manager.loadDyNetYaml(name)
    ymlFig = plt.figure(figsize=(8.0, 3), facecolor='w')
    times = np.array(range(0, len(dyNetFF1.measurements))) * 0.01
    plt.plot(times, dyNetFF1.epochData[15].measurements_predicted, linewidth=2)
    plt.plot(times, dyNetFF1.epochData[7].measurements_predicted, linewidth=2)
    plt.plot(times, dyNetFF1.epochData[3].measurements_predicted, linewidth=2)
    plt.plot(times, dyNetFF1.epochData[1].measurements_predicted, linewidth=2)
    plt.plot(times, dyNetFF1.epochData[0].measurements_predicted, linewidth=2)
    lowy, highy = plt.gca().get_ybound()
    plt.plot(times, dyNetFF1.measurements, 'k', linewidth=1, alpha=0.7)
    plt.gca().set_ybound(lowy, highy)
    plt.xlabel(r"t\;(\rm{s})")
    plt.ylabel(r"\theta\;(\rm{rad})")
    leg = plt.legend([r"\theta_{\rm{epoch}_{15}}", r"\theta_{\rm{epoch}_7}",
                    r"\theta_{\rm{epoch}_3}", r"\theta_{\rm{epoch}_1}",
                    r"\theta_{\rm{epoch}_0}", thetaLabel],
                   loc='best', fancybox=True)
    low, high = plt.gca().get_xbound()
    plt.gca().set_xbound(low, high + (high - low) * 0.3)  # space for legend
    leg.get_frame().set_alpha(0.5)
    return ymlFig

#     xCoord, yCoord = 1,2
#     modLatexVar=r"\ddot{"+latexVar+r"}"
#     latexUnits=r"\frac{\rm{rad}}{\rm{s}^2}"
#     filtOp = [lambda(x): filt.ddif(x) for filt in filts]
#     epochDataIndex=2
def plotInvestigationSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar, latexUnits,
                            filtOps, epochs, filtersMS, epochDataIndex, ax1, rawData,
                            times, dat, epochStart):
    ax = plt.subplot(ySubs, xSubs, 1 + yCoord * xSubs + xCoord, sharex=ax1)
    for epoch in epochs:
        ax.plot(times[2:], dat.epochData[epoch].states_used[1:-1, epochDataIndex], linewidth=2)
    for epoch in epochs:
        ax.plot(times[2:], dat.epochData[epoch].states_predicted[:-2, epochDataIndex], linewidth=2)
    low, high = ax.get_ybound()
    for filtOp in filtOps:
        ax.plot(times[2:], filtOp(rawData[2:]), linewidth=2)
    ax.set_ybound(low, high)
    ax.set_xlabel(r"$t\;(\rm{s})$")
    ax.set_ylabel(r"$" + modLatexVar + r"\;(" + latexUnits + r")$")
    legList = []
    for epoch in epochs:
        legList.append(r"$" + modLatexVar + r"_{\rm{epoch}\;{" + str(epoch + epochStart) + r"}}$")
    for epoch in epochs:
        legList.append(r"$" + modLatexVar + r"_{\rm{predicted,\;epoch}\;{" + str(epoch + epochStart) + r"}}$")
    for ms in filtersMS:
        legList.append(r"$" + modLatexVar + r"_{\rm{filt,\;\sigma=" + str(ms) + r"\;ms}}$")
    leg = ax.legend(legList, loc='right', fancybox=True)
    leg.get_frame().set_alpha(0.5)
    return ax
def plotApproximateStateComparisonSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar, latexUnits,
                            filtOps, epochs, filtersMS, epochDataIndex, ax1, rawData,
                            times, dat, epochStart):
    ax = plt.subplot(ySubs, xSubs, 1 + yCoord * xSubs + xCoord, sharex=ax1)
    for epoch in epochs:
        ax.plot(times[2:], dat.epochData[epoch].states_used[1:-1, epochDataIndex], linewidth=2)
    for epoch in epochs:
        ax.plot(times[2:], dat.epochData[epoch].states_predicted[:-2, epochDataIndex], linewidth=2)
    low, high = ax.get_ybound()
    for filtOp in filtOps:
        ax.plot(times[2:], filtOp(rawData[2:]), linewidth=2)
    ax.set_ybound(low, high)
    ax.set_xlabel(r"$t\;(\rm{s})$")
    ax.set_ylabel(r"$" + modLatexVar + r"\;(" + latexUnits + r")$")
    legList = []
    for epoch in epochs:
        legList.append(r"$" + modLatexVar + r"_{\rm{epoch}\;{" + str(epoch + epochStart) + r"}}$")
    for epoch in epochs:
        legList.append(r"$" + modLatexVar + r"_{\rm{predicted,\;epoch}\;{" + str(epoch + epochStart) + r"}}$")
    for ms in filtersMS:
        legList.append(r"$" + modLatexVar + r"_{\rm{filt,\;\sigma=" + str(ms) + r"\;ms}}$")
    leg = ax.legend(legList, loc='right', fancybox=True)
    leg.get_frame().set_alpha(0.5)
    return ax
def plotMeasurementComparisonSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar, latexUnits,
                             epochs, measurementDataIndex, ax1, rawData,
                            times, dat, epochStart):
    ax = plt.subplot(ySubs, xSubs, 1 + yCoord * xSubs + xCoord, sharex=ax1)
    for epoch in epochs:
        ax.plot(times[2:], dat.epochData[epoch].measurements_predicted[1:-1, measurementDataIndex], linewidth=2)
    low, high = ax.get_ybound()
    ax.plot(times, rawData, color='k', linewidth=1.5, alpha=0.7)
    ax.set_ybound(low, high)
    ax.set_xlabel(r"$t\;(\rm{s})$")
    ax.set_ylabel(r"$" + modLatexVar + r"\;(" + latexUnits + r")$")
    legList = []
    for epoch in epochs:
        legList.append(r"$" + modLatexVar + r"_{\rm{epoch}\;{" + str(epoch + epochStart) + r"}}$")
    legList.append(r"$" + modLatexVar + r"_{actual}}$")
    leg = ax.legend(legList, loc='right', fancybox=True)
    leg.get_frame().set_alpha(0.5)
    return ax
def plotInvestigationSubfigNoShareX(xCoord, yCoord, xSubs, ySubs, modLatexVar, latexUnits,
                            filtOps, epochs, filtersMS, epochDataIndex, rawData,
                            times, dat, epochStart):
    ax = plt.subplot(ySubs, xSubs, 1 + yCoord * xSubs + xCoord)
    for epoch in epochs:
        ax.plot(times[2:], dat.epochData[epoch].states_used[1:-1, epochDataIndex], linewidth=2)
    for epoch in epochs:
        ax.plot(times[2:], dat.epochData[epoch].states_predicted[:-2, epochDataIndex], linewidth=2)
    low, high = ax.get_ybound()
    for filtOp in filtOps:
        ax.plot(times[2:], filtOp(rawData[2:]), linewidth=2)
    ax.set_ybound(low, high)
    ax.set_xlabel(r"$t\;(\rm{s})$")
    ax.set_ylabel(r"$" + modLatexVar + r"\;(" + latexUnits + r")$")
    legList = []
    for epoch in epochs:
        legList.append(r"$" + modLatexVar + r"_{\rm{epoch}\;{" + str(epoch + epochStart) + r"}}$")
    for epoch in epochs:
        legList.append(r"$" + modLatexVar + r"_{\rm{predicted,\;epoch}\;{" + str(epoch + epochStart) + r"}}$")
    for ms in filtersMS:
        legList.append(r"$" + modLatexVar + r"_{\rm{filt,\;\sigma=" + str(ms) + r"\;ms}}$")
    leg = ax.legend(legList, loc='right', fancybox=True)
    leg.get_frame().set_alpha(0.5)
    return ax
def investigateDyNetTraining(yamlFileName, thetaLabel, filtersMS, epochs, epochStart):
    dat = manager.loadDyNetYaml(yamlFileName)
    ymlFig = plt.figure(figsize=(24.0, 4), facecolor='w')
    dt = 0.0005
    times = np.array(range(0, len(dat.measurements))) * dt
    filters = []
    for ms in filtersMS:
        filters.extend([ConvolutionFilter(ms * 1e-3, dt)])
    xSubs = 3
    ySubs = 3

    rawData = dat.measurements[:, 0]
    latexVar = r"\theta"
    xCoord = 0
    stateIndexes = [0, 1, 2]
    yCoord = 0
    ax1 = plt.subplot(ySubs, xSubs, 1 + yCoord * xSubs + xCoord)
    ax = ax1
    
    modLatexVar = r"{" + latexVar + r"}"
    latexUnits = r"\rm{tic}"
    epochDataIndex = stateIndexes[0]
    filtOps = [lambda(x): filt.filt(x) for filt in filters]
    ax = plotInvestigationSubfigNoShareX(xCoord, yCoord, xSubs, ySubs, modLatexVar,
                            latexUnits, filtOps, epochs, filtersMS, epochDataIndex,
                            rawData, times, dat, epochStart)
    low, high = ax.get_ybound()
    ax.plot(times, rawData, 'k', linewidth=1, alpha=0.7)
    ax.set_ybound(low, high)
    ax1 = ax
    
    # # theta Dot
    yCoord = 1
    modLatexVar = r"\dot{" + latexVar + r"}"
    latexUnits = r"\frac{\rm{tic}}{\rm{s}}"
    epochDataIndex = stateIndexes[1]
    filtOps = [lambda(x): filt.diff(x) for filt in filters]
    ax = plotInvestigationSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar,
                            latexUnits, filtOps, epochs, filtersMS, epochDataIndex,
                            ax1, rawData, times, dat, epochStart)
    
    # # theta Double Dot
    yCoord = 2
    modLatexVar = r"\ddot{" + latexVar + r"}"
    latexUnits = r"\frac{\rm{tic}}{\rm{s}^2}"
    epochDataIndex = stateIndexes[2]
    filtOps = [lambda(x): filt.ddif(x) for filt in filters]
    ax = plotInvestigationSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar,
                            latexUnits, filtOps, epochs, filtersMS, epochDataIndex,
                            ax1, rawData, times, dat, epochStart)
    ax.set_xlabel(r"$t\;(\rm{s})$")
    
    # # q 
    rawData = dat.measurements[:, 2]
    latexVar = r"q"
    xCoord = 1
    stateIndexes = [3, 4, 5]
    yCoord = 0
    modLatexVar = r"{" + latexVar + r"}"
    latexUnits = r"\rm{tic}"
    epochDataIndex = stateIndexes[0]
    filtOps = [lambda(x): filt.filt(x) for filt in filters]
    ax = plotInvestigationSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar,
                            latexUnits, filtOps, epochs, filtersMS, epochDataIndex,
                            ax1, rawData, times, dat, epochStart)
    low, high = ax.get_ybound()
    ax.plot(times, rawData, 'k', linewidth=1, alpha=0.7)
    ax.set_ybound(low, high)
    
    # # q Dot
    yCoord = 1
    modLatexVar = r"\dot{" + latexVar + r"}"
    latexUnits = r"\frac{\rm{tic}}{\rm{s}}"
    epochDataIndex = stateIndexes[1]
    filtOps = [lambda(x): filt.diff(x) for filt in filters]
    ax = plotInvestigationSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar,
                            latexUnits, filtOps, epochs, filtersMS, epochDataIndex,
                            ax1, rawData, times, dat, epochStart)
    
    # # q Double Dot
    yCoord = 2
    modLatexVar = r"\ddot{" + latexVar + r"}"
    latexUnits = r"\frac{\rm{tic}}{\rm{s}^2}"
    epochDataIndex = stateIndexes[2]
    filtOps = [lambda(x): filt.ddif(x) for filt in filters]
    ax = plotInvestigationSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar,
                            latexUnits, filtOps, epochs, filtersMS, epochDataIndex,
                            ax1, rawData, times, dat, epochStart)
    ax.set_xlabel(r"$t\;(\rm{s})$")
    
    # ## End q, begin S
    xCoord, yCoord = 2, 2
    rawData = dat.measurements[:, 1]
    modLatexVar = r"{s_{\rm{enc}}}"
    latexUnits = r"\rm{tic}"
    epochDataIndex = 6
    filtOps = [lambda(x): filt.filt(x) for filt in filters]
    ax = plotInvestigationSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar,
                            latexUnits, filtOps, epochs, filtersMS, epochDataIndex,
                            ax1, rawData, times, dat, epochStart)
    low, high = ax.get_ybound()
    ax.plot(times, rawData, 'k', linewidth=1, alpha=0.7)
    ax.set_ybound(low, high)
    ax.set_xlabel(r"$t\;(\rm{s})$")

    return ymlFig
def investigateDyNetTraining2(yamlFileName, thetaLabel, filtersMS, epochs, epochStart):
    # # Assumes alternate position-delta state space representation
    dat = manager.loadDyNetYaml(yamlFileName)
    ymlFig = plt.figure(figsize=(8.0, 10), facecolor='w')
    dt = 0.0005
    times = np.array(range(0, len(dat.measurements))) * dt
    filters = []
    for ms in filtersMS:
        filters.extend([ConvolutionFilter(ms * 1e-3, dt)])
    xSubs = 1
    ySubs = 9
    #
    latexVar = r"X"  #
    #
    rawData = dat.measurements[:, 2]
    xCoord = 0
    stateIndexes = [0, 1, 2]
    yCoord = 0
    ax1 = plt.subplot(ySubs, xSubs, 1 + yCoord * xSubs + xCoord)
    ax = ax1
    
    modLatexVar = r"{" + latexVar + r"}"
    latexUnits = r"\rm{tic}"
    epochDataIndex = stateIndexes[0]
    filtOps = [lambda(x): filt.filt(x) for filt in filters]
    ax = plotInvestigationSubfigNoShareX(xCoord, yCoord, xSubs, ySubs, modLatexVar,
                            latexUnits, filtOps, epochs, filtersMS, epochDataIndex,
                            rawData, times, dat, epochStart)
    low, high = ax.get_ybound()
    ax.plot(times, rawData, 'k', linewidth=1, alpha=0.7)
    ax.set_ybound(low, high)
    ax1 = ax
    
    yCoord = 1
    modLatexVar = r"\dot{" + latexVar + r"}"
    latexUnits = r"\frac{\rm{tic}}{\rm{s}}"
    epochDataIndex = stateIndexes[1]
    filtOps = [lambda(x): filt.diff(x) for filt in filters]
    ax = plotInvestigationSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar,
                            latexUnits, filtOps, epochs, filtersMS, epochDataIndex,
                            ax1, rawData, times, dat, epochStart)
    
    # # theta Double Dot
    yCoord = 2
    modLatexVar = r"\ddot{" + latexVar + r"}"
    latexUnits = r"\frac{\rm{tic}}{\rm{s}^2}"
    epochDataIndex = stateIndexes[2]
    filtOps = [lambda(x): filt.ddif(x) for filt in filters]
    ax = plotInvestigationSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar,
                            latexUnits, filtOps, epochs, filtersMS, epochDataIndex,
                            ax1, rawData, times, dat, epochStart)
    ax.set_xlabel(r"$t\;(\rm{s})$")
    
    #
    latexVar = r"\Delta" 
    #
    rawData = dat.measurements[:, 1] - 8885
    xCoord = 0
    stateIndexes = [3, 4, 5]
    yCoord = 3
    modLatexVar = r"{" + latexVar + r"}"
    latexUnits = r"\rm{tic}"
    epochDataIndex = stateIndexes[0]
    filtOps = [lambda(x): filt.filt(x) for filt in filters]
    ax = plotInvestigationSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar,
                            latexUnits, filtOps, epochs, filtersMS, epochDataIndex,
                            ax1, rawData, times, dat, epochStart)
    low, high = ax.get_ybound()
    ax.plot(times, rawData, 'k', linewidth=1, alpha=0.7)
    ax.plot(times, -200.0/(2048.0)*dat.inputs[:,0], color='m', linewidth=1, alpha=0.7)
    ax.set_ybound(low, high)
    
    # # q Dot
    yCoord = 4
    modLatexVar = r"\dot{" + latexVar + r"}"
    latexUnits = r"\frac{\rm{tic}}{\rm{s}}"
    epochDataIndex = stateIndexes[1]
    filtOps = [lambda(x): filt.diff(x) for filt in filters]
    ax = plotInvestigationSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar,
                            latexUnits, filtOps, epochs, filtersMS, epochDataIndex,
                            ax1, rawData, times, dat, epochStart)
    
    # # q Double Dot
    yCoord = 5
    modLatexVar = r"\ddot{" + latexVar + r"}"
    latexUnits = r"\frac{\rm{tic}}{\rm{s}^2}"
    epochDataIndex = stateIndexes[2]
    filtOps = [lambda(x): filt.ddif(x) for filt in filters]
    ax = plotInvestigationSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar,
                            latexUnits, filtOps, epochs, filtersMS, epochDataIndex,
                            ax1, rawData, times, dat, epochStart)
    ax.set_xlabel(r"$t\;(\rm{s})$")
    
#     # ## End q, begin S
    xCoord, yCoord = 0, 6
    rawData = dat.measurements[:, 0]
    measurementDataIndex = 0
    modLatexVar = r"{\rm{enc}_{Mot}}"
    latexUnits = r"\rm{tic}"
    plotMeasurementComparisonSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar, latexUnits,
                             epochs, measurementDataIndex, ax1, rawData, times, dat, epochStart)
    
    xCoord, yCoord = 0, 7
    rawData = dat.measurements[:, 1]
    measurementDataIndex = 1
    modLatexVar = r"{\rm{enc}_{Spr}}"
    plotMeasurementComparisonSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar, latexUnits,
                             epochs, measurementDataIndex, ax1, rawData, times, dat, epochStart)
    
    xCoord, yCoord = 0, 8
    rawData = dat.measurements[:, 2]
    measurementDataIndex = 2
    modLatexVar = r"{\rm{enc}_{Jnt}}"
    plotMeasurementComparisonSubfig(xCoord, yCoord, xSubs, ySubs, modLatexVar, latexUnits,
                             epochs, measurementDataIndex, ax1, rawData, times, dat, epochStart)

    return ymlFig
def plotTrainingEpoch(dataFromYaml, epochNumber):
    fig = plt.figure(figsize=(12, 8), facecolor='w')
    
def main():
    print "hello world"
#     data = manager.loadRawMatrix(rawSweep5)
#     to = 23660  +45*2000# sweepTest5
#     to=to+180000
#     tf = 530000  # sweepTest5
#     tf=to+270000-180000
#     tf= to+1000 # HalfSecond ofr exp1Short
#     tf= to+2000 # Two and half seconds for exp2Short
#     plotSimpleData(data, to, tf, "exp3Short")
#     buildSimpleDataSet(data, to, tf, "exp3Short.iodat")  
  

#     plotFancySharedX(data, to, tf, "exp1Fancy")  
    manager.saveFig(investigateDyNetTraining2("exp3PosDef1Epoch79.yaml", 
                                              r"\theta",  [12], [0], 79),   "exp3PosDef1Epoch79Tall.pdf")
#     manager.saveFig(investigateDyNetTraining2("exp2PosDef0Epoch3.yaml", r"\theta",  [2], [0], 3),   "exp2PosDef0Epoch3.pdf")
#     manager.saveFig(investigateDyNetTraining2("exp2PosDef0Epoch4.yaml", r"\theta",  [2], [0], 4),   "exp2PosDef0Epoch4.pdf")
#     manager.saveFig(investigateDyNetTraining2("exp2PosDef0Epoch10.yaml", r"\theta", [2], [0], 10),  "exp2PosDef0Epoch10.pdf")
#     manager.saveFig(investigateDyNetTraining2("exp2PosDef0Epoch15.yaml", r"\theta", [2], [0], 15),  "exp2PosDef0Epoch15.pdf")
#     manager.saveFig(investigateDyNetTraining("exp1ShortTr2Epoch5.yaml", r"\theta", [2], [0], 5), "exp1ShortTr2Epoch5.pdf")
#     manager.saveFig(investigateDyNetTraining2("exp2PosDef0Epoch5.yaml", r"\theta",  [4], [0], 5),   "exp2PosDef0Epoch5.pdf")                                                
#     manager.saveFig(investigateDyNetTraining2("exp2PosDef0Epoch35.yaml", r"\theta", [8], [0], 35),  "exp2PosDef0Epoch35.pdf")
#     manager.saveFig(investigateDyNetTraining2("exp2PosDef0Epoch79.yaml", r"\theta", [4], [0], 79),  "exp2PosDef0Epoch79.pdf")
#     saveTrainingSet("sweepTest5.trn", data[to:tf, 1].reshape((-1, 1)), data[to:tf, 2:4])
    plt.show()
    manager.saveFigsAfterPltShow()
    
if __name__ == '__main__':
    main()
