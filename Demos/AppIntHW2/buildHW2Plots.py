# //{    Training error and validation error, simulated data, feedforward network}
# //{    Training error and validation error, experimental data, feedforward network}
# //{    Training error and validation error, simulated data, recurrant network}
# //{    Training error and validation error, experimental data, recurrant network}

import scipy.integrate as integrate
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from networkUtils.pendulumDemo import *
from networkUtils.dyNetUtils.trainingFileManager import * 
from networkUtils.dyNetUtils.convolutionFilter import *
from matplotlib.backends.backend_pdf import PdfPages

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

Run1 = "pendubot_run1.dat"
Run2 = "pendubot_run2.dat"
Run3 = "pendubot_run3.dat"
Impulses = "pendubot_impulses.dat"
Steps = "pendubot_steps.dat"
output_filename = "example_out.dat"
manager = TrainingFileManager("C:/Users/Gray/wk/grayutils/Demos/AppIntHW2");
# prefix, style, run => expff1 exp[ff or rc][1,2,3,i,s]
def saveFFTrainingData(run, flag, minDat, maxDat, filtSigma):
    filter = ConvolutionFilter(filtSigma, 0.01)
    rawData = manager.loadRawMatrix(run)
    omit = 10
    inputs = rawData[minDat:maxDat, 0]
    thetas = rawData[minDat:maxDat, 1]
    experimentalData = np.array([[x for x in filter.filt(thetas)[omit:-omit]],
                               [x for x in filter.diff(thetas)[omit:-omit]],
                               [x for x in inputs[omit:-omit]],
                               [x for x in filter.ddif(thetas)[omit:-omit]]]).transpose()
    manager.saveIODat("expff" + flag + ".iodat", experimentalData, 3, 1)
def saveRCTrainingData(run, flag, minDat, maxDat):
    rawData = manager.loadRawMatrix(run)
    experimentalData = np.array([rawData[minDat:maxDat, 0],
                               rawData[minDat:maxDat, 1]]).transpose()
    manager.saveIODat("exprc" + flag + ".iodat", experimentalData, 1, 1)
def saveAll():
    struct = [{"min":800, "max":1580, "name":Run1, "flag":"1", "filt": 0.04},
            {"min":600, "max":1400, "name":Run2, "flag":"2", "filt": 0.04},
            {"min":700, "max":1300, "name":Run3, "flag":"3", "filt": 0.04},
            {"min":2300, "max":2600, "name":Impulses, "flag":"i", "filt": 0.04},
            {"min":0, "max":1200, "name":Steps, "flag":"s", "filt": 0.01}]
    for ob in struct:
        saveFFTrainingData(ob["name"], ob["flag"], ob["min"], ob["max"], ob["filt"])
        saveRCTrainingData(ob["name"], ob["flag"], ob["min"], ob["max"])
        
def getDataAndPlotSpecificDataset():
    filter = ConvolutionFilter(0.1, 0.01)
    print "hello world"
    rawData = manager.loadRawMatrix(Steps)
    minDat = 0
    maxDat = 1200
    omit = 10
    numberOfData = maxDat - minDat - omit - omit
    thetas = rawData[minDat:maxDat, 1]
    inputs = rawData[minDat:maxDat, 0]
    fig = plt.figure()
    times = np.linspace(0, (numberOfData) * 0.01, numberOfData)
    plt.plot(times, thetas[omit:-omit])
    rotations = []
    experimentalData = np.array([filter.filt(thetas)[omit:-omit],
                               filter.diff(thetas)[omit:-omit],
                               rawData[minDat + omit:maxDat - omit, 0],
                               filter.ddif(thetas)[omit:-omit]]).transpose()
    manager.saveIODat("expffs.iodat", experimentalData, 3, 1)
    
    lines = plt.plot(times, filter.filt(thetas)[omit:-omit], linewidth=4.0)
    lines = plt.plot(times, filter.diff(thetas)[omit:-omit] / 10)
    plt.setp(lines, linewidth=4.0)
    lines = plt.plot(times, filter.ddif(thetas)[omit:-omit] / 100)
    plt.setp(lines, linewidth=4.0)
    lines = plt.plot(times, filter.filt(inputs)[omit:-omit])
    plt.setp(lines, linewidth=4.0)
    plt.plot(times, rawData[minDat + omit:maxDat - omit, 0])
    plt.legend([r"$\theta_{\rm{raw}}\;\rm{rad}$",
                r"$\theta_{\rm{filt}}\;\rm{rad}$",
                r"$\omega_{\rm{filt}} \;\frac{\rm{rad}}{\rm{ds}}$",
                r"$\alpha_{\rm{filt}} \;\frac{\rm{rad}}{\rm{ds}^2}$",
                r"$u_{\rm{filt}}$"
                r"$u_{\rm{raw}}$"])
    plt.title(r"\TeX\ is Number "
          r"$\displaystyle\sum_{n=1}^\infty\frac{-e^{i\pi}}{2^n}$!",
          fontsize=16, color='gray')
    plt.xlabel("time in seconds")
    return fig
def softGravFricPlots():
    gravFricPlots(name="DDYsim4.yaml", nlin=1e1, nameAddition="Soft")
def gravFricPlots(name="dlsrcsim3.yaml", nlin=1e6, nameAddition=""):
    dat = manager.loadDyNetYaml(name)
    ideal = np.array([49.7633, 1.76566, 1.47403, 445.626])
    epoch3 = dat.epochData[3].parameters_used
    epoch15 = dat.epochData[15].parameters_used
    grav = lambda((theta, a)):-(math.sin(theta) * a)
    fricHard = lambda((theta, omega, b, c)):-(math.tanh(1e6 * omega) * b + c * omega)
    fric = lambda((theta, omega, b, c)):-(math.tanh(nlin * omega) * b + c * omega)
    gravThetas = np.linspace(-math.pi / 2, math.pi / 2, 400)
    idealGrav = np.array([grav((theta, ideal[0])) for theta in gravThetas])
    epoch3Grav = np.array([grav((theta, epoch3[0])) for theta in gravThetas])
    epoch15Grav = np.array([grav((theta, epoch15[0])) for theta in gravThetas])
    fig = plt.figure(figsize=(6.0, 3), facecolor='w')
    plt.plot(gravThetas, idealGrav, linewidth=2)
    plt.plot(gravThetas, epoch3Grav, linewidth=2)
    plt.plot(gravThetas, epoch15Grav, linewidth=2)
    plt.xlabel(r"$\theta$")
    plt.ylabel(r"$\alpha_{\rm{gravity}}$")
    plt.legend([r"sim", "epoch3", "epoch15"])
    manager.saveFig(fig, "gravityPlot" + nameAddition + ".pdf")
    
    fricTheta1 = 0
    frictionOmegas = np.linspace(-1.0, 1.0, 400)
    idealZeroThetaFric = np.array([fricHard((fricTheta1, omega, ideal[1], ideal[2])) for omega in frictionOmegas])
    epoch3ZeroThetaFric = np.array([fric((fricTheta1, omega, epoch3[1], epoch3[2])) for omega in frictionOmegas])
    epoch15ZeroThetaFric = np.array([fric((fricTheta1, omega, epoch15[1], epoch15[2])) for omega in frictionOmegas])
    fig = plt.figure(figsize=(6.0, 3), facecolor='w')
    plt.plot(frictionOmegas, idealZeroThetaFric, linewidth=2)
    plt.plot(frictionOmegas, epoch3ZeroThetaFric, linewidth=2)
    plt.plot(frictionOmegas, epoch15ZeroThetaFric, linewidth=2)
    plt.xlabel(r"$\dot{\theta}$")
    plt.ylabel(r"$\alpha_{\rm{friciton}}$")
    plt.legend([r"sim", "epoch3", "epoch15"])
    manager.saveFig(fig, "frictionPlot" + nameAddition + ".pdf")


def getDataAndPlotSimDataset():
    simff1Dat = manager.loadIODat("simff1.iodat")
    fig = plt.figure()
    times = np.linspace(0, (len(simff1Dat)) * 0.01, len(simff1Dat))
    plt.plot(times, simff1Dat[:, 0], linewidth=2.0)
    plt.plot(times, simff1Dat[:, 1] / 10, linewidth=2.0)
    plt.plot(times, simff1Dat[:, 3] / 100, linewidth=2.0)
    plt.plot(times, simff1Dat[:, 2], linewidth=2.0)
    plt.legend([r"$\theta_{\rm{sim}}\;\rm{rad}$",
                r"$\omega_{\rm{sim}} \;\frac{\rm{rad}}{\rm{ds}}$",
                r"$\alpha_{\rm{sim}} \;\frac{\rm{rad}}{\rm{ds}^2}$",
                r"$u_{\rm{sim}}$"], loc='best')
    plt.title(r"Simulation Data",
          fontsize=16, color='black')
    plt.xlabel("time in seconds")
    return fig

def plotFFTraining(filename):
    gdff1Dat = manager.loadRawMatrix(filename)
    fig = plt.figure(figsize=(6.0, 3), facecolor='w')
    plt.plot(gdff1Dat[0:1000, 0], gdff1Dat[0:1000, [1, 2]], linewidth=3.0)
    leg = plt.legend(["training error", "validation error"], fontsize=16, loc='best')
    plt.xlabel("Epochs")
    plt.ylabel("Error")
    return fig

def plotDyNetFFTrainer(name, thetaLabel):
    dyNetFF1 = manager.loadDyNetYaml(name)
    ymlFig = plt.figure(figsize=(8.0, 3), facecolor='w')
    plt.plot(dyNetFF1.epochData[4].measurements_predicted, linewidth=2)
    plt.plot(dyNetFF1.epochData[3].measurements_predicted, linewidth=2)
    plt.plot(dyNetFF1.epochData[2].measurements_predicted, linewidth=2)
    plt.plot(dyNetFF1.epochData[1].measurements_predicted, linewidth=2)
    plt.plot(dyNetFF1.epochData[0].measurements_predicted, linewidth=2)
    lowy, highy = plt.gca().get_ybound()
    plt.plot(dyNetFF1.measurements, 'k', linewidth=1, alpha=0.7)
    plt.gca().set_ybound(lowy, highy)
    plt.xlabel(r"t\;(\rm{s})")
    plt.ylabel(r"\ddot{\theta}\;(\frac{\rm{rad}}{\rm{s}^2})")
    leg = plt.legend([r"\ddot{\theta}_{\rm{epoch}_4}", r"\ddot{\theta}_{\rm{epoch}_3}",
                    r"\ddot{\theta}_{\rm{epoch}_2}", r"\ddot{\theta}_{\rm{epoch}_1}",
                    r"\ddot{\theta}_{\rm{epoch}_0}", thetaLabel],
                   loc='best', fancybox=True)
    low, high = plt.gca().get_xbound()
    plt.gca().set_xbound(low, high + (high - low) * 0.3)  # space for legend
    leg.get_frame().set_alpha(0.5)
    return ymlFig
def plotDyNetRCTrainer(name, thetaLabel):
    dyNetFF1 = manager.loadDyNetYaml(name)
    ymlFig = plt.figure(figsize=(8.0, 3), facecolor='w')
    times = np.array(range(0, len(dyNetFF1.measurements))) * 0.01
    plt.plot(times, dyNetFF1.epochData[15].measurements_predicted, linewidth=2)
    plt.plot(times, dyNetFF1.epochData[7].measurements_predicted, linewidth=2)
    plt.plot(times, dyNetFF1.epochData[3].measurements_predicted, linewidth=2)
    plt.plot(times, dyNetFF1.epochData[1].measurements_predicted, linewidth=2)
    plt.plot(times, dyNetFF1.epochData[0].measurements_predicted, linewidth=2)
    lowy, highy = plt.gca().get_ybound()
    plt.plot(times, dyNetFF1.measurements, 'k', linewidth=1, alpha=0.7)
    plt.gca().set_ybound(lowy, highy)
    plt.xlabel(r"t\;(\rm{s})")
    plt.ylabel(r"\theta\;(\rm{rad})")
    leg = plt.legend([r"\theta_{\rm{epoch}_{15}}", r"\theta_{\rm{epoch}_7}",
                    r"\theta_{\rm{epoch}_3}", r"\theta_{\rm{epoch}_1}",
                    r"\theta_{\rm{epoch}_0}", thetaLabel],
                   loc='best', fancybox=True)
    low, high = plt.gca().get_xbound()
    plt.gca().set_xbound(low, high + (high - low) * 0.3)  # space for legend
    leg.get_frame().set_alpha(0.5)
    return ymlFig

def plotDyNetShooting(thetaLabel, nameBase):
#     dyNetFF1 = manager.loadDyNetYaml(name)
#     fileBase<<number<<"Ep"<<i<<"Tr.iodat";
#     manager.loadIODat(nameBase+"Ep"+str(0)+"Tr.iodat")[4]
    ymlFig = plt.figure(figsize=(8.0, 3), facecolor='w')
    times = np.array(range(0, len(manager.loadIODat(nameBase + "Ep" + str(15) + "Tr.iodat")[:, 4]))) * 0.01
#     plt.plot(times, dyNetFF1.epochData[15].measurements_predicted, linewidth=2)
#     plt.plot(times, dyNetFF1.epochData[7].measurements_predicted, linewidth=2)
#     plt.plot(times, dyNetFF1.epochData[3].measurements_predicted, linewidth=2)
#     plt.plot(times, dyNetFF1.epochData[1].measurements_predicted, linewidth=2)
#     plt.plot(times, dyNetFF1.epochData[0].measurements_predicted, linewidth=2)
    plt.plot(times, manager.loadIODat(nameBase + "Ep" + str(15) + "Tr.iodat")[:, 4], linewidth=2)
    plt.plot(times, manager.loadIODat(nameBase + "Ep" + str(7) + "Tr.iodat")[:, 4], linewidth=2)
    plt.plot(times, manager.loadIODat(nameBase + "Ep" + str(3) + "Tr.iodat")[:, 4], linewidth=2)
    plt.plot(times, manager.loadIODat(nameBase + "Ep" + str(1) + "Tr.iodat")[:, 4], linewidth=2)
    plt.plot(times, manager.loadIODat(nameBase + "Ep" + str(0) + "Tr.iodat")[:, 4], linewidth=2)
    lowy, highy = plt.gca().get_ybound()
    plt.plot(times, manager.loadIODat(nameBase + "Ep" + str(0) + "Tr.iodat")[:, 3], 'k', linewidth=1, alpha=0.7)
    plt.gca().set_ybound(lowy, highy)
    plt.xlabel(r"t\;(\rm{s})")
    plt.ylabel(r"\theta\;(\rm{rad})")
    leg = plt.legend([r"\theta_{\rm{shoot}_{15}}", r"\theta_{\rm{shoot}_7}",
                    r"\theta_{\rm{shoot}_3}", r"\theta_{\rm{shoot}_1}",
                    r"\theta_{\rm{shoot}_0}", thetaLabel],
                   loc='best', fancybox=True)
    low, high = plt.gca().get_xbound()
    plt.gca().set_xbound(low, high + (high - low) * 0.3)  # space for legend
    leg.get_frame().set_alpha(0.5)
    return ymlFig
def plotDyNetFFError(name):
    ymlDat = manager.loadDyNetYaml(name)
    errors = [epoch.error for epoch in ymlDat.epochData]
    ymlFig = plt.figure(figsize=(4.0, 1.5), facecolor='w')
    plt.plot(errors, linewidth=2)
    plt.xlabel("Epochs")
    plt.ylabel("Error")
    return ymlFig
def makeDyNetPlots():
    manager.saveFig(plotDyNetFFError("dlsrcexps0.yaml"), "dlsrcexps0Err.pdf")
    manager.saveFig(plotDyNetRCTrainer("dlsrcexps0.yaml", r"\theta_{\rm{steps}}"), "dlsrcexps0Fig.pdf")
    manager.saveFig(plotDyNetFFError("dlsrcexpi0.yaml"), "dlsrcexpi0Err.pdf")
    manager.saveFig(plotDyNetRCTrainer("dlsrcexpi0.yaml", r"\theta_{\rm{impulses}}"), "dlsrcexpi0Fig.pdf")
    manager.saveFig(plotDyNetFFError("dlsrcexp1.yaml"), "dlsrcexp1Err.pdf")
    manager.saveFig(plotDyNetRCTrainer("dlsrcexp1.yaml", r"\theta_{\rm{exp1}}"), "dlsrcexp1Fig.pdf")
    manager.saveFig(plotDyNetFFError("dlsrcexp2.yaml"), "dlsrcexp2Err.pdf")
    manager.saveFig(plotDyNetRCTrainer("dlsrcexp2.yaml", r"\theta_{\rm{exp2}}"), "dlsrcexp2Fig.pdf")
    manager.saveFig(plotDyNetFFError("dlsrcexp3.yaml"), "dlsrcexp3Err.pdf")
    manager.saveFig(plotDyNetRCTrainer("dlsrcexp3.yaml", r"\theta_{\rm{exp3}}"), "dlsrcexp3Fig.pdf")
    
    
def makeDyNetTrainingPlot(name):
    dat = manager.loadIODat(name)
    fig = plt.figure(figsize=(7.5, 3.5), facecolor='w')
    plt.semilogy(dat[:, 0], dat[:, 1:4])
    low, high = plt.gca().get_ybound()
    plt.gca().set_ybound(low * (low / high) ** 1.5, high)
    plt.xlabel(r"epoch")
    plt.ylabel("error")
    plt.legend([r"$\sum^T_{t=0}||e_{p,\rm{prediciton}}||_{\Sigma_P}+\sum^T_{t=1}||e_{p,\rm{update}}||_{\Sigma_U}$ trainingData",
                r"$\sum^T_{t=0}||e_{p,\rm{prediciton\;(shooting)}}||_{\Sigma_P}$ trainingData",
                r"$\sum^T_{t=0}||e_{p,\rm{prediciton\;(shooting)}}||_{\Sigma_P}$ validationData"],
               loc="best")
    return fig

    
def makeDyNetValidationPlots():
#     manager.saveFig(makeDyNetTrainingPlot("dlsrcexp1.trn" ), "dlsrcexp1Trn.pdf" )
#     manager.saveFig(makeDyNetTrainingPlot("dlsrcexp2.trn" ), "dlsrcexp2Trn.pdf" )
#     manager.saveFig(makeDyNetTrainingPlot("dlsrcexp3.trn" ), "dlsrcexp3Trn.pdf" )
    manager.saveFig(makeDyNetTrainingPlot("dlsrcexps0.trn"), "dlsrcexps0Trn.pdf")
    manager.saveFig(makeDyNetTrainingPlot("dlsrcexpi0.trn"), "dlsrcexpi0Trn.pdf")
#     manager.saveFig(makeDyNetTrainingPlot("dlsrcsim1.trn" ), "dlsrcsim1Trn.pdf" )
#     manager.saveFig(makeDyNetTrainingPlot("dlsrcsim2.trn" ), "dlsrcsim2Trn.pdf" )
#     manager.saveFig(plotDyNetShooting( r"\theta_{\rm{ exp1}}", "dlsrcexp1"),  "dlsrcexp1Shoot.pdf")
#     manager.saveFig(plotDyNetShooting( r"\theta_{\rm{ exp2}}", "dlsrcexp2"),  "dlsrcexp2Shoot.pdf")
#     manager.saveFig(plotDyNetShooting( r"\theta_{\rm{ exp3}}", "dlsrcexp3"),  "dlsrcexp3Shoot.pdf")
    manager.saveFig(plotDyNetShooting(r"\theta_{\rm{steps}}", "dlsrcexps0"), "dlsrcexps0Shoot.pdf")
    manager.saveFig(plotDyNetShooting(r"\theta_{\rm{impulses}}", "dlsrcexpi0"), "dlsrcexpi0Shoot.pdf")
#     manager.saveFig(plotDyNetShooting( r"\theta_{\rm{ sim1}}", "dlsrcsim1"),  "dlsrcsim1Shoot.pdf")
#     manager.saveFig(plotDyNetShooting( r"\theta_{\rm{ sim2}}", "dlsrcsim2"),  "dlsrcsim2Shoot.pdf")
    
def makeSimpleResponsePlot(name):
    dat = manager.loadIODat(name)
    fig = plt.figure(figsize=(3, 1), facecolor='w')
    plt.plot(0.01 * np.array(range(0, len(dat))), dat[:, 1], linewidth=2)
    plt.xlabel(r"$t\;(\rm{s})$")
    plt.ylabel(r"$\theta\;(\rm{rad})$")
    return fig

def calculateVariance(dataYaml, epoch):
    vec = dataYaml.epochData[epoch].measurements_predicted - dataYaml.measurements
    n = len(vec)
    var = 1.0 / (n + 1.0) * vec.T.dot(vec)
    return var

def plotTestLowPass(name, thetaLabel):
    dat = manager.loadDyNetYaml(name)
    ymlFig = plt.figure(figsize=(12.0, 8), facecolor='w')
    ax1 = plt.subplot(3, 1, 1)
    sigma050ms = ConvolutionFilter(0.050, 0.01)
    sigma100ms = ConvolutionFilter(0.100, 0.01)
    sigma200ms = ConvolutionFilter(0.200, 0.01)
    times = np.array(range(0, len(dat.measurements))) * 0.01
    ax1.plot(times, dat.epochData[79].measurements_predicted, linewidth=2)
    ax1.plot(times, dat.epochData[7].measurements_predicted, linewidth=2)
    print "times", times.shape
    print "filtMeasurenets", (sigma050ms.filt(dat.measurements[:, 0])).shape
    lowFilt = sigma050ms.filt(dat.measurements[:, 0])
    midFilt = sigma100ms.filt(dat.measurements[:, 0])
    hiiFilt = sigma200ms.filt(dat.measurements[:, 0])
    ax1.plot(times, lowFilt, linewidth=2)
    ax1.plot(times, midFilt, linewidth=2)
    ax1.plot(times, hiiFilt, linewidth=2)
    ax1.plot(times, dat.measurements, 'k', linewidth=1, alpha=0.7)
#     ax1.set_xlabel(r"$t\;(\rm{s})$")
    ax1.set_ylabel(r"$\theta\;(\rm{rad})$")
    leg = ax1.legend([r"$\theta_{\rm{epoch}_{79}}$",
                      r"$\theta_{\rm{epoch}_7}$",
                      r"$\theta_{\rm{filt,\sigma=050ms}}$",
                      r"$\theta_{\rm{filt,\sigma=100ms}}$",
                      r"$\theta_{\rm{filt,\sigma=200ms}}$", thetaLabel],
                   loc='right', fancybox=True)
    leg.get_frame().set_alpha(0.5)
    ax2 = plt.subplot(3, 1, 2, sharex=ax1)
    ax2.plot(times[1:], dat.epochData[79].states_used[:-1, 1], linewidth=2)
    ax2.plot(times[1:], dat.epochData[15].states_used[:-1, 1], linewidth=2)
    ax2.plot(times[1:], sigma050ms.diff(dat.measurements[1:, 0]), linewidth=2)
    ax2.plot(times[1:], sigma100ms.diff(dat.measurements[1:, 0]), linewidth=2)
    ax2.plot(times[1:], sigma200ms.diff(dat.measurements[1:, 0]), linewidth=2)
#     ax2.set_xlabel(r"$t\;(\rm{s})$")
    ax2.set_ylabel(r"$\dot\theta\;(\rm{rad})$")
    leg = ax2.legend([r"$\dot{\theta}_{\rm{epoch}_{79}}$",
                      r"$\dot{\theta}_{\rm{epoch}_{15}}$",
                      r"$\dot{\theta}_{\rm{filt,\sigma=050ms}}$",
                      r"$\dot{\theta}_{\rm{filt,\sigma=100ms}}$",
                      r"$\dot{\theta}_{\rm{filt,\sigma=200ms}}$"],
                   loc='right', fancybox=True)
    leg.get_frame().set_alpha(0.5)
    ax3 = plt.subplot(3, 1, 3, sharex=ax1)
    ax3.plot(times[1:], dat.epochData[79].states_used[:-1, 2], linewidth=2)
    ax3.plot(times[1:], dat.epochData[15].states_used[:-1, 2], linewidth=2)
    ax3.plot(times[1:], sigma050ms.ddif(dat.measurements[1:, 0]), linewidth=2)
    ax3.plot(times[1:], sigma100ms.ddif(dat.measurements[1:, 0]), linewidth=2)
    ax3.plot(times[1:], sigma200ms.ddif(dat.measurements[1:, 0]), linewidth=2)
    ax3.set_xlabel(r"$t\;(\rm{s})$")
    ax3.set_ylabel(r"$\ddot\theta\;(\rm{rad})$")
    leg = ax3.legend([r"$\ddot{\theta}_{\rm{epoch}_{79}}$",
                      r"$\ddot{\theta}_{\rm{epoch}_{15}}$",
                      r"$\ddot{\theta}_{\rm{filt,\sigma=050ms}}$",
                      r"$\ddot{\theta}_{\rm{filt,\sigma=100ms}}$",
                      r"$\ddot{\theta}_{\rm{filt,\sigma=200ms}}$"],
                   loc='right', fancybox=True)
#     low, high = ax1.get_xbound()
#     ax1.set_xbound(low, high + (high - low) * 0.3)  # space for legend
    leg.get_frame().set_alpha(0.5)
    return ymlFig

def investigateDyNetTraining(yamlFileName, thetaLabel, filtersMS, epochs):
    dat = manager.loadDyNetYaml(yamlFileName)
    ymlFig = plt.figure(figsize=(12.0, 8), facecolor='w')
    times = np.array(range(0, len(dat.measurements))) * 0.01
    filters = []
    for ms in filtersMS:
        filters.extend([ConvolutionFilter(ms * 1e-3, 0.01)])
    
    ax1 = plt.subplot(3, 1, 1)
    for epoch in epochs:
        ax1.plot(times, dat.epochData[epoch].measurements_predicted, linewidth=2)
    for filt in filters:
        ax1.plot(times, filt.filt(dat.measurements[:, 0]), linewidth=2)
    ax1.plot(times, dat.measurements, 'k', linewidth=1, alpha=0.7)
    ax1.set_ylabel(r"$\theta\;(\rm{rad})$")
    leg1 = []
    for epoch in epochs:
        leg1.append(r"$\theta_{\rm{epoch}_{" + str(epoch) + r"}}$")
    for ms in filtersMS:
        leg1.append(r"$\theta_{\rm{filt,\sigma=" + str(ms) + r"ms}}$")
    leg1.append(thetaLabel)
    leg = ax1.legend(leg1, loc='right', fancybox=True)
    leg.get_frame().set_alpha(0.5)
    
    
    ax2 = plt.subplot(3, 1, 2, sharex=ax1)
    for epoch in epochs:
        ax2.plot(times[1:], dat.epochData[epoch].states_used[:-1, 1], linewidth=2)
    for filt in filters:
        ax2.plot(times[1:], filt.diff(dat.measurements[1:, 0]), linewidth=2)
    ax2.set_ylabel(r"$\dot\theta\;(\frac{\rm{rad}}{\rm{s}})$")
    leg2 = []
    for epoch in epochs:
        leg2.append(r"$\dot{\theta}_{\rm{epoch}_{" + str(epoch) + r"}}$")
    for ms in filtersMS:
        leg2.append(r"$\dot{\theta}_{\rm{filt,\sigma=" + str(ms) + r"ms}}$")
    leg = ax2.legend(leg2, loc='right', fancybox=True)
    leg.get_frame().set_alpha(0.5)
    
    
    ax3 = plt.subplot(3, 1, 3, sharex=ax1)
    for epoch in epochs:
        ax3.plot(times[2:], dat.epochData[epoch].states_used[1:-1, 2], linewidth=2)
    for epoch in epochs:
        ax3.plot(times[2:], dat.epochData[epoch].states_predicted[:-2, 2], linewidth=2)
    for filt in filters:
        ax3.plot(times[2:], filt.ddif(dat.measurements[2:, 0]), linewidth=2)
    ax3.set_xlabel(r"$t\;(\rm{s})$")
    ax3.set_ylabel(r"$\ddot\theta\;(\frac{\rm{rad}}{\rm{s}^2})$")
    leg3 = []
    for epoch in epochs:
        leg3.append(r"$\ddot{\theta}_{\rm{epoch}_{" + str(epoch) + r"}}$")
    for epoch in epochs:
        leg3.append(r"$\ddot{\theta}_{\rm{predicted, epoch}_{" + str(epoch) + r"}}$")
    for ms in filtersMS:
        leg3.append(r"$\ddot{\theta}_{\rm{filt,\sigma=" + str(ms) + r"ms}}$")
    leg = ax3.legend(leg3, loc='right', fancybox=True)
    
#     low, high = ax1.get_xbound()
#     ax1.set_xbound(low, high + (high - low) * 0.3)  # space for legend
    leg.get_frame().set_alpha(0.5)
    return ymlFig

def investigationsAndTheirHistory():
    # keeps around all the lines which generated old figures
#     manager.saveFig(investigateDyNetTraining("V3Net42.yaml", r"\theta_{\rm{steps}}", 
#                                              [25], [1, 2, 4, 8]), "V3Net42Inv1.pdf") 
#     manager.saveFig(investigateDyNetTraining("V3Net42.yaml", r"\theta_{\rm{steps}}", 
#                                              [100], [1, 2, 4, 16]), "V3Net42Inv2.pdf")   
#     manager.saveFig(investigateDyNetTraining("V3Net42.yaml", r"\theta_{\rm{steps}}", 
#                                              [35,45,55], [16]), "V3Net42Inv3.pdf")    
#     manager.saveFig(investigateDyNetTraining("V3Net42.yaml", r"\theta_{\rm{steps}}", 
#                                              [65,75,85], [16]), "V3Net42Inv4.pdf")     
#     manager.saveFig(investigateDyNetTraining("V3Net42.yaml", r"\theta_{\rm{steps}}", 
#                                              [95,100,110], [16]), "V3Net42Inv5.pdf")         
#     manager.saveFig(investigateDyNetTraining("V3Net1.yaml", r"\theta_{\rm{steps}}", 
#                                              [95,100,110], [16]), "V3Net1Inv1.pdf")
#     manager.saveFig(investigateDyNetTraining("V3Net1.yaml", r"\theta_{\rm{steps}}", 
#                                              [95], [0,1,2,16]), "V3Net1Inv2.pdf")   
#     manager.saveFig(investigateDyNetTraining("V3Net2.yaml", r"\theta_{\rm{steps}}", 
#                                              [95], [0,1,2,16]), "V3Net2Inv1.pdf")    
#     manager.saveFig(investigateDyNetTraining("LPSteps0.yaml", r"\theta_{\rm{steps}}", 
#                                              [95], [0,1,2,16]), "LPSteps0Inv1.pdf")  
#     manager.saveFig(investigateDyNetTraining("V3Net2.yaml", r"\theta_{\rm{steps}}", 
#                                              [25], [0,1,2,16]), "V3Net2Inv2.pdf")    
#     manager.saveFig(investigateDyNetTraining("LPSteps0.yaml", r"\theta_{\rm{steps}}", 
#                                              [25], [0,1,2,16]), "LPSteps0Inv2.pdf")  
#     manager.saveFig(investigateDyNetTraining("V3Net3.yaml", r"\theta_{\rm{steps}}", 
#                                              [38], [1,2,6]), "V3Net3Inv1.pdf")    
#     manager.saveFig(investigateDyNetTraining("LPSteps0.yaml", r"\theta_{\rm{steps}}", 
#                                              [38], [1,2,16]), "LPSteps0Inv3.pdf")  
#     manager.saveFig(investigateDyNetTraining("V3Net4.yaml", r"\theta_{\rm{steps}}", 
#                                              [38], [1,2,6]), "V3Net4Inv1.pdf")    
#     manager.saveFig(investigateDyNetTraining("LPSteps0.yaml", r"\theta_{\rm{steps}}", 
#                                              [38], [1,2,6]), "LPSteps0Inv3.pdf")  
    manager.saveFig(investigateDyNetTraining("V3Net13.yaml", r"\theta_{\rm{steps}}", 
                                             [38], [39]), "V3Net13.pdf")    
#     manager.saveFig(investigateDyNetTraining("LPSteps0.yaml", r"\theta_{\rm{steps}}", 
#                                              [38], [17]), "LPSteps0InvJust17.pdf")  

def main():
#     saveAll()
#     makeDyNetPlots()

#     manager.saveFig(plotTestLowPass("LPs0.yaml", r"\theta_{\rm{LP\;impulses}}"), "LPs0Fig11.pdf")
#     manager.saveFig(plotTestLowPass("LPImp0.yaml", r"\theta_{\rm{LP\;impulse}}"), "LPImp0Fig11.pdf")
#     manager.saveFig(plotTestLowPass("GFs0.yaml", r"\theta_{\rm{GF\;impulses}}"), "GFs0Fig11.pdf")
#     manager.saveFig(investigateDyNetTraining("GFImp0.yaml", r"\theta_{\rm{GF\;impulse}}", [50, 70], [14, 15, 40]), "GFImp0Inv1.pdf") 

    investigationsAndTheirHistory()
#     manager.saveFig(plotTestLowPass("GFImp0.yaml", r"\theta_{\rm{GF\;impulse}}"), "GFImp0Fig1.pdf")
#     manager.saveFig(plotDyNetRCTrainer("DDYsim3.yaml", r"\theta_{\rm{sim3}}"), "DDYsim3Fig.pdf")
#     manager.saveFig(plotDyNetShooting( r"\theta_{\rm{sim3}}","DDYsim3"), "DDYsim3Shoot.pdf")
#     manager.saveFig(plotDyNetRCTrainer("DDYsim4.yaml", r"\theta_{\rm{sim4}}"), "DDYsim4Fig.pdf")
#     manager.saveFig(plotDyNetShooting( r"\theta_{\rm{sim4}}","DDYsim4"), "DDYsim4Shoot.pdf")
#     manager.saveFig(makeDyNetTrainingPlot("DDYsim4.trn"),"DDYsim4Trn.pdf")
#     softGravFricPlots()
#     manager.saveFig(makeDyNetTrainingPlot("DDYsim3.trn"),"DDYsim3Trn.pdf")
#     manager.saveIODat("simrc2.iodat", manager.loadIODat("simff2.iodat")[:,[2,0]], 1, 1)
#     manager.saveIODat("simrc1.iodat", manager.loadIODat("simff1.iodat")[:,[2,0]], 1, 1)
#     makeDyNetValidationPlots()
#     manager.saveFig(makeSimpleResponsePlot("dlsrcsim1.step"),"sim1step.pdf")
#     manager.saveFig(makeSimpleResponsePlot("dlsrcsim1.sine"),"sim1sine.pdf")
#     manager.saveFig(makeSimpleResponsePlot("DDYsim4.step"),"sim4step.pdf")
#     manager.saveFig(makeSimpleResponsePlot("DDYsim4.sine"),"sim4sine.pdf")
#     dlsrcexp2=manager.loadDyNetYaml("dlsrcexp1.yaml")
#     for i in range(0,15):
#         print "dlsrcexp2 epoch ", i, "variance is",calculateVariance(dlsrcexp2,i)
#     gravFricPlots()
#     manager.saveFig(makeSimpleResponsePlot("dlsrcexps0.step"),"exps0step.pdf")
#     manager.saveFig(makeSimpleResponsePlot("dlsrcexps0.sine"),"exps0sine.pdf")
#     manager.saveFig(makeSimpleResponsePlot("dlsrcexpi0.step"), "expi0step.pdf")
#     manager.saveFig(makeSimpleResponsePlot("dlsrcexpi0.sine"), "expi0sine.pdf")
#     manager.saveFig(plotFFTraining("gdff1.gdtrn"), "gdff1.pdf")
#     manager.saveFig(plotFFTraining("gdff2.gdtrn"), "gdff2.pdf")
#     manager.saveFig(plotFFTraining("gdff3.gdtrn"), "gdff3.pdf")
#     manager.saveFig(plotFFTraining("gdffi.gdtrn"), "gdffi.pdf")
#     manager.saveFig(plotFFTraining("gdffs.gdtrn"), "gdffs.pdf")
#     manager.saveFig(getDataAndPlotSpecificDataset(), "dataSet.pdf")
#     manager.saveFig(getDataAndPlotSimDataset(), "simff1.pdf")
#     manager.saveFig(plotDyNetFFTrainer("dlsffsim1.yaml",r"\ddot{\theta}_{\rm{sim}}"), "ymlFig.pdf")
#     manager.saveFig(plotDyNetFFTrainer("dlsffexps0.yaml", r"\ddot{\theta}_{\rm{steps, \sigma = 0.01}}"), "dlsffexps0FigLessFiltered.pdf")
#     manager.saveFig(plotDyNetFFError("dlsffexps0.yaml"), "dlsffexps0Err.pdf")
#     manager.saveFig(plotDyNetFFError("dlsffsim1.yaml"), "dlsffsim1Err.pdf")
    plt.show()
    
    manager.saveFigsAfterPltShow()  
if __name__ == '__main__':
    main()
