/*
 * runHW2.cpp
 *
 *  Created on: Apr 28, 2014
 *      Author: Gray
 */

//{	Training error and validation error, simulated data, feedforward network}
//{	Training error and validation error, experimental data, feedforward network}
//{	Training error and validation error, simulated data, recurrant network}
//{	Training error and validation error, experimental data, recurrant network}
#include <DataSet.hpp>
#include <DyNetTrainer.hpp>
#include <GradientDescentTrainer.hpp>
#include <NetworkStateVector.hpp>
#include <PendulumDemoRandomDataGenerator.hpp>
#include <PendulumNet.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <sstream>

#define PI 3.14159265395

using namespace std;
using namespace uta::networkUtils;
using namespace uta::networkUtils::pendulumDemo;
namespace uta
{
namespace networkUtils
{
namespace AppIntHW2
{
double calcDyNetShootingError(Trainable& network, DataSet& validationSet, double stdDevMeasurement, VectorXd state0);
double calcDyNetShootingErrorAndPushToFile(Trainable& network, DataSet& validationSet, double stdDevMeasurement,
		VectorXd state0, string fileName);
void recordFunction(Trainable& network, string fileName, MatrixXd inputs, VectorXd startingStates);
VectorXd generateSineInputs(unsigned size, double freq, double dt, double amplitude);
VectorXd generateStepInputs(unsigned size, double freq, double dt, double amplitude);
void trainFFNetwork(DataSet& trainingSet, DataSet& validationSet, string fileName)
{
	NetworkStateVector state;
	double dt = 0.01, aGuess = 0.4, bGuess = 0.2, cGuess = 0.3, dGuess = -2.0;
	aGuess = 45.6159 * 1e1;
	bGuess = 0.728237 * 1e1;
	cGuess = -0.0421311 * 1e1;
	dGuess = 400.0 * 1e1;
//	a= 41.7347
//	b= -0.456419
//	c= -0.981899
//	d= -2.32863
//	a= 40.2696
//	b= -1.85958
//	c= -2.8132
//	d= -6.71319
	Network* pendulumNet = new AlphaPendulumNet(state, dt, aGuess, bGuess, cGuess, dGuess);
	GradientDescentTrainer trainer(fileName);
	trainer.setEpochMax(400);
	trainer.setTimeLimit(0.5);
	vector<double> initialMemory;
	trainer.setInitialMemory(initialMemory);
	cout << "network input size is " << pendulumNet->inputSize() << endl;

	trainer.trainNetwork(trainingSet, validationSet, *pendulumNet, 0.001);
	cout << "-------trainFFNetwork done  --" << endl;
	cout << "-------final weights  --" << endl;
	cout << "a= " << pendulumNet->getWeight(0) << endl;
	cout << "b= " << pendulumNet->getWeight(1) << endl;
	cout << "c= " << pendulumNet->getWeight(2) << endl;
	cout << "d= " << pendulumNet->getWeight(3) << endl;

}
void dampedLStrainFFNetwork(DataSet& trainingSet, DataSet& validationSet, string fileBase, unsigned number)
{
	NetworkStateVector state;
	double dt = 0.01, aGuess = 0.4, bGuess = 0.2, cGuess = 0.3, dGuess = -2.0;
	aGuess = 63;
	bGuess = 1.2;
	cGuess = -0.4;
	dGuess = 313;
//	a= 41.7347
//	b= -0.456419
//	c= -0.981899
//	d= -2.32863
//	a= 40.2696
//	b= -1.85958
//	c= -2.8132
//	d= -6.71319
	Network* pendulumNet = new AlphaPendulumNetV2(state, dt, aGuess, bGuess, cGuess, dGuess, 1.0 / 667);
	DyNetTrainerParameters params;
	params.trustRegionScaleParameter = 1.0e3;
	params.linearitySetPoint = 0.75;
	params.linearityMinimumError = 0.25;
	params.trustRegionPenalty = 0.2;
	params.trustRegionPositiveBump = 1.3;
	DyNetTrainer trainer(params, trainingSet, *pendulumNet);
	VectorXd stdDevParams(4), stdDevmeasurement(1), updateNoise(0), states(0);
	stdDevParams << 63, 1.2, 0.4, 300;
	stdDevParams *= 0.1;
	stdDevmeasurement << 0.10;
	trainer.setParameterUncertainty(stdDevParams);
	trainer.setStateUpdateNoise(updateNoise);
	trainer.setStateUncertainty(updateNoise);
	trainer.setMeasurementNoise(stdDevmeasurement);
	trainer.setStates(states);
	cout << "network input size is " << pendulumNet->inputSize() << endl;

	for (int i = 0; i < 5; i++)
		trainer.trainOneEpoch();
	trainer.log(fileBase, number);

	cout << "-------trainFFNetwork done  --" << endl;
	cout << "-------final weights  --" << endl;
	cout << "a= " << pendulumNet->getWeight(0) << endl;
	cout << "b= " << pendulumNet->getWeight(1) << endl;
	cout << "c= " << pendulumNet->getWeight(2) << endl;
	cout << "d= " << pendulumNet->getWeight(3) << endl;

}
void dampedLStrainRCNetwork(DataSet& trainingSet, DataSet& validationSet, double validationThetaZero,
		double validationOmegaZero, string fileBase, double thetaZero, double omegaZero, unsigned number,
		string trainingDataFileName, string stepFileName, string sineFileName, int epochs, double geometricDerelaxation,
		double softness, double startingRelaxation)
{
	NetworkStateVector state;
	double dt = 0.01, aGuess = 0.4, bGuess = 0.2, cGuess = 0.3, dGuess = -2.0;
//	a= 65.5293
//	b= 0.0886516
//	c= 0.0404539
//	d= 516.172
//	aGuess = 67.4944;
//	bGuess = 1.35948;
//	cGuess =-0.360333;
//	dGuess = 496.412;
	aGuess = 45.6159 * (1 + 1e-1);
	bGuess = 0.728237 * (1 - 1e-1);
	cGuess = 0.0421311 * (1 - 1e-1);
	dGuess = 400.0 * (1 - 1e-1);
	aGuess = 45;
	bGuess = 4.0;
	cGuess = 0.3;
	dGuess = 200.0;
//	a= 41.7347
//	b= -0.456419
//	c= -0.981899
//	d= -2.32863
//	a= 40.2696
//	b= -1.85958
//	c= -2.8132
//	d= -6.71319
	Network* pendulumNet = new FullPendulumNetV2(state, dt, aGuess, bGuess, cGuess, dGuess, softness);
	DyNetTrainerParameters params;
	params.trustRegionScaleParameter = 1.0e3;
	params.linearitySetPoint = 0.75;
	params.linearityMinimumError = 0.25;
	params.trustRegionPenalty = 0.2;
	params.trustRegionPositiveBump = 1.3;
	DyNetTrainer trainer(params, trainingSet, *pendulumNet);
	VectorXd stdDevParams(4), stdDevmeasurement(1), stdUpdateNoise(2), stdStateUncertainty(2);
	stdDevParams << 4.5, .07, .004, 40; // shape of damping on parameters
	stdDevmeasurement << 0.05; // weight on prediction error
	stdStateUncertainty << 100.0, 1000.0; // shape of damping on states
	stdUpdateNoise << 0.0001, 10.000 * startingRelaxation; // weight on update error
	cout << "network memory size is " << trainer.viewSizes().memorySize << endl;
	cout << "dataset input size is " << trainingSet.viewInputSize() << endl;
	trainer.setParameterUncertainty(stdDevParams);
	trainer.setStateUpdateNoise(stdUpdateNoise);
	trainer.setStateUncertainty(stdStateUncertainty);
	trainer.setMeasurementNoise(stdDevmeasurement);
	VectorXd IC(2);
	IC << thetaZero, omegaZero;
	VectorXd validationState0(2);
	validationState0 << validationThetaZero, validationOmegaZero;
	VectorXd states = initializeStates(*pendulumNet, trainingSet, IC);
	trainer.setStates(states);
	cout << "network input size is " << pendulumNet->inputSize() << endl;

	DataSet trainingErrorDataSet(1, 3); // epoch: dyNetTrainingError, shootingtrainingError, shootingvalidiationError
	double trainingDSIns[1];
	double trainingDSOuts[3];

	for (int i = 0; i < epochs; i++)
	{
		trainer.trainOneEpoch();
		stringstream trainingName, validationName;
		trainingName << fileBase << number << "Ep" << i << "Tr.iodat";
		double shootingError = calcDyNetShootingErrorAndPushToFile(*pendulumNet, trainingSet,
				(double) stdDevmeasurement(0), IC, trainingName.str());
		validationName << fileBase << number << "Ep" << i << "Val.iodat";
		double validation = calcDyNetShootingErrorAndPushToFile(*pendulumNet, validationSet,
				(double) stdDevmeasurement(0), validationState0, validationName.str());
		trainingDSIns[0] = i;
		trainingDSOuts[0] = trainer.getEpochError();
		trainingDSOuts[1] = shootingError;
		trainingDSOuts[2] = validation;
		trainingErrorDataSet.addLine(trainingDSIns, trainingDSOuts);
		stdUpdateNoise *= 1.0; //geometricDerelaxation;
		stdDevmeasurement *= 1.0 / geometricDerelaxation; // bias towards later editions
		trainer.setStateUpdateNoise(stdUpdateNoise);
		trainer.setMeasurementNoise(stdDevmeasurement);
	}
	trainingErrorDataSet.pushToFile(trainingDataFileName);
	trainer.log(fileBase, number);
	vector<double> bestWeights(trainer.getElitePoint().weights);
	pendulumNet->setWeights(bestWeights);

	cout << "-------trainFFNetwork done  --" << endl;
	cout << "-------final weights  --" << endl;
	cout << "a= " << pendulumNet->getWeight(0) << endl;
	cout << "b= " << pendulumNet->getWeight(1) << endl;
	cout << "c= " << pendulumNet->getWeight(2) << endl;
	cout << "d= " << pendulumNet->getWeight(3) << endl;

	//calculate and save step response of resulting model
	VectorXd zeroIC(2);
	zeroIC << 0.0, 0.0;
	VectorXd step = generateStepInputs(500, 0.033, 0.01, 0.01);
	recordFunction(*pendulumNet, stepFileName, step, zeroIC);
	VectorXd sine = generateSineInputs(1000, 0.66, 0.01, 0.01);
	recordFunction(*pendulumNet, sineFileName, sine, zeroIC);
	//calculate and save sine response of resulting model

}
void testLowPassNetwork(DataSet& trainingSet, string fileBase, unsigned number, string trainingDataFileName, int epochs)
{
	NetworkStateVector state;
	double dt = 0.01;
	Network* pendulumNet = new NetworkLowPass(state, dt);
	DyNetTrainerParameters params;
	params.trustRegionScaleParameter = 1.0e3;
	params.linearitySetPoint = 0.75;
	params.linearityMinimumError = 0.25;
	params.trustRegionPenalty = 0.2;
	params.trustRegionPositiveBump = 1.3;
	DyNetTrainer trainer(params, trainingSet, *pendulumNet);
	VectorXd stdDevParams(0), stdDevmeasurement(1), stdUpdateNoise(3), stdStateUncertainty(3);
//	stdDevParams << ; // shape of damping on parameters
	stdDevmeasurement << 0.05; // weight on prediction error
	stdStateUncertainty << 1000.0, 1000.0, 10000.0; // shape of damping on states
	stdUpdateNoise << 0.001, 0.001, 50.0; // weight on update error
	// meas 0.05, thetaEq 0.001, omegaEq 0.001, alphaEq 50.0 -> approx std gaussian convolution 38.89ms with a slight delay
	// meas 0.05, thetaEq 0.001, omegaEq 0.001, alphaEq 100.0 -> approx std gaussian convolution 27.5ms with a slight delay
	// meas 0.05, thetaEq 0.001, omegaEq 0.001, alphaEq 200.0 -> approx std gaussian convolution 19.5ms with a slight delay

	trainer.setParameterUncertainty(stdDevParams);
	trainer.setStateUpdateNoise(stdUpdateNoise);
	trainer.setStateUncertainty(stdStateUncertainty);
	trainer.setMeasurementNoise(stdDevmeasurement);
	VectorXd IC(3);
	IC << 0.0, 0.0, 0.0;
	VectorXd states = initializeStates(*pendulumNet, trainingSet, IC); //all zero, btw
	trainer.setStates(states);
	cout << "network input size is " << pendulumNet->inputSize() << endl;

	for (int i = 0; i < epochs; i++)
	{
		trainer.trainOneEpoch();
	}
	trainer.log(fileBase, number);
}
void testGaussianFilterNetwork(DataSet& trainingSet, string fileBase, unsigned number, string trainingDataFileName,
		int epochs)
{
	NetworkStateVector state;
	double dt = 0.01;
	Network* pendulumNet = new NetworkGaussianFilter(state, dt);
	DyNetTrainerParameters params;
	params.trustRegionScaleParameter = 1.0e3;
	params.linearitySetPoint = 0.75;
	params.linearityMinimumError = 0.25;
	params.trustRegionPenalty = 0.2;
	params.trustRegionPositiveBump = 1.3;
	DyNetTrainer trainer(params, trainingSet, *pendulumNet);
	VectorXd stdDevParams(0), stdDevmeasurement(1), stdUpdateNoise(4), stdStateUncertainty(4);
//	stdDevParams << ; // shape of damping on parameters
	stdDevmeasurement << 0.05; // weight on prediction error
	stdStateUncertainty << 1000.0, 1000.0, 10000.0, 10000.0; // shape of damping on states
	stdUpdateNoise << 0.001, 0.001, 0.001, 500.0; // weight on update error
	// meas 0.05, thetaEq 0.001, omegaEq 0.001, alphaEq 50.0 -> approx std gaussian convolution 38.89ms with a slight delay
	// meas 0.05, thetaEq 0.001, omegaEq 0.001, alphaEq 100.0 -> approx std gaussian convolution 27.5ms with a slight delay
	// meas 0.05, thetaEq 0.001, omegaEq 0.001, alphaEq 200.0 -> approx std gaussian convolution 19.5ms with a slight delay

	trainer.setParameterUncertainty(stdDevParams);
	trainer.setStateUpdateNoise(stdUpdateNoise);
	trainer.setStateUncertainty(stdStateUncertainty);
	trainer.setMeasurementNoise(stdDevmeasurement);
	VectorXd IC(4);
	IC << 0.0, 0.0, 0.0, 0.0;
	VectorXd states = initializeStates(*pendulumNet, trainingSet, IC); //all zero, btw
	trainer.setStates(states);
	cout << "network input size is " << pendulumNet->inputSize() << endl;

	for (int i = 0; i < epochs; i++)
	{
		trainer.trainOneEpoch();
	}
	trainer.log(fileBase, number);
}
VectorXd generateRandomInputs(unsigned size)
{
	VectorXd randomInputs(size);
	for (unsigned i = 0; i < size; i++)
	{
		//TODO: implement a random input
	}
	return randomInputs;
}

VectorXd generateSineInputs(unsigned size, double freq, double dt, double amplitude)
{
	VectorXd sineInput(size);
	double phase = 0;
	for (unsigned i = 0; i < size; i++)
	{
		phase += 2 * 3.14159 * freq * dt;
		sineInput(i) = sin(phase) * amplitude;
	}
	return sineInput;
}
void recordFunction(Trainable& network, string fileName, MatrixXd inputs, VectorXd startingStates)
{
	VectorXd state(startingStates);
	double prediction;
	DataSet dataToWrite(1, 1);
	double dsI[1], dsO[1];
	for (int i = 0; i < inputs.rows(); i++)
	{
		network.setInput(0, (double) state(0));
		network.setInput(1, (double) state(1));
		network.setInput(2, (double) inputs(i, 0));
		dsI[0] = inputs(i, 0);
		network.calculateOutputs();
		state(0) = network.getOutput(0);
		state(1) = network.getOutput(1);
		dsO[0] = network.getOutput(2);
		dataToWrite.addLine(dsI, dsO);
	}
	dataToWrite.pushToFile(fileName);
}
VectorXd generateStepInputs(unsigned size, double freq, double dt, double amplitude)
{
	VectorXd sineInput(size);
	double phase = 0;
	for (unsigned i = 0; i < size; i++)
	{
		phase += freq * dt;
		if (phase >= 1)
			phase -= 1.0;
		sineInput(i) = ((phase < 0.5) ? amplitude : 0);
	}
	return sineInput;
}
void generateDataToFile(Trainable& network, VectorXd inputs, VectorXd state0, double dt, string fileName)
{
	// Assert network has three inputs, one output. The output is the acceleration of the first input,
	// and the second input is velocity.
	// Assert state0 is size 2
	VectorXd state(state0);
	DataSet dataSet(3, 1);
	double dataIns[3];
	double dataOuts[1];
	for (unsigned i = 0; i < inputs.rows(); i++)
	{
		dataIns[0] = (double) state(0);
		dataIns[1] = (double) state(1);
		dataIns[2] = (double) inputs(i);
		network.setInput(0, dataIns[0]);
		network.setInput(1, dataIns[1]);
		network.setInput(2, dataIns[2]);
		network.calculateOutputs();
		dataOuts[0] = network.getOutput(0);
		state(0) += dt * state(1) + 0.5 * dt * dt * dataOuts[0];
		state(1) += dt * dataOuts[0];
		dataSet.addLine(dataIns, dataOuts);
	}
	dataSet.pushToFile(fileName);
}
double calcDyNetShootingError(Trainable& network, DataSet& validationSet, double stdDevMeasurement, VectorXd state0)
{
	// Assert network has three inputs, one output. The output is the acceleration of the first input,
	// and the second input is velocity.
	// Assert state0 is size 2
	VectorXd state(state0);
	double error = 0;

	for (unsigned i = 0; i < validationSet.viewSize(); i++)
	{
		network.setInput(0, (double) state(0));
		network.setInput(1, (double) state(1));
		network.setInput(2, (double) validationSet.getInput(i, 0));
		network.calculateOutputs();
		state(0) = network.getOutput(0);
		state(1) = network.getOutput(1);
		double err = (validationSet.getOutput(i, 0) - network.getOutput(2)) / stdDevMeasurement;
		error += err * err;
	}
	return error;
}
double calcDyNetShootingErrorAndPushToFile(Trainable& network, DataSet& validationSet, double stdDevMeasurement,
		VectorXd state0, string fileName)
{
	// Assert network has three inputs, one output. The output is the acceleration of the first input,
	// and the second input is velocity.
	// Assert state0 is size 2
	VectorXd state(state0);
	double error = 0;
	DataSet dataSet(3, 2);
	double dataIns[3];
	double dataOuts[2];
	for (unsigned i = 0; i < validationSet.viewSize(); i++)
	{
		network.setInput(0, (double) state(0));
		network.setInput(1, (double) state(1));
		network.setInput(2, (double) validationSet.getInput(i, 0));
		network.calculateOutputs();
		state(0) = network.getOutput(0);
		state(1) = network.getOutput(1);

		dataOuts[0] = validationSet.getOutput(i, 0);
		dataOuts[1] = network.getOutput(2);
		double err = (validationSet.getOutput(i, 0) - network.getOutput(2)) / stdDevMeasurement;
		error += err * err;
		dataSet.addLine(dataIns, dataOuts);
	}
	dataSet.pushToFile(fileName);
	return error;
}

void generateFeedForwardSimDataSet1()
{
	double a = 42.4138;
	double b = 0.86046;
	double c = 0.0472982;
	double d = 418.495;
	PendulumABCDDataGenerator dataGenerator(a, b, c, d);
	dataGenerator.generateRandomData0mem("simff1.iodat", 1000, 1.4, -0.1, 0.01, 0.1, 0.01, 10, 40, 0.01, 5);
	cout << "generated simff1.iodat" << endl;
}
void generateFeedForwardSimDataSet2()
{
	double a = 45.6159;
	double b = 0.728237;
	double c = 0.0421311;
	double d = 418.495;
	NetworkStateVector netState;
	Network* pendulumNetwork = new AlphaPendulumNetV2(netState, 0.01, a, b, c, d, 1e-6);
	DataSet expffs("expffs.iodat");
	cout << "expffs sizes: i,o,dat = " << expffs.viewInputSize() << ", " << expffs.viewOutputSize() << ", "
			<< expffs.viewSize() << endl;
	VectorXd inputs(expffs.viewSize());
	for (unsigned i = 0; i < expffs.viewSize(); i++)
		inputs(i) = expffs.getInput(i, 2);
	VectorXd state0(2);
	state0 << expffs.getInput(0, 0), expffs.getInput(0, 1);
	generateDataToFile(*pendulumNetwork, inputs, state0, 0.01, "simff2.iodat");
	cout << "generated simff2.iodat" << endl;
}
void generateFeedForwardSimDataSet3(double stdDevParams, double thetaMeasureNoise, double alphaMeasureNoise,
		double accelerationNoise)
{
	default_random_engine gen(1337);
	normal_distribution<double> baseGaussian(0.0, 1.0);

	double a = 50.6159 * (1 + baseGaussian(gen) * stdDevParams);
	double b = 1.728237 * (1 + baseGaussian(gen) * stdDevParams);
	double c = 1.421311 * (1 + baseGaussian(gen) * stdDevParams);
	double d = 418.495 * (1 + baseGaussian(gen) * stdDevParams);
	cout << "trainingSet3 uses a,b,c,d: " << a << " " << b << " " << c << " " << d << endl;

	NetworkStateVector netState;
	Network* pendulumNetwork = new AlphaPendulumNetV2(netState, 0.01, a, b, c, d, 1e-6);
	DataSet expffs("expffs.iodat");
	cout << "expffs sizes: i,o,dat = " << expffs.viewInputSize() << ", " << expffs.viewOutputSize() << ", "
			<< expffs.viewSize() << endl;
	VectorXd inputs(expffs.viewSize());
	for (unsigned i = 0; i < expffs.viewSize(); i++)
		inputs(i) = expffs.getInput(i, 2);
	VectorXd state0(2);
	state0 << 0.6, -0.1;
	VectorXd state(state0);
	DataSet ffDat(3, 1);
	DataSet rcDat(1, 1);
	Network& network = *pendulumNetwork;
	double dt = 0.01;
	double ffIns[3], rcIns[1];
	double ffOuts[1], rcOuts[1];
	for (unsigned i = 0; i < inputs.rows() / 2; i++)
	{
		ffIns[0] = (double) state(0);
		ffIns[1] = (double) state(1);
		ffIns[2] = (double) inputs(i) / 10.0;
		rcIns[0] = ffIns[2];
		network.setInput(0, ffIns[0]);
		network.setInput(1, ffIns[1]);
		network.setInput(2, ffIns[2]);
		network.calculateOutputs();
		double alpha = network.getOutput(0) + accelerationNoise * baseGaussian(gen);
		state(0) += dt * state(1) + 0.5 * dt * dt * alpha;
		state(1) += dt * alpha;
		ffOuts[0] = alpha + baseGaussian(gen) * alphaMeasureNoise;
		rcOuts[0] = state(0) + baseGaussian(gen) * thetaMeasureNoise;
		ffDat.addLine(ffIns, ffOuts);
		rcDat.addLine(rcIns, rcOuts);
	}
	ffDat.pushToFile("simff3.iodat");
	rcDat.pushToFile("simrc3.iodat");
	cout << "generated simff3.iodat" << endl;
}
void generateImpulseDataSet()
{
	DataSet imp(1, 1);
	double in[1];
	double out[1];
	for (unsigned i = 0; i < 500; i++)
	{
		in[0] = 0;
		out[0] = 0;
		if (i == 250)
			out[0] = 1.0;
		imp.addLine(in, out);
	}
	imp.pushToFile("imp.iodat");
	cout << "generated imp.iodat" << endl;
}

void trainV3Net(DataSet& trainingSet, string fileBase, unsigned number, string trainingDataFileName)
{
	NetworkStateVector state;
//	[a= 60.4392 c= 0.86337 d= 292.529]
//  [a= 62.4013 c= 0.867281 d= 328.184] // produces even lower error
//	double dt = 0.01, aGuess = 0, bGuess = 0, cGuess = 1, dGuess = 100, softness = 1e2;
//	[a= 62.2873 b= -0.103776 c= 0.881519 d= 324.926 e= 100.309]
	double dt = 0.01, aGuess = 60, bGuess = 0, cGuess = 0.8, dGuess = 300, softness = 0;
	Network* pendulumNet = new FullPendulumNetV3(state, dt, aGuess, bGuess, cGuess, dGuess, softness);
//	Network* pendulumNet = new NetworkLowPass(state,dt);
	DyNetTrainerParameters params;
	params.trustRegionScaleParameter = 1.0e4;
	params.linearitySetPoint = 0.75;
	params.linearityMinimumError = 0.25;
	params.trustRegionPenalty = 0.2;
	params.trustRegionPositiveBump = 1.3;
//	double geometricMeasurementStdModifier = pow(finalMeasurementStd / initialMeasurementStd, 1.0 / epochs);
//	double geometricAccelerationStdModifier = pow(finalAccelerationStd / initialAccelerationStd, 1.0 / epochs);
	DyNetTrainer trainer(params, trainingSet, *pendulumNet);
	DyNetTrainerParameters validatorParams(params);
	validatorParams.trustRegionScaleParameter = 1e6;
	DyNetValidator validator(validatorParams, trainingSet, *pendulumNet);
	VectorXd stdDevParams(5), stdDevmeasurement(1), stdUpdateNoise(3), stdStateUncertainty(3);
	stdDevParams << 0.1, 0.01, .1, 0.01, 0.1; // shape of damping on parameters
	stdStateUncertainty << 1000.0, 1.0, 10000.0; // shape of damping on states
//	stdDevmeasurement << initialMeasurementStd; // weight on prediction error
	// To validate this filter network we should calculate the error of the gaussian solution on the validation
	// data, and then fix the weights of this filter, or run a kalman smoother (good option), or use the weights of the
	// filter solution as the first step of the filter and then run one training epoch (or go until convergence) with the weights fixed and see if
	// we improve over the filter solution (we should) the amount of improvement should increase over time as the weights
	// get better with time.

	stdUpdateNoise << 0.001, 0.001, 50.0; // weight on update error
	cout << "network memory size is " << trainer.viewSizes().memorySize << endl;
	cout << "dataset input size is " << trainingSet.viewInputSize() << endl;
	trainer.setParameterUncertainty(stdDevParams);
	trainer.setStateUpdateNoise(stdUpdateNoise);
	validator.setStateUpdateNoise(stdUpdateNoise);
	trainer.setStateUncertainty(stdStateUncertainty);
	validator.setStateUncertainty(stdStateUncertainty);
	VectorXd IC(3);
	IC << 0.0, 0.0, 0.0;
//	VectorXd validationState0(2);
//	validationState0 << validationThetaZero, validationOmegaZero;
	VectorXd states = initializeStatesZero(trainingSet, IC);
	trainer.setStates(states);
	validator.setStates(states);
	cout << "network input size is " << pendulumNet->inputSize() << endl;

	DataSet trainingErrorDataSet(1, 3); // epoch: dyNetTrainingError, shootingtrainingError, shootingvalidiationError
	double trainingDSIns[1];
	double trainingDSOuts[3];
	double previousError = 99e99;
	int i;
	double start = 0.00125; // best so far is 0.00125
	double maxGeom = 35;
	double final = 5.0;
	double geometricModifier = pow(final / start, 1.0 / maxGeom);
	stdDevmeasurement << 0.05;
	validator.setMeasurementNoise(stdDevmeasurement);
	for (int i=0;i<10;i++)
		validator.trainOneEpoch();
	VectorXd lowPassedValidationStates(validator.getElitePoint().dyNetStates);
	stdDevmeasurement << final;
	validator.setMeasurementNoise(stdDevmeasurement);
	double previousValError = validator.getElitePoint().error;
	for (i = 0; i < 40; i++)
	{
		stdDevmeasurement << start * pow(geometricModifier, i); // weight on prediction error (don't change these if we want to validate against past)
		// changed from 0.05; larger values equate to a more long term goal, hopefully I don't have to implement
		// changing this over time, but I could.
		if (i > maxGeom)
			stdDevmeasurement << final;
		// ramp up. (comparable with last time's best estimate of 10.9926  [a= 52.7979 c= 0.869031 	d= 121.579 ])
		// I got: 9.46081 [a= 55.2327 c= 0.864427 d= 165.693]
		// Using 35 GeomMax
		// starting from 0.0005  : 9.1615  [a= 55.7388 c= 0.865532 d= 176.066]
		// starting from 0.001   : 8.13744 [a= 57.4449 c= 0.866353 d= 220.854]
		// starting from 0.0012  : 7.56769 [a= 59.0848 c= 0.864874 d= 261.125]
		// starting from 0.001225: 7.37305 [a= 60.1589 c= 0.863684 d= 284.879]
		// starting from 0.00125 : 7.26156 [a= 61.4564 c= 0.864456 d= 311.656]
		// starting from 0.001275: 7.2622  [a= 61.4416 c= 0.864414 d= 311.382]
		// starting from 0.0013  : 7.26284 [a= 61.427  c= 0.864374 d= 311.113]
		// starting from 0.0014  : 7.26538 [a= 61.3714 c= 0.864229 d= 310.086]
		// starting from 0.0015  : 7.26787 [a= 61.3197 c= 0.864104 d= 309.129]
		// starting from 0.002   : 7.27964 [a= 61.1052 c= 0.863692 d= 305.142]
		// starting from 0.0025  : 7.2903  [a= 60.9402 c= 0.863486 d= 302.053]
		// starting from 0.005   : 7.33192 [a= 60.4392 c= 0.86337  d= 292.529]
		// starting from 0.01    : 7.386   [a= 59.9636 c= 0.863815 d= 283.22 ]
		// starting from 0.04    : 7.52573 [a= 59.1168 c= 0.865316 d= 265.768]
		// starting from 0.05    : 7.55135 [a= 58.9944 c= 0.865558 d= 263.14 ]
		// starting from 0.06    : 7.57278 [a= 58.8973 c= 0.865749 d= 261.034]

		// 39 geometric max      : 7.57642 [a= 64.2956 c= 0.976831 d= 381.803]
		// 38 geometric max      : 7.57642 [a= 63.8474 c= 0.903036 d= 363.947]
		// 37 geometric max      : 7.25895 [a= 63.1152 c= 0.888717 d= 345.505]
		// 36 geometric max      : 7.24187 [a= 62.2878 c= 0.870523 d= 327.968]
		// 35 geometric max      : 7.37305 [a= 60.1589 c= 0.863684 d= 284.879]
		// 33 geometric max      : 9.57796 [a= 55.2107 c= 0.863775 d= 161.755]

		// hard to tell what is luck and what is not, but there is strong evidence that starting with too low a time
		// constant gives you crappy data. There is also strong evidence that the ending bit is important.

		// using 0.00125 (37) and starting with good guesses: 7.28064 [a= 63.5716 c= 0.892697 d= 354.657]
		// using 0.00125 (35) and starting with good guesses: 7.24544 [a= 61.9872 c= 0.866441 d= 320.842]
		// using 0.00125 (20) and starting with good guesses: 7.31994 [a= 60.7523 c= 0.861175 d= 294.461]
		// using 0.00125 (10) and starting with good guesses: 7.3169 [a= 60.7745 c= 0.861129 d= 295.114]
		// using 0.00125 (2) and starting with good guesses: 7.27664[a= 61.235 c= 0.861157 d= 305.688]
		// using 0.00125 (1) and starting with good guesses: 7.27673[a= 61.2334 c= 0.861155 d= 305.659]
		// using 0.00125 (0) and starting with good guesses: 7.27673[a= 61.2334 c= 0.861155 d= 305.659]
		// using 0.05 (35) and starting with good guesses: 7.24148 [a= 62.7006 c= 0.86969 d= 333.706]
		// using 0.05 (30) and starting with good guesses: 7.24905 [a= 61.8742 c= 0.86265 d= 317.768]
		// using 0.05 (20) and starting with good guesses: 7.27313 [a= 61.2972 c= 0.861225 d= 306.84]
		// using 0.5 (35?) and starting with good guesses: 7.28064 [a= 63.5716 c= 0.892697 d= 354.657]
		// using 0.1 (35) and starting with good guesses: 7.24064 [a= 62.4013 c= 0.867281 d= 328.184]

		// using 0.1 (35) and starting with double good guesses: 7.25428 [a= 63.328 c= 0.875272 d= 344.995]
		// but going an extra 20 epochs : 7.24035 [a= 62.5317 c= 0.865855 d= 329.59]
		// maxes out (with double good start) at 83 epoch, 7.24035 [a= 62.5289 c= 0.865845 d= 329.557]

		// With a crap apriori:
		// Training 143 epochs at 0.1 (100) : 7.24035 [a= 62.5289 c= 0.865845 d= 329.557]
		// Training 101 epochs at 0.001 (50) : 7.24035 [a= 62.5289 c= 0.865845 d= 329.557]
		// Training 95 epochs at 0.01 (50) : 7.24035 [a= 62.5289 c= 0.865845 d= 329.557]
		// Training 94 epochs at 0.001 (35) : 7.24035 [a= 62.5289 c= 0.865845 d= 329.557]
		// Training 84 epochs at 0.00125 (35) : 7.24035 [a= 62.5289 c= 0.865845 d= 329.557]
		// putting back in friction, it didn't help !
		// Training 200 epochs 0.00125 (35) (not stable) : 7.24133 [a= 62.2873 b= -0.103776 c= 0.881519 d= 324.926 e= 100.309]
		// Training 200 epochs 0.00125 (100)(-->50 not 5) (not stable at all) : 0.30464 (50 units) [a= 67.5131 b= -2.13416 c= 0.868772 d= 314.447 e= 1781.27]
		// Tried stiffening the damping parameter on velocity (in hopes of stabilizing solver more) and I think that might have helped,
		// Training 200 epochs 0.00125 (100)(-->50 not 5) (not stable at all) : 2.39557 (50 units) [a= 58.6529 b= 3.47219 c= 1.18943 d= 326.003 e= -1468.78]
		// But then is is a really crappy dataset for identifying friction. Especially with those
		// goofy zero velocity crossing jumps
		// so the high initial training rate is important even if you have good parameters to start
		//TODO: rock this house

		trainer.setMeasurementNoise(stdDevmeasurement);

		trainer.trainOneEpoch();
		vector<double> bestWeightsThusFar(trainer.getElitePoint().weights);
		pendulumNet->setWeights(bestWeightsThusFar);
		bool validationHasNotConverged = true;
		int valEpoch = 0;
		validator.setStates(lowPassedValidationStates);
		while (validationHasNotConverged)
		{
			// TODO: solve this problem with the validator: the validator does not converge to the
			// lowest error solution the way the primary does. Consider midpoint start shooting method validation.
			// Implement a slow validator which uses the same geometric progression idea. It seems like filters
			// are not easy to make converge if you start with a low frequency high model reliance goal, even if
			// you start with a nearly ideal model.
			validator.trainOneEpoch();
			if (abs(validator.getEpochError() - previousValError) < 1e-5)
				validationHasNotConverged = false;
			previousValError = validator.getEpochError();
			valEpoch++;
			if (valEpoch > 10)
				validationHasNotConverged = false;
		}
		stringstream trainingName, validationName;
		trainingName << fileBase << number << "Ep" << i << "Tr.iodat";
//		double shootingError = calcDyNetShootingErrorAndPushToFile(*pendulumNet, trainingSet,
//				(double) stdDevmeasurement(0), IC, trainingName.str());
//		validationName << fileBase << number << "Ep" << i << "Val.iodat";
//		double validation = calcDyNetShootingErrorAndPushToFile(*pendulumNet, validationSet,
//				(double) stdDevmeasurement(0), validationState0, validationName.str());
		trainingDSIns[0] = i;
		trainingDSOuts[0] = trainer.getEpochError();
		trainingDSOuts[1] = validator.getEpochError();
//		trainingDSOuts[2] = validation;
		trainingErrorDataSet.addLine(trainingDSIns, trainingDSOuts);
		if (abs(trainer.getEpochError() - previousError) < 1e-14)
			break;
		previousError = trainer.getEpochError();
//		stdUpdateNoise[2] *= geometricAccelerationStdModifier;
//		stdDevmeasurement *= geometricMeasurementStdModifier; // bias towards later editions
//		trainer.setStateUpdateNoise(stdUpdateNoise);
//		trainer.setMeasurementNoise(stdDevmeasurement);
	}
	cout << "trained for " << i << " epochs" << endl;
	trainingErrorDataSet.pushToFile(trainingDataFileName);
	trainer.log(fileBase, number);
	vector<double> bestWeights(trainer.getElitePoint().weights);
	pendulumNet->setWeights(bestWeights);

	cout << "-------trainV3Network done  --" << endl;
	cout << "-------final weights with error --" << trainer.getElitePoint().error << endl;
	cout << "a= " << pendulumNet->getWeight(0) << endl;
	cout << "b= " << pendulumNet->getWeight(1) << endl;
	cout << "c= " << pendulumNet->getWeight(2) << endl;
	cout << "d= " << pendulumNet->getWeight(3) << endl;
	cout << "e= " << pendulumNet->getWeight(4) << endl;

	//calculate and save step response of resulting model
//	VectorXd zeroIC(2);
//	zeroIC << 0.0, 0.0;
//	VectorXd step = generateStepInputs(500, 0.033, 0.01, 0.01);
//	recordFunction(*pendulumNet, stepFileName, step, zeroIC);
//	VectorXd sine = generateSineInputs(1000, 0.66, 0.01, 0.01);
//	recordFunction(*pendulumNet, sineFileName, sine, zeroIC);
	//calculate and save sine response of resulting model

}

void backPropTrain(DataSet& trainingSet, DataSet& validationSet, double validationThetaZero, double validationOmegaZero,
		string fileBase, double thetaZero, double omegaZero, unsigned number, string trainingDataFileName,
		string stepFileName, string sineFileName, int epochs, double softness, double finalAccelerationStd,
		double initialAccelerationStd, double initialMeasurementStd, double finalMeasurementStd)
{
	NetworkStateVector state;
	double dt = 0.01, aGuess = 44.53, bGuess = 2.47, cGuess = 15.8202, dGuess = 200.7;
	Network* pendulumNet = new FullPendulumNetV3(state, dt, aGuess, bGuess, cGuess, dGuess, softness);
	DyNetTrainerParameters params;
	params.trustRegionScaleParameter = 1.0e3;
	params.linearitySetPoint = 0.75;
	params.linearityMinimumError = 0.25;
	params.trustRegionPenalty = 0.2;
	params.trustRegionPositiveBump = 1.3;
//	double geometricMeasurementStdModifier = pow(finalMeasurementStd / initialMeasurementStd, 1.0 / epochs);
//	double geometricAccelerationStdModifier = pow(finalAccelerationStd / initialAccelerationStd, 1.0 / epochs);
	DyNetTrainer trainer(params, trainingSet, *pendulumNet);
	VectorXd stdDevParams(5), stdDevmeasurement(1), stdUpdateNoise(3), stdStateUncertainty(3);
	stdDevParams << 0.045, 0.07, .4 * 10, 0.4, 10.0; // shape of damping on parameters
	stdDevmeasurement << initialMeasurementStd; // weight on prediction error
	stdStateUncertainty << 100.0, 100.0, 10.0; // shape of damping on states
	stdUpdateNoise << 0.001, 0.001, initialAccelerationStd; // weight on update error
	cout << "network memory size is " << trainer.viewSizes().memorySize << endl;
	cout << "dataset input size is " << trainingSet.viewInputSize() << endl;
	trainer.setParameterUncertainty(stdDevParams);
	trainer.setStateUpdateNoise(stdUpdateNoise);
	trainer.setStateUncertainty(stdStateUncertainty);
	trainer.setMeasurementNoise(stdDevmeasurement);
	VectorXd IC(3);
	IC << 0.0, 0.0, 0.0;
//	VectorXd validationState0(2);
//	validationState0 << validationThetaZero, validationOmegaZero;
	VectorXd states = initializeStates(*pendulumNet, trainingSet, IC);
	trainer.setStates(states);
	cout << "network input size is " << pendulumNet->inputSize() << endl;

	DataSet trainingErrorDataSet(1, 3); // epoch: dyNetTrainingError, shootingtrainingError, shootingvalidiationError
	double trainingDSIns[1];
	double trainingDSOuts[3];
	double previousError = 99e99;
	int i;
	for (i = 0; i < epochs; i++)
	{
		trainer.trainOneEpoch();
		stringstream trainingName, validationName;
		trainingName << fileBase << number << "Ep" << i << "Tr.iodat";
//		double shootingError = calcDyNetShootingErrorAndPushToFile(*pendulumNet, trainingSet,
//				(double) stdDevmeasurement(0), IC, trainingName.str());
//		validationName << fileBase << number << "Ep" << i << "Val.iodat";
//		double validation = calcDyNetShootingErrorAndPushToFile(*pendulumNet, validationSet,
//				(double) stdDevmeasurement(0), validationState0, validationName.str());
		trainingDSIns[0] = i;
		trainingDSOuts[0] = trainer.getEpochError();
//		trainingDSOuts[1] = shootingError;
//		trainingDSOuts[2] = validation;
		trainingErrorDataSet.addLine(trainingDSIns, trainingDSOuts);
		if (abs(trainer.getEpochError() - previousError) < 1e-12)
			break;
		previousError = trainer.getEpochError();
//		stdUpdateNoise[2] *= geometricAccelerationStdModifier;
//		stdDevmeasurement *= geometricMeasurementStdModifier; // bias towards later editions
//		trainer.setStateUpdateNoise(stdUpdateNoise);
//		trainer.setMeasurementNoise(stdDevmeasurement);
	}
	cout << "trained for " << i << " epochs" << endl;
	trainingErrorDataSet.pushToFile(trainingDataFileName);
	trainer.log(fileBase, number);
	vector<double> bestWeights(trainer.getElitePoint().weights);
	pendulumNet->setWeights(bestWeights);

	cout << "-------trainFFNetwork done  --" << endl;
	cout << "-------final weights  --" << endl;
	cout << "a= " << pendulumNet->getWeight(0) << endl;
	cout << "b= " << pendulumNet->getWeight(1) << endl;
	cout << "c= " << pendulumNet->getWeight(2) << endl;
	cout << "d= " << pendulumNet->getWeight(3) << endl;
	cout << "e= " << pendulumNet->getWeight(4) << endl;

	//calculate and save step response of resulting model
//	VectorXd zeroIC(2);
//	zeroIC << 0.0, 0.0;
//	VectorXd step = generateStepInputs(500, 0.033, 0.01, 0.01);
//	recordFunction(*pendulumNet, stepFileName, step, zeroIC);
//	VectorXd sine = generateSineInputs(1000, 0.66, 0.01, 0.01);
//	recordFunction(*pendulumNet, sineFileName, sine, zeroIC);
	//calculate and save sine response of resulting model

}

}
}
}
//[87.78340844053233, 1.6221484642588, -0.1163943544956207, 736.0967810848022] from dynet trainer

using namespace uta::networkUtils::AppIntHW2;
int main(int argc, const char* argv[])
{
	cout << "-------AppIntHW2: -----------------------" << endl;
//	generateFeedForwardSimDataSet1();
//	generateFeedForwardSimDataSet2();
//	generateFeedForwardSimDataSet3(0.05, 0.001, 1.0, 0.0001);
	generateImpulseDataSet();
	cout << "===Pendulum Mem Test===" << endl;
//	DataSet expff1("expff1.iodat");
	DataSet expff2("expff2.iodat");
//	DataSet expff3("expff3.iodat");
	DataSet expffi("expffi.iodat");
//	DataSet expffs("expffs.iodat");
//	DataSet simff1("simff1.iodat");
	DataSet exprcs("exprcs.iodat");
	DataSet exprc1("exprc1.iodat");
	DataSet exprc2("exprc2.iodat");
	DataSet exprc3("exprc3.iodat");
	DataSet exprci("exprci.iodat");
	DataSet simrc1("simrc1.iodat");
//
//	DataSet simff2("simff2.iodat");
	DataSet simrc2("simrc2.iodat");
//	DataSet simff3("simff3.iodat");
	DataSet simrc3("simrc3.iodat");

	DataSet imp("imp.iodat");
//	cout << simff1.viewOutputSize() << " " << simff1.viewInputSize() << endl;
//	dampedLStrainFFNetwork(expff1, expff2, "dlsffexp", 1);
//	dampedLStrainFFNetwork(simff2, expff2, "dlsffsim", 2);
//	dampedLStrainFFNetwork(simff1, expff2, "dlsffsim", 1);
//	dampedLStrainFFNetwork(expffs, expff2, "dlsffexps", 0);
	dampedLStrainFFNetwork(expffi, expff2, "dlsffexpi", 0);
////
//	dampedLStrainFFNetwork(simff3, simff2, "dlsffsim", 3);
//	dampedLStrainRCNetwork(simrc3, simrc2, -0.285, 2.0, "dlsrcsim", 0.12, -1.30, 3, "dlsrcsim3.trn", "dlsrcsim3.step",
//			"dlsrcsim3.sine", 16, 1.0, 1e-1,1.0);
//	dampedLStrainRCNetwork(simrc3, simrc2, -0.285, 2.0, "DDYsim", 0.596, (.589 - .596) / 0.01, 4, "DDYsim4.trn",
//			"DDYsim4.step", "DDYsim4.sine", 16, 0.90, 1e-1,1.0);
//	dampedLStrainRCNetwork(exprcs, exprci, 0.01, 0.0, "dlsrcexps", 0.01, 0.000, 0, "dlsrcexps0.trn", "dlsrcexps0.step",
//			"dlsrcexps0.sine", 32, 1.0 - 5e-2, 3e-3, 1.0);
//	dampedLStrainRCNetwork(exprci, exprcs, 0.01, 0.000, "dlsrcexpi", 0.01, 0.0, 0, "dlsrcexpi0.trn", "dlsrcexpi0.step",
//			"dlsrcexpi0.sine", 32, 1.0 - 5e-2, 3e-3, 1.0);
//	testLowPassNetwork(exprcs, "LPs", 0, "LPs0.trn", 20);
//	testLowPassNetwork(imp, "LPImp", 0, "LPImp0.trn", 20);
//	testGaussianFilterNetwork(exprcs, "GFs", 0, "GFs0.trn", 20);
//	testGaussianFilterNetwork(imp, "GFImp", 0, "GFImp0.trn", 80);
//	testLowPassNetwork(exprcs, "LPSteps", 0, "LPSteps0.trn", 20);
	trainV3Net(exprcs, "V3Net", 14, "V3Net14.trn");
//	dampedLStrainRCNetwork(exprc1, exprc3, 0.12, -1.30, "dlsrcexp", -0.197, 2.0, 1, "dlsrcexp1.trn", "dlsrcexp1.step",
//			"dlsrcexp1.sine", 16, 1.0, 1e-2,1.0);
//	dampedLStrainRCNetwork(exprc2, exprc1, -0.197, 2.0, "dlsrcexp", -0.285, 2.0, 2, "dlsrcexp2.trn", "dlsrcexp2.step",
//			"dlsrcexp2.sine", 16, 1.0, 1e-2,1.0);
//	dampedLStrainRCNetwork(exprc3, exprc2, -0.285, 2.0, "dlsrcexp", 0.12, -1.30, 3, "dlsrcexp3.trn", "dlsrcexp3.step",
//			"dlsrcexp3.sine", 16, 1.0, 1e-2,1.0);
//	dampedLStrainRCNetwork(simrc2, simrc1, -0.285, 2.0, "dlsrcsim", 0.12, -1.30, 2, "dlsrcsim2.trn", "dlsrcsim2.step",
//			"dlsrcsim2.sine", 16, 1.0, 1e-1,1.0);
//	dampedLStrainRCNetwork(simrc1, simrc2, -0.285, 2.0, "dlsrcsim", 0.12, -1.30, 1, "dlsrcsim1.trn", "dlsrcsim1.step",
//			"dlsrcsim1.sine", 16, 1.0, 1e-1,1.0);
//	trainFFNetwork(expff2, expff3, "gdff2.gdtrn");
//	trainFFNetwork(expff3, expff1, "gdff3.gdtrn");
//	trainFFNetwork(expffi, simff1, "gdffi.gdtrn");
//	trainFFNetwork(expffs, expffi, "gdffs.gdtrn");
//	trainFFNetwork(simff1, expffi, "gdsimff1.gdtrn");
//	trainFFNetwork(simff2, simff1, "gdsimff2.gdtrn");
	cout << "-------run AppIntHW2 Completed Successfully--" << endl;
	return 0;
}
