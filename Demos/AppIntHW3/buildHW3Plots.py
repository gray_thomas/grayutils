import scipy.integrate as integrate
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from networkUtils.pendulumDemo import *
from networkUtils.dyNetUtils.trainingFileManager import * 
from networkUtils.dyNetUtils.convolutionFilter import *
from matplotlib.backends.backend_pdf import PdfPages
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

Run1 = "pendubot_run1.dat"
Run2 = "pendubot_run2.dat"
Run3 = "pendubot_run3.dat"
Impulses = "pendubot_impulses.dat"
Steps = "pendubot_steps.dat"
output_filename = "example_out.dat"
manager = TrainingFileManager("C:/Users/Gray/wk/grayutils/Demos/AppIntHW3");

def plotController(fileName, pdfName):
    dat = manager.loadIODat(fileName)
    fig = plt.figure(figsize=(6, 1), facecolor='w')
    plt.plot(dat[:,0], linewidth=2)
    plt.plot(dat[:,3], linewidth=2)
    plt.xlabel(r"controller samples")
    plt.ylabel(r"$\theta$")
    plt.legend([r"\theta_{\rm{sim1}}",r"\theta_{d,\rm{sim1}}"])
    manager.saveFig(fig, pdfName)
def plotTraining(fileName, pdfBaseName):
    dat=manager.loadDyNetYaml(fileName)
    fig = plt.figure(figsize=(6,1),facecolor='w')
    plt.plot(dat.epochData[0].states_used[:,0], linewidth=2)
    plt.plot(dat.epochData[0].states_predicted[:,0], linewidth=2)
    plt.xlabel(r"$t\;(\rm{tics})$")
    plt.ylabel(r"$\theta\;(\rm{rad})$")
    manager.saveFig(fig,pdfBaseName+"Theta.pdf")
    fig = plt.figure(figsize=(6,1),facecolor='w')
    plt.plot(dat.epochData[0].states_used[:,1], linewidth=2)
    plt.plot(dat.epochData[0].states_predicted[:,1], linewidth=2)
    plt.xlabel(r"$t\;(\rm{tics})$")
    plt.ylabel(r"$\omega\;(\rm{\frac{rad}{s}})$")
    manager.saveFig(fig,pdfBaseName+"Omega.pdf")
    fig = plt.figure(figsize=(6,1),facecolor='w')
    plt.plot(dat.epochData[0].states_used[:,2], linewidth=2)
    plt.plot(dat.epochData[0].states_predicted[:,2], linewidth=2)
    plt.xlabel(r"$t\;(\rm{tics})$")
    plt.ylabel(r"$\alpha\;(\rm{\frac{rad}{s^2}})$")
    manager.saveFig(fig,pdfBaseName+"Alpha.pdf")
#     fig = plt.figure(figsize=(6,1),facecolor='w')
#     plt.plot(dat.epochData[0].states_used[:,3], linewidth=2)
#     plt.plot(dat.epochData[0].states_predicted[:,3], linewidth=2)
#     plt.xlabel(r"$t\;(\rm{tics})$")
#     plt.ylabel(r"$u\;(\rm{enc})$")
#     manager.saveFig(fig,pdfBaseName+"Input.pdf")
def main():
#     plotController("Sim1Opt7Epoch29.iodat", "Sim1Opt7Epoch29.pdf")    
#     plotTraining(  "Sim1Opt7Epoch29.yaml",  "Sim1Opt7Epoch29")    
    for i in [3,7,12,16,22,29] :   
        plotController("Sim1Opt11Epoch"+str(i)+".iodat", "Sim1Opt10Epoch"+str(i)+".pdf")
#     plotTraining(  "Sim1Opt9Epoch4.yaml",  "Sim1Opt9Epoch4")
#     plotTraining(  "Sim1Opt9Epoch15.yaml",  "Sim1Opt9Epoch15")
#     plotTraining(  "Sim1Opt9Epoch29.yaml",  "Sim1Opt9Epoch29")
    plt.show()
    manager.saveFigsAfterPltShow()  

if __name__ == '__main__':
    main()
