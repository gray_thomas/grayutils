/*
 * runHW3.cpp
 *
 *  Created on: May 2, 2014
 *      Author: Gray
 */

#include <iostream>
#include <DataSet.hpp>
#include <DyNetTrainer.hpp>
#include <GradientDescentTrainer.hpp>
#include <NetworkStateVector.hpp>
#include <PendulumDemoRandomDataGenerator.hpp>
#include <PendulumNet.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <sstream>

#define PI 3.14159265395

using namespace std;
using namespace uta::networkUtils;
using namespace uta::networkUtils::pendulumDemo;
using namespace std;
namespace uta
{
namespace networkUtils
{
namespace AppIntHW3
{
double calcDyNetShootingError(Trainable& network, DataSet& validationSet, double stdDevMeasurement, VectorXd state0);
double calcDyNetShootingErrorAndPushToFile(Trainable& network, DataSet& validationSet, double stdDevMeasurement,
		VectorXd state0, string fileName);
void recordFunction(Trainable& network, string fileName, MatrixXd inputs, VectorXd startingStates);
VectorXd generateSineInputs(unsigned size, double freq, double dt, double amplitude);
VectorXd generateStepInputs(unsigned size, double freq, double dt, double amplitude);

void train(DataSet& trainingSet, DataSet& validationSet, double validationThetaZero, double validationOmegaZero,
		string fileBase, double thetaZero, double omegaZero, unsigned number, string trainingDataFileName,
		string stepFileName, string sineFileName, int epochs, double geometricDerelaxation, double softness,
		double startingRelaxation)
{
	NetworkStateVector state;
	double dt = 0.01, aGuess = 0.4, bGuess = 0.2, cGuess = 0.3, dGuess = -2.0;
//	a= 65.5293
//	b= 0.0886516
//	c= 0.0404539
//	d= 516.172
//	aGuess = 67.4944;
//	bGuess = 1.35948;
//	cGuess =-0.360333;
//	dGuess = 496.412;
	aGuess = 45.6159 * (1 + 1e-1);
	bGuess = 0.728237 * (1 - 1e-1);
	cGuess = 0.0421311 * (1 - 1e-1);
	dGuess = 400.0 * (1 - 1e-1);
	aGuess = 45;
	bGuess = 4.0;
	cGuess = 0.3;
	dGuess = 200.0;
//	a= 41.7347
//	b= -0.456419
//	c= -0.981899
//	d= -2.32863
//	a= 40.2696
//	b= -1.85958
//	c= -2.8132
//	d= -6.71319
	double kp = 1.0;
	double kd = 1.0;
	Network* pendulumNet = new PDPendulum(state, dt);
//	FullPendulumNetV2PD(state, dt, aGuess, bGuess, cGuess, dGuess, kp, kd, softness);
	DyNetTrainerParameters params;
	params.trustRegionScaleParameter = 1.0e3;
	params.linearitySetPoint = 0.75;
	params.linearityMinimumError = 0.25;
	params.trustRegionPenalty = 0.2;
	params.trustRegionPositiveBump = 1.3;
	DyNetTrainer trainer(params, trainingSet, *pendulumNet);
	VectorXd stdDevParams(4), stdDevmeasurement(1), stdUpdateNoise(2), stdStateUncertainty(2);
	stdDevParams << 4.5, .07, .004, 40; // shape of damping on parameters
	stdDevmeasurement << 0.05; // weight on prediction error
	stdStateUncertainty << 100.0, 1000.0; // shape of damping on states
	stdUpdateNoise << 0.0001, 10.000 * startingRelaxation; // weight on update error
	cout << "network memory size is " << trainer.viewSizes().memorySize << endl;
	cout << "dataset input size is " << trainingSet.viewInputSize() << endl;
	trainer.setParameterUncertainty(stdDevParams);
	trainer.setStateUpdateNoise(stdUpdateNoise);
	trainer.setStateUncertainty(stdStateUncertainty);
	trainer.setMeasurementNoise(stdDevmeasurement);
	VectorXd IC(2);
	IC << thetaZero, omegaZero;
	VectorXd validationState0(2);
	validationState0 << validationThetaZero, validationOmegaZero;
	VectorXd states = initializeStates(*pendulumNet, trainingSet, IC);
	trainer.setStates(states);
	cout << "network input size is " << pendulumNet->inputSize() << endl;

	DataSet trainingErrorDataSet(1, 3); // epoch: dyNetTrainingError, shootingtrainingError, shootingvalidiationError
	double trainingDSIns[1];
	double trainingDSOuts[3];

	for (int i = 0; i < epochs; i++)
	{
		trainer.trainOneEpoch();
		stringstream trainingName, validationName;
		trainingName << fileBase << number << "Ep" << i << "Tr.iodat";
		double shootingError = calcDyNetShootingErrorAndPushToFile(*pendulumNet, trainingSet,
				(double) stdDevmeasurement(0), IC, trainingName.str());
		validationName << fileBase << number << "Ep" << i << "Val.iodat";
		double validation = calcDyNetShootingErrorAndPushToFile(*pendulumNet, validationSet,
				(double) stdDevmeasurement(0), validationState0, validationName.str());
		trainingDSIns[0] = i;
		trainingDSOuts[0] = trainer.getEpochError();
		trainingDSOuts[1] = shootingError;
		trainingDSOuts[2] = validation;
		trainingErrorDataSet.addLine(trainingDSIns, trainingDSOuts);
		stdUpdateNoise *= 1.0; //geometricDerelaxation;
		stdDevmeasurement *= 1.0 / geometricDerelaxation; // bias towards later editions
		trainer.setStateUpdateNoise(stdUpdateNoise);
		trainer.setMeasurementNoise(stdDevmeasurement);
	}
	trainingErrorDataSet.pushToFile(trainingDataFileName);
	trainer.log(fileBase, number);
	vector<double> bestWeights(trainer.getElitePoint().weights);
	pendulumNet->setWeights(bestWeights);

	cout << "-------trainFFNetwork done  --" << endl;
	cout << "-------final weights  --" << endl;
	cout << "a= " << pendulumNet->getWeight(0) << endl;
	cout << "b= " << pendulumNet->getWeight(1) << endl;
	cout << "c= " << pendulumNet->getWeight(2) << endl;
	cout << "d= " << pendulumNet->getWeight(3) << endl;

	//calculate and save step response of resulting model
	VectorXd zeroIC(2);
	zeroIC << 0.0, 0.0;
	VectorXd step = generateStepInputs(500, 0.033, 0.01, 0.01);
	recordFunction(*pendulumNet, stepFileName, step, zeroIC);
	VectorXd sine = generateSineInputs(1000, 0.66, 0.01, 0.01);
	recordFunction(*pendulumNet, sineFileName, sine, zeroIC);
	//calculate and save sine response of resulting model

}
VectorXd generateRandomInputs(unsigned size)
{
	VectorXd randomInputs(size);
	for (unsigned i = 0; i < size; i++)
	{
		//TODO: implement a random input
	}
	return randomInputs;
}

VectorXd generateSineInputs(unsigned size, double freq, double dt, double amplitude)
{
	VectorXd sineInput(size);
	double phase = 0;
	for (unsigned i = 0; i < size; i++)
	{
		phase += 2 * 3.14159 * freq * dt;
		sineInput(i) = sin(phase) * amplitude;
	}
	return sineInput;
}
void recordFunction(Trainable& network, string fileName, MatrixXd inputs, VectorXd startingStates)
{
	VectorXd state(startingStates);
	double prediction;
	DataSet dataToWrite(1, 1);
	double dsI[1], dsO[1];
	for (int i = 0; i < inputs.rows(); i++)
	{
		network.setInput(0, (double) state(0));
		network.setInput(1, (double) state(1));
		network.setInput(2, (double) inputs(i, 0));
		dsI[0] = inputs(i, 0);
		network.calculateOutputs();
		state(0) = network.getOutput(0);
		state(1) = network.getOutput(1);
		dsO[0] = network.getOutput(2);
		dataToWrite.addLine(dsI, dsO);
	}
	dataToWrite.pushToFile(fileName);
}
VectorXd generateStepInputs(unsigned size, double freq, double dt, double amplitude)
{
	VectorXd sineInput(size);
	double phase = 0;
	for (unsigned i = 0; i < size; i++)
	{
		phase += freq * dt;
		if (phase >= 1)
			phase -= 1.0;
		sineInput(i) = ((phase < 0.5) ? amplitude : 0);
	}
	return sineInput;
}
void generateDataToFile(Trainable& network, VectorXd inputs, VectorXd state0, double dt, string fileName)
{
	// Assert network has three inputs, one output. The output is the acceleration of the first input,
	// and the second input is velocity.
	// Assert state0 is size 2
	VectorXd state(state0);
	DataSet dataSet(3, 1);
	double dataIns[3];
	double dataOuts[1];
	for (unsigned i = 0; i < inputs.rows(); i++)
	{
		dataIns[0] = (double) state(0);
		dataIns[1] = (double) state(1);
		dataIns[2] = (double) inputs(i);
		network.setInput(0, dataIns[0]);
		network.setInput(1, dataIns[1]);
		network.setInput(2, dataIns[2]);
		network.calculateOutputs();
		dataOuts[0] = network.getOutput(0);
		state(0) += dt * state(1) + 0.5 * dt * dt * dataOuts[0];
		state(1) += dt * dataOuts[0];
		dataSet.addLine(dataIns, dataOuts);
	}
	dataSet.pushToFile(fileName);
}
double calcDyNetShootingError(Trainable& network, DataSet& validationSet, double stdDevMeasurement, VectorXd state0)
{
	// Assert network has three inputs, one output. The output is the acceleration of the first input,
	// and the second input is velocity.
	// Assert state0 is size 2
	VectorXd state(state0);
	double error = 0;

	for (unsigned i = 0; i < validationSet.viewSize(); i++)
	{
		network.setInput(0, (double) state(0));
		network.setInput(1, (double) state(1));
		network.setInput(2, (double) validationSet.getInput(i, 0));
		network.calculateOutputs();
		state(0) = network.getOutput(0);
		state(1) = network.getOutput(1);
		double err = (validationSet.getOutput(i, 0) - network.getOutput(2)) / stdDevMeasurement;
		error += err * err;
	}
	return error;
}
double calcDyNetShootingErrorAndPushToFile(Trainable& network, DataSet& validationSet, double stdDevMeasurement,
		VectorXd state0, string fileName)
{
	// Assert network has three inputs, one output. The output is the acceleration of the first input,
	// and the second input is velocity.
	// Assert state0 is size 2
	VectorXd state(state0);
	double error = 0;
	DataSet dataSet(3, 2);
	double dataIns[3];
	double dataOuts[2];
	for (unsigned i = 0; i < validationSet.viewSize(); i++)
	{
		network.setInput(0, (double) state(0));
		network.setInput(1, (double) state(1));
		network.setInput(2, (double) validationSet.getInput(i, 0));
		network.calculateOutputs();
		state(0) = network.getOutput(0);
		state(1) = network.getOutput(1);

		dataOuts[0] = validationSet.getOutput(i, 0);
		dataOuts[1] = network.getOutput(2);
		double err = (validationSet.getOutput(i, 0) - network.getOutput(2)) / stdDevMeasurement;
		error += err * err;
		dataSet.addLine(dataIns, dataOuts);
	}
	dataSet.pushToFile(fileName);
	return error;
}

void generateFeedForwardSimDataSet1()
{
	double a = 42.4138;
	double b = 0.86046;
	double c = 0.0472982;
	double d = 418.495;
	PendulumABCDDataGenerator dataGenerator(a, b, c, d);
	dataGenerator.generateRandomData0mem("simff1.iodat", 1000, 1.4, -0.1, 0.01, 0.1, 0.01, 10, 40, 0.01, 5);
	cout << "generated simff1.iodat" << endl;
}
void generateFeedForwardSimDataSet2()
{
	double a = 45.6159;
	double b = 0.728237;
	double c = 0.0421311;
	double d = 418.495;
	NetworkStateVector netState;
	Network* pendulumNetwork = new AlphaPendulumNetV2(netState, 0.01, a, b, c, d, 1e-6);
	DataSet expffs("expffs.iodat");
	cout << "expffs sizes: i,o,dat = " << expffs.viewInputSize() << ", " << expffs.viewOutputSize() << ", "
			<< expffs.viewSize() << endl;
	VectorXd inputs(expffs.viewSize());
	for (unsigned i = 0; i < expffs.viewSize(); i++)
		inputs(i) = expffs.getInput(i, 2);
	VectorXd state0(2);
	state0 << expffs.getInput(0, 0), expffs.getInput(0, 1);
	generateDataToFile(*pendulumNetwork, inputs, state0, 0.01, "simff2.iodat");
	cout << "generated simff2.iodat" << endl;
}
void generateFeedForwardSimDataSet3(double stdDevParams, double thetaMeasureNoise, double alphaMeasureNoise,
		double accelerationNoise)
{
	default_random_engine gen(1337);
	normal_distribution<double> baseGaussian(0.0, 1.0);

	double a = 50.6159 * (1 + baseGaussian(gen) * stdDevParams);
	double b = 1.728237 * (1 + baseGaussian(gen) * stdDevParams);
	double c = 1.421311 * (1 + baseGaussian(gen) * stdDevParams);
	double d = 418.495 * (1 + baseGaussian(gen) * stdDevParams);
	cout << "trainingSet3 uses a,b,c,d: " << a << " " << b << " " << c << " " << d << endl;

	NetworkStateVector netState;
	Network* pendulumNetwork = new AlphaPendulumNetV2(netState, 0.01, a, b, c, d, 1e-6);
	DataSet expffs("expffs.iodat");
	cout << "expffs sizes: i,o,dat = " << expffs.viewInputSize() << ", " << expffs.viewOutputSize() << ", "
			<< expffs.viewSize() << endl;
	VectorXd inputs(expffs.viewSize());
	for (unsigned i = 0; i < expffs.viewSize(); i++)
		inputs(i) = expffs.getInput(i, 2);
	VectorXd state0(2);
	state0 << 0.6, -0.1;
	VectorXd state(state0);
	DataSet ffDat(3, 1);
	DataSet rcDat(1, 1);
	Network& network = *pendulumNetwork;
	double dt = 0.01;
	double ffIns[3], rcIns[1];
	double ffOuts[1], rcOuts[1];
	for (unsigned i = 0; i < inputs.rows() / 2; i++)
	{
		ffIns[0] = (double) state(0);
		ffIns[1] = (double) state(1);
		ffIns[2] = (double) inputs(i) / 10.0;
		rcIns[0] = ffIns[2];
		network.setInput(0, ffIns[0]);
		network.setInput(1, ffIns[1]);
		network.setInput(2, ffIns[2]);
		network.calculateOutputs();
		double alpha = network.getOutput(0) + accelerationNoise * baseGaussian(gen);
		state(0) += dt * state(1) + 0.5 * dt * dt * alpha;
		state(1) += dt * alpha;
		ffOuts[0] = alpha + baseGaussian(gen) * alphaMeasureNoise;
		rcOuts[0] = state(0) + baseGaussian(gen) * thetaMeasureNoise;
		ffDat.addLine(ffIns, ffOuts);
		rcDat.addLine(rcIns, rcOuts);
	}
	ffDat.pushToFile("simff3.iodat");
	rcDat.pushToFile("simrc3.iodat");
	cout << "generated simff3.iodat" << endl;
}
void simulateNetwork(Trainable* net, DataSet& trainingData, VectorXd state0, string simName)
{
	VectorXd states = initializeStates(*net, trainingData, state0);
	DataSet resultingPattern(5, 6);
	double resI[5], resO[6];
	for (int i = 0; i < trainingData.viewSize() - 1; i++)
	{
		resI[0] = states[i * 4 + 0];
		resI[1] = states[i * 4 + 1];
		resI[2] = states[i * 4 + 2];
		resI[3] = states[i * 4 + 3];
		resI[4] = trainingData.getInput(i, 0);
		for (int j = 0; j < 5; j++)
			net->setInput(j, resI[j]);
		net->calculateOutputs();
		for (int j = 0; j < 6; j++)
			resO[j] = net->getOutput(j);
		resultingPattern.addLine(resI, resO);
	}
	resultingPattern.pushToFile(simName);
}
void simulateNetwork3(Trainable* net, DataSet& trainingData, VectorXd state0, string simName)
{
	VectorXd states = initializeStates(*net, trainingData, state0);
	DataSet resultingPattern(5, 7);
	double resI[5], resO[7];
	for (int i = 0; i < trainingData.viewSize() - 1; i++)
	{
		resI[0] = states[i * 3 + 0];
		resI[1] = states[i * 3 + 1];
		resI[2] = states[i * 3 + 2];
		resI[3] = trainingData.getInput(i, 0);
		for (int j = 0; j < 4; j++)
			net->setInput(j, resI[j]);
		net->calculateOutputs();
		for (int j = 0; j < 6; j++)
			resO[j] = net->getOutput(j);
		resultingPattern.addLine(resI, resO);
	}
	resultingPattern.pushToFile(simName);
}
void genSim(string simName, string dataFileName)
{
	NetworkStateVector state;

	Network * net = new PDPendulum(state, 0.001);
	VectorXd state0(4);
	double theta0 = 0.4, omega0 = -0.0, alpha0 = 0, u0 = -0.01;
	state0 << theta0, omega0, alpha0, u0;
// inputs: ref
// outputs: tracking error and control-> both should be zero.
	DataSet trainingData(1, 2);
	double ins[1], outs[2];
	for (int i = 0; i < 10000; i++)
	{
		if (i < 4000)
		{
			if (i % 200 == 0)
				ins[0] = sin(i * 0.005) * 1.8; // start with one stepwise sine
		}
		else if (i < 8000)
			ins[0] = sin(i * 0.01) * 1.2; // then faster
		else
			ins[0] = 0; // then hold position
		outs[0] = 0; // desire zero error
		outs[1] = 0; // desire zero current
		trainingData.addLine(ins, outs);
	}
	trainingData.pushToFile(dataFileName);
	simulateNetwork(net, trainingData, state0, simName);
}
void genSim3(string simName, string dataFileName)
{
	NetworkStateVector state;

	Network * net = new PDPendulum(state, 0.001);
	VectorXd state0(3);
	double theta0 = 0.4, omega0 = -0.0, alpha0 = 0;
	state0 << theta0, omega0, alpha0;
// inputs: ref
// outputs: tracking error and control-> both should be zero.
	DataSet trainingData(2, 3);
	double ins[2], outs[3];
	for (int i = 0; i < 10000; i++)
	{
		if (i < 4000)
		{
			if (i % 200 == 0)
				ins[0] = sin(i * 0.005) * 0.18; // start with one stepwise sine
		}
		else if (i < 8000)
			ins[0] = sin(i * 0.01) * 0.12; // then faster
		else
			ins[0] = 0; // then hold position
		ins[1] = exp(-0.005*i); // stability bias, used to weight states by time!
		outs[0] = 0; // desire zero error
		outs[1] = 0; // desire zero current
		outs[2] = 0;
		trainingData.addLine(ins, outs);
	}
	trainingData.pushToFile(dataFileName);
	simulateNetwork3(net, trainingData, state0, simName);
}
void doTraining(string filebase, int number)
{
	DataSet trialDat("TrialDat2.iodat");
	NetworkStateVector state;
	PDPendulum* net = new PDPendulum(state, 0.01);
	DyNetTrainerParameters params;
	params.trustRegionScaleParameter = 1.0e2;
	params.linearitySetPoint = 0.75;
	params.linearityMinimumError = 0.25;
	params.trustRegionPenalty = 0.2;
	params.trustRegionPositiveBump = 4.3;
	DyNetTrainer trainer(params, trialDat, *net);
	cout << "network memory size is " << trainer.viewSizes().memorySize << endl;
	cout << "dataset input size is " << trialDat.viewInputSize() << endl;
	VectorXd IC = VectorXd::Zero(net->viewStateNames().size());
	VectorXd states = initializeStates(*net, trialDat, IC);
	trainer.setStates(states);
	cout << "network input size is " << net->inputSize() << endl;
	Eigen::VectorXd stdDevMeasurementError = net->getStdDevMeasurementError();
	Eigen::VectorXd stdDevParamsDamping = net->getStdDevParamsDamping();
	Eigen::VectorXd stdDevStateDamping = net->getStdDevStateDamping();
	Eigen::VectorXd stdDevUpdateError = net->getStdDevUpdateError();
	cout <<" good "<<stdDevMeasurementError<< endl;
	trainer.setMeasurementNoise(stdDevMeasurementError);
	cout << "bad"<<endl;
	trainer.setParameterUncertainty(stdDevParamsDamping);
	trainer.setStateUncertainty(stdDevStateDamping);
	trainer.setStateUpdateNoise(stdDevUpdateError);
	DataSet trainingErrorDataSet(1, 1);
	double trainingDSIns[1];
	double trainingDSOuts[1];
	int i;
	int epochs = 30;
	for (i = 0; i < epochs; i++)
	{
		cout << "epoch " << i << endl;
		stdDevMeasurementError << net->getStdDevMeasurementError(((double) i) / (epochs - 1));
		stdDevParamsDamping << net->getStdDevParamsDamping(((double) i) / (epochs - 1));
		stdDevStateDamping << net->getStdDevStateDamping(((double) i) / (epochs - 1));
		stdDevUpdateError << net->getStdDevUpdateError(((double) i) / (epochs - 1));
		trainer.setMeasurementNoise(stdDevMeasurementError);
		trainer.setParameterUncertainty(stdDevParamsDamping);
		trainer.setStateUncertainty(stdDevStateDamping);
		trainer.setStateUpdateNoise(stdDevUpdateError);

		trainer.trainOneEpoch();
		stringstream ss;
		ss << filebase << number << "Epoch" << i << ".iodat";
		simulateNetwork3(net, trialDat, IC, ss.str());
		trainingDSIns[0] = i;
		trainingDSOuts[0] = trainer.getEpochError();
		trainer.log1Epoch(filebase, number, i);
		trainingErrorDataSet.addLine(trainingDSIns, trainingDSOuts);
		cout << "-------final weights with error --" << trainer.getElitePoint().error << endl;
		for (int i = 0; i < net->viewParameterNames().size(); i++)
			cout << net->viewParameterNames()[i] << "= " << net->getWeight(i) << endl;
	}
	cout << "trained for " << i << " epochs" << endl;
	stringstream ss;
	ss << filebase << number << ".Trn";
	trainingErrorDataSet.pushToFile(ss.str());
	trainer.log(filebase, number);

	vector<double> bestWeights(trainer.getElitePoint().weights);
	cout << "-------training done  --" << endl;
}

}
}
}
using namespace uta::networkUtils::AppIntHW3;
int main(int argc, const char* argv[])
{
	cout << "Hello AppInt HW3" << endl;
	genSim3("Sim2.iodat", "TrialDat2.iodat");
	doTraining("Sim1Opt",11);


	cout << "Goodbye AppInt HW3" << endl;
	return 0;
}

