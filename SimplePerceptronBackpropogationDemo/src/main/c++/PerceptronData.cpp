/*
 * PerceptronData.cpp
 *
 *    Created on: Oct 7, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */


#include <PerceptronData.hpp>
#include <fstream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

void operator<<(TrainingDataPoint &dataPoint, string &line)
{
	istringstream splitLine(line);
	for (unsigned i = 0; i < dataPoint.inputs.size(); i++)
		splitLine >> dataPoint.inputs[i];
	for (unsigned o = 0; o < dataPoint.desiredOutputs.size(); o++)
		splitLine >> dataPoint.desiredOutputs[o];
}

void operator<<(SimpleDataSet &dataSet, ifstream &file)
{
	if (file.is_open())
	{
		string line;
		getline(file, line);
		istringstream splitLine(line);
		int inputSize, outputSize;
		splitLine >> inputSize;
		splitLine >> outputSize;
		while (getline(file, line))
		{
			TrainingDataPoint dataPoint;
			dataPoint.desiredOutputs.resize(outputSize);
			dataPoint.inputs.resize(inputSize);
			dataPoint << line;
			dataSet.data.push_back(dataPoint);
		}
		file.close();
	} else {
		std::cerr<<"failure to open file"<<std::endl;
		throw 0;
	}
}
ostream & operator<<(ostream &stream, TrainingDataPoint &dataPoint)
{
	for (unsigned i = 0; i < dataPoint.inputs.size(); i++)
		stream << dataPoint.inputs[i] << " ";
	for (unsigned o = 0; o < dataPoint.desiredOutputs.size(); o++)
		stream << dataPoint.desiredOutputs[o] << " ";
	return stream;
}
ostream & operator<<(ostream &stream, SimpleDataSet &dataSet)
{
	stream << ((TrainingDataPoint) dataSet.data[0]).inputs.size() << " ";
	stream << ((TrainingDataPoint) dataSet.data[0]).desiredOutputs.size();
	stream << endl;
	for (unsigned i = 0; i < dataSet.data.size(); i++)
	{
		stream << dataSet.data[i] << endl;
	}
	return stream;
}



