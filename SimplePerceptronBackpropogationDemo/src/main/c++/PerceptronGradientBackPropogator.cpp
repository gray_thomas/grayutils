/*
 * PerceptronGradientBackPropogator.cpp
 *
 *    Created on: Oct 7, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include <PerceptronGradientBackPropogator.hpp>
#include <vector>
#include <PerceptronWeights.hpp>
#include <cmath>

struct PerceptronWeights;
struct TrainingDataPoint;

using namespace std;

PerceptronGradientBackPropogator::PerceptronGradientBackPropogator(VectorOverInputAndLayers<unsigned>& layerSizes) :
			internalState(layerSizes),
			tempPartialwrtState(layerSizes),
			partialDerivativeOfErrorWithRespectToWeights(layerSizes)
{
	cout << "start constructor" << endl;
	unsigned outputSize = layerSizes[layerSizes.size() - 1];
	for (unsigned o = 0; o < outputSize; o++)
	{
		PerceptronWeights der(layerSizes);
		partialDerivativeOfOutputsWithRespectToWeights.push_back(der);
	}
	cout << "end constructor" << endl;

}

double PerceptronGradientBackPropogator::calculateError(const PerceptronWeights& currentWeights, const TrainingDataPoint& dataPoint)
{

	for (unsigned i = 0; i < currentWeights.layerSizes[0]; i++)
		internalState.layerOutput[internalState.inputLayerIndex][i] = dataPoint.inputs[i];
	unsigned indexOfTheOutputLayer = currentWeights.layerSizes.size() - 1;
	for (unsigned layerWithInputsIndex = 1; layerWithInputsIndex < currentWeights.layerSizes.size(); layerWithInputsIndex++)
	{
		unsigned indexOfParentLayer = layerWithInputsIndex - 1;
		unsigned indexOfChildLayer = layerWithInputsIndex;
		unsigned childSize = currentWeights.layerSizes[indexOfChildLayer];
		unsigned parentSize = currentWeights.layerSizes[indexOfParentLayer];
		unsigned fencePost = 1;
		for (unsigned childIndex = 0; childIndex < childSize; childIndex++)
		{
			internalState.layerAccumulator[indexOfChildLayer - fencePost][childIndex] =
					(double) currentWeights.layerBiases[indexOfChildLayer - fencePost][childIndex];

			for (unsigned parentIndex = 0; parentIndex < parentSize; parentIndex++)
				internalState.layerAccumulator[indexOfChildLayer - fencePost][childIndex] +=
						internalState.layerOutput[indexOfParentLayer][parentIndex]
								* currentWeights.layerWeights[indexOfChildLayer - fencePost][parentIndex][childIndex];

		}
		for (unsigned z = 0; z < currentWeights.layerSizes[layerWithInputsIndex]; z++)
			internalState.layerOutput[layerWithInputsIndex][z] =
					layerWithInputsIndex < indexOfTheOutputLayer ?
							tanh(internalState.layerAccumulator[layerWithInputsIndex - 1][z]) :
							internalState.layerAccumulator[layerWithInputsIndex - 1][z];
	}
	double errorForDataPoint = 0;

	for (unsigned o = 0; o < currentWeights.layerSizes[internalState.outputLayerIndex]; o++)
	{
		internalState.errorVector[o] = internalState.layerOutput[internalState.outputLayerIndex][o] - dataPoint.desiredOutputs[o];
		errorForDataPoint += internalState.errorVector[o] * internalState.errorVector[o];
	}

	return errorForDataPoint;
}

double PerceptronGradientBackPropogator::calculateErrorAndGradient(const PerceptronWeights &currentWeights,
																	const TrainingDataPoint &dataPoint)
{
	double error = calculateError(currentWeights, dataPoint);
	unsigned indexOfTheOutputLayer = currentWeights.layerSizes.size() - 1;
	for (unsigned o = 0; o < currentWeights.layerSizes[indexOfTheOutputLayer]; o++)
	{
		tempPartialwrtState.errorVector[o] = 2 * internalState.errorVector[o];
		tempPartialwrtState.layerOutput[indexOfTheOutputLayer][o] = tempPartialwrtState.errorVector[o];
		tempPartialwrtState.layerAccumulator[indexOfTheOutputLayer - 1][o] = tempPartialwrtState.layerOutput[indexOfTheOutputLayer][o];
	}
	for (unsigned l = indexOfTheOutputLayer - 1; l > 0; l--)
	{
		for (unsigned p = 0; p < currentWeights.layerSizes[l]; p++)
		{
			tempPartialwrtState.layerOutput[l][p] = 0;
			for (unsigned c = 0; c < currentWeights.layerSizes[l + 1]; c++)
				tempPartialwrtState.layerOutput[l][p] += tempPartialwrtState.layerAccumulator[l][c] * currentWeights.layerWeights[l][p][c];
		}
		for (unsigned p = 0; p < currentWeights.layerSizes[l]; p++)
		{
			double tanhOfAccumulatorValue = internalState.layerOutput[l][p];
			double derivitiveOfTanh = 1 - tanhOfAccumulatorValue * tanhOfAccumulatorValue;
			tempPartialwrtState.layerAccumulator[l - 1][p] = derivitiveOfTanh * tempPartialwrtState.layerOutput[l][p];
		}
	}
	for (unsigned p = 0; p < currentWeights.layerSizes[0]; p++)
	{
		tempPartialwrtState.layerOutput[0][p] = 0;
		for (unsigned c = 0; c < currentWeights.layerSizes[1]; c++)
			tempPartialwrtState.layerOutput[0][p] += tempPartialwrtState.layerAccumulator[0][c] * currentWeights.layerWeights[0][p][c];
	}

	for (unsigned l = 0; l < indexOfTheOutputLayer; l++)
	{
		for (unsigned o = 0; o < currentWeights.layerSizes[l + 1]; o++)
		{
			partialDerivativeOfErrorWithRespectToWeights.layerBiases[l][o] = tempPartialwrtState.layerAccumulator[l][o];
		}
		for (unsigned p = 0; p < currentWeights.layerSizes[l]; p++)
		{
			for (unsigned c = 0; c < currentWeights.layerSizes[l + 1]; c++)
			{
				partialDerivativeOfErrorWithRespectToWeights.layerWeights[l][p][c] = tempPartialwrtState.layerAccumulator[l][c]
						* internalState.layerOutput[l][p];
			}
		}
	}

	return error;
}
double PerceptronGradientBackPropogator::calculateErrorGradientAndJacobian(const PerceptronWeights &currentWeights,
																			const TrainingDataPoint &dataPoint)
{
	double error = calculateError(currentWeights, dataPoint);

	return error;
}
void PerceptronGradientBackPropogator::packGradient(PerceptronWeights &gradientToPack)
{
	gradientToPack << this->partialDerivativeOfErrorWithRespectToWeights;
}
void PerceptronGradientBackPropogator::packJacobian(VectorOverOutputs<PerceptronWeights> &jacobianToPack)
{
}

PerceptronInternalState::PerceptronInternalState(VectorOverInputAndLayers<unsigned>& layerSizes)
{
	cout << "in internalState constructor" << endl;
	for (unsigned layer = 0; layer < layerSizes.size(); layer++)
	{
		VectorOverLayerSize<double> output;
		output.resize(layerSizes[layer]);
		layerOutput.push_back(output);
	}

	for (unsigned layer = 1; layer < layerSizes.size(); layer++)
	{
		VectorOverLayerSize<double> accumulator;
		accumulator.resize(layerSizes[layer]);
		layerAccumulator.push_back(accumulator);
	}
	unsigned sizeOfOutputs = layerSizes[layerSizes.size() - 1];
	errorVector.resize(sizeOfOutputs);
	inputLayerIndex = 0;
	outputLayerIndex = layerSizes.size() - 1;
}

void PerceptronGradientBackPropogator::packOutputs(VectorOverOutputs<double>& outputsToPack)
{
	VectorOverOutputs<double> &outputs = internalState.layerOutput[internalState.outputLayerIndex];
	for (unsigned i = 0; i < outputs.size(); i++)
		outputsToPack[i] = outputs[i];
}
