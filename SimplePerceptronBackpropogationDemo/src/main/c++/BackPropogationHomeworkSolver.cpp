//============================================================================
// Name        : BackPropogationHomeworkSolver.cpp
// Author      : Gray Thomas
// Version     : 0.0
// Copyright   : All rights reserved
// Description : This ought to solve my homework problem.
//============================================================================

#include <iostream>
#include <fstream>
#include <Eigen/Dense>
#include <sys/time.h>
#include <vector>
#include <string>
#include "PerceptronData.hpp"
#include "PerceptronWeights.hpp"
#include "PerformanceTimer.hpp"
#include "PerceptronGradientBackPropogator.hpp"
#include "PerceptronTrainer.hpp"
#include <stdlib.h> 
// std::cout<<"hello root namespace"<<std::endl;
// std::cout << getenv ("GRAY_UTILS_ROOT");
#define DATA_DIR "/SimplePerceptronBackpropogationDemo/src/main/data/"
#define TEST_PATTERNS_FILE_NAME "SimplePerceptronBackpropogationDemo/src/main/data/patterns.tst"
#define TRAINING_PATTERNS_FILE_NAME "SimplePerceptronBackpropogationDemo/src/main/data/patterns.trn"
#define LEARNING_DATA_FILE_NAME "SimplePerceptronBackpropogationDemo/src/main/data/learning.dat"
#define NETWORK_WEIGHTS_FILE_NAME "SimplePerceptronBackpropogationDemo/src/main/data/network.wts"


using namespace std;
using namespace Eigen;

void demonstrateSomeSimpleEigenLibraryFunctions(void)
{
	Matrix3d a;
	Matrix3d b;
	typedef Matrix<double, Dynamic, Dynamic, 0, 3, 9> MyMatrixXd;
	MyMatrixXd dynamicTestMatrix1(3, 3);
	a << 1, 2, 3, 4, 5, 6, 7, 8, 9;
	dynamicTestMatrix1 = a;
	for (int i = 0; i < 1000; i++)
	{
		b = a;
		dynamicTestMatrix1.resize(3, 3);
		b = dynamicTestMatrix1;
		dynamicTestMatrix1.resize(1, 9);
		a.transposeInPlace();
	}

	cout << "Matrix B:" << endl << b << endl;
	cout << "Dynamic test matrix 1 has " << dynamicTestMatrix1.rows();
	cout << " rows and " << dynamicTestMatrix1.cols();
	cout << " columns and is of total size " << dynamicTestMatrix1.size();
	cout << endl;
}

void generateRandomNetwork(VectorOverInputAndLayers<unsigned>& layerSizes, string name)
{
	default_random_engine engine(1234234987);
	PerceptronWeights randomWeights(layerSizes, engine);
	ofstream outputFile;
	outputFile.open(name);
	outputFile << randomWeights;
	outputFile.close();
}

void generateRandomData(unsigned inputs, unsigned outputs, unsigned dataSize, vector<double> (*func)(vector<double>), string name)
{
	string input = "Input.dat";
	string output = "Output.dat";
	string tst = ".tst";
	string trn = ".trn";
	ofstream insDataFile, outsDataFile, tstDataFile, trnDataFile;
	insDataFile.open(string(getenv ("GRAY_UTILS_ROOT"))+DATA_DIR + name + input);
	outsDataFile.open(string(getenv ("GRAY_UTILS_ROOT"))+DATA_DIR + name + output);
	tstDataFile.open(string(getenv ("GRAY_UTILS_ROOT"))+DATA_DIR + name + tst);
	trnDataFile.open(string(getenv ("GRAY_UTILS_ROOT"))+DATA_DIR + name + trn);

	tstDataFile << inputs << " " << outputs << endl;
	trnDataFile << inputs << " " << outputs << endl;

	default_random_engine engine(1234234987);
	uniform_real_distribution<double> dist(-2, 2);
	normal_distribution<double> noise(0.0, 0.5);
	vector<double> inputsVector;
	inputsVector.resize(inputs);

	for (unsigned d = 0; d < dataSize; d++)
	{
		for (unsigned i = 0; i < inputs; i++)
		{
			double inDatum = dist(engine);
			inputsVector[i] = inDatum;
			trnDataFile << inDatum << " ";
			insDataFile << inDatum << " ";
		}
		vector<double> outsVectorTrn = func(inputsVector);
		for (unsigned o = 0; o < outputs; o++)
		{
			double outDatum = outsVectorTrn[o] + noise(engine);
			trnDataFile << outDatum << " ";
			outsDataFile << outDatum << " ";
		}
		trnDataFile << endl;
		insDataFile << endl;
		outsDataFile << endl;

		for (unsigned i = 0; i < inputs; i++)
		{
			double inDatum = dist(engine);
			inputsVector[i] = inDatum;
			tstDataFile << inDatum << " ";
			insDataFile << inDatum << " ";
		}
		vector<double> outsVectorTest = func(inputsVector);
		for (unsigned o = 0; o < outputs; o++)
		{
			double outDatum = outsVectorTest[o] + noise(engine);
			tstDataFile << outDatum << " ";
			outsDataFile << outDatum << " ";
		}
		tstDataFile << endl;
		insDataFile << endl;
		outsDataFile << endl;
	}

	insDataFile.close();
	outsDataFile.close();
	tstDataFile.close();
	trnDataFile.close();
}

vector<double> func1(vector<double> input)
{
	double y;
	double x = input[0];
	y = 3 * sin(2 * x - 3 * x * x) + exp((x - 1) * (x + 1));
	vector<double> output;
	output.push_back(y);
	return output;
}

vector<double> func2(vector<double> input)
{
	double x, y, z;
	x = input[0];
	y = input[1];
	z = x * x + y * y <= 1.0 ? 1.0 : 0.0;
	vector<double> output;
	output.push_back(z);
	return output;
}

vector<double> func3(vector<double> input)
{
	double x, y;
	x = input[0];
	y = 10 * x;
	vector<double> ret;
	ret.push_back(y);
	return ret;
}

void trainNetwork(string dataName, vector<unsigned> layerSizes, unsigned maxIter, double learningRate)
{
	std::cout<<"hey"<<std::endl;
	default_random_engine engine(123423123451234);
	PerceptronWeights startingWeights(layerSizes, engine);
	ifstream patternsTrain, patternsTest;
	patternsTrain.open(string(getenv ("GRAY_UTILS_ROOT"))+DATA_DIR + dataName + ".trn");
	patternsTest.open(string(getenv ("GRAY_UTILS_ROOT"))+DATA_DIR + dataName + ".tst");
	cout <<string(getenv ("GRAY_UTILS_ROOT"))+DATA_DIR + dataName + ".tst" << endl;
	SimpleDataSet trainingSet, validationSet;
	cout << patternsTrain.is_open()<<endl;
	trainingSet << patternsTrain;
	validationSet << patternsTest;
	cout << trainingSet.data.size()<<endl;
	cout << "---------------pointer is here----------------"<<endl;


	PerceptronTrainer trainer(maxIter);
	string file = string(getenv ("GRAY_UTILS_ROOT"))+DATA_DIR + dataName + "learning.dat";
	cout << "before training" << endl << startingWeights << endl;

	trainer.trainPerceptron(startingWeights, trainingSet, validationSet, file, learningRate);
	cout << "after training" << endl << startingWeights << endl;
	vector<vector<double> > inputGrid;
	int numVals = 1000;
	for (unsigned i = 0; i < layerSizes[0]; i++)
	{
		{
			vector<double> inputRow;
			for (double i1 = -2.0; i1 <= 2.0; i1 += (4.0 / numVals))
				inputRow.push_back(i1);
			inputGrid.push_back(inputRow);
		}
	}

	trainer.plotPerceptron(startingWeights, string(getenv ("GRAY_UTILS_ROOT"))+DATA_DIR + dataName + "approximator", inputGrid);
	patternsTrain.close();
	patternsTest.close();

}

int main()
{
	std::cout<<"Hello world!"<<std::endl;
	std::cout<<"grayutils is at "<< getenv ("GRAY_UTILS_ROOT")<<endl;
	PerformanceTimer performanceTimer;
	performanceTimer.startTimer();

	generateRandomData(1, 1, 100, func3, "test3Data");
	vector<unsigned> size3;
	size3.push_back(1);
	size3.push_back(5);
	size3.push_back(1);
	trainNetwork("test3Data", size3, 10, 0.001);

	generateRandomData(1, 1, 200, func1, "test1Data");
	vector<unsigned> size1;
	size1.push_back(1);
	size1.push_back(30);
	size1.push_back(1);
	trainNetwork("test1Data", size1, 10, 0.05);

	cout << "done with number 1"<<endl;
	generateRandomData(2, 1, 1000, func2, "test2Data");
	vector<unsigned> size2;
	size2.push_back(2);
	size2.push_back(4);
	size2.push_back(1);

	trainNetwork("test2Data", size2, 1000, 0.05);
	cout<<"done!"<<endl;
	return 0;

	ifstream patternsTrain;
	ifstream patternsTest;
	ofstream learningDat;
	ifstream networkWts;
	char networkWeightsName[] = NETWORK_WEIGHTS_FILE_NAME;
	networkWts.open(networkWeightsName, ios::in);
	PerceptronWeights networkWeights;
	networkWeights << networkWts;
	cout << networkWeights;

	char patternsTestName[] = TEST_PATTERNS_FILE_NAME;
	patternsTest.open(patternsTestName, ios::in);
	SimpleDataSet testDataSet;
	testDataSet << patternsTest;
	cout << testDataSet;

	PerceptronWeights gradient(networkWeights.layerSizes);
	PerceptronGradientBackPropogator backPropogator(networkWeights.layerSizes);
	cout << "error is " << backPropogator.calculateError(networkWeights, testDataSet.data[0]) << endl;
	backPropogator.calculateErrorAndGradient(networkWeights, testDataSet.data[0]);
	backPropogator.packGradient(gradient);
	cout << "gradient is " << gradient << endl;

	demonstrateSomeSimpleEigenLibraryFunctions();
	performanceTimer.endTimer();

	cout << "Hello World!!! my weights are stored in the file " << NETWORK_WEIGHTS_FILE_NAME << endl;

	cout << "process took " << performanceTimer.getTime() << " seconds" << endl;
	return 0;
}
