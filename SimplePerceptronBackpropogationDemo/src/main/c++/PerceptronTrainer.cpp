/*
 * PerceptronTrainer.cpp
 *
 *    Created on: Oct 7, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include <PerceptronTrainer.hpp>
#include <vector>
#include <PerceptronWeights.hpp>
#include <PerceptronData.hpp>
#include <PerceptronGradientBackPropogator.hpp>
#include <fstream>
#include <random>
using namespace std;

void PerceptronTrainer::trainPerceptron(PerceptronWeights& weightsToTrain, const SimpleDataSet& trainingDataSet,
										const SimpleDataSet &validationDataSet, string& file, double learningRate)
{
	cout << "about to construct a perceptron backpropogator" << endl;
	PerceptronGradientBackPropogator backPropogator(weightsToTrain.layerSizes);
	cout << "perceptron backpropogator created." << endl;
	ofstream outputFile;
	outputFile.open(file);
	cout << "opening "+file<<endl;
	// cout << trainingDataSet.data[0].inputs[0];
	epoc = 0;
	double eta = -learningRate;
	double additiveNoise = 2e-4; // noise floor around error = 20 with 1e-3;
	default_random_engine rand(123423);
	double trainError = 0;
	double testError = 0;
	double eliteError = 1e99;
	PerceptronWeights eliteWeights(weightsToTrain.layerSizes);
	do
	{
		subEpoc = 0;
		do
		{
			trainError = 0;
			testError = 0;
			PerceptronWeights gradient(weightsToTrain.layerSizes);
			PerceptronWeights tempGradient(weightsToTrain.layerSizes);
			PerceptronWeights additiveWeightNoise(weightsToTrain.layerSizes, rand);

			for (unsigned d = 0; d < validationDataSet.data.size(); d++)
			{
				trainError += backPropogator.calculateErrorAndGradient(weightsToTrain, trainingDataSet.data[d]);
				backPropogator.packGradient(tempGradient);
				gradient += tempGradient;

				testError += backPropogator.calculateError(weightsToTrain, validationDataSet.data[d]);
			}
			trainError /= validationDataSet.data.size();
			testError /= validationDataSet.data.size();
			gradient * (1.0 / validationDataSet.data.size());
			outputFile << epoc << " " << trainError << " " << testError << endl;
			if (trainError < eliteError)
			{
				cout << "new best error! : " << trainError << endl;
				eliteWeights * 0;
				eliteWeights += weightsToTrain;
				eliteError = trainError;
			}

			weightsToTrain += (eta) * gradient;
			weightsToTrain += (additiveNoise * sqrt(trainError)) * additiveWeightNoise;
			trainerMemory.errorHistory.push_back(trainError);
			trainerMemory.validationHistory.push_back(testError);
			epoc++;
			subEpoc++;
		} while (!testEndCondition(epoc, trainError, testError));
		cout << "starting new network" << endl;
		PerceptronWeights randomWeights(weightsToTrain.layerSizes, rand);
		weightsToTrain * 0;
		weightsToTrain += randomWeights;
	} while (epoc < maxIter);

	weightsToTrain * 0;
	weightsToTrain += eliteWeights;
//	outputFile << "bestWeights: error" << eliteError << endl << eliteWeights;
	outputFile.close();
	cout << "before: weightsToTrain" << weightsToTrain << endl;
	cout << "before: trainingDataSet" << trainingDataSet.data.size() << endl;
	cout << "before: validationDataSet" << validationDataSet.data.size() << endl;

	return;
}

void PerceptronTrainer::initializeTrainerData(const SimpleDataSet& dataSet)
{
}

double PerceptronTrainer::testPerceptron(string fileLocationForWeights, string fileLocationForTestingDataSet)
{
	ifstream weightFile;
	ifstream dataFile;

	weightFile.open(fileLocationForWeights);
	dataFile.open(fileLocationForTestingDataSet);

	PerceptronWeights weights;
	SimpleDataSet dataSet;
	weights << weightFile;
	dataSet << dataFile;
	double totalError = 0;
	PerceptronGradientBackPropogator backPropogator(weights.layerSizes);
	for (unsigned i = 0; i < dataSet.data.size(); i++)
	{
		totalError += backPropogator.calculateError(weights, dataSet.data[i]);
	}

	totalError *= (1.0 / dataSet.data.size());

	weightFile.close();
	dataFile.close();
	return totalError;
}
void PerceptronTrainer::plotPerceptron(PerceptronWeights &weightsToPlot, string fileLocationForPlotData,
										vector<vector<double> > &inputGrid)
{
	ofstream plotInputData;
	ofstream plotOutputData;
	plotInputData.open(fileLocationForPlotData + "ApproximatorInputs.dat");
	plotOutputData.open(fileLocationForPlotData + "ApproximatorOutputs.dat");
	PerceptronGradientBackPropogator backPropogator(weightsToPlot.layerSizes);
	vector<double> inputs;
	for (unsigned i = 0; i < weightsToPlot.layerSizes[0]; i++)
		inputs.push_back(0);
	recursivePlotPerceptron(inputs, weightsToPlot, backPropogator, plotInputData, plotOutputData, inputGrid, 0);
	plotInputData.close();
	plotOutputData.close();
}

void PerceptronTrainer::recursivePlotPerceptron(vector<double> &inputs, PerceptronWeights &weightsToPlot,
												PerceptronGradientBackPropogator &backPropogator,
												ofstream& outputInputs, ofstream& outputOutputs,
												vector<vector<double> > &inputGrid, unsigned index)
{
	for (unsigned i = 0; i < inputGrid[index].size(); i++)
	{
		inputs[index] = inputGrid[index][i];
		if (index >= inputGrid.size() - 1)
		{
			TrainingDataPoint point;
			point.inputs = inputs;
			point.desiredOutputs.resize(weightsToPlot.layerSizes[weightsToPlot.layerSizes.size() - 1]);
			backPropogator.calculateError(weightsToPlot, point);
			backPropogator.packOutputs(point.desiredOutputs);
			for (unsigned in = 0; in < point.inputs.size(); in++)
				outputInputs << point.inputs[in] << " ";
			for (unsigned out = 0; out < point.desiredOutputs.size(); out++)
				outputOutputs << point.desiredOutputs[out] << " ";
			outputInputs << endl;
			outputOutputs << endl;
		}
		else
			recursivePlotPerceptron(inputs, weightsToPlot, backPropogator, outputInputs, outputOutputs, inputGrid,
					index + 1);
	}
}

bool PerceptronTrainer::testEndCondition(unsigned epoc, double trainErr, double testErr)
{
	unsigned endIndex = trainerMemory.errorHistory.size() - 1;
	unsigned numberOfEpochs = 200;
	if (subEpoc > numberOfEpochs + 5)
	{
		double errorChangePerEpoch = (-trainerMemory.errorHistory[endIndex - numberOfEpochs]
				+ trainerMemory.errorHistory[endIndex]) / numberOfEpochs;
		double errorGeometricChangePerEpoch = pow(
				trainerMemory.errorHistory[endIndex] / trainerMemory.errorHistory[endIndex - numberOfEpochs],
				1.0 / numberOfEpochs);
		double validationGeometricChangePerEpoch = pow(
				trainerMemory.validationHistory[endIndex]
						/ trainerMemory.validationHistory[endIndex - numberOfEpochs], 1.0 / numberOfEpochs);

		double validationChangePerEpoch = (-trainerMemory.validationHistory[endIndex - numberOfEpochs]
				+ trainerMemory.validationHistory[endIndex]) / numberOfEpochs;
//		cout << errorGeometricChangePerEpoch << " <=relative change " << validationGeometricChangePerEpoch
//				<< " <=validation change this epoc (averaged) " << endl;
		if (errorGeometricChangePerEpoch > 1.0 - 1e-7)
			return true;
		if (validationGeometricChangePerEpoch > 1.0)
			return true;

	}
	return epoc >= maxIter;
}

//void PerceptronTrainer::recursivePlotPerceptron(PerceptronWeights& weightsToPlot, ofstream outputInputs, ofstream outputOutputs,
//												vector<vector<double> >& inputGrid)
//{
//}
