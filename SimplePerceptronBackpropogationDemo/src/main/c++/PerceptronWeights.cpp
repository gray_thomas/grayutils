/*
 * PerceptronWeights.cpp
 *
 *    Created on: Oct 6, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include <PerceptronWeights.hpp>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <random>

using namespace std;

void packLayerSizes(PerceptronWeights &weights, ifstream &file)
{
	string unsplitLine;
	getline(file, unsplitLine);
	istringstream lineStream(unsplitLine);
	while (!lineStream.eof())
	{
		unsigned size;
		lineStream >> size;
		weights.layerSizes.push_back(size);
	}
}
void packWeights(PerceptronWeights &weights, ifstream &file)
{
	string unsplitLine;
	for (unsigned layerNumber = 1; layerNumber < weights.layerSizes.size(); layerNumber++)
	{
		unsigned layerSize = weights.layerSizes[layerNumber];
		unsigned parentLayerSize = weights.layerSizes[layerNumber - 1];

		vector<vector<double> > weightMatrix;
		weightMatrix.resize(parentLayerSize);
		for (unsigned parentLayerIndex = 0; parentLayerIndex < parentLayerSize; parentLayerIndex++)
		{
			getline(file, unsplitLine);
			istringstream lineStream(unsplitLine);
			vector<double> weightRow;
			weightRow.resize(layerSize);
			for (unsigned i = 0; i < layerSize; i++)
				lineStream >> weightRow[i];
			weightMatrix[parentLayerIndex] = weightRow;
		}
		weights.layerWeights.push_back(weightMatrix);
	}
}
void packBiases(PerceptronWeights &weights, ifstream &file)
{
	string unsplitLine;
	for (unsigned layerNumber = 1; layerNumber < weights.layerSizes.size(); layerNumber++)
	{
		getline(file, unsplitLine);
		istringstream lineStream(unsplitLine);
		unsigned layerSize = weights.layerSizes[layerNumber];
		vector<double> biases;
		biases.resize(layerSize);
		for (unsigned i = 0; i < layerSize; i++)
			lineStream >> biases[i];
		weights.layerBiases.push_back(biases);
	}
}
void operator<<(PerceptronWeights &weights, ifstream &file)
{
	packLayerSizes(weights, file);
	packWeights(weights, file);
	packBiases(weights, file);
}

void unpackLayerSizes(ostream &stream, PerceptronWeights &weights)
{
	for (unsigned i = 0; i < weights.layerSizes.size(); i++)
		stream << weights.layerSizes[i] << " ";
	stream << endl;
}
void unpackWeights(ostream &stream, PerceptronWeights &weights)
{
	for (unsigned destinationLayer = 1; destinationLayer < weights.layerSizes.size(); destinationLayer++)
	{
		for (unsigned parentLayerIndex = 0; parentLayerIndex < weights.layerSizes[destinationLayer - 1]; parentLayerIndex++)
		{
			for (unsigned destinationLayerIndex = 0; destinationLayerIndex < weights.layerSizes[destinationLayer]; destinationLayerIndex++)
			{
				stream << weights.layerWeights[destinationLayer - 1][parentLayerIndex][destinationLayerIndex] << " ";
			}
			stream << endl;
		}
	}

}
void unpackBiases(ostream &stream, PerceptronWeights &weights)
{
	for (unsigned destinationLayer = 1; destinationLayer < weights.layerSizes.size(); destinationLayer++)
	{
		for (unsigned destinationLayerIndex = 0; destinationLayerIndex < weights.layerSizes[destinationLayer]; destinationLayerIndex++)
			stream << weights.layerBiases[destinationLayer - 1][destinationLayerIndex] << " ";
		stream << endl;
	}
}

ostream & operator<<(ostream &stream, PerceptronWeights &weights)
{
	unpackLayerSizes(stream, weights);

	unpackWeights(stream, weights);
	unpackBiases(stream, weights);
	return stream;
}

PerceptronWeights::PerceptronWeights(VectorOverInputAndLayers<unsigned>& layerSizes)
{
	for (unsigned l = 0; l < layerSizes.size(); l++)
		this->layerSizes.push_back(layerSizes[l]);
	for (unsigned l = 1; l < layerSizes.size(); l++)
	{
		VectorOverLayerSize<double> bias;
		bias.resize(layerSizes[l]);
		layerBiases.push_back(bias);
	}
	for (unsigned l = 1; l < layerSizes.size(); l++)
	{
		VectorOverParentLayerSize<VectorOverLayerSize<double> > weight;
		for (unsigned p = 0; p < layerSizes[l - 1]; p++)
		{
			VectorOverLayerSize<double> weightRow;
			weightRow.resize(layerSizes[l]);
			weight.push_back(weightRow);
		}
		layerWeights.push_back(weight);
	}
}
PerceptronWeights::PerceptronWeights(VectorOverInputAndLayers<unsigned>& layerSizes, default_random_engine &engine)
{
	normal_distribution<double> dist(1.0, 1.0);

	for (unsigned l = 0; l < layerSizes.size(); l++)
		this->layerSizes.push_back(layerSizes[l]);
	for (unsigned l = 1; l < layerSizes.size(); l++)
	{
		VectorOverLayerSize<double> bias;
		bias.resize(layerSizes[l]);
		for (unsigned z = 0; z < layerSizes[l]; z++)
			bias[z] = dist(engine);
		layerBiases.push_back(bias);
	}
	for (unsigned l = 1; l < layerSizes.size(); l++)
	{
		VectorOverParentLayerSize<VectorOverLayerSize<double> > weight;
		for (unsigned p = 0; p < layerSizes[l - 1]; p++)
		{
			VectorOverLayerSize<double> weightRow;
			weightRow.resize(layerSizes[l]);
			for (unsigned z = 0; z < layerSizes[l]; z++)
				weightRow[z] = dist(engine);
			weight.push_back(weightRow);
		}
		layerWeights.push_back(weight);
	}
}

void operator<<(PerceptronWeights& output, PerceptronWeights& source)
{
	unsigned layers = source.layerSizes.size();
	for (unsigned l = 0; l < layers; l++)
		if (output.layerSizes[l] != source.layerSizes[l])
			cerr << "problem in << operator for PerceptronWeights: layer sizes do not match" << endl;

	for (unsigned l = 1; l < layers; l++)
		for (unsigned c = 0; c < source.layerSizes[l]; c++)
		{
			output.layerBiases[l - 1][c] = source.layerBiases[l - 1][c];
			for (unsigned p = 0; p < source.layerSizes[l - 1]; p++)
				output.layerWeights[l - 1][p][c] = source.layerWeights[l - 1][p][c];
		}

}

void operator+=(PerceptronWeights& output, PerceptronWeights& source)
{
	unsigned layers = source.layerSizes.size();
	for (unsigned l = 0; l < layers; l++)
		if (output.layerSizes[l] != source.layerSizes[l])
			cerr << "problem in << operator for PerceptronWeights: layer sizes do not match" << endl;

	for (unsigned l = 1; l < layers; l++)
		for (unsigned c = 0; c < source.layerSizes[l]; c++)
		{
			output.layerBiases[l - 1][c] += source.layerBiases[l - 1][c];
			for (unsigned p = 0; p < source.layerSizes[l - 1]; p++)
				output.layerWeights[l - 1][p][c] += source.layerWeights[l - 1][p][c];
		}
}

PerceptronWeights& operator *(PerceptronWeights& output, double scalar)
{
	unsigned layers = output.layerSizes.size();

	for (unsigned l = 1; l < layers; l++)
		for (unsigned c = 0; c < output.layerSizes[l]; c++)
		{
			output.layerBiases[l - 1][c] *= scalar;
			for (unsigned p = 0; p < output.layerSizes[l - 1]; p++)
				output.layerWeights[l - 1][p][c] *= scalar;
		}
	return output;
}
PerceptronWeights operator +(PerceptronWeights& val1,  PerceptronWeights& val2)
{
	PerceptronWeights returnVal(val1.layerSizes);
	returnVal+=val1;
	returnVal+=val2;
	return returnVal;
}

PerceptronWeights& operator *(double scalar, PerceptronWeights& output)
{
	return output*scalar;
}
