/*
 * VectorTypes.hpp
 *
 *    Created on: Oct 7, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef VECTORTYPES_HPP_
#define VECTORTYPES_HPP_

#include <vector>
using namespace std;

template <typename T>
using VectorOverData=vector<T>;
template<typename T>
using VectorOverOutputs=vector<T>;
template<typename T>
using VectorOverLayerSize=vector<T>;
template<typename T>
using VectorOverLayers=vector<T>;
template<typename T>
using VectorOverInputs=vector<T>;
template<typename T>
using VectorOverInputAndLayers=vector<T>;
template<typename T>
using VectorOverParentLayerSize=vector<T>;
template<typename T>
using VectorOverEpoch = vector<T>;


#endif /* VECTORTYPES_HPP_ */
