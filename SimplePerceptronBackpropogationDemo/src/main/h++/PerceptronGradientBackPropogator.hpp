/*
 * PerceptronGradientBackPropogator.hpp
 *
 *    Created on: Oct 7, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef PERCEPTRONGRADIENTBACKPROPOGATOR_HPP_
#define PERCEPTRONGRADIENTBACKPROPOGATOR_HPP_

#include <VectorTypes.hpp>
#include <PerceptronData.hpp>
#include <PerceptronWeights.hpp>


using namespace std;

struct PerceptronInternalState
{
	PerceptronInternalState(VectorOverInputAndLayers<unsigned>& layerSizes);
	unsigned inputLayerIndex;
	VectorOverLayers<VectorOverLayerSize<double> > layerAccumulator;
	VectorOverInputAndLayers<VectorOverLayerSize<double> > layerOutput;
	unsigned outputLayerIndex;
	VectorOverOutputs<double> errorVector;
};

class PerceptronGradientBackPropogator
{
public:
	PerceptronGradientBackPropogator(VectorOverInputAndLayers<unsigned> &layerSizes);
	void calculateOutputs(const PerceptronWeights &currentWeights, const TrainingDataPoint &dataPoint, VectorOverOutputs<double> &outputs );
	double calculateError(const PerceptronWeights &currentWeights, const TrainingDataPoint &dataPoint);
	double calculateErrorAndGradient(const PerceptronWeights &currentWeights, const TrainingDataPoint &dataPoint);
	double calculateErrorGradientAndJacobian(const PerceptronWeights &currentWeights, const TrainingDataPoint &dataPoint);
	void packGradient(PerceptronWeights &gradientToPack);
	void packJacobian(VectorOverOutputs<PerceptronWeights> &jacobianToPack);
	void packOutputs(VectorOverOutputs<double> &outputsToPack);

private:
	PerceptronInternalState internalState;
	PerceptronInternalState tempPartialwrtState;
	VectorOverOutputs<PerceptronWeights> partialDerivativeOfOutputsWithRespectToWeights;
	PerceptronWeights partialDerivativeOfErrorWithRespectToWeights;
};

#endif /* PERCEPTRONGRADIENTBACKPROPOGATOR_HPP_ */
