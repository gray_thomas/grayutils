/*
 * PerceptronData.hpp
 *
 *    Created on: Oct 6, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef PERCEPTRONDATA_HPP_
#define PERCEPTRONDATA_HPP_


#include <iostream>
#include <string>
#include <vector>

using namespace std;

struct TrainingDataPoint
{
	vector<double> inputs;
	vector<double> desiredOutputs;
};
struct SimpleDataSet
{
	vector<TrainingDataPoint> data;
};

void operator<<(TrainingDataPoint &dataPoint, string &line);

void operator<<(SimpleDataSet &dataSet, ifstream &file);

ostream & operator<<(ostream &stream, TrainingDataPoint &dataPoint);

ostream & operator<<(ostream &stream, SimpleDataSet &dataSet);

#endif /* PERCEPTRONDATA_HPP_ */
