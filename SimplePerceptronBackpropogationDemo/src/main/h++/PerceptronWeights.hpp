/*
 * PerceptronWeights.hpp
 *
 *    Created on: Oct 6, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef PERCEPTRONWEIGHTS_HPP_
#define PERCEPTRONWEIGHTS_HPP_

#include <VectorTypes.hpp>
#include <iostream>
#include <random>

using namespace std;

struct PerceptronWeights
{
	PerceptronWeights(void){}
	PerceptronWeights(VectorOverInputAndLayers<unsigned>& layerSizes);
	PerceptronWeights(VectorOverInputAndLayers<unsigned>& layerSizes,default_random_engine &engine);
	VectorOverInputAndLayers<unsigned> layerSizes;
	VectorOverLayers<VectorOverParentLayerSize<VectorOverLayerSize<double> > > layerWeights;
	VectorOverLayers<VectorOverLayerSize<double> > layerBiases;
};
void operator<<(PerceptronWeights &weights, ifstream &file);
ostream & operator<<(ostream &stream, PerceptronWeights &weights);
void operator<<(PerceptronWeights &output,PerceptronWeights &source);
void operator+=(PerceptronWeights &output,PerceptronWeights &source);
PerceptronWeights operator+(PerceptronWeights &val1,PerceptronWeights &val2);
PerceptronWeights& operator*(PerceptronWeights &output,double scalar);
PerceptronWeights& operator*(double scalar,PerceptronWeights &output);

#endif /* PERCEPTRONWEIGHTS_HPP_ */
