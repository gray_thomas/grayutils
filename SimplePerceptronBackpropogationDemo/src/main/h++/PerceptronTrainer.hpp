/*
 * PerceptronTrainer.hpp
 *
 *    Created on: Oct 7, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef PERCEPTRONTRAINER_HPP_
#define PERCEPTRONTRAINER_HPP_

#include <PerceptronGradientBackPropogator.hpp>
#include <PerceptronWeights.hpp>
#include <VectorTypes.hpp>
#include <string>

struct SimpleDataSet;

using namespace std;

struct PerceptronTrainerData
{
	VectorOverEpoch<double> errorHistory;
	VectorOverEpoch<double> validationHistory;
};
class PerceptronTrainer
{
public:
	PerceptronTrainer(unsigned maxIter) :
				maxIter(maxIter)
	{
	}
	void trainPerceptron(PerceptronWeights &weightsToTrain, const SimpleDataSet &trainingDataSet,
							const SimpleDataSet &validationDataSet, string& file, double learningRate);
	double testPerceptron(string fileLocationForWeights, string fileLocationForTestingDataSet);
	void plotPerceptron(PerceptronWeights &weightsToPlot, string fileLocationForPlotData,
						vector<vector<double> > &inputGrid);
private:
	void initializeTrainerData(const SimpleDataSet &dataSet);
	bool testEndCondition(unsigned epoc, double trainErr, double testErr);
	void recursivePlotPerceptron(vector<double> &inputs, PerceptronWeights &weightsToPlot,
									PerceptronGradientBackPropogator &backPropogator, ofstream& outputInputs,
									ofstream& outputOutputs, vector<vector<double> > &inputGrid, unsigned index);
	PerceptronTrainerData trainerMemory;
	unsigned maxIter;
	unsigned epoc;
	unsigned subEpoc;
};
#endif /* PERCEPTRONTRAINER_HPP_ */
