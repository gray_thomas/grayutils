


#!/usr/bin/env python
"""
Compute the cross spectral density of two signals
"""
import numpy as np
from math import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm



plt.show()

# make a little extra space between the subplots
inputData = np.fromfile("test2DataInput.dat", sep=" ")
outputData = np.fromfile("test2DataOutput.dat", sep=" ")
testInputs = np.fromfile("test2DataapproximatorApproximatorInputs.dat", sep=" ")
testOutputs = np.fromfile("test2DataapproximatorApproximatorOutputs.dat",sep=" ")



xSet = testInputs[0::2]
ySet = testInputs[1::2]
zSet = testOutputs

print len(xSet)
print len(ySet)
print len(zSet)

# plt.plot(inputData, outputData, 'b*', testInputs,testOutputs, 'r')#, t, s2, 'g-')
# plt.xlim(-2.2,2.2)
# plt.ylim(-1,2)
# plt.grid(True)

# plt.title(r'$z = (x^2+y^2<1)?1:0\;\forall x,y\in [-2,2]$', fontsize=20)
# plt.xlabel(r"""$x$""", fontsize=20)
# plt.ylabel(r'$y_1$', fontsize=20)


plt.subplots_adjust(wspace=15)

LearningResultMatrix = np.fromfile("test2Datalearning.dat", sep=" ")
epochs = LearningResultMatrix[0::3]
error = LearningResultMatrix[1::3]
validation = LearningResultMatrix[2::3]
print epochs
plt.plot(epochs, [log(e) for e in error],'b', epochs,[log(e) for e in validation],'g')#, t, s2, 'g-')
plt.xlim(0,epochs[len(epochs)-1])
# plt.ylim(0,100)
plt.grid(True)

plt.title(r'$Convergence$', fontsize=20)
plt.xlabel(r"""$Epoch$""", fontsize=20)
plt.ylabel(r'$\log(Error)$', fontsize=20)
plt.legend([r"$training$",r"$validation$"], fontsize=20)

plt.show();
fig = plt.figure()
ax = fig.gca(projection='3d')
# X, Y, Z = axes3d.get_test_data(0.05)
X= np.reshape(xSet,[1000,1000])
Y= np.reshape(ySet,[1000,1000])
Z= np.reshape(zSet,[1000,1000])
print X
ax.plot_surface(X, Y, Z, rstride=8, cstride=8, alpha=0.3)
cset = ax.contour(X, Y, Z, zdir='z', offset=-1, cmap=cm.coolwarm)
cset = ax.contour(X, Y, Z, zdir='x', offset=-2, cmap=cm.coolwarm)
cset = ax.contour(X, Y, Z, zdir='y', offset=2, cmap=cm.coolwarm)

ax.set_xlabel('X')
ax.set_xlim(-2, 2)
ax.set_ylabel('Y')
ax.set_ylim(-2, 2)
ax.set_zlabel('Z')
ax.set_zlim(-1, 2)

plt.show()