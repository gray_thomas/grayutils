


#!/usr/bin/env python
"""
Compute the cross spectral density of two signals
"""
import numpy as np
import matplotlib.pyplot as plt

# make a little extra space between the subplots
plt.subplots_adjust(wspace=0.5)
inputData = np.fromfile("test1DataInput.dat", sep=" ")
outputData = np.fromfile("test1DataOutput.dat", sep=" ")
testInputs = np.fromfile("test1DataapproximatorApproximatorInputs.dat", sep=" ")
testOutputs = np.fromfile("test1DataapproximatorApproximatorOutputs.dat",sep=" ")
dt = 0.01
t = np.arange(0, 30, dt)
nse1 = np.random.randn(len(t))                 # white noise 1
nse2 = np.random.randn(len(t))                 # white noise 2
r = np.exp(-t/0.05)

cnse1 = np.convolve(nse1, r, mode='same')*dt   # colored noise 1
cnse2 = np.convolve(nse2, r, mode='same')*dt   # colored noise 2

# two signals with a coherent part and a random part
s1 = 0.01*np.sin(2*np.pi*10*t) + cnse1
s2 = 0.01*np.sin(2*np.pi*10*t) + cnse2

# plt.subplot(211)

plt.plot(inputData, outputData, 'b*', testInputs,testOutputs, 'r')#, t, s2, 'g-')
plt.xlim(-2.2,2.2)
plt.ylim(-10,30)
plt.grid(True)

plt.title(r'$y_1 = 3 \cdot\sin(2 x - 3 x^2)+e^{(x-1)(x+1)} \;\forall x\in [-2,2]$', fontsize=20)
plt.xlabel(r"""$x$""", fontsize=20)
plt.ylabel(r'$y_1$')
plt.show()