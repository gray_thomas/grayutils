


#!/usr/bin/env python
"""
Compute the cross spectral density of two signals
"""
import numpy as np
from math import *
import matplotlib.pyplot as plt

# make a little extra space between the subplots
inputData = np.fromfile("test1DataInput.dat", sep=" ")
outputData = np.fromfile("test1DataOutput.dat", sep=" ")
testInputs = np.fromfile("test1DataapproximatorApproximatorInputs.dat", sep=" ")
testOutputs = np.fromfile("test1DataapproximatorApproximatorOutputs.dat",sep=" ")


plt.subplot(211)

plt.plot(inputData, outputData, 'b*', testInputs,testOutputs, 'r')#, t, s2, 'g-')
plt.xlim(-2.2,2.2)
plt.ylim(-10,30)
plt.grid(True)

plt.title(r'$y_1 = 3 \cdot\sin(2 x - 3 x^2)+e^{(x-1)(x+1)} \;\forall x\in [-2,2]$', fontsize=20)
plt.xlabel(r"""$x$""", fontsize=20)
plt.ylabel(r'$y_1$', fontsize=20)


plt.subplot(212)
plt.subplots_adjust(wspace=15)

LearningResultMatrix = np.fromfile("test1Datalearning.dat", sep=" ")
epochs = LearningResultMatrix[0::3]
error = LearningResultMatrix[1::3]
validation = LearningResultMatrix[2::3]
print epochs
plt.plot(epochs, [log(e) for e in error],'b', epochs,[log(e) for e in validation],'g')#, t, s2, 'g-')
plt.xlim(0,epochs[len(epochs)-1])
# plt.ylim(0,100)
plt.grid(True)

plt.title(r'$Convergence$', fontsize=20)
plt.xlabel(r"""$Epoch$""", fontsize=20)
plt.ylabel(r'$\log(Error)$', fontsize=20)
plt.legend([r"$training$",r"$validation$"], fontsize=20)

plt.show()