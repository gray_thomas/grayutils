#include <iostream>
#include <LTIStateSpaceCompensator.hpp>
#include <Eigen/Dense>
int main(void)
{
	std::cout << "hello world!" << std::endl;
	gu::linearControls::LTIStateSpaceCompensator lowPassFilter;
	lowPassFilter.setup(1, 1, 1);
	lowPassFilter.getA() << -0.5;
	lowPassFilter.getB() << 0.5;
	lowPassFilter.getC() << 1;
	lowPassFilter.getD() << 0;
	lowPassFilter.resetDiscreteApproximationsFancy(1.0, 1);

	std::cout << "low pass filter is " << lowPassFilter.seeState() << std::endl;
	Eigen::VectorXd inputs(1);
	inputs << 1.0;
	for (int i = 0; i < 10; i++)
	{
		VectorXd outputs = lowPassFilter.update(inputs);
		std::cout << "output is " << outputs[0];
		std::cout << std::endl;
	}
	return 0;
}
