/*
 * Oscillator.cpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "Oscillator.hpp"
#include <cmath>
#define PI 3.14159265359
namespace gu
{
namespace linearControls
{
Oscillator::Oscillator(double dt) :
			dT(dt),
			value(0),
			frequency(0)
{
}
double Oscillator::getNext()
{
	value += dT * frequency;
	if (value >= 1.0)
		value -= 1.0;
	if (value > 1.0)
		cerr << "overflow=" << value << endl;
	return value;
}
double oscTriangle(double value)
{
	return (value < 0.5) ? (2.0 * value) : (2.0 - 2.0 * value);
}
double oscSine(double value)
{
	return std::sin((double) value * 2 * PI);
}
double oscMappedSine(double value, double min, double max)
{
	return 0.5 * (max + min) + 0.5*oscSine(value) * (max - min);
}
double oscMappedTriangle(double value, double min, double max)
{
	return min + (max - min) * oscTriangle(value);
}
}
}
