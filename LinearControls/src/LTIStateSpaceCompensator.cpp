/*
 * LTIStateSpaceCompensator.nputcpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "LTIStateSpaceCompensator.hpp"
#include <iostream>
#include <Eigen/Dense>
using namespace std;
using namespace Eigen;
namespace gu
{
namespace linearControls
{
LTIStateSpaceCompensator::LTIStateSpaceCompensator() :
			A(),
			B(),
			C(),
			D(),
			PhiADt(),
			PhiBDt(),
			state(),
			dt(0)
{
}
LTIStateSpaceCompensator::LTIStateSpaceCompensator(const LTIStateSpaceCompensator& G) :
			A(G.A),
			B(G.B),
			C(G.C),
			D(G.D),
			state(G.state),
			PhiADt(G.PhiADt),
			PhiBDt(G.PhiBDt),
			dt(G.dt)
{
}
void LTIStateSpaceCompensator::setup(unsigned states, unsigned inputs, unsigned outputs)
{
	A.setZero(states, states);
	B.setZero(states, inputs);
	C.setZero(outputs, states);
	D.setZero(outputs, inputs);
	PhiADt.setIdentity(states, states);
	PhiBDt.setZero(states, inputs);
	state.setZero(states);
}

void LTIStateSpaceCompensator::resetDiscreteApproximations(double dt)
{
	this->dt = dt;
	PhiADt.setIdentity();
	PhiADt += A * dt;
	PhiBDt.setZero();
	PhiBDt += B * dt;
	cout << "LTI Comp: PhiA" << PhiADt << endl;
}
void LTIStateSpaceCompensator::resetDiscreteApproximationsFancy(double dt, unsigned iterations)
{
	this->dt = dt;
	// Approximates Matrix Exponent using Euler integration.
	PhiADt.setIdentity();
	PhiBDt.setZero();
	double newdt = dt / iterations;
	for (unsigned i = 0; i < iterations; i++)
	{
		PhiADt += A * PhiADt * newdt;
		PhiBDt += (A * PhiBDt + B) * newdt;
	}
	cout << "LTI Comp: PhiA:\n" << PhiADt << "\nPhiB:\n" << PhiBDt << "\nC:\n" << C << "\nD:\n" << D << endl;
	cout << "LTI Comp has these eigenvalues: " << A.eigenvalues().transpose();
	cout << endl;
}
MatrixXd& LTIStateSpaceCompensator::getA()
{
	return A;
}
MatrixXd& LTIStateSpaceCompensator::getB()
{
	return B;
}
MatrixXd& LTIStateSpaceCompensator::getC()
{
	return C;
}
MatrixXd& LTIStateSpaceCompensator::getD()
{
	return D;
}
void LTIStateSpaceCompensator::setState(VectorXd state)
{
	this->state = state;
}
VectorXd LTIStateSpaceCompensator::update(VectorXd inputs)
{
	state = PhiADt * state + PhiBDt * inputs;
	VectorXd outputs;
	outputs = C * state + D * inputs;
	return outputs;
}

//LTIStateSpaceCompensator pad(LTIStateSpaceCompensator& G)
//{
//	LTIStateSpaceCompensator ret;
//	ret.setup(G.getA().rows(), 2*G.getB().cols(), 2*G.getC().rows());
//	ret.getA()<<G.getA();
//	ret.getB().block(0,0,G.getB().rows(),G.getB().cols())<<G.getB();
//	ret.getC()<<G.getC(),G.getC();
//}
std::ostream& operator<<(std::ostream& out, const LTIStateSpaceCompensator& G)
{
	MatrixXd Full(G.seeA().rows() + G.seeC().rows(), G.seeA().cols() + G.seeB().cols());
	Full << G.seeA(), G.seeB(), G.seeC(), G.seeD();
	out << Full;
	return out;
}
LTIStateSpaceCompensator operator*(double scale, const LTIStateSpaceCompensator& compensator)
{
	LTIStateSpaceCompensator ret(compensator);
	ret.getC() *= scale;
	ret.getD() *= scale;
	return ret;
}
LTIStateSpaceCompensator operator*(const LTIStateSpaceCompensator& compensator, double scale)
{
	return scale * compensator;
}
LTIStateSpaceCompensator operator*(const MatrixXd& outputScale, const LTIStateSpaceCompensator& compensator)
{
	LTIStateSpaceCompensator ret(compensator);
	ret.getC() << outputScale * ret.getC();
	ret.getD() << outputScale * ret.getD();
	return ret;
}
LTIStateSpaceCompensator operator*(const LTIStateSpaceCompensator& compensator, const MatrixXd& inputScale)
{
	LTIStateSpaceCompensator ret(compensator);
	ret.getB() << ret.getB() * inputScale;
	ret.getPhiBdt() << ret.getPhiBdt() * inputScale;
	ret.getD() << ret.getD() * inputScale;
	return ret;
}
/*LTIStateSpaceCompensator tf2ss(std::vector<double> num, std::vector<double> den)
 {
 LTIStateSpaceCompensator ret;
 int ssize=den.size()-1;
 ret.setup(ssize,1,1);
 ret.getA().setZero();
 ret.getA().diagonal(1).setOnes();
 for (int i=0;i<ssize;i++)
 ret.getA()(ssize-1,i)=den[i]/den[ssize];
 ret.getB().setZero();
 ret.getB()(ssize-1,0)=1.0/den[ssize];
 ret.getD().setZero();
 ret.getC().setZero();
 for (int i=0;i<min(num.size(),ssize))
 }*/
LTIStateSpaceCompensator compensatorFeedback(const LTIStateSpaceCompensator& G, const LTIStateSpaceCompensator& K)
{
	LTIStateSpaceCompensator ret;
	MatrixXd Ap(G.seeA());
	MatrixXd Bp(G.seeB());
	MatrixXd Cp(G.seeC());
	MatrixXd Dp(G.seeD());
	MatrixXd Ak(K.seeA());
	MatrixXd Bk(K.seeB());
	MatrixXd Ck(K.seeC());
	MatrixXd Dk(K.seeD());

	MatrixXd L_A(Ap.rows() + Ak.rows(), Ap.cols() + Ak.cols());
	MatrixXd L_B1(L_A.rows(), Bk.cols());
	MatrixXd L_B2(L_A.rows(), Bk.cols() + Bp.cols());
	MatrixXd L_C1(Cp.rows(), L_A.cols());
	MatrixXd L_C2(Cp.rows() + Ck.rows(), L_A.cols());
	MatrixXd L_D11(L_C1.rows(), L_B1.cols());
	MatrixXd L_D12(L_C1.rows(), L_B2.cols());
	MatrixXd L_D21(L_C2.rows(), L_B1.cols());
	MatrixXd L_D22(L_C2.rows(), L_B2.cols());
	L_A << Ap, MatrixXd::Zero(Ap.rows(), Ak.cols()), MatrixXd::Zero(Ak.rows(), Ap.cols()), Ak;
	L_B1 << MatrixXd::Zero(Ap.rows(), L_B1.cols()), Bk;
	L_B2 << MatrixXd::Zero(Bp.rows(), Bk.cols()), Bp, -Bk, MatrixXd::Zero(Bk.rows(), Bp.cols());
	L_D11.setZero();
	L_D12 << MatrixXd::Zero(Cp.rows(), Bk.cols()), Dp;
	L_D21 << MatrixXd::Zero(Cp.rows(), Bk.cols()), Dk;
	L_D22 << MatrixXd::Zero(Dp.rows(), Dk.cols()), Dp, -Dk, MatrixXd::Zero(Dk.rows(), Dp.cols());
	L_C1 << Cp, MatrixXd::Zero(Cp.rows(), Ak.cols());
	L_C2 << Cp, MatrixXd::Zero(Cp.rows(), Ck.cols()), MatrixXd::Zero(Ck.rows(), Cp.cols()), Ck;
	MatrixXd Loop(L_A.rows() + L_C1.rows() + L_C2.rows(), L_A.cols() + L_B1.cols() + L_B2.cols());
	Loop << L_A, L_B1, L_B2, L_C1, L_D11, L_D12, L_C2, L_D21, L_D22;
	cout << "Full open Loop\n" << Loop << endl;

	MatrixXd F = MatrixXd::Identity(Bk.cols() + Bp.cols(), Cp.rows() + Ck.rows());
	MatrixXd Q = MatrixXd::Identity(L_D22.rows(), F.cols()) - L_D22 * F;
	MatrixXd Fc = F * Q.inverse();
	cout << "F prime matrix\n" << Fc << endl;

	ret.setup(Ap.rows() + Ak.rows(), Bp.cols(), Cp.rows());
	ret.getA() << L_A + L_B2 * Fc * L_C2;
	ret.getB() << L_B1 + L_B2 * Fc * L_D21;
	ret.getC() << L_C1 + L_D12 * Fc * L_C2;
	ret.getD() << L_D11 + L_D12 * Fc * L_D21;
	cout << "closed loop system\n" << ret << endl;
	cout << "closed loop system has these eigenvalues: " << ret.getA().eigenvalues().transpose();
	cout << endl;

	return ret;
}
}
}

