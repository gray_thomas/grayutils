/*
 * HumeLTIFilters.cpp
 *
 *    Created on: Apr 20, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "HumeLTIFilters.hpp"
namespace gu
{
namespace linearControls
{
LTIStateSpaceCompensator setupSecondOrderLowpassDifferentiator(double frequency, double dt)
{
	LTIStateSpaceCompensator filter;
	filter.setup(2, 1, 1);
	filter.getA() << 0, 1, -frequency * frequency, -2 * frequency;
	filter.getB() << 0, frequency * frequency;
	filter.getC() << 0, 1;
	filter.getD() << 0.0;
	filter.resetDiscreteApproximationsFancy(dt, 10);
	return filter;
}

LTIStateSpaceCompensator setupFirstOrderLowpassDifferentiator(double frequency, double dt)
{
	LTIStateSpaceCompensator filter;
	filter.setup(1, 1, 1);
	filter.getA() << -frequency;
	filter.getB() << -frequency;
	filter.getC() << 1;
	filter.getD() << 1;
	filter.resetDiscreteApproximationsFancy(dt, 10);
	return filter;
}

LTIStateSpaceCompensator setupFirstOrderLowpass(double frequency, double dt)
{
	LTIStateSpaceCompensator filter;
	filter.setup(1, 1, 1);
	filter.getA() << -frequency;
	filter.getB() << frequency;
	filter.getC() << 1;
	filter.getD() << 0;
	filter.resetDiscreteApproximationsFancy(dt, 10);
	return filter;
}

LTIStateSpaceCompensator setupDoubleDifferentiator(double freq, double dt)
{
	LTIStateSpaceCompensator filter;
	filter.setup(2, 1, 2);
	filter.getA() << 0, 1, -freq * freq, -2 * freq;
	filter.getB() << 0, freq * freq;
	filter.getC() << 0, 1, -freq * freq, -2 * freq;
	filter.getD() << 0, 1;
	filter.resetDiscreteApproximationsFancy(dt, 100);
	return filter;
}
LTIStateSpaceCompensator setup2State2Diff(double freq, double dt)
{
	LTIStateSpaceCompensator filter;
	filter.setup(2, 1, 3);
	filter.getA() << 0, 1, -freq * freq, -2 * freq;
	filter.getB() << 0, freq * freq;
	filter.getC() << 1, 0, 0, 1, -freq * freq, -2 * freq;
	filter.getD() << 0, 0, 1;
	filter.resetDiscreteApproximationsFancy(dt, 100);
	return filter;
}
LTIStateSpaceCompensator setup3rdOrderButterworthDifferentiator(double freq, double dt)
{
	LTIStateSpaceCompensator filter;
	filter.setup(3, 1, 3);
	filter.getA() << 0, 1, 0, 0, 0, 1, -freq * freq * freq, -2 * freq * freq, -2 * freq;
	filter.getB() << 0, 0, freq * freq * freq;
	filter.getC() << 1, 0, 0, 0, 1, 0, 0, 0, 1;
	filter.getD() << 0, 0, 0;
	filter.resetDiscreteApproximationsFancy(dt, 100);
	return filter;
}

LTIStateSpaceCompensator setupLead(double gain, double pole, double zero, double dt)
{
	LTIStateSpaceCompensator filter;
	filter.setup(1, 1, 1);
	filter.getA() << -pole;
	filter.getB() << gain * pole / zero;
	filter.getC() << zero + -pole;
	filter.getD() << 0 + gain * pole / zero;
	filter.resetDiscreteApproximationsFancy(dt, 100);
	return filter;
}
LTIStateSpaceCompensator setupInfiniteGainLead(double crossoverFrequency, double poleZeroSpread,
												double crossoverGain, double dt)
{
	LTIStateSpaceCompensator filter;
	double f = sqrt(poleZeroSpread);
	double w = crossoverFrequency;
	double pole = crossoverFrequency * f;
	double zero = crossoverFrequency / f;
	double f2 = pow(f, 2);
	double k = w * f2 * (f2 + pow(1 - f2, 2)) / (sqrt(pow(2 * f * (1 - f2), 2) + pow(f - pow(1 - f2, 2), 2)));
	filter.setup(3, 1, 1);
	filter.getA() << 0, 1, 0, 0, 0, 1, 0, -pow(pole, 2), -pole;
	filter.getB() << 0, 0, k;
	filter.getC() << pow(zero, 2), zero, 1.0;
	filter.getD() << 0.0;
	filter.resetDiscreteApproximationsFancy(dt, 100);
	return filter;
}

}
}
