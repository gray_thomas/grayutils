/*
 * LTIStateSpaceCompensator.hpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef LTISTATESPACECOMPENSATOR_HPP_
#define LTISTATESPACECOMPENSATOR_HPP_
#include <Eigen/Dense>
#include <iostream>
#include <vector>
using namespace Eigen;
namespace gu
{
namespace linearControls
{
class LTIStateSpaceCompensator
{
	MatrixXd A;
	MatrixXd B;
	MatrixXd C;
	MatrixXd D;
	MatrixXd PhiADt;
	MatrixXd PhiBDt;
	VectorXd state;
	double dt;
public:
	LTIStateSpaceCompensator();
	LTIStateSpaceCompensator(const LTIStateSpaceCompensator& G);
	void setup(unsigned states, unsigned inputs, unsigned outputs);
	MatrixXd& getA();
	MatrixXd& getB();
	MatrixXd& getC();
	MatrixXd& getD();
	double getDt(){return dt;}
	MatrixXd& getPhiADt(){return PhiADt;}
	MatrixXd& getPhiBdt(){return PhiBDt;}
	const MatrixXd& seeA() const {return A;}
	const MatrixXd& seeB() const {return B;}
	const MatrixXd& seeC() const {return C;}
	const MatrixXd& seeD() const {return D;}
	const MatrixXd& seePhiADt() const {return PhiADt;}
	const MatrixXd& seePhiBDt() const {return PhiBDt;}
	const VectorXd& seeState() const {return state;}

	void resetDiscreteApproximations(double dt);
	void resetDiscreteApproximationsFancy(double dt, unsigned iterations);
	void setState(VectorXd state);
	VectorXd update(VectorXd inputs);
};
std::ostream& operator<<(std::ostream& out, const LTIStateSpaceCompensator& compensator);
LTIStateSpaceCompensator operator*(double scale, const LTIStateSpaceCompensator& compensator);
LTIStateSpaceCompensator operator*(const LTIStateSpaceCompensator& compensator, double scale);
LTIStateSpaceCompensator operator*(MatrixXd& prescale, const LTIStateSpaceCompensator& compensator);
LTIStateSpaceCompensator operator*(const LTIStateSpaceCompensator& compensator, MatrixXd& postscale);
LTIStateSpaceCompensator compensatorFeedback(const LTIStateSpaceCompensator& G, const LTIStateSpaceCompensator& K);
//LTIStateSpaceCompensator tf2ss(std::vector<double> num, std::vector<double> den);
}
}

#endif /* LTISTATESPACECOMPENSATOR_HPP_ */
