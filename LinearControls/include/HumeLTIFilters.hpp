/*
 * HumeLTIFilters.hpp
 *
 *    Created on: Apr 20, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#ifndef HUMELTIFILTERS_HPP_
#define HUMELTIFILTERS_HPP_
#include "LTIStateSpaceCompensator.hpp"
#include "Eigen/Dense"
using namespace Eigen;
namespace gu
{
namespace linearControls
{
LTIStateSpaceCompensator setupSecondOrderLowpassDifferentiator(double frequency, double dt);
LTIStateSpaceCompensator setupFirstOrderLowpassDifferentiator(double frequency, double dt);
LTIStateSpaceCompensator setupFirstOrderLowpass(double frequency, double dt);
LTIStateSpaceCompensator setupDoubleDifferentiator(double freq, double dt);
LTIStateSpaceCompensator setup2State2Diff(double freq, double dt);
LTIStateSpaceCompensator setup3rdOrderButterworthDifferentiator(double freq, double dt);
LTIStateSpaceCompensator setupLead(double gain, double pole, double zero, double dt);
LTIStateSpaceCompensator setupInfiniteGainLead(double crossoverFrequency, double poleZeroSpread,
												double crossoverGain, double dt);
}
}

#endif /* HUMELTIFILTERS_HPP_ */
