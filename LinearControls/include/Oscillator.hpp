/*
 * Oscillator.hpp
 *
 *    Created on: Apr 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef OSCILLATOR_HPP_
#define OSCILLATOR_HPP_
#include <iostream>
using namespace std;
namespace gu
{
namespace linearControls
{
class Oscillator
{
private:
	double dT;
public:
	Oscillator(double dt);
	double value;
	double frequency;
	double getNext();
};
double oscTriangle(double value);
double oscSine(double value);
double oscMappedSine(double value, double min, double max);
double oscMappedTriangle(double value, double min, double max);

}
}

#endif /* OSCILLATOR_HPP_ */
