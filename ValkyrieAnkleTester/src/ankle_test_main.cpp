#include <iostream>
#include "smtclient.hpp"




int main(void)
{
    std::cout << "Hello World" << std::endl;
    std::string resourceName = "/right_leg/j2/JointAPS_Angle_Rad";
    SMT::SMTClient client;
    Ankle_Actuator rlj5(client, std::string("right_leg"), std::string("j5"));
    Ankle_Actuator rlj6(client, std::string("right_leg"), std::string("j6"));
    Ankle_Actuator ankles[] = {rlj5, rlj6};
    SMT::Resource resource = client.getResource(resourceName);

    // ASSERT_EQ(resource.getType(),"float32");
    for (int i = 0; i < 2; i++)
    {
        Ankle_Actuator &ankle = ankles[i];
        ankle.set_desired_force(0.0);
    }

    float value = -32.1234;

    resource.set<float>(value);
    // ASSERT_EQ(resource.get<float>(), value);
}