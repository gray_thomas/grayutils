
#include <iostream>
#include <fstream>
#include "smtclient.hpp"
#include <Daemon.hpp>
// #define CURRENT_MODE True
#include "ValkyrieRobot.hpp"
#include "LTIStateSpaceCompensator.hpp"
#include "HumeLTIFilters.hpp"
#include "Oscillator.hpp"
#include <AggressiveTimingDaemon.hpp>
#include <chrono>
#include <cmath>

#define TIME_STEP 0.001
class PrintThread : public gu::timing::AggressiveTimingDaemon
{
    V1smt::control::ValkyrieRobot &val;
    gu::linearControls::Oscillator osc1;
    gu::linearControls::Oscillator osc2;
    gu::linearControls::Oscillator osc3;
    gu::linearControls::Oscillator osc4;
    std::ofstream outputFile;
    int i;
    std::chrono::time_point<std::chrono::high_resolution_clock> initial_time;
public:
    PrintThread(V1smt::control::ValkyrieRobot &val):
            gu::timing::AggressiveTimingDaemon(TIME_STEP),
            val(val), 
            osc1(TIME_STEP),
            osc2(TIME_STEP),
            osc3(TIME_STEP),
            osc4(TIME_STEP),
            outputFile(), 
            i(0), 
            initial_time(std::chrono::high_resolution_clock::now())
    {
        osc1.frequency = 0.01; //Hz 100 seconds per frequency sweep
        osc2.frequency = 0.1; //Hz coupled to osc1's frequency output
        osc3.frequency = 1; //Hz coupled oscillator: command5
        osc4.frequency = 1; //Hz coupled oscillator: command6
        outputFile.open("/home/gray/wk/grayutils/ValkyrieAnkleTester/data/data.new");
        outputFile << "iter, time_us, act5_torque_des, act6_torque_des";
        outputFile << ", " << "val.rightAnkle.getActuator5().getPosition()";
        outputFile << ", " << "val.rightAnkle.getActuator5().getVelocity()";
        outputFile << ", " << "val.rightAnkle.getActuator5().getEffort()";
        outputFile << ", " << "val.rightAnkle.getActuator5().getIncrementalPosition()";
        outputFile << ", " << "val.rightAnkle.getActuator5().getIncrementalVelocity()";
        outputFile << ", " << "val.rightAnkle.getActuator5().getFaults()";
        outputFile << ", " << "val.rightAnkle.getActuator5().getHeartBeat()";

        outputFile << ", " << "val.rightAnkle.getActuator6().getPosition()";
        outputFile << ", " << "val.rightAnkle.getActuator6().getVelocity()";
        outputFile << ", " << "val.rightAnkle.getActuator6().getEffort()";
        outputFile << ", " << "val.rightAnkle.getActuator6().getIncrementalPosition()";
        outputFile << ", " << "val.rightAnkle.getActuator6().getIncrementalVelocity()";
        outputFile << ", " << "val.rightAnkle.getActuator6().getFaults()";
        outputFile << ", " << "val.rightAnkle.getActuator6().getHeartBeat()";

        outputFile << ", " << "val.rightAnkle.getPitchJoint().getPosition()";
        outputFile << ", " << "val.rightAnkle.getPitchJoint().getVelocity()";
        outputFile << ", " << "val.rightAnkle.getRollJoint().getPosition()";
        outputFile << ", " << "val.rightAnkle.getRollJoint().getVelocity()";

        outputFile << ", " << "val.rightAnkle.getFootLoadcell().a1.get<float>()";
        outputFile << ", " << "val.rightAnkle.getFootLoadcell().a2.get<float>()";
        outputFile << ", " << "val.rightAnkle.getFootLoadcell().a3.get<float>()";
        outputFile << ", " << "val.rightAnkle.getFootLoadcell().a4.get<float>()";
        outputFile << ", " << "val.rightAnkle.getFootLoadcell().a5.get<float>()";
        outputFile << ", " << "val.rightAnkle.getFootLoadcell().a6.get<float>()";

        outputFile << ", " << "val.rightAnkle.getForce5().force.get<float>()";
        outputFile << ", " << "val.rightAnkle.getForce6().force.get<float>()";
        outputFile << "\n";
    }
    ~PrintThread()
    {
        outputFile.close();
    }
    virtual void agressivelyTimedLoop()
    {
        i++;
        // std::cout << val.rightAnkle.getPitchJoint().getPosition()<<"\n";
        // <Double Chirp:
        // double minFreq=4.0;//Hz
        // double maxFreq=1000.0;//Hz
        // double minCommand=-300.0; // Newtons
        // double maxCommand=300.0; // Newtons
        // double frequency = std::exp(gu::linearControls::oscMappedTriangle(osc1.getNext(), std::log(minFreq), std::log(maxFreq)));
        // osc2.frequency = 0.1 * frequency;
        // double phaseShift = osc2.getNext();
        // osc3.frequency = frequency;
        // osc4.frequency = frequency;
        // double command5 = gu::linearControls::oscMappedSine(osc3.getNext()+phaseShift, minCommand, maxCommand);
        // double command6 = gu::linearControls::oscMappedSine(osc4.getNext(), minCommand, maxCommand);
        // />

        // <Double steps
        double minFreq=1.0;//Hz
        double maxFreq=40.0;//Hz
        double minCommand=-600.0; // Newtons
        double maxCommand=600.0; // Newtons
        double frequency = std::exp(gu::linearControls::oscMappedTriangle(osc1.getNext(), std::log(minFreq), std::log(maxFreq)));
        osc2.frequency = 0.1 * frequency;
        double phaseShift = osc2.getNext();
        osc3.frequency = frequency*1.1156; // offset them slightly to desynchronize
        osc4.frequency = frequency;
        double command5 = (osc3.getNext()>0.5)? minCommand:maxCommand;
        double command6 = (osc4.getNext()>0.5)? minCommand:maxCommand;
        // />

        // command6=0.0;// Zeros the second command

        outputFile << i;
        auto time_since_start = std::chrono::high_resolution_clock::now() - initial_time;
        signed int time_ms=std::chrono::duration_cast<std::chrono::duration<signed int, std::micro>>(time_since_start).count();
        outputFile << ", " << time_ms;
        outputFile << ", " << command5;
        outputFile << ", " << command6;
        outputFile << ", " << val.rightAnkle.getActuator5().getPosition();
        outputFile << ", " << val.rightAnkle.getActuator5().getVelocity();
        outputFile << ", " << val.rightAnkle.getActuator5().getEffort();
        outputFile << ", " << val.rightAnkle.getActuator5().getIncrementalPosition();
        outputFile << ", " << val.rightAnkle.getActuator5().getIncrementalVelocity();
        outputFile << ", " << val.rightAnkle.getActuator5().getFaults();
        outputFile << ", " << val.rightAnkle.getActuator5().getHeartBeat();

        outputFile << ", " << val.rightAnkle.getActuator6().getPosition();
        outputFile << ", " << val.rightAnkle.getActuator6().getVelocity();
        outputFile << ", " << val.rightAnkle.getActuator6().getEffort();
        outputFile << ", " << val.rightAnkle.getActuator6().getIncrementalPosition();
        outputFile << ", " << val.rightAnkle.getActuator6().getIncrementalVelocity();
        outputFile << ", " << val.rightAnkle.getActuator6().getFaults();
        outputFile << ", " << val.rightAnkle.getActuator6().getHeartBeat();

        outputFile << ", " << val.rightAnkle.getPitchJoint().getPosition();
        outputFile << ", " << val.rightAnkle.getPitchJoint().getVelocity();
        outputFile << ", " << val.rightAnkle.getRollJoint().getPosition();
        outputFile << ", " << val.rightAnkle.getRollJoint().getVelocity();

        outputFile << ", " << val.rightAnkle.getFootLoadcell().a1.get<int>();
        outputFile << ", " << val.rightAnkle.getFootLoadcell().a2.get<int>();
        outputFile << ", " << val.rightAnkle.getFootLoadcell().a3.get<int>();
        outputFile << ", " << val.rightAnkle.getFootLoadcell().a4.get<int>();
        outputFile << ", " << val.rightAnkle.getFootLoadcell().a5.get<int>();
        outputFile << ", " << val.rightAnkle.getFootLoadcell().a6.get<int>();

        outputFile << ", " << val.rightAnkle.getForce5().force.get<float>();
        outputFile << ", " << val.rightAnkle.getForce6().force.get<float>();

        outputFile << "\n";
        std::cout << command5<< ", " << command6 << "\n";


        val.rightAnkle.getActuator5().setControl(command5);
        val.rightAnkle.getActuator6().setControl(command6);
        // sleep(TIME_STEP);
    }
};
class SimThread : public gu::threads::Daemon
{
    V1smt::control::ValkyrieRobot &val;
public:
    SimThread(V1smt::control::ValkyrieRobot &val):
        val(val)  {}
    virtual void loop()
    {
        std::cout << val.rightAnkle.getPitchJoint().getPosition() << "\n";

        sleep(TIME_STEP);
    }
};



int main(void)
{
    std::cout << "Hello World" << std::endl;
    V1smt::control::ValkyrieRobot val;

    PrintThread howdyA(val);
    // HowdyThread howdyB;
    howdyA.start();
    sleep(0.5);
    // howdyB.start();


    float value = -32.1234;

    pause();
    // ASSERT_EQ(resource.get<float>(), value);
}