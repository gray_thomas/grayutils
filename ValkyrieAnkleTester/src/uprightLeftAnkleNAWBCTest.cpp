
#include <iostream>
#include <fstream>
#include "smtclient.hpp"
#include <Daemon.hpp>
// #define CURRENT_MODE True
#include "ValkyrieRobot.hpp"
#include "LTIStateSpaceCompensator.hpp"
#include "HumeLTIFilters.hpp"
#include "Oscillator.hpp"
#include <AggressiveTimingDaemon.hpp>
#include <chrono>
#include <cmath>
#include <Eigen/Dense>
#include <iomanip>
#include <random>



#define TIME_STEP 0.001
class ControlThread : public gu::timing::AggressiveTimingDaemon
{
    V1smt::control::ValkyrieRobot &val;
    gu::linearControls::Oscillator osc1;
    gu::linearControls::Oscillator osc2;
    gu::linearControls::Oscillator osc3;
    gu::linearControls::Oscillator osc4;
    std::ofstream outputFile;
    std::default_random_engine gen;
    int i;
    std::chrono::time_point<std::chrono::high_resolution_clock> initial_time;
    double bias5;
    double bias6;
public:
    ControlThread(V1smt::control::ValkyrieRobot &val):
            gu::timing::AggressiveTimingDaemon(TIME_STEP),
            val(val),
            osc1(TIME_STEP),
            osc2(TIME_STEP),
            osc3(TIME_STEP),
            osc4(TIME_STEP),
            outputFile(),
            i(0),
            initial_time(std::chrono::high_resolution_clock::now())
    {
        osc1.frequency = 0.01; //Hz 100 seconds per frequency sweep
        osc2.frequency = 0.2; //Hz
        osc3.frequency = 0.3; //Hz
        osc4.frequency = 1; //Hz
        outputFile.open("/home/gray/wk/grayutils/ValkyrieAnkleTester/data/data.new");
        outputFile << "iter, time_us, act5_torque_des, act6_torque_des";
        outputFile << ", " << "test_ankle.getActuator5().getPosition()";
        outputFile << ", " << "test_ankle.getActuator5().getVelocity()";
        outputFile << ", " << "test_ankle.getActuator5().getEffort()";
        outputFile << ", " << "test_ankle.getActuator5().getIncrementalPosition()";
        outputFile << ", " << "test_ankle.getActuator5().getIncrementalVelocity()";
        outputFile << ", " << "test_ankle.getActuator5().getFaults()";
        outputFile << ", " << "test_ankle.getActuator5().getHeartBeat()";

        outputFile << ", " << "test_ankle.getActuator6().getPosition()";
        outputFile << ", " << "test_ankle.getActuator6().getVelocity()";
        outputFile << ", " << "test_ankle.getActuator6().getEffort()";
        outputFile << ", " << "test_ankle.getActuator6().getIncrementalPosition()";
        outputFile << ", " << "test_ankle.getActuator6().getIncrementalVelocity()";
        outputFile << ", " << "test_ankle.getActuator6().getFaults()";
        outputFile << ", " << "test_ankle.getActuator6().getHeartBeat()";

        outputFile << ", " << "test_ankle.getPitchJoint().getPosition()";
        outputFile << ", " << "test_ankle.getPitchJoint().getVelocity()";
        outputFile << ", " << "test_ankle.getRollJoint().getPosition()";
        outputFile << ", " << "test_ankle.getRollJoint().getVelocity()";

        outputFile << ", " << "test_ankle.getFootLoadcell().a1.get<float>()";
        outputFile << ", " << "test_ankle.getFootLoadcell().a2.get<float>()";
        outputFile << ", " << "test_ankle.getFootLoadcell().a3.get<float>()";
        outputFile << ", " << "test_ankle.getFootLoadcell().a4.get<float>()";
        outputFile << ", " << "test_ankle.getFootLoadcell().a5.get<float>()";
        outputFile << ", " << "test_ankle.getFootLoadcell().a6.get<float>()";

        outputFile << ", " << "test_ankle.getForce5().force.get<float>()";
        outputFile << ", " << "test_ankle.getForce6().force.get<float>()";
        outputFile << "\n";
    }
    ~ControlThread()
    {
        outputFile.close();
    }
    virtual void agressivelyTimedLoop()
    {
        i++;
        double minFreq = 0.1;
        double maxFreq = 10;
        double frequency = std::exp(
            gu::linearControls::oscMappedTriangle(
                osc1.getNext(), std::log(minFreq), std::log(maxFreq)));
        osc2.frequency = 0.2*frequency; //Hz lissajous
        osc3.frequency = 0.3*frequency; //Hz
        Eigen::Vector2d des_x;
        des_x<<
            0.1*gu::linearControls::oscSine(osc2.getNext()),
            0.1*gu::linearControls::oscSine(osc3.getNext());

        auto test_ankle=val.leftAnkle;

        double pitch = test_ankle.getPitchJoint().getPosition();
        double pitch_vel = test_ankle.getPitchJoint().getVelocity();
        double roll = test_ankle.getRollJoint().getPosition();
        double roll_vel = test_ankle.getRollJoint().getVelocity();

        Eigen::Vector2d q;
        Eigen::Vector2d qd;
        q<<roll,pitch;
        qd<<roll_vel, pitch_vel;
        Eigen::Matrix2d Jsensing=Eigen::Matrix2d::Identity(); //Js
        Eigen::Matrix2d Jtorque; //Jt, comparable to Js'
         // but allowing for non-colocated actuators
        Jtorque<< 0.75, 0.75,
                   0.75, -0.75;
        Eigen::Matrix2d JtorqueInv;
        JtorqueInv<<0.75,0.75,
                    0.75, -0.75;
        Eigen::Vector2d x=Jsensing * q;
        Eigen::Vector2d xd=Jsensing * qd;
        // M is approximately m * I, m subsumed into the gains
        
        double Kdx_0 = 20.0;
        double Kdx_1 = 20.0;
        Eigen::Matrix2d Kd;
        Kd << Kdx_0, 0.0, 0.0, Kdx_1;

        double Kpx_0 = 5000;
        double Kpx_1 = 5000;
        Eigen::Matrix2d Kp;
        Kp<<Kpx_0,0.0,
            0.0,Kpx_1;

        Eigen::Matrix2d gravity_comp;
    	gravity_comp << -87.46, -106.25,
                        -71.83,  98.26;
    	Eigen::Vector2d gravity_bias;
    	gravity_bias << -6.42, -2.10;

        Eigen::Vector2d grav_est = gravity_bias+gravity_comp*q;
        double ag_factor=0.0; // anti-gravity factor, should stabilize (it does!)
        Eigen::Vector2d anti_grav = gravity_bias+(1.0+ag_factor)*gravity_comp*q;
        // account for desired postion in the anti-grav:
        anti_grav-=ag_factor*gravity_comp*des_x;//TODO: probable bug: gravity_comp is jointspace
        // this adjusts the setpoint like the anti-grav were an extra Kp element.

        // ang5=roll ang6=pitch
        // from static positioning tests
        // tau5 ~= -6.42 + -87.46 ang5 + -106.25 ang6 
        // tau6 ~= -2.10 + -71.83 ang5 + 98.26 ang6


        // Don't screw up these signs, or you risk anti-stablity
        Eigen::Vector2d des_accel = - Kp * (x-des_x) - Kd * xd;
        Eigen::Vector2d gamma_desired = JtorqueInv * des_accel;

        double command5 = anti_grav[0] + gamma_desired[0];
        double command6 = anti_grav[1] + gamma_desired[1];

        double saturation=120.0;
        if (command5 > saturation)
            command5=saturation;
        if (command5< -saturation)
            command5=-saturation;
        if (command6 > saturation)
            command6=saturation;
        if (command6 < -saturation)
            command6=-saturation;

        double tau5 = test_ankle.getActuator5().getEffort()-bias5;
        double tau6 = test_ankle.getActuator6().getEffort()-bias6;

        // command6=0.0;// Zeros the second command
        if (i%100==3)
        {
            std::cout<<"{pitch:            "<<std::setw(14)<<pitch;
            std::cout<<", roll:            "<<std::setw(14)<<roll<<"\n";
            std::cout<<", des_pitch:       "<<std::setw(14)<<des_x[1];
            std::cout<<", des_roll:        "<<std::setw(14)<<des_x[0]<<"\n";
            std::cout<<", tau5:            "<<std::setw(14)<<tau5;
            std::cout<<", tau6:            "<<std::setw(14)<<tau6<<"\n";
            std::cout<<", command5:        "<<std::setw(14)<<command5-bias5;
            std::cout<<", command6:        "<<std::setw(14)<<command6-bias6<<"\n";
            std::cout<<"}"<<std::endl;
        }

        outputFile << i;

        auto time_since_start = std::chrono::high_resolution_clock::now() - initial_time;
        signed int time_ms=std::chrono::duration_cast<std::chrono::duration<signed int, std::micro>>(time_since_start).count();
        outputFile << ", " << time_ms;
        outputFile << ", " << command5;
        outputFile << ", " << command6;
        outputFile << ", " << test_ankle.getActuator5().getPosition();
        outputFile << ", " << test_ankle.getActuator5().getVelocity();
        outputFile << ", " << test_ankle.getActuator5().getEffort();
        outputFile << ", " << test_ankle.getActuator5().getIncrementalPosition();
        outputFile << ", " << test_ankle.getActuator5().getIncrementalVelocity();
        outputFile << ", " << test_ankle.getActuator5().getFaults();
        outputFile << ", " << test_ankle.getActuator5().getHeartBeat();

        outputFile << ", " << test_ankle.getActuator6().getPosition();
        outputFile << ", " << test_ankle.getActuator6().getVelocity();
        outputFile << ", " << test_ankle.getActuator6().getEffort();
        outputFile << ", " << test_ankle.getActuator6().getIncrementalPosition();
        outputFile << ", " << test_ankle.getActuator6().getIncrementalVelocity();
        outputFile << ", " << test_ankle.getActuator6().getFaults();
        outputFile << ", " << test_ankle.getActuator6().getHeartBeat();

        outputFile << ", " << test_ankle.getPitchJoint().getPosition();
        outputFile << ", " << test_ankle.getPitchJoint().getVelocity();
        outputFile << ", " << test_ankle.getRollJoint().getPosition();
        outputFile << ", " << test_ankle.getRollJoint().getVelocity();

        outputFile << ", " << test_ankle.getFootLoadcell().a1.get<int>();
        outputFile << ", " << test_ankle.getFootLoadcell().a2.get<int>();
        outputFile << ", " << test_ankle.getFootLoadcell().a3.get<int>();
        outputFile << ", " << test_ankle.getFootLoadcell().a4.get<int>();
        outputFile << ", " << test_ankle.getFootLoadcell().a5.get<int>();
        outputFile << ", " << test_ankle.getFootLoadcell().a6.get<int>();

        outputFile << ", " << test_ankle.getForce5().force.get<float>();
        outputFile << ", " << test_ankle.getForce6().force.get<float>();

        outputFile << "\n";
        // std::cout << command5<< ", " << command6 << "\n";

        test_ankle.getActuator5().setControl(command5);
        test_ankle.getActuator6().setControl(command6);
        // sleep(TIME_STEP);
    }
};
// class SimThread : public gu::threads::Daemon
// {
//     V1smt::control::ValkyrieRobot &val;
// public:
//     SimThread(V1smt::control::ValkyrieRobot &val):
//         val(val)  {}
//     virtual void loop()
//     {
//         std::cout << test_ankle.getPitchJoint().getPosition() << "\n";

//         sleep(TIME_STEP);
//     }
// };



int main(void)
{
    std::cout << "Beginning ankle test" << std::endl;
    V1smt::control::ValkyrieRobot val;
    ControlThread control_thread(val);
    control_thread.start();
    pause();
}
