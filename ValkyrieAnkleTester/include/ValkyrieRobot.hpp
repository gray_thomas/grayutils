#ifndef VALKYRIE_ROBOT_HPP
#define VALKYRIE_ROBOT_HPP

#include "smtclient.hpp"
#include <string>

namespace V1smt
{
std::string resolve_name(std::string leg, std::string joint, std::string item);
namespace control
{


class ControlActuator
{
    //An interface I plan to use to specify control for SEAs under feedback
public:
    virtual ~ControlActuator() {}
    virtual void setControl(double control) = 0;
    virtual double getPosition() = 0;
    virtual double getVelocity() = 0;
    virtual double getEffort() = 0;
    virtual double getIncrementalPosition() =0;
    virtual double getIncrementalVelocity() =0;
    virtual int getFaults()=0;
    virtual int getHeartBeat()=0;
};

class ControlJoint
{
    //An interface I plan to use to construct a joint vector.
public:
    virtual ~ControlJoint() {}
    virtual double getPosition() = 0;
    virtual double getVelocity() = 0;
};

class V1LinearActuator: public ControlActuator
{
    SMT::Resource desired_force;
    SMT::Resource desired_current;
    SMT::Resource measured_force;
    SMT::Resource position;
    SMT::Resource embedded_velocity;
    SMT::Resource inc_pos;
    SMT::Resource inc_vel;
    SMT::Resource faults;
    SMT::Resource heart_beat;
public:
    V1LinearActuator(SMT::SMTClient client, std::string leg, std::string joint):
        desired_force(client.getResource(resolve_name(leg, joint, "JointForce_Des_N"))),
        desired_current(client.getResource(resolve_name(leg, joint, "CurrentQ_Des_Amps"))),
        measured_force(client.getResource(resolve_name(leg, joint, "JointForce_Meas_N"))),
        position(client.getResource(resolve_name(leg, joint, "Linear_Pos_m"))),
        embedded_velocity(client.getResource(resolve_name(leg, joint, "Linear_Vel_mps"))),
        inc_pos(client.getResource(resolve_name(leg, joint, "IncEnc_Pos_m"))),
        inc_vel(client.getResource(resolve_name(leg, joint, "IncEnc_Vel_mps"))),
        faults(client.getResource(resolve_name(leg, joint, "StatReg2"))),
        heart_beat(client.getResource(resolve_name(leg, joint, "Proc_HeartBeat")))
    {}
    virtual void setControl(double control)
    {
        #ifdef CURRENT_MODE
            desired_current.set<float>(control);
        #else
            desired_force.set<float>(control);
        #endif
    }
    virtual double getPosition()
    {
        return position.get<float>();
    }
    virtual double getVelocity()
    {
        return embedded_velocity.get<float>();
    }
    virtual double getEffort()
    {
        return measured_force.get<float>();
    }
    virtual double getIncrementalPosition()
    {
        return inc_pos.get<float>();
    }
    virtual double getIncrementalVelocity()
    {
        return inc_vel.get<float>();
    }
    virtual int getFaults()
    {
        return faults.get<int>();
    }
    virtual int getHeartBeat()
    {
        return heart_beat.get<int>();
    }
};

class V1Joint : public ControlJoint
{
    SMT::Resource angle;
    SMT::Resource angular_velocity;
public:
    V1Joint(SMT::SMTClient client, std::string assy, std::string joint):
        angle(client.getResource(resolve_name(assy, joint, "JointAPS_Angle_Rad"))),
        angular_velocity(client.getResource(resolve_name(assy, joint, "JointAPS_Vel_Radps")))
    {}
    virtual double getPosition()
    {
        return angle.get<float>();
    }
    virtual double getVelocity()
    {
        return angular_velocity.get<float>();
    }

};

class V1RotaryActuator
{

};

class ForceSensor
{
public:
    SMT::Resource force;
    ForceSensor(SMT::SMTClient client, std::string assy, std::string joint):
        force(client.getResource(resolve_name(assy, joint, "LoadCell_N")))
    {}
};

class LoadCell
{
public:
    SMT::Resource a1;
    SMT::Resource a2;
    SMT::Resource a3;
    SMT::Resource a4;
    SMT::Resource a5;
    SMT::Resource a6;
    LoadCell(SMT::SMTClient client, std::string sub_assembly, std::string joint):
        a1(client.getResource(resolve_name(sub_assembly, joint, "Six_Axis_01"))),
        a2(client.getResource(resolve_name(sub_assembly, joint, "Six_Axis_02"))),
        a3(client.getResource(resolve_name(sub_assembly, joint, "Six_Axis_03"))),
        a4(client.getResource(resolve_name(sub_assembly, joint, "Six_Axis_04"))),
        a5(client.getResource(resolve_name(sub_assembly, joint, "Six_Axis_05"))),
        a6(client.getResource(resolve_name(sub_assembly, joint, "Six_Axis_06")))
    {}
    

};

class V1Ankle
{
    V1LinearActuator act_5;
    V1LinearActuator act_6;
    LoadCell foot_loadcell;
    V1Joint pitch_joint;
    V1Joint roll_joint;
    ForceSensor force_5;
    ForceSensor force_6;

public:
    // TODO: add geometric parameters representation
    V1Ankle(SMT::SMTClient client, std::string leg):
        act_5(client, leg, "j5"),
        act_6(client, leg, "j6"),
        foot_loadcell(client, leg, "j6"),
        pitch_joint(client, leg, "j6"), //TODO: confirm (should have confirmed!)
        roll_joint(client, leg, "j5"),
        force_5(client, leg, "j5"),
        force_6(client, leg, "j6")
    {
        //  std::string base = "/"+leg+"/"+joint;
        // SMT::SMTClient client;
    }
    // TODO: jacobian geometry stuff goes here
    ControlJoint &getPitchJoint()
    {
        return pitch_joint;
    }
    ControlJoint &getRollJoint()
    {
        return roll_joint;
    }
    ControlActuator &getActuator6()
    {
        return act_6;
    }
    ControlActuator &getActuator5()
    {
        return act_5;
    }
    ForceSensor &getForce5()
    {
        return force_5;
    }
    ForceSensor &getForce6()
    {
        return force_6;
    }
    LoadCell &getFootLoadcell()
    {
        return foot_loadcell;
    }

};

class ValkyrieRobot // this will be separated into a control model and a robot interface.
{
    SMT::SMTClient client;
    // Add robot kinematic information
    // Add rigid body model information here
public:
    // V1Ankle leftAnkle;
    V1Ankle leftAnkle;
    ValkyrieRobot():
        client(),
        leftAnkle(client, "left_leg")
    {}
};

}
}
#endif