#!/usr/bin/python

# Author: James Holley <james.j.holley@nasa.gov>

# Tool to set joint torque

import time
import math
import argparse
import yaml
import pysmt
import register_builder

#import reference signal module
import sys, os
sys.path.append(os.path.abspath("."))
import utils.reference_signals as refSig

###############################################################
#parameters (configurable via commandline)
UNITS='Nm'
SIG_AMPLITUDE_DEFAULT = 0.0
SIG_FREQUENCY_HZ_DEFAULT = 0.5
SIG_OFFSET_DEFAULT = 0.0
SAMPLE_PERIOD_SEC = 0.002
###############################################################

parser = argparse.ArgumentParser(description="Puts actuators in torque control mode and provides a control signal.",formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('--channel', '-c',
                    help='name of channel node is on')
parser.add_argument('--node', '-n',
                    help='name of node to test')   
parser.add_argument('--amplitude', '-a', default=SIG_AMPLITUDE_DEFAULT, type=float,
                    help='set torque amplitude (Nm)') 
parser.add_argument('--sin', '-s', action='store_true', default=False,
                    help='use a sinusoidal reference signal')
parser.add_argument('--freq', '-f', default=SIG_FREQUENCY_HZ_DEFAULT, type=float,
                    help='set periodic signal frequency (hz)')
parser.add_argument('--offset', '-o', default=SIG_OFFSET_DEFAULT, type=float,
                    help='set periodic signal offset (Nm)') 
parser.add_argument('--period', '-p', default=SAMPLE_PERIOD_SEC, type=float,
                    help='period of signal that will be transmitted to shared memory') 
parser.add_argument('--linear', action='store_true', default=False,
                    help='is this a linear actuator?') 

args = parser.parse_args()

rnet_path = '/' + args.channel + '/' + args.node

factory = pysmt.SMTClientFactory()
factory.abstractFactories['register'] = register_builder.RegisterFactory

ctrl1 = factory.getResource(rnet_path+'/CtrlReg1')
ctrl2 = factory.getResource(rnet_path+'/CtrlReg2')
stat1 = factory.getResource(rnet_path+'/StatReg1')
stat2 = factory.getResource(rnet_path+'/StatReg2')

if args.linear:
    des_trq = pysmt.getResource(rnet_path + '/JointForce_Des_N')
else:
    des_trq = pysmt.getResource(rnet_path + '/JointTorque_Des_Nm')

CTRL_REG_DELAY = 0.1

# enable
ctrl1.MotorEnable(1)
time.sleep(CTRL_REG_DELAY)
ctrl1.BridgeEnable(1)
time.sleep(CTRL_REG_DELAY)
ctrl1.MotComSource(1)
time.sleep(CTRL_REG_DELAY)
ctrl1.CommutationSel(3)
time.sleep(CTRL_REG_DELAY)
ctrl2.ControlMode(1) #torque control mode 

startTime = time.time()

if args.sin:
    print "Generating a sinusoidal reference signal with:"
    print "Amplitude:", args.amplitude, UNITS
    print "Offset:", args.offset, UNITS
    print "Frequency", args.freq, "hz"
else:
    print "Setting desired torque to", args.amplitude, UNITS

try:
    while True:
        elapsedTime = time.time() - startTime
        if args.sin:
            des_trq( refSig.getSineSignal(elapsedTime, args.amplitude, args.offset, args.freq) )
        else:
            des_trq(args.amplitude)
        time.sleep(SAMPLE_PERIOD_SEC)
except:
    print "Ending..."

## Tear Down
des_trq(0.0)
time.sleep(CTRL_REG_DELAY)
ctrl1.MotorEnable(0)
time.sleep(CTRL_REG_DELAY)
ctrl1.BridgeEnable(0)
time.sleep(CTRL_REG_DELAY)
ctrl1.MotComSource(0)
time.sleep(CTRL_REG_DELAY)
ctrl2.ControlMode(0)

