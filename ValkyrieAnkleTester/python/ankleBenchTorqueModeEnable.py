#!/usr/bin/python

# Author: James Holley <james.j.holley@nasa.gov>
# modified: Gray Thomas

# Tool to set joint to current mode

import time

import pysmt
import register_builder

#import reference signal module
import sys, os
sys.path.append(os.path.abspath("."))
import utils.reference_signals as refSig

SAMPLE_PERIOD_SEC = 0.003
CTRL_REG_DELAY = 0.1
rnet_paths=[]
ctrl1s=[]
ctrl2s=[]
stat1s=[]
stat2s=[]
force_measurements=[]
force_controls=[]

rnet_paths.append( '/' + "left_leg" + '/' + "j6")
rnet_paths.append( '/' + "left_leg" + '/' + "j5")
factory = pysmt.SMTClientFactory()
factory.abstractFactories['register'] = register_builder.RegisterFactory
for rnet_path in rnet_paths:
	ctrl1s.append(factory.getResource(rnet_path+'/CtrlReg1'))
	ctrl2s.append(factory.getResource(rnet_path+'/CtrlReg2'))
	stat1s.append(factory.getResource(rnet_path+'/StatReg1'))
	stat2s.append(factory.getResource(rnet_path+'/StatReg2'))
	force_measurements.append(pysmt.getResource(rnet_path + '/JointForce_Meas_N'))
	force_controls.append(pysmt.getResource(rnet_path + '/JointForce_Des_N'))

# enable
#for ctrl2 in ctrl2s:
#	time.sleep(CTRL_REG_DELAY)
#	ctrl2.ControlMode(1) #torque mode
#	time.sleep(CTRL_REG_DELAY)
#
#time.sleep(1.0)
#
for cont,meas in zip(force_controls,force_measurements):
	cont(meas.read())
#
#time.sleep(1.0)
#
#for ctrl1 in ctrl1s:
#	time.sleep(CTRL_REG_DELAY)
#	ctrl1.BridgeEnable(1)
#	time.sleep(CTRL_REG_DELAY)
#	ctrl1.MotComSource(1) 
#	time.sleep(CTRL_REG_DELAY)
#	ctrl1.CommutationSel(2)
#
#print "Moters readying now..."
#time.sleep(1.0)
#
#for ctrl1 in ctrl1s:
#	time.sleep(CTRL_REG_DELAY)
#	ctrl1.MotorEnable(1)
#print "..."
for ctrl1 in ctrl1s:
    ctrl1(0xA151)
for ctrl2 in ctrl2s:
    ctrl2(0x5002)
print "Motors are ready."
try:
    while True:
        time.sleep(SAMPLE_PERIOD_SEC)
except:
    print "Motors de-readying now..."

## Tear Down
for des_cur in force_controls:
	des_cur(0.0)

for ctrl1 in ctrl1s:
    ctrl1(0xA100)
for ctrl2 in ctrl2s:
    ctrl2(0x5000)

##for ctrl1 in ctrl1s:
#	ctrl1.MotorEnable(0)
#	time.sleep(CTRL_REG_DELAY)
#	ctrl1.BridgeEnable(0)
#	time.sleep(CTRL_REG_DELAY)
#	ctrl1.MotComSource(0)
#	time.sleep(CTRL_REG_DELAY)
#for ctrl2 in ctrl2s:
#	ctrl2.ControlMode(0) #current mode
#	time.sleep(CTRL_REG_DELAY)
print "Done."
