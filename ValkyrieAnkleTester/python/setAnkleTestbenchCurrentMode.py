#!/usr/bin/python

# Author: James Holley <james.j.holley@nasa.gov>
# modified: Gray Thomas

# Tool to set joint to current mode

import time

import pysmt
import register_builder

#import reference signal module
import sys, os
sys.path.append(os.path.abspath("."))
import utils.reference_signals as refSig

SAMPLE_PERIOD_SEC = 0.003

rnet_path = '/' + "right_leg" + '/' + "j6"

factory = pysmt.SMTClientFactory()
factory.abstractFactories['register'] = register_builder.RegisterFactory

ctrl1 = factory.getResource(rnet_path+'/CtrlReg1')
ctrl2 = factory.getResource(rnet_path+'/CtrlReg2')
stat1 = factory.getResource(rnet_path+'/StatReg1')
stat2 = factory.getResource(rnet_path+'/StatReg2')

des_cur = pysmt.getResource(rnet_path + '/CurrentQ_Des_Amps')

CTRL_REG_DELAY = 0.2
# enable
time.sleep(CTRL_REG_DELAY)
ctrl1.MotorEnable(1)
time.sleep(CTRL_REG_DELAY)
ctrl1.BridgeEnable(1)
time.sleep(CTRL_REG_DELAY)
ctrl1.MotComSource(1)
time.sleep(CTRL_REG_DELAY)
ctrl1.CommutationSel(3)
time.sleep(CTRL_REG_DELAY)
ctrl2.ControlMode(0) #current control mode 
time.sleep(CTRL_REG_DELAY)
des_cur(0.0)

try:
    while True:
        time.sleep(SAMPLE_PERIOD_SEC)
except:
    print "Ending..."

## Tear Down
des_cur(0.0)
ctrl1.MotorEnable(0)
time.sleep(CTRL_REG_DELAY)
ctrl1.BridgeEnable(0)
time.sleep(CTRL_REG_DELAY)
ctrl1.MotComSource(0)
time.sleep(CTRL_REG_DELAY)
ctrl2.ControlMode(0)
time.sleep(CTRL_REG_DELAY)

