#ifndef TRANSFORM__HPP
#define TRANSFORM__HPP
#include "Eigen/Dense"
#include "Frame.hpp"
#include "FrameVector.hpp"
#include "FramePoint.hpp"
#include <random>
#include <math.h>

using namespace Eigen;
namespace gu
{
namespace vectorGeometry
{
// Utilities:
Quaterniond randomVersor(std::default_random_engine& gen);
Vector3d randomVector(std::default_random_engine& gen);
Quaterniond imaginaryQuaternion(const Vector3d& vec);
Vector3d imaginaryPart(const Quaterniond& quat);
void directionCosinesFromQuaternion(Quaterniond quat, Matrix3d& dcm);
Quaterniond versor(Vector3d rot, double angle);
Matrix3d matrixFromYPR(double yaw, double pitch, double roll);
Matrix3d matrixFromYPRConfirm(double yaw, double pitch, double roll);
void packYawPitchRoll(double& yaw, double& pitch, double& roll, const Matrix3d& dcm);
Vector3d unitVector(double x, double y, double z);
void closestQuaternionToMatrix(Quaterniond &quat, const Matrix3d &mat);

template<typename BaseFrame, typename ChildFrame>
class SpatialTransform
{
	Vector3d position;
	mutable Quaterniond orientation;
	mutable Matrix3d dcm;
	mutable bool tempUpToDate;
	bool quaternionIsPrimary;
protected:
public:
	SpatialTransform() :
					orientation(1.0, 0.0, 0.0, 0.0),
					position(0.0, 0.0, 0.0),
					dcm(),
					tempUpToDate(false),
					quaternionIsPrimary(true)
	{
	}
	SpatialTransform(const SpatialTransform<BaseFrame, ChildFrame>& clone) :
					orientation(clone.orientation),
					position(clone.position),
					dcm(clone.dcm),
					tempUpToDate(clone.tempUpToDate),
					quaternionIsPrimary(clone.quaternionIsPrimary)
	{
	}
	bool isQuaternionPrimary(void) const
	{
		return quaternionIsPrimary;
	}
	const Matrix3d& getDCM(void) const
	{
		if (!tempUpToDate && quaternionIsPrimary)
		{
			directionCosinesFromQuaternion(orientation, dcm);
			tempUpToDate = true;
		}
		return dcm;
	}
	const Quaterniond& getQuaternion(void) const
	{
		if (!tempUpToDate && !quaternionIsPrimary)
		{
			closestQuaternionToMatrix(orientation, dcm);
			tempUpToDate = true;
		}
		return orientation;
	}
	inline const Vector3d& getPosition(void) const
	{
		return this->position;
	}
	inline void setPosition(const Vector3d& position)
	{
		this->position = position;
	}
	inline void setPosition(double x, double y, double z)
	{
		this->position << x, y, z;
	}

#define MACRO_ABS(x) ((x>0)?x:-x)
	inline void setOrientation(double w, double x, double y, double z)
	{
		this->orientation.x() = x;
		this->orientation.y() = y;
		this->orientation.z() = z;
		this->orientation.w() = w;
		if ((MACRO_ABS(x) < 1e-14) && (MACRO_ABS(y) < 1e-14) && (MACRO_ABS(z) < 1e-14) && (MACRO_ABS(w) < 1e-14))
			this->orientation.w() = 1;
		this->orientation.normalize();
		this->quaternionIsPrimary = true;
		this->tempUpToDate = false;
	}
	inline void setOrientation(const Quaterniond& orientation)
	{
		this->orientation = orientation;
		this->orientation.normalize();
		this->quaternionIsPrimary = true;
		this->tempUpToDate = false;
	}
	void setOrientation(double xx, double xy, double xz, double yx, double yy, double yz, double zx, double zy, double zz)
	{
		this->dcm << xx, xy, xz, yx, yy, yz, zx, zy, zz;
		closestQuaternionToMatrix(orientation, this->dcm);
		this->quaternionIsPrimary = true;
		this->tempUpToDate = false;
	}
	void setOrientation(const Matrix3d& dcm)
	{
		this->dcm = dcm;
		closestQuaternionToMatrix(orientation, this->dcm);
		this->quaternionIsPrimary = true;
		this->tempUpToDate = false;
	}
	void setQuaternionDominant()
	{
		if (!quaternionIsPrimary)
		{
			getQuaternion();
			quaternionIsPrimary = true;
		}
	}
	void setDCMDominant()
	{
		if (quaternionIsPrimary)
		{
			getDCM();
			quaternionIsPrimary = false;
		}
	}
	FrameVector<BaseFrame> transform(const FrameVector<ChildFrame>& vector) const
	{
		FrameVector<BaseFrame> ret(getDCM() * vector.getVector());
		return ret;
	}
	FramePoint<BaseFrame> transform(const FramePoint<ChildFrame>& point) const
	{
		FramePoint<BaseFrame> ret(getPosition() + getDCM() * point.getPoint());
		return ret;
	}
	template<typename NewChildFrame>
	SpatialTransform<BaseFrame, NewChildFrame> transform(const SpatialTransform<ChildFrame, NewChildFrame>& other) const
	{
		SpatialTransform<BaseFrame, NewChildFrame> ret;
		ret.setPosition(position + getDCM() * other.getPosition());
		if (this->quaternionIsPrimary && other.isQuaternionPrimary())
			ret.setOrientation(getQuaternion() * other.getQuaternion());
		else
			ret.setOrientation(getDCM() * other.getDCM());
		return ret;
	}
	template<typename NewBaseFrame, typename NewChildFrame>
	SpatialTransform<NewBaseFrame, NewChildFrame> cast(void) const
	{
		SpatialTransform<NewBaseFrame, NewChildFrame> newTransform;
		newTransform.setPosition(getPosition());
		if (this->quaternionIsPrimary)
			newTransform.setOrientation(getQuaternion());
		else
			newTransform.setOrientation(getDCM());
		return newTransform;
	}
	SpatialTransform<ChildFrame, BaseFrame> inverse(void) const
	{
		SpatialTransform<ChildFrame, BaseFrame> ret;
		if (this->quaternionIsPrimary)
			ret.setOrientation(getQuaternion().conjugate());
		else
			ret.setOrientation(getDCM().inverse());
		ret.setPosition(-ret.getDCM() * getPosition());
		return ret;
	}

	template<typename NewChildFrame>
	SpatialTransform<BaseFrame, NewChildFrame> operator *(const SpatialTransform<ChildFrame, NewChildFrame>& other) const
	{
		return this->transform(other);
	}

	FrameVector<BaseFrame> operator *(const FrameVector<ChildFrame>& vector) const
	{
		return this->transform(vector);
	}
	FramePoint<BaseFrame> operator *(const FramePoint<ChildFrame>& point) const
	{
		return this->transform(point);
	}

};
std::ostream& operator<<(std::ostream& out, const Quaterniond& quat);
template<typename BaseFrame, typename ChildFrame>
inline std::ostream& operator<<(std::ostream& out, const SpatialTransform<BaseFrame, ChildFrame>& transform)
{

	out << "{ " << transform.getQuaternion() << "}-[" << transform.getPosition().transpose();
	out << "]_{" << BaseFrame::name() << "}";
	return out;
}

template<typename BaseFrame, typename ChildFrame>
inline Eigen::Matrix<double, 3, Dynamic> operator *(const gu::vectorGeometry::SpatialTransform<BaseFrame, ChildFrame>& transform,
		const Eigen::Matrix<double, 3, Dynamic>& input)
{
	Eigen::Matrix<double, 3, Dynamic> output = transform.getDCM() * input;
	output.colwise() += transform.getPosition();
	return output;
}
//PUNT: make a FramePointCluster class for points in the same frame based on Matrix<3,Dyanmic>

}
}
#endif
