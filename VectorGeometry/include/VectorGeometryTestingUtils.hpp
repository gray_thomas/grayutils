#ifndef VECTOR_GEOMETRY_TESTING_UTILS_HPP
#define VECTOR_GEOMETRY_TESTING_UTILS_HPP
#define ASSERT_MATRIX_NEAR(mat1,mat2,abs_error) \
		{ASSERT_EQ(mat1.rows(),mat2.rows());\
		ASSERT_EQ(mat1.cols(),mat2.cols());\
		for (int i=0;i<mat1.rows();i++)\
			for (int j=0;j<mat1.cols();j++)\
				ASSERT_NEAR((double)mat1(i,j),(double)mat2(i,j),abs_error);}
#define ASSERT_MATRIX_NOT_NEAR(mat1,mat2,abs_error) \
		{ASSERT_EQ(mat1.rows(),mat2.rows());\
		ASSERT_EQ(mat1.cols(),mat2.cols());\
		bool equal=true;\
		for (int i=0;i<mat1.rows();i++)\
			for (int j=0;j<mat1.cols();j++)\
				equal&=!(fabs((double)mat1(i,j)-(double)mat2(i,j))>abs_error);\
		ASSERT_FALSE(equal);}
#endif
