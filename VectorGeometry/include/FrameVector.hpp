#ifndef FRAME_VECTOR__HPP
#define FRAME_VECTOR__HPP
#include <Eigen/Dense>
#include <iostream>
#include "Frame.hpp"
using namespace Eigen;
namespace gu
{
namespace vectorGeometry
{
template <typename BaseFrame>
class FrameVector
{
	Vector3d vector;
public:
	FrameVector() :
				vector()
	{
	}
	FrameVector( double x, double y, double z) :
				vector()
	{
		vector << x, y, z;
	}
	FrameVector(const FrameVector<BaseFrame>& clone) :
				vector(clone.vector)
	{
	}
	FrameVector(const Vector3d& vector) :
				vector(vector)
	{
	}
	FrameVector<BaseFrame> scale(double scaleFactor) const
	{
		FrameVector<BaseFrame> ret(scaleFactor*this->vector);
		return ret;
	}
	FrameVector<BaseFrame> add(const FrameVector<BaseFrame>& other) const
	{
		FrameVector<BaseFrame> ret(this->vector + other.vector);
		return ret;
	}
	FrameVector<BaseFrame> subtract(const FrameVector<BaseFrame>& other) const
	{
		FrameVector<BaseFrame> ret(this->vector - other.vector);
		return ret;
	}
	FrameVector<BaseFrame> operator +(const FrameVector<BaseFrame>& B)
	{
		return this->add(B);
	}
	FrameVector<BaseFrame> operator -(const FrameVector<BaseFrame>& B)
	{
		return this->subtract(B);
	}
	FrameVector<BaseFrame> operator *(double scale) const
	{
		return this->scale(scale);
	}
	double dot(const FrameVector<BaseFrame>& other) const
	{
		return this->vector.dot(other.vector);
	}
	const Vector3d & getVector() const
	{
		return vector;
	}
};
template <typename BaseFrame>
inline FrameVector<BaseFrame> operator *(double d, const FrameVector<BaseFrame>& vec)
{
	return vec.scale(d);
}
template <typename BaseFrame>
inline std::ostream& operator <<(std::ostream& out, const FrameVector<BaseFrame>& vector)
{
	out << "<" << vector.getVector().transpose() << ">_{" << BaseFrame::name() << "}";
		return out;
}


}
}
#endif
