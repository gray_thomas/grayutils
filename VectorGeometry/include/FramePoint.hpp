#ifndef FRAME_POINT__HPP
#define FRAME_POINT__HPP
#include <Eigen/Dense>
#include <iostream>
#include "Frame.hpp"
#include "FrameVector.hpp"
using namespace Eigen;
namespace gu
{
namespace vectorGeometry
{
template <typename BaseFrame>
class FramePoint
{
	Vector3d point;
public:
	FramePoint() :
				point()
	{
	}
	FramePoint(double x, double y, double z) :
				point()
	{
		point << x, y, z;
	}
	FramePoint(const FramePoint<BaseFrame>& clone) :
				point(clone.point)
	{
	}
	FramePoint(Vector3d point) :
				point(point)
	{
	}
	FramePoint<BaseFrame> add(const FrameVector<BaseFrame>& other) const
	{
		FramePoint<BaseFrame> ret(this->point + other.getVector());
		return ret;
	}
	FramePoint<BaseFrame> subtract(const FrameVector<BaseFrame>& other) const
	{
		FramePoint<BaseFrame> ret(this->point - other.getVector());
		return ret;
	}
	FrameVector<BaseFrame> subtract(const FramePoint<BaseFrame>& other) const
	{
		FrameVector<BaseFrame> ret(this->point - other.point);
		return ret;
	}
	FramePoint<BaseFrame> operator +(const FrameVector<BaseFrame>& B)
	{
		return this->add(B);
	}
	FramePoint<BaseFrame> operator -(const FrameVector<BaseFrame>& B)
	{
		return this->subtract(B);
	}
	FrameVector<BaseFrame> operator -(const FramePoint<BaseFrame>& B)
	{
		return this->subtract(B);
	}
	const Vector3d & getPoint() const
	{
		return point;
	}
};
template <typename BaseFrame>
inline std::ostream& operator <<(std::ostream& out, const FramePoint<BaseFrame>& vector)
{
	out << "[" << vector.getPoint().transpose() << "]_{" << BaseFrame::name() << "}";
	return out;
}

}
}
#endif
