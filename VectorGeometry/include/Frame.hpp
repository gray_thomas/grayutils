#ifndef FRAME__HPP
#define FRAME__HPP
#include <Eigen/Dense>
#include <assert.h>
using namespace Eigen;
namespace gu
{
namespace vectorGeometry
{

template<const char * StringName>
struct Frame
{
	static const char * name()
	{
		return StringName;
	}
};
template<const char * StringName>
struct RigidBody
{
	static const char * name()
	{
		return StringName;
	}
};
template<const char * StringName, typename RigidBody>
struct RigidBodyFrame
{
	static const char * name()
	{
		return StringName;
	}
	static const char * bodyName()
	{
		return RigidBody::name();
	}
};

extern char worldFrameName[];
extern char worldBodyName[];
extern char worldBodyFrameName[];
typedef Frame<worldFrameName> worldFrame;
typedef RigidBody<worldBodyName> worldBody;
typedef RigidBodyFrame<worldBodyFrameName,worldBody> worldBodyFrame;
extern char parentFrameName[];
extern char parentBodyName[];
extern char parentBodyJointFrameName[];
typedef Frame<parentFrameName> parentFrame;
typedef RigidBody<parentBodyName> parentBody;
typedef RigidBodyFrame<parentBodyJointFrameName, parentBody> parentJointFrame;
extern char childFrameName[];
extern char childBodyName[];
extern char childBodyJointFrameName[];
typedef Frame<childFrameName> childFrame;
typedef RigidBody<childBodyName> childBody;
typedef RigidBodyFrame<childBodyJointFrameName, childBody> childJointFrame;



}
}
#endif
