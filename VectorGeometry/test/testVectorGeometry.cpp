#include <iostream>
#include "Frame.hpp"
#include "FramePoint.hpp"
#include "FrameVector.hpp"
#include "SpatialTransform.hpp"
#include "gtest/gtest.h"
using namespace gu::vectorGeometry;

char frameAName[] = "Frame A";
typedef Frame<frameAName> frameA;
TEST(FrameVector, add)
{
	FrameVector<worldFrame> vecA(1, 2, 3);
	FrameVector<worldFrame> vecB(3, 4, 1);
	ASSERT_NO_THROW(FrameVector<worldFrame> vecC = vecA + vecB);
	FrameVector<worldFrame> vecC = vecA + vecB;
	FrameVector<worldFrame> expectedC(4,6,4);
	ASSERT_TRUE((vecC.getVector()-expectedC.getVector()).norm()<1e-7);
}
TEST(FrameVector, transform)
{
	FrameVector<worldFrame> vecA(1, 1, 1);
	SpatialTransform<frameA,worldFrame> transform;
	transform.setPosition(2,3,0);
	FrameVector<frameA> vecBExpect(1, 1, 1);
	FrameVector<frameA> vecB=transform*vecA;
	ASSERT_NEAR((vecB.getVector()-vecBExpect.getVector()).norm(),0,1e-7);
}
TEST(FramePoint, transform)
{
	FramePoint<worldFrame> vecA(1, 1, 1);
	SpatialTransform<frameA,worldFrame> transform;
	transform.setPosition(2,3,0);
	FramePoint<frameA> vecBExpect(3, 4, 1);
	FramePoint<frameA> vecB=transform*vecA;
	ASSERT_NEAR((vecB.getPoint()-vecBExpect.getPoint()).norm(),0,1e-7);
}


char frame1Name[] = "Frame 1";
typedef Frame<frame1Name> frame1;
char frame2Name[] = "Frame 2";
typedef Frame<frame2Name> frame2;

void makeSomeTransforms()
{
	SpatialTransform<worldFrame, frame1> pose1;
	pose1.setPosition(1.0, 0.0, 10.0);
	pose1.setOrientation(0.7, 0.0, 0.0, 0.7);
	FramePoint<frame1> originOfFrame1(0.0, 0.0, 0.0);
	std::cout << "pose1:" << pose1 << std::endl;
	std::cout << "originOfFrame1 in frame1 " << originOfFrame1 << std::endl;
	std::cout << "originOfFrame1 in world " << pose1 * originOfFrame1 << std::endl;
	FramePoint<frame1> pointAlongXinFrame1(2.0, 0.0, 0.0);
	std::cout << "pointAlongXinFrame1 in frame1 " << pointAlongXinFrame1 << std::endl;
	std::cout << "pointAlongXinFrame1 in world " << pose1 * pointAlongXinFrame1 << std::endl;
	pose1.setDCMDominant();
	std::cout << "pointAlongXinFrame1 in world " << pose1 * pointAlongXinFrame1 << std::endl;
	pose1.setQuaternionDominant();
	std::cout << "pointAlongXinFrame1 in world " << pose1 * pointAlongXinFrame1 << std::endl;
}

