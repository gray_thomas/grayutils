#include <iostream>
#include "Frame.hpp"
#include "FramePoint.hpp"
#include "FrameVector.hpp"
#include "SpatialTransform.hpp"
#include "VectorGeometryTestingUtils.hpp"
#include "gtest/gtest.h"
using namespace gu::vectorGeometry;



TEST(directionCosinesFromQuaternion,randomTransformedVectorsMatch)
{
	std::default_random_engine gen(1234284762L);
	for (int i = 0; i < 200; i++)
	{
		Quaterniond quat = randomVersor(gen);
		Matrix3d dcm;
		directionCosinesFromQuaternion(quat, dcm);
		Vector3d vec = randomVector(gen);
		Vector3d res1 = dcm * vec;
		Quaterniond vecQuat = imaginaryQuaternion(vec);
		Quaterniond res2quat = quat * vecQuat * quat.conjugate();
		Vector3d res2 = imaginaryPart(res2quat);
		ASSERT_MATRIX_NEAR(res1, res2, 1e-7);
		if (fabs(quat.w()) < .995)
			ASSERT_MATRIX_NOT_NEAR(vec, res1, 1e-7);
	}
}
TEST(directionCosinesFromQuaternion,zeroQuaternionInput)
{
	Quaterniond quat(0.0, 0.0, 0.0, 0.0);
	Matrix3d dcm;
	directionCosinesFromQuaternion(quat, dcm);
	Matrix3d identity = Matrix3d::Identity();
	ASSERT_MATRIX_NEAR(identity, dcm, 1e-7);
}
TEST(directionCosinesFromQuaternion,ninetyDegreeRotationX)
{
	Quaterniond quat(1.0, 1.0, 0.0, 0.0);
	Matrix3d dcm;
	directionCosinesFromQuaternion(quat, dcm);
	Matrix3d expect = matrixFromYPR(0, 0, M_PI / 2);
	ASSERT_MATRIX_NEAR(expect, dcm, 1e-7);
}
TEST(directionCosinesFromQuaternion,ninetyDegreeRotationY)
{
	Quaterniond quat(1.0, 0.0, 1.0, 0.0);
	Matrix3d dcm;
	directionCosinesFromQuaternion(quat, dcm);
	Matrix3d expect = matrixFromYPR(0, M_PI / 2, 0);
	ASSERT_MATRIX_NEAR(expect, dcm, 1e-7);
}
TEST(directionCosinesFromQuaternion,ninetyDegreeRotationZ)
{
	Quaterniond quat(1.0, 0.0, 0.0, 1.0);
	Matrix3d dcm;
	directionCosinesFromQuaternion(quat, dcm);
	Matrix3d expect = matrixFromYPR(M_PI / 2, 0, 0);
	ASSERT_MATRIX_NEAR(expect, dcm, 1e-7);
}

TEST(packYawPitchRoll,randomTransformsAreReproduced)
{
	std::default_random_engine gen(1234284762L);
	for (int i = 0; i < 200; i++)
	{
		double yaw, pitch, roll;
		Quaterniond quat = randomVersor(gen);
		Matrix3d dcm, dcm2, dcm3;
		directionCosinesFromQuaternion(quat, dcm);
		packYawPitchRoll(yaw, pitch, roll, dcm);
		dcm2 = matrixFromYPR(yaw, pitch, roll);
		dcm3 = matrixFromYPRConfirm(yaw, pitch, roll);
		ASSERT_MATRIX_NEAR(dcm, dcm2, 1e-7);
		ASSERT_MATRIX_NEAR(dcm, dcm3, 1e-7);
	}
}
TEST(packYawPitchRoll,NinetyDegreeX)
{
	double yaw, pitch, roll;
	Quaterniond quat(1, 1, 0, 0);
	Matrix3d dcm, dcm2, dcm3;
	directionCosinesFromQuaternion(quat, dcm);
	packYawPitchRoll(yaw, pitch, roll, dcm);
	dcm2 = matrixFromYPR(yaw, pitch, roll);
	dcm3 = matrixFromYPRConfirm(yaw, pitch, roll);
	ASSERT_MATRIX_NEAR(dcm, dcm2, 1e-7);
	ASSERT_MATRIX_NEAR(dcm, dcm3, 1e-7);
}
TEST(packYawPitchRoll,NinetyDegreeY)
{
	double yaw, pitch, roll;
	Quaterniond quat(1, 0, 1, 0);
	Matrix3d dcm, dcm2, dcm3;
	directionCosinesFromQuaternion(quat, dcm);
	packYawPitchRoll(yaw, pitch, roll, dcm);
	dcm2 = matrixFromYPR(yaw, pitch, roll);
	dcm3 = matrixFromYPRConfirm(yaw, pitch, roll);
	ASSERT_MATRIX_NEAR(dcm, dcm2, 1e-7);
	ASSERT_MATRIX_NEAR(dcm, dcm3, 1e-7);
}
TEST(packYawPitchRoll,NinetyDegreeZ)
{
	double yaw, pitch, roll;
	Quaterniond quat(1, 0, 0, 1);
	Matrix3d dcm, dcm2, dcm3;
	directionCosinesFromQuaternion(quat, dcm);
	packYawPitchRoll(yaw, pitch, roll, dcm);
	dcm2 = matrixFromYPR(yaw, pitch, roll);
	dcm3 = matrixFromYPRConfirm(yaw, pitch, roll);
	ASSERT_MATRIX_NEAR(dcm, dcm2, 1e-7);
	ASSERT_MATRIX_NEAR(dcm, dcm3, 1e-7);
}
TEST(packYawPitchRoll,randomDifficultTransformsAreReproduced)
{
	std::vector<Quaterniond> baseQuats;
	baseQuats.push_back(Quaterniond(1, 1, 0, 0));
	baseQuats.push_back(Quaterniond(1, 0, 1, 0));
	baseQuats.push_back(Quaterniond(1, 0, 0, 1));
	baseQuats.push_back(Quaterniond(1, 0, 0, 0));
	baseQuats.push_back(Quaterniond(1, -1, 0, 0));
	baseQuats.push_back(Quaterniond(1, 0, -1, 0));
	baseQuats.push_back(Quaterniond(1, 0, 0, -1));
	baseQuats.push_back(Quaterniond(0, 1, 0, 0));
	baseQuats.push_back(Quaterniond(0, 0, 1, 0));
	baseQuats.push_back(Quaterniond(0, 0, 0, 1));
	for (std::vector<Quaterniond>::iterator it = baseQuats.begin(); it != baseQuats.end(); ++it)
	{
		std::default_random_engine gen(1234284762L);
		for (int i = 0; i < 20; i++)
		{
			double yaw, pitch, roll;
			Quaterniond quat(*it);
			Quaterniond quat2;
			std::normal_distribution<double> dist(0, 1);
			Vector3d vec;
			vec << dist(gen), dist(gen), dist(gen);
			if (fabs(vec.norm()) < 1e-7)
				vec << 1.0, 0, 0;
			quat2 = versor(vec, dist(gen) * 2 * M_PI * 1e-6);
			quat *= quat2;
			Matrix3d dcm, dcm2, dcm3;
			directionCosinesFromQuaternion(quat, dcm);
			packYawPitchRoll(yaw, pitch, roll, dcm);
			dcm2 = matrixFromYPR(yaw, pitch, roll);
			dcm3 = matrixFromYPRConfirm(yaw, pitch, roll);
			ASSERT_MATRIX_NEAR(dcm, dcm2, 1e-7);
			ASSERT_MATRIX_NEAR(dcm, dcm3, 1e-7);
		}
	}
}
TEST(closestQuaternionToMatrix,randomDifficultTransformsAreReproduced)
{
	std::vector<Quaterniond> baseQuats;
	baseQuats.push_back(Quaterniond(1, 1, 0, 0));
	baseQuats.push_back(Quaterniond(1, 0, 1, 0));
	baseQuats.push_back(Quaterniond(1, 0, 0, 1));
	baseQuats.push_back(Quaterniond(1, 0, 0, 0));
	baseQuats.push_back(Quaterniond(1, -1, 0, 0));
	baseQuats.push_back(Quaterniond(1, 0, -1, 0));
	baseQuats.push_back(Quaterniond(1, 0, 0, -1));
	baseQuats.push_back(Quaterniond(0, 1, 0, 0));
	baseQuats.push_back(Quaterniond(0, 0, 1, 0));
	baseQuats.push_back(Quaterniond(0, 0, 0, 1));
	for (std::vector<Quaterniond>::iterator it = baseQuats.begin(); it != baseQuats.end(); ++it)
	{
		std::default_random_engine gen(1234284762L);
		for (int i = 0; i < 20; i++)
		{
			double yaw, pitch, roll;
			Quaterniond quat(*it);
			Quaterniond quat2;
			std::normal_distribution<double> dist(0, 1);
			Vector3d vec;
			vec << dist(gen), dist(gen), dist(gen);
			if (fabs(vec.norm()) < 1e-7)
				vec << 1.0, 0, 0;
			quat2 = versor(vec, dist(gen) * 2 * M_PI * 1e-6);
			quat.normalize();
			quat *= quat2;
			Matrix3d dcm;
			directionCosinesFromQuaternion(quat, dcm);
			closestQuaternionToMatrix(quat2, dcm);
			ASSERT_NEAR(fabs(quat.w()), fabs(quat2.w()), 1e-6);
			ASSERT_NEAR(fabs(quat.x()), fabs(quat2.x()), 1e-6);
			ASSERT_NEAR(fabs(quat.y()), fabs(quat2.y()), 1e-6);
			ASSERT_NEAR(fabs(quat.z()), fabs(quat2.z()), 1e-6);
		}
	}
}



