#include "Frame.hpp"
namespace gu{
namespace vectorGeometry{
char worldFrameName[] = "World Frame";
char worldBodyName[] = "World Body";
char worldBodyFrameName[] = "World Body Frame";
char parentFrameName[] = "Parent Frame";
char parentBodyName[] = "Parent Body";
char parentBodyJointFrameName[] = "Joint Frame of Parent Body";
char childFrameName[] = "Child Frame";
char childBodyName[] = "Child Body";
char childBodyJointFrame[] = "Joint Frame of Child Body";
}
}
