#include <Eigen/Dense>
#include <SpatialTransform.hpp>
#include <random>
using namespace Eigen;
namespace gu
{
namespace vectorGeometry
{
Quaterniond randomVersor(std::default_random_engine& gen)
{
	std::normal_distribution<double> dist(0, 1);
	Vector3d vec;
	vec << dist(gen), dist(gen), dist(gen);
	if (fabs(vec.norm()) < 1e-7)
		vec << 1.0, 0, 0;
	return versor(vec, dist(gen) * 2 * M_PI);
}
Vector3d randomVector(std::default_random_engine& gen)
{
	std::normal_distribution<double> dist(0, 1);
	Vector3d vec;
	vec << dist(gen), dist(gen), dist(gen);
	return vec;
}
Quaterniond imaginaryQuaternion(const Vector3d& vec)
{
	Quaterniond quat;
	quat.x() = vec[0];
	quat.y() = vec[1];
	quat.z() = vec[2];
	quat.w() = 0;
	return quat;
}
Vector3d imaginaryPart(const Quaterniond& quat)
{
	Vector3d vec;
	vec << quat.x(), quat.y(), quat.z();
	return vec;
}
std::ostream& operator<<(std::ostream& out, const Eigen::Quaterniond& quat)
{
	out << quat.w() << " + " << quat.x() << "*i + " << quat.y() << "*j + " << quat.z() << "*k";
	return out;
}

void directionCosinesFromQuaternion(Quaterniond quat, Matrix3d& dcm)
{
	if (quat.norm() < 1e-7)
		quat.setIdentity();
	quat.normalize();
	double x = quat.x(), y = quat.y(), z = quat.z(), w = quat.w();
	dcm << 1 - 2 * (y * y + z * z), 2 * (x * y - z * w), 2 * (x * z + y * w), //
	2 * (x * y + z * w), 1 - 2 * (x * x + z * z), 2 * (y * z - x * w), //
	2 * (x * z - y * w), 2 * (y * z + x * w), 1 - 2 * (x * x + y * y);
}
Quaterniond versor(Vector3d rot, double angle)
{
	rot.normalize();
	double w = std::cos(angle / 2);
	double p = std::sin(angle / 2);
	Quaterniond versor;
	versor.x() = p * rot(0);
	versor.y() = p * rot(1);
	versor.z() = p * rot(2);
	versor.w() = w;
	return versor;
}
Matrix3d matrixFromYPR(double yaw, double pitch, double roll)
{
	Matrix3d m;
	m = AngleAxisd(yaw, Vector3d::UnitZ()) * AngleAxisd(pitch, Vector3d::UnitY()) * AngleAxisd(roll, Vector3d::UnitX());
	return m;
}
Matrix3d matrixFromYPRConfirm(double yaw, double pitch, double roll)
{
	Matrix3d m;
	m << cos(yaw) * cos(pitch), (cos(yaw) * sin(pitch) * sin(roll) - sin(yaw) * cos(roll)), (cos(yaw) * sin(pitch) * cos(roll)
			+ sin(yaw) * sin(roll)), sin(yaw) * cos(pitch), (sin(yaw) * sin(pitch) * sin(roll) + cos(yaw) * cos(roll)), (sin(yaw)
			* sin(pitch) * cos(roll) - cos(yaw) * sin(roll)), -sin(pitch), cos(pitch) * sin(roll), cos(pitch) * cos(roll);
	return m;
}

void packYawPitchRoll(double& yaw, double& pitch, double& roll, const Matrix3d& dcm)
{
// only works if dcm is orthogonal
	// only works for the intrinsic z-y-x convention (equivalent to x-y-z extrinsic?)
	// only valid for cos(roll)>>eps, cos(pitch)>>eps
	pitch = atan2(-(double) dcm(2, 0), sqrt(pow((double) dcm(2, 1), 2) + pow((double) dcm(2, 2), 2)));
	roll = atan2((double) dcm(2, 1), (double) dcm(2, 2));
	yaw = atan2((double) dcm(1, 0), (double) dcm(0, 0));

}

Vector3d unitVector(double x, double y, double z)
{
	Vector3d rot;
	rot << x, y, z;
	rot.normalize();
	return rot;
}
void closestQuaternionToMatrix(Quaterniond &quat, const Matrix3d &mat)
{
	const Matrix3d& d = mat;
	double dxx = d(0, 0), dxy = d(0, 1), dxz = d(0, 2), dyx = d(1, 0), dyy = d(1, 1), dyz = d(1, 2), dzx = d(2, 0), dzy = d(2, 1), dzz = d(
			2, 2);
	Matrix4d directionTest;
	directionTest.row(0) << (dxx - dyy - dzz), (dyx + dxy), (dzx + dxz), (dyz - dzy);
	directionTest.row(1) << (dyx + dxy), (dyy - dxx - dzz), (dzy + dyz), (dzx - dxz);
	directionTest.row(2) << (dzx + dxz), (dzy + dyz), (dzz - dxx - dyy), (dxy - dyx);
	directionTest.row(3) << (dyz - dzy), (dzx - dxz), (dxy - dyx), (dxx + dyy + dzz);
	directionTest *= (1.0 / 3.0);
	SelfAdjointEigenSolver<Matrix4d> solver(directionTest);
	Vector4d directionQuaternion;
	directionQuaternion << solver.eigenvectors().col(3);
	directionQuaternion.segment(0, 3) *= -1; // flip direction of transform (conjugate quaternion)
	directionQuaternion.normalize();
	if (directionQuaternion(3) < 0)
		directionQuaternion *= -1;
	quat.x() = directionQuaternion(0);
	quat.y() = directionQuaternion(1);
	quat.z() = directionQuaternion(2);
	quat.w() = directionQuaternion(3);
}

}
}
