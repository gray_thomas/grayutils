#include "gtest/gtest.h"
#include "SpatialTransform.hpp"
#include "SpatialForceVector.hpp"
#include "SpatialMotionVector.hpp"
#include "VectorGeometryTestingUtils.hpp"
#include <random>
using namespace gu::vectorGeometry;
using namespace gu::rigidBodyDynamics;

TEST(basicAlgebra,spatialVectorDualProduct)
{
	SpatialMotionVector<worldFrame> motionVector(1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	SpatialForceVector<worldFrame> forceVector(1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	double product1 = motionVector * forceVector;
	double product2 = forceVector * motionVector;
	double product3 = motionVector.dualProduct(forceVector);
	double product4 = forceVector.dualProduct(motionVector);
	EXPECT_NEAR(1, product1, 1e-7);
	EXPECT_NEAR(1, product2, 1e-7);
	EXPECT_NEAR(1, product3, 1e-7);
	EXPECT_NEAR(1, product4, 1e-7);
}
TEST(basicAlgebra,spatialVectorTransforms)
{
	SpatialMotionVector<childFrame> motionVector1(1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	SpatialForceVector<childFrame> forceVector1(1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	SpatialTransform<parentFrame, childFrame> transform;
	transform.setPosition(2, 3, 4);
	transform.setOrientation(1, 1, 0, 0);
	{
		SpatialForceVector<parentFrame> forceVector = transform * forceVector1;
		SpatialMotionVector<parentFrame> motionVector = transform * motionVector1;
		double product1 = motionVector * forceVector;
		double product2 = forceVector * motionVector;
		EXPECT_NEAR(1, product1, 1e-7);
		EXPECT_NEAR(1, product2, 1e-7);
	}
	{
		SpatialForceVector<parentFrame> forceVector = forceVector1.applyTransform(transform);
		SpatialMotionVector<parentFrame> motionVector = motionVector1.applyTransform(transform);
		double product1 = motionVector * forceVector;
		double product2 = forceVector * motionVector;
		EXPECT_NEAR(1, product1, 1e-7);
		EXPECT_NEAR(1, product2, 1e-7);
	}
}
TEST(basicAlgebra,dotProductInvarianceUnderRandomTransforms)
{
	std::default_random_engine gen;
	std::normal_distribution<double> dist(0, 1.0);
	for (int i = 0; i < 100; i++)
	{
		SpatialMotionVector<childFrame> motion(dist(gen), dist(gen), dist(gen), dist(gen), dist(gen), dist(gen));
		SpatialForceVector<childFrame> force(dist(gen), dist(gen), dist(gen), dist(gen), dist(gen), dist(gen));
		SpatialTransform<parentFrame, childFrame> transform;
		transform.setPosition(dist(gen), dist(gen), dist(gen));
		transform.setOrientation(dist(gen), dist(gen), dist(gen), dist(gen));

		EXPECT_NEAR(motion * force, (transform * motion) * (transform * force), 1e-7);
		EXPECT_NEAR(force * motion, (transform * motion) * (transform * force), 1e-7);
		EXPECT_NEAR(force * motion, (transform * force) * (transform * motion), 1e-7);
	}
}
TEST(basicAlgebra, propertiesOfTheCrossOp)
{
	std::default_random_engine gen;
	std::normal_distribution<double> dist(0, 1.0);
	for (int i = 0; i < 100; i++)
	{
		Vector3d a(dist(gen), dist(gen), dist(gen));
		Vector3d b(dist(gen), dist(gen), dist(gen));
		Matrix3d E;
		Quaterniond quat(dist(gen), dist(gen), dist(gen), dist(gen));
		gu::vectorGeometry::directionCosinesFromQuaternion(quat, E);
		double lambda = dist(gen);
		Matrix3d Einv;
		gu::vectorGeometry::directionCosinesFromQuaternion(quat.conjugate(), Einv);
		ASSERT_MATRIX_NEAR(crossOp(a), ((-crossOp(a)).transpose()), 1e-7);
		ASSERT_MATRIX_NEAR(crossOp((Vector3d )(lambda * a)), ((Matrix3d )(lambda * crossOp(a))), 1e-7);
		ASSERT_MATRIX_NEAR(crossOp((Vector3d )(b + a)), ((Matrix3d )(crossOp(a) + crossOp(b))), 1e-7);
		{
			Matrix3d mat2 = E * crossOp(a) * Einv;
			Matrix3d mat1 = crossOp(E * a);
			ASSERT_MATRIX_NEAR(mat1, mat2, 1e-7);
		}
		{
			Vector3d vec1 = crossOp(a) * b;
			Vector3d vec2 = -crossOp(b) * a;
			ASSERT_MATRIX_NEAR(vec1, vec2, 1e-7);
		}
		{
			Matrix<double, 1, 3> vec1 = ((crossOp(a) * b).transpose());
			Matrix<double, 1, 3> vec2 = (a.transpose() * crossOp(b));
			ASSERT_MATRIX_NEAR(vec1, vec2, 1e-7);
		}
		{
			Matrix3d mat2 = crossOp(crossOp(a) * b);
			Matrix3d mat1 = crossOp(a) * crossOp(b) - crossOp(b) * crossOp(a);
			ASSERT_MATRIX_NEAR(mat1, mat2, 1e-7);
		}
		{
			Matrix3d mat2 = crossOp(crossOp(a) * b);
			Matrix3d mat1 = b * a.transpose() - a * b.transpose();
			ASSERT_MATRIX_NEAR(mat1, mat2, 1e-7);
		}
		{
			Matrix3d mat2 = crossOp(a) * crossOp(b);
			Matrix3d identity=Matrix3d::Identity();
			Matrix3d subMat1=b * a.transpose();
			double dotProduct=a.transpose()*b;
			Matrix3d subMat2=(dotProduct)*(identity);
			Matrix3d mat1 = subMat1 -subMat2;
			ASSERT_MATRIX_NEAR(mat1, mat2, 1e-7);
		}

	}
}
TEST(basicAlgebra,spatialCrossProduct)
{
	SpatialMotionVector<childFrame> velocity(1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	SpatialMotionVector<childFrame> motion(1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	SpatialForceVector<childFrame> force(1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	SpatialForceVector<childFrame> newForce = velocity.cross(force);
	SpatialMotionVector<childFrame> newMotion = velocity.cross(motion);
}

