/*
 * RBDL - Rigid Body Dynamics Library
 * Copyright (c) 2011-2012 Martin Felis <martin.felis@iwr.uni-heidelberg.de>
 * Modified in 2014 by Gray Thomas
 * Licensed under the zlib license. See LICENSE for more details.
 */

#include "rbdl/Logging.hpp"

std::ostringstream LogOutput;

void ClearLogOutput() {
	LogOutput.str("");
}
