#ifndef SPATIAL_VECTOR_HPP
#define SPATIAL_VECTOR_HPP
#include <Eigen/Dense>

using Eigen::Vector3d;
using Eigen::Matrix3d;
namespace gu
{
using namespace vectorGeometry;
namespace rigidBodyDynamics
{

template<typename BaseFrame>
class SpatialMotionVector;
template<typename BaseFrame>
class SpatialForceVector;
template<typename BaseFrame>
double spatialDotProduct(const SpatialMotionVector<BaseFrame>& motion, const SpatialForceVector<BaseFrame>& force)
{
	return motion.getLinear().transpose().dot(force.getLinear()) + motion.getAngular().transpose().dot(force.getAngular());
}
Matrix3d crossOp(const Vector3d& vec)
{
	Matrix3d ret;
	ret << 0, -vec[2], vec[1], vec[2], 0, -vec[0], -vec[1], vec[0], 0;
	return ret;
}

}
}
#endif
