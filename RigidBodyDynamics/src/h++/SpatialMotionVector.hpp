#ifndef SPATIAL_MOTION_VECTOR_HPP
#define SPATIAL_MOTION_VECTOR_HPP
#include <Eigen/Dense>
#include <SpatialVector.hpp>
using Eigen::Vector3d;
using Eigen::Matrix3d;
using Eigen::Matrix;
namespace gu
{
namespace rigidBodyDynamics
{
template<typename BaseFrame>
class SpatialMotionVector
{
protected:
	Vector3d linear;
	Vector3d angular;
public:
	SpatialMotionVector() :
					linear(),
					angular()
	{
	}
	SpatialMotionVector(Vector3d linear, Vector3d angular) :
					linear(linear),
					angular(angular)
	{
	}
	SpatialMotionVector(double x, double y, double z, double rx, double ry, double rz) :
					linear(x, y, z),
					angular(rx, ry, rz)
	{
	}
	const Vector3d& getLinear(void) const
	{
		return linear;
	}
	const Vector3d& getAngular(void) const
	{
		return angular;
	}
	void setLinear(const Vector3d& linear)
	{
		this->linear = linear;
	}
	void setAngular(const Vector3d& angular)
	{
		this->angular = angular;
	}
	double dualProduct(const SpatialForceVector<BaseFrame>& forceVector) const
	{
		return spatialDotProduct(*this, forceVector);
	}
	double operator*(const SpatialForceVector<BaseFrame>& forceVector) const
	{
		return spatialDotProduct(*this, forceVector);
	}
	template<typename NewFrame>
	SpatialMotionVector<NewFrame> applyTransform(const SpatialTransform<NewFrame, BaseFrame>& transform) const
	{
		const Matrix3d& dcm = transform.getDCM();
		const Vector3d& position = transform.getPosition();
		SpatialMotionVector<NewFrame> ret(dcm * linear - dcm * position.cross(angular), dcm * angular);
		return ret;
	}

	SpatialForceVector<BaseFrame> cross(const SpatialForceVector<BaseFrame>& force)
	{
		return SpatialForceVector<BaseFrame>(angular.cross(force.getLinear()),
				angular.cross(force.getAngular()) + linear.cross(force.getLinear()));
	}
	SpatialMotionVector<BaseFrame> cross(const SpatialMotionVector<BaseFrame>& motion)
	{
		return SpatialMotionVector<BaseFrame>(angular.cross(motion.getLinear()) + linear.cross(motion.getAngular()),
				angular.cross(motion.getAngular()));
	}

};
template<typename BaseFrame, typename NewFrame>
SpatialMotionVector<NewFrame> operator*(const SpatialTransform<NewFrame, BaseFrame>&transform, const SpatialMotionVector<BaseFrame>& motion)
{
	return motion.applyTransform(transform);
}
template<typename BaseFrame>
Matrix<double,6,6> spatialCrossOp(SpatialMotionVector<BaseFrame> motionVector)
{
	Matrix<double,6,6> ret;
}
template<typename BaseFrame>
Matrix<double,6,6> spatialDualCrossOp(SpatialMotionVector<BaseFrame> motionVector)
{

}

}
}
#endif
