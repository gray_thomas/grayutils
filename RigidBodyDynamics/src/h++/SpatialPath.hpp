/*
 * RBDL - Rigid Body Dynamics Library
 * Added in 2014 by Gray Thomas
 * Licensed under the zlib license. See LICENSE for more details.
 */
 
#ifndef SPATIAL_PATH_HPP
#define SPATIAL_PATH_HPP
#include "SpatialTransform.hpp"
#include "SpatialVelocityVector.hpp"
#include "SpatialAccelerationVector.hpp"
namespace gu{
namespace vectorGeometry{

template <typename BaseFrame, typename ChildFrame, int DOF>
struct FirstOrderJointLinearization
{
	SpatialTransform<BaseFrame, ChildFrame> transform;
	SpatialVelocityVector<BaseFrame, ChildFrame> jacobian[DOF];
};

template <typename BaseFrame, typename ChildFrame, int DOF, int DerivativeLevel>
class Joint
{
	Eigen::Matrix<double,DOF,DerivativeLevel> fullState;
public:
	FirstOrderJointLinearization<BaseFrame,ChildFrame,DOF> getLinearization(Eigen::Matrix<double,DOF,1> jointState);
	SpatialTransform<BaseFrame,ChildFrame> getTransform();
	SpatialMotionVector<BaseFrame,ChildFrame> getDerivative(int level);
};

template <typename BaseFrame, typename ChildFrame>
class SpatialPathC2
{
public:
	virtual ~SpatialPathC2()
	{
	}
	virtual gu::vectorGeometry::SpatialTransform<BaseFrame, ChildFrame> getTransform(double s)=0;
	virtual gu::vectorGeometry::SpatialVelocityVector<BaseFrame, ChildFrame> getVelocity(double s)=0;
	virtual gu::vectorGeometry::SpatialAccelerationVector<BaseFrame, ChildFrame> getAcceleration(double s)=0;
};

}
}
#endif
