#ifndef SPATIAL_FORCE_VECTOR_HPP
#define SPATIAL_FORCE_VECTOR_HPP
#include <Eigen/Dense>
#include <SpatialVector.hpp>
using Eigen::Vector3d;
namespace gu
{
namespace rigidBodyDynamics
{

template<typename BaseFrame>
class SpatialForceVector
{
protected:
	Vector3d linear;
	Vector3d angular;
public:
	SpatialForceVector() :
					linear(),
					angular()
	{
	}
	SpatialForceVector(Vector3d linear, Vector3d angular) :
					linear(linear),
					angular(angular)
	{
	}
	SpatialForceVector(double x, double y, double z, double rx, double ry, double rz) :
					linear(x, y, z),
					angular(rx, ry, rz)
	{
	}
	const Vector3d& getLinear(void) const
	{
		return linear;
	}
	const Vector3d& getAngular(void) const
	{
		return angular;
	}
	void setLinear(const Vector3d& linear)
	{
		this->linear = linear;
	}
	void setAngular(const Vector3d& angular)
	{
		this->angular = angular;
	}
	double dualProduct(const SpatialMotionVector<BaseFrame>& motion) const
	{
		return spatialDotProduct(motion, *this);
	}
	double operator*(const SpatialMotionVector<BaseFrame>& motion) const
	{
		return spatialDotProduct(motion, *this);
	}
	template<typename NewFrame>
	SpatialForceVector<NewFrame> applyTransform(const SpatialTransform<NewFrame, BaseFrame>& transform) const
	{
		const Matrix3d& dcm = transform.getDCM();
		const Vector3d& position = transform.getPosition();
		SpatialForceVector<NewFrame> ret(dcm * linear, dcm * angular - dcm * position.cross(linear));
		return ret;
	}

};
template<typename BaseFrame, typename NewFrame>
SpatialForceVector<NewFrame> operator*(const SpatialTransform<NewFrame, BaseFrame>&transform, const SpatialForceVector<BaseFrame>& force)
{
	return force.applyTransform(transform);
}
}
}
#endif
