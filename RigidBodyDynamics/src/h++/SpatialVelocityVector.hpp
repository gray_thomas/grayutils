#ifndef SPATIAL_VELOCITY_VECTOR_HPP
#define SPATIAL_VELOCITY_VECTOR_HPP
#include <Eigen/Dense>
#include <SpatialVector.hpp>
using Eigen::Vector3d;
namespace gu
{
namespace vectorGeometry
{
template<typename BaseFrame, typename ChildFrame>
class SpatialVelocityVector: public SpatialVector<BaseFrame>
{
public:
	SpatialVelocityVector() :
					SpatialVector<BaseFrame>()
	{
	}
	SpatialVelocityVector(Vector3d linear, Vector3d angular) :
					SpatialVector<BaseFrame>(linear, angular)
	{
	}
};
}
}
#endif
