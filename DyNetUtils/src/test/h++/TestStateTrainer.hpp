/*
 * TestStateTrainer.hpp
 *
 *    Created on: Jan 21, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef TESTSTATETRAINER_HPP_
#define TESTSTATETRAINER_HPP_


namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
namespace tests
{
void testStateMachine(void);
void testEigenCholesky(void);
void testSparseCholeskySolver(void);
void testSparseHessianSolver(void){
	testStateMachine();
	testEigenCholesky();
	testSparseCholeskySolver();
}
}
}
}
}

#endif /* TESTSTATETRAINER_HPP_ */
