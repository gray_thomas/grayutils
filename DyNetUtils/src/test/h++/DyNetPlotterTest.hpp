/*
 * DyNetPlotterTest.hpp
 *
 *    Created on: Feb 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef DYNETPLOTTERTEST_HPP_
#define DYNETPLOTTERTEST_HPP_
#include <DyNetSizeManager.hpp>
#include <DyNetPlotter.hpp>
using namespace uta::networkUtils::dyNetUtils;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
namespace tests
{
void testDyNetPlotter(void);

class DyNetPlotterTest
{
	DyNetSizeManager& sizes;
public:
	DyNetPlotterTest(DyNetSizeManager& sizes);
};
}

}
}
}



#endif /* DYNETPLOTTERTEST_HPP_ */
