/*
 * TestBackPropThroughTime.hpp
 *
 *    Created on: Dec 3, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef TESTBACKPROPTHROUGHTIME_HPP_
#define TESTBACKPROPTHROUGHTIME_HPP_


namespace uta
{
namespace networkUtils
{
namespace tests
{
void backPropThroughTimeTest(void);
void testCorrectnessOfForwardIterativeBackPropThroughTime(void);
}
}
}

#endif /* TESTBACKPROPTHROUGHTIME_HPP_ */
