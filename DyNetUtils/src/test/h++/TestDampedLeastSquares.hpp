/*
 * TestDampedLeastSquares.hpp
 *
 *  Created on: Dec 29, 2013
 *      Author: Gray
 */

#ifndef TESTDAMPEDLEASTSQUARES_HPP_
#define TESTDAMPEDLEASTSQUARES_HPP_


namespace uta
{
namespace networkUtils
{
namespace tests
{
void leastSquaresTrainerTest(void);
void floatingMemoryTrainerTest(void);

}
}
}


#endif /* TESTDAMPEDLEASTSQUARES_HPP_ */
