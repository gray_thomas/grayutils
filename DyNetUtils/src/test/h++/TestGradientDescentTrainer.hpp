/*
 * TestGradientDescentTrainer.hpp
 *
 *    Created on: Nov 26, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef TESTGRADIENTDESCENTTRAINER_HPP_
#define TESTGRADIENTDESCENTTRAINER_HPP_

namespace uta
{
namespace networkUtils
{
namespace tests
{
void gradientDescentTrainerTest(void);
}
}
}



#endif /* TESTGRADIENTDESCENTTRAINER_HPP_ */
