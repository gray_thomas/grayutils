/*
 * TestDyNetUtils.cpp
 *
 *    Created on: Feb 16, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include <iostream>
#include <TestMethods.hpp>
#include <TestOperators.hpp>
#include <DyNetPlotterTest.hpp>
#include <TestBackPropThroughTime.hpp>
#include <TestDampedLeastSquares.hpp>
#include <TestGradientDescentTrainer.hpp>
#include <TestStateTrainer.hpp>
#include <DyNetPlotter.hpp>
//#include <yamlcpptest.hpp>

int main(int argc, const char* argv[])
{

	std::cout << "TestDyNetUtils start" << std::endl;
	cout << "before yamlcpptest emitter" << endl;
//	yamlcpptest::populateEmitter();
//	yamlcpptest::killEmitter();
	cout << "after yamlcpptest emitter" << endl;
	cout << "before yaml render" << endl;
	uta::networkUtils::dyNetUtils::populateEmitter();
	cout << "after yaml render" << endl;
	uta::networkUtils::dyNetUtils::tests::testDyNetPlotter();
	uta::networkUtils::tests::additionTest();
	uta::networkUtils::tests::multiplicationTest();
	uta::networkUtils::tests::trainableScalarTest();
	uta::networkUtils::tests::networkBuiltInConstructorTest();
	uta::networkUtils::tests::networkVectorizedInputOutputTest();
	uta::networkUtils::tests::operatorTest();
	uta::networkUtils::tests::backPropThroughTimeTest();
	uta::networkUtils::tests::testCorrectnessOfForwardIterativeBackPropThroughTime();
	uta::networkUtils::tests::leastSquaresTrainerTest();
	uta::networkUtils::tests::gradientDescentTrainerTest();
	uta::networkUtils::dyNetUtils::tests::testSparseHessianSolver();
	uta::networkUtils::tests::floatingMemoryTrainerTest();
	std::cout << "TestDyNetUtils finish" << std::endl;
	return 0;
}




