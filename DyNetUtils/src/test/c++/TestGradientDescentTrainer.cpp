/*
 * TestGradientDescentTrainer.cpp
 *
 *    Created on: Nov 26, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "TestGradientDescentTrainer.hpp"
#include "GradientCalculator.hpp"
#include "ErrorCalculator.hpp"
#include "Network.hpp"
#include "Trainable.hpp"
#include <vector>
#include <iostream>
#include "TestUtils.hpp"
#include "ScalarMultiplicationElement.hpp"
#include "ScalarAdditionElement.hpp"
#include "TrainableScalarElement.hpp"
#include "DataSet.hpp"
#include "GradientDescentTrainer.hpp"
#include "PendulumDemoRandomDataGenerator.hpp"
#include "NameMaintainer.hpp"
#include "NetworkStateVector.hpp"
#include "PendulumNet.hpp"

using namespace std;
using namespace uta::testUtils;
using namespace uta::networkUtils;
using namespace uta::networkUtils::pendulumDemo;

#define TRAINING_NETWORK_SIZE 6
namespace uta
{
namespace networkUtils
{
namespace tests
{

class TrainingExampleNet: public uta::networkUtils::Network
{
public:
	TrainingExampleNet(NetworkStateVector& state, unsigned trainingNetworkSize) :
				Network(state, trainingNetworkSize)
	{
		SingleIndexNetworkDataProxy* ins[trainingNetworkSize];
		ScalarMultiplicationElement* squares[trainingNetworkSize];
		TrainableScalarElement* weights[trainingNetworkSize];
		NoIndexNetworkDataProxy* proxy = getGeneratorNetworkInputProxy();
		for (unsigned i = 0; i < trainingNetworkSize; i++)
		{
			ins[i] = getNetworkInput(i);
			weights[i] = new TrainableScalarElement(state, proxy, 0.5);
			squares[i] = new ScalarMultiplicationElement(state);
			squares[i]->addInput(ins[i]);
			squares[i]->addInput(ins[i]);
			squares[i]->addInput(weights[i]->getScalarResult());
			setAsOutput(squares[i]->getScalarResult());
		}
	}
};
class ExampleTrainingDataSet: public DataSet
{
public:
	ExampleTrainingDataSet(unsigned size, unsigned numberOfPoints, double min, double max, double weight) :
				DataSet(size, size)
	{
		double x;
		for (unsigned i = 0; i < numberOfPoints; i++)
		{
			x = min + i * ((max - min) / (numberOfPoints - 1));
			double inputs[size];
			double outputs[size];
			for (unsigned j = 0; j < size; j++)
			{
				inputs[j] = x;
				outputs[j] = x * x * weight;
			}
			this->addLine(inputs, outputs);
		}
	}
};
static void testGradientDescent(string message, unsigned trainingNetworkSize, unsigned epochs, double rate,
								double expectedWeightAtTerminus, double perfectWeight, unsigned dataPoints)
{
	NetworkStateVector state;
	Trainable* network = new TrainingExampleNet(state, trainingNetworkSize);
	ExampleTrainingDataSet trainingSet(trainingNetworkSize, dataPoints, -1, -1, perfectWeight);
	ExampleTrainingDataSet validationSet(trainingNetworkSize, dataPoints, -1, -1, perfectWeight);
	GradientDescentTrainer trainer;
	trainer.setEpochMax(epochs);

	trainer.trainNetwork(trainingSet, validationSet, *network, rate);

	assertEquals(message, trainer.getUpcomingWeight(0), expectedWeightAtTerminus, 1e-6);
}

static void testErrorCalculatorNoMem()
{
	cout << "hey hey hey" << endl;
	double tau = 0.101;
	double eps_plus_one = 2.33;
	double xi = 0.013;
	double inputScale = 1.5e-1;
	double tauGuess = tau;
	double xiGuess = xi;
	double epsilon_plus_oneGuess = eps_plus_one;
	double inputScaleGuess = inputScale;
	const char * name=GRAY_UTILS_ROOT "/PendulumNetDemo/src/main/data/";
	NameMaintainer names(name, "trainFullNetwork_NoMem_");
	FullPendulumDemoDirectTrainingDataGenerator gen(tau, eps_plus_one, xi, inputScale);
	unsigned amountOfData = 1;
	double theta0 = -0.0;
	double omega0 = 0.3;
	double sigma_alpha = 0.00;
	double sigma_input = 1.00;
	double sigma_measure = 0.0;
	unsigned minHold = 40;
	unsigned maxHold = 300;
	double dt = 0.01;
	gen.generateRandomData0mem(names.getTrainingDataFileName(), amountOfData, theta0, omega0, sigma_alpha,
			sigma_input, sigma_measure, minHold, maxHold, dt, 1);
	DataSet trainingDataSet(names.getTrainingDataFileName());

	NetworkStateVector state;
	AltFullPendulumNet* network = new AltFullPendulumNet(state, dt, inputScaleGuess, tauGuess, xiGuess,
			epsilon_plus_oneGuess, 1);
	ErrorCalculator errorCalc = ErrorCalculator(trainingDataSet, *network);
	errorCalc.calculate(0);
	cout<<"made it here"<<endl;
	cout << "trainingDataSet alpha is " << trainingDataSet.getOutput(trainingDataSet.viewSize() - 1, 3) << endl;
	cout << "netAlpha is              " << network->getOutput(3) << endl;
	assertEquals("Alpha", network->getOutput(3), trainingDataSet.getOutput(trainingDataSet.viewSize() - 1, 3),
			fabs(network->getOutput(3) * 1e-6));
	assertEquals("Out", network->getOutput(2), trainingDataSet.getOutput(trainingDataSet.viewSize() - 1, 2),
			fabs(network->getOutput(2) * 2e-6));
	assertEquals("Omega", network->getOutput(1), trainingDataSet.getOutput(trainingDataSet.viewSize() - 1, 1),
			fabs(network->getOutput(1) * 2e-6));
	assertEquals("Theta", network->getOutput(0), trainingDataSet.getOutput(trainingDataSet.viewSize() - 1, 0),
			fabs(network->getOutput(0) * 2e-6));
	cout << "difference is " << trainingDataSet.getOutput(trainingDataSet.viewSize() - 1, 3) - network->getOutput(3)
			<< endl;
	assertEquals("error", errorCalc.getError(), 0.0, 1e-6);
}
static void testErrorCalculator1Mem()
{
	cout << "hey hey hey" << endl;
	double tau = 0.101;
	double eps_plus_one = 2.33;
	double xi = 0.013;
	double inputScale = 1.5e-1;
	unsigned reps = 1;
	const char* name=GRAY_UTILS_ROOT "/PendulumNetDemo/src/main/data/";
	NameMaintainer names(name, "testError_OneMem_");
	FullPendulumDemoDirectTrainingDataGenerator gen(tau, eps_plus_one, xi, inputScale);
	unsigned amountOfData = 20;
	double theta0 = -0.1;
	double omega0 = 0.3;
	double sigma_alpha = 0.00;
	double sigma_input = 0.00;
	double sigma_measure = 0.0;
	unsigned minHold = 40;
	unsigned maxHold = 300;
	double dt = 0.001;
	gen.generateRandomData1mem(names.getTrainingDataFileName(), amountOfData, theta0, omega0, sigma_alpha,
			sigma_input, sigma_measure, minHold, maxHold, dt, reps);
	DataSet trainingDataSet(names.getTrainingDataFileName());
	assertEquals("trainingDataSet.InputSize", trainingDataSet.viewInputSize(), 2, 1e-5);
	assertEquals("trainingDataSet.OutputSize", trainingDataSet.viewOutputSize(), 2, 1e-5);

	NetworkStateVector state;
	AltFullPendulumNet* network = new AltFullPendulumNet(state, dt, inputScale, tau, xi, eps_plus_one, reps);
	ErrorCalculator errorCalc = ErrorCalculator(trainingDataSet, *network);
	vector<double> initMemGuess;
	initMemGuess.push_back(theta0);
	errorCalc.setInitialMemory(initMemGuess);
	errorCalc.setupPlotting(names.getTrainingDataRunsNameBase(), 1);
	// 0.3 0   0.299378 -0.124334 0.299378 0.65107 -0.995245 -12.4334 2.2773 2.2773
	errorCalc.calculate(0);

	assertEquals("Output", network->getOutput(2), trainingDataSet.getOutput(trainingDataSet.viewSize() - 1, 1),
			fabs(network->getOutput(2) * 2e-6));
	assertEquals("Omega", network->getOutput(1), trainingDataSet.getOutput(trainingDataSet.viewSize() - 1, 0),
			fabs(network->getOutput(1) * 2e-5));
	cout << "netAlpha is              " << network->getOutput(3) << endl;
	cout << "netMeasure is            " << network->getOutput(2) << " while expected is "
			<< trainingDataSet.getOutput(0, 1) << endl;
	cout << "netOmega is              " << network->getOutput(1) << " while expected is "
			<< trainingDataSet.getOutput(0, 0) << endl;
	cout << "netTheta is              " << network->getOutput(0) << endl;
	assertEquals("error with same initial conditions, no memory", errorCalc.getError(), 0.0, 2e-6);
}
static void testErrorCalculator2Mem(bool useGrad)
{
	cout << "hey hey hey" << endl;
	double tau = 0.101;
	double eps_plus_one = 2.33;
	double xi = 0.013;
	double inputScale = 1.5e-1;
	unsigned reps = 1;
	const char* name = GRAY_UTILS_ROOT "/PendulumNetDemo/src/main/data/";
	NameMaintainer names(name, "testError_TwoMem_");
	FullPendulumDemoDirectTrainingDataGenerator gen(tau, eps_plus_one, xi, inputScale);
	unsigned amountOfData = 20;
	double theta0 = -0.0;
	double omega0 = 0.3;
	double sigma_alpha = 0.00;
	double sigma_input = 0.00;
	double sigma_measure = 0.0;
	unsigned minHold = 40;
	unsigned maxHold = 300;
	double dt = 0.001;
	gen.generateRandomData2mem(names.getTrainingDataFileName(), amountOfData, theta0, omega0, sigma_alpha,
			sigma_input, sigma_measure, minHold, maxHold, dt, reps);
	DataSet trainingDataSet(names.getTrainingDataFileName());
	assertEquals("trainingDataSet.InputSize", trainingDataSet.viewInputSize(), 1, 1e-5);
	assertEquals("trainingDataSet.OutputSize", trainingDataSet.viewOutputSize(), 1, 1e-5);

	NetworkStateVector state;
	AltFullPendulumNet* network = new AltFullPendulumNet(state, dt, inputScale, tau, xi, eps_plus_one, reps);
	ErrorCalculator errorCalc =
			useGrad ? GradientCalculator(trainingDataSet, *network) : ErrorCalculator(trainingDataSet, *network);
	vector<double> initMemGuess;
	initMemGuess.push_back(theta0);
	initMemGuess.push_back(omega0);
	errorCalc.setInitialMemory(initMemGuess);
	errorCalc.setupPlotting(names.getTrainingDataRunsNameBase(), 1);
	errorCalc.calculate(0);

	cout << "netAlpha is              " << network->getOutput(3) << endl;
	cout << "netMeasure is            " << network->getOutput(2) << " while expected is "
			<< trainingDataSet.getOutput(0, 0) << endl;
	cout << "netOmega is              " << network->getOutput(1) << endl;
	cout << "netTheta is              " << network->getOutput(0) << endl;
	assertEquals("Output", network->getOutput(2), trainingDataSet.getOutput(trainingDataSet.viewSize() - 1, 0),
			fabs(network->getOutput(2) * 2e-6));

	assertEquals("error with same initial conditions, no memory", errorCalc.getError(), 0.0, 1e-4);
	// The calculation of alpha is only 1e-5 accurate between the two processes, and the same accuracy is returned for
	// -(sin(theta)/(eps_plus_one*tau*tau)
}
static void testGradCalculator2Mem()
{
	cout << "hey hey hey" << endl;
	double tau = 0.101;
	double eps_plus_one = 2.33;
	double xi = 0.013;
	double inputScale = 1.5e-1;
	unsigned reps = 5;

	const char* name=GRAY_UTILS_ROOT "/PendulumNetDemo/src/main/data/";
	NameMaintainer names(name, "testGrad_TwoMem_");
	FullPendulumDemoDirectTrainingDataGenerator gen(tau, eps_plus_one, xi, inputScale);
	unsigned amountOfData = 200;
	double theta0 = -0.4;
	double omega0 = 0.5;
	double sigma_alpha = 0.20;
	double sigma_input = 1.00;
	double sigma_measure = 0.7;
	unsigned minHold = 40;
	unsigned maxHold = 300;
	double dt = 0.001;
	gen.generateRandomData2mem(names.getTrainingDataFileName(), amountOfData, theta0, omega0, sigma_alpha,
			sigma_input, sigma_measure, minHold, maxHold, dt, reps);
	DataSet trainingDataSet(names.getTrainingDataFileName());
	assertEquals("trainingDataSet.InputSize", trainingDataSet.viewInputSize(), 1, 1e-5);
	assertEquals("trainingDataSet.OutputSize", trainingDataSet.viewOutputSize(), 1, 1e-5);

	NetworkStateVector state;
	AltFullPendulumNet* network = new AltFullPendulumNet(state, dt, inputScale, tau, xi, eps_plus_one, reps);
	GradientCalculator gradCalc = GradientCalculator(trainingDataSet, *network);
	ErrorCalculator errorCalc = ErrorCalculator(trainingDataSet, *network);
	vector<double> initMemGuess;
	initMemGuess.push_back(theta0);
	initMemGuess.push_back(omega0);
	errorCalc.setInitialMemory(initMemGuess);
	gradCalc.setInitialMemory(initMemGuess);
	errorCalc.setupPlotting(names.getTrainingDataRunsNameBase(), 1);
	errorCalc.calculate(0);

	gradCalc.calculate(0);
	double originalError = errorCalc.getError();
	vector<double> newErrors;
	newErrors.push_back(originalError);
	unsigned mem = 0;
	double scale = 1e-5;
	double grad = gradCalc.getError_InitialMemory(mem);
	cout << "===Testing gradient for memory " << mem << " ===" << endl;
	for (unsigned i = 1; i < 2; i++)
	{
		vector<double> newMemGuess(initMemGuess);
		newMemGuess[mem] += scale * i;
		errorCalc.setInitialMemory(newMemGuess);
		errorCalc.calculate(i);
		double errorChange = errorCalc.getError() - originalError;
		double effectiveLinearRelationship = errorChange / (scale * i);
		cout << "effective partial" << effectiveLinearRelationship << endl;
		assertEquals("ratioOfApproximatedGradientToCalculatedGradient", grad / effectiveLinearRelationship, 1, 1e-3);
	}
	mem = 1;
	grad = gradCalc.getError_InitialMemory(mem);
	cout << "===Testing gradient for memory " << mem << " ===" << endl;
	for (unsigned i = 1; i < 2; i++)
	{
		vector<double> newMemGuess(initMemGuess);
		newMemGuess[mem] += scale * i;
		errorCalc.setInitialMemory(newMemGuess);
		errorCalc.calculate(i);
		double errorChange = errorCalc.getError() - originalError;
		double effectiveLinearRelationship = errorChange / (scale * i);
		cout << "effective partial" << effectiveLinearRelationship << endl;
		assertEquals("ratioOfApproximatedGradientToCalculatedGradient", grad / effectiveLinearRelationship, 1, 1e-3);
	}
	vector<double> originalWeights;
	network->packWeights(originalWeights);
	vector<double> scales;
	scales.push_back(1e-6);
	scales.push_back(1e-6);
	scales.push_back(1e-4);
	scales.push_back(1e-6);
	for (unsigned weight = 0; weight < originalWeights.size(); weight++)
	{
		cout << "===Testing gradient for weight " << weight << " ===" << endl;
		grad = gradCalc.getError_Weights(weight);
		for (unsigned i = 1; i < 2; i++)
		{
			vector<double> newWeights(originalWeights);
			newWeights[weight] += scales[weight] * i;
			network->setWeights(newWeights);
			errorCalc.setInitialMemory(initMemGuess);
			errorCalc.calculate(i);
			double errorChange = errorCalc.getError() - originalError;
			double effectiveLinearRelationship = errorChange / (scales[weight] * i);
			assertEquals("ratioOfApproximatedGradientToCalculatedGradient", grad / effectiveLinearRelationship, 1,
					1e-3);
		}
	}

}
void gradientDescentTrainerTest(void)
{
	cout << "-------Gradient Descent Trainer Test:----------------" << endl;
	testGradientDescent("Simple Test One Epoch With Optimal Training Rate For 2 identical points", 3, 1, 0.5, 1, 1.0,
			2);
	testGradientDescent("Simple Test One Epoch With Optimal Training Rate For 3 identical points", 5, 1, 0.5, 1, 1.0,
			3);
	testGradientDescent("Simple Test One Epoch With Optimal Training Rate For 4 identical points", 2, 1, 0.5, 1, 1.0,
			4);
	testGradientDescent("Test One Epoch With Less Than Perfect Rate", 4, 1, 0.25, 0.75, 1, 3);
	testGradientDescent("Test Convergence over 1000 epochs", 3, 1000, 0.05, 256, 256, 55);
	cout << "================== Testing the error calculator ================ " << endl;
	testErrorCalculatorNoMem();
	testErrorCalculator1Mem();
	testErrorCalculator2Mem(false);
	testErrorCalculator2Mem(true);
	cout << "================== Testing the gradient calculator ================ " << endl;
	testGradCalculator2Mem();
	cout << "-------Gradient Descent Trainer Test Complete!-------" << endl;

}
}
}
}

