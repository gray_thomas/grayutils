/*
 * TestStateTrainer.cpp
 *
 *    Created on: Jan 21, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "SparseHessianSolver.hpp"
#include <Eigen/Dense>
#include <iostream>
#include "TestUtils.hpp"
#include <Eigen/Cholesky>
using namespace Eigen;
using namespace std;
using namespace uta::testUtils;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
namespace tests
{
void testStateMachine(void)
{
	cout << "testing state solver's state machine" << endl;
	/** First draft iterative API:
	 * First: Initialize with known problem size, for static memory allocation.
	 * Second: Add estimations and state updates until dataSize updates have happened.
	 * Note: References to the (dense) matrixes is provided, matrixes are copied into place.
	 * Note: At this point additional states added will throw an error
	 * Note: addStateUpdate acts as state machine {InitialState, MiddleState, FinalState}
	 * Third: Ask for the solution to begin the backwards substitution process.
	 * Forth: Reference to solution is returned.
	 * */
	unsigned stateSize, parameterSize, dataSize, measureSize;
	stateSize = 1;
	parameterSize = 1;
	dataSize = 2;
	measureSize = 2;
	SparseHessianSolver iterativeSolver(stateSize, parameterSize, dataSize);
	MatrixXd B, K, A, J, G, H;
	VectorXd y, x;
	B.setIdentity(measureSize, stateSize);
	K.setIdentity(measureSize, parameterSize);
	y.setZero(measureSize);
	G.setIdentity(measureSize, measureSize);
	A.setIdentity(stateSize, stateSize);
	J.setIdentity(stateSize, parameterSize);
	x.setZero(stateSize);
	H.setIdentity(stateSize, stateSize);

	iterativeSolver.addEstimation(B, K, G, y);
	iterativeSolver.addStateUpdate(A, J, H, x);
	iterativeSolver.addEstimation(B, K, G, y);
	bool exceptionThrown = false;
	try
	{
		iterativeSolver.addStateUpdate(A, J, H, x);
	} catch (runtime_error& e)
	{
		exceptionThrown = true;
	}
	if (!exceptionThrown)
		throw AssertionFailedException("should have thrown exception with addition of new state transition!");
	cout << "state machine for state solver passed its test" << endl;
}
void testEigenCholesky(void)
{
	cout << "testing Eigen library's Cholesky module" << endl;
	MatrixXd A = MatrixXd::Identity(3, 3);
	A << 4, 12, -16, 12, 37, -43, -16, -43, 98;
	cout << A << endl;
	LLT<MatrixXd> LofA(A.selfadjointView<Eigen::Lower>());
	MatrixXd L = LofA.matrixL();
	cout << "L\n" << L << endl;
	MatrixXd U = LofA.matrixU();
	cout << "U\n" << U << endl;
	MatrixXd Aprime = L * U;
	cout << Aprime << endl;
//	L.triangularView<Eigen::Lower>().solve();

	cout << "Eigen's Cholesky library seems to have passed." << endl;
}
void testCholeskySolverWithRandomMatrices(unsigned stateSize, unsigned parameterSize, unsigned dataSize,
		unsigned measureSize, double tolerance, bool useTrust=false)
{
	SparseHessianSolver iterativeSolver(stateSize, parameterSize, dataSize);
	MatrixXd B, K, A, J, G, H;
	VectorXd y, x;
	B.setIdentity(measureSize, stateSize) *= 2;
	K.setIdentity(measureSize, parameterSize) *= 3;
	y.setZero(measureSize);
	G.setIdentity(measureSize, measureSize) *= 4;
	A.setIdentity(stateSize, stateSize) *= 5;
	J.setIdentity(stateSize, parameterSize) *= 6;
	x.setZero(stateSize);
	H.setIdentity(stateSize, stateSize) *= 7;
	MatrixXd denseRTSigmaR;
	MatrixXd denseR, denseSigma;
	VectorXd denseY, denseX;
	unsigned totalMeasurementDimension = dataSize * (stateSize + measureSize) - stateSize;
	unsigned totalParametersDimension = dataSize * stateSize + parameterSize;
	denseR.setZero(totalMeasurementDimension, totalParametersDimension);
	denseSigma.setZero(totalMeasurementDimension, totalMeasurementDimension);
	denseY.setZero(totalMeasurementDimension);
	denseX.setZero(totalParametersDimension);
	denseRTSigmaR.setZero(totalParametersDimension,totalParametersDimension);

	B = MatrixXd::Random(measureSize, stateSize);
	K = MatrixXd::Random(measureSize, parameterSize);
	G.setIdentity(measureSize, measureSize);
	y = VectorXd::Random(measureSize);

	iterativeSolver.addEstimation(B, K, G, y);
	unsigned end = stateSize * dataSize;
	unsigned measurementStart = 0;
	unsigned statesStart = 0;
	denseR.block(measurementStart, statesStart, measureSize, stateSize) = B;
	denseR.block(measurementStart, end, measureSize, parameterSize) = K;
	denseSigma.block(measurementStart, measurementStart, measureSize, measureSize) = G;
	denseY.block(measurementStart, 0, measureSize, 1) = y;
	measurementStart += measureSize;
	statesStart += 0;

	for (unsigned i = 0; i < dataSize - 1; i++)
	{
		A = MatrixXd::Identity(stateSize, stateSize) + MatrixXd::Random(stateSize, stateSize);
		J = MatrixXd::Random(stateSize, parameterSize);
		H.setIdentity(stateSize, stateSize);
		x = VectorXd::Random(stateSize);

		iterativeSolver.addStateUpdate(A, J, H, x);
		denseR.block(measurementStart, statesStart, stateSize, stateSize) = A;
		denseR.block(measurementStart, statesStart + stateSize, stateSize, stateSize) = -MatrixXd::Identity(stateSize,
				stateSize);
		denseR.block(measurementStart, end, stateSize, parameterSize) = J;
		denseSigma.block(measurementStart, measurementStart, stateSize, stateSize) = H;
		denseY.block(measurementStart, 0, stateSize, 1) = x;
		measurementStart += stateSize;
		statesStart += stateSize;

		B = MatrixXd::Random(measureSize, stateSize);
		K = MatrixXd::Random(measureSize, parameterSize);
		G.setIdentity(measureSize, measureSize);
		y = VectorXd::Random(measureSize);

		iterativeSolver.addEstimation(B, K, G, y);
		denseR.block(measurementStart, statesStart, measureSize, stateSize) = B;
		denseR.block(measurementStart, end, measureSize, parameterSize) = K;
		denseSigma.block(measurementStart, measurementStart, measureSize, measureSize) = G;
		denseY.block(measurementStart, 0, measureSize, 1) = y;
		measurementStart += measureSize;
		statesStart += 0;
	}
	iterativeSolver.finishAndGetSolution();
	denseRTSigmaR += denseR.transpose() * denseSigma * denseR;
	LLT<MatrixXd> lltObject = LLT<MatrixXd>(denseRTSigmaR);
	VectorXd denseScaledY = denseR.transpose() * denseSigma * denseY;
	VectorXd denseForwardSolution = lltObject.matrixL().solve(denseScaledY);
	VectorXd result = lltObject.solve((const MatrixXd &) (denseR.transpose() * denseSigma * denseY));
	MatrixXd denseL = MatrixXd(lltObject.matrixL());

	for (unsigned i = 0; i < dataSize; i++)
		assertEqualMatrices("RTSigmaRBlockDiagonal", (MatrixXd) iterativeSolver.getHessianDiag(i),
				(MatrixXd) denseRTSigmaR.block(i * stateSize, i * stateSize, stateSize, stateSize), tolerance);
	for (unsigned i = 0; i < dataSize - 1; i++)
		assertEqualMatrices("RTSigmaRBlockSuperDiagonal", (MatrixXd) iterativeSolver.getHessianSuperDiag(i),
				(MatrixXd) denseRTSigmaR.block(i * stateSize, (i + 1) * stateSize, stateSize, stateSize), tolerance);
	for (unsigned i = 0; i < dataSize - 1; i++)
		assertEqualMatrices("RTSigmaRBlockSubDiagonal", (MatrixXd) iterativeSolver.getHessianSubDiag(i),
				(MatrixXd) denseRTSigmaR.block((i + 1) * stateSize, i * stateSize, stateSize, stateSize), tolerance);
	for (unsigned i = 0; i < dataSize; i++)
		assertEqualMatrices("RTSigmaRBlockSuperBorder", iterativeSolver.getHessianSubBorder(i),
				(MatrixXd) denseRTSigmaR.block(dataSize * stateSize, i * stateSize, parameterSize, stateSize),
				tolerance);
	assertEqualMatrices("RTSigmaRTerminalBlock", iterativeSolver.getHessianDiagonalEnd(),
			(MatrixXd) denseRTSigmaR.block(dataSize * stateSize, dataSize * stateSize, parameterSize, parameterSize),
			tolerance);

	for (unsigned i = 0; i < dataSize; i++)
		assertEqualMatrices("LDiag", iterativeSolver.getLDiag(i),
				(MatrixXd) denseL.block(stateSize * i, stateSize * i, stateSize, stateSize), tolerance);
	for (unsigned i = 0; i < dataSize - 1; i++)
		assertEqualMatrices("LSubDiag", iterativeSolver.getLSubDiag(i),
				(MatrixXd) denseL.block((i + 1) * stateSize, stateSize * i, stateSize, stateSize), tolerance);
	for (unsigned i = 0; i < dataSize; i++)
		assertEqualMatrices("LSubBorder", iterativeSolver.getLSubBoarder(i),
				(MatrixXd) denseL.block(dataSize * stateSize, stateSize * i, parameterSize, stateSize), tolerance);
	assertEqualMatrices("LEnd", iterativeSolver.getLEnd(),
			(MatrixXd) denseL.block(dataSize * stateSize, dataSize * stateSize, parameterSize, parameterSize),
			tolerance);
	for (unsigned i = 0; i < dataSize; i++)
		assertEqualMatrices("ScaledY", iterativeSolver.getGradient(i),
				(MatrixXd) denseScaledY.block(stateSize * i, 0, stateSize, 1), tolerance);
	for (unsigned i = 0; i < dataSize; i++)
		assertEqualMatrices("ForwardSolution", iterativeSolver.getForwardSolution(i),
				(MatrixXd) denseForwardSolution.block(stateSize * i, 0, stateSize, 1), tolerance);
	assertEqualMatrices("ForwardSolutionEnd", iterativeSolver.getForwardSolutionEnd(),
			(MatrixXd) denseForwardSolution.block(stateSize * dataSize, 0, parameterSize, 1), tolerance);

	assertEqualMatrices("BackSolutionEnd", iterativeSolver.getBackSolutionEnd(),
			(MatrixXd) result.block(stateSize * dataSize, 0, parameterSize, 1), tolerance);
	for (unsigned i = 0; i < dataSize; i++)
		assertEqualMatrices("BackSolution", iterativeSolver.getBackSolution((dataSize - 1 - i)),
				(MatrixXd) result.block(stateSize * (dataSize - 1 - i), 0, stateSize, 1), tolerance);

	assertEqualMatrices("solution", iterativeSolver.getSolution(), result, tolerance);
}
void testSparseCholeskySolver(void)
{
	cout << "testing state solver's solution accuracy." << endl;
	std::srand(2341325);
	unsigned stateSize, parameterSize, dataSize, measureSize;
	stateSize = 3;
	parameterSize = 3;
	dataSize = 8;
	measureSize = 3;
	for (unsigned i = 0; i < 20; i++)
		testCholeskySolverWithRandomMatrices(stateSize, parameterSize, dataSize, measureSize, 1e-8);
	stateSize = 2;
	parameterSize = 3;
	dataSize = 20;
	measureSize = 2;
	for (unsigned i = 0; i < 5; i++)
		testCholeskySolverWithRandomMatrices(stateSize, parameterSize, dataSize, measureSize, 1e-8);
	stateSize = 4;
	parameterSize = 7;
	dataSize = 12;
	measureSize = 3;
	for (unsigned i = 0; i < 5; i++)
		testCholeskySolverWithRandomMatrices(stateSize, parameterSize, dataSize, measureSize, 1e-8);
	cout << "sparse solver is accurate" << endl;

}
}
}
}
}
