/*
 * TestBackPropThroughTime.cpp
 *
 *    Created on: Dec 3, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "TestBackPropThroughTime.hpp"
#include "Network.hpp"
#include "Trainable.hpp"
#include <vector>
#include <iostream>
#include <random>
#include "TestUtils.hpp"
#include "DataProvider.hpp"
#include "ScalarMultiplicationElement.hpp"
#include "ScalarAdditionElement.hpp"
#include "TrainableScalarElement.hpp"
#include "ScalarConstant.hpp"
#include "DataSet.hpp"
#include "GradientDescentTrainer.hpp"
#include "BackPropThroughTimeTrainer.hpp"
using namespace uta::networkUtils;
using namespace uta::testUtils;
using namespace std;
#define TRAINING_NETWORK_SIZE 6
namespace uta
{
namespace networkUtils
{
namespace tests
{

class TestableBackPropThroughTimeTrainer: public GradientDescentTrainer
{
public:
	TestableBackPropThroughTimeTrainer(unsigned memSize) :
			GradientDescentTrainer()
	{

	}

	const vector<double>& viewNonEliteWeights(void)
	{
		return this->weights;
	}
	const vector<double>& viewEliteWeights(void)
	{
		return this->eliteWeights;
	}
	const vector<double>& viewNonEliteInitialMemory(void)
	{
		return this->initialMemory;
	}
	const vector<double>& viewEliteInitialMemory(void)
	{
		return this->eliteInitialMemory;
	}

	double getFinalError(void)
	{
		return this->testError;
	}
};

class BackPropThroughTimeNet: public Network
{
public:
	BackPropThroughTimeNet(NetworkStateVector& state) :
			Network(state, 2)
	{
		SingleIndexNetworkDataProxy* velocityIn;
		SingleIndexNetworkDataProxy* positionMemIn;
		ScalarAdditionElement* positionMemOut = new ScalarAdditionElement(state);
		ScalarAdditionElement* positionTimes2OutPut = new ScalarAdditionElement(state);
		TrainableScalarElement* velocityGain;
		ScalarMultiplicationElement* scaledVelocity = new ScalarMultiplicationElement(state);
		NoIndexNetworkDataProxy* proxy = getGeneratorNetworkInputProxy();
		positionMemIn = getNetworkInput(0);
		velocityIn = getNetworkInput(1);
		velocityGain = new TrainableScalarElement(state, proxy, 1.0);

		scaledVelocity->addInput(velocityGain->getScalarResult());
		scaledVelocity->addInput(velocityIn);

		positionMemOut->addInput(positionMemIn);
		positionMemOut->addInput(scaledVelocity->getScalarResult());

		positionTimes2OutPut->addInput(positionMemOut->getScalarResult());
		positionTimes2OutPut->addInput(positionMemOut->getScalarResult());

		setAsOutput(positionMemOut->getScalarResult());
		setAsOutput(positionTimes2OutPut->getScalarResult());

	}
};
class BackPropThroughTimeNetDataSet: public DataSet
{
public:
	BackPropThroughTimeNetDataSet(unsigned numberOfPoints, unsigned inputHold, double weight, double startingInput,
			double startingState) :
			DataSet(1, 1)
	{
		double x, u;
		u = startingInput;
		x = startingState;
		unsigned hold = inputHold;
		for (unsigned i = 0; i < numberOfPoints; i++)
		{
			if (hold-- <= 0)
			{
				hold = inputHold;
				u = -2 * u;
			}
			double inputs[1];
			double outputs[1];
			x += weight * u;

			inputs[0] = u;
			outputs[0] = 2 * x;

			this->addLine(inputs, outputs);
		}
	}
};

class SimpleDataSet: public DataSet
{
public:
	SimpleDataSet(unsigned numberOfPoints) :
			DataSet(1, 1)
	{
		for (unsigned i = 0; i < numberOfPoints; i++)
		{
			double ins[1];
			double outs[1];
			ins[0] = 0;
			outs[0] = 0;
			this->addLine(ins, outs);
		}
	}
};
//class SingleStateGeneralBPTTDataSet: public DataSet
//{
//public:
//	SingleStateGeneralBPTTDataSet(unsigned numberOfPoints) :
//				DataSet(1, 1)
//	{
//		for (unsigned i = 0; i < numberOfPoints; i++)
//		{
//			generateInputs(i);
//			generateOutputs(i);
//			outputs[0] = 0;
//			this->addLine(inputs, outputs);
//		}
//	}
//	virtual ~SingleStateGeneralBPTTDataSet()
//	{
//	}
//protected:
//	double inputs[1];
//	double outputs[1];
//	virtual void generateInputs(unsigned i)=0;
//	virtual void generateOutputs(unsigned i)=0;
//};
class ForcedLinearSystemDataSet: public DataSet
{
private:
	double A, B, C, D, x, u, dt, amp, omega;
public:
	ForcedLinearSystemDataSet(unsigned numberOfPoints, double A, double B, double C, double D, double x0, double dt,
			double amplitude, double omega) :
			DataSet(1, 1), A(A), B(B), C(C), D(D), x(x0), u(0), dt(dt), amp(amplitude), omega(omega), ins(), outs()

	{
		for (unsigned i = 0; i < numberOfPoints; i++)
		{
			generateInputs(i);
			generateOutputs(i);
			this->addLine(ins, outs);
		}

	}
	~ForcedLinearSystemDataSet()
	{
	}
protected:
	double ins[1];
	double outs[1];
	void generateInputs(unsigned i)
	{
		u = amp * sin(omega * i * dt);
		ins[0] = u;
	}
	void generateOutputs(unsigned i)
	{
		outs[0] = C * x + D * u;
		x = x + dt * (A * x + B * u);
	}
};

class ForcedLinearSystemNetNoTrainables: public uta::networkUtils::Network
{
public:

	ForcedLinearSystemNetNoTrainables(NetworkStateVector& state, double A, double B, double C, double D, double dt) :
			Network(state, 2)
	{
		SingleIndexNetworkDataProxy x = *getNetworkInput(0);
		SingleIndexNetworkDataProxy u = *getNetworkInput(1);
//		SingleIndexNetworkDataProxy newX = x + constant(dt) * (constant(A) * x + constant(B) * u);
//		SingleIndexNetworkDataProxy y = constant(C) * x + constant(D) * u;

		setAsOutput(&(x + constant(dt) * (constant(A) * x + constant(B) * u)));
		setAsOutput(&(constant(C) * x + constant(D) * u));
	}
};
class ForcedLinearSystemNetOneTrainable: public uta::networkUtils::Network
{
public:

	ForcedLinearSystemNetOneTrainable(NetworkStateVector& state, double A, double B, double C, double D, double dt,
			double biasGuess, double biasScale) :
			Network(state, 2)
	{
		SingleIndexNetworkDataProxy x = *getNetworkInput(0);
		SingleIndexNetworkDataProxy u = *getNetworkInput(1);
//		SingleIndexNetworkDataProxy newX = x + constant(dt) * (constant(A) * x + constant(B) * u);
//		SingleIndexNetworkDataProxy y = constant(C) * x + constant(D) * u;

		setAsOutput(&(x + constant(dt) * (constant(A) * x + constant(B) * u)));
		setAsOutput(&(constant(C) * x + constant(D) * u + constant(biasScale) * trainable(biasGuess)));
	}
};

static void testBackPropSimpleLinearCaseConvergesForThisGuess(double x0Guess)
{

	NetworkStateVector state;
	double A, B, C, D, dt, x0, amp, omega;
	A = -1;
	B = 1;
	C = 1;
	D = 1;
	dt = 0.01;
	x0 = 1.0;
	amp = 0.7;
	omega = 0.5;
	Trainable* network = new ForcedLinearSystemNetNoTrainables(state, A, B, C, D, dt);
	ForcedLinearSystemDataSet dataSet(100, A, B, C, D, x0, dt, amp, omega);
	TestableBackPropThroughTimeTrainer trainer(1);

	double initialX0Estimate = x0Guess;
	trainer.setEpochMax(30);
	vector<double> initialMemory;
	initialMemory.push_back(initialX0Estimate);
	trainer.setInitialMemory(initialMemory);
	trainer.setupLogging("src/main/data/testBackPropSimpleLinearCaseConvergesForGuess", 1);
	trainer.trainNetwork(dataSet, dataSet, *network, 0.5);
	const vector<double>& eliteMem = trainer.viewEliteInitialMemory();
	assertEquals("memoryShouldHaveMovedTowardsTheActualValue,Fast!", (eliteMem[0] - x0) / (initialX0Estimate - x0), 0,
			1e-2);
}
static void testBackPropSimpleLinearCaseConvergesForThisGuessWithModelError(double x0Guess,
		default_random_engine& generator)
{
	normal_distribution<double> distribution(0, 0.04);
	NetworkStateVector state;
	double A, B, C, D, dt, x0, amp, omega;
	A = -1;
	B = 1;
	C = 1;
	D = 1;
	dt = 0.01;
	x0 = 1.0;
	amp = 0.7;
	omega = 0.5;
	Trainable* network = new ForcedLinearSystemNetNoTrainables(state, A + distribution(generator),
			B + distribution(generator), C + distribution(generator), D + distribution(generator), dt);
	ForcedLinearSystemDataSet dataSet(20, A, B, C, D, x0, dt, amp, omega);
	TestableBackPropThroughTimeTrainer trainer(1);

	double initialX0Estimate = x0Guess;
	trainer.setEpochMax(200);
	vector<double> initialMemory;
	initialMemory.push_back(initialX0Estimate);
	trainer.setInitialMemory(initialMemory);
	trainer.trainNetwork(dataSet, dataSet, *network, 0.5);
	const vector<double>& eliteMem = trainer.viewEliteInitialMemory();
	assertEquals("Despite model error, memory should have moved noticeably towards the actual value.",
			(eliteMem[0] - x0) / (initialX0Estimate - x0), 0, 3e-1);
}

static void testBackPropConvergesForLinearSystemWithNoWeightsAndOneInitialCondition()
{
	cout << endl << "\t\t=======testBackPropConvergesForLinearSystemWithNoWeightsAndOneInitialCondition===" << endl;

	testBackPropSimpleLinearCaseConvergesForThisGuess(10.0);
	testBackPropSimpleLinearCaseConvergesForThisGuess(0.1);
	testBackPropSimpleLinearCaseConvergesForThisGuess(-10.0);
	default_random_engine generator(3247623);
	for (unsigned i = 0; i < 20; i++)
		testBackPropSimpleLinearCaseConvergesForThisGuessWithModelError(0.0, generator);
	cout << "\t\t===END=testBackPropConvergesForLinearSystemWithNoWeightsAndOneInitialCondition===" << endl << endl;
}

static void assertOneWeightOneMemConvergesWithUncertainty(double x0Guess, double biasGuess, default_random_engine& gen)
{
	double sigma = 0.07;
	normal_distribution<double> dist(0, sigma);
	NetworkStateVector state;
	double A, B, C, D, dt, x0, amp, omega;
	A = -1;
	B = 1;
	C = 1;
	D = 1;
	dt = 0.01;
	double biasOriginalGuess = biasGuess;
	double biasScale = 0.05 * dist(gen) / sigma;
	double trueBias = 0.0;
	x0 = 6.0;
	amp = 100.7;
	omega = 0.5;
	Trainable* network = new ForcedLinearSystemNetOneTrainable(state, A + dist(gen), B + dist(gen), C + dist(gen),
			D + dist(gen), dt, biasOriginalGuess, biasScale);
	ForcedLinearSystemDataSet dataSet(9, A, B, C, D, x0, dt, amp, omega);
	TestableBackPropThroughTimeTrainer trainer(1);

	double initialX0Estimate = x0Guess;
	trainer.setEpochMax(400);
	vector<double> initialMemory;
	initialMemory.push_back(initialX0Estimate);
	trainer.setInitialMemory(initialMemory);
	trainer.trainNetwork(dataSet, dataSet, *network, 0.1);
	const vector<double>& eliteMem = trainer.viewEliteInitialMemory();
	const vector<double>& eliteWeight = trainer.viewEliteWeights();
	assertEquals("Despite uncertainty, result should be closer to the actual value!",
			(eliteMem[0] - x0) * (eliteWeight[0] - trueBias)
					/ ((initialX0Estimate - x0) * (biasOriginalGuess - trueBias)), 0, 3e-1);
}

static void testBackPropConvergesForLinearSystemOneWeight()
{
	cout << endl << "\t\t=======testBackPropConvergesForLinearSystemOneWeight===" << endl;

	NetworkStateVector state;
	double A, B, C, D, dt, x0, amp, omega;
	A = -1;
	B = 1;
	C = 1;
	D = 1;
	dt = 0.01;
	double biasOriginalGuess = 0.5;
	double biasScale = 0.05;
	double trueBias = 0.0;
	x0 = 1.0;
	amp = 0.7;
	omega = 0.5;
	Trainable* network = new ForcedLinearSystemNetOneTrainable(state, A, B, C, D, dt, biasOriginalGuess, biasScale);
	ForcedLinearSystemDataSet dataSet(6, A, B, C, D, x0, dt, amp, omega);
	TestableBackPropThroughTimeTrainer trainer(1);

	double initialX0Estimate = 0.0;
	trainer.setEpochMax(100);
	vector<double> initialMemory;
	initialMemory.push_back(initialX0Estimate);
	trainer.setInitialMemory(initialMemory);
	trainer.trainNetwork(dataSet, dataSet, *network, 0.1);
	const vector<double>& eliteMem = trainer.viewEliteInitialMemory();
	const vector<double>& eliteWeight = trainer.viewEliteWeights();
	assertEquals("result should be closer to the actual value!",
			(eliteMem[0] - x0) * (eliteWeight[0] - trueBias)
					/ ((initialX0Estimate - x0) * (biasOriginalGuess - trueBias)), 0, 1e-1);
	default_random_engine generator(3247623);
	for (unsigned i = 0; i < 20; i++)
		assertOneWeightOneMemConvergesWithUncertainty(1.3, 0.3, generator);

	cout << "\t\t===END=testBackPropConvergesForLinearSystemOneWeight===" << endl << endl;
}

static void assertLinearNetworkBehaves(double A, double B, double C, double D, double dt, double x0, double u,
		double ddx, double ddy)
{
	NetworkStateVector state;
	Trainable* network = new ForcedLinearSystemNetNoTrainables(state, A, B, C, D, dt);
	network->setInput(0, x0);
	network->setInput(1, u);
	network->calculateOutputs();
	assertEquals("OutputX", network->getOutput(0), x0 + dt * (A * x0 + B * u), 1e-6);
	assertEquals("OutputY", network->getOutput(1), C * x0 + D * u, 1e-6);

	network->setOutputPartial(0, ddx);
	network->setOutputPartial(1, ddy);
	network->calculatePartials();
	assertEquals("PartialX", network->getInputPartial(0), ddx * (1 + dt * A) + ddy * (C), 1e-6);
	assertEquals("PartialU", network->getInputPartial(1), ddx * (dt * B) + ddy * (D), 1e-6);
}
static void testLinearSystemTestNetwork()
{
	cout << "==============testLinearSystemTestNetwork==========" << endl;

	default_random_engine generator(23402);
	normal_distribution<double> distribution(0, 20);
	double A, B, C, D, dt, x0, u, ddx, ddy;
	for (int i = 0; i < 20; i++)
	{
		A = distribution(generator);
		B = distribution(generator);
		C = distribution(generator);
		D = distribution(generator);
		dt = distribution(generator);
		x0 = distribution(generator);
		ddx = distribution(generator);
		ddy = distribution(generator);
		u = distribution(generator);
		assertLinearNetworkBehaves(A, B, C, D, dt, x0, u, ddx, ddy);
	}
	A = -1;
	B = 1;
	C = 1;
	D = 1;
	dt = 0.5;
	x0 = 1.0;
	ddx = 1.0;
	ddy = 0.5;
	u = 0;
	assertLinearNetworkBehaves(A, B, C, D, dt, x0, u, ddx, ddy);
	cout << "=========END=testLinearSystemTestNetwork=END=======" << endl;
}

static void testPerfectInitialConditionsResultInZeroError()
{
	cout << "===testPerfectInitialConditionsResultInZeroError===" << endl;
	NetworkStateVector state;
	double A, B, C, D, dt, x0, amp, omega;
	A = -1;
	B = 1;
	C = 1;
	D = 1;
	dt = 0.5;
	x0 = 1.0;
	double initialX0Estimate = 1.0;
	amp = 1.0;
	omega = 0.5;
	double u = amp * sin(omega * 0 * dt);
	double expectedY = C * x0 + D * u;

	Trainable* network = new ForcedLinearSystemNetNoTrainables(state, A, B, C, D, dt);
	ForcedLinearSystemDataSet dataSet(1, A, B, C, D, x0, dt, amp, omega);
	TestableBackPropThroughTimeTrainer trainer(1);
	trainer.setEpochMax(1);
	vector<double> initialMemory;
	initialMemory.push_back(initialX0Estimate);
	trainer.setInitialMemory(initialMemory);

	trainer.trainNetwork(dataSet, dataSet, *network, 0.2);

	const vector<double>& finalMem = trainer.viewNonEliteInitialMemory();
	const vector<double>& finalWeights = trainer.viewNonEliteWeights();
	assertEquals("weightsShouldHaveSizeZero", finalWeights.size(), 0.0, 1e-6);
	assertEquals("memoryShouldHaveSizeOne", finalMem.size(), 1.0, 1e-6);
	assertEquals("dataSetOutput", dataSet.getOutput(0, 0), expectedY, 1e-6);

	const vector<double>& eliteMem = trainer.viewEliteInitialMemory();
	assertEquals("memoryShouldNotHaveChangedInOneEpoch", eliteMem[0], initialX0Estimate, 1e-6);
	assertEquals("finalErrorShouldBeZeroOnFirstEpochWithPefectICs", trainer.getFinalError(), 0.0, 1e-6);
	cout << "==========END==========================END=========" << endl;
}

//static void testBackPropThroughTime(string message, double rate, double expectedWeight, double idealWeight,
//									unsigned dataPoints)
//{
//	NetworkStateVector state;
//	Trainable* network = new BackPropThroughTimeNet(state);
//	BackPropThroughTimeNetDataSet trainingSet(dataPoints, dataPoints, idealWeight, 1.0, 0.0);
//	BackPropThroughTimeTrainer trainer(1);
//	trainer.setTimeLimit(0.2);
//	trainer.setEpochMax(1);
//	trainer.trainNetwork(trainingSet, trainingSet, *network, rate);
//
//	vector<double> endWeights = network->getWeights();
//	assertEquals(message, endWeights[0], expectedWeight, 1e-6);
//}
//static void testOneEpochOnePoint(string message, double rate, double expectedWeight, double idealWeight,
//									unsigned dataPoints)
//{
//	NetworkStateVector state;
//	Trainable* network = new BackPropThroughTimeNet(state);
//	BackPropThroughTimeNetDataSet trainingSet(dataPoints, dataPoints, idealWeight, 1.0, 0.0);
//	BackPropThroughTimeTrainer trainer(1);
//	trainer.setTimeLimit(0.2);
//	trainer.setEpochMax(1);
//	trainer.trainNetwork(trainingSet, trainingSet, *network, rate);
//
//	vector<double> endWeights = network->getWeights();
//	assertEquals(message, endWeights[0], expectedWeight, 1e-6);
//}

/*class ForcedLinearSystemNetNoTrainables: public uta::networkUtils::Network
 {
 public:

 ForcedLinearSystemNetNoTrainables(NetworkStateVector& state, double A, double B, double C, double D, double dt) :
 Network(state, 2)
 {
 SingleIndexNetworkDataProxy x = *getNetworkInput(0);
 SingleIndexNetworkDataProxy u = *getNetworkInput(1);
 //		SingleIndexNetworkDataProxy newX = x + constant(dt) * (constant(A) * x + constant(B) * u);
 //		SingleIndexNetworkDataProxy y = constant(C) * x + constant(D) * u;

 setAsOutput(&(x + constant(dt) * (constant(A) * x + constant(B) * u)));
 setAsOutput(&(constant(C) * x + constant(D) * u));
 }
 };*/
class UnwrappableExampleNetwork: public Network
{
public:
	UnwrappableExampleNetwork(NetworkStateVector& state, unsigned layers);

};
UnwrappableExampleNetwork::UnwrappableExampleNetwork(NetworkStateVector& state, unsigned layers) :
		Network(state, (layers + 2))
{
	SingleIndexNetworkDataProxy wA = trainable(-0.25);
	SingleIndexNetworkDataProxy wB = trainable(-0.001);
	SingleIndexNetworkDataProxy wC = trainable(1.25);
	SingleIndexNetworkDataProxy wD = trainable(0.6);
	SingleIndexNetworkDataProxy wE = trainable(1.25);
	SingleIndexNetworkDataProxy x = *this->getNetworkInput(0);
	SingleIndexNetworkDataProxy xd = *this->getNetworkInput(1);
	vector<SingleIndexNetworkDataProxy> outputs;
	for (unsigned i = 0; i < layers; i++)
	{
		SingleIndexNetworkDataProxy u = *getNetworkInput(i + 2);
		x = x + xd;
		xd = xd + wA * x + wB * xd + wC * u;
		SingleIndexNetworkDataProxy y = wD * x + wE * xd;
		outputs.push_back(y);
	}
	setAsOutput(&x);
	setAsOutput(&xd);
	for (unsigned i = 0; i < layers; i++)
		setAsOutput(&outputs[i]);
}
class SpoofData: public DataProvider
{
	VectorXd inputs;
	VectorXd outputs;
	bool unrolled;
	unsigned layersOrSize;
public:
	SpoofData(long seed, unsigned layers) :
			inputs(layers), outputs(layers), unrolled(false), layersOrSize(layers)
	{
		srand(seed);
		inputs = VectorXd::Random(layers);
		outputs = VectorXd::Random(layers);
	}
	void setUnrolled(bool unroll)
	{
		this->unrolled = unroll;
	}
	unsigned viewSize(void)
	{
		if (unrolled)
			return 1;
		else
			return layersOrSize;
	}
	unsigned viewInputSize(void)
	{
		if (unrolled)
			return layersOrSize;
		else
			return 1;
	}
	unsigned viewOutputSize(void)
	{
		if (unrolled)
			return layersOrSize;
		else
			return 1;
	}
	void packInputs(unsigned index, vector<double> &ins)
	{
		if (unrolled)
		{
			if (index != 0)
				throw runtime_error("size is only 1");
			if (ins.size() != layersOrSize)
				throw runtime_error("ins should have size matching layers");
			for (unsigned i = 0; i < ins.size(); i++)
				ins[i] = inputs(i);
		}
		else
		{
			if (ins.size() != 1)
				throw runtime_error("ins should have size 1");
			ins[0] = inputs(index);
		}
	}
	void packOutputs(unsigned index, vector<double> &outs)
	{
		if (unrolled)
		{
			if (index != 0)
				throw runtime_error("size is only 1");
			if (outs.size() != layersOrSize)
				throw runtime_error("outs should have size matching layers");
			for (unsigned i = 0; i < outs.size(); i++)
				outs[i] = outputs(i);
		}
		else
		{
			if (outs.size() != 1)
				throw runtime_error("outs should have size 1");
			outs[0] = outputs(index);
		}
	}
	double getOutput(unsigned dataIndex, unsigned outIndex)
	{
		if (unrolled)
		{
			if (dataIndex != 0)
				throw runtime_error("size is only 1");
			return outputs[outIndex];
		}
		else
		{
			if (outIndex != 0)
				throw runtime_error("ouputs should have size 1");
			return outputs[dataIndex];
		}
	}
	double getInput(unsigned dataIndex, unsigned inputIndex)
	{
		if (unrolled)
		{
			if (dataIndex != 0)
				throw runtime_error("size is only 1");
			return inputs[inputIndex];
		}
		else
		{
			if (inputIndex != 0)
				throw runtime_error("inputs should have size 1");
			return inputs[dataIndex];
		}
	}

};
void testCorrectnessOfForwardIterativeBackPropThroughTime(void)
{
	cout << "-------testCorrectnessOfForwardIterativeBackPropThroughTime----------------" << endl;
	unsigned numLayers = 10;
	unsigned numWeights = 5;
	NetworkStateVector forwardIterationBackPropState;
	NetworkStateVector backIterationState;
	UnwrappableExampleNetwork* forwardIterationNet = new UnwrappableExampleNetwork(forwardIterationBackPropState, 1);
	UnwrappableExampleNetwork* backIterationNet = new UnwrappableExampleNetwork(backIterationState, numLayers);
	SpoofData dataSpoof(1337, numLayers);
	dataSpoof.setUnrolled(true);
	GradientCalculator unwrappedCalc(dataSpoof, *backIterationNet);
	unwrappedCalc.setErrorThreshold(1e99);
	unwrappedCalc.calculate(0);
	vector<double> errorWeightsUnrolled;
	for (unsigned i = 0; i < numWeights; i++)
		errorWeightsUnrolled.push_back(unwrappedCalc.getError_Weights(i));

	dataSpoof.setUnrolled(false);
	GradientCalculator iterativeCalc(dataSpoof, *forwardIterationNet);
	iterativeCalc.setErrorThreshold(1e99);
	iterativeCalc.calculate(0);
	vector<double> errorWeightsIterative;
	for (unsigned i = 0; i < numWeights; i++)
		errorWeightsIterative.push_back(iterativeCalc.getError_Weights(i));

	for (unsigned i = 0; i < numWeights; i++)
		assertEquals("Weight Partial", numLayers*errorWeightsIterative[i], errorWeightsUnrolled[i], 1e-6);
	cout << "-------testCorrectnessOfForwardIterativeBackPropThroughTime passed!-------" << endl;

}
void backPropThroughTimeTest(void)
{
	cout << "-------Back Prop Through Time Test:----------------" << endl;
	testLinearSystemTestNetwork();
	testPerfectInitialConditionsResultInZeroError();
	testBackPropConvergesForLinearSystemWithNoWeightsAndOneInitialCondition();
	testBackPropConvergesForLinearSystemOneWeight();
	cout << "-------Back Prop Through Time Test Complete!-------" << endl;

}
}
}
}
