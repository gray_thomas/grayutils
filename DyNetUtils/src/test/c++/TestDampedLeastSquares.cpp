/*
 * TestDampedLeastSquares.cpp
 *
 *  Created on: Dec 29, 2013
 *      Author: Gray
 */

#include "TestDampedLeastSquares.hpp"
#include "LeastSquaresTrainer.hpp"
#include <DyNetTrainer.hpp>
#include "Network.hpp"
#include "Trainable.hpp"
#include <vector>
#include <iostream>
#include <random>
#include <Eigen/Dense>
#include <sstream>
#include "TestUtils.hpp"
#include "ScalarMultiplicationElement.hpp"
#include "ScalarAdditionElement.hpp"
#include "TrainableScalarElement.hpp"
#include "DataSet.hpp"
#include "GradientDescentTrainer.hpp"
//#include "PendulumDemoRandomDataGenerator.hpp"
#include "NameMaintainer.hpp"
#include "NetworkStateVector.hpp"
#include "PendulumNet.hpp"

using namespace std;
using namespace uta::testUtils;
using namespace uta::networkUtils;
using namespace uta::networkUtils::pendulumDemo;
using namespace uta::networkUtils::dyNetUtils;
using namespace Eigen;

#define TRAINING_NETWORK_SIZE 6
namespace uta
{
namespace networkUtils
{
namespace tests
{

class TrainingExampleNet: public uta::networkUtils::Network
{
public:
	TrainingExampleNet(NetworkStateVector& state, unsigned trainingNetworkSize) :
				Network(state, trainingNetworkSize)
	{
		SingleIndexNetworkDataProxy* ins[trainingNetworkSize];
		ScalarMultiplicationElement* squares[trainingNetworkSize];
		TrainableScalarElement* weights[trainingNetworkSize];
		NoIndexNetworkDataProxy* proxy = getGeneratorNetworkInputProxy();
		for (unsigned i = 0; i < trainingNetworkSize; i++)
		{
			ins[i] = getNetworkInput(i);
			weights[i] = new TrainableScalarElement(state, proxy, 0.5);
			squares[i] = new ScalarMultiplicationElement(state);
			squares[i]->addInput(ins[i]);
			squares[i]->addInput(ins[i]);
			squares[i]->addInput(weights[i]->getScalarResult());
			setAsOutput(squares[i]->getScalarResult());
		}
	}
};
class ExampleTrainingDataSet: public DataSet
{
public:
	ExampleTrainingDataSet(unsigned size, unsigned numberOfPoints, double min, double max, double weight) :
				DataSet(size, size)
	{
		double x;
		for (unsigned i = 0; i < numberOfPoints; i++)
		{
			x = min + i * ((max - min) / (numberOfPoints - 1));
			double inputs[size];
			double outputs[size];
			for (unsigned j = 0; j < size; j++)
			{
				inputs[j] = x;
				outputs[j] = x * x * weight;
			}
			this->addLine(inputs, outputs);
		}
	}
};
class LinearNet: public uta::networkUtils::Network
{
public:
	LinearNet(NetworkStateVector& state, Matrix<double, Dynamic, Dynamic> initWeights) :
				Network(state, initWeights.cols())
	{
		unsigned inSize = initWeights.cols();
		unsigned outSize = initWeights.rows();

		for (unsigned o = 0; o < outSize; o++)
		{
			SingleIndexNetworkDataProxy* out = &constant(0);
			for (unsigned i = 0; i < inSize; i++)
			{
				double weight = initWeights(o, i);
				out = &(*out + trainable(weight) * *getNetworkInput(i));
			}
			setAsOutput(out);
		}
	}
};
class LinearDataSet: public DataSet
{
public:
	LinearDataSet(unsigned numberOfPoints, std::default_random_engine gen,
					vector<normal_distribution<double> > inputDistributions,
					vector<normal_distribution<double> > noiseDistributions,
					Matrix<double, Dynamic, Dynamic> perfectWeights) :
				DataSet(inputDistributions.size(),
						perfectWeights.rows() - (perfectWeights.cols() - inputDistributions.size()))
	{
		unsigned inputSize = inputDistributions.size();
		unsigned memorySize = perfectWeights.cols() - inputSize;
		unsigned outputSize = perfectWeights.rows() - memorySize;
		if (noiseDistributions.size() != outputSize)
			throw runtime_error("incorrect number of noise distributions");

		Matrix<double, Dynamic, 1> inputsVector;
		inputsVector.setZero(inputSize);
		Matrix<double, Dynamic, 1> compositeInputs;
		compositeInputs.setZero(inputSize + memorySize);
		Matrix<double, Dynamic, 1> compositeOutputs;
		compositeOutputs.setZero(outputSize + memorySize);

		for (unsigned i = 0; i < numberOfPoints; i++)
		{
			for (unsigned j = 0; j < inputSize; j++)
				inputsVector(j) += inputDistributions[j](gen);
			compositeInputs.block(memorySize, 0, inputSize, 1) = inputsVector;
			compositeOutputs = perfectWeights * compositeInputs;
			compositeInputs.block(0, 0, memorySize, 1) = compositeOutputs.block(0, 0, memorySize, 1);
			double inputs[inputSize];
			double outputs[outputSize];
			for (unsigned out = 0; out < outputSize; out++)
				outputs[out] = compositeOutputs(memorySize + out) + noiseDistributions[out](gen);
			for (unsigned in = 0; in < inputSize; in++)
				inputs[in] = compositeInputs(memorySize + in);
			this->addLine(inputs, outputs);
		}
	}
};
class AlternateLinearDataSet: public DataSet
{
public:
	AlternateLinearDataSet(std::default_random_engine gen, MatrixXd& inputs,
							vector<normal_distribution<double> >& measurmentDistribution,
							vector<normal_distribution<double> >& transitionNoiseDistribution,
							VectorXd& initialConditions, MatrixXd& perfectWeights) :
				DataSet(inputs.rows(), perfectWeights.rows() - (perfectWeights.cols() - inputs.rows()))
	{
		unsigned dataSize = inputs.cols();
		unsigned inputSize = inputs.rows();
		unsigned stateSize = perfectWeights.cols() - inputSize;
		unsigned measurementSize = perfectWeights.rows() - stateSize;
		cout << "making alternate linear data set with inputSize " << inputSize << " and measurementSize "
				<< measurementSize << endl;
		if (measurmentDistribution.size() != measurementSize)
			throw runtime_error("incorrect number of noise distributions");

		VectorXd inputsVector;

		inputsVector.setZero(inputSize);
		for (int i = 0; i < initialConditions.size(); i++)
			inputsVector[i] = initialConditions[i];

		VectorXd noiseVector;
		noiseVector.setZero(stateSize + measurementSize);

		VectorXd compositeInputs;
		compositeInputs.setZero(stateSize + inputSize);
		VectorXd compositeOutputs;
		compositeOutputs.setZero(stateSize + measurementSize);
		compositeOutputs.head(stateSize) = initialConditions;

		for (unsigned dataIndex = 0; dataIndex < dataSize; dataIndex++)
		{
			for (unsigned state = 0; state < stateSize; state++)
				noiseVector.head(stateSize)[state] = transitionNoiseDistribution[state](gen);
			for (unsigned measure = 0; measure < measurementSize; measure++)
				noiseVector.tail(measurementSize)[measure] = measurmentDistribution[measure](gen);

			compositeInputs.head(stateSize) = compositeOutputs.head(stateSize);
			compositeInputs.tail(inputSize) = inputs.col(dataIndex);
			compositeOutputs = perfectWeights * compositeInputs + noiseVector;

			double inputs[inputSize];
			double outputs[measurementSize];
			for (unsigned out = 0; out < measurementSize; out++)
				outputs[out] = compositeOutputs.tail(measurementSize)[out];
			for (unsigned in = 0; in < inputSize; in++)
				inputs[in] = compositeInputs.tail(inputSize)[in];
			this->addLine(inputs, outputs);
		}
	}
};
void testTrainerUsingLinearSystem(unsigned epochs, unsigned dataPoints)
{
	NetworkStateVector state;

	Matrix<double, Dynamic, Dynamic> perfectWeights(2, 2);
	perfectWeights << 0.95, 0.25, -2.0, -0.0;
	Matrix<double, Dynamic, Dynamic> wrongWeights(2, 2);
	wrongWeights << 0.949, 0.25, -2.0, -0.0;
	vector<normal_distribution<double> > inputChangeDistributions;
	inputChangeDistributions.push_back(normal_distribution<double>(0.0, 0.01));
	vector<normal_distribution<double> > noiseDist;
	noiseDist.push_back(normal_distribution<double>(0.0, 0.01));
	Trainable* network = new LinearNet(state, wrongWeights);
	LinearDataSet trainingSet(dataPoints, default_random_engine(-128985), inputChangeDistributions, noiseDist,
			perfectWeights);
	LinearDataSet validationSet(dataPoints, default_random_engine(1337), inputChangeDistributions, noiseDist,
			perfectWeights);
	LeastSquaresTrainer trainer;
	trainer.setEpochMax(epochs);
	trainer.setupLogging(string(GRAY_UTILS_ROOT) + string("/DyNetUtils/src/test/data/linearSystem"), 1);
	vector<double> initialMemory;
	initialMemory.push_back(1e-2);
	trainer.setInitialMemory(initialMemory);

	trainer.trainNetwork(trainingSet, validationSet, *network, 0.01);

}
DyNetPoint runLinearTest(VectorXd& measurementVariances, VectorXd& transitionVariances, MatrixXd& inputs,
							VectorXd & perfectInitialConditions, MatrixXd& perfectWeights,
							VectorXd& guessInitialCondition, MatrixXd& guessWeights,
							DyNetTrainerParameters& trainingParams, vector<string>& measurementNames,
							vector<string>& inputNames, vector<string>& stateNames, vector<string>& parameterNames,
							string& yamlFileName, unsigned epochs)
{
	vector<normal_distribution<double> > measurmentDistribution;
	for (int i = 0; i < measurementVariances.rows(); i++)
		measurmentDistribution.push_back(normal_distribution<double>(0.0, sqrt((double) measurementVariances[i])));
	vector<normal_distribution<double> > transitionNoiseDistribution;
	for (int i = 0; i < transitionVariances.rows(); i++)
		transitionNoiseDistribution.push_back(
				normal_distribution<double>(0.0, sqrt((double) transitionVariances[i])));
	AlternateLinearDataSet trainingSet(default_random_engine(-1337), inputs, measurmentDistribution,
			transitionNoiseDistribution, perfectInitialConditions, perfectWeights);
	NetworkStateVector state;

	Trainable* network = new LinearNet(state, guessWeights);
	VectorXd initialStatesVector = initializeStates(*network, trainingSet, guessInitialCondition);
	DyNetTrainer dyNetTrainer(trainingParams, trainingSet, *network);
	dyNetTrainer.setNames(measurementNames, inputNames, stateNames, parameterNames);
	dyNetTrainer.setStates(initialStatesVector);
	stringstream stringStream;
	stringStream << string(GRAY_UTILS_ROOT) << yamlFileName;
	for (unsigned e = 0; e < epochs; e++)
		dyNetTrainer.trainOneEpoch();
	dyNetTrainer.log(stringStream.str(), epochs);
	return dyNetTrainer.getElitePoint();
}
struct DyNetTestParameters
{
	VectorXd measurementVariances;
	VectorXd transitionVariances;
	MatrixXd perfectWeights;
	MatrixXd testWeights;
	MatrixXd inputs;
	VectorXd perfectICs;
	VectorXd testICs;
	vector<string> measurementNames;
	vector<string> stateNames;
	vector<string> inputNames;
	vector<string> parameterNames;
	string saveName;
	DyNetTrainerParameters trainerParams;
	unsigned epochs;
	long seed;
};
DyNetPoint runLinearTest(DyNetTestParameters& params)
{
	vector<normal_distribution<double> > measurmentDistribution;
	for (int i = 0; i < params.measurementVariances.rows(); i++)
		measurmentDistribution.push_back(
				normal_distribution<double>(0.0, sqrt((double) params.measurementVariances[i])));
	vector<normal_distribution<double> > transitionNoiseDistribution;
	for (int i = 0; i < params.transitionVariances.rows(); i++)
		transitionNoiseDistribution.push_back(
				normal_distribution<double>(0.0, sqrt((double) params.transitionVariances[i])));
	AlternateLinearDataSet trainingSet(default_random_engine(params.seed), params.inputs, measurmentDistribution,
			transitionNoiseDistribution, params.perfectICs, params.perfectWeights);
	NetworkStateVector state;

	Trainable* network = new LinearNet(state, params.testWeights);
	VectorXd initialStatesVector = initializeStates(*network, trainingSet, params.testICs);
	DyNetTrainer dyNetTrainer(params.trainerParams, trainingSet, *network);
	dyNetTrainer.setNames(params.measurementNames, params.inputNames, params.stateNames, params.parameterNames);
	dyNetTrainer.setStates(initialStatesVector);
	stringstream stringStream;
	stringStream << string(GRAY_UTILS_ROOT) << params.saveName;
	for (unsigned e = 0; e < params.epochs; e++)
		dyNetTrainer.trainOneEpoch();
	dyNetTrainer.log(stringStream.str(), params.seed);
	return dyNetTrainer.getElitePoint();
}
DyNetTestParameters getStandard1DParams()
{
	DyNetTestParameters params;
	params.measurementVariances = VectorXd(1);
	params.measurementVariances << 1e-56;
	params.transitionVariances = VectorXd(1);
	params.transitionVariances << 1e-56;
	params.perfectWeights = MatrixXd(2, 2);
	params.perfectWeights << 0.75, 0.25, -2.0, -0.0;
	params.testWeights = MatrixXd(2, 2);
	params.testWeights << 0.75, 0.25, -2.0, -0.0;
	params.inputs = MatrixXd(1, 30);
	params.inputs << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1;
	params.perfectICs = VectorXd(1);
	params.perfectICs << 1.0;
	params.measurementNames.push_back("measurement one");
	params.stateNames.push_back("state one");
	params.inputNames.push_back("input one");
	params.parameterNames.push_back("A");
	params.parameterNames.push_back("B");
	params.parameterNames.push_back("C");
	params.parameterNames.push_back("D");
	params.testICs = VectorXd(1);
	params.testICs << 1.0;
	params.saveName = string("/DyNetUtils/src/test/data/linear1Ddefault");
	params.trainerParams.epoch = 0;
	params.trainerParams.trustRegionScaleParameter = 1.0e3;
	params.trainerParams.linearitySetPoint = 0.75;
	params.trainerParams.linearityMinimumError = 0.25;
	params.trainerParams.trustRegionPenalty = 0.2;
	params.trainerParams.trustRegionPositiveBump = 1.3;
	params.epochs = 1;
	params.seed = 1337;
	return params;
}
void testIdealSystemZeroError(double errorTolerance)
{
	DyNetTestParameters params = getStandard1DParams();
	params.saveName = string("/DyNetUtils/src/test/data/testPerfectZeroError");
	params.epochs = 3;
	DyNetPoint elitePoint = runLinearTest(params);
	assertEquals("The elite point of a noiseless network initialized with perfect weights has no error.",
			elitePoint.error, 0.0, errorTolerance);
}
void testIdealSystemNoWeightChange(double errorTolerance)
{
	cout << "this is the test of an ideal system, which should not change its weights" << endl;
	DyNetTestParameters params = getStandard1DParams();
	params.saveName = string("/DyNetUtils/src/test/data/testPerfectNoChange");
	params.epochs = 3;
	DyNetPoint elitePoint = runLinearTest(params);
	for (unsigned i = 0; i < 4; i++)
		assertEquals(
				"A system initialized perfectly with perfect measurements will never deviate from these optimal parameters.",
				elitePoint.weights[i], (double) (params.perfectWeights(i / 2, i % 2)), errorTolerance);
}
void testSomeNoiseSystemLittleWeightChange(double measurementVariance, double errorTolerance)
{
	cout << "testSomeNoiseSystemLittleWeightChange" << endl;
	DyNetTestParameters params = getStandard1DParams();
	params.saveName = string("/DyNetUtils/src/test/data/testNoiseLittleChange");
	params.measurementVariances[0] = measurementVariance;
	params.epochs = 20;
	VectorXd weightStdDev;
	weightStdDev.setZero(4, 1);
	unsigned numRuns = 30;
	for (unsigned i = 0; i < numRuns; i++)
	{
		params.seed++;
		DyNetPoint elitePoint = runLinearTest(params);
		for (unsigned j = 0; j < 2; j++)
			weightStdDev(j, 0) += (double) (pow(
					(double) (elitePoint.weights[j] - params.perfectWeights(j / 2, j % 2)), 2.0));
	}
	for (unsigned i = 0; i < 4; i++)
		assertEquals("Noise will not, on average, move a system far from optimal parameters.",
				sqrt((double) weightStdDev(i) / numRuns), 0.0, errorTolerance);
}
void testSomeNoiseSystemLittleWeightChange(double measurementVariance, unsigned dataSize, double AMatrixTolerance,
											double BMatrixTolerance, double CMatrixTolerance,
											double DMatrixTolerance)
{
	cout << "testSomeNoiseSystemLittleWeightChange" << endl;
	DyNetTestParameters params = getStandard1DParams();
	stringstream ss;
	ss << "/DyNetUtils/src/test/data/testDeviation_Noise_" << measurementVariance << "_DataSize_" << dataSize
			<< "_seed_";
	params.saveName = ss.str();
	params.measurementVariances[0] = measurementVariance;
	params.epochs = 20;
	unsigned numRuns = 5;
	params.inputs.setZero(1, dataSize);
	for (unsigned i = 0; i < dataSize; i++)
		params.inputs(0, i) = (i % 10 > 5 ? 0.0 : 1.0);
	params.inputs *= 1e2;
	VectorXd weightStdDev;
	weightStdDev.setZero(4, 1);
	for (unsigned i = 0; i < numRuns; i++)
	{
		params.seed++;
		DyNetPoint elitePoint = runLinearTest(params);
		for (unsigned j = 0; j < 2; j++)
			weightStdDev(j, 0) += (double) (pow(
					(double) (elitePoint.weights[j] - params.perfectWeights(j / 2, j % 2)), 2.0));
	}
	assertEquals("A matrix will not, on average, move a system far from optimal parameters.",
			sqrt((double) weightStdDev(0) / numRuns), 0.0, AMatrixTolerance);
	assertEquals("B matrix will not, on average, move a system far from optimal parameters.",
			sqrt((double) weightStdDev(1) / numRuns), 0.0, BMatrixTolerance);
	assertEquals("C matrix will not, on average, move a system far from optimal parameters.",
			sqrt((double) weightStdDev(2) / numRuns), 0.0, CMatrixTolerance);
	assertEquals("D matrix will not, on average, move a system far from optimal parameters.",
			sqrt((double) weightStdDev(3) / numRuns), 0.0, DMatrixTolerance);

}

void testConvergence(double measurementVariance, unsigned dataSize, double AMatrixTolerance, double BMatrixTolerance,
						double CMatrixTolerance, double DMatrixTolerance, double GuessA, double GuessB,
						double GuessC, double GuessD)
{
	cout << "testSomeNoiseSystemLittleWeightChange" << endl;
	DyNetTestParameters params = getStandard1DParams();
	params.testWeights << GuessA, GuessB, GuessC, GuessD;
	stringstream ss;
	ss << "/DyNetUtils/src/test/data/testConvergence" << //
			"_Noise_" << measurementVariance << //
			"_A_" << GuessA << //
			"_B_" << GuessB << //
			"_C_" << GuessC << //
			"_D_" << GuessD << //
			"_DataSize_" << dataSize << //
			"_seed_";
	params.saveName = ss.str();
	params.measurementVariances[0] = measurementVariance;
	params.epochs = 20;
	unsigned numRuns = 5;
	params.inputs.setZero(1, dataSize);
	for (unsigned i = 0; i < dataSize; i++)
		params.inputs(0, i) = (i % 10 > 5 ? 0.0 : 1.0);
	params.inputs *= 1e2;
	VectorXd weightStdDev;
	weightStdDev.setZero(4, 1);
	for (unsigned i = 0; i < numRuns; i++)
	{
		params.seed++;
		DyNetPoint elitePoint = runLinearTest(params);
		for (unsigned j = 0; j < 4; j++)
			if (j == 1 || j == 2)
				weightStdDev(j, 0) += (double) (pow(
						(double) (elitePoint.weights[1] * elitePoint.weights[2]
								- params.perfectWeights(0, 1) * params.perfectWeights(1, 0)), 2.0));
			else
				weightStdDev(j, 0) += (double) (pow(
						(double) (elitePoint.weights[j] - params.perfectWeights(j / 2, j % 2)), 2.0));
	}

	assertEquals("A matrix will converge near to simulated parameter.", sqrt((double) weightStdDev(0) / numRuns),
			0.0, AMatrixTolerance);
	assertEquals("B*C scalar product will converge near to simulated parameter.", sqrt((double) weightStdDev(1) / numRuns),
			0.0, BMatrixTolerance);
//	assertEquals("C matrix will converge near to simulated parameter.", sqrt((double) weightStdDev(2) / numRuns),
//			0.0, CMatrixTolerance);
	assertEquals("D matrix will converge near to simulated parameter.", sqrt((double) weightStdDev(3) / numRuns),
			0.0, DMatrixTolerance);

}

void testFloatingMemoryTrainer2DLinear(unsigned epochs, unsigned dataPoints)
{
	Matrix<double, Dynamic, Dynamic> perfectWeights(3, 3);
	perfectWeights << 0.95, -0.3, 0.1, 0.1, 0.85, 0.25, 1.0, -2.0, -0.0;
	Matrix<double, Dynamic, Dynamic> wrongWeights(3, 3);
	wrongWeights << 0.92, -0.3, 0.1, 0.1, 0.85, 0.25, 1.0, -2.0, -0.0;
	vector<normal_distribution<double> > inputChangeDistributions;
	inputChangeDistributions.push_back(normal_distribution<double>(1.0, 1e-2)); //step input
	vector<normal_distribution<double> > noiseDist;
	noiseDist.push_back(normal_distribution<double>(0.0, 1e-2));
	LinearDataSet trainingSet(dataPoints, default_random_engine(-128985), inputChangeDistributions, noiseDist,
			perfectWeights);
	LinearDataSet validationSet(dataPoints, default_random_engine(1337), inputChangeDistributions, noiseDist,
			perfectWeights);
	{
		NetworkStateVector cheatState;
		DyNetTrainerParameters cheatParams;
		cheatParams.epoch = 0;
		VectorXd cheatIC(2);
		cheatIC << 0.0, 0.0;
		Trainable* cheatNetwork = new LinearNet(cheatState, perfectWeights);
		VectorXd cheatStates = initializeStates(*cheatNetwork, trainingSet, cheatIC);
		DyNetTrainer cheatTrainer(cheatParams, trainingSet, *cheatNetwork);
		cheatTrainer.setStates(cheatStates);
		cout << "hey" << endl;
		stringstream stringStream;
		stringStream << string(GRAY_UTILS_ROOT) << string("/DyNetUtils/src/test/data/linear2Dcheat");
		cheatTrainer.log(stringStream.str(), 0);
		cheatTrainer.trainOneEpoch();
		cheatTrainer.log(stringStream.str(), 1);
		cout << cheatTrainer.getEpochError() << endl;
	}

	{
		NetworkStateVector state;
		DyNetTrainerParameters params;
		params.epoch = 0;
		VectorXd initialConditions(2);
		initialConditions << 0.0, 0.0;
		Trainable* network = new LinearNet(state, wrongWeights);
		VectorXd states = initializeStates(*network, trainingSet, initialConditions);
		cout << "states=\n" << states << endl;
		DyNetTrainer trainer(params, trainingSet, *network);
		trainer.setStates(states);
		stringstream ss;
		ss << string(GRAY_UTILS_ROOT) << string("/DyNetUtils/src/test/data/linear2Dreal");
		for (unsigned e = 0; e < epochs; e++)
		{
			trainer.log(ss.str(), e);
			trainer.trainOneEpoch();
			cout << trainer.getEpochError() << endl;
		}
//		trainer.applyEliteWeights();
		trainer.log(ss.str(), epochs);
		cout << "final first weight is " << network->getWeight(0) << endl;
	}

}
void testLeastSquaresTraining(string message, unsigned trainingNetworkSize, unsigned epochs, double rate,
								double expectedWeightAtTerminus, double perfectWeight, unsigned dataPoints)
{
	NetworkStateVector state;
	Trainable* network = new TrainingExampleNet(state, trainingNetworkSize);
	ExampleTrainingDataSet trainingSet(trainingNetworkSize, dataPoints, -1, 1, perfectWeight);
	ExampleTrainingDataSet validationSet(trainingNetworkSize, dataPoints, -1, 1, perfectWeight);
	LeastSquaresTrainer trainer;
	trainer.setEpochMax(epochs);

	trainer.trainNetwork(trainingSet, validationSet, *network, rate);

	assertEquals(message, trainer.getUpcomingWeight(0), expectedWeightAtTerminus, 1e-6);
}

void leastSquaresTrainerTest(void)
{
	cout << "=======Damped Least Squares Trainer Test:=============== " << endl;
	/*{
	 unsigned epochsToTrain = 10; // usually 1000
	 unsigned trainingNetworkSize = 3;
	 double trainingRate = 0.05;
	 double expectedWeightAtTerminus = 256;
	 double perfectWeight = 256;
	 unsigned dataPoints = 6; // usually 55
	 testLeastSquaresTraining("Test Convergence over 1000 epochs", trainingNetworkSize, epochsToTrain, trainingRate,
	 expectedWeightAtTerminus, perfectWeight, dataPoints);
	 }*/
	{
		cout << "-------Linear System Estimation Test:------------------- " << endl;
		unsigned epochs = 1;
		unsigned dataPoints = 100;
		testTrainerUsingLinearSystem(epochs, dataPoints);
	}
	cout << "=======Damped Least Squares Trainer Test Complete!====== " << endl;

}
void testDyNetTrainerTrustRegionAdjustment()
{
	unsigned dataPoints = 200;
	unsigned epochs = 10;
	cout << "-----Trust Region Adjustment Test---------" << endl;
	Matrix<double, Dynamic, Dynamic> perfectWeights(2, 2);
	perfectWeights << 0.95, 0.25, -2.0, -0.0;
	Matrix<double, Dynamic, Dynamic> wrongWeights(2, 2);
	wrongWeights << 0.1, 0.25, -2.0, -0.0;
	vector<normal_distribution<double> > inputChangeDistributions;

	inputChangeDistributions.push_back(normal_distribution<double>(1.0, 1e-2)); //step input
	vector<normal_distribution<double> > noiseDist;
	double measurementStdDev = 1e-2;
	noiseDist.push_back(normal_distribution<double>(0.0, measurementStdDev));
	LinearDataSet trainingSet(dataPoints, default_random_engine(-128985), inputChangeDistributions, noiseDist,
			perfectWeights);
	LinearDataSet validationSet(dataPoints, default_random_engine(1337), inputChangeDistributions, noiseDist,
			perfectWeights);
	{
		NetworkStateVector cheatState;
		DyNetTrainerParameters cheatParams;
		cheatParams.epoch = 0;
		VectorXd cheatIC(1);
		cheatIC << 0.0;
		Trainable* cheatNetwork = new LinearNet(cheatState, perfectWeights);
		VectorXd cheatStates = initializeStates(*cheatNetwork, trainingSet, cheatIC);
		DyNetTrainer cheatTrainer(cheatParams, trainingSet, *cheatNetwork);
		cheatTrainer.setStates(cheatStates);
		stringstream stringStream;
		stringStream << string(GRAY_UTILS_ROOT) << string("/DyNetUtils/src/test/data/linearRegionAdjustementCheat");
		cheatTrainer.log(stringStream.str(), 0);
		cheatTrainer.trainOneEpoch();
		cheatTrainer.log(stringStream.str(), 1);
		cout << cheatTrainer.getEpochError() << endl;
		// TODO: the error you get with the perfect weights should be statistically bounded
		// by the real variance over the expected covariance. Both unsensed update noise
		// and unsensed measurement noise should be added
	}

	{
		// TODO: assert that error prediction ratio is within tolerance at each time step.
		// TODO: include viewing of the predition ratio in the interface for DyNetTrainer.
		// TODO: assert that error decreases.
		// TODO: assert that the error converges to the error from the previous test with the cheater.
		// TODO: assert that the initial trust region, which is exceptionally large, is reduced immediately
		NetworkStateVector state;
		DyNetTrainerParameters params;
		params.epoch = 0;
		VectorXd initialConditions(1);
		initialConditions << 0.0;
		Trainable* network = new LinearNet(state, wrongWeights);
		VectorXd states = initializeStates(*network, trainingSet, initialConditions);
		cout << "states=\n" << states << endl;
		DyNetTrainer trainer(params, trainingSet, *network);
		trainer.setStates(states);
		stringstream ss;
		ss << string(GRAY_UTILS_ROOT) << string("/DyNetUtils/src/test/data/linearRegionAdjustementReal");
		for (unsigned e = 0; e < epochs; e++)
		{
			trainer.log(ss.str(), e);
			trainer.trainOneEpoch();
			cout << trainer.getEpochError() << endl;
		}
//		trainer.applyEliteWeights();
		trainer.log(ss.str(), epochs);
		cout << "final first weight is " << network->getWeight(0) << endl;

	}
	cout << "-----Trust Region Adjustment Test Passed--" << endl;
}
void floatingMemoryTrainerTest(void)
{
	cout << "hey, hey" << endl;
	cout << "=======Floating Memory Trainer Test:=============== " << endl;
	testIdealSystemZeroError(1e-17);
	testIdealSystemNoWeightChange(1e-7);
//	testSomeNoiseSystemLittleWeightChange(1e-28, 1e-14);
//	testSomeNoiseSystemLittleWeightChange(1e-24, 1e-12);
//	testSomeNoiseSystemLittleWeightChange(1e-20, 1e-10);
//	testSomeNoiseSystemLittleWeightChange(1e-15, 1e-8);
//	testSomeNoiseSystemLittleWeightChange(1e-11, 1e-6);
//	testSomeNoiseSystemLittleWeightChange(1e-11, 30, 4e-9, 4e-9, 1e-6, 1e-7);
//	testSomeNoiseSystemLittleWeightChange(1e-11, 60, 2e-9, 2e-9, 1e-6, 1e-7);
//	testSomeNoiseSystemLittleWeightChange(1e-11, 120, 1.2e-9, 1e-9, 1e-6, 1e-7);
//	testSomeNoiseSystemLittleWeightChange(1e-9, 30, 4e-8, 4e-8, 1e-4, 1e-4);
//	testSomeNoiseSystemLittleWeightChange(1e-7, 30, 4e-7, 4e-7, 1e-4, 1e-4);
//	testSomeNoiseSystemLittleWeightChange(1e-5, 30, 4e-6, 5e-5, 1e-4, 1e-4);
//	testSomeNoiseSystemLittleWeightChange(1e-5, 60, 2e-6, 6e-5, 1e-4, 1e-4);
//	testSomeNoiseSystemLittleWeightChange(1e-5, 120, 2e-6, 6e-5, 1e-4, 1e-4);
//	testSomeNoiseSystemLittleWeightChange(1e-3, 120, 2e-5, 4e-2, 1e-4, 1e-4);
	testConvergence(1e-3, 120, 2e-5, 4e-2, 0, 1e-4, 0.75, 0.25, -2.0, -0.0);		//perfect weights
	testConvergence(1e-3, 120, 2e-5, 4e-2, 0, 1e-4, 0.75 + 1e-1, 0.25, -2.0, -0.0);
	testConvergence(1e-2, 120, 4e-5, 1e-4, 0, 3e-4, 0.75 + 1e-1, 0.25, -2.0, -0.1);

	testConvergence(30, 120, 4e-3, 1e-2, 0, 3e-2, 0.75 + 1e-1, 0.25, -2.0, -0.1);// high noise
//	testLowNoise1DLinearNetwork();
//	cout << "End 1D Linear DyNetTrainer test" << endl;
//	testFloatingMemoryTrainer2DLinear(epochs, dataPoints);
//	testDyNetTrainerTrustRegionAdjustment();
	cout << "=======Floating Memory Trainer Test Complete!====== " << endl;

}
}
}
}

