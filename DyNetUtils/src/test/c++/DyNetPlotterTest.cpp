/*
 * DyNetPlotterTest.cpp
 *
 *    Created on: Feb 13, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include <DyNetPlotterTest.hpp>
#include <DyNetPlotter.hpp>
#include <iostream>
#include <DyNetSizeManager.hpp>
#include <DataSet.hpp>
#include <DyNetPoints.hpp>
#include <DyNetStates.hpp>
#include <string>
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <TestUtils.hpp>

using namespace std;
using namespace uta::networkUtils::dyNetUtils;
using namespace uta::networkUtils;
using namespace uta::testUtils;

namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
namespace tests
{
void testDyNetPlotter(void)
{
	cout << "=======testDyNetPlotter=======" << endl;
	string desiredResult = "DyNetTrainer DataDump:\n"
			"  inputs: [torque1, input2]\n"
			"  measurements: [output1, output2]\n"
			"  states: [position]\n"
			"  parameters: [alpha, $\\beta$]\n"
			"  epochNumber: 1\n"
			"  dataSize: 2\n"
			"  trainingData:\n"
			"    inputs:\n"
			"      - [0.12, 0.5600000000000001]\n"
			"      - [0.6, 0.37]\n"
			"    measurements:\n"
			"      - [0.13, 0.9]\n"
			"      - [0.75, 0.86]\n"
			"  epochData:\n"
			"    - error: 0.555\n"
			"      states used:\n"
			"        - [0.2]\n"
			"        - [2.3]\n"
			"      parameters used: [30, 45]\n"
			"      state prediction:\n"
			"        - [2]\n"
			"        - [4.5]\n"
			"      measurement prediction:\n"
			"        - [2.3, 4.5]\n"
			"        - [3.4, 213]\n"
			"      update:\n"
			"        parameter delta: [22, 77]\n"
			"        states delta:\n"
			"          - [1.2]\n"
			"          - [1.3]\n"
			"        change in error: 4.5\n"
			"        trust parameter: 0.26";
	DyNetSizeManager sizes;
	sizes.dataSize = 2;
	sizes.inputSize = 2;
	sizes.outputSize = 2;
	sizes.unjudgedSize = 0;
	sizes.memorySize = 1;
	sizes.weightSize = 2;
	vector<string> measurementNames;
	measurementNames.push_back("output1");
	measurementNames.push_back("output2");
	const char *inputNamesInit[] =
	{ "torque1", "input2" };
	vector<string> inputNames(inputNamesInit, end(inputNamesInit));
	const char *stateNamesInit[] =
	{ "position" };
	vector<string> stateNames(stateNamesInit, end(stateNamesInit));
	const char *parameterNamesInit[] =
	{ "alpha", "$\\beta$" };
	vector<string> parameterNames(parameterNamesInit, end(parameterNamesInit));
	DataSet sampleData(sizes.inputSize, sizes.outputSize);
	DyNetPoint point1(sizes);
	point1.error = 0.555;
	point1.dyNetStates.getState(0) << 0.2;
	point1.dyNetStates.getState(1) << 2.3;
	point1.weights[0] = 30;
	point1.weights[1] = 45;
	DyNetVectorDelta vector1(sizes);
	vector1.delta << 1.2, 1.3, 22, 77;
	vector1.predictedChangeInError = 4.5;

	DyNetOutputs output1(sizes);
	DyNetOutputs output2(sizes);
	output1.mem << 2.0;
	output1.outputs << 2.3, 4.5;
	output2.mem << 4.5;
	output2.outputs << 3.4, 213;

	double ins[2];
	double outs[2];
	ins[0] = 0.12;
	outs[0] = 0.13;
	ins[1] = 0.56;
	outs[1] = 0.9;
	sampleData.addLine(ins, outs);
	ins[0] = 0.6;
	outs[0] = 0.75;
	ins[1] = 0.37;
	outs[1] = 0.86;
	sampleData.addLine(ins, outs);

	double trustRegion = 0.26;

	DyNetPlotterTest test(sizes);
	string fileName("testLocation");
	DyNetPlotter testPlotter(sizes, fileName);
	cout << "before rendering Yaml" << endl;
	testPlotter.setMeasurementNames(measurementNames);
	testPlotter.setInputNames(inputNames);
	testPlotter.setStateNames(stateNames);
	testPlotter.setParameterNames(parameterNames);

	testPlotter.setData(sampleData);
	testPlotter.logOutput(0, output1);
	testPlotter.logOutput(1, output2);
	testPlotter.logEpoch(point1, vector1, trustRegion);

	ofstream testFile;
	testFile.open("GRAY_UTILS_ROOT/DyNetUtils/src/test/data/DyNetTestYaml.yaml");
	if (testFile.is_open())
		try
		{
			testPlotter.renderYAML(testFile);
		} catch (exception& e)
		{
			cerr << e.what();
			cout << "error occurred" << e.what();
		}
	else
		testPlotter.renderYAML(cout);
	stringstream out;
	testPlotter.renderYAML(out);
	//GT: this test is not good, the decimal precision output does not need to be
	// consistant to this degree
//	cout << "::::\n"<< out.str()<<":::::\n"<<endl;
//	if (strcmp(out.str().c_str(), desiredResult.c_str()))
//		throw AssertionFailedException("YAML string not as expected");
	testFile.close();
	cout << endl << "after rendering Yaml" << endl;
	cout << "-------testDyNetPlotter-------" << endl;
}

DyNetPlotterTest::DyNetPlotterTest(DyNetSizeManager& sizes) :
			sizes(sizes)
{

}
}
}
}
}
