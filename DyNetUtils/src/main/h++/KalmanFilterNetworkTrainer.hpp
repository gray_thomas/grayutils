/*
 * KalmanFilterNetworkTrainer.hpp
 *
 *    Created on: Dec 3, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef KALMANFILTERNETWORKTRAINER_HPP_
#define KALMANFILTERNETWORKTRAINER_HPP_

#include <iostream>
#include <string>
#include <vector>

#include "GradientDescentTrainer.hpp"
#include "DataSet.hpp"
#include "Network.hpp"
#include "PerformanceTimer.hpp"
#include <Eigen/Core>
#include <Eigen/Dense>
using namespace uta::timingUtils;
using namespace std;
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
class KalmanFilterNetworkTrainer: public GradientDescentTrainer
{
private:
	typedef Matrix<double, Dynamic, Dynamic, 0, 9, 9> DynamicStateSizeMatrix;
	typedef Matrix<double, Dynamic, Dynamic, 0, 9, 9> DynamicMeasurementSizeMatrix;
	DynamicStateSizeMatrix stateCovariance;
	DynamicStateSizeMatrix stateUpdateNoiseCovariance;
	DynamicMeasurementSizeMatrix measurementNoiseCovariance;
public:
	KalmanFilterNetworkTrainer(string logFileName) :
				GradientDescentTrainer(logFileName),
				stateCovariance(0, 0),
				stateUpdateNoiseCovariance(0, 0),
				measurementNoiseCovariance(0, 0)
	{
	}
	virtual void trainNetwork(DataProvider &trainingDataSet, DataProvider &validationDataSet, Trainable& network,
								double learningRate)
	{
		stateCovariance.resize(network.weightSize(), network.weightSize());
		stateUpdateNoiseCovariance.resize(network.weightSize(), network.weightSize());
		measurementNoiseCovariance.resize(trainingDataSet.viewOutputSize(), trainingDataSet.viewOutputSize());
		GradientDescentTrainer::trainNetwork(trainingDataSet, validationDataSet, network, learningRate);
	}
protected:
	virtual void trainOneEpoch(DataProvider &trainingDataSet, Trainable& network, double learningRate)
	{
		GradientDescentTrainer::trainOneEpoch(trainingDataSet, network, learningRate);
	}
	virtual void setupForEpoch(DataProvider &trainingDataSet, Trainable& network)
	{
		GradientDescentTrainer::setupForEpoch(trainingDataSet, network);
	}
	virtual void updateWeights(Trainable& network, double learningRate, double dataSize)
	{
		Matrix3d a;
		Matrix3d b;
		typedef Matrix<double, Dynamic, Dynamic, 0, 3, 9> MyMatrixXd;
		MyMatrixXd dynamicTestMatrix1(3, 3);
		a << 1, 2, 3, 4, 5, 6, 7, 8, 9;
		dynamicTestMatrix1 = a;
		for (int i = 0; i < 1000; i++)
		{
			b = a;
			dynamicTestMatrix1.resize(3, 3);
			b = dynamicTestMatrix1;
			dynamicTestMatrix1.resize(1, 9);
			a.transposeInPlace();
		}
		cout << "weights = " << weights << endl;
		cout << "gradient = " << weightPartialsAccumulated << endl;
		for (unsigned i = 0; i < weights.size(); i++)
			weightPartialsAccumulated[i] *= -(learningRate / dataSize);
		cout << "delta = " << weightPartialsAccumulated << endl;
		for (unsigned i = 0; i < weights.size(); i++)
			weights[i] += weightPartialsAccumulated[i];

		network.setWeights(weights);
		if (trainError < eliteError)
		{
			eliteError = trainError;
			for (unsigned i = 0; i < weights.size(); i++)
				eliteWeights[i] = weights[i];
		}
	}
	virtual double validateWeights(DataSet &validationDataSet, Trainable& network)
	{
		return GradientDescentTrainer::validateWeights(validationDataSet, network);
	}
	virtual bool doesTimerRemain(void)
	{
		return GradientDescentTrainer::doesTimerRemain();
	}
	virtual bool doEpochsRemain(void)
	{
		return GradientDescentTrainer::doEpochsRemain();
	}
	virtual double calculateOneDatumError(Trainable& network)
	{
		double error = GradientDescentTrainer::calculateOneDatumError(network);
		return error;
	}

};
}
}
#endif /* KALMANFILTERNETWORKTRAINER_HPP_ */
