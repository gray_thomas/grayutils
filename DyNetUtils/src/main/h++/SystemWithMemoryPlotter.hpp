/*
 * SystemWithMemoryPlotter.hpp
 *
 *    Created on: Dec 14, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SYSTEMWITHMEMORYPLOTTER_HPP_
#define SYSTEMWITHMEMORYPLOTTER_HPP_
#include <vector>
#include <string>
#include "DataSet.hpp"
#include "DyNetSizeManager.hpp"
using namespace std;
using namespace uta::networkUtils::dyNetUtils;
namespace uta
{
namespace networkUtils
{

class SystemWithMemoryPlotter
{
	bool dataRecording;
	bool dataRecordingThisEpoch;
	DataSet* outputData;
	unsigned plotEveryXEpochsCounter;
	unsigned plotEveryXEpochs;
	string dataSetFileNameBase;
	DyNetSizeManager sizes;
	double* ins;
	double* outs;
public:
	SystemWithMemoryPlotter(DyNetSizeManager& sizes);
	void setupRecording(string dataSetFileName, unsigned plotEveryXEpochs);
	void prepareEpoch();
	void recordData(void);
	void wrapUpAndSaveFile(unsigned epochNumber);
	void setInput(unsigned index, double value);
	void setOutputMemory(unsigned index, double value);
	void setOutputs(unsigned index, double value);
	void setDesiredOutputs(unsigned index, double value);
	void setUnJudgedOutputs(unsigned index, double value);
	void setPointError( double value);
	void setAccumulatedError( double value);
};

}
}

#endif /* SYSTEMWITHMEMORYPLOTTER_HPP_ */
