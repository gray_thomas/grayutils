/*
 * DyNetOutputs.hpp
 *
 *    Created on: Feb 10, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef DYNETOUTPUTS_HPP_
#define DYNETOUTPUTS_HPP_
#include "Eigen/Dense"
#include "DyNetSizeManager.hpp"
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
class DyNetOutputs: public VectorXd
{
public:
	Block<VectorXd> mem;
	Block<VectorXd> outputs;
	Block<VectorXd> unjudgedOutputs;
	DyNetOutputs(DyNetSizeManager& sizes);

};
}
}
}

#endif /* DYNETOUTPUTS_HPP_ */
