/*
 * DyNetErrorVector.hpp
 *
 *    Created on: Feb 10, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef DYNETERRORVECTOR_HPP_
#define DYNETERRORVECTOR_HPP_

#include "Eigen/Dense"
#include "DyNetSizeManager.hpp"
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
class DyNetErrorVector: public VectorXd
{
public:
	unsigned dataSize;
	unsigned outputSize;
	unsigned memorySize;
	Eigen::Block<VectorXd> outputError(unsigned dataIndex);
	Eigen::Block<VectorXd> memoryError(unsigned dataIndex);
	DyNetErrorVector(DyNetSizeManager& sizes);
};
}
}
}
#endif /* DYNETERRORVECTOR_HPP_ */
