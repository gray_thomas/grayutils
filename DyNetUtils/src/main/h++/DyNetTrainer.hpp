/*
 * DyNetTrainer.hpp
 *
 *  Created on: Jan 9, 2014
 *      Author: Gray
 */

#ifndef FLOATINGMEMORYTRAINER_HPP_
#define FLOATINGMEMORYTRAINER_HPP_
#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
#include "StateTrainingMatrixBuilder.hpp"
#include "DyNetSizeManager.hpp"
#include "DataSet.hpp"
#include "Network.hpp"
#include "PerformanceTimer.hpp"
#include "DyNetPoints.hpp"
#include "DyNetPlotter.hpp"
#include "DyNetErrorVector.hpp"
#include <Eigen/Dense>
using namespace uta::timingUtils;
using namespace uta::networkUtils::dyNetUtils;
using namespace std;
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
struct DyNetTrainerParameters
{
	unsigned epoch = 0;
	double linearitySetPoint = 1e-1; //1e-1
	double trustRegionScaleParameter = 1e-2; //1e-2
	double linearityMinimumError = 1e-3; //1e-3
	double trustRegionPositiveBump = 1.5; //1.5
	double trustRegionPenalty = 1e-1;

};
void setCovarianceMatrixFromStandardDeviationVector(MatrixXd& mat, VectorXd& vec);
void setMatrixIfSizeMatches(MatrixXd& mat, const MatrixXd& newMat);
VectorXd initializeStates(Trainable& network, DataProvider& trainingData, VectorXd& initialConditions);
VectorXd initializeStatesZero(DataProvider& trainingData, VectorXd& initialConditions);


class DyNetTrainer
{
protected:
	DyNetTrainerParameters params;
	DataProvider &dataSet;
	Trainable& network;

	double epochError;
	DyNetSizeManager sizes;
	DyNetJacobian jacobian;
	DyNetErrorVector regressionErrorVector;

	double error;
	DyNetJacobianCalculator dyNetJacobianCalculator;
	SparseHessianSolver sparseHessianSolver;

	DyNetPoint basePoint;
	DyNetPoint elitePoint;
	DyNetPoint testPoint;
	DyNetVectorDelta testVectorDelta;
	DyNetVectorDelta tempVectorDelta;
	DyNetPlotter plotter;
	bool firstPointGenerated;
	MatrixXd updateWeighting;
	MatrixXd measurementWeighting;
	MatrixXd stateDamping;
	MatrixXd parameterDamping;
	VectorXd getInputs(unsigned dataIndex);
	VectorXd getOutputs(unsigned dataIndex);
	void setRecordFromDelta(DyNetPoint& recordToSet, DyNetPoint& baseRecord, DyNetVectorDelta& suggestion);
	void recordPlottableData();
	void calculatePointErrorAndDelta(DyNetPoint& record, DyNetVectorDelta& delta);
	void calculateValidationPointErrorAndDelta(DyNetPoint& record, DyNetVectorDelta& delta);
	void generateFirstPoint();
public:
	DyNetTrainer(DyNetTrainerParameters params, DataProvider &trainingDataSet, Trainable& network);
	virtual ~DyNetTrainer()
	{
	}
	void setNames(vector<string>& measurementNames, vector<string>& inputNames, vector<string>& stateNames,
					vector<string>& parameterNames);
	virtual void trainOneEpoch();
	void trainWithTimeLimit(double seconds);
	void trainWithEpochLimit(unsigned epochs);
	void trainWithTwoLimits(double seconds, unsigned epochs);
	void log(string filebase, unsigned epoch);
	void log1Epoch(string filebase, unsigned number, unsigned epoch);
	double getEpochError(void)
	{
		return epochError;
	}
	void setStates(VectorXd states)
	{
		this->basePoint.dyNetStates << states;
	}
	DyNetPoint getElitePoint(void)
	{
		return elitePoint;
	}
	void setMeasurementNoise(VectorXd& standardDeviations)
	{
		setCovarianceMatrixFromStandardDeviationVector(measurementWeighting,standardDeviations);
	}
	void setStateUpdateNoise(VectorXd& standardDeviations)
	{
		setCovarianceMatrixFromStandardDeviationVector(updateWeighting,standardDeviations);
	}
	void setStateUncertainty(VectorXd& standardDeviations)
	{
		setCovarianceMatrixFromStandardDeviationVector(stateDamping,standardDeviations);
	}
	void setParameterUncertainty(VectorXd& standardDeviations)
	{
		setCovarianceMatrixFromStandardDeviationVector(parameterDamping,standardDeviations);
	}
	const MatrixXd& viewMeasurementWeight()const {return measurementWeighting;}
	const MatrixXd& viewUpdateWeight()const {return updateWeighting;}
	const MatrixXd& viewStateDamping()const {return stateDamping;}
	const MatrixXd& viewParameterDamping()const {return parameterDamping;}
	void setMeasurementNoise(MatrixXd& covariance)
	{
		setMatrixIfSizeMatches(measurementWeighting,covariance.inverse());
	}
	void setStateUpdateNoise(MatrixXd& covariance)
	{
		setMatrixIfSizeMatches(updateWeighting,covariance.inverse());
	}
	void setStateUncertainty(MatrixXd& covariance)
	{
		setMatrixIfSizeMatches(stateDamping,covariance.inverse());
	}
	void setParameterUncertainty(MatrixXd& covariance)
	{
		setMatrixIfSizeMatches(parameterDamping,covariance.inverse());
	}
	const DyNetSizeManager& viewSizes()
	{
		return sizes;
	}
};

class DyNetValidator
{
protected:
	DyNetTrainerParameters params;
	DataProvider &dataSet;
	Trainable& network;

	double epochError;
	DyNetSizeManager sizes;
	DyNetJacobian jacobian;
	DyNetErrorVector regressionErrorVector;

	double error;
	DyNetJacobianCalculator dyNetJacobianCalculator;
	SparseHessianSolver sparseHessianSolver;

	DyNetPoint basePoint;
	DyNetPoint elitePoint;
	DyNetPoint testPoint;
	DyNetVectorDelta testVectorDelta;
	DyNetVectorDelta tempVectorDelta;
	bool firstPointGenerated;
	MatrixXd updateWeighting;
	MatrixXd measurementWeighting;
	MatrixXd stateDamping;
//	MatrixXd parameterDamping;
	VectorXd getInputs(unsigned dataIndex);
	VectorXd getOutputs(unsigned dataIndex);
	void setRecordFromDelta(DyNetPoint& recordToSet, DyNetPoint& baseRecord, DyNetVectorDelta& suggestion);
	void recordPlottableData();
	void calculatePointErrorAndDelta(DyNetPoint& record, DyNetVectorDelta& delta);
	void calculateValidationPointErrorAndDelta(DyNetPoint& record, DyNetVectorDelta& delta);
	void generateFirstPoint();
public:
	DyNetValidator(DyNetTrainerParameters params, DataProvider &trainingDataSet, Trainable& network);
	virtual ~DyNetValidator()
	{
	}
	void setNames(vector<string>& measurementNames, vector<string>& inputNames, vector<string>& stateNames,
					vector<string>& parameterNames);
	virtual void trainOneEpoch();
	void trainWithTimeLimit(double seconds);
	void trainWithEpochLimit(unsigned epochs);
	void trainWithTwoLimits(double seconds, unsigned epochs);
	void log(string filebase, unsigned epoch);
	double getEpochError(void)
	{
		return epochError;
	}
	void setStates(VectorXd states)
	{
		this->basePoint.dyNetStates << states;
	}
	DyNetPoint getElitePoint(void)
	{
		return elitePoint;
	}
	void setMeasurementNoise(VectorXd& standardDeviations)
	{
		setCovarianceMatrixFromStandardDeviationVector(measurementWeighting,standardDeviations);
	}
	void setStateUpdateNoise(VectorXd& standardDeviations)
	{
		setCovarianceMatrixFromStandardDeviationVector(updateWeighting,standardDeviations);
	}
	void setStateUncertainty(VectorXd& standardDeviations)
	{
		setCovarianceMatrixFromStandardDeviationVector(stateDamping,standardDeviations);
	}
	void setMeasurementNoise(MatrixXd& covariance)
	{
		setMatrixIfSizeMatches(measurementWeighting,covariance.inverse());
	}
	void setStateUpdateNoise(MatrixXd& covariance)
	{
		setMatrixIfSizeMatches(updateWeighting,covariance.inverse());
	}
	void setStateUncertainty(MatrixXd& covariance)
	{
		setMatrixIfSizeMatches(stateDamping,covariance.inverse());
	}

	const DyNetSizeManager& viewSizes()
	{
		return sizes;
	}
};
}
}
}

#endif /* FLOATINGMEMORYTRAINER_HPP_ */
