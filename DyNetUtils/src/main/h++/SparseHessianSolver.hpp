/*
 * SparseHessianSolver.hpp
 *
 *    Created on: Jan 21, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef STATESOLVER_HPP_
#define STATESOLVER_HPP_
#include <Eigen/Dense>
#include <stdexcept>
#include <cstdint>
#include <vector>

#define DEBUG_SPARSE_HESSIAN_SOLVER false
using namespace Eigen;
using namespace std;

namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
struct BorderedLowerBlockBiDiagonal
{
public:
	vector<MatrixXd> diag, subDiag, subBorder;
	MatrixXd end;
	BorderedLowerBlockBiDiagonal(unsigned diagonalLength, unsigned diagonalBlockRowSize,
									unsigned boarderBlockRowSize, unsigned diagonalBlockColumnSize,
									unsigned boarderBlockColumnSize)
	{
		for (unsigned i = 0; i < diagonalLength; i++)
			diag.push_back(MatrixXd::Zero(diagonalBlockRowSize, diagonalBlockColumnSize));
		for (unsigned i = 0; i < diagonalLength - 1; i++)
			subDiag.push_back(MatrixXd::Zero(diagonalBlockRowSize, diagonalBlockColumnSize));
		for (unsigned i = 0; i < diagonalLength; i++)
			subBorder.push_back(MatrixXd::Zero(boarderBlockRowSize, diagonalBlockColumnSize));
		end = MatrixXd::Zero(boarderBlockRowSize, boarderBlockColumnSize);
	}
	void reset(void)
	{
		for (unsigned i = 0; i < diag.size(); i++)
			diag[i].setZero();
		for (unsigned i = 0; i < diag.size() - 1; i++)
			subDiag[i].setZero();
		for (unsigned i = 0; i < diag.size(); i++)
			subBorder[i].setZero();
		end.setZero();
	}
};

struct BlockVectorWithEnd: public vector<VectorXd>
{
public:
	VectorXd end;
	BlockVectorWithEnd(unsigned n, unsigned vSize, unsigned endSize)
	{
		for (unsigned i = 0; i < n; i++)
			push_back(VectorXd::Zero(vSize));
		end = VectorXd::Zero(endSize);
	}
	void reset(void)
	{
		for (unsigned i = 0; i < size(); i++)
			(*this)[i].setZero();
		end.setZero();
	}
};
bool checkMatrix(MatrixXd& matrix);
bool checkVector(VectorXd& matrix);
class SparseHessianSolver
{
	/** First draft iterative API:
	 * First: Initialize with known problem size, for static memory allocation.
	 * Second: Add estimations and state updates until dataSize updates have happened.
	 * Note: References to the (dense) matrixes is provided, matrixes are copied into place.
	 * Note: At this point additional states added will throw an error
	 * Note: addStateUpdate acts as state machine {InitialState, MiddleState, FinalState}
	 * Third: Ask for the solution to begin the backwards substitution process.
	 * Forth: Reference to solution is returned.
	 * */
private:
	enum class SolverState
		: std::int8_t
		{	Initial_State = 1, Middle_State = 2, Final_State = 3
	};
	SolverState solverState;
	unsigned stateNumber;
	VectorXd solution;
	unsigned stateSize, parameterSize, dataSize;
	MatrixXd remainderDiagonal, remainderSuperBorder, remainderEnd;
	BorderedLowerBlockBiDiagonal ltHessian;
	BorderedLowerBlockBiDiagonal Hessian;
	BlockVectorWithEnd gradient;
	BlockVectorWithEnd forwardSolution;
	BlockVectorWithEnd backSolution;
	void unsafeFinish(void);
	void unsafeAddStateCovariance(MatrixXd& matrixCovariance, unsigned state);
	void unsafeAddParameterCovariance(MatrixXd& matrixCovariance);
	void unsafeAddEstimation(MatrixXd& B, MatrixXd& K, MatrixXd& G, VectorXd& y, unsigned state);
	void unsafeStateUpdate(MatrixXd& A, MatrixXd& J, MatrixXd& H, VectorXd& x, unsigned state);
	void unsafeCholesky(unsigned state);
	void unsafeForwardSolve(unsigned state);
	void unsafeEndForwardSolve(void);
	void unsafeEndCholesky(void);
public:

	SparseHessianSolver(unsigned stateSize, unsigned parameterSize, unsigned dataSize) :
				solverState(SolverState::Initial_State),
				stateNumber(0),
				solution(stateSize * dataSize + parameterSize),
				stateSize(stateSize),
				parameterSize(parameterSize),
				dataSize(dataSize),
				ltHessian(dataSize, stateSize, parameterSize, stateSize, parameterSize),
				Hessian(dataSize, stateSize, parameterSize, stateSize, parameterSize),
				gradient(dataSize, stateSize, parameterSize),
				forwardSolution(dataSize, stateSize, parameterSize),
				backSolution(dataSize, stateSize, parameterSize)
	{
		// to ensure solvability: stateSize*dataSize+parameterSize unknowns
		// stateSize*(dataSize-1) update equations
		// measurementSize*dataSize measurement equations.
		// so stateSize+parameterSize < measurmentSize *dataSize
		remainderDiagonal = MatrixXd::Zero(stateSize, stateSize);
		remainderSuperBorder = MatrixXd::Zero(stateSize, parameterSize);
		remainderEnd = MatrixXd::Zero(parameterSize, parameterSize);

	}
	void addEstimation(MatrixXd& measurementFromStatesJacobian, MatrixXd& measurementFromParametersJacobian,
						MatrixXd& measurementCovariance, VectorXd& measurementError)
	{
		if (measurementFromStatesJacobian.rows() != measurementError.rows()
				|| measurementFromStatesJacobian.rows() != measurementFromParametersJacobian.rows()
				|| measurementFromStatesJacobian.rows() != measurementCovariance.rows()
				|| measurementFromStatesJacobian.rows() != measurementCovariance.cols())
			throw runtime_error("measurement sizes are inconsistent!");
		if (measurementFromStatesJacobian.cols() != (int) stateSize)
			throw runtime_error("state sizes are inconsistent!");
		if (measurementFromParametersJacobian.cols() != (int) parameterSize)
			throw runtime_error("parameter sizes are inconsistent!");
		if (!checkMatrix(measurementFromStatesJacobian))
			throw runtime_error("non-finite matrix element in measurementFromStatesJacobian!");
		if (!checkMatrix(measurementFromParametersJacobian))
			throw runtime_error("non-finite matrix element in measurementFromParametersJacobian!");
		if (!checkMatrix(measurementCovariance))
			throw runtime_error("non-finite matrix element in measurementCovariance!");
		if (!checkVector(measurementError))
			throw runtime_error("non-finite matrix element in measurementError!");

		unsafeAddEstimation(measurementFromStatesJacobian, measurementFromParametersJacobian, measurementCovariance,
				measurementError, stateNumber);
	}
	void addParameterTrust(MatrixXd& parameterTrustCovariance)
	{
		if (parameterTrustCovariance.cols() != (int) parameterSize
				|| parameterTrustCovariance.rows() != (int) parameterSize)
			throw runtime_error("parameter sizes are inconsistent!");
		if (!checkMatrix(parameterTrustCovariance))
			throw runtime_error("non-finite matrix element in parameterTrustCovariance!");
		unsafeAddParameterCovariance(parameterTrustCovariance);
	}
	void addEstimationWithTrust(MatrixXd& measurementFromStatesJacobian, MatrixXd& measurementFromParametersJacobian,
								MatrixXd& measurementCovariance, VectorXd& measurementError,
								MatrixXd& stateTrustCovariance)
	{
		if (measurementFromStatesJacobian.rows() != measurementError.rows()
				|| measurementFromStatesJacobian.rows() != measurementFromParametersJacobian.rows()
				|| measurementFromStatesJacobian.rows() != measurementCovariance.rows()
				|| measurementFromStatesJacobian.rows() != measurementCovariance.cols())
			throw runtime_error("measurement sizes are inconsistent!");
		if (measurementFromStatesJacobian.cols() != (int) stateSize || stateTrustCovariance.rows() != (int) stateSize
				|| stateTrustCovariance.cols() != (int) stateSize)
			throw runtime_error("state sizes are inconsistent!");
		if (measurementFromParametersJacobian.cols() != (int) parameterSize)
			throw runtime_error("parameter sizes are inconsistent!");
		if (!checkMatrix(measurementFromStatesJacobian))
			throw runtime_error("non-finite matrix element in measurementFromStatesJacobian!");
		if (!checkMatrix(measurementFromParametersJacobian))
			throw runtime_error("non-finite matrix element in measurementFromParametersJacobian!");
		if (!checkMatrix(measurementCovariance))
			throw runtime_error("non-finite matrix element in measurementCovariance!");
		if (!checkMatrix(stateTrustCovariance))
			throw runtime_error("non-finite matrix element in stateTrustCovariance!");
		if (!checkVector(measurementError))
			throw runtime_error("non-finite matrix element in measurementError!");
		unsafeAddEstimation(measurementFromStatesJacobian, measurementFromParametersJacobian, measurementCovariance,
				measurementError, stateNumber);
		unsafeAddStateCovariance(stateTrustCovariance, stateNumber);
	}
	void addStateUpdate(MatrixXd& newFromOldStateJacobian, MatrixXd& newStateFromParametersJacobian,
						MatrixXd& stateUpdateCovariance, VectorXd& stateUpdateError)
	{
		//TODO: generalize to the case where the Identity matrix used implicitly below is not identity
		if (solverState == SolverState::Final_State)
			throw std::runtime_error("all state transitions have been recorded");
		if (!checkMatrix(newFromOldStateJacobian))
			throw runtime_error("non-finite matrix element in newFromOldStateJacobian!");
		if (!checkMatrix(newStateFromParametersJacobian))
			throw runtime_error("non-finite matrix element in newStateFromParametersJacobian!");
		if (!checkMatrix(stateUpdateCovariance))
			throw runtime_error("non-finite matrix element in measurementCovariance!");
		if (!checkVector(stateUpdateError))
			throw runtime_error("non-finite matrix element in stateUpdateError!");
		unsafeStateUpdate(newFromOldStateJacobian, newStateFromParametersJacobian, stateUpdateCovariance,
				stateUpdateError, stateNumber);
		if (solverState == SolverState::Initial_State)
			solverState = SolverState::Middle_State;
		unsafeCholesky(stateNumber);
		unsafeForwardSolve(stateNumber);

		stateNumber++;
		if (stateNumber >= dataSize - 1)
			solverState = SolverState::Final_State;
	}

	void reset(void)
	{
		solverState = SolverState::Initial_State;
		stateNumber = 0;
		solution.setZero();
		remainderDiagonal.setZero();
		remainderSuperBorder.setZero();
		remainderEnd.setZero();
		ltHessian.reset();
		Hessian.reset();
		gradient.reset();
		forwardSolution.reset();
		backSolution.reset();
	}

	VectorXd& finishAndGetSolution(void)
	{
		if (solverState == SolverState::Final_State)
		{
			unsafeEndCholesky();
			unsafeEndForwardSolve();
			unsafeFinish();
			return solution;
		}
		else
		{
			throw std::runtime_error("not ready to return solution!");
		}
	}

	const MatrixXd& getLDiag(unsigned i) const
	{
		return ltHessian.diag[i];
	}

	const MatrixXd& getLEnd() const
	{
		return ltHessian.end;
	}

	const MatrixXd& getLSubBoarder(unsigned i) const
	{
		return ltHessian.subBorder[i];
	}

	const MatrixXd& getLSubDiag(unsigned i) const
	{
		return ltHessian.subDiag[i];
	}

	const MatrixXd& getHessianDiag(unsigned i) const
	{
		return Hessian.diag[i];
	}

	const MatrixXd& getHessianDiagonalEnd() const
	{
		return Hessian.end;
	}

	const MatrixXd& getHessianSubBorder(unsigned i) const
	{
		return Hessian.subBorder[i];
	}

	MatrixXd getHessianSuperDiag(unsigned i) const
	{
		return (Hessian.subDiag[i]).transpose();
	}

	const MatrixXd& getHessianSubDiag(unsigned i) const
	{
		return Hessian.subDiag[i];
	}

	const VectorXd& getGradient(unsigned i) const
	{
		return gradient[i];
	}

	const VectorXd& getGradientEnd() const
	{
		return gradient.end;
	}

	const VectorXd& getForwardSolution(unsigned i) const
	{
		return forwardSolution[i];
	}

	const VectorXd& getForwardSolutionEnd() const
	{
		return forwardSolution.end;
	}

	const VectorXd& getBackSolution(unsigned i) const
	{
		return backSolution[i];
	}

	const VectorXd& getBackSolutionEnd() const
	{
		return backSolution.end;
	}

	const VectorXd& getSolution() const
	{
		return solution;
	}
	double getLinearPredictedErrorChange() const
	{
		double predictedErrorChange = backSolution.end.dot(gradient.end);
		for (unsigned i = 0; i < backSolution.size(); i++)
			predictedErrorChange += backSolution[i].dot(gradient[i]);
		return predictedErrorChange;
	}
};

}
}
}

#endif /* STATESOLVER_HPP_ */
