/*
 * GradientDescentTrainer.hpp
 *
 *    Created on: Nov 25, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef GRADIENTDESCENTTRAINER_HPP_
#define GRADIENTDESCENTTRAINER_HPP_

#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
#include "ErrorCalculator.hpp"
#include "DataProvider.hpp"
#include "Network.hpp"
#include "GradientCalculator.hpp"
#include "PerformanceTimer.hpp"
#include <Eigen/Core>
#include <Eigen/Dense>
using namespace uta::timingUtils;
using namespace std;
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
ostream& operator<<(ostream& cout, vector<double> &arrayList);

class GradientDescentTrainer
{
protected:
	ofstream logFile;
	double timeLimitInSeconds;
	unsigned trainingEpochLimit;
protected:
	GradientCalculator* gradCalc;
	ErrorCalculator* errorCalc;
	unsigned trainingEpochs;
	string plottingFileBaseName;
	unsigned plottingFrequency;
	bool plottingEnabled;
	PerformanceTimer timer;
public:
	GradientDescentTrainer(string logFileName);
	GradientDescentTrainer();
	virtual ~GradientDescentTrainer();
	virtual void trainNetwork(DataProvider &trainingDataSet, DataProvider &validationDataSet, Trainable& network,
			double learningRate);
	void setTimeLimit(double timeInSeconds);
	void setEpochMax(unsigned maxEpoch);
	void setupLogging(string fileName, unsigned logEveryXepochs);
	double getUpcomingWeight(unsigned index)
	{
		if (index >= weights.size())
			throw std::range_error("weights");
		return weights[index];
	}
	double getUpcomingMemory(unsigned index)
	{
		if (index >= initialMemory.size())
			throw std::range_error("memory");
		return initialMemory[index];
	}
	void setInitialMemory(vector<double> initialMemory)
	{
		for (unsigned i = 0; i < initialMemory.size(); i++)
		{
			if (this->initialMemory.size() < i + 1)
				this->initialMemory.push_back(0.0);
			this->initialMemory[i] = initialMemory[i];
		}
	}
protected:
	double trainError;
	double testError;
	double eliteError;
	vector<double> eliteWeights;
	vector<double> weights;
	vector<double> initialMemory;
	vector<double> eliteInitialMemory;
	virtual void trainOneEpoch(DataProvider &trainingDataSet, Trainable& network, double learningRate);
	virtual bool doesTimerRemain(void);
	virtual bool doEpochsRemain(void);
};

}
}

#endif /* GRADIENTDESCENTTRAINER_HPP_ */
