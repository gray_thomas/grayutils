/*
 * ErrorCalculator.hpp
 *
 *    Created on: Dec 13, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef ERRORCALCULATOR_HPP_
#define ERRORCALCULATOR_HPP_

#include <vector>
#include <string>
#include "BackPropThroughTimeStructures.hpp"

#include "SystemWithMemoryPlotter.hpp"
#include "DataSet.hpp"
#include "Network.hpp"

using namespace uta::networkUtils::dyNetUtils;
using namespace std;
namespace uta
{
namespace networkUtils
{
class ErrorCalculator
{
protected:
	DataProvider& dataSet;
	Trainable& network;
	DyNetSizeManager sizes;
	double error;
	vector<double> memory;
	vector<double> initialMemory;
	SystemWithMemoryPlotter plotter;

	double errorThreshold;
	unsigned indexOfThresholdCrossing;
	bool thresholdCrossed;
	void checkErrorThreshold(unsigned dataIndex);
	void resetErrorThreshold();

	void packAndRecordInputs(unsigned dataIndex);
	void packAndRecordMemory(void);
	void unpackAndRecordOutputMemory(void);
	void unpackJudgeAndRecordOutputs(unsigned dataIndex);
	void recordUnJudgedOutputs(void);

public:
	ErrorCalculator(DataProvider& dataSet, Trainable& network);
	virtual ~ErrorCalculator()
	{
	}

	void setupPlotting(string dataSetFileNameBase, unsigned plotEveryXEpochs);
	virtual void calculate(unsigned epochNumber);
	void setInitialMemory(vector<double> initialMemory);
	double getError(void);
	DyNetSizeManager const & getSizes(void)
	{
		return sizes;
	}
	unsigned getIndexOfThresholdCrossing(void)
	{
		return this->indexOfThresholdCrossing;
	}
	void setErrorThreshold(double errorThreshold)
	{
		this->errorThreshold = errorThreshold;
	}
};
}
}

#endif /* ERRORCALCULATOR_HPP_ */
