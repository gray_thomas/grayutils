/*
 * LeastSquaresTrainer.hpp
 *
 *  Created on: Dec 29, 2013
 *      Author: Gray
 */

#ifndef LEASTSQUARESTRAINER_HPP_
#define LEASTSQUARESTRAINER_HPP_

#include <DyNetSizeManager.hpp>
#include <Eigen/Core>
#include <GradientDescentTrainer.hpp>
#include <string>

namespace uta { namespace networkUtils { class DataProvider; } }
namespace uta { namespace networkUtils { class RegressorCalculator; } }
namespace uta { namespace networkUtils { class Trainable; } }

using namespace uta::timingUtils;
using namespace std;
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
class LeastSquaresTrainer: public GradientDescentTrainer
{
protected:
	DyNetSizeManager sizes;
	RegressorCalculator* regressorCalculator;
	Matrix<double,Dynamic,1> globalEstimates;
	Matrix<double,Dynamic,1> globalCertainty;

	double dampingFactor;
	double linearizationApproximationBreakdownErrorThreshold;
	void trainOneEpoch(DataProvider &trainingDataSet, Trainable& network, double learningRate);
public:
	LeastSquaresTrainer(string logFileName) :
			GradientDescentTrainer(logFileName), sizes(), regressorCalculator(NULL), dampingFactor(0.5),
					linearizationApproximationBreakdownErrorThreshold(1e-5)
	// TODO: switch to using only the DyNet (with the sparse hessian) and GradientDescent (benito style) Trainers
	{
	}
	LeastSquaresTrainer() :
			GradientDescentTrainer(), sizes(), regressorCalculator(NULL), dampingFactor(0.5),
			linearizationApproximationBreakdownErrorThreshold(1e-5)
	{
	}
	void trainNetwork(DataProvider &trainingDataSet, DataProvider &validationDataSet, Trainable& network, double learningRate);
	void setLinearizationApproximationBreakdownError(double absoluteErrorThreshold);
};
}
}

#endif /* LEASTSQUARESTRAINER_HPP_ */
