/*
 * BackPropThroughTimeStructures.hpp
 *
 *    Created on: Dec 14, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef BACKPROPTHROUGHTIMESTRUCTURES_HPP_
#define BACKPROPTHROUGHTIMESTRUCTURES_HPP_
#include "Eigen/Dense"
#include <iostream>
using namespace Eigen;
namespace uta{
namespace networkUtils{
typedef Matrix<double, Dynamic, Dynamic> RowPartialOverColumnPartial;

typedef struct
{
	RowPartialOverColumnPartial pointError_weights;
	RowPartialOverColumnPartial pointError_memoryPosterior;
	RowPartialOverColumnPartial memory_weights;
	RowPartialOverColumnPartial memory_memoryPosterior;
	void resize(unsigned weightSize, unsigned memorySize)
	{
		pointError_weights.setZero(1, weightSize);
		pointError_memoryPosterior.setZero(1, memorySize);
		memory_weights.setZero(memorySize, weightSize);
		memory_memoryPosterior.setZero(memorySize, memorySize);
	}
} PointwiseBackPropPartials;

std::ostream& operator<<(std::ostream& stream,PointwiseBackPropPartials& partials);


class BackPropThroughTimeMemory
{
public:
	RowPartialOverColumnPartial errorPosterior_weights;
	RowPartialOverColumnPartial errorPosterior_initialMemory;
	RowPartialOverColumnPartial memoryPosterior_weights;
	RowPartialOverColumnPartial memoryPosterior_initialMemory;
	void resize(unsigned weightSize, unsigned memorySize)
	{
		errorPosterior_weights.setZero(1, weightSize);
		errorPosterior_initialMemory.setZero(1, memorySize);
		memoryPosterior_weights.setZero(memorySize, weightSize);
		memoryPosterior_initialMemory.setIdentity(memorySize, memorySize);
	}

	void updateFromPoint(PointwiseBackPropPartials& partials)
	{
		errorPosterior_weights += partials.pointError_weights
				+ partials.pointError_memoryPosterior * memoryPosterior_weights;
		errorPosterior_initialMemory += partials.pointError_memoryPosterior * memoryPosterior_initialMemory;
		memoryPosterior_initialMemory = partials.memory_memoryPosterior * memoryPosterior_initialMemory;
		memoryPosterior_weights = partials.memory_weights
				+ partials.memory_memoryPosterior * memoryPosterior_weights;
	}
} ;



}
}

#endif /* BACKPROPTHROUGHTIMESTRUCTURES_HPP_ */
