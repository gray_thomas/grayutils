/*
 * GradientCalculator.hpp
 *
 *    Created on: Dec 13, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef GRADIENTCALCULATOR_HPP_
#define GRADIENTCALCULATOR_HPP_
#include <vector>

#include "BackPropThroughTimeStructures.hpp"
#include "DataSet.hpp"
#include "ErrorCalculator.hpp"
#include "Network.hpp"
using namespace std;
namespace uta
{
namespace networkUtils
{

class GradientCalculator: public ErrorCalculator
{
protected:
	vector<double> inputs;
	vector<double> outputs;
	vector<double> desiredOutputs;
	vector<double> errorPartials;
	vector<double> weightPartials;
	vector<double> accumulatedWeightPartials;
	PointwiseBackPropPartials pointwisePartials;
	BackPropThroughTimeMemory backPropThroughTimeMemory;
	DataProvider& trainingDataSet;
	Trainable& network;
	vector<double> errors;




	void handlePointError(unsigned dataIndex);
	void zeroNonPointErrorPartials(void);
	void calculateGradientOfError(void);
	void calculateJacobianOfMemory(void);
public:
	GradientCalculator(DataProvider &trainingDataSet, Trainable& network);
	virtual ~GradientCalculator(void)
	{
	}
	void calculateGradientOverDataSet(void);
	virtual void calculate(unsigned epochNumber);
	double getError_Weights(unsigned index);
	double getError_InitialMemory(unsigned index);


private:
};
}
}
#endif /* GRADIENTCALCULATOR_HPP_ */
