/*
 * BackPropThroughTimeTrainer.hpp
 *
 *    Created on: Dec 3, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef BACKPROPTHROUGHTIMETRAINER_HPP_
#define BACKPROPTHROUGHTIMETRAINER_HPP_
#include <iostream>
#include <string>
#include <vector>

#include "BackPropThroughTimeStructures.hpp"
#include "DyNetSizeManager.hpp"
#include "GradientDescentTrainer.hpp"
#include "SystemWithMemoryPlotter.hpp"
#include "DataSet.hpp"
#include "Network.hpp"
#include "PerformanceTimer.hpp"
#include <Eigen/Core>
#include <Eigen/Dense>
using namespace uta::timingUtils;
using namespace uta::networkUtils::dyNetUtils;
using namespace std;
using namespace Eigen;
namespace uta
{
namespace networkUtils
{




class BackPropThroughTimeTrainer: public GradientDescentTrainer
{
protected:
	vector<double> memory;
	vector<double> initialMemory;
	vector<double> eliteInitialMemory;
	vector<double> inputPartials;

//
	vector<double> inputs;
	vector<double> outputs;
	vector<double> desiredOutputs;
//

	PointwiseBackPropPartials pointwisePartials;
	BackPropThroughTimeMemory backPropThroughTimeMemory;
	DyNetSizeManager sizes;
	SystemWithMemoryPlotter plotter;
public:
	BackPropThroughTimeTrainer(unsigned memSize, string logFileName);
	BackPropThroughTimeTrainer(unsigned memSize);
	void setInitialMemory(vector<double> mem);
	void setupDataRecording(string dataSetFileName, unsigned plotEveryXEpochs);
	double getMemory(unsigned index)
	{
		return initialMemory[index];
	}

protected:
	virtual void setupForEpoch(DataProvider &trainingDataSet, Trainable& network);
	virtual void handleOneDataPoint(unsigned index, DataProvider &trainingDataSet, Trainable& network);
	virtual void updateWeights(Trainable& network, double learningRate, double dataSize);
	double calculateOneDatumError(Trainable& network);
	double validateWeights(DataProvider &validationDataSet, Trainable& network);
private:
	void setNetworkInput(Trainable& network);
	void fetchNetworkOutputs(Trainable& network);
	void calculateGradientOfError(Trainable& network);
	void calculateJacobianOfMemory(Trainable& network);
	void updateMemoryFromPoint(void);
};

}
}

#endif /* BACKPROPTHROUGHTIMETRAINER_HPP_ */
