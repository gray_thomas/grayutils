/*
 * DyNetPoints.hpp
 *
 *  Created on: Feb 10, 2014
 *      Author: Gray
 */

#ifndef DYNETPOINTS_HPP_
#define DYNETPOINTS_HPP_

#include <vector>
#include "DyNetSizeManager.hpp"
#include "DyNetStates.hpp"
#include <Eigen/Dense>
using namespace uta::networkUtils::dyNetUtils;
using namespace std;
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{

struct DyNetPoint
{
	DyNetStates dyNetStates;
	vector<double> weights;
	double error;
	double measurementError;
	double transitionError;
	DyNetPoint(DyNetSizeManager& sizes) :
			dyNetStates(sizes), weights(), error(1e99), measurementError(0), transitionError(0)
	{
		weights.resize(sizes.weightSize);
	}

};
struct DyNetVectorDelta
{
	VectorXd delta;
	double predictedChangeInError;
	DyNetVectorDelta(DyNetSizeManager& sizes) :
			delta(), predictedChangeInError(0)
	{
		delta.setZero(sizes.dataSize * sizes.memorySize + sizes.weightSize);
	}
};
}
}
}

#endif /* DYNETPOINTS_HPP_ */
