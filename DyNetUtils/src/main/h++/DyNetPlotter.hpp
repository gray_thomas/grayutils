/*
 * DyNetPlotter.hpp
 *
 *    Created on: Feb 9, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef DYNETPLOTTER_HPP_
#define DYNETPLOTTER_HPP_

#include <vector>
#include <string>
#include "DataSet.hpp"
#include "DyNetSizeManager.hpp"
#include "DyNetPoints.hpp"
#include <DyNetOutputs.hpp>
#include <iostream>
#include <Eigen/Dense>
#include <sstream>
using namespace Eigen;
using namespace std;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
class DyNetPlotter
{
	DyNetSizeManager& sizes;
	std::vector<string> inputNames;
	std::vector<string> measurementNames;
	std::vector<string> parameterNames;
	std::vector<string> stateNames;
	unsigned inputSize;
	unsigned measurementSize;
	unsigned parameterSize;
	unsigned stateSize;
	unsigned epochNumber;
	unsigned dataSize;
	struct TrainingData
	{
		vector<vector<double>> inputs;
		vector<vector<double>> measurements;
	} trainingData;
	struct EpochUpdatePrediction
	{
		vector<double> parameterDelta;
		vector<vector<double>> statesDelta;
		double predictedErrorChange;
		double trustParameter;
		EpochUpdatePrediction(DyNetSizeManager& sizes, DyNetVectorDelta& delta, double trustParameter);
	};
	struct EpochData
	{
		double error;
		vector<vector<double>> statesUsed;
		vector<double> parametersUsed;
		vector<vector<double>> statePrediction;
		vector<vector<double>> measurementPrediction;
		EpochUpdatePrediction updatePrediction;
		EpochData(DyNetSizeManager& sizes, DyNetPoint& point, DyNetVectorDelta& delta,
					vector<vector<double> >& statePrediction, vector<vector<double> > &measurementPrediction,
					double trustParameter);
	};
	vector<EpochData> epochData;
	vector<vector<double> > tempStatePrediction;
	vector<vector<double> > tempMeasurementPrediction;

public:
	DyNetPlotter(DyNetSizeManager& sizes, string fileName);
	void setData(DataProvider& dataSet);
	void setInputNames(vector<string>& inputNames);
	void setMeasurementNames(vector<string>& measurementNames);
	void setParameterNames(vector<string>& parameterNames);
	void setStateNames(vector<string>& stateNames);
	void logOutput(unsigned index, DyNetOutputs& outputs);
	void logEpoch(DyNetPoint& point, DyNetVectorDelta& delta, double trustParameter);
	void renderYAML(ostream& ostream);
	void render1EpochYAML(ostream& ostream, unsigned int epoch);
};
void altRenderYAML(void);
void populateEmitter();
}
}
}

#endif /* DYNETPLOTTER_HPP_ */
