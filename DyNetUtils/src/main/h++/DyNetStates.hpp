/*
 * DyNetStates.hpp
 *
 *    Created on: Feb 10, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef DYNETSTATES_HPP_
#define DYNETSTATES_HPP_

#include "Eigen/Dense"
#include "DyNetSizeManager.hpp"
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
class DyNetStates: public VectorXd
{
	unsigned memorySize;
public:
	DyNetStates(DyNetSizeManager& sizes);
	Block<VectorXd> getState(unsigned dataIndex);
};
}
}
}

#endif /* DYNETSTATES_HPP_ */
