/*
 * DenseDyNetRegressor.hpp
 *
 *    Created on: Feb 10, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef DENSEDYNETREGRESSOR_HPP_
#define DENSEDYNETREGRESSOR_HPP_

#include "Eigen/Dense"
#include "DyNetSizeManager.hpp"
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
class DenseDyNetRegressor: public MatrixXd
{
public:
	unsigned memorySize;
	unsigned outputSize;
	unsigned inputSize;
	unsigned weightSize;
	unsigned dataSize;
	Block<MatrixXd> memoryComparisonWeightComponent(unsigned dataIndex);
	Block<MatrixXd> memoryComparisonFirstMemoryComponent(unsigned dataIndex);
	Block<MatrixXd> memoryComparisonSecondMemoryComponent(unsigned dataIndex);
	Block<MatrixXd> outputComparisonWeightComponent(unsigned dataIndex);
	Block<MatrixXd> outputComparisonMemoryComponent(unsigned dataIndex);
	DenseDyNetRegressor(DyNetSizeManager& sizes);

};
}
}
}
#endif /* DENSEDYNETREGRESSOR_HPP_ */
