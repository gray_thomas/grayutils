/*
 * SystemWithMemorySizeManager.hpp
 *
 *    Created on: Dec 14, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef SYSTEMWITHMEMORYSIZEMANGER_HPP_
#define SYSTEMWITHMEMORYSIZEMANGER_HPP_
#include <stdexcept>
using namespace std;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
class DyNetSizeManager
{
public:
	unsigned memorySize;
	unsigned weightSize;
	unsigned outputSize;
	unsigned inputSize;
	unsigned unjudgedSize;
	unsigned dataSize;
	unsigned totalOutputSize();
	unsigned totalInputSize();
	DyNetSizeManager();
	DyNetSizeManager(unsigned dataSize, unsigned dataInputs, unsigned dataOutputs, unsigned networkInputs,
						unsigned networkOutputs, unsigned networkWeights);

};
}
}
}
#endif /* SYSTEMWITHMEMORYSIZEMANGER_HPP_ */
