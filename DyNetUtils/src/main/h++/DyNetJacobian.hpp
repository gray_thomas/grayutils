/*
 * DyNetJacobian.hpp
 *
 *    Created on: Feb 10, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef DYNETJACOBIAN_HPP_
#define DYNETJACOBIAN_HPP_
#include "Eigen/Dense"
#include "DyNetSizeManager.hpp"
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
class DyNetJacobian: public MatrixXd
{
public:
	Block<MatrixXd> outputs_memory;
	Block<MatrixXd> outputs_weights;
	Block<MatrixXd> memory_memory;
	Block<MatrixXd> memory_weights;
	Block<MatrixXd> all_memory;
	Block<MatrixXd> all_weights;
	DyNetJacobian(DyNetSizeManager& sizes);

};

}
}
}


#endif /* DYNETJACOBIAN_HPP_ */
