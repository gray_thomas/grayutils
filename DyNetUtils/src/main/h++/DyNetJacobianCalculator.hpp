/*
 * DyNetJacobianCalculator.hpp
 *
 *    Created on: Feb 10, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef DYNETJACOBIANCALCULATOR_HPP_
#define DYNETJACOBIANCALCULATOR_HPP_
#include "Network.hpp"
#include "DyNetInputs.hpp"
#include "DyNetOutputs.hpp"
#include "DyNetJacobian.hpp"

namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
class DyNetJacobianCalculator
{
	Trainable& network;
	unsigned weightSize;
public:
	DyNetJacobianCalculator(Trainable& network) :
				network(network),
				weightSize(network.weightSize())
	{
	}
	void calculateDyNetJacobian(DyNetInputs& inputs, DyNetOutputs& outputs, DyNetJacobian& jacobian);
};
}
}
}

#endif /* DYNETJACOBIANCALCULATOR_HPP_ */
