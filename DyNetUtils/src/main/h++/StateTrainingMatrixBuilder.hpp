/*
 * StateTrainingMatrixBuilder.hpp
 *
 *  Created on: Jan 4, 2014
 *      Author: Gray
 */

#ifndef FLOATINGMEMORYREGRESSORCALCULATOR_HPP_
#define FLOATINGMEMORYREGRESSORCALCULATOR_HPP_

#include <vector>

#include "BackPropThroughTimeStructures.hpp"
#include "SparseHessianSolver.hpp"
#include "DyNetSizeManager.hpp"
#include "DataSet.hpp"
#include "ErrorCalculator.hpp"
#include "GradientCalculator.hpp"
#include "Network.hpp"
#include "Eigen/Core"
#include "DyNetJacobianCalculator.hpp"
#include "DyNetJacobian.hpp"
#include "DyNetOutputs.hpp"
#include "DyNetInputs.hpp"
#include "DenseDyNetRegressor.hpp"
#include "DyNetErrorVector.hpp"
#include "DyNetStates.hpp"
using namespace std;
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{




class StateTrainingMatrixBuilder
{
protected:

	DyNetSizeManager sizes;
	DenseDyNetRegressor regressorMatrix;
	DyNetJacobian jacobian;
	DyNetErrorVector regressionErrorVector;

	DataProvider& dataSet;
	Trainable& network;

	double error;
//	SystemWithMemoryPlotter plotter;
	DyNetStates memories;
	DyNetInputs networkInputs;
	DyNetOutputs networkOutputs;
	DyNetJacobianCalculator dyNetJacobianCalculator;
	SparseHessianSolver solver;
	VectorXd solution;
public:
	StateTrainingMatrixBuilder(DataProvider& trainingDataSet, Trainable& network);
	DyNetSizeManager const & viewSizes() const
	{
		return sizes;
	}
protected:
	void setupSizes(void);
	VectorXd getInputs(unsigned dataIndex);
	VectorXd getOutputs(unsigned dataIndex);
	void recordPlottableData();

	void calculate(unsigned epochNumber);

	void setMemories(VectorXd& memories)
	{
		for (int i = 0; i < memories.rows(); i++)
			this->memories(i) = memories(i);
	}
	VectorXd const & getSolution() const;
	MatrixXd const & getRegressor() const
	{
		return regressorMatrix;
	}
	VectorXd const & getRegressionError() const
	{
		return regressionErrorVector;
	}
};
}
}
}

#endif /* FLOATINGMEMORYREGRESSORCALCULATOR_HPP_ */
