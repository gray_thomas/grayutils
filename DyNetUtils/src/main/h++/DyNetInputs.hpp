/*
 * DyNetInputs.hpp
 *
 *    Created on: Feb 10, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef DYNETINPUTS_HPP_
#define DYNETINPUTS_HPP_

#include "Eigen/Dense"
#include "DyNetSizeManager.hpp"
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
class DyNetInputs: public VectorXd
{
public:
	Block<VectorXd> mem;
	Block<VectorXd> inputs;
	DyNetInputs(DyNetSizeManager& sizes);
};
}
}
}
#endif /* DYNETINPUTS_HPP_ */
