/*
 * RegressorCalculator.hpp
 *
 *  Created on: Dec 29, 2013
 *      Author: Gray
 */

#ifndef REGRESSORCALCULATOR_HPP_
#define REGRESSORCALCULATOR_HPP_

#include <vector>

#include "BackPropThroughTimeStructures.hpp"
#include "DataSet.hpp"
#include "ErrorCalculator.hpp"
#include "GradientCalculator.hpp"
#include "Network.hpp"
#include "Eigen/Core"
using namespace std;
namespace uta
{
namespace networkUtils
{

class RegressorCalculator: public GradientCalculator
{
protected:
	MatrixXd regressorMatrix;
	MatrixXd errors_memory;
	MatrixXd errors_weights;
	VectorXd regressionErrorVector;
	void calculateJacobianOfErrors(void);
public:
	RegressorCalculator(DataProvider &trainingDataSet, Trainable& network);
	virtual ~RegressorCalculator(void)
	{
	}
	virtual void calculate(unsigned epochNumber);
	MatrixXd const & getRegressor() const
	{
		return regressorMatrix;
	}
	VectorXd const & getRegressionError() const
	{
		return regressionErrorVector;
	}

private:
	void zeroOutputPartials();
};
}
}

#endif /* REGRESSORCALCULATOR_HPP_ */
