/*
 * DyNetInputs.cpp
 *
 *    Created on: Feb 11, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "DyNetInputs.hpp"
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{

DyNetInputs::DyNetInputs(DyNetSizeManager& sizes) :
			VectorXd(sizes.memorySize + sizes.inputSize),
			mem(this->block(0, 0, sizes.memorySize, 1)),
			inputs(this->block(sizes.memorySize, 0, sizes.inputSize, 1))
{
	this->setZero();
}
}
}
}
