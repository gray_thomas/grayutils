/*
 * DyNetTrainer.cpp
 *
 *  Created on: Jan 9, 2014
 *      Author: Gray
 */

#include "DyNetTrainer.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
#include <cmath>
#include <sstream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include "StateTrainingMatrixBuilder.hpp"
#include "DyNetSizeManager.hpp"
#include "DataSet.hpp"
#include "Network.hpp"
#include "PerformanceTimer.hpp"
#include "DyNetErrorVector.hpp"
using namespace uta::timingUtils;
using namespace std;
using namespace Eigen;
using namespace uta::networkUtils::dyNetUtils;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{

DyNetTrainer::DyNetTrainer(DyNetTrainerParameters params, DataProvider &trainingDataSet, Trainable& network) :
		params(params), dataSet(trainingDataSet), network(network), epochError(0),
				sizes(trainingDataSet.viewSize(), trainingDataSet.viewInputSize(), trainingDataSet.viewOutputSize(),
						network.inputSize(), network.outputSize(), network.weightSize()), jacobian(sizes),
				regressionErrorVector(sizes), error(0.0), dyNetJacobianCalculator(network),
				sparseHessianSolver(sizes.memorySize, sizes.weightSize, sizes.dataSize), basePoint(sizes),
				elitePoint(sizes), testPoint(sizes), testVectorDelta(sizes), tempVectorDelta(sizes),
				plotter(sizes, "fred.yaml"), firstPointGenerated(false), updateWeighting(), measurementWeighting()
{
	updateWeighting.setIdentity(sizes.memorySize, sizes.memorySize);
	measurementWeighting.setIdentity(sizes.outputSize, sizes.outputSize);
	stateDamping.setIdentity(sizes.memorySize, sizes.memorySize);
	parameterDamping.setIdentity(sizes.weightSize, sizes.weightSize);
	// to ensure solvability: stateSize*dataSize+parameterSize unknowns
	// stateSize*(dataSize-1) update equations
	// measurementSize*dataSize measurement equations.
	// so stateSize+parameterSize < measurmentSize *dataSize
	if (sizes.memorySize + sizes.weightSize > sizes.outputSize * sizes.dataSize)
		throw std::runtime_error("system is underconstrained. present different problem.");
	plotter.setData(trainingDataSet);
}
DyNetValidator::DyNetValidator(DyNetTrainerParameters params, DataProvider &validationDataSet, Trainable& network) :
		params(params), dataSet(validationDataSet), network(network), epochError(0),
				sizes(validationDataSet.viewSize(), validationDataSet.viewInputSize(),
						validationDataSet.viewOutputSize(), network.inputSize(), network.outputSize(),
						network.weightSize()), jacobian(sizes), regressionErrorVector(sizes), error(0.0),
				dyNetJacobianCalculator(network), sparseHessianSolver(sizes.memorySize, 0, sizes.dataSize),
				basePoint(sizes), elitePoint(sizes), testPoint(sizes), testVectorDelta(sizes), tempVectorDelta(sizes),
				firstPointGenerated(false), updateWeighting(), measurementWeighting()
{
	updateWeighting.setIdentity(sizes.memorySize, sizes.memorySize);
	measurementWeighting.setIdentity(sizes.outputSize, sizes.outputSize);
	stateDamping.setIdentity(sizes.memorySize, sizes.memorySize);
//	parameterDamping.setIdentity(sizes.weightSize, sizes.weightSize);
	// to ensure solvability: stateSize*dataSize+parameterSize unknowns
	// stateSize*(dataSize-1) update equations
	// measurementSize*dataSize measurement equations.
	// so stateSize+parameterSize < measurmentSize *dataSize
	if (sizes.memorySize + sizes.weightSize > sizes.outputSize * sizes.dataSize)
		throw std::runtime_error("system is underconstrained. present different problem.");
}

void DyNetTrainer::setNames(vector<string>& measurementNames, vector<string>& inputNames, vector<string>& stateNames,
		vector<string>& parameterNames)
{
	plotter.setMeasurementNames(measurementNames);
	plotter.setInputNames(inputNames);
	plotter.setStateNames(stateNames);
	plotter.setParameterNames(parameterNames);
}

void DyNetTrainer::setRecordFromDelta(DyNetPoint& recordToSet, DyNetPoint& baseRecord, DyNetVectorDelta& suggestion)
{
	recordToSet.weights.resize(sizes.weightSize);
	for (unsigned w = 0; w < sizes.weightSize; w++)
		recordToSet.weights[w] = baseRecord.weights[w] + suggestion.delta[sizes.dataSize * sizes.memorySize + w];
	for (unsigned m = 0; m < sizes.dataSize * sizes.memorySize; m++)
		recordToSet.dyNetStates[m] = baseRecord.dyNetStates[m] + suggestion.delta[m];
}
void DyNetValidator::setRecordFromDelta(DyNetPoint& recordToSet, DyNetPoint& baseRecord, DyNetVectorDelta& suggestion)
{
	recordToSet.weights.resize(sizes.weightSize);
	for (unsigned w = 0; w < sizes.weightSize; w++)
		recordToSet.weights[w] = baseRecord.weights[w] + suggestion.delta[sizes.dataSize * sizes.memorySize + w];
	for (unsigned m = 0; m < sizes.dataSize * sizes.memorySize; m++)
		recordToSet.dyNetStates[m] = baseRecord.dyNetStates[m] + suggestion.delta[m];
}
void DyNetTrainer::generateFirstPoint()
{
	cout << "generating initial record" << endl;
	network.packWeights(basePoint.weights);
	calculatePointErrorAndDelta(basePoint, testVectorDelta);
	firstPointGenerated = true;
}
void DyNetValidator::generateFirstPoint()
{
	cout << "generating initial record" << endl;
	network.packWeights(basePoint.weights);
	calculatePointErrorAndDelta(basePoint, testVectorDelta);
	firstPointGenerated = true;
}

void DyNetTrainer::trainOneEpoch()
{
	if (!firstPointGenerated)
		generateFirstPoint();

	setRecordFromDelta(testPoint, basePoint, testVectorDelta);

	calculatePointErrorAndDelta(testPoint, tempVectorDelta);
	double predictedTestPointError = basePoint.error + testVectorDelta.predictedChangeInError;
	double actuatalTestPointError = testPoint.error;
	double predictionError = abs(actuatalTestPointError - predictedTestPointError);
	double scaledPredictionError = predictionError / (basePoint.error + 1e-16);

//	cout << "predictedTestPointError is " << predictedTestPointError << endl;
//	cout << "actuatalTestPointError is " << actuatalTestPointError << endl;
//	cout << "prediction error is " << predictionError << endl;
//	cout << "scaled prediction error is " << scaledPredictionError << endl;
//
//	if (scaledPredictionError > params.linearitySetPoint)
//	{
//		params.trustRegionScaleParameter *= params.trustRegionPenalty;
//		cout << "error too high, decreasing trust region size to " << params.trustRegionScaleParameter << endl;
//	}
//	else if (scaledPredictionError < params.linearityMinimumError)
//	{
//		params.trustRegionScaleParameter *= params.trustRegionPositiveBump;
//		cout << "convergence hindered by conservative trust region, increasing trust region size" << endl;
//	}
//	else
//	{
//		cout << "trust region is appropriate" << endl;
//	}

	epochError = basePoint.error;
	testVectorDelta = tempVectorDelta;
	basePoint = testPoint;
}
void DyNetValidator::trainOneEpoch()
{
	if (!firstPointGenerated)
		generateFirstPoint();

	setRecordFromDelta(testPoint, basePoint, testVectorDelta);

	calculatePointErrorAndDelta(testPoint, tempVectorDelta);
	double predictedTestPointError = basePoint.error + testVectorDelta.predictedChangeInError;
	double actuatalTestPointError = testPoint.error;
	double predictionError = abs(actuatalTestPointError - predictedTestPointError);
	double scaledPredictionError = predictionError / (basePoint.error + 1e-16);

//	cout << "predictedTestPointError is " << predictedTestPointError << endl;
//	cout << "actuatalTestPointError is " << actuatalTestPointError << endl;
//	cout << "prediction error is " << predictionError << endl;
//	cout << "scaled prediction error is " << scaledPredictionError << endl;
//
//	if (scaledPredictionError > params.linearitySetPoint)
//	{
//		params.trustRegionScaleParameter *= params.trustRegionPenalty;
//		cout << "error too high, decreasing trust region size to " << params.trustRegionScaleParameter << endl;
//	}
//	else if (scaledPredictionError < params.linearityMinimumError)
//	{
//		params.trustRegionScaleParameter *= params.trustRegionPositiveBump;
//		cout << "convergence hindered by conservative trust region, increasing trust region size" << endl;
//	}
//	else
//	{
//		cout << "trust region is appropriate" << endl;
//	}

	epochError = basePoint.error;
	testVectorDelta = tempVectorDelta;
	basePoint = testPoint;
}
//TODO: these high level training functions only use the train one epoch function. They could be static and work on an interface.
void DyNetTrainer::trainWithTimeLimit(double seconds)
{
}
void DyNetTrainer::trainWithEpochLimit(unsigned epochs)
{
}
void DyNetTrainer::trainWithTwoLimits(double seconds, unsigned epochs)
{
}
VectorXd initializeStates(Trainable& network, DataProvider& trainingData, VectorXd& initialConditions)
{
	VectorXd states;
	unsigned memSize = initialConditions.rows();
	cout << "initialize states, memSize is " << memSize << endl;
	states.setZero(memSize * trainingData.viewSize());
	for (unsigned j = 0; j < memSize; j++)
	{
		states(j) = initialConditions(j);
	}
	for (unsigned i = 0; i < (trainingData.viewSize() - 1); i++)
	{
		for (unsigned j = 0; j < memSize; j++)
		{
			network.setInput(j, (double) states(i * memSize + j));
		}
		for (unsigned j = memSize; j < network.inputSize(); j++)
			network.setInput(j, trainingData.getInput(i, j - memSize));
		network.calculateOutputs();
		for (unsigned j = 0; j < memSize; j++)
		{
			states((i + 1) * memSize + j) = network.getOutput(j);
		}
	}
	return states;
}
VectorXd initializeStatesZero(DataProvider& trainingData, VectorXd& initialConditions)
{
	VectorXd states;
	unsigned memSize = initialConditions.rows();
	cout << "initialize states to zero, memSize is " << memSize << endl;
	states.setZero(memSize * trainingData.viewSize());
	return states;
}

void DyNetTrainer::log(string filebase, unsigned number)
{
	ofstream outStream;
	stringstream ss;
	ss << filebase << number << ".yaml";
	outStream.open(ss.str().c_str());
	if (outStream.is_open())
	{
		plotter.renderYAML(outStream);
		cout << "successfully output to file " << ss.str() << endl;
	}
	else
		cerr << "failed to open file " << ss.str() << endl;
	outStream.close();
}
void DyNetTrainer::log1Epoch(string filebase, unsigned number, unsigned epoch)
{
	ofstream outStream;
	stringstream ss;
	ss << filebase << number <<"Epoch"<<epoch<< ".yaml";
	outStream.open(ss.str().c_str());
	if (outStream.is_open())
	{
		plotter.render1EpochYAML(outStream, epoch);
		cout << "successfully output to file " << ss.str() << endl;
	}
	else
		cerr << "failed to open file " << ss.str() << endl;
	outStream.close();
}

void setCovarianceMatrixFromStandardDeviationVector(MatrixXd& mat, VectorXd& vec)
{
	if (mat.rows() != vec.rows())
		throw runtime_error("setCovarianceMatrixFromStandardDeviationVector: sizes do not match");
	if (mat.cols() != vec.rows())
		throw runtime_error("setCovarianceMatrixFromStandardDeviationVector: sizes do not match");
	mat.setIdentity();
	for (unsigned i = 0; i < vec.rows(); i++)
		mat(i, i) = 1.0 / (vec(i) * vec(i));
}
void setMatrixIfSizeMatches(MatrixXd& mat, const MatrixXd& newMat)
{
	if (mat.rows() != newMat.rows())
		throw runtime_error("setMatrixIfSizeMatches: sizes do not match");
	if (mat.cols() != newMat.cols())
		throw runtime_error("setMatrixIfSizeMatches: sizes do not match");
	mat = newMat;
}
void DyNetTrainer::calculatePointErrorAndDelta(DyNetPoint& point, DyNetVectorDelta& deltaToPack)
{
	point.error = 0;
	sparseHessianSolver.reset();
	network.setWeights(point.weights);
	//TODO: add damping to the parameters in addition to the existing damping on the states.
	point.measurementError = 0;
	point.transitionError = 0;
	DyNetInputs dyNetInputs(sizes);
	DyNetOutputs dyNetOutputs(sizes);
	MatrixXd T = parameterDamping * (1 / params.trustRegionScaleParameter);
	sparseHessianSolver.addParameterTrust(T);

	for (unsigned dataIndex = 0; dataIndex < dataSet.viewSize(); dataIndex++)
	{
		dyNetInputs.mem = point.dyNetStates.getState(dataIndex);
		dyNetInputs.inputs = getInputs(dataIndex);
		dyNetJacobianCalculator.calculateDyNetJacobian(dyNetInputs, dyNetOutputs, jacobian);
		plotter.logOutput(dataIndex, dyNetOutputs);
		regressionErrorVector.outputError(dataIndex) = getOutputs(dataIndex) - dyNetOutputs.outputs;
		MatrixXd B = MatrixXd(jacobian.outputs_memory);
		MatrixXd K = MatrixXd(jacobian.outputs_weights);
		MatrixXd G = this->measurementWeighting;
		VectorXd y = VectorXd(getOutputs(dataIndex) - dyNetOutputs.outputs);
		MatrixXd T = this->stateDamping * (1 / params.trustRegionScaleParameter);
		point.error += y.transpose() * G * y;
		point.measurementError += y.transpose() * G * y;

//		sparseHessianSolver.addEstimation(B, K, G, y);
		//TODO: implement trust region with a non-equal weighting on the various states and parameters.
		sparseHessianSolver.addEstimationWithTrust(B, K, G, y, T);
		if (dataIndex + 1 < sizes.dataSize)
		{
			// Note: we assume a fixed structure for measurements for now.
			VectorXd error = point.dyNetStates.getState(dataIndex + 1) - dyNetOutputs.mem;
			regressionErrorVector.memoryError(dataIndex) = error;
			MatrixXd A = MatrixXd(jacobian.memory_memory);
			MatrixXd J = MatrixXd(jacobian.memory_weights);
			MatrixXd H = this->updateWeighting;
			sparseHessianSolver.addStateUpdate(A, J, H, error);
			point.error += error.transpose() * H * error;
			point.transitionError += error.transpose() * H * error;
		}
	}
	cout << "Training : updateE = " << point.transitionError << " measureE = "
			<< point.measurementError << " totalE = " << point.error << endl;
	deltaToPack.delta = sparseHessianSolver.finishAndGetSolution();
	for (unsigned i = 0; i < deltaToPack.delta.rows(); i++)
		if (std::isnan((double) deltaToPack.delta[i]))
			deltaToPack.delta[i] = 0;
	deltaToPack.predictedChangeInError = -sparseHessianSolver.getLinearPredictedErrorChange();
	if (point.error < elitePoint.error)
		elitePoint = point; // should be a deep copy of a pure data struct.

	plotter.logEpoch(point, deltaToPack, this->params.trustRegionScaleParameter);

}
void DyNetValidator::calculatePointErrorAndDelta(DyNetPoint& point, DyNetVectorDelta& deltaToPack)
{
	point.error = 0;
	sparseHessianSolver.reset();
	network.setWeights(point.weights);
	//TODO: add damping to the parameters in addition to the existing damping on the states.
	point.measurementError = 0;
	point.transitionError = 0;
	DyNetInputs dyNetInputs(sizes);
	DyNetOutputs dyNetOutputs(sizes);
	for (unsigned dataIndex = 0; dataIndex < dataSet.viewSize(); dataIndex++)
	{
		dyNetInputs.mem = point.dyNetStates.getState(dataIndex);
		dyNetInputs.inputs = getInputs(dataIndex);
		dyNetJacobianCalculator.calculateDyNetJacobian(dyNetInputs, dyNetOutputs, jacobian);
		regressionErrorVector.outputError(dataIndex) = getOutputs(dataIndex) - dyNetOutputs.outputs;
		MatrixXd B = MatrixXd(jacobian.outputs_memory);
		MatrixXd K = MatrixXd::Zero(sizes.outputSize, 0);
		MatrixXd G = this->measurementWeighting;
		VectorXd y = VectorXd(getOutputs(dataIndex) - dyNetOutputs.outputs);
		MatrixXd T = this->stateDamping * (1 / params.trustRegionScaleParameter);
		point.error += y.transpose() * G * y;
		point.measurementError += y.transpose() * G * y;

		//TODO: implement trust region with a non-equal weighting on the various states and parameters.
		sparseHessianSolver.addEstimationWithTrust(B, K, G, y, T);
		if (dataIndex + 1 < sizes.dataSize)
		{
			// Note: we assume a fixed structure for measurements for now.
			VectorXd error = point.dyNetStates.getState(dataIndex + 1) - dyNetOutputs.mem;
			regressionErrorVector.memoryError(dataIndex) = error;
			MatrixXd A = MatrixXd(jacobian.memory_memory);
			MatrixXd J = MatrixXd::Zero(sizes.memorySize, 0);
			MatrixXd H = this->updateWeighting;
			sparseHessianSolver.addStateUpdate(A, J, H, error);
			point.error += error.transpose() * H * error;
			point.transitionError += error.transpose() * H * error;
		}
	}
	cout << "validation : updateE = " << point.transitionError << " measureE = "
			<< point.measurementError << " totalE = " << point.error << endl;

	deltaToPack.delta.head(sizes.memorySize * sizes.dataSize) << sparseHessianSolver.finishAndGetSolution();
	for (unsigned i = 0; i < deltaToPack.delta.rows(); i++)
		if (std::isnan((double) deltaToPack.delta[i]))
			deltaToPack.delta[i] = 0; // never introduce nan into the state deltas
	deltaToPack.predictedChangeInError = -sparseHessianSolver.getLinearPredictedErrorChange();
	if (point.error < elitePoint.error)
		elitePoint = point; // should be a deep copy of a pure data struct.
}

VectorXd DyNetTrainer::getInputs(unsigned dataIndex)
{
	VectorXd inputs(sizes.inputSize);
	for (unsigned i = 0; i < sizes.inputSize; i++)
		inputs(i) = dataSet.getInput(dataIndex, i);
	return inputs;
}
VectorXd DyNetTrainer::getOutputs(unsigned dataIndex)
{
	VectorXd outputs(sizes.outputSize);
	for (unsigned i = 0; i < sizes.outputSize; i++)
		outputs(i) = dataSet.getOutput(dataIndex, i);
	return outputs;
}
VectorXd DyNetValidator::getInputs(unsigned dataIndex)
{
	VectorXd inputs(sizes.inputSize);
	for (unsigned i = 0; i < sizes.inputSize; i++)
		inputs(i) = dataSet.getInput(dataIndex, i);
	return inputs;
}
VectorXd DyNetValidator::getOutputs(unsigned dataIndex)
{
	VectorXd outputs(sizes.outputSize);
	for (unsigned i = 0; i < sizes.outputSize; i++)
		outputs(i) = dataSet.getOutput(dataIndex, i);
	return outputs;
}
}

}
}

