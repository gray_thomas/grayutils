/*
 * RegressorCalculator.cpp
 *
 *  Created on: Dec 29, 2013
 *      Author: Gray
 */

#include "RegressorCalculator.hpp"
#include "DyNetSizeManager.hpp"
#include <iostream>
#include <string>
#include <vector>

#include "PerformanceTimer.hpp"
#include "DataSet.hpp"
#include "Network.hpp"
#define NO_TIME_LIMIT -1.0
#define NO_EPOCH_LIMIT 0
using namespace std;
using namespace uta::timingUtils;
namespace uta
{
namespace networkUtils
{
RegressorCalculator::RegressorCalculator(DataProvider &trainingDataSet, Trainable& network) :
		GradientCalculator(trainingDataSet, network), regressorMatrix(), errors_memory(), errors_weights(),
				regressionErrorVector()
{
	errors_memory.setZero(sizes.outputSize, sizes.memorySize);
	errors_weights.setZero(sizes.outputSize, sizes.weightSize);
	regressorMatrix.setZero(trainingDataSet.viewSize() * sizes.outputSize, sizes.weightSize + sizes.memorySize);
	regressionErrorVector.setZero(trainingDataSet.viewSize() * sizes.outputSize);
}

void RegressorCalculator::calculate(unsigned epochNumber)
{
	for (unsigned m = 0; m < sizes.memorySize; m++)
		memory[m] = initialMemory[m];
	error = 0;
	plotter.prepareEpoch();
	pointwisePartials.resize(sizes.weightSize, sizes.memorySize);
	backPropThroughTimeMemory.resize(sizes.weightSize, sizes.memorySize);
	resetErrorThreshold();
	for (unsigned dataIndex = 0; dataIndex < dataSet.viewSize(); dataIndex++)
	{
		packAndRecordMemory();
		packAndRecordInputs(dataIndex);

		network.calculateOutputs();
		unpackAndRecordOutputMemory();
		zeroNonPointErrorPartials();
		handlePointError(dataIndex);
		for (unsigned o = 0; o < sizes.outputSize; o++)
			regressionErrorVector(dataIndex * sizes.outputSize + o) = errors[o];
		checkErrorThreshold(dataIndex);
		plotter.recordData();
		calculateGradientOfError();
		calculateJacobianOfErrors();
		calculateJacobianOfMemory();
		regressorMatrix.block(sizes.outputSize * dataIndex, 0, sizes.outputSize, sizes.memorySize) = errors_memory
				* backPropThroughTimeMemory.memoryPosterior_initialMemory;
		regressorMatrix.block(sizes.outputSize * dataIndex, sizes.memorySize, sizes.outputSize, sizes.weightSize) =
				errors_weights + errors_memory * backPropThroughTimeMemory.memoryPosterior_weights;
		cout << "new regressor block = \n"
				<< regressorMatrix.block(sizes.outputSize * dataIndex, 0, sizes.outputSize,
						sizes.memorySize + sizes.outputSize) << "\n";

		backPropThroughTimeMemory.updateFromPoint(pointwisePartials);
	}
	plotter.wrapUpAndSaveFile(epochNumber);
}

void RegressorCalculator::zeroOutputPartials()
{
	for (unsigned m = 0; m < sizes.memorySize; m++)
		network.setOutputPartial(m, 0.0);
	for (unsigned o = 0; o < sizes.outputSize; o++)
		network.setOutputPartial(sizes.memorySize + o, 0.0);
	for (unsigned uj = 0; uj < sizes.unjudgedSize; uj++)
		network.setOutputPartial(sizes.memorySize + sizes.outputSize + uj, 0.0);
}

void RegressorCalculator::calculateJacobianOfErrors()
{
	zeroOutputPartials();
	for (unsigned o = 0; o < sizes.outputSize; o++)
	{
		network.setOutputPartial(sizes.memorySize + o, errors[o]);
		network.calculatePartials();
		for (unsigned m = 0; m < sizes.memorySize; m++)
			errors_memory(o, m) = network.getInputPartial(m);
		for (unsigned w = 0; w < sizes.weightSize; w++)
			errors_weights(o, w) = network.getWeightPartial(w);
		network.setOutputPartial(sizes.memorySize + o, 0.0);
	}
}

}
}
