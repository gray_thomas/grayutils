/*
 * DyNetJacobianCalculator.cpp
 *
 *    Created on: Feb 11, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include "DyNetJacobianCalculator.hpp"

namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
void DyNetJacobianCalculator::calculateDyNetJacobian(DyNetInputs& inputs, DyNetOutputs& outputs,
														DyNetJacobian& jacobian)
{
	for (unsigned i = 0; (int) i < inputs.rows(); i++)
		network.setInput(i, (double) inputs(i)); // cast unnecessary
	network.calculateOutputs();
	for (unsigned i = 0; (int) i < outputs.rows(); i++)
		outputs(i, 0) = network.getOutput(i);

	for (unsigned i = 0; i < network.outputSize(); i++)
		network.setOutputPartial(i, 0.0);
	for (unsigned r = 0; (int) r < jacobian.rows(); r++)
	{
		network.setOutputPartial(r, 1.0);
		network.calculatePartials();
		unsigned memSize = jacobian.cols() - weightSize;
		for (unsigned m = 0; m < memSize; m++)
			jacobian.all_memory(r, m) = network.getInputPartial(m);
		for (unsigned w = 0; w < weightSize; w++)
			jacobian.all_weights(r, w) = network.getWeightPartial(w);
		network.setOutputPartial(r, 0.0);
	}
}

}
}
}
