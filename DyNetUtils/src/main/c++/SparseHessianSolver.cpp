/*
 * SparseHessianSolver.cpp
 *
 *    Created on: Jan 21, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "SparseHessianSolver.hpp"

#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/Cholesky>
#include <stdexcept>
#include <cstdint>
#include <cmath>
#include <iostream>

using namespace Eigen;
using namespace std;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{

bool checkMatrix(MatrixXd& matrix)
{
	for (unsigned r = 0; r < matrix.rows(); r++)
		for (unsigned c = 0; c < matrix.cols(); c++)
			if (std::isnan((double) matrix(r, c)) || std::isinf((double) matrix(r, c)))
				return false;
	return true;
}
bool checkDiagonalRank(MatrixXd& matrix)
{
	unsigned minRC = std::min(matrix.rows(), matrix.cols());
	for (unsigned r = 0; r < minRC; r++)
		if (matrix(r, r) == 0)
			return false;
	return true;
}
bool checkVector(VectorXd& matrix)
{
	for (unsigned r = 0; r < matrix.rows(); r++)
		if (std::isnan((double) matrix(r)) || std::isinf((double) matrix(r)))
			return false;
	return true;
}
void SparseHessianSolver::unsafeAddStateCovariance(MatrixXd& matrixCovariance, unsigned state)
{
	Hessian.diag[state] += matrixCovariance; // eqivalent to adding a measurement
	// B'GB=matrixCovariance, B'Gy=0. That is, an exact measurement of the state saying we have it correct right now.
	// the hessian is inverted, so large hessians result in small deltas.  matrix covariance = 1/std^2
}
void SparseHessianSolver::unsafeAddParameterCovariance(MatrixXd& matrixCovariance)
{
	Hessian.end += matrixCovariance;
}

void SparseHessianSolver::unsafeAddEstimation(MatrixXd& B, MatrixXd& K, MatrixXd& G, VectorXd& y, unsigned state)
{
	Hessian.diag[state] += B.transpose() * G * B;
	Hessian.subBorder[state] += K.transpose() * G * B;
	Hessian.end += K.transpose() * G * K;
	gradient[state] += B.transpose() * G * y;
	gradient.end += K.transpose() * G * y;
}
void SparseHessianSolver::unsafeStateUpdate(MatrixXd& A, MatrixXd& J, MatrixXd& H, VectorXd& x, unsigned state)
{
	Hessian.diag[state] += A.transpose() * H * A;
	Hessian.diag[state + 1] += H;
	Hessian.subDiag[state] += -H * A;
	Hessian.subBorder[state] += J.transpose() * H * A;
	Hessian.subBorder[state + 1] += -J.transpose() * H;
	Hessian.end += J.transpose() * H * J;
	gradient[state] += A.transpose() * H * x;
	gradient[state + 1] += -H * x;
	gradient.end += J.transpose() * H * x;
	if (DEBUG_SPARSE_HESSIAN_SOLVER)
		cout << "x = \n" << x << endl;
}

void SparseHessianSolver::unsafeCholesky(unsigned state)
{
	LLT<MatrixXd> LofA(remainderDiagonal + Hessian.diag[state]);
	MatrixXd MatrixU = LofA.matrixU();
	ltHessian.diag[state].triangularView<Eigen::Lower>() = MatrixU.transpose();
	TriangularView<MatrixXd, Upper> upperU = MatrixU.triangularView<Eigen::Upper>();
	ltHessian.subDiag[state] = upperU.solve<Eigen::OnTheRight>((const Eigen::MatrixXd &) Hessian.subDiag[state]);
	ltHessian.subBorder[state] = upperU.solve<Eigen::OnTheRight>(
			(const Eigen::MatrixXd &) (Hessian.subBorder[state] + remainderSuperBorder.transpose()));
	if (DEBUG_SPARSE_HESSIAN_SOLVER)
		if (!checkMatrix(ltHessian.diag[state]))
			throw runtime_error("matrix elements are non-finite in ltHessian.diag[state]");
	if (DEBUG_SPARSE_HESSIAN_SOLVER)
		if (!checkMatrix(ltHessian.subDiag[state]))
			throw runtime_error("matrix elements are non-finite in ltHessian.subDiag[state]");
	if (DEBUG_SPARSE_HESSIAN_SOLVER)
		if (!checkMatrix(ltHessian.subBorder[state]))
			throw runtime_error("matrix elements are non-finite in ltHessian.subBorder[state]");
	remainderDiagonal = -ltHessian.subDiag[state] * ltHessian.subDiag[state].transpose();
	remainderSuperBorder = -ltHessian.subDiag[state] * ltHessian.subBorder[state].transpose();
	remainderEnd += -ltHessian.subBorder[state] * ltHessian.subBorder[state].transpose();
}
void SparseHessianSolver::unsafeEndCholesky()
{
	unsigned lastState = dataSize - 1;
	LLT<MatrixXd> LofA(remainderDiagonal + Hessian.diag[lastState]);
	MatrixXd MatrixU = LofA.matrixU();
	ltHessian.diag[lastState].triangularView<Eigen::Lower>() = MatrixU.transpose();
	TriangularView<MatrixXd, Upper> upperU = MatrixU.triangularView<Eigen::Upper>();
	ltHessian.subBorder[lastState] = upperU.solve<Eigen::OnTheRight>(
			(const Eigen::MatrixXd &) (Hessian.subBorder[lastState] + remainderSuperBorder.transpose()));

	remainderEnd += -ltHessian.subBorder[lastState] * ltHessian.subBorder[lastState].transpose();

	MatrixXd Li = LLT<MatrixXd>(remainderEnd + Hessian.end).matrixL();
	ltHessian.end.triangularView<Eigen::Lower>() = Li;
}
void SparseHessianSolver::unsafeForwardSolve(unsigned state)
{
	TriangularView<MatrixXd, Lower> lowerDiag = ltHessian.diag[state].triangularView<Lower>();
	forwardSolution[state] += gradient[state];
	lowerDiag.solveInPlace(forwardSolution[state]);
	forwardSolution[state + 1] -= ltHessian.subDiag[state] * forwardSolution[state];
	forwardSolution.end -= ltHessian.subBorder[state] * forwardSolution[state];
	if (DEBUG_SPARSE_HESSIAN_SOLVER)
		cout << "ltHessian.end = \n" << ltHessian.end << endl;
}

void SparseHessianSolver::unsafeEndForwardSolve()
{
	unsigned lastState = dataSize - 1;
	TriangularView<MatrixXd, Lower> lowerDiag = ltHessian.diag[lastState].triangularView<Lower>();
	forwardSolution[lastState] += gradient[lastState];
	lowerDiag.solveInPlace(forwardSolution[lastState]);
	forwardSolution.end -= ltHessian.subBorder[lastState] * forwardSolution[lastState];
	forwardSolution.end += gradient.end;
	if (DEBUG_SPARSE_HESSIAN_SOLVER)
	{
		cout << "ltHessian.end = \n" << ltHessian.end << endl;
		if (!checkVector(forwardSolution.end))
			throw runtime_error("unsafeEndForwardSolve: non-finite in forwardSolution.end");
		if (!checkDiagonalRank(ltHessian.end))
			throw runtime_error("unsafeEndForwardSolve: singular ltHessian.end");
	}
	TriangularView<MatrixXd, Lower> lowerEnd = ltHessian.end.triangularView<Lower>();
	lowerEnd.solveInPlace(forwardSolution.end);
}

void SparseHessianSolver::unsafeFinish()
{
	if (DEBUG_SPARSE_HESSIAN_SOLVER)
	{
		if (!checkVector(forwardSolution.end))
			throw runtime_error("unsafeFinish: non-finite forwardSolution.end");
		if (!checkDiagonalRank(ltHessian.end))
			throw runtime_error("unsafeFinish: singular ltHessian.end");
	}
	backSolution.end = ltHessian.end.triangularView<Lower>().transpose().solve(forwardSolution.end);
	this->solution.block(stateSize * dataSize, 0, parameterSize, 1) = backSolution.end;
	unsigned lastState = dataSize - 1;
	backSolution[lastState] = forwardSolution[lastState];
	backSolution[lastState] -= ltHessian.subBorder[lastState].transpose() * backSolution.end;
	if (DEBUG_SPARSE_HESSIAN_SOLVER)
	{
		cout << "unsafeFinish iteration " << lastState << endl;
		if (!checkDiagonalRank(ltHessian.diag[lastState]))
			throw runtime_error("matrix is singular: ltHessian.diag[state]");
		if (!checkVector(backSolution[lastState]))
			throw runtime_error("matrix elements are non-finite in backSolution[state]");
	}
	ltHessian.diag[lastState].transpose().triangularView<Upper>().solveInPlace(backSolution[lastState]);
	this->solution.block(stateSize * lastState, 0, stateSize, 1) = backSolution[lastState];
	for (unsigned state = lastState - 1; state + 1 > 0; state--)
	{
		backSolution[state] = forwardSolution[state];
		backSolution[state] -= ltHessian.subBorder[state].transpose() * backSolution.end;
		backSolution[state] -= ltHessian.subDiag[state].transpose() * backSolution[state + 1];
		if (DEBUG_SPARSE_HESSIAN_SOLVER)
		{
			cout << "unsafeFinish iteration " << state << endl;
			if (!checkDiagonalRank(ltHessian.diag[state]))
				throw runtime_error("matrix is singular: ltHessian.diag[state]");
			if (!checkVector(backSolution[state]))
				throw runtime_error("matrix elements are non-finite in backSolution[state]");
		}
		ltHessian.diag[state].transpose().triangularView<Upper>().solveInPlace(backSolution[state]);
		this->solution.block(stateSize * state, 0, stateSize, 1) = backSolution[state];
	}
}

}
}
}
