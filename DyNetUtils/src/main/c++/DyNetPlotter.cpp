/*
 * DyNetPlotter.cpp
 *
 *    Created on: Feb 10, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include "DyNetPlotter.hpp"
#include "DyNetPoints.hpp"
#include <DyNetOutputs.hpp>
#include <iostream>
#include <stdexcept>
#include <Eigen/Dense>
#include <yaml-cpp/yaml.h>
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
DyNetPlotter::DyNetPlotter(DyNetSizeManager& sizes, string fileName) :
			sizes(sizes),
			inputNames(),
			measurementNames(),
			parameterNames(),
			stateNames(),
			inputSize(sizes.inputSize),
			measurementSize(sizes.outputSize),
			parameterSize(sizes.weightSize),
			stateSize(sizes.memorySize),
			epochNumber(0),
			dataSize(sizes.dataSize),
			trainingData(),
			epochData(),
			tempStatePrediction(sizes.dataSize,vector<double>(sizes.memorySize)),
			tempMeasurementPrediction(sizes.dataSize,vector<double>(sizes.outputSize))

{
}
void DyNetPlotter::setInputNames(vector<string>& inputNames)
{
	if (inputNames.size() != inputSize)
		throw runtime_error("input names are the wrong size");
	this->inputNames = inputNames;
}
void DyNetPlotter::setMeasurementNames(vector<string>& measurementNames)
{
	if (measurementNames.size() != measurementSize)
		throw runtime_error("measurement names are the wrong size");
	this->measurementNames = measurementNames;
}
void DyNetPlotter::setParameterNames(vector<string>& parameterNames)
{
	if (parameterNames.size() != parameterSize)
		throw runtime_error("parameter names are the wrong size");
	this->parameterNames = parameterNames;
}
void DyNetPlotter::setStateNames(vector<string>& stateNames)
{
	if (stateNames.size() != stateSize)
		throw runtime_error("state names are the wrong size");
	this->stateNames = stateNames;
}
void DyNetPlotter::setData(DataProvider& dataSet)
{
	this->trainingData.inputs.reserve(dataSet.viewSize());
	this->trainingData.measurements.reserve(dataSet.viewSize());
	for (unsigned i = 0; i < dataSize; i++)
	{
		vector<double> newinputs;
		vector<double> newoutputs;
		newinputs.resize(inputSize);
		newoutputs.resize(measurementSize);
		dataSet.packInputs(i, newinputs);
		dataSet.packOutputs(i, newoutputs);
		trainingData.inputs.push_back(newinputs);
		trainingData.measurements.push_back(newoutputs);
	}
}
void operator<<(vector<vector<double> >& vector, DyNetStates& states)
{
	for (unsigned i = 0; i < vector.size(); i++)
		for (unsigned j = 0; j < vector[0].size(); j++)
			vector[i][j] = states.getState(i)[j];
}
void operator<<(vector<double>& stdVector, Block<VectorXd>& eigenVector)
{
	for (unsigned i = 0; i < stdVector.size(); i++)
		stdVector[i] = eigenVector[i];
}
void DyNetPlotter::logOutput(unsigned dataIndex, DyNetOutputs& outputs)
{
	tempStatePrediction[dataIndex] << outputs.mem;
	tempMeasurementPrediction[dataIndex] << outputs.outputs;
}
void DyNetPlotter::logEpoch(DyNetPoint& point, DyNetVectorDelta& delta, double trustParameter)
{
	this->epochNumber++;
	EpochData epochDatum(sizes, point, delta, tempStatePrediction, tempMeasurementPrediction, trustParameter);
	this->epochData.push_back(epochDatum);
}

DyNetPlotter::EpochData::EpochData(DyNetSizeManager& sizes, DyNetPoint& point, DyNetVectorDelta& delta,
									vector<vector<double> >& statePrediction,
									vector<vector<double> > &measurementPrediction, double trustParameter) :
			error(point.error),
			statesUsed(sizes.dataSize, vector<double>(sizes.memorySize)),
			parametersUsed(sizes.weightSize),
			statePrediction(sizes.dataSize, vector<double>(sizes.memorySize)),
			measurementPrediction(sizes.dataSize, vector<double>(sizes.outputSize)),
			updatePrediction(sizes, delta, trustParameter)
{
	statesUsed << point.dyNetStates;
	parametersUsed = point.weights;
	this->measurementPrediction = measurementPrediction;
	this->statePrediction = statePrediction;

}
DyNetPlotter::EpochUpdatePrediction::EpochUpdatePrediction(DyNetSizeManager& sizes, DyNetVectorDelta& delta,
															double trustParameter) :
			parameterDelta(sizes.weightSize),
			statesDelta(sizes.dataSize, vector<double>(sizes.memorySize)),
			predictedErrorChange(delta.predictedChangeInError),
			trustParameter(trustParameter)
{
	unsigned j = 0;
	for (unsigned i = 0; i < sizes.dataSize; i++)
		for (unsigned k = 0; k < sizes.memorySize; k++)
			statesDelta[i][k] = delta.delta[j++];
	for (unsigned i = 0; i < sizes.weightSize; i++)
		parameterDelta[i] = delta.delta[j++];
}
void displayDoubleArray(YAML::Emitter& emitter, vector<vector<double> >& vec)
{
	emitter << YAML::BeginSeq;
	for (unsigned i = 0; i < vec.size(); i++)
		emitter << YAML::Flow << vec[i];
	emitter << YAML::EndSeq;
}

void DyNetPlotter::renderYAML(ostream& ostream)
{
	//TODO: determine why YAML::Emitter destruction causes crash.
	YAML::Emitter emitter;
	emitter << YAML::BeginMap;
	emitter << YAML::Key << "DyNetTrainer DataDump";
	emitter << YAML::Value;
	emitter << YAML::BeginMap;
	emitter << YAML::Key << "inputs";
	emitter << YAML::Value << YAML::Flow << inputNames;
	emitter << YAML::Key << "measurements";
	emitter << YAML::Value << YAML::Flow << measurementNames;
	emitter << YAML::Key << "states";
	emitter << YAML::Value << YAML::Flow << stateNames;
	emitter << YAML::Key << "parameters";
	emitter << YAML::Value << YAML::Flow << parameterNames;
	emitter << YAML::Key << "epochNumber";
	emitter << YAML::Value << epochNumber;
	emitter << YAML::Key << "dataSize";
	emitter << YAML::Value << dataSize;

	emitter << YAML::Key << "trainingData";
	emitter << YAML::Value;
	emitter << YAML::BeginMap;
	{
		emitter << YAML::Key << "inputs";
		emitter << YAML::Value;
		displayDoubleArray(emitter, trainingData.inputs);
		emitter << YAML::Key << "measurements";
		emitter << YAML::Value;
		displayDoubleArray(emitter, trainingData.measurements);
	}
	emitter << YAML::EndMap;

	emitter << YAML::Key << "epochData";
	emitter << YAML::Value << YAML::BeginSeq;
	for (unsigned i = 0; i < this->epochData.size(); i++)
	{
		emitter << YAML::BeginMap;
		EpochData& dat = epochData[i];
		emitter << YAML::Key << "error";
		emitter << YAML::Value << dat.error;
		emitter << YAML::Key << "states used";
		emitter << YAML::Value;
		displayDoubleArray(emitter, dat.statesUsed);
		emitter << YAML::Key << "parameters used";
		emitter << YAML::Value << YAML::Flow << dat.parametersUsed;
		emitter << YAML::Key << "state prediction";
		emitter << YAML::Value;
		displayDoubleArray(emitter, dat.statePrediction);
		emitter << YAML::Key << "measurement prediction";
		emitter << YAML::Value;
		displayDoubleArray(emitter, dat.measurementPrediction);
		emitter << YAML::Key << "update";
		emitter << YAML::Value;
		{
			emitter << YAML::BeginMap;
			emitter << YAML::Key << "parameter delta";
			emitter << YAML::Value << YAML::Flow << dat.updatePrediction.parameterDelta;
			emitter << YAML::Key << "states delta";
			emitter << YAML::Value;
			displayDoubleArray(emitter, dat.updatePrediction.statesDelta);
			emitter << YAML::Key << "change in error";
			emitter << YAML::Value << dat.updatePrediction.predictedErrorChange;
			emitter << YAML::Key << "trust parameter";
			emitter << YAML::Value << dat.updatePrediction.trustParameter;
			emitter << YAML::EndMap;
		}

		emitter << YAML::EndMap;
	}
	emitter << YAML::EndSeq;

	emitter << YAML::EndMap;
	emitter << YAML::EndMap;
//	string value=emitter.c_str();
	std::string lastError = emitter.GetLastError();
	string value = lastError;
	cout << value << endl;
	std::string output = emitter.c_str();
	ostream << output;
}

void DyNetPlotter::render1EpochYAML(ostream& ostream, unsigned int epoch)
{
	//TODO: determine why YAML::Emitter destruction causes crash.
	YAML::Emitter emitter;
	emitter << YAML::BeginMap;
	emitter << YAML::Key << "DyNetTrainer DataDump";
	emitter << YAML::Value;
	emitter << YAML::BeginMap;
	emitter << YAML::Key << "inputs";
	emitter << YAML::Value << YAML::Flow << inputNames;
	emitter << YAML::Key << "measurements";
	emitter << YAML::Value << YAML::Flow << measurementNames;
	emitter << YAML::Key << "states";
	emitter << YAML::Value << YAML::Flow << stateNames;
	emitter << YAML::Key << "parameters";
	emitter << YAML::Value << YAML::Flow << parameterNames;
	emitter << YAML::Key << "epochNumber";
	emitter << YAML::Value << epochNumber;
	emitter << YAML::Key << "dataSize";
	emitter << YAML::Value << dataSize;

	emitter << YAML::Key << "trainingData";
	emitter << YAML::Value;
	emitter << YAML::BeginMap;
	{
		emitter << YAML::Key << "inputs";
		emitter << YAML::Value;
		displayDoubleArray(emitter, trainingData.inputs);
		emitter << YAML::Key << "measurements";
		emitter << YAML::Value;
		displayDoubleArray(emitter, trainingData.measurements);
	}
	emitter << YAML::EndMap;

	emitter << YAML::Key << "epochData";
	emitter << YAML::Value << YAML::BeginSeq;
	{
		unsigned i=epoch;
		emitter << YAML::BeginMap;
		EpochData& dat = epochData[i];
		emitter << YAML::Key << "error";
		emitter << YAML::Value << dat.error;
		emitter << YAML::Key << "states used";
		emitter << YAML::Value;
		displayDoubleArray(emitter, dat.statesUsed);
		emitter << YAML::Key << "parameters used";
		emitter << YAML::Value << YAML::Flow << dat.parametersUsed;
		emitter << YAML::Key << "state prediction";
		emitter << YAML::Value;
		displayDoubleArray(emitter, dat.statePrediction);
		emitter << YAML::Key << "measurement prediction";
		emitter << YAML::Value;
		displayDoubleArray(emitter, dat.measurementPrediction);
		emitter << YAML::Key << "update";
		emitter << YAML::Value;
		{
			emitter << YAML::BeginMap;
			emitter << YAML::Key << "parameter delta";
			emitter << YAML::Value << YAML::Flow << dat.updatePrediction.parameterDelta;
			emitter << YAML::Key << "states delta";
			emitter << YAML::Value;
			displayDoubleArray(emitter, dat.updatePrediction.statesDelta);
			emitter << YAML::Key << "change in error";
			emitter << YAML::Value << dat.updatePrediction.predictedErrorChange;
			emitter << YAML::Key << "trust parameter";
			emitter << YAML::Value << dat.updatePrediction.trustParameter;
			emitter << YAML::EndMap;
		}

		emitter << YAML::EndMap;
	}
	emitter << YAML::EndSeq;

	emitter << YAML::EndMap;
	emitter << YAML::EndMap;
//	string value=emitter.c_str();
	std::string lastError = emitter.GetLastError();
	string value = lastError;
	cout << value << endl;
	std::string output = emitter.c_str();
	ostream << output;
}

using namespace YAML;

}
}
}

#include <yaml-cpp/yaml.h>
using namespace YAML;
using namespace std;
void createPopulateAndDestroyYamlEmitter()
{
	Emitter out;
	out << BeginSeq;
	out << "fuzzy";
	out << "bunnies";
	out << EndSeq;
	cout << out.c_str() << endl;
}
void uta::networkUtils::dyNetUtils::populateEmitter()
{
	cout << "This function creates and destroys a yaml emitter" << endl;
	createPopulateAndDestroyYamlEmitter();
	cout << "emitter safely deleted" << endl;
}
void uta::networkUtils::dyNetUtils::altRenderYAML(void)
{
	cout << "altRenderYAML" << endl;
	// fun fact, this will still compile without including the yaml-cpp include folder,
	// but the Emittor destructor will mysteriously cause segfaults!
	Emitter emitter;
	emitter << BeginDoc;
	emitter << BeginSeq;
	cout << "hey" << endl;
	emitter << "eggs";
	cout << "hey" << endl;
	emitter << "bread";
	emitter << "milk";

	emitter << EndSeq;
	emitter << EndDoc;
//	string value=emitter.c_str();
	std::string output = emitter.c_str();
	std::string lastError = emitter.GetLastError();
	string value = lastError;
	cout << output << value << endl;
}

