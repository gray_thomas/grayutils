/*
 * FloatingMemoryRegressorCalculator.cpp
 *
 *  Created on: Jan 5, 2014
 *      Author: Gray
 */

#include "StateTrainingMatrixBuilder.hpp"
#include "DyNetSizeManager.hpp"

using namespace Eigen;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{

StateTrainingMatrixBuilder::StateTrainingMatrixBuilder(DataProvider& trainingDataSet, Trainable& network) :
			sizes(trainingDataSet.viewSize(), trainingDataSet.viewInputSize(), trainingDataSet.viewOutputSize(),
					network.inputSize(), network.outputSize(), network.weightSize())
					,
			regressorMatrix(sizes),
			jacobian(sizes),
			regressionErrorVector(sizes),
			dataSet(trainingDataSet),
			network(network),
			error(0.0),
			memories(sizes),
			networkInputs(sizes),
			networkOutputs(sizes),
			dyNetJacobianCalculator(network),
			solver(sizes.memorySize, sizes.weightSize, sizes.dataSize),
			solution()
{
}

void StateTrainingMatrixBuilder::setupSizes(void)
{
	sizes.inputSize = dataSet.viewInputSize();
	sizes.outputSize = dataSet.viewOutputSize();
	sizes.dataSize = dataSet.viewSize();
	if (network.inputSize() < dataSet.viewInputSize())
		cerr << "Network does not fit dataSet!" << endl;
	sizes.memorySize = network.inputSize() - dataSet.viewInputSize();
	if (network.outputSize() < sizes.memorySize + dataSet.viewOutputSize())
		cerr << "Network does not have enough outputs!" << endl;
	sizes.unjudgedSize = network.outputSize() - dataSet.viewOutputSize() - sizes.memorySize;
	sizes.weightSize = network.weightSize();
}

void StateTrainingMatrixBuilder::calculate(unsigned epochNumber)
{
	error = 0;
	solver.reset();
	for (unsigned dataIndex = 0; dataIndex < dataSet.viewSize(); dataIndex++)
	{
		networkInputs.mem = memories.getState(dataIndex);
		networkInputs.inputs = getInputs(dataIndex);
		dyNetJacobianCalculator.calculateDyNetJacobian(networkInputs, networkOutputs, jacobian);

		regressionErrorVector.outputError(dataIndex) = getOutputs(dataIndex) - networkOutputs.outputs;
		regressorMatrix.outputComparisonMemoryComponent(dataIndex) = jacobian.outputs_memory;
		regressorMatrix.outputComparisonWeightComponent(dataIndex) = jacobian.outputs_weights;
		MatrixXd B = MatrixXd(jacobian.outputs_memory);
		MatrixXd K = MatrixXd(jacobian.outputs_weights);
		MatrixXd G = MatrixXd::Identity(sizes.outputSize, sizes.outputSize);
		VectorXd y = VectorXd(getOutputs(dataIndex) - networkOutputs.outputs);
		solver.addEstimation(B, K, G, y);
//				jacobian.outputs_memory, jacobian.outputs_weights, MatrixXd::Identity(sizes.outputSize,sizes.outputSize)
//				,VectorXd(regressionErrorVector.outputError(dataIndex)));
		if (dataIndex + 1 < sizes.dataSize)
		{
			VectorXd error = memories.getState(dataIndex + 1) - networkOutputs.mem;
			regressionErrorVector.memoryError(dataIndex) = error;
			regressorMatrix.memoryComparisonWeightComponent(dataIndex) = jacobian.memory_weights;
			regressorMatrix.memoryComparisonFirstMemoryComponent(dataIndex) = jacobian.memory_memory;
			regressorMatrix.memoryComparisonSecondMemoryComponent(dataIndex) = -MatrixXd::Identity(sizes.memorySize,
					sizes.memorySize);
			MatrixXd A = MatrixXd(jacobian.memory_memory);
			MatrixXd J = MatrixXd(jacobian.memory_weights);
			MatrixXd H = MatrixXd::Identity(sizes.memorySize, sizes.memorySize);
			solver.addStateUpdate(A, J, H, error);
			this->error += error.squaredNorm();
		}

	}
	solution = solver.finishAndGetSolution();
	// plotter.wrapUpAndSaveFile(epochNumber); // TODO: replace with more generic plotter. eg:
	// indexOfOutputICompairsonPlot=plotter.addTimeSeriesComparisonPlot("output i");
	// unsigned indexOfErrorPlot=plotter.addLonePlot("error");
	// ideally, this would be auto-parsed by python plot-maker.
}
VectorXd const & StateTrainingMatrixBuilder::getSolution(void) const
{
	return solution;
}
VectorXd StateTrainingMatrixBuilder::getInputs(unsigned dataIndex)
{
	VectorXd inputs(sizes.inputSize);
	for (unsigned i = 0; i < sizes.inputSize; i++)
		inputs(i) = dataSet.getInput(dataIndex, i);
	return inputs;
}
VectorXd StateTrainingMatrixBuilder::getOutputs(unsigned dataIndex)
{
	VectorXd outputs(sizes.outputSize);
	for (unsigned i = 0; i < sizes.outputSize; i++)
		outputs(i) = dataSet.getOutput(dataIndex, i);
	return outputs;
}
}
}
}
