/*
 * DyNetSizeManager.cpp
 *
 *    Created on: Feb 19, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include <DyNetSizeManager.hpp>
#include <stdexcept>
using namespace std;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{

unsigned DyNetSizeManager::totalOutputSize()
{
	return memorySize + outputSize + unjudgedSize;
}
unsigned DyNetSizeManager::totalInputSize()
{
	return memorySize + inputSize;
}
DyNetSizeManager::DyNetSizeManager() :
			memorySize(0),
			weightSize(0),
			outputSize(0),
			inputSize(0),
			unjudgedSize(),
			dataSize()
{

}
DyNetSizeManager::DyNetSizeManager(unsigned dataSize, unsigned dataInputs, unsigned dataOutputs,
									unsigned networkInputs, unsigned networkOutputs, unsigned networkWeights) :
			DyNetSizeManager()
{
	this->inputSize = dataInputs;
	this->outputSize = dataOutputs;
	this->dataSize = dataSize;
	if (networkInputs < inputSize)
		throw runtime_error("Network does not fit dataSet!");
	this->memorySize = networkInputs - dataInputs;
	if (networkOutputs < memorySize + outputSize)
		throw runtime_error("Network does not have enough outputs!");
	this->unjudgedSize = networkOutputs - dataOutputs - memorySize;
	this->weightSize = networkWeights;
}

}
}
}

