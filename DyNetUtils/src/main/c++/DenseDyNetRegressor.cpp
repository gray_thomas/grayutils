/*
 * DenseDyNetRegressor.cpp
 *
 *    Created on: Feb 10, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "DenseDyNetRegressor.hpp"
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
Block<MatrixXd> DenseDyNetRegressor::memoryComparisonWeightComponent(unsigned dataIndex)
{
	if (dataIndex >= dataSize - 1)
		throw runtime_error("out of range");
	return this->block(dataIndex * memorySize, 0, memorySize, weightSize);
}
Block<MatrixXd> DenseDyNetRegressor::memoryComparisonFirstMemoryComponent(unsigned dataIndex)
{
	if (dataIndex >= dataSize - 1)
		throw runtime_error("out of range");
	return this->block(dataIndex * memorySize, weightSize + dataIndex * memorySize, memorySize, memorySize);
}
Block<MatrixXd> DenseDyNetRegressor::memoryComparisonSecondMemoryComponent(unsigned dataIndex)
{
	if (dataIndex >= dataSize - 1)
		throw runtime_error("out of range");
	return this->block(dataIndex * memorySize, weightSize + (dataIndex + 1) * memorySize, memorySize, memorySize);
}
Block<MatrixXd> DenseDyNetRegressor::outputComparisonWeightComponent(unsigned dataIndex)
{
	return this->block((dataSize - 1) * memorySize + outputSize * dataIndex, 0, outputSize, weightSize);
}
Block<MatrixXd> DenseDyNetRegressor::outputComparisonMemoryComponent(unsigned dataIndex)
{
	return this->block((dataSize - 1) * memorySize + outputSize * dataIndex, weightSize + dataIndex * memorySize,
			outputSize, memorySize);
}
DenseDyNetRegressor::DenseDyNetRegressor(DyNetSizeManager& sizes) :
			MatrixXd(),
			memorySize(sizes.memorySize),
			outputSize(sizes.outputSize),
			inputSize(sizes.inputSize),
			weightSize(sizes.weightSize),
			dataSize(sizes.dataSize)
{
	unsigned equationNumber = (dataSize - 1) * memorySize + dataSize * outputSize;
	unsigned deltaSize = weightSize + dataSize * memorySize;
	this->setZero(equationNumber, deltaSize);
}
}
}
}

