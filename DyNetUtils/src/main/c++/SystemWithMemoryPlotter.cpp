/*
 * SystemWithMemoryPlotter.cpp
 *
 *    Created on: Dec 14, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include "SystemWithMemoryPlotter.hpp"

#include <vector>
#include <string>
#include <iostream>
#include "DataSet.hpp"
#include "DyNetSizeManager.hpp"
using namespace std;
namespace uta
{
namespace networkUtils
{
SystemWithMemoryPlotter::SystemWithMemoryPlotter(DyNetSizeManager& sizes) :
			dataRecording(false),
			dataRecordingThisEpoch(false),
			outputData(NULL),
			plotEveryXEpochsCounter(0),
			plotEveryXEpochs(0),
			sizes(sizes),
			ins(NULL),
			outs(NULL)
{
}
void SystemWithMemoryPlotter::setupRecording(string dataSetFileName, unsigned plotEveryXEpochs)
{
	dataRecording = true;
	dataRecordingThisEpoch = true;
	this->plotEveryXEpochs = plotEveryXEpochs;
	this->plotEveryXEpochsCounter = plotEveryXEpochs;
	this->dataSetFileNameBase = dataSetFileName;
}
void SystemWithMemoryPlotter::prepareEpoch()
{
	if (dataRecording)
	{
		if (++plotEveryXEpochsCounter >= plotEveryXEpochs)
		{
			plotEveryXEpochsCounter = 0;
			dataRecordingThisEpoch = true;
			outputData = new DataSet(sizes.inputSize,
					sizes.memorySize + sizes.outputSize * 2 + sizes.unjudgedSize + 2);
			ins = new double[sizes.inputSize];
			outs = new double[sizes.memorySize + sizes.outputSize * 2 + sizes.unjudgedSize + 2];
		}
		else
		{
			dataRecordingThisEpoch = false;
		}
	}
}

void SystemWithMemoryPlotter::setInput(unsigned index, double value)
{
	if (dataRecording)
	{
		ins[index] = value;
	}
}
void SystemWithMemoryPlotter::setOutputMemory(unsigned index, double value)
{
	if (dataRecording)
	{
		outs[index] = value;
	}
}
void SystemWithMemoryPlotter::setOutputs(unsigned index, double value)
{
	if (dataRecording)
	{
		outs[sizes.memorySize + index] = value;
	}
}
void SystemWithMemoryPlotter::setDesiredOutputs(unsigned index, double value)
{
	if (dataRecording)
	{
		outs[sizes.memorySize + sizes.outputSize + index] = value;
	}
}
void SystemWithMemoryPlotter::setUnJudgedOutputs(unsigned index, double value)
{
	if (dataRecording)
	{
		outs[sizes.memorySize + sizes.outputSize + sizes.outputSize + index] = value;
	}
}
void SystemWithMemoryPlotter::setPointError(double value)
{
	if (dataRecording)
	{
		outs[sizes.memorySize + sizes.outputSize * 2 + sizes.unjudgedSize] = value;
	}
}
void SystemWithMemoryPlotter::setAccumulatedError(double value)
{
	if (dataRecording)
	{
		outs[sizes.memorySize + sizes.outputSize * 2 + sizes.unjudgedSize + 1] = value;
	}
}

void SystemWithMemoryPlotter::recordData(void)
{
	if (dataRecordingThisEpoch)
	{
		outputData->addLine(ins, outs);
	}
}

void SystemWithMemoryPlotter::wrapUpAndSaveFile(unsigned epochNumber)
{
	if (dataRecordingThisEpoch)
	{
		delete[] ins;
		ins = NULL;
		delete[] outs;
		outs = NULL;
		dataRecordingThisEpoch = false;
		stringstream ss;
		ss << dataSetFileNameBase << epochNumber << ".ioDat";
		outputData->pushToFile(ss.str());
		delete outputData;
		outputData = NULL;
	}
}
}
}
