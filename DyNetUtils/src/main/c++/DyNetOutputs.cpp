/*
 * DyNetOutputs.cpp
 *
 *    Created on: Feb 11, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include "DyNetOutputs.hpp"

namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
DyNetOutputs::DyNetOutputs(DyNetSizeManager& sizes) :
			VectorXd(sizes.memorySize + sizes.outputSize + sizes.unjudgedSize),
			mem(this->block(0, 0, sizes.memorySize, 1)),
			outputs(this->block(sizes.memorySize, 0, sizes.outputSize, 1)),
			unjudgedOutputs(this->block(sizes.memorySize + sizes.outputSize, 0, sizes.unjudgedSize, 1))
{
	this->setZero();
}
}
}
}
