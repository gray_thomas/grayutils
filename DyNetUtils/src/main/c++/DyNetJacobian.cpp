/*
 * DyNetJacobian.cpp
 *
 *    Created on: Feb 11, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "DyNetJacobian.hpp"

namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
DyNetJacobian::DyNetJacobian(DyNetSizeManager& sizes) :
			MatrixXd(sizes.memorySize + sizes.outputSize, sizes.memorySize + sizes.weightSize),
			outputs_memory(this->block(sizes.memorySize, 0, sizes.outputSize, sizes.memorySize)),
			outputs_weights(this->block(sizes.memorySize, sizes.memorySize, sizes.outputSize, sizes.weightSize)),
			memory_memory(this->block(0, 0, sizes.memorySize, sizes.memorySize)),
			memory_weights(this->block(0, sizes.memorySize, sizes.memorySize, sizes.weightSize)),
			all_memory(this->block(0, 0, sizes.memorySize + sizes.outputSize, sizes.memorySize)),
			all_weights(this->block(0, sizes.memorySize, sizes.memorySize + sizes.outputSize, sizes.weightSize))

{
	this->setZero();
}
}
}
}
