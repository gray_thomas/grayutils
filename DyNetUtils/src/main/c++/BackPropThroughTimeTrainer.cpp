/*
 * BackPropThroughTimeTrainer.cpp
 *
 *    Created on: Dec 3, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "BackPropThroughTimeTrainer.hpp"

#include <iostream>
#include <string>
#include <vector>

#include "GradientDescentTrainer.hpp"
#include "DataSet.hpp"
#include "Network.hpp"
#include "PerformanceTimer.hpp"
#include <Eigen/Core>
#include <Eigen/Dense>
using namespace uta::timingUtils;
using namespace std;
using namespace Eigen;
#define NO_TIME_LIMIT -1.0
#define NO_EPOCH_LIMIT 0
namespace uta
{
namespace networkUtils
{

BackPropThroughTimeTrainer::BackPropThroughTimeTrainer(unsigned memSize, string logFileName) :
			BackPropThroughTimeTrainer(memSize)
{
	logFile.open(logFileName.c_str());
}
BackPropThroughTimeTrainer::BackPropThroughTimeTrainer(unsigned memSize) :

			GradientDescentTrainer(),
			memory(),
			initialMemory(),
			eliteInitialMemory(),
			pointwisePartials(),
			backPropThroughTimeMemory(),
			sizes(),
			plotter(sizes)

{
	sizes.memorySize = memSize;
	memory.resize(memSize);
	initialMemory.resize(memSize);
	eliteInitialMemory.resize(memSize);
}
void BackPropThroughTimeTrainer::setInitialMemory(vector<double> mem)
{
	for (unsigned m = 0; m < sizes.memorySize; m++)
		initialMemory[m] = mem[m];
}
void BackPropThroughTimeTrainer::setupDataRecording(string dataSetFileName, unsigned plotEveryXEpochs)
{
	plotter.setupRecording(dataSetFileName, plotEveryXEpochs);
}

void BackPropThroughTimeTrainer::setupForEpoch(DataProvider &trainingDataSet, Trainable& network)
{
//	GradientDescentTrainer::setupForEpoch(trainingDataSet, network);
	sizes.inputSize = trainingDataSet.viewInputSize();
	sizes.weightSize = network.weightSize();
	sizes.outputSize = trainingDataSet.viewOutputSize();
	plotter.prepareEpoch();
	pointwisePartials.resize(sizes.weightSize, sizes.memorySize);
	backPropThroughTimeMemory.resize(sizes.weightSize, sizes.memorySize);

	if (sizes.totalInputSize() != network.inputSize())
		cerr << "input sizes do not match!" << endl;
	if (sizes.totalOutputSize() != network.outputSize())
		cerr << "output sizes do not match!" << endl;
	for (unsigned m = 0; m < sizes.memorySize; m++)
		memory[m] = initialMemory[m];
}

void BackPropThroughTimeTrainer::setNetworkInput(Trainable& network)
{
	for (unsigned m = 0; m < sizes.memorySize; m++)
		network.setInput(m, memory[m]);
	for (unsigned i = 0; i < sizes.inputSize; i++)
		network.setInput(sizes.memorySize + i, inputs[i]);
}

void BackPropThroughTimeTrainer::fetchNetworkOutputs(Trainable& network)
{
	for (unsigned m = 0; m < sizes.memorySize; m++)
		memory[m] = network.getOutput(m);
	for (unsigned o = 0; o < sizes.outputSize; o++)
		outputs[o] = network.getOutput(sizes.memorySize + o);
}

void BackPropThroughTimeTrainer::calculateGradientOfError(Trainable& network)
{
	for (unsigned m = 0; m < sizes.memorySize; m++)
		network.setOutputPartial(m, 0.0);
	for (unsigned o = 0; o < sizes.outputSize; o++)
	{
		double e = outputs[o] - desiredOutputs[o];
		network.setOutputPartial(sizes.memorySize + o, 2.0 * e);
	}
	network.calculatePartials();
	for (unsigned m = 0; m < sizes.memorySize; m++)
		pointwisePartials.pointError_memoryPosterior(0, m) = network.getInputPartial(m);
	for (unsigned w = 0; w < sizes.weightSize; w++)
		pointwisePartials.pointError_weights(0, w) = network.getWeightPartial(w);
}

void BackPropThroughTimeTrainer::calculateJacobianOfMemory(Trainable& network)
{
	for (unsigned z = 0; z < sizes.totalOutputSize(); z++)
		network.setOutputPartial(z, 0.0);
	for (unsigned m = 0; m < sizes.memorySize; m++)
	{
		if (m > 0)
			network.setOutputPartial(m - 1, 0.0);

		network.setOutputPartial(m, 1.0);
		network.calculatePartials();
		for (unsigned mp = 0; mp < sizes.memorySize; mp++)
			pointwisePartials.memory_memoryPosterior(m, mp) = network.getInputPartial(mp);
		for (unsigned w = 0; w < sizes.weightSize; w++)
			pointwisePartials.pointError_weights(m, w) = network.getWeightPartial(w);
	}
}
void BackPropThroughTimeTrainer::updateMemoryFromPoint()
{
	backPropThroughTimeMemory.errorPosterior_weights += pointwisePartials.pointError_weights
			+ pointwisePartials.pointError_memoryPosterior * backPropThroughTimeMemory.memoryPosterior_weights;
	backPropThroughTimeMemory.errorPosterior_initialMemory += pointwisePartials.pointError_memoryPosterior
			* backPropThroughTimeMemory.memoryPosterior_initialMemory;
	backPropThroughTimeMemory.memoryPosterior_initialMemory = pointwisePartials.memory_memoryPosterior
			* backPropThroughTimeMemory.memoryPosterior_initialMemory;
	backPropThroughTimeMemory.memoryPosterior_weights = pointwisePartials.memory_weights
			+ pointwisePartials.memory_memoryPosterior * backPropThroughTimeMemory.memoryPosterior_weights;
}

void BackPropThroughTimeTrainer::handleOneDataPoint(unsigned index, DataProvider &trainingDataSet, Trainable& network)
{
	trainingDataSet.packInputs(index, inputs);
	trainingDataSet.packOutputs(index, desiredOutputs);

	setNetworkInput(network);
	network.calculateOutputs();
	fetchNetworkOutputs(network);

	calculateGradientOfError(network);

	calculateJacobianOfMemory(network);

	updateMemoryFromPoint();
}
double BackPropThroughTimeTrainer::calculateOneDatumError(Trainable& network)
{
	double error = 0;
	for (unsigned o = 0; o < sizes.outputSize; o++)
	{
		double e = outputs[o] - desiredOutputs[o];
		error += e * e;
	}
	plotter.recordData();
	return error;
}
double BackPropThroughTimeTrainer::validateWeights(DataProvider &validationDataSet, Trainable& network)
{
	return 0.0;
}

void BackPropThroughTimeTrainer::updateWeights(Trainable& network, double learningRate, double dataSize)
{
	if (trainError < eliteError)
	{
		eliteError = trainError;
		for (unsigned w = 0; w < sizes.weightSize; w++)
			eliteWeights[w] = weights[w];
		for (unsigned m = 0; m < sizes.memorySize; m++)
			eliteInitialMemory[m] = initialMemory[m];
	}
	plotter.wrapUpAndSaveFile(this->trainingEpochs);

	double dotProd = 0;
	for (unsigned w = 0; w < sizes.weightSize; w++)
		dotProd -= (learningRate / dataSize) * backPropThroughTimeMemory.errorPosterior_weights(0, w)
				* backPropThroughTimeMemory.errorPosterior_weights(0, w);
	for (unsigned m = 0; m < sizes.memorySize; m++)
		dotProd -= (learningRate / dataSize) * backPropThroughTimeMemory.errorPosterior_initialMemory(0, m)
				* backPropThroughTimeMemory.errorPosterior_initialMemory(0, m);
	double expectedErrorChangeRatio = dotProd / this->trainError;
	cout << "expected change in error " << (dotProd) << endl;

	for (unsigned w = 0; w < sizes.weightSize; w++)
		weights[w] -= (learningRate / dataSize) * backPropThroughTimeMemory.errorPosterior_weights(0, w);

	for (unsigned m = 0; m < sizes.memorySize; m++)
		initialMemory[m] -= (learningRate / dataSize) * backPropThroughTimeMemory.errorPosterior_initialMemory(0, m);

	network.setWeights(weights);
}

}
}
