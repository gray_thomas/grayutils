/*
 * GradientDescentTrainer.cpp
 *
 *    Created on: Nov 25, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "GradientDescentTrainer.hpp"
#include "GUFiles.hpp"

#include <iostream>
#include <string>
#include <vector>

#include "PerformanceTimer.hpp"
#include "DataSet.hpp"
#include "Network.hpp"
#define NO_TIME_LIMIT -1.0
#define NO_EPOCH_LIMIT 0
using namespace std;
using namespace uta::timingUtils;
namespace uta
{
namespace networkUtils
{

GradientDescentTrainer::GradientDescentTrainer(string logFileName) :
			GradientDescentTrainer()
{
	logFile.open(gu::data_local(logFileName).c_str());
}
GradientDescentTrainer::GradientDescentTrainer() :
			logFile(),
			timeLimitInSeconds(1.0),
			trainingEpochLimit(NO_EPOCH_LIMIT),
			gradCalc(NULL),
			errorCalc(NULL),
			trainingEpochs(0),
			plottingFileBaseName(""),
			plottingFrequency(0),
			plottingEnabled(false),
			timer(),
			trainError(0),
			testError(0),
			eliteError(1e99),
			eliteWeights(),
			weights()
{
}

GradientDescentTrainer::~GradientDescentTrainer()
{
	logFile.close();
}

void GradientDescentTrainer::trainNetwork(DataProvider &trainingDataSet, DataProvider &validationDataSet, Trainable& network,
											double learningRate)
{
	trainingEpochs = 0;
	timer.startTimer();
	trainError = 0;
	eliteError = 1e99;
	eliteWeights = network.getWeights();
	gradCalc = new GradientCalculator(trainingDataSet, network);
	errorCalc = new ErrorCalculator(validationDataSet, network);
	if (this->plottingEnabled)
		gradCalc->setupPlotting(this->plottingFileBaseName, this->plottingFrequency);
	eliteInitialMemory.resize(errorCalc->getSizes().memorySize);
	do
	{
		trainOneEpoch(trainingDataSet, network, learningRate);

		if (logFile.is_open())
		{
			errorCalc->calculate(trainingEpochs);
			testError = errorCalc->getError();
			logFile << trainingEpochs << " " << trainError << " " << testError << endl;
			cout << trainingEpochs << " " << "train E "<< trainError << " test E " << testError;
			cout << " time is "<<timer.getCurrentTime()<<" timelimit is "<<timeLimitInSeconds<<endl;
		}
	} while (doesTimerRemain() && doEpochsRemain());
	delete gradCalc;
	gradCalc = NULL;
	delete errorCalc;
	errorCalc = NULL;

	network.setWeights(eliteWeights);
	cout << "trained for " << trainingEpochs << " epoch." << endl;
}

ostream& operator<<(ostream& cout, vector<double> &arrayList)
{
	cout << "[";
	for (unsigned i = 0; i < arrayList.size(); i++)
	{
		cout << arrayList[i];
		if (i + 1 < arrayList.size())
			cout << ", ";
	}
	cout << "]";
	return cout;
}
void GradientDescentTrainer::setupLogging(string fileName, unsigned logEveryXepochs)
{
	this->plottingEnabled = true;
	this->plottingFileBaseName = fileName;
	this->plottingFrequency = logEveryXepochs;
}

void GradientDescentTrainer::trainOneEpoch(DataProvider &trainingDataSet, Trainable& network, double learningRate)
{
	gradCalc->setInitialMemory(initialMemory);
	gradCalc->calculate(this->trainingEpochs);

	trainError = gradCalc->getError();
	network.packWeights(weights);
//	cout << trainError << " is the training Error this epoch" << endl;
	if (trainError < eliteError)
	{
		eliteError = trainError;
		for (unsigned i = 0; i < weights.size(); i++)
			eliteWeights[i] = weights[i];
		for (unsigned i = 0; i < initialMemory.size(); i++)
			eliteInitialMemory[i] = initialMemory[i];
	}

	for (unsigned i = 0; i < weights.size(); i++)
		weights[i] -= gradCalc->getError_Weights(i) * (learningRate);
	for (unsigned i = 0; i < initialMemory.size(); i++)
		initialMemory[i] -= gradCalc->getError_InitialMemory(i) * (learningRate);

	network.setWeights(weights);
}

void GradientDescentTrainer::setTimeLimit(double timeInSeconds)
{
	this->timeLimitInSeconds = timeInSeconds;
}
void GradientDescentTrainer::setEpochMax(unsigned maxEpoch)
{
	this->trainingEpochLimit = maxEpoch;
}

bool GradientDescentTrainer::doesTimerRemain(void)
{
	if (this->timeLimitInSeconds == NO_TIME_LIMIT)
	{
		std::cout<<"noTimeLimit"<<std::endl;
		return true;
	}
	else if (timer.getCurrentTime() > timeLimitInSeconds)
	{
		return false;
	}
	return true;
}

bool GradientDescentTrainer::doEpochsRemain(void)
{
	trainingEpochs++;
	if (this->trainingEpochLimit == NO_EPOCH_LIMIT)
		return true;
	if (trainingEpochs >= trainingEpochLimit)
		return false;
	return true;
}

}
}
