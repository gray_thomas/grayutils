/*
 * DyNetErrorVector.cpp
 *
 *    Created on: Feb 10, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include "DyNetErrorVector.hpp"
// FloatingMemoryRegressorCalculator::FloatingMemoryRegressionErrorVector::
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
Block<VectorXd> DyNetErrorVector::outputError(unsigned dataIndex)
{
	return this->block((dataSize - 1) * memorySize + dataIndex * outputSize, 0, outputSize, 1);
}

Block<VectorXd> DyNetErrorVector::memoryError(unsigned dataIndex)
{
	if (dataIndex >= dataSize - 1)
		throw runtime_error("out of range");
	return this->block(dataIndex * memorySize, 0, memorySize, 1);
}

DyNetErrorVector::DyNetErrorVector(DyNetSizeManager& sizes) :
			VectorXd(),
			dataSize(sizes.dataSize),
			outputSize(sizes.outputSize),
			memorySize(sizes.memorySize)
{
	unsigned equationNumber = (dataSize - 1) * memorySize + dataSize * outputSize;
	this->setZero(equationNumber);
}
}
}
}

