/*
 * GradientCalculator.cpp
 *
 *    Created on: Dec 13, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "GradientCalculator.hpp"
#include <iostream>
#include <string>
#include <vector>

#include "PerformanceTimer.hpp"
#include "DataSet.hpp"
#include "Network.hpp"
#define NO_TIME_LIMIT -1.0
#define NO_EPOCH_LIMIT 0
using namespace std;
using namespace uta::timingUtils;
namespace uta
{
namespace networkUtils
{
GradientCalculator::GradientCalculator(DataProvider &trainingDataSet, Trainable& network) :
		ErrorCalculator(trainingDataSet, network), inputs(), outputs(), desiredOutputs(), errorPartials(),
				weightPartials(), accumulatedWeightPartials(), trainingDataSet(trainingDataSet), network(network),
				errors()
{
	inputs.resize(trainingDataSet.viewInputSize());
	outputs.resize(trainingDataSet.viewOutputSize());
	desiredOutputs.resize(trainingDataSet.viewOutputSize());
	errorPartials.resize(trainingDataSet.viewOutputSize());
	errors.resize(trainingDataSet.viewOutputSize());

	accumulatedWeightPartials.resize(network.weightSize());
}



void GradientCalculator::handlePointError(unsigned dataIndex)
{
	double pointError = 0;
	for (unsigned outIndex = 0; outIndex < sizes.outputSize; outIndex++)
	{
		plotter.setOutputs(outIndex, network.getOutput(sizes.memorySize + outIndex));
		plotter.setDesiredOutputs(outIndex, dataSet.getOutput(dataIndex, outIndex));
		double e = network.getOutput(sizes.memorySize + outIndex) - dataSet.getOutput(dataIndex, outIndex);
		errors[outIndex] = e;
		pointError += e * e / static_cast<double>(dataSet.viewSize());

	}
	for (unsigned outIndex = 0; outIndex < sizes.outputSize; outIndex++)
	{
		network.setOutputPartial(sizes.memorySize + outIndex,
				2.0 * errors[outIndex] / static_cast<double>(dataSet.viewSize()));
	}
	plotter.setPointError(pointError);
	error += pointError;

	plotter.setAccumulatedError(error);
	recordUnJudgedOutputs();
}
void GradientCalculator::zeroNonPointErrorPartials(void)
{
	for (unsigned memoryIndex = 0; memoryIndex < sizes.memorySize; memoryIndex++)
		network.setOutputPartial(memoryIndex, 0.0);
	for (unsigned unJudgedIndex = 0; unJudgedIndex < sizes.unjudgedSize; unJudgedIndex++)
		network.setOutputPartial(sizes.memorySize + sizes.outputSize + unJudgedIndex, 0.0);
}
void GradientCalculator::calculate(unsigned epochNumber)
{
	for (unsigned m = 0; m < sizes.memorySize; m++)
		memory[m] = initialMemory[m];
	error = 0;
	plotter.prepareEpoch();
	pointwisePartials.resize(sizes.weightSize, sizes.memorySize);
	backPropThroughTimeMemory.resize(sizes.weightSize, sizes.memorySize);
	resetErrorThreshold();
	for (unsigned dataIndex = 0; dataIndex < dataSet.viewSize(); dataIndex++)
	{
		packAndRecordMemory();
		packAndRecordInputs(dataIndex);

		network.calculateOutputs();
		unpackAndRecordOutputMemory();
		zeroNonPointErrorPartials();
		handlePointError(dataIndex);
		checkErrorThreshold(dataIndex);
		plotter.recordData();
		calculateGradientOfError();
		calculateJacobianOfMemory();

		backPropThroughTimeMemory.updateFromPoint(pointwisePartials);
	}
	plotter.wrapUpAndSaveFile(epochNumber);
}
void GradientCalculator::calculateGradientOfError()
{
	network.calculatePartials();
	for (unsigned m = 0; m < sizes.memorySize; m++)
		pointwisePartials.pointError_memoryPosterior(0, m) = network.getInputPartial(m);
	for (unsigned w = 0; w < sizes.weightSize; w++)
		pointwisePartials.pointError_weights(0, w) = network.getWeightPartial(w);
}

void GradientCalculator::calculateJacobianOfMemory()
{
	for (unsigned z = 0; z < sizes.totalOutputSize(); z++)
		network.setOutputPartial(z, 0.0);
	for (unsigned memoryIndex = 0; memoryIndex < sizes.memorySize; memoryIndex++)
	{
		if (memoryIndex > 0)
			network.setOutputPartial(memoryIndex - 1, 0.0);

		network.setOutputPartial(memoryIndex, 1.0);
		network.calculatePartials();
		for (unsigned posteriorIndex = 0; posteriorIndex < sizes.memorySize; posteriorIndex++)
			pointwisePartials.memory_memoryPosterior(memoryIndex, posteriorIndex) = network.getInputPartial(
					posteriorIndex);
		for (unsigned weightIndex = 0; weightIndex < sizes.weightSize; weightIndex++)
		{
			pointwisePartials.memory_weights(memoryIndex, weightIndex) = network.getWeightPartial(weightIndex);
		}
	}
}

double GradientCalculator::getError_Weights(unsigned index)
{
	return this->backPropThroughTimeMemory.errorPosterior_weights(0, index);
}
double GradientCalculator::getError_InitialMemory(unsigned index)
{
	return this->backPropThroughTimeMemory.errorPosterior_initialMemory(0, index);
}

}
}
