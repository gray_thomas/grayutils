/*
 * DyNetStates.cpp
 *
 *    Created on: Feb 11, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "DyNetStates.hpp"
using namespace Eigen;
namespace uta
{
namespace networkUtils
{
namespace dyNetUtils
{
DyNetStates::DyNetStates(DyNetSizeManager& sizes) :
			VectorXd(sizes.memorySize * sizes.dataSize),
			memorySize(sizes.memorySize)
{
	this->setZero(memorySize * sizes.dataSize);
}
Block<VectorXd> DyNetStates::getState(unsigned dataIndex)
{
	return this->block(memorySize * dataIndex, 0, memorySize, 1);
}
}
}
}
