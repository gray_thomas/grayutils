/*
 * LeastSquaresTrainer.cpp
 *
 *  Created on: Dec 29, 2013
 *      Author: Gray
 */

#ifndef LEASTSQUARESTRAINER_CPP_
#define LEASTSQUARESTRAINER_CPP_

#include <DataProvider.hpp>
#include <DyNetSizeManager.hpp>
#include <Eigen/Core>
#include <ErrorCalculator.hpp>
#include <GradientCalculator.hpp>
#include <GradientDescentTrainer.hpp>
#include <LeastSquaresTrainer.hpp>
#include <PerformanceTimer.hpp>
#include <RegressorCalculator.hpp>
#include <Trainable.hpp>
#include <fstream>
#include <new>
#include <stdexcept>
#include <string>
#include <vector>

using namespace Eigen;
using namespace std;
namespace uta
{
namespace networkUtils
{

void LeastSquaresTrainer::trainNetwork(DataProvider &trainingDataSet, DataProvider &validationDataSet, Trainable& network,
										double learningRate)
{
	trainingEpochs = 0;
	timer.startTimer();
	trainError = 0;
	eliteError = 1e99;
	eliteWeights = network.getWeights();
	regressorCalculator = new RegressorCalculator(trainingDataSet, network);
	errorCalc = new ErrorCalculator(validationDataSet, network);
	if (this->plottingEnabled)
		regressorCalculator->setupPlotting(this->plottingFileBaseName, this->plottingFrequency);
	sizes = DyNetSizeManager(regressorCalculator->getSizes());
	eliteInitialMemory.resize(sizes.memorySize);
	do
	{
		trainOneEpoch(trainingDataSet, network, learningRate);

		if (logFile.is_open())
		{
			errorCalc->calculate(trainingEpochs);
			testError = errorCalc->getError();
			logFile << trainingEpochs << " " << trainError << " " << testError << endl;
			cout << trainingEpochs << " " << trainError << " " << testError << endl;
		}
	} while (doesTimerRemain() && doEpochsRemain());
	delete regressorCalculator;
	regressorCalculator = NULL;
	delete errorCalc;
	errorCalc = NULL;

	network.setWeights(eliteWeights);
	cout << "trained for " << trainingEpochs << " epoch." << endl;
}
void LeastSquaresTrainer::trainOneEpoch(DataProvider &trainingDataSet, Trainable& network, double learningRate)
{
	if (initialMemory.size() != sizes.memorySize)
		throw runtime_error("initial memory not set");
	regressorCalculator->setInitialMemory(initialMemory);
	regressorCalculator->calculate(this->trainingEpochs);
	trainError = regressorCalculator->getError();
	network.packWeights(weights);
	cout << trainError << " is the training Error this epoch" << endl;
	if (trainError < eliteError)
	{
		eliteError = trainError;
		for (unsigned i = 0; i < weights.size(); i++)
			eliteWeights[i] = weights[i];
		for (unsigned i = 0; i < initialMemory.size(); i++)
			eliteInitialMemory[i] = initialMemory[i];
	}
	unsigned linearizationBreakdownIndex = regressorCalculator->getIndexOfThresholdCrossing();
	if (linearizationBreakdownIndex >= trainingDataSet.viewSize())
		linearizationBreakdownIndex = trainingDataSet.viewSize() - 1;
	unsigned maxRegressorRow = (linearizationBreakdownIndex + 1) * sizes.outputSize;
	unsigned regressorColumnSize = sizes.weightSize + sizes.memorySize;

	Matrix<double, Dynamic, Dynamic> regressor = regressorCalculator->getRegressor().block(0, 0, maxRegressorRow,
			regressorColumnSize);
	Matrix<double, Dynamic, 1> regressionError = regressorCalculator->getRegressionError().block(0, 0,
			maxRegressorRow, 1);
	Matrix<double, Dynamic, Dynamic> smallSymmetricProduct = regressor.transpose() * regressor;
	cout << "the small symmetric product's determinant is " << (smallSymmetricProduct.determinant()) << endl;
	cout << "the small symmetric product matrix is \n" << (smallSymmetricProduct) << endl;
	cout << "the regressor matrix itself is = \n" << regressor << endl;
	for (unsigned i = 0; i < regressorColumnSize; i++)
		smallSymmetricProduct(i, i) *= (1 + dampingFactor);
	// This is the marquard augmentation to levenburg's damped least squares.
	cout << "the damping factor is " << dampingFactor << endl;
	cout << "the levenburg-marquard symmetric matrix's determinant is " << (smallSymmetricProduct.determinant())
			<< endl;
	cout << "the levenburg-marquard symmetric matrix is \n" << (smallSymmetricProduct) << endl;
	Matrix<double, Dynamic, 1> resultingDelta = smallSymmetricProduct.colPivHouseholderQr().solve(
			regressor.transpose() * regressionError);
	cout << "the resulting delta is \n" << resultingDelta << endl;

	for (unsigned i = 0; i < weights.size(); i++)
		weights[i] -= regressorCalculator->getError_Weights(i) * (learningRate);
	for (unsigned i = 0; i < initialMemory.size(); i++)
		initialMemory[i] -= regressorCalculator->getError_InitialMemory(i) * (learningRate);

	network.setWeights(weights);
}

void LeastSquaresTrainer::setLinearizationApproximationBreakdownError(double absoluteErrorThreshold)
{
	linearizationApproximationBreakdownErrorThreshold = absoluteErrorThreshold;
}

}
}

#endif /* LEASTSQUARESTRAINER_CPP_ */
