/*
 * ErrorCalculator.cpp
 *
 *    Created on: Dec 13, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "ErrorCalculator.hpp"
#include <iostream>
#include <stdexcept>
using namespace std;



namespace uta
{
namespace networkUtils
{

ErrorCalculator::ErrorCalculator(DataProvider& dataSet, Trainable& network) :
			dataSet(dataSet),
			network(network),
			error(0),
			memory(),
			initialMemory(),
			plotter(sizes),
			errorThreshold(1e99),
			indexOfThresholdCrossing(0),
			thresholdCrossed(false)
{
	sizes.inputSize = dataSet.viewInputSize();
	sizes.outputSize = dataSet.viewOutputSize();
	sizes.dataSize = dataSet.viewSize();

	if (network.inputSize() < dataSet.viewInputSize())
		throw runtime_error("Network does not fit dataSet!");
	sizes.memorySize = network.inputSize() - dataSet.viewInputSize();

	if (network.outputSize() < sizes.memorySize + dataSet.viewOutputSize())
		throw runtime_error("Network does not have enough outputs!");
	sizes.unjudgedSize = network.outputSize() - dataSet.viewOutputSize() - sizes.memorySize;
	sizes.weightSize = network.weightSize();

	plotter = SystemWithMemoryPlotter(sizes);
	memory.resize(sizes.memorySize);
	initialMemory.resize(sizes.memorySize);
	for (unsigned m = 0; m < sizes.memorySize; m++)
		initialMemory[m] = 0.0;
}

void ErrorCalculator::setupPlotting(string dataSetFileName, unsigned plotEveryXEpochs)
{
	plotter.setupRecording(dataSetFileName, plotEveryXEpochs);
}

void ErrorCalculator::packAndRecordInputs(unsigned dataIndex)
{
	for (unsigned inputIndex = 0; inputIndex < sizes.inputSize; inputIndex++)
	{
		network.setInput(sizes.memorySize + inputIndex, dataSet.getInput(dataIndex, inputIndex));
		plotter.setInput(inputIndex, dataSet.getInput(dataIndex, inputIndex));
	}
}
void ErrorCalculator::packAndRecordMemory(void)
{
	for (unsigned memIndex = 0; memIndex < sizes.memorySize; memIndex++)
	{
		network.setInput(memIndex, memory[memIndex]);
	}
}
void ErrorCalculator::unpackAndRecordOutputMemory(void)
{
	for (unsigned memIndex = 0; memIndex < sizes.memorySize; memIndex++)
	{
		memory[memIndex] = network.getOutput(memIndex);
		plotter.setOutputMemory(memIndex, memory[memIndex]);
	}
}
void ErrorCalculator::unpackJudgeAndRecordOutputs(unsigned dataIndex)
{
	double pointError = 0;
	for (unsigned outIndex = 0; outIndex < sizes.outputSize; outIndex++)
	{
		plotter.setOutputs(outIndex, network.getOutput(sizes.memorySize + outIndex));
		plotter.setDesiredOutputs(outIndex, dataSet.getOutput(dataIndex, outIndex));
		double e = network.getOutput(sizes.memorySize + outIndex) - dataSet.getOutput(dataIndex, outIndex);
		pointError += e * e;
	}
	plotter.setPointError(pointError);
	error += pointError / static_cast<double>(dataSet.viewSize());
	plotter.setAccumulatedError(error);
}
void ErrorCalculator::recordUnJudgedOutputs(void)
{
	for (unsigned unJudgedIndex = 0; unJudgedIndex < sizes.unjudgedSize; unJudgedIndex++)
		plotter.setUnJudgedOutputs(unJudgedIndex,
				network.getOutput(unJudgedIndex + sizes.memorySize + sizes.outputSize));
}
void ErrorCalculator::calculate(unsigned epochNumber)
{
	for (unsigned m = 0; m < sizes.memorySize; m++)
		memory[m] = initialMemory[m];
	error = 0;
	plotter.prepareEpoch();
	resetErrorThreshold();
	for (unsigned dataIndex = 0; dataIndex < dataSet.viewSize(); dataIndex++)
	{
		packAndRecordMemory();
		packAndRecordInputs(dataIndex);
		network.calculateOutputs();
		unpackAndRecordOutputMemory();
		unpackJudgeAndRecordOutputs(dataIndex);
		recordUnJudgedOutputs();
		checkErrorThreshold(dataIndex);
		plotter.recordData();
	}
	plotter.wrapUpAndSaveFile(epochNumber);
}
void ErrorCalculator::setInitialMemory(vector<double> initialMemory)
{
	for (unsigned m = 0; m < sizes.memorySize; m++)
		this->initialMemory[m] = initialMemory[m];
}
double ErrorCalculator::getError(void)
{
	return error;
}

void ErrorCalculator::resetErrorThreshold()
{
	this->thresholdCrossed = false;
	indexOfThresholdCrossing = 0;
}

void ErrorCalculator::checkErrorThreshold(unsigned dataIndex)
{
	if (!thresholdCrossed)
	{
		if (error * static_cast<double>(dataSet.viewSize()) > errorThreshold)
		{
			cout << "error margin exceeded at data point " << dataIndex << endl;
			thresholdCrossed = true;
			indexOfThresholdCrossing = dataIndex;
		}
		else
		{
			// In the case where the threshold is never crossed, "index of threshold crossing" is the size of data set.
			indexOfThresholdCrossing = dataIndex + 1;
		}
	}
}

}
}
