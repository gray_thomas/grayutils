/*
 * BackPropThroughTimeStructures.cpp
 *
 *    Created on: Dec 15, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include "BackPropThroughTimeStructures.hpp"
#include "Eigen/Dense"
#include <iostream>
using namespace Eigen;
namespace uta
{
namespace networkUtils
{

std::ostream& operator<<(std::ostream& stream, PointwiseBackPropPartials& partials)
{
	stream << "PointwiseBackPropPartials:" << std::endl;
	stream << "memory_memoryPosterior [" << partials.memory_memoryPosterior <<"]"<< std::endl;
	stream << std::endl;
	stream << "memory_weights [" << partials.memory_weights <<"]" << std::endl;
	stream << std::endl;
	stream << "pointError_memoryPosterior [" << partials.pointError_memoryPosterior <<"]"<< std::endl;
	stream << "pointError_weights [" << partials.pointError_weights << "]"<<std::endl;
	return stream;
}
}
}
