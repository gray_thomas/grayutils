/*
 * SEANetwork.cpp
 *
 *  Created on: Mar 25, 2014
 *      Author: Gray
 */

#include <SEANetwork.hpp>
#include <NetworkDataProxy.hpp>
#include <ScalarElementMath.hpp>
namespace uta
{
namespace demos
{
namespace SEA
{
using namespace networkUtils;

SEANetwork::SEANetwork(NetworkStateVector& stateVector) :
		Network(stateVector, 6)
{
	NetReal dT = constant(1.0);
	NetReal motorPosition = *this->getNetworkInput(0);
	NetReal motorVelocity = *this->getNetworkInput(1);
	NetReal jointPosition = *this->getNetworkInput(2);
	NetReal jointVelocity = *this->getNetworkInput(3);
	NetReal motorCurrent = *this->getNetworkInput(4);
	NetReal externalForce = *this->getNetworkInput(5);
	NetReal motorReciprocalInertia = trainable(1.0); //TODO: all bullshit values now.
	NetReal jointReciprocalInertia = trainable(1.0);
	NetReal jointFriction = trainable(1.0); //Bullshite
	NetReal motorFriction = trainable(0.01);
	NetReal springConstant = trainable(0.01);
	NetReal motorConstant = trainable(0.05);
	NetReal motorEncoderZero = trainable(9000);
	NetReal motorEncoderScale = trainable(1000);
	NetReal jointEncoderZero = trainable(7000);
	NetReal jointEncoderScale = trainable(1000);
	NetReal springEncoderZero = trainable(8000);
	NetReal springEncoderScale = trainable(10);

	NetReal motorAccel = motorReciprocalInertia
			* (motorConstant * motorCurrent - springConstant * (motorPosition - jointPosition)
					- motorFriction * sign(motorVelocity));
	NetReal jointAccel = jointReciprocalInertia
			* (externalForce + springConstant * (motorPosition - jointPosition) - jointFriction * sign(jointVelocity));
	NetReal motorPositionOut = motorPosition + motorVelocity * dT + motorAccel * dT * dT * constant(0.5);
	NetReal motorVelocityOut = motorVelocity + motorAccel * dT;
	NetReal jointPositionOut = jointPosition + jointVelocity * dT + jointAccel * dT * dT * constant(0.5);
	NetReal jointVelocityOut = jointVelocity + jointAccel * dT;

	NetReal motorEncoder = motorEncoderZero + motorPosition * motorEncoderScale;
	NetReal springEncoder = springEncoderZero + (motorPosition + -jointPosition) * springEncoderScale;
	NetReal jointEncoder = jointEncoderZero + (jointPosition) * jointEncoderScale;
	setAsOutput(&motorPositionOut);
	setAsOutput(&motorVelocityOut);
	setAsOutput(&jointPositionOut);
	setAsOutput(&jointVelocityOut);
	setAsOutput(&motorEncoder);
	setAsOutput(&jointEncoder);
	setAsOutput(&springEncoder);
}

// Network constructor attempts to contain the domain specific knowledge.
FullSEA::FullSEA(NetworkStateVector& state, double dt) :
		DyNetWithNames(state, 7, 1)
{
	// these can be safely re-ordered
	SingleIndexNetworkDataProxy o = *(getNetworkState(0, 1e2, "motor position"));
	SingleIndexNetworkDataProxy od = *(getNetworkState(1, 1e3, "motor velocity"));
	SingleIndexNetworkDataProxy odd = *(getNetworkState(2, 1e4, "motor acceleration"));
	SingleIndexNetworkDataProxy j = *(getNetworkState(3, 1e1, "joint position"));
	SingleIndexNetworkDataProxy jd = *(getNetworkState(4, 1e2, "joint velocity"));
	SingleIndexNetworkDataProxy jdd = *(getNetworkState(5, 1e3, "joint acceleration"));
	SingleIndexNetworkDataProxy s = *(getNetworkState(6, 1e3, "spring position"));
	// start input indexing over from zero.
	SingleIndexNetworkDataProxy input = *(this->getNetworkInput(0, "motor current"));

	// since meta-data is associated with parameters when they are constructed, their order is less important.
	// Non-scalar inputs are future work, but have potential to raise the abstraction barrier for users.
//	double unitShift = 2000 * 2000;
	// Models are expressed using overloaded operator arithmetic, a strategy which scales well to spatial vector algebra.
//# spring simple alphas: [-27097.385252, -0.135839, 3.499051, -37.354313, -70.188208, -75.600643]
	SingleIndexNetworkDataProxy motorSpringTransmission = this->trainable(-0.135839, 0.0001, "motorSpringTransmission");
	SingleIndexNetworkDataProxy springOutputTransmission = this->trainable(3.499051, 0.001, "springOutputTransmission");
	SingleIndexNetworkDataProxy springPredictionBias = this->trainable(-27097.385252, 0.01, "springPredictionBias");
	SingleIndexNetworkDataProxy springEstimate = motorSpringTransmission * o + springOutputTransmission * j
			+ springPredictionBias;

	SingleIndexNetworkDataProxy springDevZero = this->trainable(8857.84, 10, "springDevZero"); // fairly well set, I think.
	SingleIndexNetworkDataProxy springDeviation = s - springDevZero;

//# model estimate: constantAccel=-56.314494, currentConstant=0.007972, springConstant=0.006355, coulombFriction=-0.265531, damping=-0.000798
	SingleIndexNetworkDataProxy inputToMotorAcceleration = this->trainable(31888, 10.0, "inputToMotorAcceleration");
	SingleIndexNetworkDataProxy springDevToMotAccel = this->trainable(25418.0, 10.0, "springDevToMotAccel");
	SingleIndexNetworkDataProxy motorAccelBias = this->trainable(218186, 10.0, "motorAccelBias");
	SingleIndexNetworkDataProxy motorAccelerationPrediction = inputToMotorAcceleration * input
			+ springDevToMotAccel * springDeviation + motorAccelBias;

//# exterior model estimate: constantAccel=7.985633, springConstant=-0.000891, linearGravity=-0.000013, damping=-0.000681, friction=-0.003735
	SingleIndexNetworkDataProxy gravity = this->trainable(329861.0, 1000, "gravity");
	SingleIndexNetworkDataProxy linearGrav = this->trainable(-52.0, 1, "linearGrav");
	SingleIndexNetworkDataProxy springDevToJointAccel = this->trainable(-3565.0, 10.0, "springDevToJointAccel");
	SingleIndexNetworkDataProxy jointAccelerationPrediction = springDevToJointAccel * springDeviation + gravity
			+ linearGrav * j;

	// Enables low pass filter mode:
//	jointAccelerationPrediction = jointAccelerationPrediction*constant(0.0);
//	motorAccelerationPrediction = motorAccelerationPrediction * constant(0.0);

	// outputs are not preallocated, so order is significant, but the program will tell you if the names don't match.
	setAsStateUpdate(&(o + constant(dt) * od), 1.0e-6, "motor position"); // enforce
	setAsStateUpdate(&(od + constant(dt) * odd), 1.0e-2, "motor velocity"); // enforce
	setAsStateUpdate(&(motorAccelerationPrediction), 1.0e5, "motor acceleration"); // punt not so much
	setAsStateUpdate(&(j + constant(dt) * jd), 1.0e-6, "joint position"); // enforce
	setAsStateUpdate(&(jd + constant(dt) * jdd), 1.0e-2, "joint velocity"); // enforce
	setAsStateUpdate(&(jointAccelerationPrediction), 2.0e4, "joint acceleration"); // punt not so much
	setAsStateUpdate(&(springEstimate), 5e5, "spring position"); // punt hard (solve this last?)
	//TODO: it's hard to distinguish weghts due to punting and weights due to order of magnitude of the actual variables.

	// true outputs must follow state update outputs
	setAsMeasurement(&(o + constant(0.0)), 5e-1, "motor encoder reading"); // this needs to be accurate to observe the spring equation
	setAsMeasurement(&(s + constant(0.0)), 5e-1, "spring encoder reading"); // this needs to be accurate to observe the spring equation
	setAsMeasurement(&(j + constant(0.0)), 5e-1, "joint encoder reading"); // the model will survive some noise here
}
SEALowPass::SEALowPass(NetworkStateVector& state, double dt) :
		DyNetWithNames(state, 6, 1)
{
	SingleIndexNetworkDataProxy x = *(getNetworkState(0, 1e2, "position"));
	SingleIndexNetworkDataProxy xd = *(getNetworkState(1, 1e4, "velocity"));
	SingleIndexNetworkDataProxy xdd = *(getNetworkState(2, 1e6, "acceleration"));
	SingleIndexNetworkDataProxy D = *(getNetworkState(3, 1e2, "delta position"));
	SingleIndexNetworkDataProxy Dd = *(getNetworkState(4, 1e4, "delta velocity"));
	SingleIndexNetworkDataProxy Ddd = *(getNetworkState(5, 1e6, "delta acceleration"));
	// x is in joint encoder units, D is in spring encoder units.
	SingleIndexNetworkDataProxy input = *(this->getNetworkInput(0, "motor current"));

	SingleIndexNetworkDataProxy jointToMotor = this->trainable(25.7, 2, "jointToMotor");
	SingleIndexNetworkDataProxy motorZero = this->trainable(-262541, 100000, "motorZero");
	SingleIndexNetworkDataProxy deflectionToMotor = this->trainable(-7.361656078151341, 1, "deflectionToMotor");
	SingleIndexNetworkDataProxy deflectionToJoint = this->trainable(0.28579177611300893, .1, "deflectionToJoint");
	SingleIndexNetworkDataProxy avgSpringValue = this->trainable(8885.51, .01, "avgSpringValue");
	// By tuning the parameters of the equivalent low pass filter, we can find a reasonable set of
	// weights, which is a far more constrained problem then I initially thought, because the weights determine
	// the time constant of the effective filter, and they are both sensitive in this regard and of wide ranging
	// magnitude. Note that this filter, now with the position-delta representation of the states has
	// trainable outputs which can be trained even in the absence of prediction.
	setAsStateUpdate(&(x + constant(dt) * xd), 1.0e-4, "position");
	setAsStateUpdate(&(xd + constant(dt) * xdd), 1.0e-4, "velocity");
	setAsStateUpdate(&(constant(0)), 1.0e5, "acceleration");
	setAsStateUpdate(&(D + constant(dt) * Dd), 1.0e-4, "delta position");
	setAsStateUpdate(&(Dd + constant(dt) * Ddd), 1.0e-4, "delta velocity");
	setAsStateUpdate(&(constant(0)), 2.0e5, "delta acceleration");

	setAsMeasurement(&(jointToMotor*x+motorZero+deflectionToMotor*D), 1e1, "motor encoder reading");
	setAsMeasurement(&(avgSpringValue+D), 1e0, "spring encoder reading");
	setAsMeasurement(&(x+deflectionToJoint*D), 1e0, "joint encoder reading");
	//TODO: DyNets should be able to differentiate between prediction state updates and consistancy state updates.
}
PositionDeflectionSEA::PositionDeflectionSEA(NetworkStateVector& state, double dt) :
		DyNetWithNames(state, 6, 1)
{
	SingleIndexNetworkDataProxy pos = *(getNetworkState(0, 1e2, "position"));
	SingleIndexNetworkDataProxy posDot = *(getNetworkState(1, 1e4, "velocity"));
	SingleIndexNetworkDataProxy posDDot = *(getNetworkState(2, 1e6, "acceleration"));
	SingleIndexNetworkDataProxy def = *(getNetworkState(3, 1e2, "delta position"));
	SingleIndexNetworkDataProxy defDot = *(getNetworkState(4, 1e4, "delta velocity"));
	SingleIndexNetworkDataProxy defDDot = *(getNetworkState(5, 1e6, "delta acceleration"));
	// x is in joint encoder units, D is in spring encoder units.
	SingleIndexNetworkDataProxy input = *(this->getNetworkInput(0, "motor current"));

	SingleIndexNetworkDataProxy jointToMotor = this->trainable(25.7545, 0.2, "jointToMotor");
	SingleIndexNetworkDataProxy motorZero = this->trainable( -265260, 1000, "motorZero");
	SingleIndexNetworkDataProxy deflectionToMotor = this->trainable(-1.5166, 1, "deflectionToMotor");
	SingleIndexNetworkDataProxy deflectionToJoint = this->trainable(0.0970216, .1, "deflectionToJoint");
	SingleIndexNetworkDataProxy avgSpringValue = this->trainable(8879.59, .01, "avgSpringValue");

	SingleIndexNetworkDataProxy inputToPos = this->trainable(1027.54, 10, "inputToPos");
	SingleIndexNetworkDataProxy constToPos = this->trainable(17910.5, 1000, "constToPos");
	SingleIndexNetworkDataProxy inputToDef = this->trainable(-3509.51, 10, "inputToDef");
	SingleIndexNetworkDataProxy defToDefOsc = this->trainable(-17927.3, 100, "defToDefOsc");
	SingleIndexNetworkDataProxy posToPosDamp =  this->trainable(-5.55309, 10, "posToPosDamp");
	// By tuning the parameters of the equivalent low pass filter, we can find a reasonable set of
	// weights, which is a far more constrained problem then I initially thought, because the weights determine
	// the time constant of the effective filter, and they are both sensitive in this regard and of wide ranging
	// magnitude. Note that this filter, now with the position-delta representation of the states has
	// trainable outputs which can be trained even in the absence of prediction.
	setAsStateUpdate(&(pos + constant(dt) * posDot), 1.0e-4, "position");
	setAsStateUpdate(&(posDot + constant(dt) * posDDot), 1.0e-4, "velocity");
	setAsStateUpdate(&(inputToPos*input+constToPos+ posToPosDamp* posDot), 1.0e5, "acceleration");
	setAsStateUpdate(&(def + constant(dt) * defDot), 1.0e-4, "delta position");
	setAsStateUpdate(&(defDot + constant(dt) * defDDot), 1.0e-4, "delta velocity");
	setAsStateUpdate(&(inputToDef*input+defToDefOsc*def), 2.0e5, "delta acceleration");

	setAsMeasurement(&(jointToMotor*pos+motorZero+deflectionToMotor*def), 1e1, "motor encoder reading");
	setAsMeasurement(&(avgSpringValue+def), 1e0, "spring encoder reading");
	setAsMeasurement(&(pos+deflectionToJoint*def), 1e0, "joint encoder reading");
	//TODO: DyNets should be able to differentiate between prediction state updates and consistancy state updates.
}
}
}
}

