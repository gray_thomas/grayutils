/*
 * MainPendulumNetDemo.cpp
 *
 *    Created on: Nov 24, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
#include <random>
#include "DataSet.hpp"
#include "PendulumDemoRandomDataGenerator.hpp"
#include "BackPropThroughTimeTrainer.hpp"
#include "NameMaintainer.hpp"
#include "NetworkStateVector.hpp"
#include "PendulumNet.hpp"
#include "GUFiles.hpp"

using namespace std;
using namespace uta::networkUtils;
using namespace uta::networkUtils::pendulumDemo;

void trainFullNetwork()
{
	cout << "Hello World! Testing Full Network With No memory." << endl;
	default_random_engine rand(334534);
	lognormal_distribution<double> guessError(0.0, 0.001);
	double sigma_alpha = 0.00005;
	double sigma_measure = 0.1;

	double tau = 0.101;
	double tauGuess = tau * guessError(rand);
	double xi = 0.013;
	double xiGuess = xi * guessError(rand);
	double eps_plus_one = 2.33;
	double epsilon_plus_oneGuess = eps_plus_one * guessError(rand);
	double inputScale = 1.5e-1;
	double inputScaleGuess = inputScale * guessError(rand);
	double theta0 = -1.0;
	double theta0Guess = theta0 * guessError(rand);
	double omega0 = 0.3;
	double omega0Guess = omega0 * guessError(rand);

	double learningRate = 0.01;

	double sigma_input = 1.00;
	unsigned minHold = 40;
	unsigned maxHold = 300;
	double dt = 0.01;
	unsigned eulerStepsInGenerator = 1;
	unsigned eulerStepsInNetwork = 1;
	unsigned recordGenerationFrequency = 1;
	unsigned amountOfData = 200;
	unsigned epochMax = 100;

	gu::set_active_folder("DyNets/Pendulum/trainFullNetwork_TwoMem");
	FullPendulumDemoDirectTrainingDataGenerator gen(tau, eps_plus_one, xi, inputScale);
	gen.generateRandomData2mem("patterns.trn", amountOfData, theta0, omega0, sigma_alpha,
			sigma_input, sigma_measure, minHold, maxHold, dt, eulerStepsInGenerator);
	DataSet trainingDataSet("patterns.trn");
	NetworkStateVector state;
	AltFullPendulumNet* network = new AltFullPendulumNet(state, dt, inputScaleGuess, tauGuess, xiGuess,
			epsilon_plus_oneGuess, eulerStepsInNetwork);
	GradientDescentTrainer trainer("learning.dat");
	trainer.setupLogging("dataRuns", recordGenerationFrequency);
	trainer.setEpochMax(epochMax);
	trainer.setTimeLimit(10.0);
	vector<double> initialMemGuess;
	initialMemGuess.resize(2);
	initialMemGuess[0] = theta0Guess;
	initialMemGuess[1] = omega0Guess;
	trainer.setInitialMemory(initialMemGuess);
	cout << "weight0 is " << network->getWeight(0) << " actually " << tau << endl;
	cout << "weight1 is " << network->getWeight(1) << " actually " << xi << endl;
	cout << "weight2 is " << network->getWeight(2) << " actually " << eps_plus_one << endl;
	cout << "weight3 is " << network->getWeight(3) << " actually " << inputScale << endl;
	cout << "memory1 is " << trainer.getUpcomingMemory(0) << " actually " << theta0 << endl;
	trainer.trainNetwork(trainingDataSet, trainingDataSet, *network, learningRate);
	cout << "weight0 is " << network->getWeight(0) << " started as " << tauGuess << " actually " << tau << endl;
	cout << "weight1 is " << network->getWeight(1) << " started as " << xiGuess << " actually " << xi << endl;
	cout << "weight2 is " << network->getWeight(2) << " started as " << epsilon_plus_oneGuess << " actually "
			<< eps_plus_one << endl;
	cout << "weight3 is " << network->getWeight(3) << " started as " << inputScaleGuess << " actually " << inputScale
			<< endl;
	cout << "memory0 is " << trainer.getUpcomingMemory(0) << " started as " << theta0Guess << " actually " << theta0
			<< endl;
	cout << "memory1 is " << trainer.getUpcomingMemory(1) << " started as " << omega0Guess << " actually " << omega0
			<< endl;
	cout << "Done with test." << endl;
}
void trainFullNetwork1Mem()
{
	cout << "Hello World! Testing Full Network With No memory." << endl;
	default_random_engine rand(334534);
	normal_distribution<double> guessError(1.0, 0.000002);
	double sigma_alpha = 0.00005;
	double sigma_measure = 0.000001;

	double tau = 0.101;
	double tauGuess = tau * guessError(rand);
	double xi = 0.013;
	double xiGuess = xi * guessError(rand);
	double eps_plus_one = 2.33;
	double epsilon_plus_oneGuess = eps_plus_one * guessError(rand);
	double inputScale = 1.5e-1;
	double inputScaleGuess = inputScale * guessError(rand);
	double theta0 = -1.0;
	double theta0Guess = theta0 * guessError(rand) * guessError(rand);

	double learningRate = 0.01;

	double sigma_input = 1.00;
	double omega0 = 0.3;
	unsigned minHold = 40;
	unsigned maxHold = 60;
	double dt = 0.07;
	unsigned eulerStepsInGenerator = 1;
	unsigned eulerStepsInNetwork = 1;
	unsigned recordGenerationFrequency = 1;
	unsigned amountOfData = 1;
	unsigned epochMax = 41;
	gu::set_active_folder("DyNets/Pendulum/trainFullNetwork_OneMem");
	FullPendulumDemoDirectTrainingDataGenerator gen(tau, eps_plus_one, xi, inputScale);
	gen.generateRandomData1mem("patterns.trn", amountOfData, theta0, omega0, sigma_alpha,
			sigma_input, sigma_measure, minHold, maxHold, dt, eulerStepsInGenerator);
	DataSet trainingDataSet("patterns.trn");
	NetworkStateVector state;
	AltFullPendulumNet* network = new AltFullPendulumNet(state, dt, inputScaleGuess, tauGuess, xiGuess,
			epsilon_plus_oneGuess, eulerStepsInNetwork);
	GradientDescentTrainer trainer("learning.dat");
	trainer.setupLogging("dataRun", recordGenerationFrequency);
	trainer.setEpochMax(epochMax);
	vector<double> initialMemGuess;
	initialMemGuess.resize(1);
	initialMemGuess[0] = theta0Guess;
	trainer.setInitialMemory(initialMemGuess);
	cout << "weight0 is " << network->getWeight(0) << " actually " << tau << endl;
	cout << "weight1 is " << network->getWeight(1) << " actually " << xi << endl;
	cout << "weight2 is " << network->getWeight(2) << " actually " << eps_plus_one << endl;
	cout << "weight3 is " << network->getWeight(3) << " actually " << inputScale << endl;
	cout << "memory1 is " << trainer.getUpcomingMemory(0) << " actually " << theta0 << endl;
	trainer.trainNetwork(trainingDataSet, trainingDataSet, *network, learningRate);
	cout << "weight0 is " << network->getWeight(0) << " actually " << tau << endl;
	cout << "weight1 is " << network->getWeight(1) << " actually " << xi << endl;
	cout << "weight2 is " << network->getWeight(2) << " actually " << eps_plus_one << endl;
	cout << "weight3 is " << network->getWeight(3) << " actually " << inputScale << endl;
	cout << "memory1 is " << trainer.getUpcomingMemory(0) << " actually " << theta0 << endl;
	cout << "Done with test." << endl;
}
void trainFullNetwork0Mem()
{
	cout << "Hello World! Testing Full Network With No memory." << endl;
	double tau = 0.101;
	double eps_plus_one = 2.33;
	double xi = 0.013;
	double inputScale = 1.5e-1;
	double tauGuess = 0.101;
	double xiGuess = 0.013;
	double epsilon_plus_oneGuess = 2.33;
	double inputScaleGuess = 1.5e-1;
	gu::set_active_folder("DyNets/Pendulum/trainFullNetwork_NoMem");
	FullPendulumDemoDirectTrainingDataGenerator gen(tau, eps_plus_one, xi, inputScale);
	unsigned amountOfData = 30;
	double theta0 = -1.0;
	double omega0 = 0.3;
	double sigma_alpha = 0.05;
	double sigma_input = 1.00;
	double sigma_measure = 0.1;
	unsigned minHold = 40;
	unsigned maxHold = 300;
	double dt = 0.01;
	gen.generateRandomData0mem("patterns.trn", amountOfData, theta0, omega0, sigma_alpha,
			sigma_input, sigma_measure, minHold, maxHold, dt, 10);
	DataSet trainingDataSet("patterns.trn");

	NetworkStateVector state;
	{
	AltFullPendulumNet* network = new AltFullPendulumNet(state, dt, inputScaleGuess, tauGuess, xiGuess,
			epsilon_plus_oneGuess, 1);
	BackPropThroughTimeTrainer trainer(0, "learning.dat");
	trainer.setEpochMax(10);
	cout << "weight0 is " << network->getWeight(0) << endl;
	cout << "weight1 is " << network->getWeight(1) << endl;
	cout << "weight2 is " << network->getWeight(2) << endl;
	cout << "weight3 is " << network->getWeight(3) << endl;
	trainer.trainNetwork(trainingDataSet, trainingDataSet, *network, 0.1);
	cout << "weight0 is " << network->getWeight(0) << endl;
	cout << "weight1 is " << network->getWeight(1) << endl;
	cout << "weight2 is " << network->getWeight(2) << endl;
	cout << "weight3 is " << network->getWeight(3) << endl;
	cout << "Done with test." << endl;
	std::cout<<"deleting stuff"<<std::endl;}
//	delete gen;
	std::cout<<"heyB"<<std::endl;
}
void trainAlphaNetwork()
{
	cout << "Hello World! Testing Alpha Network Training." << endl;
	gu::set_active_folder("DyNets/Pendulum/alpha_net");
	PendulumDemoDirectTrainingDataGenerator dataGenerator(.101, 2.33, 0.13, 0.01);
	dataGenerator.generateRandomData("patterns.trn", 2000);
	dataGenerator.generateRandomData("patterns.tst", 2000);
	DataSet trainingDataSet("patterns.trn");
	DataSet validationDataSet("patterns.tst");
	GradientDescentTrainer trainer("learning.dat");
	trainer.setEpochMax(8000);
	trainer.setTimeLimit(60);
	NetworkStateVector state;
	PendulumNet* pendulumNet = new PendulumNet(state);

	trainer.trainNetwork(trainingDataSet, validationDataSet, *pendulumNet, 2e-6);
}

int main()
{
	trainFullNetwork();
//	trainFullNetwork1Mem();
//	trainFullNetwork0Mem();
//	trainAlphaNetwork();
}
