/*
 * PendulumNet.cpp
 *
 *    Created on: Nov 23, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "PendulumNet.hpp"

#include <memory>
#include <new>
#include <sstream>
#include <string>
#include <vector>

#include "NetworkDataProxy.hpp"
#include "ScalarAdditionElement.hpp"
#include "ScalarCosineOperator.hpp"
#include "ScalarMultiplicationElement.hpp"
#include "ScalarNegationOperator.hpp"
#include "ScalarReciprocalOperator.hpp"
#include "ScalarSignumOperator.hpp"
#include "ScalarSineOperator.hpp"
#include "TrainableScalarElement.hpp"
#include "AbstractMultiInputMultiOutputNetworkObject.hpp"
#include "Network.hpp"
#include "NetworkStateVector.hpp"
#include "ScalarElement.hpp"
#include "ScalarOperator.hpp"
#include "TestUtils.hpp"

#define PI 3.14159265395

using namespace std;
using namespace uta::networkUtils;
using namespace uta::testUtils;
namespace uta
{
namespace networkUtils
{
namespace pendulumDemo
{

FullPendulumNet::FullPendulumNet(NetworkStateVector& state, double dt, double aGuess, double bGuess, double cGuess,
		double dGuess) :
		Network(state, 3)
{
	SingleIndexNetworkDataProxy theta = *(this->getNetworkInput(0));
	SingleIndexNetworkDataProxy omega = *(this->getNetworkInput(1));
	SingleIndexNetworkDataProxy input = *(this->getNetworkInput(2));

	SingleIndexNetworkDataProxy a = this->trainable(aGuess);
	SingleIndexNetworkDataProxy b = this->trainable(bGuess);
	SingleIndexNetworkDataProxy c = this->trainable(cGuess);
	SingleIndexNetworkDataProxy d = this->trainable(dGuess);

	SingleIndexNetworkDataProxy alpha = -(sin(theta) * a + sign(omega) * (cos(theta) * b + c * omega * omega))
			+ d * input;
	setAsOutput(&(theta + constant(dt) * omega));
	setAsOutput(&(omega + constant(dt) * alpha));
	setAsOutput(&(theta * constant(1.0)));
}
FullPendulumNetV2::FullPendulumNetV2(NetworkStateVector& state, double dt, double aGuess, double bGuess, double cGuess,
		double dGuess, double signStiffness) :
		Network(state, 3)
{
	SingleIndexNetworkDataProxy theta = *(this->getNetworkInput(0));
	SingleIndexNetworkDataProxy omega = *(this->getNetworkInput(1));
	SingleIndexNetworkDataProxy input = *(this->getNetworkInput(2));

	SingleIndexNetworkDataProxy a = this->trainable(aGuess);
	SingleIndexNetworkDataProxy b = this->trainable(bGuess);
	SingleIndexNetworkDataProxy c = this->trainable(cGuess);
	SingleIndexNetworkDataProxy d = this->trainable(dGuess);

	SingleIndexNetworkDataProxy alpha = -(sin(theta) * a + sign(omega, signStiffness) * b + c * omega) + d * input;
	setAsOutput(&(theta + constant(dt) * omega));
	setAsOutput(&(omega + constant(dt) * alpha));
	setAsOutput(&(theta * constant(1.0)));
}

FullPendulumNetV3::FullPendulumNetV3(NetworkStateVector& state, double dt, double aGuess, double bGuess, double cGuess,
		double dGuess, double signStiffness) :
		Network(state, 4)
{
	SingleIndexNetworkDataProxy theta = *(this->getNetworkInput(0));
	SingleIndexNetworkDataProxy omega = *(this->getNetworkInput(1));
	SingleIndexNetworkDataProxy alpha = *(this->getNetworkInput(2));
	SingleIndexNetworkDataProxy input = *(this->getNetworkInput(3));

	SingleIndexNetworkDataProxy a = this->trainable(aGuess);
	SingleIndexNetworkDataProxy b = this->trainable(bGuess);
	SingleIndexNetworkDataProxy c = this->trainable(cGuess);
	SingleIndexNetworkDataProxy d = this->trainable(dGuess);
	SingleIndexNetworkDataProxy e = this->trainable(signStiffness);

//	SingleIndexNetworkDataProxy alphaPred = -(sin(theta) * a + sign(omega * e, 1.0) * b + c * omega)
//			+ d * input;
	SingleIndexNetworkDataProxy alphaPred = -(sin(theta) * a + c * omega) + d * input;
//	SingleIndexNetworkDataProxy alphaPred=theta*a;
	setAsOutput(&(theta + constant(dt) * omega));
	setAsOutput(&(omega + constant(dt) * alpha));
	setAsOutput(&(alphaPred)); // this is not technically the correct alpha to compare,
	// and doing this will de-localize my alpha predictor slightly, but the correct way to do it would require an
	// extra output which is always compared to zero. In the future I should consider making networks dataset-aware
	// and aware of the solver as well, so the network can specify zero comparisons more easily. They come up a lot.
	// it might even make sense to have the network declare all outputs as equal zero functions and treat measurements
	// as inputs. Thus the network might write: setAsEqualsZeroExpression(predictedOutput-measurement, MeasurementSigma)
	// this might also allow trainable sigmas. The network would also be able to explicitly reference future states:
	// setAsDifferenceRelationEqualToZero(theta + dt*omega - futureTheta, updateNoiseStdDev)
	setAsOutput(&(theta * constant(1.0)));
}

NetworkLowPass::NetworkLowPass(NetworkStateVector& state, double dt) :
		Network(state, 4)
{
	// When trained on a single input single output system, this should effectively filter the data and return
	// estimates of the derivatives.
	SingleIndexNetworkDataProxy theta = *(this->getNetworkInput(0));
	SingleIndexNetworkDataProxy omega = *(this->getNetworkInput(1));
	SingleIndexNetworkDataProxy alpha = *(this->getNetworkInput(2)); // fake state
	SingleIndexNetworkDataProxy input = *(this->getNetworkInput(3));

	setAsOutput(&(theta + constant(dt) * omega)); // stdDev should be low, since we expect this to be nearly perfect
	setAsOutput(&(omega + constant(dt) * alpha)); // stdDev should be low, since we expect this to be nearly perfect
	setAsOutput(&(alpha * constant(0.0))); // compares next alpha to zero
	setAsOutput(&(theta * constant(1.0))); // output
}
NetworkGaussianFilter::NetworkGaussianFilter(NetworkStateVector& state, double dt) :
		Network(state, 5)
{
	// When trained on a single input single output system, this should effectively filter the data and return
	// estimates of the derivatives.
	SingleIndexNetworkDataProxy theta = *(this->getNetworkInput(0));
	SingleIndexNetworkDataProxy omega = *(this->getNetworkInput(1));
	SingleIndexNetworkDataProxy alpha = *(this->getNetworkInput(2)); // fake state
	SingleIndexNetworkDataProxy alphadot = *(this->getNetworkInput(3)); // fake state
	SingleIndexNetworkDataProxy input = *(this->getNetworkInput(4));

	setAsOutput(&(theta + constant(dt) * omega)); // stdDev should be low, since we expect this to be nearly perfect
	setAsOutput(&(omega + constant(dt) * alpha)); // stdDev should be low, since we expect this to be nearly perfect
	setAsOutput(&(alpha + constant(dt) * alphadot)); // compares next alpha to previous
	setAsOutput(&(alphadot * constant(0.0))); // // compares next alphadot to zero
	setAsOutput(&(theta * constant(1.0))); // output
}
PDPendulum::PDPendulum(NetworkStateVector& state, double dt) :
		DyNetWithNames(state, 3, 2)
{
	SingleIndexNetworkDataProxy theta = *(this->getNetworkState(0, SpliningStdDev(1e5, 1e5), "theta")); // state
	SingleIndexNetworkDataProxy omega = *(this->getNetworkState(1, SpliningStdDev(1e5, 1e5), "omega")); // state
	SingleIndexNetworkDataProxy alpha = *(this->getNetworkState(2, SpliningStdDev(1e5, 1e5), "alpha")); // state/relax
	SingleIndexNetworkDataProxy ref = *(this->getNetworkInput(0, "ref")); // true input
	SingleIndexNetworkDataProxy timeBias = *(this->getNetworkInput(1, "timeBias"));
	SingleIndexNetworkDataProxy a = this->constant(60);
	SingleIndexNetworkDataProxy c = this->constant(0.8);
	SingleIndexNetworkDataProxy d = this->constant(300);
	SingleIndexNetworkDataProxy Kp = this->trainable(9.0/300.0, SpliningStdDev(1e-2, 1e-1), "Kp");
	SingleIndexNetworkDataProxy Kd = this->trainable(-6.0/300.0, SpliningStdDev(1e-2, 1e-1), "Kd");

	SingleIndexNetworkDataProxy e = (ref - theta);
	setAsStateUpdate(&(theta + constant(dt) * omega), SpliningStdDev(0.001, 0.001), "theta");
	setAsStateUpdate(&(omega + constant(dt) * alpha), SpliningStdDev(0.001, 0.001), "omega");
	//constant(0.0)*(-(sin(theta) * a + c * omega) + d * (Kp * e + Kd * omega))
	setAsStateUpdate(&(-(sin(theta) * a + c * omega)+d*(Kp*e+Kd*omega)),
			SpliningStdDev(50, 50), "alpha");
	//input needs to stiffen with time.
	//to speed things up, we can relax theta and omega in the beginning as well.
	setAsMeasurement(&(e * timeBias), SpliningStdDev(0.00125*400, 0.00125*400*500), "tracking error term"); // optimal control-desire zero1
	setAsMeasurement(&(Kp * e + Kd * omega), SpliningStdDev(1e3, 1e4), "control effort term"); // optimal control-desire zero
	setAsMeasurement(&(omega * timeBias), SpliningStdDev(1e3, 1e3), "omega magnitude"); // optimal control-desire zero
}
//OptimalPendulum::OptimalPendulum(NetworkStateVector& state, double dt) :
//		DyNetWithNames(state, 4, 1)
//{ // This can't help but solve the optimal control problem
//	SingleIndexNetworkDataProxy theta = *(this->getNetworkState(0, SpliningStdDev(1e4, 1e2), "theta")); // state
//	SingleIndexNetworkDataProxy omega = *(this->getNetworkState(1, SpliningStdDev(1e4, 1e2), "omega")); // state
//	SingleIndexNetworkDataProxy alpha = *(this->getNetworkState(2, SpliningStdDev(1e4, 1e2), "alpha")); // state/relax
//	SingleIndexNetworkDataProxy input = *(this->getNetworkState(3, SpliningStdDev(1e4, 1e2), "input")); // state/relax
//	SingleIndexNetworkDataProxy ref = *(this->getNetworkInput(0, "ref")); // true input
//	SingleIndexNetworkDataProxy a = this->constant(60);
//	SingleIndexNetworkDataProxy c = this->constant(0.8);
//	SingleIndexNetworkDataProxy d = this->constant(300);
//	SingleIndexNetworkDataProxy Kp = this->trainable(0.01, SpliningStdDev(1e-3, 1e-2), "Kp");
//	SingleIndexNetworkDataProxy Kd = this->trainable(-0.001, SpliningStdDev(1e-3, 1e-2), "Kd");
//	SingleIndexNetworkDataProxy e = (ref - theta);
//	setAsStateUpdate(&(theta + constant(dt) * omega), SpliningStdDev(1e-4, 1e-3), "theta");
//	setAsStateUpdate(&(omega + constant(dt) * alpha), SpliningStdDev(1e-4, 1e-3), "omega");
//	setAsStateUpdate(&(-(sin(theta) * a + c * omega) + d * input), SpliningStdDev(2e-4, 2e-3), "alpha");
//	setAsStateUpdate(&(Kp * e + Kd * omega), SpliningStdDev(1e-3, 1e-3), "input"); // continuity, relaxation
//	//input needs to stiffen with time.
//	//to speed things up, we can relax theta and omega in the beginning as well.
//	setAsMeasurement(&(e * constant(1.0)), SpliningStdDev(1e-4, 1e2), "tracking error term"); // optimal control-desire zero1
//	setAsMeasurement(&(omega * constant(1.0)), SpliningStdDev(1e-2, 1e4), "velocity term"); // optimal control-desire zero
//}
AlphaPendulumNet::AlphaPendulumNet(NetworkStateVector& state, double dt, double aGuess, double bGuess, double cGuess,
		double dGuess) :
		Network(state, 3)
{
	SingleIndexNetworkDataProxy theta = *(this->getNetworkInput(0));
	SingleIndexNetworkDataProxy omega = *(this->getNetworkInput(1));
	SingleIndexNetworkDataProxy input = *(this->getNetworkInput(2));

	SingleIndexNetworkDataProxy a = this->trainable(aGuess);
	SingleIndexNetworkDataProxy b = this->trainable(bGuess);
	SingleIndexNetworkDataProxy c = this->trainable(cGuess);
	SingleIndexNetworkDataProxy d = this->trainable(dGuess);

	SingleIndexNetworkDataProxy alpha = -(sin(theta) * a + sign(omega) * (cos(theta) * b + c * omega * omega))
			+ d * input;
	setAsOutput(&(alpha * constant(1.0)));
}
AlphaPendulumNetV2::AlphaPendulumNetV2(NetworkStateVector& state, double dt, double aGuess, double bGuess,
		double cGuess, double dGuess, double signStiffness) :
		Network(state, 3)
{
	SingleIndexNetworkDataProxy theta = *(this->getNetworkInput(0));
	SingleIndexNetworkDataProxy omega = *(this->getNetworkInput(1));
	SingleIndexNetworkDataProxy input = *(this->getNetworkInput(2));

	SingleIndexNetworkDataProxy a = this->trainable(aGuess);
	SingleIndexNetworkDataProxy b = this->trainable(bGuess);
	SingleIndexNetworkDataProxy c = this->trainable(cGuess);
	SingleIndexNetworkDataProxy d = this->trainable(dGuess);

	SingleIndexNetworkDataProxy alpha = -(sin(theta) * a + sign(omega, signStiffness) * b + c * omega) + d * input;
	setAsOutput(&(alpha * constant(1.0)));
}
void ConverterOfPendulumParameters::setOriginal(double tau, double xi, double epsion_plus_one, double inputScaling)
{
	this->tau = tau;
	this->xi = xi;
	this->epsilon_plus_one = epsilon_plus_one;
	this->inputScaling = inputScaling;
	a = 1.0 / (tau * tau * epsilon_plus_one);
	b = xi / (tau * tau * epsilon_plus_one);
	c = xi / (epsilon_plus_one);
	d = inputScaling / (tau * tau * epsilon_plus_one);
}
void ConverterOfPendulumParameters::setNew(double a, double b, double c, double d)
{

}

AltFullPendulumNet::AltFullPendulumNet(NetworkStateVector& state, double dt, double inputScalingGuess, double tauGuess,
		double xiGuess, double epsilon_plus_oneGuess, unsigned numberOfRepititions) :
		Network(state, 3)
{
	SingleIndexNetworkDataProxy theta = *(this->getNetworkInput(0));
	SingleIndexNetworkDataProxy omega = *(this->getNetworkInput(1));
	SingleIndexNetworkDataProxy input = *(this->getNetworkInput(2));

	SingleIndexNetworkDataProxy tau = this->trainable(tauGuess);
	SingleIndexNetworkDataProxy xi = this->trainable(xiGuess);
	SingleIndexNetworkDataProxy epsilon_plus_one = this->trainable(epsilon_plus_oneGuess);
	SingleIndexNetworkDataProxy inputScaling = this->trainable(inputScalingGuess);
	dt = dt / static_cast<double>(numberOfRepititions);
	for (unsigned i = 0; i < numberOfRepititions; i++)
	{
		SingleIndexNetworkDataProxy sign_omega = sign(omega);
		SingleIndexNetworkDataProxy coulombFrictionFromGravity = sign_omega * xi * cos(theta);
		SingleIndexNetworkDataProxy coulombFrictionFromCentrifugalAcceleration = sign_omega * xi * tau * tau * omega
				* omega;
		SingleIndexNetworkDataProxy inputForce = input * inputScaling;

		SingleIndexNetworkDataProxy alpha = -(sin(theta) + coulombFrictionFromCentrifugalAcceleration
				+ coulombFrictionFromGravity + inputForce) / (tau * tau * epsilon_plus_one);

		theta = (theta + constant(dt) * omega + constant(0.5 * dt * dt) * alpha);
		omega = (omega + constant(dt) * alpha);
		if (i + 1 >= numberOfRepititions)
		{
			setAsOutput(&(theta * constant(1.0)));
			setAsOutput(&(omega * constant(1.0)));
			setAsOutput(&(theta * constant(1.0)));
			setAsOutput(&(alpha * constant(1.0)));
		}
	}

}

PendulumNet::PendulumNet(NetworkStateVector& state) :
		Network(state, 2), state(state)
{
	SingleIndexNetworkDataProxy theta = *(this->getNetworkInput(0));
	SingleIndexNetworkDataProxy omega = *(this->getNetworkInput(1));

	SingleIndexNetworkDataProxy tau = this->trainable(0.101);
	SingleIndexNetworkDataProxy xi = this->trainable(0.013);

	SingleIndexNetworkDataProxy sign_omega = sign(omega);
	SingleIndexNetworkDataProxy coulombFrictionFromGravity = sign_omega * xi * cos(theta);
	SingleIndexNetworkDataProxy coulombFrictionFromCentrifugalAcceleration = sign_omega * xi * tau * tau * omega
			* omega;
	SingleIndexNetworkDataProxy alpha = -(sin(theta) + coulombFrictionFromCentrifugalAcceleration
			+ coulombFrictionFromGravity) / (tau * tau * this->trainable(2.33));
	setAsOutput(&alpha);

	/*	NoIndexNetworkDataProxy* gen = this->getGeneratorNetworkInputProxy();
	 SingleIndexNetworkDataProxy* theta = this->getNetworkInput(0);
	 SingleIndexNetworkDataProxy* omega = this->getNetworkInput(1);

	 TrainableScalarElement* tau = new TrainableScalarElement(state, gen, 0.101);
	 TrainableScalarElement* epsilon_plus_one = new TrainableScalarElement(state, gen, 2.33);
	 TrainableScalarElement* xi = new TrainableScalarElement(state, gen, 0.013);

	 ScalarSineOperator* sin_theta = new ScalarSineOperator(state, theta);
	 ScalarCosineOperator* cos_theta = new ScalarCosineOperator(state, theta);

	 ScalarMultiplicationElement* omega_squared = new ScalarMultiplicationElement(state);
	 omega_squared->addInput(omega);
	 omega_squared->addInput(omega);

	 ScalarSignumOperator* sign_omega = new ScalarSignumOperator(state, omega, 1e-6);

	 ScalarMultiplicationElement* tau_squared = new ScalarMultiplicationElement(state);
	 tau_squared->addInput(tau->getScalarResult());
	 tau_squared->addInput(tau->getScalarResult());

	 ScalarMultiplicationElement* coulombFrictionFromGravity = new ScalarMultiplicationElement(state);
	 coulombFrictionFromGravity->addInput(sign_omega->getScalarResult());
	 coulombFrictionFromGravity->addInput(cos_theta->getScalarResult());
	 coulombFrictionFromGravity->addInput(xi->getScalarResult());

	 ScalarMultiplicationElement* coulombFrictionFromCentrifugalAcceleration = new ScalarMultiplicationElement(state);
	 coulombFrictionFromCentrifugalAcceleration->addInput(sign_omega->getScalarResult());
	 coulombFrictionFromCentrifugalAcceleration->addInput(xi->getScalarResult());
	 coulombFrictionFromCentrifugalAcceleration->addInput(tau_squared->getScalarResult());
	 coulombFrictionFromCentrifugalAcceleration->addInput(omega_squared->getScalarResult());

	 ScalarAdditionElement* negativeNumerator = new ScalarAdditionElement(state);
	 negativeNumerator->addInput(sin_theta->getScalarResult());
	 negativeNumerator->addInput(coulombFrictionFromCentrifugalAcceleration->getScalarResult());
	 negativeNumerator->addInput(coulombFrictionFromGravity->getScalarResult());

	 ScalarNegationOperator* numerator = new ScalarNegationOperator(state, negativeNumerator->getScalarResult());

	 ScalarMultiplicationElement* denominator = new ScalarMultiplicationElement(state);
	 denominator->addInput(tau_squared->getScalarResult());
	 denominator->addInput(epsilon_plus_one->getScalarResult());

	 ScalarReciprocalOperator* reciprocalDenominator = new ScalarReciprocalOperator(state,
	 denominator->getScalarResult());

	 ScalarMultiplicationElement* alpha = new ScalarMultiplicationElement(state);
	 alpha->addInput(numerator->getScalarResult());
	 alpha->addInput(reciprocalDenominator->getScalarResult());

	 setAsOutput(alpha->getScalarResult());*/
}

void PendulumNet::testForward(string testName, double inputTheta, double inputOmega, double expectedAlpha)
{
	setInput(0, inputTheta);
	setInput(1, inputOmega);
	executeForwardNetwork();
	stringstream message;
	message << testName << "(" << inputTheta << ", " << inputOmega << ")";
	assertEquals(message.str(), expectedAlpha, getOutput(0), 1e-6);
	setOutputPartial(0, 1.0);
	executeGradientNetwork();
}
void PendulumNet::testTauPartial(string testName, double inputTheta, double inputOmega, double expectedAlpha,
		double expectedTauPartial)
{
	testForward("pendulumPartialToAlpha", inputTheta, inputOmega, expectedAlpha);
	stringstream message;
	message << testName << "(" << inputTheta << ", " << inputOmega << ")";
	double actualPartial = state.getPartialsWithRespectToTrainableData()[0];
	assertEquals(message.str(), expectedTauPartial, actualPartial, 1e-5);
}

}
}
}

