/*
 * PendulumNet.hpp
 *
 *    Created on: Nov 23, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef PENDULUMNET_HPP_
#define PENDULUMNET_HPP_

#include <string>

#include "Network.hpp"
#include "NetworkStateVector.hpp"

using namespace std;
using namespace uta::networkUtils;

namespace uta
{
namespace networkUtils
{
namespace pendulumDemo
{

class PendulumNet: public uta::networkUtils::Network
{
private:
	NetworkStateVector& state;
public:
	PendulumNet(NetworkStateVector& state);

	void testForward(string testName, double inputTheta, double inputOmega, double expectedAlpha);
	void testTauPartial(string testName, double inputTheta, double inputOmega, double expectedAlpha,
						double expectedTauPartial);
};
class FullPendulumNet: public uta::networkUtils::Network
{
public:
	FullPendulumNet(NetworkStateVector& state, double dt, double aGuess, double bGuess, double cGuess,
					double dGuess);
};

class FullPendulumNetV2: public uta::networkUtils::Network
{
public:
	FullPendulumNetV2(NetworkStateVector& state, double dt, double aGuess, double bGuess, double cGuess,
					double dGuess, double signStiffness);
};
class FullPendulumNetV3: public uta::networkUtils::Network
{
public:
	FullPendulumNetV3(NetworkStateVector& state, double dt, double aGuess, double bGuess, double cGuess,
					double dGuess, double signStiffness);
};

class NetworkLowPass: public uta::networkUtils::Network
{
public:
	NetworkLowPass(NetworkStateVector& state, double dt);
};
class NetworkGaussianFilter: public uta::networkUtils::Network
{
public:
	NetworkGaussianFilter(NetworkStateVector& state, double dt);
};
class PDPendulum: public DyNetWithNames
{
public:
	PDPendulum(NetworkStateVector& state, double dt);
};
class AlphaPendulumNet: public uta::networkUtils::Network
{
public:
	AlphaPendulumNet(NetworkStateVector& state, double dt, double aGuess, double bGuess, double cGuess,
					double dGuess);
};
class AlphaPendulumNetV2: public uta::networkUtils::Network
{
public:
	AlphaPendulumNetV2(NetworkStateVector& state, double dt, double aGuess, double bGuess, double cGuess,
					double dGuess, double signStiffness);
};
class ConverterOfPendulumParameters
{
public:
	double tau, xi, epsilon_plus_one, inputScaling;
	double a, b, c, d;
	void setOriginal(double tau, double xi, double epsion_plus_one, double inputScaling);
	void setNew(double a, double b, double c, double d);
};

class AltFullPendulumNet: public Network
{
public:
	AltFullPendulumNet(NetworkStateVector& state, double dt, double inputScalingGuess, double tauGuess,
						double xiGuess, double epsilon_plus_oneGuess, unsigned number);
};

}
}
}

#endif /* PENDULUMNET_HPP_ */
