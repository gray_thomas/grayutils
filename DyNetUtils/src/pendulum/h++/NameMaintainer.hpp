/*
 * NameMaintainer.hpp
 *
 *    Created on: Nov 25, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef NAMEMAINTAINER_HPP_
#define NAMEMAINTAINER_HPP_

#include <sstream>
#include <string>

using namespace std;
namespace uta
{
namespace networkUtils
{
namespace pendulumDemo
{
class NameMaintainer
{
public:
	NameMaintainer(string data_directory, string folderName)
	{
		verificationData << data_directory << folderName << "patterns.tst";
		trainingData << data_directory << folderName << "patterns.trn";
		trainingLog << data_directory << folderName << "learning.dat";
		networkTrainableWeights << data_directory << folderName << "network.wts";
		trainingDataRunsNameBase << data_directory << folderName << "dataRuns";
	}
	stringstream verificationData;
	stringstream trainingData;
	stringstream trainingLog;
	stringstream networkTrainableWeights;
	stringstream trainingDataRunsNameBase;
	string getVerificationDataFileName(void)
	{
		return verificationData.str();
	}
	string getTrainingDataFileName(void)
	{
		return trainingData.str();
	}
	string getTrainingLogFileName(void)
	{
		return trainingLog.str();
	}
	string getNetworkTrainableWeightsFileName(void)
	{
		return networkTrainableWeights.str();
	}
	string getTrainingDataRunsNameBase()
	{
		return trainingDataRunsNameBase.str();
	}
};
}
}
}

#endif /* NAMEMAINTAINER_HPP_ */
