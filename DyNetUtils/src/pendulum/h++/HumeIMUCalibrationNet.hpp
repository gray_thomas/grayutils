/*
 * HumeIMUCalibrationNet.hpp
 *
 *    Created on: Jan 20, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef HUMEIMUCALIBRATIONNET_HPP_
#define HUMEIMUCALIBRATIONNET_HPP_

#include <string>

#include "Network.hpp"
#include "NetworkStateVector.hpp"

using namespace std;
using namespace uta::networkUtils;

namespace uta
{
namespace networkUtils
{
namespace pendulumDemo
{

class HumeIMUCalibrationNet: public uta::networkUtils::Network
{
private:
	NetworkStateVector& state;
public:
	PendulumNet(NetworkStateVector& state);

	void testForward(string testName, double inputTheta, double inputOmega, double expectedAlpha);
	void testTauPartial(string testName, double inputTheta, double inputOmega, double expectedAlpha,
						double expectedTauPartial);
};


}
}
}


#endif /* HUMEIMUCALIBRATIONNET_HPP_ */
