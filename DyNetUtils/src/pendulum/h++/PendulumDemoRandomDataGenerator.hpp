/*
 * PendulumDemoRandomDataGenerator.hpp
 *
 *    Created on: Nov 25, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef PENDULUMDEMORANDOMDATAGENERATOR_HPP_
#define PENDULUMDEMORANDOMDATAGENERATOR_HPP_
#include <random>
#include <string>
#include <math.h>
using namespace std;
namespace uta
{
namespace networkUtils
{
namespace pendulumDemo
{

class PendulumDemoDirectTrainingDataGenerator
{
	double tau, eps_plus_one, xi, sigma;
	std::default_random_engine generator;
public:
	PendulumDemoDirectTrainingDataGenerator(double tau, double eps_plus_one, double xi, double sigma) :
			tau(tau), eps_plus_one(eps_plus_one), xi(xi), sigma(sigma), generator(2552)
	{

	}
	void generateRandomData(string fileName, unsigned amount)
	{

		normal_distribution<double> distribution(0, 2);
		normal_distribution<double> noise(0, sigma);
		double ins[2];
		double outs[1];
		DataSet dataSet(2, 1);
		for (unsigned i = 0; i < amount; i++)
		{
			ins[0] = distribution(generator);
			ins[1] = distribution(generator);
			outs[0] = expectedAlpha(ins[0], ins[1]) + noise(generator);

			dataSet.addLine(ins, outs);
		}
		dataSet.pushToFile(fileName);
	}
	double sign(double x)
	{
		return x > 0 ? 1 : -1;
	}
	double expectedAlpha(double theta, double omega)
	{
		return ((-sin(theta) - sign(omega) * xi * cos(theta) - sign(omega) * xi * tau * tau * omega * omega)
				/ (eps_plus_one * tau * tau));
	}
};

class FullPendulumDemoDirectTrainingDataGenerator
{
	double tau, eps_plus_one, xi, inputScale;
	default_random_engine generator;
public:
	FullPendulumDemoDirectTrainingDataGenerator(double tau, double eps_plus_one, double xi, double inputScale) :
			tau(tau), eps_plus_one(eps_plus_one), xi(xi), inputScale(inputScale), generator(2552)
	{

	}
	void generateRandomData2mem(string fileName, unsigned amount, double theta0, double omega0, double sigma_alpha,
			double sigma_input, double sigma_measure, unsigned minHold, unsigned maxHold, double dt,
			unsigned numberOfIntermediatePoints)
	{

		normal_distribution<double> distribution(0, sigma_input);
		uniform_int_distribution<unsigned> holdDuration(minHold, maxHold);
		normal_distribution<double> alphaNoise(0, sigma_alpha);
		normal_distribution<double> measureMentNoise(0, sigma_measure);
		double inputTheta, inputOmega, inputTorque;
		double outputTheta, outputOmega, outputMeasurement;
		DataSet dataSet(1, 1);
		outputTheta = theta0;
		outputOmega = omega0;
		dt = dt / numberOfIntermediatePoints;
		int holdCounter = 0;

		for (unsigned i = 0; i < amount; i++)
		{
			holdCounter--;
			if (holdCounter <= 0)
			{
				holdCounter = holdDuration(generator);
				inputTorque = distribution(generator);
			}
			for (unsigned j = 0; j < numberOfIntermediatePoints; j++)
			{
				inputTheta = outputTheta;
				inputOmega = outputOmega;
				double alpha = (expectedAlpha(inputTheta, inputOmega, inputTorque) + alphaNoise(generator));
				outputOmega += (dt * (alpha));
				outputTheta += (dt * inputOmega) + dt * dt * 0.5 * alpha;
			}
			outputMeasurement = outputTheta + measureMentNoise(generator);
			double outs[1] =
			{ outputMeasurement };
			double ins[1] =
			{ inputTorque };
			dataSet.addLine(ins, outs);
		}
		dataSet.pushToFile(fileName);
	}
	void generateRandomData1mem(string fileName, unsigned amount, double theta0, double omega0, double sigma_alpha,
			double sigma_input, double sigma_measure, unsigned minHold, unsigned maxHold, double dt,
			unsigned numberOfIntermediatePoints)
	{

		normal_distribution<double> distribution(0, sigma_input);
		uniform_int_distribution<unsigned> holdDuration(minHold, maxHold);
		normal_distribution<double> alphaNoise(0, sigma_alpha);
		normal_distribution<double> measureMentNoise(0, sigma_measure);
		double inputTheta, inputOmega, inputTorque;
		double outputTheta, outputOmega, outputMeasurement;
		DataSet dataSet(2, 2);
		outputTheta = theta0;
		outputOmega = omega0;
		dt = dt / numberOfIntermediatePoints;
		int holdCounter = 0;

		for (unsigned i = 0; i < amount; i++)
		{
			holdCounter--;
			if (holdCounter <= 0)
			{
				holdCounter = holdDuration(generator);
				inputTorque = distribution(generator);
			}
			for (unsigned j = 0; j < numberOfIntermediatePoints; j++)
			{
				inputTheta = outputTheta;
				inputOmega = outputOmega;
				double alpha = (expectedAlpha(inputTheta, inputOmega, inputTorque) + alphaNoise(generator));
				outputOmega += (dt * (alpha));
				outputTheta += (dt * inputOmega) + dt * dt * 0.5 * alpha;
			}
			outputMeasurement = outputTheta + measureMentNoise(generator);
			double outs[2] =
			{ outputOmega, outputMeasurement };
			double ins[2] =
			{ inputOmega, inputTorque };
			dataSet.addLine(ins, outs);
		}
		dataSet.pushToFile(fileName);
	}
	void generateRandomData0mem(string fileName, unsigned amount, double theta0, double omega0, double sigma_alpha,
			double sigma_input, double sigma_measure, unsigned minHold, unsigned maxHold, double dt,
			unsigned numberOfIntermediatePoints)
	{

		normal_distribution<double> distribution(0, sigma_input);
		uniform_int_distribution<unsigned> holdDuration(minHold, maxHold);
		normal_distribution<double> alphaNoise(0, sigma_alpha);
		normal_distribution<double> measurementNoise(0, sigma_measure);
		double inputTheta, inputOmega, inputTorque;
		double outputTheta, outputOmega, outputMeasurement;
		double alpha;
		DataSet dataSet(3, 4);
		outputTheta = theta0;
		outputOmega = omega0;
		dt = dt / numberOfIntermediatePoints;
		int holdCounter = 0;

		for (unsigned i = 0; i < amount; i++)
		{
			holdCounter--;
			if (holdCounter <= 0)
			{
				holdCounter = holdDuration(generator);
				inputTorque = distribution(generator);
			}
			for (unsigned j = 0; j < numberOfIntermediatePoints; j++)
			{
				inputTheta = outputTheta;
				inputOmega = outputOmega;
				alpha = (expectedAlpha(inputTheta, inputOmega, inputTorque) + alphaNoise(generator));
				outputOmega += (dt * (alpha));
				outputTheta += (dt * inputOmega) + dt * dt * 0.5 * alpha;
			}
			outputMeasurement = outputTheta + measurementNoise(generator);
			double outs[4] =
			{ outputTheta, outputOmega, outputMeasurement, alpha };
			double ins[3] =
			{ inputTheta, inputOmega, inputTorque };
			dataSet.addLine(ins, outs);
		}
		dataSet.pushToFile(fileName);
	}

	double sign(double x)
	{
		return tanh(1e6 * x);
	}
	double expectedAlpha(double theta, double omega, double input)
	{

		return (-(sin(theta) + sign(omega) * xi * cos(theta) + sign(omega) * xi * tau * tau * omega * omega
				+ inputScale * input) / (eps_plus_one * tau * tau));
	}
};
//#include "Network.hpp"
//class DataGenerator
//{
//	default_random_engine generator;
//	Network* network;
//public:
//	DataGenerator(Network* network) :
//			network(network), generator(2552)
//	{
//
//	}
class PendulumABCDDataGenerator
{
	double a,b,c,d;
	default_random_engine generator;
public:
	PendulumABCDDataGenerator(double a, double b, double c, double d) :
			a(a),b(b),c(c),d(d), generator(2552)
	{

	}
	void generateRandomData0mem(string fileName, unsigned amount, double theta0, double omega0, double sigma_alpha,
			double sigma_input, double sigma_measure, unsigned minHold, unsigned maxHold, double dt,
			unsigned numberOfIntermediatePoints)
	{

		normal_distribution<double> distribution(0, sigma_input);
		uniform_int_distribution<unsigned> holdDuration(minHold, maxHold);
		normal_distribution<double> alphaNoise(0, sigma_alpha);
		normal_distribution<double> measurementNoise(0, sigma_measure);
		double inputTheta, inputOmega, inputTorque;
		double outputTheta, outputOmega, outputMeasurement;
		double alpha;
		DataSet dataSet(3, 1);
		outputTheta = theta0;
		outputOmega = omega0;
		dt = dt / numberOfIntermediatePoints;
		int holdCounter = 0;

		for (unsigned i = 0; i < amount; i++)
		{
			holdCounter--;
			if (holdCounter <= 0)
			{
				holdCounter = holdDuration(generator);
				inputTorque = distribution(generator);
			}
			for (unsigned j = 0; j < numberOfIntermediatePoints; j++)
			{
				inputTheta = outputTheta;
				inputOmega = outputOmega;
				alpha = (expectedAlpha(inputTheta, inputOmega, inputTorque) + alphaNoise(generator));
				outputOmega += (dt * (alpha));
				outputTheta += (dt * inputOmega) + dt * dt * 0.5 * alpha;
			}
			outputMeasurement = outputTheta + measurementNoise(generator);
			double outs[1] =
			{  alpha };
			double ins[3] =
			{ inputTheta, inputOmega, inputTorque };
			dataSet.addLine(ins, outs);
		}
		dataSet.pushToFile(fileName);
	}

	double sign(double x)
	{
		return tanh(1e6 * x);
	}
	double expectedAlpha(double theta, double omega, double input)
	{

		return -(sin(theta) * a + sign(omega) * b + c * omega)
				+ d * input;
	}
};
}
}
}

#endif /* PENDULUMDEMORANDOMDATAGENERATOR_HPP_ */
