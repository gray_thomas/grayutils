/*
 * SEANetwork.hpp
 *
 *  Created on: Mar 25, 2014
 *      Author: Gray
 */

#ifndef SEANETWORK_HPP_
#define SEANETWORK_HPP_

#include <Network.hpp>
namespace uta
{
namespace demos
{
namespace SEA
{
using namespace networkUtils;

class SEANetwork: public Network
{
public:
	SEANetwork(NetworkStateVector& stateVector);
};
class FullSEA: public DyNetWithNames
{
public:
	FullSEA(NetworkStateVector& state, double dt);
};
class SEALowPass: public DyNetWithNames
{
public:
	SEALowPass(NetworkStateVector& state, double dt);
};
class PositionDeflectionSEA: public DyNetWithNames
{
public:
	PositionDeflectionSEA(NetworkStateVector& state, double dt);
};
}
}
}

#endif /* SEANETWORK_HPP_ */
