/*
 * TestPendulumNetDemo.cpp
 *
 *    Created on: Nov 23, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include "PendulumNet.hpp"
#include "TestPendulumNetDemo.hpp"
#include "TestMethods.hpp"
#include "TestUtils.hpp"
#include "TestOperators.hpp"

#include <cmath>
#include <iostream>
#include <memory>
#include <new>
#include <sstream>
#include <string>
#include <vector>
#include <random>

#define PI 3.14159265395

using namespace std;
using namespace uta::networkUtils;
using namespace uta::networkUtils::pendulumDemo;
using namespace uta::testUtils;
namespace uta
{
namespace networkUtils
{
namespace tests
{

double sign(double x)
{
	return x > 0 ? 1 : -1;
}
double signum(double x)
{
	return tanh(1e6 * x);
}

double testAlpha(double theta, double omega)
{

	double tau = 0.101;
	double epsilon_plus_one = 2.33;
	double xi = 0.013;

	double sin_theta = sin(theta);
	double cos_theta = cos(theta);

	double omega_squared = 1;
	omega_squared *= (omega);
	omega_squared *= (omega);

	double sign_omega = signum(omega);

	double tau_squared = 1;
	tau_squared *= tau;
	tau_squared *= tau;

	double coulombFrictionFromGravity = 1;
	coulombFrictionFromGravity *= sign_omega;
	coulombFrictionFromGravity *= cos_theta;
	coulombFrictionFromGravity *= xi;

	double coulombFrictionFromCentrifugalAcceleration = 1;
	coulombFrictionFromCentrifugalAcceleration *= sign_omega;
	coulombFrictionFromCentrifugalAcceleration *= xi;
	coulombFrictionFromCentrifugalAcceleration *= tau_squared;
	coulombFrictionFromCentrifugalAcceleration *= omega_squared;

	double negativeNumerator = 0;
	negativeNumerator += sin_theta;
	negativeNumerator += coulombFrictionFromCentrifugalAcceleration;
	negativeNumerator += coulombFrictionFromGravity;

	double numerator = -negativeNumerator;

	double denominator = 1;
	denominator *= tau_squared;
	denominator *= epsilon_plus_one;

	double reciprocalDenominator = 1 / denominator;

	double alpha = 1;
	alpha *= numerator;
	alpha *= reciprocalDenominator;

	return alpha;
}

double expectedAlpha(double theta, double omega)
{
	double tau = 0.101;
	double epsilon_plus_one = 2.33;
	double xi = 0.013;

	return ((-sin(theta) - sign(omega) * xi * cos(theta) - sign(omega) * xi * tau * tau * omega * omega)
			/ (epsilon_plus_one * tau * tau));
}
double expectedPartialOfAlphaWithRespectToTau(double theta, double omega)
{
	double tau = 0.101;
	double epsilon_plus_one = 2.33;
	double xi = 0.013;
	return 2 / (tau * tau * tau) * (sin(theta) + sign(omega) * xi * cos(theta)) / (epsilon_plus_one);
}
static void pendulumMemTest(void)
{
	NetworkStateVector state;
	PendulumNet* pendulumNet = new PendulumNet(state);
//	don't delete pendulumNet; pendulum is automatically cleaned up when state leaves scope
}

void pendulumTest(void)
{
	cout << "-------Pendulum Network Test: -----------------------" << endl;
	cout<< "===Pendulum Mem Test==="<<endl;
	pendulumMemTest();
	cout<< "===Pendulum Mem Test Success!==="<<endl;
	NetworkStateVector state;
	PendulumNet* pendulumNet = new PendulumNet(state);
	state.print();
	pendulumNet->print();
	pendulumNet->testForward("pendulum", 0, 0, 0);
	pendulumNet->testForward("pendulum", 0, 1, expectedAlpha(0, 1));
	assertEquals("two expected alphas agree", testAlpha(0.0256091, -1.01637), expectedAlpha(0.0256091, -1.01637),
			1e-6);

	default_random_engine generator(234012);
	normal_distribution<double> distribution(0, 1);
	for (int i = 0; i < 30; i++)
	{
		double theta = distribution(generator);
		double omega = distribution(generator);
		pendulumNet->testForward("pendulum", theta, omega, testAlpha(theta, omega));
		pendulumNet->testTauPartial("pendulumTauPartial", theta, omega, testAlpha(theta, omega),
				expectedPartialOfAlphaWithRespectToTau(theta, omega));
	}

	cout << "-------Pendulum Network Test Completed Successfully--" << endl;
}

}
}
}
