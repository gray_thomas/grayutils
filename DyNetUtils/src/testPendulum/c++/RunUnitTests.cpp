/*
 * RunUnitTests.cpp
 *
 *    Created on: Nov 19, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include <iostream>
#include "TestMethods.hpp"
#include "TestOperators.hpp"
#include "TestGradientDescentTrainer.hpp"
#include "TestBackPropThroughTime.hpp"
#include "TestDampedLeastSquares.hpp"
#include "TestStateTrainer.hpp"
#include "DyNetPlotterTest.hpp"

int main(int argc, const char* argv[])
{
	std::cout << "Tests, hey hey hey. Gray_utils_root =" << "GRAY_UTILS_ROOT" << std::endl;

	uta::networkUtils::tests::additionTest();
	uta::networkUtils::tests::multiplicationTest();
	uta::networkUtils::tests::trainableScalarTest();
	uta::networkUtils::tests::networkBuiltInConstructorTest();
	uta::networkUtils::tests::operatorTest();
	uta::networkUtils::tests::pendulumTest();
	uta::networkUtils::tests::networkVectorizedInputOutputTest();
	uta::networkUtils::tests::gradientDescentTrainerTest();
	uta::networkUtils::tests::backPropThroughTimeTest();
	uta::networkUtils::tests::leastSquaresTrainerTest();
	uta::networkUtils::tests::testCorrectnessOfForwardIterativeBackPropThroughTime();
	std::cout<<"hey"<<std::endl;
	uta::networkUtils::tests::floatingMemoryTrainerTest();
	uta::networkUtils::dyNetUtils::tests::testSparseHessianSolver();
	uta::networkUtils::dyNetUtils::tests::testDyNetPlotter();
	std::cout << "Unit testing for networkUtils library complete! Well done sir!" << std::endl;
	return 0;
}
