/*
 * TestPendulumNetDemo.hpp
 *
 *    Created on: Nov 23, 2013
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#ifndef TESTPENDULUMNETDEMO_HPP_
#define TESTPENDULUMNETDEMO_HPP_

namespace uta
{
namespace networkUtils
{
namespace tests
{

double sign(double x);
double signum(double x);
double testAlpha(double theta, double omega);
double expectedAlpha(double theta, double omega);
double expectedPartialOfAlphaWithRespectToTau(double theta, double omega);
void pendulumTest(void);

}
}
}


#endif /* TESTPENDULUMNETDEMO_HPP_ */
