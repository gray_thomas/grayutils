import pickle as pkl
import io
from urllib2 import urlopen
class MemoizedHTML(object):
	def __init__(self):
		# self.dict={}
		try:
			with open('memoization.pkl','r') as fil:
				self.dict=pkl.load(fil)
		except Exception as e:	
			print e.what()
			self.dict={}

	def clear(self):
		self.dict={}

	def save(self):
		with open('memoization.pkl','w') as fil:
			pkl.dump(self.dict,fil)


	def fetch_html(self,location):
		try:
			return self.dict[location]
		except KeyError:
			self.dict[location]=urlopen(location).read()
			return self.dict[location]

if __name__=="__main__":
	mem=MemoizedHTML()
	BASE_URL = "http://usnews.rankingsandreviews.com"
	example="/cars-trucks/Mitsubishi_Mirage/2015/"
	"<span>MSRP:</span>"
	html = mem.fetch_html(BASE_URL+example)
	# mem.clear()
	mem.save()
