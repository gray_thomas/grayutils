import re
from AllCars import all_cars
compact_cars=[
"Accent",
"Civic",
"Corolla",
"Cruze",
"Dart",
"Elantra",
"Fiesta",
"Fit",
"Focus",
"Forte",
"Golf",
"Impreza",
"Jetta",
"Lancer",
"Mazda2",
"Mazda3",
"Mirage",
"Prius",
"Rao",
"Sentra",
"Sonic",
"Spark",
"Versa",
"xD",
"Yarris"]

pds={}

for car_name in compact_cars:
	years={}
	# print car_name
	for i in range(0,len(all_cars)):
		if all_cars[i][2]==car_name:
			# print all_cars[i]
			years[all_cars[i][0]]=all_cars[i][4]
	pds[car_name]=years
# print pds

from bs4 import BeautifulSoup

from memoized_html import MemoizedHTML
mem=MemoizedHTML()
BASE_URL = "http://usnews.rankingsandreviews.com"
example="/cars-trucks/Toyota_Prius/2010/"
example="/cars-trucks/Toyota_Prius/2009/"
"<span>MSRP:</span>"
html = mem.fetch_html(BASE_URL+example)
# mem.clear()

# print html
def extract_overall_score(soupin):
	num= float(soupin.find_all('span')[3].decode_contents())
	assert 10>=num
	assert 0<num
	return num
def extract_score(stringin):
	return float(stringin.decode_contents())
def extract_label(stringin):
	return str(stringin.decode_contents())
matcher=re.compile(r"circle-(\d)_(\d)")
def extract_reliability_score(stringin):
	gs=matcher.search((table_entries[13].find_all('span')[0]).decode()).groups()
	num=(float(gs[0])+0.1*float(gs[1]))*2.0
	assert 10>=num
	assert 0<num
	return num

main_page_soup = BeautifulSoup(html, "lxml")
table_entries=main_page_soup.find('div',id='center-rail').find_all('td')
subsoup_overall=main_page_soup.find('div',id='center-rail').find_all('td')[1].find_all('span')[3]

print extract_label(table_entries[0])
print extract_overall_score(table_entries[1])#overall

print extract_label(table_entries[2])
print extract_score(table_entries[3])# critic's rating

print extract_label(table_entries[4])
print extract_score(table_entries[5])# performance

print extract_label(table_entries[6])
print extract_score(table_entries[7])# interior

print extract_label(table_entries[8])
print extract_score(table_entries[9])# cost to own

print extract_label(table_entries[10])
print extract_score(table_entries[11])# safety

print extract_label(table_entries[12])
print extract_reliability_score(table_entries[13]) # reliability

print '\n--\n'.join([item.decode() for item in main_page_soup.find('div',id='product-summary').find_all('div')])

mem.save()
# print soup.find('div',id='center-rail')
# print soup.find('div',clas='scores sidebar-section')
# print len( soup.find('div',clazz='scores sidebar-section'))
def get_category_links(section_url):
    html = urlopen(section_url).read()
    soup = BeautifulSoup(html, "lxml")
    boccat = soup.find("dl", "boccat")
    category_links = [BASE_URL + dd.a["href"] for dd in boccat.findAll("dd")]
    return category_links