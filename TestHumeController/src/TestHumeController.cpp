//============================================================================
// Name        : TestHumeController.cpp
// Author      : Gray Thomas
// Version     :
// Copyright   : All rights reserved
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "HumeController.hpp"
#include <unistd.h>
#include <random>
#include <NetworkUtils/src/test/h++/TestUtils.hpp>
#include "HumeLTIFilters.hpp"
#include "JoystickReceiver.hpp"
#include "HumeJoystickController.hpp"

#include <vector>
using namespace std;
using namespace uta::hume;
using namespace uta::testUtils;

void testKinematics()
{
	cout << "Testing Kinematics" << endl;
	SimpleKinematics kinematics;
	kinematics.setDefaultParameters();
	SimpleKinematicObjective objective;
	double jointAngles[2][3];
	objective.leftFootDistanceAheadOfBody = 000;
	objective.rightFootDistanceAheadOfBody = 300;
	objective.leftFootDistanceLeftOfBody = 200;
	objective.rightFootDistanceLeftOfBody = -200;
	objective.hipHeight = 740;
	objective.forwardLeanAngleRadians = -0.3;
	objective.bodyRollAboutTheForwardDirection = 0.00;
	kinematics.getJointAnglesFromPlanarCOMRelativeToStanceAndTwoTiltPlanes(objective, jointAngles);
	cout << "joint angles : [   ";
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 3; j++)
			cout << jointAngles[i][j] << "  ";
	cout << " ]" << endl;
	short jointEncoders[2][3];
	double jointFractions[2][3];
	SimpleCalibration calibration;
	calibration.getEncodersFromJointAngles(jointAngles, jointEncoders);
	calibration.getFractionsFromJointAngles(jointAngles, jointFractions);
	cout << "encoders : [   ";
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 3; j++)
			cout << jointEncoders[i][j] << "(" << jointFractions[i][j] << ")" << "  ";
	cout << " ]" << endl;
}
void testShorts()
{
	cout << hex << sh(-0.5) << endl;
	cout << hex << sh(0.5) << endl;
	cout << hex << sh(2) << endl;
	cout << hex << sh(-2) << endl;
	cout << hex << sh(0) << endl;
}

void testOutputOverTime()
{
	//setup
	HumeController controller;
	M3HumeEmbeddedStatus* statuses[2][3];
	M3HumeEmbeddedCommand* commands[2][3];
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 3; j++)
		{
			statuses[i][j] = new M3HumeEmbeddedStatus();
			commands[i][j] = new M3HumeEmbeddedCommand();
		}
	message message;
	for (int i = 0; i < 20; i++)
	{
		HumeIMU imu;
		controller.doControl(statuses, message, imu, commands);
		for (int j = 0; j < 2; j++)
			for (int k = 0; k < 3; k++)
				cout << "Command for Joint " << j << " " << k << " : " << *(commands[j][k]) << endl;

		usleep(100);
	}
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 3; j++)
		{
			delete statuses[i][j];
			delete commands[i][j];
		}
}

void testCalibration()
{
	SimpleCalibration calibration;
	short encoderMin[2][3] =
	{
	{ 0, 50, -500 },
	{ 0, 50, -500 } };
	short encoderMax[2][3] =
	{
	{ 100, 150, 500 },
	{ 100, 150, 500 } };
	double valuesMin[2][3] =
	{
	{ 0.0, -1.0, -100.0 },
	{ -1.0, -1.0, -100.0 } };
	double valuesMax[2][3] =
	{
	{ 1.0, 1.0, 100.0 },
	{ 0.0, 0.0, 000.0 } };
	calibration.setJointEncoderParameters(valuesMin, valuesMax, encoderMin, encoderMax);
	short encoders[2][3];
	double values[2][3] =
	{
	{ 0.5, 1.0, 100.0 },
	{ -0.5, -1.0, 100.0 } };
	short expected[2][3] =
	{
	{ 50, 150, 500 },
	{ 50, 50, 500 } };
	calibration.getEncodersFromJointAngles(values, encoders);
	cout << dec;
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 3; j++)
			assertEquals("encoder calibration should work", expected[i][j], encoders[i][j], 1e-4);
	Matrix<double, 2, 3> torques;
	torques << 1.0, 2.0, -20.0, 20.0, 20.0, 2.0;
	cout << "Torques are \n" << torques << "\n\tand coressponding spring enoders are\n"
			<< calibration.getSpringEncodersFromTorques(torques) << endl;
}

void testPlotting()
{
	HumeLogger logger;
	logger.setupConsoleLogging();
	cout << "plotting test" << endl;
	HumeIMU exmpleIMU;
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			exmpleIMU.orientation[i][j] = (10 * i + j) * 1 + 1000.01;
	for (int j = 0; j < 3; j++)
		exmpleIMU.accelerometer[j] = (j) * 1 + 2000.01;
	for (int j = 0; j < 3; j++)
		exmpleIMU.angularVelocity[j] = (j) * 1 + 3000.01;
	for (int j = 0; j < 3; j++)
		exmpleIMU.magnetometer[j] = (j) * 1 + 4000.01;
	cout << exmpleIMU << endl;

	message exampleMessage;
	exampleMessage.index = 1337;
	exampleMessage.validBits = 0x7e000000;
	for (int i = 0; i < 7; i++)
	{
		exampleMessage.x[i] = i * 10 + 1000.001;
		exampleMessage.y[i] = i * 10 + 1001.001;
		exampleMessage.z[i] = i * 10 + 1002.001;
	}

	cout << exampleMessage << endl;
	NewM3HumeEmbeddedControlReport exampleReport;
	exampleReport.controlTicIndex = 42;
	exampleReport.rawOutCommandedMotorDAC = 10;
	exampleReport.raw1MotorEncoder = 20;
	exampleReport.raw2JointEncoder = 30;
	exampleReport.raw3SpringEncoder = 40;
	cout << exampleReport << endl;
	M3HumeEmbeddedStatus exampleStatus[2][3];
	M3HumeEmbeddedStatus* exampleStatusPtr[2][3];
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 3; j++)
		{
			exampleStatusPtr[i][j] = &exampleStatus[i][j];
			for (int q = 0; q < 2; q++)
			{
				exampleStatus[i][j].reports[q].controlTicIndex = (q + i + j) % 2 + 10 * i + j;
				exampleStatus[i][j].reports[q].rawOutCommandedMotorDAC = 1000 + 100 * i + 10 * j + q;
				exampleStatus[i][j].reports[q].raw1MotorEncoder = 2000 + 100 * i + 10 * j + q;
				exampleStatus[i][j].reports[q].raw2JointEncoder = 3000 + 100 * i + 10 * j + q;
				exampleStatus[i][j].reports[q].raw3SpringEncoder = 4000 + 100 * i + 10 * j + q;
			}
		}

	logger.doLogging(exampleStatusPtr, exampleMessage, exmpleIMU);
	logger.setupLogging("/home/hcrl/wk/grayutils/Demos/data/testLogger.dat");
	logger.doLogging(exampleStatusPtr, exampleMessage, exmpleIMU);
	logger.doLogging(exampleStatusPtr, exampleMessage, exmpleIMU);
}
void testFrictionBiasGainEstimatorUpdate()
{
	FrictionBiasGainEstimator estimator;
	estimator.setup(0.5, 0.01);
	double gain = 0.5;
	double friction = 0.3;
	double bias = 2.0;
	double DAC = 2.0;
	double velocity = 1.0;
	cout << "\nFrictionBiasGain Estimator test!\n";
	MatrixXd actual;
	MatrixXd expected;
	expected.setZero(3, 1);
	expected << gain, friction, bias;
	for (int i = 0; i < 18; i++)
	{
		velocity = 1 - 3 * (i % 2);
		DAC = 3 * i % 20 - 2 * i % 3 + 3;
		actual = estimator.update(bias + gain * DAC + friction * signum(velocity), velocity, DAC);
		cout << actual.transpose() << endl;
//		cout << estimator.debuge()<<endl;
//		cout << estimator.debugB()<<endl;
	}

	assertEqualMatrices("Friction Bias Gain Vector", expected, actual, 1e-4);
	friction = 0.7;
	for (int i = 0; i < 10; i++)
	{
		velocity = 1 - 3 * (i % 2);
		DAC = 3 * i % 20 - 2 * i % 3 + 3;
		cout << estimator.update(bias + gain * DAC + friction * signum(velocity), velocity, DAC).transpose() << endl;
		//		cout << estimator.debuge()<<endl;
		//		cout << estimator.debugB()<<endl;
	}
	friction = 0.3;
	bias = 4.0;
	gain = 0.8;
	for (int i = 0; i < 10; i++)
	{
		velocity = 1 - 3 * (i % 2);
		DAC = 3 * i % 20 - 2 * i % 3 + 3;
		cout << estimator.update(bias + gain * DAC + friction * signum(velocity), velocity, DAC).transpose() << endl;
		//		cout << estimator.debuge()<<endl;
		//		cout << estimator.debugB()<<endl;
	}
}
struct SliderParams
{
	double mass;
	double damping;
	double friction;
	double stictionThreshold;
};
struct SEAParams
{
	SliderParams motor;
	SliderParams output;
	double torquePerUnitInput;
	double springConstant;
	double transmissionRatioMotorToSpring;
	double transmissionRatioSpringToOutput;
	double springDamping;
};
double signlog(double in)
{
	return atan(in * 1e-2);
}
VectorXd AccelSEA(VectorXd& state, SEAParams& sea, double input, double bias)
{
	double o = state[0];
	double od = state[1];
	double x = state[2];
	double xd = state[3];
	double mu = sea.springDamping;
	double springDeflection = sea.transmissionRatioMotorToSpring * o - x / sea.transmissionRatioSpringToOutput;
	double springDeflectionDot = sea.transmissionRatioMotorToSpring * od - xd / sea.transmissionRatioSpringToOutput;
	double springTorque = sea.springConstant * springDeflection;
	double springDamping = mu * springDeflectionDot;
	double inputTorque = sea.torquePerUnitInput * input;
	double motorFrictionTorque = sea.motor.friction * signlog(od);
	double motorDampingTorque = sea.motor.damping * od;
	double motorSpringTorque = sea.transmissionRatioMotorToSpring * (springTorque + springDamping); //0.006355 = K*q*T1/M
	double odd = (inputTorque - motorFrictionTorque - motorDampingTorque - motorSpringTorque) / sea.motor.mass;
	if (abs(od) < 10)
		if (abs(odd) < sea.motor.stictionThreshold)
			odd = -1000.0 * od;
	double outputFriction = sea.output.friction * signlog(xd);
	double outputDamping = sea.output.damping * xd;
	double outputSpringTorque = (springTorque + springDamping) / sea.transmissionRatioSpringToOutput;
	double xdd = (bias - outputFriction - outputDamping + outputSpringTorque) / sea.output.mass;
	VectorXd ret;
	ret.setZero(4);
	ret << od, odd, xd, xdd;
	return ret;
}
class RungeKutta
{
	MatrixXd table;
	MatrixXd Bs;
	MatrixXd ks;
	MatrixXd ys;
	int stateSize;
	int patternSize;
	bool firstIter;
	bool lastIsFirst;
public:
	void size()
	{
		table.setZero(patternSize - 1, patternSize - 1);
		Bs.setZero(1, patternSize);
		ks.setZero(stateSize, patternSize);
		ys.setZero(stateSize, patternSize);
		firstIter = false;
	}
	void setDormandPrince(unsigned stateSize)
	{
		this->stateSize = stateSize;
		lastIsFirst = true;
		patternSize = 7;
		size();

		table << (1.0 / 5.0), (0.0), (0.0), (0.0), (0.0), (0.0), //
		(3.0 / 40.0), (9.0 / 40.0), (0.0), (0.0), (0.0), (0.0), //
		(44.0 / 45.0), (-56.0 / 15.0), (32.0 / 9.0), (0.0), (0.0), (0.0), //
		(19372.0 / 6561.0), (-25360.0 / 2187.0), (64448.0 / 6561.0), (-212.0 / 729.0), (0.0), (0.0), //
		(9017.0 / 3168.0), (-355.0 / 33.0), (46732.0 / 5247.0), (49.0 / 176.0), (-5103.0 / 18656.0), (0.0), //
		(35.0 / 384.0), (0.0), (500.0 / 1113.0), (125.0 / 192.0), (-2187.0 / 6784.0), (11.0 / 84.0);
		Bs.resize(2, patternSize);
		Bs << (5179.0 / 57600.0), (0.0), (7571.0 / 16695.0), (393.0 / 640.0), //
		(-92097.0 / 339200.0), (187.0 / 2100.0), (1.0 / 40.0), // 4th order solution
		(35.0 / 384.0), (0.0), (500.0 / 1113.0), (125.0 / 192.0), //
		(-2187.0 / 6784.0), (11.0 / 84.0), 0.0; // 5th order solution
	}
	void setRK4(unsigned stateSize)
	{
		// ys of size 5
		// ks of size 4
		patternSize = 4;
		lastIsFirst = false;
		this->stateSize = stateSize;
		size();
		table << 0.5, 0.0, 0.0, //
		0.0, 0.5, 0.0, //
		0.0, 0.0, 1.0; //
		Bs.resize(1, patternSize);
		Bs << (1.0 / 6.0), (1.0 / 3.0), (1.0 / 3.0), (1.0 / 6.0);
	}
	template<typename Func>
	MatrixXd evaluate(VectorXd state, Func diff, double dt)
	{
		ys.col(0) = state;
		if (lastIsFirst && !firstIter)
			ks.col(0) = ks.col(patternSize - 1); //TODO: stepsize must not change.
		else
			ks.col(0) = dt * diff(VectorXd(ys.col(0)));
		for (int i = 1; i < patternSize; i++)
		{
			ys.col(i) = ys.col(i - 1);
			for (int j = 0; j < i - 1; j++)
				ys.col(i) += table(i - 1, j) * ks.col(j);
			ks.col(i) = dt * diff(VectorXd(ys.col(i)));
		}
		firstIter = false;
		MatrixXd ret(stateSize, Bs.rows());
		for (int i = 0; i < Bs.rows(); i++)
			ret.col(i) = ys.col(0);
		return ret + ks * Bs.transpose();
	}

};

VectorXd& SimSEA(VectorXd& state, SEAParams& sea, double input, double bias, double dt)
{
	double o = state[0];
	double od = state[1];
	double x = state[2];
	double xd = state[3];
	double springDeflection = sea.transmissionRatioMotorToSpring * o - x / sea.transmissionRatioSpringToOutput;
	double springTorque = sea.springConstant * springDeflection;
	double inputTorque = sea.torquePerUnitInput * input;
	double motorSpringTorque = sea.transmissionRatioMotorToSpring * springTorque; //0.006355 = K*q*T1/M
	double outputSpringTorque = springTorque / sea.transmissionRatioSpringToOutput;
	VectorXd k1 = dt * AccelSEA(state, sea, input, bias);
	VectorXd y1 = state + 1.0 / 3.0 * k1;
	VectorXd k2 = dt * AccelSEA(y1, sea, input, bias);
	VectorXd y2 = state - 1.0 / 3.0 * k1 + 1.0 * k2;
	VectorXd k3 = dt * AccelSEA(y2, sea, input, bias);
	VectorXd y3 = state + 1.0 * k1 - 1.0 * k2 + 1.0 * k3;
	VectorXd k4 = dt * AccelSEA(y3, sea, input, bias);
	state += 0.125 * k1 + 0.375 * k2 + 0.375 * k3 + 0.125 * k4; // Runge Kutta fourth order.

//	if (od == 0.0 or signum((double) state[1]) != signum(od))
//	{
//		if (abs(inputTorque - motorSpringTorque) < sea.motor.stictionThreshold)
//			state[1] = 0;
//	}
//	if (xd == 0.0 or signum((double) state[3]) != signum(xd))
//	{
//		if (abs(bias + outputSpringTorque) < sea.output.stictionThreshold)
//			state[3] = 0;
//	}

	return state;
}

double seaGain(SEAParams& sea)
{
	return sea.torquePerUnitInput
			/ (sea.transmissionRatioMotorToSpring * sea.transmissionRatioSpringToOutput * sea.output.mass);
}

double seaOuputMassForNaturalSpringFrequency(SEAParams& sea, double frequency)
{
	double w = frequency;
	double T2 = sea.transmissionRatioSpringToOutput;
	double T1 = sea.transmissionRatioMotorToSpring;
	double I = sea.motor.mass;
	double K = sea.springConstant;

	return 1.0 / ((pow(w, 2) / K - pow(T1, 2) / I) * (pow(T2, 2)));
}

SEAParams getDefaultRightKneeModel(void)
{
	SEAParams sea;
	sea.motor.mass = 1.0;
	double uptpt = 2000 * 2000;	// units_of_per_tic_per_tic
	sea.motor.damping = 0.000798 * sea.motor.mass * 2000;	//0.000798 = B/I in units_of_per_tic_per_tic *tic
	sea.motor.friction = 0.4 * sea.motor.mass * uptpt;	//0.265531 = F/I
	sea.transmissionRatioMotorToSpring = 0.028634;
	sea.transmissionRatioSpringToOutput = 1.0 / 1.581332;

	sea.springConstant = 0.006355 * sea.motor.mass * uptpt / sea.transmissionRatioMotorToSpring; //0.006355 = K*q*T1/I
	sea.torquePerUnitInput = 0.007972 * sea.motor.mass * uptpt; // 0.007972=Kt/I
	sea.motor.stictionThreshold = 50 * sea.torquePerUnitInput; // lets say 50 dac units equivalent

	sea.output.mass = seaOuputMassForNaturalSpringFrequency(sea, 20 * 2 * 3.141592653589793);
	cout << "output mass is " << sea.output.mass << endl;
	;
	sea.output.damping = 0.000681 * sea.output.mass * 2000; // 0.000681 = b/M
	sea.output.friction = 0.003735 * sea.output.mass * uptpt; // 0.003735 = f/M
	sea.output.stictionThreshold = 0.0;
	double lambda = 0.1;
	sea.springDamping = lambda * sea.output.damping * pow(sea.transmissionRatioSpringToOutput, 2);
	sea.output.damping = sea.output.damping - pow(sea.transmissionRatioSpringToOutput, -2) * sea.springDamping;
	sea.motor.damping = sea.motor.damping - pow(sea.transmissionRatioMotorToSpring, 2) * sea.springDamping;
	return sea;
}
void setSEAOutputMass(SEAParams& sea, double relativeOutputMass)
{
	sea.output.mass = relativeOutputMass * seaOuputMassForNaturalSpringFrequency(sea, 20 * 2 * 3.141592653589793);
	double uptpt = 2000 * 2000;	// units_of_per_tic_per_tic
	sea.output.damping = 0.000681 * sea.output.mass * 2000; // 0.000681 = b/M
	sea.output.friction = 0.003735 * sea.output.mass * uptpt; // 0.003735 = f/M
}
double seaSpringEncoder(SEAParams& sea, VectorXd& state)
{
	return sea.transmissionRatioMotorToSpring * state[0] - state[2] / sea.transmissionRatioSpringToOutput;
}

void testControllerInSimulation()
{
	string dir = "/home/hcrl/wk/mekabot/m3hume_monolith/cppsim/";
	string fname = "demo1.txt";
	ofstream out;
	out.open((dir + fname).c_str());
	//DONE: library dependencies separated through interface.
	//TODO: simulation of an SEA with friction and changeable gain.
	//TODO: model gaussian filtered noise in the SEA
	//TODO: make sure the estimator still converges to reasonable friction gain bias params
	//TODO: use the gain parameter to design the linear compensator
	//TODO: use the LTI system class to simulate the behavior of the system under delayed feedback control
	double timeToRun = 0.2;
	double time = 0;
	double control_dt = 0.001;
	int iterations = timeToRun / control_dt;
	double dt = 0.00004;
	int sim_steps_per_control_step = control_dt / dt;
	SEAParams sea = getDefaultRightKneeModel();
	FrictionBiasGainEstimator estimator;
	estimator.setup(0.99, 1);
	VectorXd state;
	double input = 0;
	double bias = 10;
	auto diff = [&] (VectorXd state)
	{	return (MatrixXd) AccelSEA(state,sea,input,bias);};
	LTIStateSpaceCompensator DACLowpass = setup3rdOrderButterworthDifferentiator(200, control_dt);
	LTIStateSpaceCompensator jointFilter = setup3rdOrderButterworthDifferentiator(200, control_dt);

//	LTIStateSpaceCompensator leadController = setupLead(1.0 / 8.69e0, 100, 10, control_dt);
	LTIStateSpaceCompensator leadController = setupInfiniteGainLead(1.0, 100, 1.0 / 11950.0, control_dt);
//	LTIStateSpaceCompensator leadController = setupLead(1.0 / 8.69e2, 10, 1, control_dt); this worked.
	Oscillator sineOsc1(control_dt);
	sineOsc1.frequency = 4.0;
	Oscillator experimentOsc(control_dt);
	experimentOsc.frequency = 1.0 / timeToRun;

	RungeKutta solver;
	solver.setDormandPrince(4);
	state.setZero(4);
	VectorXd dacFilterInputs(1);
	VectorXd jointFilterInput(1);
	VectorXd filteredJoint(3);
	VectorXd leadInput(1);
	state << 1.0 / 0.02863 * 100, 0.0, 1.0 / 1.58 * 100, 0.0;
	double lpDac;
	for (int i = 0; i < iterations; i++)
	{
		double experiment = experimentOsc.getNext();
		if (experiment > 0.95)
		{
			setSEAOutputMass(sea, 1.3); //mass doubles half way into the experiment
		}
		double sineOscVal = oscMappedSine(sineOsc1.getNext(), -2e6, 2e6);
//		double pulseOscVale = sineOsc1.getNext() > 0.5 ? 1.0 : -1.0;
		jointFilterInput << state[2];	//avoids cheating;
		dacFilterInputs << input; //adds delay?
		filteredJoint = jointFilter.update(jointFilterInput);
		lpDac = DACLowpass.update(dacFilterInputs)[0];
		leadInput << -500 - state[2] - state[0] * 0.375; //state[2];
		input = leadController.update(leadInput)[0];	//control;
		bias = sineOscVal;
		VectorXd estimatedGainFrictionBias = estimator.update((double) filteredJoint[2], (double) filteredJoint[1],
				lpDac);

		for (int j = 0; j < sim_steps_per_control_step; j++)
		{
//			state = SimSEA(state, sea, input, bias, dt);
			state = solver.evaluate(state, diff, dt).col(0);
			time += dt;
		}
		out << time << " " //
				<< input << " " //
				<< state[0] << " " //
				<< state[1] << " " //
				<< state[2] << " " //
				<< state[3] << " " //
				<< seaSpringEncoder(sea, state) << " " //
				<< AccelSEA(state, sea, input, bias)[3] << " " //
				<< filteredJoint[0] << " " //
				<< filteredJoint[1] << " " //
				<< filteredJoint[2] << " " //
				<< bias << " " //
				<< seaGain(sea) << " "  //
				<< estimatedGainFrictionBias[0] << " " //
				<< estimatedGainFrictionBias[1] << " " //
				<< estimatedGainFrictionBias[2] << " " //
				<< lpDac << "\n";
	}
	out.close();

}

void testSEASim()
{
	cout << "Sanity Checking SEA Simulation" << endl;
	SEAParams sea = getDefaultRightKneeModel();
	VectorXd state(4);
	VectorXd expectedStateDot(4);
	VectorXd stateDot;
	double input(0), bias(0);
	state << 0.0, 0.0, 0.0, 0.0;
	input = 0;
	bias = 0;
	stateDot = AccelSEA(state, sea, input, bias);
	expectedStateDot << 0.0, 0.0, 0.0, 0.0;
	assertEqualMatrices("zero accel", stateDot, expectedStateDot, 1e-12);
	state << 0.0, 0.0, 0.0, 0.0;
	input = 100000;
	bias = 0;
	stateDot = AccelSEA(state, sea, input, bias);
	expectedStateDot << 0.0, 0.007972 * input * 4e6, 0.0, 0.0;
	assertEqualMatrices("accel", stateDot, expectedStateDot, 1e-2);
	state << 0.0, 1e20, 0.0, 0.0;
	input = 0;
	bias = 0;
	stateDot = AccelSEA(state, sea, input, bias);
	expectedStateDot << 1e20, -0.000798 * 1e20 * 2e3, 0.0, 0.0;
	//assertEqualMatrices("accel", stateDot, expectedStateDot, 1e8);
//	constantAccel=-56.314494, currentConstant=0.007972, springConstant=0.006355, coulombFriction=-0.265531, damping=-0.000798
//	xdd:= constantAccel=7.985633, springConstant=-0.000891, linearGravity=-0.000013, damping=-0.000681, friction=-0.003735

}
void testCompensatorFeedback()
{
	cout << "Testing Compensator Feedback" << endl;
	LTIStateSpaceCompensator Gp = setupFirstOrderLowpass(1,0.001);
	LTIStateSpaceCompensator Gk = setupLead(100,1000,10,0.001);
	LTIStateSpaceCompensator Gcl = compensatorFeedback(Gp, Gk);
	LTIStateSpaceCompensator Gcl2 = compensatorFeedback(Gp, Gk*0.01);
	LTIStateSpaceCompensator Gcl3 = compensatorFeedback(Gp, Gk*0.0001);
	LTIStateSpaceCompensator Gcl4 = compensatorFeedback(Gp, Gk*1e8);
	vector <double> num{1.0};
	vector <double> den{0.0, 0.0, 1.0};
//	LTIStateSpaceCompensator plant =
	LTIStateSpaceCompensator leadController = setupInfiniteGainLead(1.0, 100, 1.0 / 11950.0, 0.001);

}
using namespace uta::ui;
int main()
{
	cout << "Hello 2 World!!!" << endl; // prints Hello World!!!
//	testOutputOverTime();
//	testKinematics();
//	testShorts();
//	testCalibration();
//	testPlotting();
//	testSEASim();
//	testFrictionBiasGainEstimatorUpdate();
//	testControllerInSimulation();
	testCompensatorFeedback();
	uta::hume::ui::HumeJoystickGainTuner reader;
	reader.start();
	cout << "Well done world!" << endl;
	pause();
	return 0;
}
