/*
 ============================================================================
 Name        : JoystickDriver.cpp
 Author      : Gray Thomas
 Version     :
 Copyright   : All rights reserved
 Description : Hello World in C++,
 ============================================================================
 */

#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "Daemon.hpp"
#include <stdexcept>
#include "joystick_udp.hpp"
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
// #include "JoystickReceiver.hpp"
using namespace std;
namespace gu{
namespace ui{

void send_udp(JoystickState *pBuf, const char* ipAddress);


struct sockaddr_in si_other;
int slen;

int s = 0;
int indexOfMessage = 0;
void send_udp(JoystickState *pMsg, const char * sendAddress)
{
	if (s == 0)
	{
		if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
			return;
		si_other.sin_addr.s_addr = inet_addr(sendAddress);

	}

	struct sockaddr_in si_to;
	memset((char *) &si_to, 0, sizeof(si_to));
	si_to.sin_family = AF_INET;
	si_to.sin_port = htons(JOYSTICK_PORT);
	si_to.sin_addr.s_addr = inet_addr(sendAddress); // this is the meka ip address

	slen = sizeof(si_to);

	int ret = sendto(s, pMsg, sizeof(JoystickState), 0, (const struct sockaddr*) &si_to, sizeof(si_to));
	if (indexOfMessage % 100 == 0)
		fprintf(stderr, "Size of message is %d, ADDR: %08x(%d:%d %d)\n", (int) sizeof(JoystickState),si_to.sin_addr.s_addr, slen, (int) sizeof(si_to), ret);
	++indexOfMessage;
}

class Joystick: public threads::Daemon
{
	int fileNumber;
	const char * ip;
	struct
	{
		unsigned int time;
		short value;
		unsigned char type;
		unsigned char number;
	} message;

	JoystickState joystickState;
protected:
	void handleButton(int number, short isOn)
	{
		if (number < 0)
			return;
		if (number > 13)
			return;
		joystickState.buttons[number] = isOn;
		send_udp(&joystickState, ip);
	}
	void handleAxis(int number, short value)
	{
		if (number < 0)
			return;
		if (number > 6)
			return;
		joystickState.axes[number] = value;
		send_udp(&joystickState, ip);
	}
	void loop(void)
	{

		int bytes = read(fileNumber, &message, sizeof(message));
		if (bytes == 8)
			switch (message.type)
			{
			case 1:
			case 129:
				handleButton(message.number, message.value);
				break;
			case 2:
			case 130:
				handleAxis(message.number, message.value);
				break;
			default:
				break;
			}
	}
public:
	Joystick(const char * input, const char * ip)
	{
		fileNumber = open(input, O_RDONLY | O_NONBLOCK);
		if (fileNumber < 0)
			throw runtime_error("file failed to open");
		this->ip = ip;
	}
	~Joystick()
	{
		close(fileNumber);
	}
};

}
}
using namespace gu::ui;
using namespace gu::threads;
int main(int argc, char *argv[])
{
	const char * ipstring = "";
	if (argc != 2)
	{
		ipstring = "192.168.1.6";
		fprintf(stderr, "No IP address specified, using default %s\n", ipstring);
	}
	else
	{
		int a, b, c, d;
		if (sscanf(argv[1], "%d.%d.%d.%d", &a, &b, &c, &d) != 4)
		{
			ipstring = "192.168.1.6";
			fprintf(stderr, "Invalid IP: %s, using default %s\n", argv[1], ipstring);
		}
		else
		{
			ipstring = argv[1];
			fprintf(stderr, "Good IP: using %d.%d.%d.%d = %s\n", a, b, c, d, ipstring);
		}
	}
	Joystick joystick("/dev/input/js0", ipstring);
//	JoystickReader reader;
	joystick.start();
//	reader.start();
	cout << "Hello World2!!!" << endl; /* prints Hello World!!! */
	pause();

	puts("Terminated.");
	return 0;
}
