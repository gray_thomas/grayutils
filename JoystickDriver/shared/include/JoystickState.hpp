/*
 * JoystickState.hpp
 *
 *    Created on: Apr 21, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */
#ifndef JOYSTICKSTATE_HPP_
#define JOYSTICKSTATE_HPP_
//#define POS_PORT                51128
#define JOYSTICK_PORT                51129
namespace gu
{
namespace ui
{

#pragma pack(push, 1)
typedef struct
{
	short axes[7];
	bool buttons[14];
} JoystickState;
#pragma pack(pop)
}
}

#endif /* JOYSTICKSTATE_HPP_ */
