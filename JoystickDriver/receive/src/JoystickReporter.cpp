/*
 * JoystickReporter.cpp
 *
 *    Created on: Apr 21, 2014
 *        Author: Gray Thomas
 *   Affiliation: Human Centered Robotics Lab, University of Texas at Austin
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include "JoystickReceiver.hpp"

using namespace std;
using namespace gu::ui;
using namespace gu::threads;
int main(void)
{
	JoystickReader reader;
	reader.start();
	cout << "Hello World Joystick Reporter!!!" << endl; /* prints Hello World!!! */
	pause();

	puts("Terminated.");
	return 0;
}
