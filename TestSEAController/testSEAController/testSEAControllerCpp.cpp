
#include <cmath>
#include <iostream>
#include <vector>
#include <functional>
#include <boost/python.hpp>
#include <boost/python/stl_iterator.hpp>
#include <AttitudeEstimator/HumePoseKalmanFilter.hpp>

namespace example
{
namespace bp = boost::python;
void NotImplementedError()
{
    PyErr_SetString(PyExc_NotImplementedError, "Not Implemented");
    bp::throw_error_already_set();
}
void IndexError()
{
    PyErr_SetString(PyExc_IndexError, "Index Error");
    bp::throw_error_already_set();
}
void RuntimeError(const char *error_string)
{
    PyErr_SetString(PyExc_RuntimeError, error_string);
    bp::throw_error_already_set();
}
void ValueError(const char *error_string)
{
    PyErr_SetString(PyExc_ValueError, error_string);
    bp::throw_error_already_set();
}
void StopIteration()
{
    PyErr_SetString(PyExc_StopIteration, "Stop Iteration");
    bp::throw_error_already_set();
}

struct VectorToTupleType
{
    VectorToTupleType()
    {
        bp::to_python_converter<const std::vector<double>, VectorToTupleType>();
    }

    static PyObject *convert(const std::vector<double> &x)
    {
        bp::list new_tuple;
        for (auto i : x) new_tuple.append(i);
        return bp::incref(bp::tuple(new_tuple).ptr());
    }
};
struct VectorFromTupleType
{
    VectorFromTupleType()
    {
        bp::converter::registry::push_back(&convertible, &construct,
                                           bp::type_id<std::vector<double>>());
    }

    static void *convertible(PyObject *obj_ptr)
    {
        if (!PyTuple_Check(obj_ptr)) return 0;
        return obj_ptr;
    }

    static void construct(PyObject *obj_ptr,
                          bp::converter::rvalue_from_python_stage1_data *data)
    {
        assert(PyTuple_Check(obj_ptr));
        unsigned int length = PyTuple_Size(obj_ptr);
        void *storage = ((bp::converter::rvalue_from_python_storage< std::vector<double> > *)data)->storage.bytes;
        new (storage) std::vector<double>(length);
        for (unsigned int i = 0; i < length; ++i)
            static_cast< std::vector<double>* >(storage)->at(i)
                = PyFloat_AsDouble(PyTuple_GetItem(obj_ptr, i));
        data->convertible = storage;
    }
};

bp::object pass_through(const bp::object &obj)
{
    return obj;
}
class ExampleCppClass
{
public:
    int memberVar1 = 0;
    int getVar1()
    {
        return memberVar1;
    }
    void setVar1(int var)
    {
        memberVar1 = var;
    }

};

}//namespace example
// in the root namespace
namespace bp = boost::python;

class example_wrapper: public example::ExampleCppClass
{
public:
    typedef double num_type;
    typedef std::vector<num_type> vec_type;

protected: //makes derived code much easier to read
    enum my_enum {A, B, C};

    boost::python::object num, str;
    vec_type vec1, vec2, vec3;
    num_type num1, num2, num3;
    my_enum enum1, enum2, enum3;

public:
    num_type time_tolerance;
    bool tracking_events;
    int i;
    int readonly1;

    example_wrapper() = delete;
    example_wrapper(boost::python::object num,
                    boost::python::object str) :
        num(num),
        str(str),
        readonly1(42)
    {
    	i=0;
    }
    void def_arg_example(bp::object arg_with_default = bp::object())
    {
        namespace bp = boost::python;
    }
    
    boost::python::object next()
    {
        namespace bp = boost::python;
        i++;
        if (i>=20)
        	example::StopIteration();
        return bp::make_tuple(1.134,12342.0);
    }
    boost::python::object return_py_list()
    {
        namespace bp = boost::python;
        bp::list alist;
        double doub1 = 5.234;
        double doub2 = 2.2134;
        boost::python::object tup1 = bp::make_tuple(doub1, doub2);
        alist.append(tup1);
        // bp::extract<num_type>(system.attr("state")[0])
        // bp::object event, iter = system.attr("events").attr("__iter__")();
        return alist;
    }
    boost::python::str get_hello() const
    {
        return "status undefined!";
    }
};
char const* greet()
{
	return "hello world from cpptests.";
}
BOOST_PYTHON_MODULE(testSEAControllerCpp)
{
    using namespace boost::python;
    example::VectorToTupleType vec2tup;
    example::VectorFromTupleType vecFtup;

    class_<example_wrapper >("example_cpp_class",init<object,object>())
    // .def(init<object,object>)
    .add_property("memberVar1", &example_wrapper::getVar1, &example_wrapper::setVar1)
    .def("__iter__", &example::pass_through)
    .def("next", &example_wrapper::next)
    .def("def_arg_example", &example_wrapper::def_arg_example, (arg("arg_with_default") = object()) )
    .def_readonly("readonly1", &example_wrapper::readonly1);
    def("greet",greet);
}
